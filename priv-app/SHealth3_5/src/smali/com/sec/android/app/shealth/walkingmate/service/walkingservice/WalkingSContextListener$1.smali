.class Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;
.super Ljava/lang/Object;
.source "WalkingSContextListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->setActiveTime(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

.field final synthetic val$buffer:I

.field final synthetic val$save_duration:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;II)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$buffer:I

    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 43

    .prologue
    .line 357
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v42

    monitor-enter v42

    .line 358
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$buffer:I

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    add-long/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v5, v5

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v36

    .line 360
    .local v36, "startTime":Ljava/lang/String;
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    .line 361
    .local v28, "query":Ljava/lang/StringBuilder;
    const-string v2, "SELECT * FROM walk_info_extended"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    const-string v2, " WHERE "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    const-string/jumbo v2, "user_device__id LIKE \'10009%\' "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    const-string v2, " AND "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    const/4 v13, 0x0

    .line 369
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_1
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastDurationUpdateTime:J

    invoke-static/range {v36 .. v36}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 370
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 373
    :cond_0
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setActiveTime() sBufferForSteps.accumlatedDuration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " save_duration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cursor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    if-eqz v13, :cond_12

    .line 375
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    .line 376
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 377
    const-string v2, "active_time"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 378
    .local v8, "accumulatedDuration":J
    new-instance v39, Landroid/content/ContentValues;

    invoke-direct/range {v39 .. v39}, Landroid/content/ContentValues;-><init>()V

    .line 379
    .local v39, "values":Landroid/content/ContentValues;
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "db startTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v36

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "db activeTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v2, v2

    add-long/2addr v8, v2

    .line 383
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iput-wide v8, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    .line 384
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    const-string v3, "_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    .line 385
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    invoke-static/range {v36 .. v36}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastDurationUpdateTime:J

    .line 386
    const-string v2, "active_time"

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    iget-wide v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 388
    :try_start_2
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v5

    iget-wide v5, v5, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 721
    .end local v8    # "accumulatedDuration":J
    .end local v39    # "values":Landroid/content/ContentValues;
    :cond_1
    :goto_0
    if-eqz v13, :cond_2

    .line 722
    :try_start_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 725
    :cond_2
    :goto_1
    monitor-exit v42
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 726
    return-void

    .line 390
    .restart local v8    # "accumulatedDuration":J
    .restart local v39    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v20

    .line 391
    .local v20, "ie":Ljava/lang/IllegalArgumentException;
    :try_start_4
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 718
    .end local v8    # "accumulatedDuration":J
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v39    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v16

    .line 719
    .local v16, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 721
    if-eqz v13, :cond_2

    .line 722
    :try_start_6
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 725
    .end local v13    # "cursor":Landroid/database/Cursor;
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v28    # "query":Ljava/lang/StringBuilder;
    .end local v36    # "startTime":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v42
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v2

    .line 395
    .restart local v13    # "cursor":Landroid/database/Cursor;
    .restart local v28    # "query":Ljava/lang/StringBuilder;
    .restart local v36    # "startTime":Ljava/lang/String;
    :cond_3
    :try_start_7
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-lez v2, :cond_11

    .line 396
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setActiveTime() duration is more than 10 mins!! sBufferForSteps.accumlatedDuration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " save_duration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int/lit8 v31, v2, 0x1

    .line 398
    .local v31, "slotCount":I
    new-instance v39, Landroid/content/ContentValues;

    invoke-direct/range {v39 .. v39}, Landroid/content/ContentValues;-><init>()V

    .line 400
    .restart local v39    # "values":Landroid/content/ContentValues;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 401
    .local v11, "builder":Ljava/lang/StringBuilder;
    const-string v2, "SELECT start_time"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    const-string v2, " "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    const-string v2, "FROM walk_info"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    const-string v2, " "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    const-string v2, "WHERE total_step > 0 "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    const-string v2, " AND "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    const-string/jumbo v2, "user_device__id LIKE \'10009%\' "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    const-string v2, " AND "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time builder query = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string/jumbo v7, "start_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v25

    .line 412
    .local v25, "lockCursor":Landroid/database/Cursor;
    if-eqz v25, :cond_f

    .line 414
    :try_start_8
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 416
    const-wide/16 v23, 0x0

    .line 417
    .local v23, "lastStartTime":J
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long v37, v2, v4

    .line 420
    .local v37, "totalDuration":J
    :cond_4
    const-wide/16 v29, -0x1

    .line 421
    .local v29, "rowId":J
    const-string/jumbo v2, "start_time"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 422
    .local v40, "walkStartTime":J
    const/4 v2, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 423
    const-string v2, "SELECT * FROM walk_info_extended"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    const-string v2, " WHERE "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    const-string/jumbo v2, "user_device__id LIKE \'10009%\' "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    const-string v2, " AND "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v40

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 429
    .local v10, "active":Landroid/database/Cursor;
    if-eqz v10, :cond_6

    .line 430
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 431
    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v29

    .line 432
    const-string/jumbo v2, "start_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    .line 433
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setActiveTime() rowId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v29

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 436
    :cond_5
    :try_start_9
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 441
    :cond_6
    :goto_2
    const-wide/32 v2, 0x927c0

    cmp-long v2, v37, v2

    if-lez v2, :cond_9

    .line 442
    const-wide/32 v14, 0x927c0

    .line 443
    .local v14, "durationInsert":J
    const-wide/32 v2, 0x927c0

    sub-long v37, v37, v2

    .line 447
    :goto_3
    if-lez v31, :cond_a

    const/16 v18, 0x1

    .local v18, "hasMore":Z
    :goto_4
    if-eqz v18, :cond_7

    .line 448
    add-int/lit8 v31, v31, -0x1

    .line 450
    :cond_7
    :try_start_a
    invoke-virtual/range {v39 .. v39}, Landroid/content/ContentValues;->clear()V

    .line 451
    const-wide/16 v2, 0x0

    cmp-long v2, v29, v2

    if-lez v2, :cond_b

    .line 452
    const-string v2, "active_time"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 454
    :try_start_b
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v29

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 455
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    move-wide/from16 v0, v40

    iput-wide v0, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastDurationUpdateTime:J
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 471
    :goto_5
    if-nez v18, :cond_c

    .line 476
    :goto_6
    if-lez v31, :cond_e

    .line 478
    :try_start_c
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remaining slot size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v31

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    const/4 v12, 0x0

    .local v12, "count":I
    :goto_7
    move/from16 v0, v31

    if-ge v12, v0, :cond_e

    .line 483
    const-wide/32 v2, 0x927c0

    cmp-long v2, v37, v2

    if-lez v2, :cond_d

    .line 484
    const-wide/32 v26, 0x927c0

    .line 485
    .local v26, "moreDurationInsert":J
    const-wide/32 v2, 0x927c0

    sub-long v37, v37, v2

    .line 489
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v3, 0xa

    move-wide/from16 v0, v23

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v21

    .line 490
    .local v21, "insertStartTime":J
    move-wide/from16 v23, v21

    .line 491
    const-string/jumbo v2, "start_time"

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 492
    const-string v2, "end_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v4, 0xa

    move-wide/from16 v0, v21

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 493
    const-string v2, "active_time"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 494
    const-string/jumbo v2, "user_device__id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 496
    :try_start_d
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_7
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 480
    :goto_9
    add-int/lit8 v12, v12, 0x1

    goto :goto_7

    .line 437
    .end local v12    # "count":I
    .end local v14    # "durationInsert":J
    .end local v18    # "hasMore":Z
    .end local v21    # "insertStartTime":J
    .end local v26    # "moreDurationInsert":J
    :catch_2
    move-exception v17

    .line 438
    .local v17, "ex":Ljava/lang/Exception;
    :try_start_e
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto/16 :goto_2

    .line 503
    .end local v10    # "active":Landroid/database/Cursor;
    .end local v17    # "ex":Ljava/lang/Exception;
    .end local v23    # "lastStartTime":J
    .end local v29    # "rowId":J
    .end local v37    # "totalDuration":J
    .end local v40    # "walkStartTime":J
    :catch_3
    move-exception v2

    .line 506
    :try_start_f
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_0

    .line 507
    :catch_4
    move-exception v16

    .line 508
    .restart local v16    # "e":Ljava/lang/Exception;
    :try_start_10
    const-string v2, "WalkingSContextListener"

    const-string v3, "Exception occurred while closing cursor"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_0

    .line 721
    .end local v11    # "builder":Ljava/lang/StringBuilder;
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v25    # "lockCursor":Landroid/database/Cursor;
    .end local v31    # "slotCount":I
    .end local v39    # "values":Landroid/content/ContentValues;
    :catchall_1
    move-exception v2

    if-eqz v13, :cond_8

    .line 722
    :try_start_11
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 445
    .restart local v10    # "active":Landroid/database/Cursor;
    .restart local v11    # "builder":Ljava/lang/StringBuilder;
    .restart local v23    # "lastStartTime":J
    .restart local v25    # "lockCursor":Landroid/database/Cursor;
    .restart local v29    # "rowId":J
    .restart local v31    # "slotCount":I
    .restart local v37    # "totalDuration":J
    .restart local v39    # "values":Landroid/content/ContentValues;
    .restart local v40    # "walkStartTime":J
    :cond_9
    move-wide/from16 v14, v37

    .restart local v14    # "durationInsert":J
    goto/16 :goto_3

    .line 447
    :cond_a
    const/16 v18, 0x0

    goto/16 :goto_4

    .line 456
    .restart local v18    # "hasMore":Z
    :catch_5
    move-exception v20

    .line 457
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_12
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_3
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    goto/16 :goto_5

    .line 505
    .end local v10    # "active":Landroid/database/Cursor;
    .end local v14    # "durationInsert":J
    .end local v18    # "hasMore":Z
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v23    # "lastStartTime":J
    .end local v29    # "rowId":J
    .end local v37    # "totalDuration":J
    .end local v40    # "walkStartTime":J
    :catchall_2
    move-exception v2

    .line 506
    :try_start_13
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_9
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 510
    :goto_a
    :try_start_14
    throw v2
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_1
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 460
    .restart local v10    # "active":Landroid/database/Cursor;
    .restart local v14    # "durationInsert":J
    .restart local v18    # "hasMore":Z
    .restart local v23    # "lastStartTime":J
    .restart local v29    # "rowId":J
    .restart local v37    # "totalDuration":J
    .restart local v40    # "walkStartTime":J
    :cond_b
    :try_start_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-wide/from16 v0, v40

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v23

    .line 461
    const-string/jumbo v2, "start_time"

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 462
    const-string v2, "end_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v4, 0xa

    move-wide/from16 v0, v40

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 463
    const-string v2, "active_time"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 464
    const-string/jumbo v2, "user_device__id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 466
    :try_start_16
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_16
    .catch Ljava/lang/IllegalArgumentException; {:try_start_16 .. :try_end_16} :catch_6
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_3
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    goto/16 :goto_5

    .line 467
    :catch_6
    move-exception v20

    .line 468
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_17
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 474
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_c
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    goto/16 :goto_6

    .line 487
    .restart local v12    # "count":I
    :cond_d
    move-wide/from16 v26, v37

    .restart local v26    # "moreDurationInsert":J
    goto/16 :goto_8

    .line 497
    .restart local v21    # "insertStartTime":J
    :catch_7
    move-exception v20

    .line 498
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_3
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    goto/16 :goto_9

    .line 506
    .end local v10    # "active":Landroid/database/Cursor;
    .end local v12    # "count":I
    .end local v14    # "durationInsert":J
    .end local v18    # "hasMore":Z
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v21    # "insertStartTime":J
    .end local v23    # "lastStartTime":J
    .end local v26    # "moreDurationInsert":J
    .end local v29    # "rowId":J
    .end local v37    # "totalDuration":J
    .end local v40    # "walkStartTime":J
    :cond_e
    :try_start_18
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_8
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    goto/16 :goto_0

    .line 507
    :catch_8
    move-exception v16

    .line 508
    .restart local v16    # "e":Ljava/lang/Exception;
    :try_start_19
    const-string v2, "WalkingSContextListener"

    const-string v3, "Exception occurred while closing cursor"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 507
    .end local v16    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v16

    .line 508
    .restart local v16    # "e":Ljava/lang/Exception;
    const-string v3, "WalkingSContextListener"

    const-string v4, "Exception occurred while closing cursor"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_a

    .line 515
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_f
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long v37, v2, v4

    .line 516
    .restart local v37    # "totalDuration":J
    const-string v2, "WalkingSContextListener"

    const-string v3, "Taking else case as LOCK cursor is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_b
    move/from16 v0, v19

    move/from16 v1, v31

    if-ge v0, v1, :cond_1

    .line 520
    const-wide/32 v2, 0x927c0

    cmp-long v2, v37, v2

    if-lez v2, :cond_10

    .line 521
    const-wide/32 v32, 0x927c0

    .line 522
    .local v32, "splitDurationInsert":J
    const-wide/32 v2, 0x927c0

    sub-long v37, v37, v2

    .line 526
    :goto_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-static/range {v36 .. v36}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    mul-int/lit8 v5, v19, 0xa

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v34

    .line 527
    .local v34, "splitTime":J
    const-string/jumbo v2, "start_time"

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 528
    const-string v2, "end_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v4, 0xa

    move-wide/from16 v0, v34

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 529
    const-string v2, "active_time"

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 530
    const-string/jumbo v2, "user_device__id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_1
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    .line 532
    :try_start_1a
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1a .. :try_end_1a} :catch_a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_1
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    .line 517
    :goto_d
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_b

    .line 524
    .end local v32    # "splitDurationInsert":J
    .end local v34    # "splitTime":J
    :cond_10
    move-wide/from16 v32, v37

    .restart local v32    # "splitDurationInsert":J
    goto :goto_c

    .line 533
    .restart local v34    # "splitTime":J
    :catch_a
    move-exception v20

    .line 534
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_1b
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 540
    .end local v11    # "builder":Ljava/lang/StringBuilder;
    .end local v19    # "i":I
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v25    # "lockCursor":Landroid/database/Cursor;
    .end local v31    # "slotCount":I
    .end local v32    # "splitDurationInsert":J
    .end local v34    # "splitTime":J
    .end local v37    # "totalDuration":J
    .end local v39    # "values":Landroid/content/ContentValues;
    :cond_11
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "db creation 1: start_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    new-instance v39, Landroid/content/ContentValues;

    invoke-direct/range {v39 .. v39}, Landroid/content/ContentValues;-><init>()V

    .line 542
    .restart local v39    # "values":Landroid/content/ContentValues;
    const-string/jumbo v2, "start_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 543
    const-string v2, "end_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/16 v6, 0xa

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 544
    const-string v2, "active_time"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 545
    const-string/jumbo v2, "user_device__id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_1
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    .line 547
    :try_start_1c
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1c .. :try_end_1c} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_1
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    goto/16 :goto_0

    .line 548
    :catch_b
    move-exception v20

    .line 549
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_1d
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 554
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v39    # "values":Landroid/content/ContentValues;
    :cond_12
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setActiveTime() taking cursor null else!! sBufferForSteps.DBid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_21

    .line 556
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-lez v2, :cond_20

    .line 557
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int/lit8 v31, v2, 0x1

    .line 558
    .restart local v31    # "slotCount":I
    new-instance v39, Landroid/content/ContentValues;

    invoke-direct/range {v39 .. v39}, Landroid/content/ContentValues;-><init>()V

    .line 560
    .restart local v39    # "values":Landroid/content/ContentValues;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 561
    .restart local v11    # "builder":Ljava/lang/StringBuilder;
    const-string v2, "SELECT start_time"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string v2, " "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    const-string v2, "FROM walk_info"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    const-string v2, " "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    const-string v2, "WHERE total_step > 0 "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    const-string v2, " AND "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    const-string/jumbo v2, "user_device__id LIKE \'10009%\' "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    const-string v2, " AND "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v2, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v4, v4

    add-long v37, v2, v4

    .line 571
    .restart local v37    # "totalDuration":J
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string/jumbo v7, "start_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 572
    .restart local v25    # "lockCursor":Landroid/database/Cursor;
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setActiveTime() lockCursor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "accumulation builder query = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_1
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    .line 573
    if-eqz v25, :cond_1f

    .line 575
    :try_start_1e
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 577
    const-wide/16 v23, 0x0

    .line 580
    .restart local v23    # "lastStartTime":J
    :cond_13
    const-wide/16 v29, -0x1

    .line 581
    .restart local v29    # "rowId":J
    const-string/jumbo v2, "start_time"

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 582
    .restart local v40    # "walkStartTime":J
    const/4 v2, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 583
    const-string v2, "SELECT * FROM walk_info_extended"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    const-string v2, " WHERE "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    const-string/jumbo v2, "user_device__id LIKE \'10009%\' "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    const-string v2, " AND "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v40

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 589
    .restart local v10    # "active":Landroid/database/Cursor;
    if-eqz v10, :cond_15

    .line 590
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 591
    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v29

    .line 592
    const-string/jumbo v2, "start_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    .line 593
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setActiveTime() rowId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v29

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_d
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    .line 596
    :cond_14
    :try_start_1f
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_c
    .catchall {:try_start_1f .. :try_end_1f} :catchall_3

    .line 601
    :cond_15
    :goto_e
    const-wide/32 v2, 0x927c0

    cmp-long v2, v37, v2

    if-lez v2, :cond_17

    .line 602
    const-wide/32 v14, 0x927c0

    .line 603
    .restart local v14    # "durationInsert":J
    const-wide/32 v2, 0x927c0

    sub-long v37, v37, v2

    .line 607
    :goto_f
    if-lez v31, :cond_18

    const/16 v18, 0x1

    .restart local v18    # "hasMore":Z
    :goto_10
    if-eqz v18, :cond_16

    .line 608
    add-int/lit8 v31, v31, -0x1

    .line 610
    :cond_16
    :try_start_20
    invoke-virtual/range {v39 .. v39}, Landroid/content/ContentValues;->clear()V

    .line 611
    const-wide/16 v2, 0x0

    cmp-long v2, v29, v2

    if-lez v2, :cond_19

    .line 612
    const-string v2, "active_time"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_d
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    .line 614
    :try_start_21
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v29

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 615
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    move-wide/from16 v0, v40

    iput-wide v0, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastDurationUpdateTime:J
    :try_end_21
    .catch Ljava/lang/IllegalArgumentException; {:try_start_21 .. :try_end_21} :catch_f
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_d
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    .line 631
    :goto_11
    if-nez v18, :cond_1a

    .line 636
    :goto_12
    if-lez v31, :cond_1e

    .line 638
    :try_start_22
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remaining slot size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v31

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    const/4 v12, 0x0

    .restart local v12    # "count":I
    :goto_13
    move/from16 v0, v31

    if-ge v12, v0, :cond_1e

    .line 643
    const-wide/32 v2, 0x927c0

    cmp-long v2, v37, v2

    if-lez v2, :cond_1b

    .line 644
    const-wide/32 v26, 0x927c0

    .line 645
    .restart local v26    # "moreDurationInsert":J
    const-wide/32 v2, 0x927c0

    sub-long v37, v37, v2

    .line 649
    :goto_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v3, 0xa

    move-wide/from16 v0, v23

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v21

    .line 650
    .restart local v21    # "insertStartTime":J
    move-wide/from16 v23, v21

    .line 651
    const-string/jumbo v2, "start_time"

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 652
    const-string v2, "end_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v4, 0xa

    move-wide/from16 v0, v21

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 653
    const-string v2, "active_time"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 654
    const-string/jumbo v2, "user_device__id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_d
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    .line 656
    :try_start_23
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_23
    .catch Ljava/lang/IllegalArgumentException; {:try_start_23 .. :try_end_23} :catch_11
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_d
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    .line 640
    :goto_15
    add-int/lit8 v12, v12, 0x1

    goto :goto_13

    .line 597
    .end local v12    # "count":I
    .end local v14    # "durationInsert":J
    .end local v18    # "hasMore":Z
    .end local v21    # "insertStartTime":J
    .end local v26    # "moreDurationInsert":J
    :catch_c
    move-exception v17

    .line 598
    .restart local v17    # "ex":Ljava/lang/Exception;
    :try_start_24
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_d
    .catchall {:try_start_24 .. :try_end_24} :catchall_3

    goto/16 :goto_e

    .line 685
    .end local v10    # "active":Landroid/database/Cursor;
    .end local v17    # "ex":Ljava/lang/Exception;
    .end local v23    # "lastStartTime":J
    .end local v29    # "rowId":J
    .end local v40    # "walkStartTime":J
    :catch_d
    move-exception v16

    .line 686
    .restart local v16    # "e":Ljava/lang/Exception;
    :try_start_25
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_3

    .line 689
    :try_start_26
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_e
    .catchall {:try_start_26 .. :try_end_26} :catchall_1

    goto/16 :goto_0

    .line 690
    :catch_e
    move-exception v2

    goto/16 :goto_0

    .line 605
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v10    # "active":Landroid/database/Cursor;
    .restart local v23    # "lastStartTime":J
    .restart local v29    # "rowId":J
    .restart local v40    # "walkStartTime":J
    :cond_17
    move-wide/from16 v14, v37

    .restart local v14    # "durationInsert":J
    goto/16 :goto_f

    .line 607
    :cond_18
    const/16 v18, 0x0

    goto/16 :goto_10

    .line 616
    .restart local v18    # "hasMore":Z
    :catch_f
    move-exception v20

    .line 617
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_27
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_d
    .catchall {:try_start_27 .. :try_end_27} :catchall_3

    goto/16 :goto_11

    .line 688
    .end local v10    # "active":Landroid/database/Cursor;
    .end local v14    # "durationInsert":J
    .end local v18    # "hasMore":Z
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v23    # "lastStartTime":J
    .end local v29    # "rowId":J
    .end local v40    # "walkStartTime":J
    :catchall_3
    move-exception v2

    .line 689
    :try_start_28
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_15
    .catchall {:try_start_28 .. :try_end_28} :catchall_1

    .line 692
    :goto_16
    :try_start_29
    throw v2
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_1
    .catchall {:try_start_29 .. :try_end_29} :catchall_1

    .line 620
    .restart local v10    # "active":Landroid/database/Cursor;
    .restart local v14    # "durationInsert":J
    .restart local v18    # "hasMore":Z
    .restart local v23    # "lastStartTime":J
    .restart local v29    # "rowId":J
    .restart local v40    # "walkStartTime":J
    :cond_19
    :try_start_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-wide/from16 v0, v40

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v23

    .line 621
    const-string/jumbo v2, "start_time"

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 622
    const-string v2, "end_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v4, 0xa

    move-wide/from16 v0, v40

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 623
    const-string v2, "active_time"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 624
    const-string/jumbo v2, "user_device__id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_d
    .catchall {:try_start_2a .. :try_end_2a} :catchall_3

    .line 626
    :try_start_2b
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2b .. :try_end_2b} :catch_10
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_d
    .catchall {:try_start_2b .. :try_end_2b} :catchall_3

    goto/16 :goto_11

    .line 627
    :catch_10
    move-exception v20

    .line 628
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_2c
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_11

    .line 634
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_1a
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_13

    goto/16 :goto_12

    .line 647
    .restart local v12    # "count":I
    :cond_1b
    move-wide/from16 v26, v37

    .restart local v26    # "moreDurationInsert":J
    goto/16 :goto_14

    .line 657
    .restart local v21    # "insertStartTime":J
    :catch_11
    move-exception v20

    .line 658
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_15

    .line 663
    .end local v10    # "active":Landroid/database/Cursor;
    .end local v12    # "count":I
    .end local v14    # "durationInsert":J
    .end local v18    # "hasMore":Z
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v21    # "insertStartTime":J
    .end local v23    # "lastStartTime":J
    .end local v26    # "moreDurationInsert":J
    .end local v29    # "rowId":J
    .end local v40    # "walkStartTime":J
    :cond_1c
    const-string v2, "WalkingSContextListener"

    const-string v3, "Taking lock cursor else case!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    const/16 v19, 0x0

    .restart local v19    # "i":I
    :goto_17
    move/from16 v0, v19

    move/from16 v1, v31

    if-ge v0, v1, :cond_1e

    .line 667
    const-wide/32 v2, 0x927c0

    cmp-long v2, v37, v2

    if-lez v2, :cond_1d

    .line 668
    const-wide/32 v32, 0x927c0

    .line 669
    .restart local v32    # "splitDurationInsert":J
    const-wide/32 v2, 0x927c0

    sub-long v37, v37, v2

    .line 673
    :goto_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-static/range {v36 .. v36}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    mul-int/lit8 v5, v19, 0xa

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v34

    .line 674
    .restart local v34    # "splitTime":J
    const-string/jumbo v2, "start_time"

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 675
    const-string v2, "end_time"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    const/16 v4, 0xa

    move-wide/from16 v0, v34

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 676
    const-string v2, "active_time"

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 677
    const-string/jumbo v2, "user_device__id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "10009_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_d
    .catchall {:try_start_2c .. :try_end_2c} :catchall_3

    .line 679
    :try_start_2d
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2d .. :try_end_2d} :catch_12
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_3

    .line 664
    :goto_19
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_17

    .line 671
    .end local v32    # "splitDurationInsert":J
    .end local v34    # "splitTime":J
    :cond_1d
    move-wide/from16 v32, v37

    .restart local v32    # "splitDurationInsert":J
    goto :goto_18

    .line 680
    .restart local v34    # "splitTime":J
    :catch_12
    move-exception v20

    .line 681
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_2e
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_d
    .catchall {:try_start_2e .. :try_end_2e} :catchall_3

    goto :goto_19

    .line 689
    .end local v19    # "i":I
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v32    # "splitDurationInsert":J
    .end local v34    # "splitTime":J
    :cond_1e
    :try_start_2f
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_2f} :catch_13
    .catchall {:try_start_2f .. :try_end_2f} :catchall_1

    goto/16 :goto_0

    .line 690
    :catch_13
    move-exception v2

    goto/16 :goto_0

    .line 695
    :cond_1f
    :try_start_30
    const-string v2, "WalkingSContextListener"

    const-string v3, "Lock Cursor is NULL !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 700
    .end local v11    # "builder":Ljava/lang/StringBuilder;
    .end local v25    # "lockCursor":Landroid/database/Cursor;
    .end local v31    # "slotCount":I
    .end local v37    # "totalDuration":J
    .end local v39    # "values":Landroid/content/ContentValues;
    :cond_20
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v2

    iget-wide v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$1;->val$save_duration:I

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    .line 702
    const-string v2, "WalkingSContextListener"

    const-string/jumbo v3, "update using buffer1"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "duration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DBid 1 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    new-instance v39, Landroid/content/ContentValues;

    invoke-direct/range {v39 .. v39}, Landroid/content/ContentValues;-><init>()V

    .line 706
    .restart local v39    # "values":Landroid/content/ContentValues;
    const-string v2, "active_time"

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    iget-wide v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_30} :catch_1
    .catchall {:try_start_30 .. :try_end_30} :catchall_1

    .line 708
    :try_start_31
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v5

    iget-wide v5, v5, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_31
    .catch Ljava/lang/IllegalArgumentException; {:try_start_31 .. :try_end_31} :catch_14
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_31} :catch_1
    .catchall {:try_start_31 .. :try_end_31} :catchall_1

    goto/16 :goto_0

    .line 710
    :catch_14
    move-exception v20

    .line 711
    .restart local v20    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_32
    const-string v2, "WalkingSContextListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 715
    .end local v20    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v39    # "values":Landroid/content/ContentValues;
    :cond_21
    const-string v2, "WalkingSContextListener"

    const-string v3, "data is not saved"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_32} :catch_1
    .catchall {:try_start_32 .. :try_end_32} :catchall_1

    goto/16 :goto_0

    .line 690
    .restart local v11    # "builder":Ljava/lang/StringBuilder;
    .restart local v25    # "lockCursor":Landroid/database/Cursor;
    .restart local v31    # "slotCount":I
    .restart local v37    # "totalDuration":J
    .restart local v39    # "values":Landroid/content/ContentValues;
    :catch_15
    move-exception v3

    goto/16 :goto_16
.end method
