.class final Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;
.super Ljava/lang/Object;
.source "ShareUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareView(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$extraImageList:Ljava/util/List;

.field final synthetic val$mainContent:Landroid/graphics/Bitmap;

.field final synthetic val$mainView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/graphics/Bitmap;Ljava/util/List;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$mainView:Landroid/view/View;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$mainContent:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$extraImageList:Ljava/util/List;

    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 102
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$mainView:Landroid/view/View;

    const v6, 0x7f080798

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 103
    .local v4, "titleView":Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->getScreenshot(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$mainContent:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/2addr v6, v7

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 108
    .local v3, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 109
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1, v0, v8, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$mainContent:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v1, v5, v8, v6, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 112
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$extraImageList:Ljava/util/List;

    if-nez v5, :cond_0

    .line 113
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$activity:Landroid/app/Activity;

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaDialog(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    .line 119
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$activity:Landroid/app/Activity;

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveShareImageToSdCard(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/io/File;

    move-result-object v2

    .line 116
    .local v2, "file":Ljava/io/File;
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$extraImageList:Ljava/util/List;

    const/4 v6, 0x0

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 117
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$1;->val$extraImageList:Ljava/util/List;

    check-cast v5, Ljava/util/ArrayList;

    invoke-static {v6, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaPopup(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0
.end method
