.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;
.super Lcom/sec/android/app/shealth/food/utils/PauseHandler;
.source "BarcodeSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected processMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 184
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 202
    :goto_0
    return-void

    .line 187
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->stopCameraFocusing()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showErrorDialogAboutFailBarcodeSearch()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    goto :goto_0

    .line 191
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->stopCameraFocusing()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcode:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$600(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showSuccessDialog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1000(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_BARCODE_SEARCH:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 199
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected storeMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 179
    const/4 v0, 0x1

    return v0
.end method
