.class public Lcom/sec/android/app/shealth/help/activity/HelpSpO2Activity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpSpO2Activity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 22
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpSpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f09101b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 25
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 13
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    const v0, 0x7f03020a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpSpO2Activity;->setContentView(I)V

    .line 17
    :goto_0
    return-void

    .line 16
    :cond_0
    const v0, 0x7f03020b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpSpO2Activity;->setContentView(I)V

    goto :goto_0
.end method
