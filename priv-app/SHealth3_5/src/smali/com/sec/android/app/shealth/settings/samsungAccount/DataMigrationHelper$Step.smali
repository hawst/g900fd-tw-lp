.class final enum Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
.super Ljava/lang/Enum;
.source "DataMigrationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Step"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum CHECK_SERVER:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum CONFIRM_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum LOCAL_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum LOCAL_MIGRATION:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum NETWORK_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum NOTICE_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum SAMSUNG_ACCOUNT_NOTICE_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

.field public static final enum UPLOAD_DATA_TO_SERVER:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "LOCAL_MIGRATION"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_MIGRATION:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "UPLOAD_DATA_TO_SERVER"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->UPLOAD_DATA_TO_SERVER:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 89
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "CHECK_SERVER"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->CHECK_SERVER:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 91
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "CONFIRM_POPUP"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->CONFIRM_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 92
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "NOTICE_POPUP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NOTICE_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 93
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "PROGRESS_POPUP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 94
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "SAMSUNG_ACCOUNT_NOTICE_POPUP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->SAMSUNG_ACCOUNT_NOTICE_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 95
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "NETWORK_FAILED_POPUP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NETWORK_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 96
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    const-string v1, "LOCAL_FAILED_POPUP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 84
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_MIGRATION:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->UPLOAD_DATA_TO_SERVER:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->CHECK_SERVER:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->CONFIRM_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NOTICE_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->SAMSUNG_ACCOUNT_NOTICE_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NETWORK_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->LOCAL_FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 84
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    return-object v0
.end method
