.class public interface abstract Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IHealthControllerListener"
.end annotation


# virtual methods
.method public abstract onActivityTypeChanged(I)V
.end method

.method public abstract onChangedWorkoutStatus(I)V
.end method

.method public abstract onChannelStateChanged(ILjava/lang/String;)V
.end method

.method public abstract onMaxDurationReached(J)V
.end method

.method public abstract onNewLocation(Landroid/location/Location;)V
.end method

.method public abstract onScreenLockMode(Z)V
.end method

.method public abstract onShownUnlockProgress(Z)V
.end method

.method public abstract onStartWorkOut()V
.end method

.method public abstract onUpdateGoalFromWorkoutController(II)V
.end method

.method public abstract onUpdateLapClock(J)V
.end method

.method public abstract onUpdateVaule(I)V
.end method

.method public abstract onVisualGuideShow(Z)V
.end method
