.class public Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;
    }
.end annotation


# static fields
.field public static final UNDERLINE_STATE_ABNORMAL:I = 0x2

.field public static final UNDERLINE_STATE_INVISIBLE:I = 0x0

.field public static final UNDERLINE_STATE_NORMAL:I = 0x1

.field private static dayId:I


# instance fields
.field dayButtonMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;",
            ">;"
        }
    .end annotation
.end field

.field endOfMonth:J

.field private isFinal:Z

.field mContext:Landroid/content/Context;

.field private mCurrentDay:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field mPeriodType:I

.field mSelectedTime:J

.field private mTime:Ljava/util/Calendar;

.field private resources:Landroid/content/res/Resources;

.field startOfMonth:J

.field final startOfSelectedDay:J

.field final startOfToday:J

.field public uDaysInfo:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x12fd1

    sput v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->dayId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/Calendar;JZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Calendar;",
            "JZ)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfToday:J

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mTime:Ljava/util/Calendar;

    iput-wide p4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mSelectedTime:J

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->dayButtonMap:Ljava/util/Map;

    iget-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mSelectedTime:J

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfSelectedDay:J

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mTime:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfMonth:J

    iget-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfMonth:J

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->endOfMonth:J

    iput-boolean p6, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->isFinal:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;JZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->onCalendarDayClicked(JZ)V

    return-void
.end method

.method private build(I)V
    .locals 0

    return-void
.end method

.method private checkIfClickAllowed(J)Z
    .locals 2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkIfDataExists(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;Ljava/lang/Long;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->uDaysInfo:Ljava/util/TreeMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->uDaysInfo:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->uDaysInfo:Ljava/util/TreeMap;

    invoke-virtual {v0, p2}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->uDaysInfo:Ljava/util/TreeMap;

    invoke-virtual {v0, p2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    if-eqz p1, :cond_1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->checkIfClickAllowed(J)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, p0, v2, v4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;Ljava/lang/Long;Z)V

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->getMedalResourceId()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->getMedalResourceId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->enableMedalView()V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setMedalImage(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setMedalState(Z)V

    :cond_1
    return-void
.end method

.method private onCalendarDayClicked(JZ)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/calendar/OnPeriodSelectedListener;

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mPeriodType:I

    invoke-interface {v0, p1, p2, v1, p3}, Lcom/sec/android/app/shealth/framework/ui/calendar/OnPeriodSelectedListener;->onPeriodSelected(JIZ)V

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    invoke-super {p0}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->dayButtonMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->getItem(I)Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    const/4 v7, 0x2

    const/4 v9, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v0, "Calendar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current date : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v9}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->calendar_day_button:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->resources:Landroid/content/res/Resources;

    invoke-direct {v0, p2, v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;-><init>(Landroid/view/View;Landroid/content/res/Resources;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v5, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->dayId:I

    add-int/lit8 v6, v5, 0x1

    sput v6, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->dayId:I

    invoke-virtual {v0, v5}, Landroid/view/View;->setId(I)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v9}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setText(Ljava/lang/String;)V

    const/4 v0, 0x7

    invoke-virtual {v4, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, v2, :cond_8

    move v0, v2

    :goto_1
    invoke-virtual {v4, v2}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mTime:Ljava/util/Calendar;

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ne v5, v6, :cond_1

    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mTime:Ljava/util/Calendar;

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-eq v5, v6, :cond_a

    :cond_1
    if-eqz v0, :cond_9

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setDayType(I)V

    :cond_2
    :goto_2
    iget-wide v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfToday:J

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-nez v0, :cond_3

    iget-wide v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfToday:J

    iget-wide v7, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfMonth:J

    cmp-long v0, v5, v7

    if-ltz v0, :cond_3

    iget-wide v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfToday:J

    iget-wide v7, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->endOfMonth:J

    cmp-long v0, v5, v7

    if-gtz v0, :cond_3

    invoke-virtual {v1, v9}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setDayType(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->checkIfClickAllowed(J)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, p0, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;Ljava/lang/Long;Z)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-wide v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfSelectedDay:J

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-nez v0, :cond_5

    iget-wide v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfSelectedDay:J

    iget-wide v7, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfMonth:J

    cmp-long v0, v5, v7

    if-ltz v0, :cond_5

    iget-wide v5, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->startOfSelectedDay:J

    iget-wide v7, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->endOfMonth:J

    cmp-long v0, v5, v7

    if-gtz v0, :cond_5

    iget-object v0, v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isSelected()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setDayType(I)V

    const-string v0, "CalendarS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "state : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->isSelected()Z

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->checkIfClickAllowed(J)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, p0, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;Ljava/lang/Long;Z)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->dayButtonMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->isFinal:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->checkIfDataExists(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;Ljava/lang/Long;)V

    :cond_6
    const-string v0, "Calendar"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current date final : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4, v9}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "CalendarS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->mDayBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->isSelected()Z

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object p2

    :cond_7
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;

    move-object v1, v0

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setDayType(I)V

    goto/16 :goto_2

    :cond_a
    if-eqz v0, :cond_b

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setDayType(I)V

    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->checkIfClickAllowed(J)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->mCurrentDay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, p0, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;Ljava/lang/Long;Z)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;->setDayType(I)V

    goto :goto_3
.end method

.method public isEnabled(I)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public setDaysInfo(Ljava/util/TreeMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/SortedMap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->uDaysInfo:Ljava/util/TreeMap;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarGridAdapter;->notifyDataSetChanged()V

    return-void
.end method
