.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V
    .locals 0

    .prologue
    .line 963
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getHrmName()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 982
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDevice;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "device_id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 985
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 986
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 987
    const-string v0, "custom_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 991
    if-eqz v6, :cond_0

    .line 993
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 996
    :cond_0
    :goto_0
    return-object v0

    .line 991
    :cond_1
    if-eqz v6, :cond_2

    .line 993
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 996
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMSensorName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 991
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 993
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method


# virtual methods
.method public onChannelStateChanged(Ljava/lang/String;II)V
    .locals 7
    .param p1, "uid"    # Ljava/lang/String;
    .param p2, "state"    # I
    .param p3, "connectionType"    # I

    .prologue
    const/4 v5, 0x1

    .line 1001
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->currentMode:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v4

    if-ne v4, v5, :cond_5

    .line 1002
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateAcessaryText2()V

    .line 1003
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMUID()Ljava/lang/String;

    move-result-object v2

    .line 1005
    .local v2, "hrmuidStr":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1006
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1007
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v4

    invoke-interface {v4, p2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onChannelStateChanged(ILjava/lang/String;)V

    .line 1008
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHRMConnectState:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v4

    if-eq v4, p2, :cond_1

    .line 1009
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHRMConnectState:I
    invoke-static {v4, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1802(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;I)I

    .line 1010
    const/4 v4, 0x2

    if-ne p2, v4, :cond_3

    .line 1013
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isPaused:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1014
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->getHrmName()Ljava/lang/String;

    move-result-object v1

    .line 1015
    .local v1, "hrmName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$1;

    invoke-direct {v5, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1095
    .end local v1    # "hrmName":Ljava/lang/String;
    .end local v2    # "hrmuidStr":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateAcessaryText2()V

    .line 1096
    :cond_2
    return-void

    .line 1024
    .restart local v2    # "hrmuidStr":Ljava/lang/String;
    :cond_3
    if-nez p2, :cond_4

    .line 1025
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1032
    :cond_4
    if-ne p2, v5, :cond_1

    .line 1033
    const/4 v4, 0x5

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0, v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->onUpdateVaule(IFI)V

    .line 1036
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isPaused:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1037
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->getHrmName()Ljava/lang/String;

    move-result-object v1

    .line 1038
    .restart local v1    # "hrmName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$3;

    invoke-direct {v5, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1050
    .end local v1    # "hrmName":Ljava/lang/String;
    .end local v2    # "hrmuidStr":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getFEUID()Ljava/lang/String;

    move-result-object v0

    .line 1051
    .local v0, "feuidStr":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1052
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1053
    .local v3, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1054
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1058
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;

    invoke-direct {v5, p0, p2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;ILjava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method public onHealthServiceError(Ljava/lang/String;II)V
    .locals 0
    .param p1, "uid"    # Ljava/lang/String;
    .param p2, "code"    # I
    .param p3, "connectionType"    # I

    .prologue
    .line 1101
    return-void
.end method

.method public onMaxDurationReached(J)V
    .locals 2
    .param p1, "second"    # J

    .prologue
    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->currentMode:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1152
    :cond_0
    return-void
.end method

.method public onNewLocation(Landroid/location/Location;)V
    .locals 1
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1106
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onNewLocation(Landroid/location/Location;)V

    .line 1108
    :cond_0
    return-void
.end method

.method public onNotiGpsState(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1175
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onNotiGpsState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", onGpsProvider : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onGpsProvider:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1176
    if-nez p1, :cond_0

    .line 1177
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1200
    :goto_0
    return-void

    .line 1189
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onSensorInfoReceived(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->currentMode:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1140
    :cond_0
    :goto_0
    return-void

    .line 1136
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->curActivityType:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1137
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->curActivityType:I
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2702(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;I)I

    goto :goto_0
.end method

.method public onUpdateLapClock(J)V
    .locals 3
    .param p1, "second"    # J

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onUpdateLapClock(J)V

    .line 1116
    :cond_0
    return-void
.end method

.method public onUpdateTEPaceGuide(I)V
    .locals 2
    .param p1, "paceGuide"    # I

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1130
    return-void
.end method

.method public onUpdateVaule(IFI)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "value"    # F
    .param p3, "unit"    # I

    .prologue
    .line 967
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 968
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onUpdateValue  has not key type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 970
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 971
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onUpdateVaule(I)V

    .line 979
    :cond_0
    :goto_0
    return-void

    .line 973
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_0

    .line 974
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->dispData:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 976
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onUpdateVaule(I)V

    goto :goto_0
.end method

.method public onWorkoutSaved(J)V
    .locals 3
    .param p1, "exerciseID"    # J

    .prologue
    const/4 v2, 0x2

    .line 1167
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->currentMode:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1171
    :goto_0
    return-void

    .line 1169
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->startDetailActivity(IJ)V
    invoke-static {v0, v2, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;IJ)V

    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->initCurWorkoutResult(I)V

    goto :goto_0
.end method
