.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->onResponseReceived(FFFIIIIIIIIJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

.field final synthetic val$cal:F

.field final synthetic val$deviceType:I

.field final synthetic val$distance:F

.field final synthetic val$totalStep:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;IFFI)V
    .locals 0

    .prologue
    .line 1333
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$deviceType:I

    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$distance:F

    iput p4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$cal:F

    iput p5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$totalStep:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1337
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$deviceType:I

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1002(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;I)I

    .line 1338
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v0

    const/16 v1, 0x2724

    if-ne v0, v1, :cond_0

    .line 1339
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    const/16 v1, 0x2728

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1002(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;I)I

    .line 1341
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v2, v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1343
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$distance:F

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2302(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;F)F

    .line 1344
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$cal:F

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2402(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;I)I

    .line 1345
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->val$totalStep:I

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$802(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;I)I

    .line 1346
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setSteps(I)V

    .line 1347
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDistance(F)V

    .line 1348
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCalories(F)V

    .line 1349
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateAnimation(I)V

    .line 1351
    :cond_1
    return-void
.end method
