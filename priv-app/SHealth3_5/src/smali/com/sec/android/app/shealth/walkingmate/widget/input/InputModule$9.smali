.class Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 662
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private valueIncOrDec(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 677
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1900(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 678
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMoveDistance:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$2100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V
    invoke-static {v2, v3, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZ)V

    .line 686
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1402(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;I)I

    .line 687
    return-void

    :cond_1
    move v0, v1

    .line 678
    goto :goto_0

    .line 679
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 680
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v2

    const/16 v3, 0x2710

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v2

    const/16 v3, 0x2af8

    if-gt v2, v3, :cond_4

    .line 681
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_2
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V
    invoke-static {v2, v3, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZ)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 683
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMoveDistance:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$2100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z

    move-result v4

    if-eqz v4, :cond_5

    :goto_3
    # invokes: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V
    invoke-static {v2, v3, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZ)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 666
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 668
    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 669
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;->valueIncOrDec(Landroid/view/View;)V

    .line 670
    const/4 v0, 0x1

    .line 673
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
