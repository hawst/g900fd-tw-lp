.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "HeartrateLogViewByTagActivity.java"


# instance fields
.field private mAvgTxt:Landroid/widget/TextView;

.field private mAvgVal:Landroid/widget/TextView;

.field private mDetailsLayout:Landroid/widget/LinearLayout;

.field private mHearterateObserver:Landroid/database/ContentObserver;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

.field private mMaxTxt:Landroid/widget/TextView;

.field private mMaxVal:Landroid/widget/TextView;

.field private mMinTxt:Landroid/widget/TextView;

.field private mMinVal:Landroid/widget/TextView;

.field mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

.field private mtagName:Ljava/lang/String;

.field tag:I

.field tagIdInfo:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 35
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 38
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    .line 40
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHearterateObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->refreshAdapter()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->updateDetailsLayout()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->showDetailScreen(Ljava/lang/Object;)V

    return-void
.end method

.method private updateDetailsLayout()V
    .locals 9

    .prologue
    .line 277
    const/4 v1, 0x0

    .line 279
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget v7, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->executeGroupByTagQuery(I)Landroid/database/Cursor;

    move-result-object v1

    .line 280
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_2

    .line 281
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 282
    const-string v6, "AvgMonth"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 283
    .local v0, "avgMonth":F
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mAvgVal:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " bpm"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    const/4 v6, 0x0

    cmpg-float v6, v0, v6

    if-gtz v6, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->finish()V

    .line 286
    :cond_0
    const-string v6, "MinMonth"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 287
    .local v4, "minMonth":J
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMinVal:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " bpm"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    const-string v6, "MaxMonth"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 289
    .local v2, "maxMonth":J
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMaxVal:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " bpm"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    .end local v0    # "avgMonth":F
    .end local v2    # "maxMonth":J
    .end local v4    # "minMonth":J
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_1

    .line 300
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 303
    :cond_1
    return-void

    .line 293
    :cond_2
    if-eqz v1, :cond_3

    .line 294
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 295
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mDetailsLayout:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 299
    :catchall_0
    move-exception v6

    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_4

    .line 300
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v6
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 228
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 257
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->customizeActionBar()V

    .line 258
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    if-lez v0, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 262
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 263
    return-void

    .line 261
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 5

    .prologue
    .line 153
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 154
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "tag"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "tempTagId":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    .line 158
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    if-eqz v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    .end local v1    # "tempTagId":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->executeTagQuery(I)Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mCursor:Landroid/database/Cursor;

    .line 169
    const-string v2, "cursor Size"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    new-instance v2, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mCursor:Landroid/database/Cursor;

    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    invoke-direct {v2, v3, p0, v4}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    return-object v2

    .line 162
    .restart local v1    # "tempTagId":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "integer parsing error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 209
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 239
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f090f17

    .line 70
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    if-ltz v0, :cond_0

    .line 72
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    if-lez v0, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 76
    :cond_0
    :goto_0
    const v0, 0x7f080651

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mDetailsLayout:Landroid/widget/LinearLayout;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mDetailsLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 78
    const v0, 0x7f080656

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMaxTxt:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f080657

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMaxVal:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f080658

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMinTxt:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f080659

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMinVal:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f080653

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mAvgTxt:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f080654

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mAvgVal:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMaxTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f14

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMinTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f15

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mAvgTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f16

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMinVal:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mMaxVal:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mAvgVal:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->registerObserver()V

    .line 94
    const v0, 0x7f0205b7

    const v1, 0x7f090c10

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->setNoLogImageAndText(II)V

    .line 95
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 107
    return-void

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->unregisterObserver()V

    .line 141
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onDestroy()V

    .line 142
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 271
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 112
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onResume()V

    .line 114
    const-string/jumbo v3, "mHeartrateDatabaseHelper!=null"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-eqz v2, :cond_2

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "tagName"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;

    .line 117
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "tag"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "tempTagId":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    .line 121
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    if-eqz v2, :cond_1

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mTagInfo:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    iget v2, v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    .end local v1    # "tempTagId":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->customizeActionBar()V

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->updateDetailsLayout()V

    .line 135
    :cond_2
    return-void

    .line 114
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 126
    .restart local v1    # "tempTagId":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "integer parsing error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public registerObserver()V
    .locals 4

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHearterateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 62
    return-void
.end method

.method protected setUpMenuDeleteMode()V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method protected setUpSelectMode()V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 182
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 183
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 184
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "HEART_RATE_ID_KEY"

    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    const/16 v1, 0x45b

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 187
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public unregisterObserver()V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHearterateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 66
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 222
    return-void
.end method

.method protected updateSelectedCount(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    if-lez v0, :cond_0

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 203
    :cond_0
    return-void
.end method
