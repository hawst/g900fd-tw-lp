.class public Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateInformationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;
    }
.end annotation


# static fields
.field private static final GUIDE_CHECKED:Ljava/lang/String; = "guides_"

.field private static final GUIDE_COUNT:Ljava/lang/String; = "guides_count"


# instance fields
.field private mCheckBoxList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mGuideList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mScrollView:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mCheckBoxList:Ljava/util/List;

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->refreshFocusables()V

    return-void
.end method

.method private checkGuideWithoutAnimation(I)V
    .locals 3
    .param p1, "guideIndex"    # I

    .prologue
    const v2, 0x7f080632

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 76
    return-void
.end method

.method private initializeGuideList()V
    .locals 12

    .prologue
    const/16 v8, 0x64

    const/16 v11, 0xa

    const/4 v7, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 79
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 80
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 81
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030298

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 82
    .local v3, "viewHealthySteps":Landroid/view/View;
    const v5, 0x7f080bb0

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 83
    .local v2, "mIcon":Landroid/widget/ImageView;
    const v5, 0x7f0205bf

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 84
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 87
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v6, 0x7f0907d8

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "HealthyStepTxt":Ljava/lang/String;
    :goto_0
    const v5, 0x7f08004d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0907e4

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    const v5, 0x7f0807c3

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 95
    const v5, 0x7f030298

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 96
    .local v4, "viewInactiveTime":Landroid/view/View;
    const v5, 0x7f080bb0

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "mIcon":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 97
    .restart local v2    # "mIcon":Landroid/widget/ImageView;
    const v5, 0x7f0205c0

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 98
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    const v5, 0x7f08004d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0907e3

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    const v5, 0x7f0807c3

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0907c0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 102
    return-void

    .line 89
    .end local v0    # "HealthyStepTxt":Ljava/lang/String;
    .end local v4    # "viewInactiveTime":Landroid/view/View;
    :cond_0
    const v5, 0x7f0907d8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "HealthyStepTxt":Ljava/lang/String;
    goto/16 :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0900e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 120
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v3, 0x7f030297

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->setContentView(I)V

    .line 52
    const v3, 0x7f0804e2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 53
    const v3, 0x7f0804e7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->initializeGuideList()V

    .line 55
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 56
    .local v0, "animatedViewContainer":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mCheckBoxList:Ljava/util/List;

    const v3, 0x7f08004d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    .end local v0    # "animatedViewContainer":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 58
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v5, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v4, v1, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;Landroid/view/View;IZ)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    :cond_1
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 63
    const-string v2, "guides_count"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 64
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "guides_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->checkGuideWithoutAnimation(I)V

    .line 64
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 106
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 107
    const-string v1, "guides_count"

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "guides_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_0
    return-void
.end method
