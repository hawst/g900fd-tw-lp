.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;
.super Ljava/lang/Object;
.source "HeartRateScoverFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->resetPulseView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 0

    .prologue
    .line 1314
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1318
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "playSound  resetPulseView"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1319
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1320
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1322
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1323
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1325
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewMask:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1326
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewMask:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1328
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1329
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1331
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1332
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewGreenIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1334
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1335
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$22;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterPulseViewOrangeIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1337
    :cond_5
    return-void
.end method
