.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$ComfortZoneView;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComfortZoneView"
.end annotation


# static fields
.field public static final KEY_AVG_HUM:Ljava/lang/String; = "avgHum"

.field public static final KEY_AVG_TEMP:Ljava/lang/String; = "avgTemp"

.field public static final KEY_COUNT:Ljava/lang/String; = "count"

.field public static final KEY_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_MAX_HUM:Ljava/lang/String; = "maxHum"

.field public static final KEY_MAX_TEMP:Ljava/lang/String; = "maxTemp"

.field public static final KEY_MIN_HUM:Ljava/lang/String; = "minHum"

.field public static final KEY_MIN_TEMP:Ljava/lang/String; = "minTemp"

.field public static final VIEW_NAME:Ljava/lang/String; = "comfort_zone_view"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
