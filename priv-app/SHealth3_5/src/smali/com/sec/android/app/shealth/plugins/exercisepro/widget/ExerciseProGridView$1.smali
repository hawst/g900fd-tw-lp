.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;
.super Ljava/lang/Object;
.source "ExerciseProGridView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->getPhotoGridView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

.field final synthetic val$exercise:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;->val$exercise:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->bitmapLoader:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;->val$exercise:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->deleteCache(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->onGridItemListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->onGridItemListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;->val$exercise:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;->onDeleteButtonClick(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V

    .line 147
    :cond_0
    return-void
.end method
