.class public Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;
.super Ljava/lang/Object;
.source "DateFormatUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    return-void
.end method

.method private static getDateFormat(Landroid/content/Context;JLjava/lang/String;ZZLcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMills"    # J
    .param p3, "targetSeperate"    # Ljava/lang/String;
    .param p4, "isYear2Str"    # Z
    .param p5, "isMonth3Str"    # Z
    .param p6, "excludeType"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    .prologue
    .line 36
    const/4 v2, 0x0

    .line 37
    .local v2, "sysDateFormat":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 38
    :cond_0
    const-string/jumbo v2, "yyyy-MM-dd"

    .line 43
    :goto_0
    invoke-static {v2, p6}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getExcludeDateformat(Ljava/lang/String;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;)Ljava/lang/String;

    move-result-object v2

    .line 46
    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {v2, p3, p4, p5}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->initDateType(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 47
    .local v0, "dateFormat":Ljava/text/DateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 50
    :goto_1
    return-object v3

    .line 40
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "date_format"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 48
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    .line 50
    .restart local v0    # "dateFormat":Ljava/text/DateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public static getDayResource(I)I
    .locals 1
    .param p0, "dayOfWeek"    # I

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "dayResource":I
    packed-switch p0, :pswitch_data_0

    .line 219
    const v0, 0x7f0900f3

    .line 222
    :goto_0
    return v0

    .line 201
    :pswitch_0
    const v0, 0x7f0900f4

    .line 202
    goto :goto_0

    .line 204
    :pswitch_1
    const v0, 0x7f0900ee

    .line 205
    goto :goto_0

    .line 207
    :pswitch_2
    const v0, 0x7f0900ef

    .line 208
    goto :goto_0

    .line 210
    :pswitch_3
    const v0, 0x7f0900f0

    .line 211
    goto :goto_0

    .line 213
    :pswitch_4
    const v0, 0x7f0900f1

    .line 214
    goto :goto_0

    .line 216
    :pswitch_5
    const v0, 0x7f0900f2

    .line 217
    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getDayShortResource(I)I
    .locals 1
    .param p0, "dayOfWeek"    # I

    .prologue
    .line 227
    const/4 v0, 0x0

    .line 228
    .local v0, "dayResource":I
    packed-switch p0, :pswitch_data_0

    .line 248
    const v0, 0x7f0900fa

    .line 251
    :goto_0
    return v0

    .line 230
    :pswitch_0
    const v0, 0x7f0900fb

    .line 231
    goto :goto_0

    .line 233
    :pswitch_1
    const v0, 0x7f0900f5

    .line 234
    goto :goto_0

    .line 236
    :pswitch_2
    const v0, 0x7f0900f6

    .line 237
    goto :goto_0

    .line 239
    :pswitch_3
    const v0, 0x7f0900f7

    .line 240
    goto :goto_0

    .line 242
    :pswitch_4
    const v0, 0x7f0900f8

    .line 243
    goto :goto_0

    .line 245
    :pswitch_5
    const v0, 0x7f0900f9

    .line 246
    goto :goto_0

    .line 228
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getDayShortStr(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMillis"    # J

    .prologue
    .line 185
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 187
    .local v0, "locale":Ljava/util/Locale;
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(J)Ljava/util/GregorianCalendar;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getDayShortResource(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getDayStr(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMillis"    # J

    .prologue
    .line 191
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 193
    .local v0, "locale":Ljava/util/Locale;
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(J)Ljava/util/GregorianCalendar;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getDayResource(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getDefaultDateText(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 307
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 308
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 311
    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 313
    .local v1, "dayOfMonth":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    .line 314
    .local v4, "language":Ljava/lang/String;
    const-string v2, ""

    .line 315
    .local v2, "koreanDay":Ljava/lang/String;
    const-string v3, ""

    .line 317
    .local v3, "koreanYear":Ljava/lang/String;
    const-string v5, "ko"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "zh"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "ja"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 318
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090064

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 319
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090066

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 321
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "yyyy"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " MMM "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 333
    :goto_0
    return-object v5

    .line 324
    :cond_1
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$common$utils$calendar$DateFormatType:[I

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getCurrentSystemDateFormatType()Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/calendar/DateFormatType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 333
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "yyyy"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " MMM "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 326
    :pswitch_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " MMM yyyy"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 328
    :pswitch_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MMM "

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-static {v6, v7, v8}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " yyyy"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 330
    :pswitch_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "yyyy"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " MMM "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getExcludeDateformat(Ljava/lang/String;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;)Ljava/lang/String;
    .locals 7
    .param p0, "dateFormat"    # Ljava/lang/String;
    .param p1, "excludeType"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    .prologue
    const/16 v6, 0x2d

    const/4 v5, 0x0

    .line 73
    move-object v2, p0

    .line 75
    .local v2, "result":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 77
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$DateformatType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 91
    :goto_0
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 92
    .local v0, "first":C
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 93
    .local v1, "last":C
    if-ne v0, v6, :cond_0

    .line 94
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 96
    :cond_0
    if-ne v1, v6, :cond_1

    .line 97
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 99
    :cond_1
    const-string v3, "--"

    const-string v4, "-"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 102
    .end local v0    # "first":C
    .end local v1    # "last":C
    :cond_2
    return-object v2

    .line 79
    :pswitch_0
    const-string/jumbo v3, "yyyy"

    const-string v4, ""

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 80
    goto :goto_0

    .line 82
    :pswitch_1
    const-string v3, "MM"

    const-string v4, ""

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 83
    goto :goto_0

    .line 85
    :pswitch_2
    const-string v3, "dd"

    const-string v4, ""

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 86
    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getExcludeDayDateFormat(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMills"    # J

    .prologue
    const/4 v4, 0x0

    .line 181
    const/4 v3, 0x0

    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;->DAY:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    move-object v0, p0

    move-wide v1, p1

    move v5, v4

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getDateFormat(Landroid/content/Context;JLjava/lang/String;ZZLcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getExcludeYearDateFormat(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMills"    # J

    .prologue
    const/4 v4, 0x0

    .line 170
    const/4 v3, 0x0

    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;->YEAR:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;

    move-object v0, p0

    move-wide v1, p1

    move v5, v4

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getDateFormat(Landroid/content/Context;JLjava/lang/String;ZZLcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMills"    # J

    .prologue
    .line 112
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getDefaultDateText(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getMonthResource(J)I
    .locals 3
    .param p0, "timeMills"    # J

    .prologue
    .line 255
    const/4 v1, 0x0

    .line 256
    .local v1, "monthResource":I
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(Ljava/util/TimeZone;)Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 257
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 258
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 293
    const v1, 0x7f090113

    .line 296
    :goto_0
    return v1

    .line 260
    :pswitch_0
    const v1, 0x7f090108

    .line 261
    goto :goto_0

    .line 263
    :pswitch_1
    const v1, 0x7f090109

    .line 264
    goto :goto_0

    .line 266
    :pswitch_2
    const v1, 0x7f09010a

    .line 267
    goto :goto_0

    .line 269
    :pswitch_3
    const v1, 0x7f09010b

    .line 270
    goto :goto_0

    .line 272
    :pswitch_4
    const v1, 0x7f09010c

    .line 273
    goto :goto_0

    .line 275
    :pswitch_5
    const v1, 0x7f09010d

    .line 276
    goto :goto_0

    .line 278
    :pswitch_6
    const v1, 0x7f09010e

    .line 279
    goto :goto_0

    .line 281
    :pswitch_7
    const v1, 0x7f09010f

    .line 282
    goto :goto_0

    .line 284
    :pswitch_8
    const v1, 0x7f090110

    .line 285
    goto :goto_0

    .line 287
    :pswitch_9
    const v1, 0x7f090111

    .line 288
    goto :goto_0

    .line 290
    :pswitch_a
    const v1, 0x7f090112

    .line 291
    goto :goto_0

    .line 258
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static getOnlyYear(Landroid/content/Context;J)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMills"    # J

    .prologue
    .line 127
    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 128
    .local v0, "dateFormat":Ljava/text/DateFormat;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 131
    :goto_0
    return-object v2

    .line 129
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    .line 131
    .restart local v0    # "dateFormat":Ljava/text/DateFormat;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeMills"    # J

    .prologue
    .line 158
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 159
    .local v0, "timeFormat":Ljava/text/DateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static initDateType(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 3
    .param p0, "dateType"    # Ljava/lang/String;
    .param p1, "targetSeperate"    # Ljava/lang/String;
    .param p2, "isYear2Str"    # Z
    .param p3, "isMonth3Str"    # Z

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "dateFormat":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 138
    const-string p1, "/"

    .line 140
    :cond_0
    const-string v1, "-"

    invoke-virtual {p0, v1, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 142
    if-eqz p2, :cond_1

    .line 143
    const-string/jumbo v1, "yyyy"

    const-string/jumbo v2, "yy"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 146
    :cond_1
    if-eqz p3, :cond_2

    .line 147
    const-string v1, "MM"

    const-string v2, "MMM"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 150
    :cond_2
    return-object v0
.end method
