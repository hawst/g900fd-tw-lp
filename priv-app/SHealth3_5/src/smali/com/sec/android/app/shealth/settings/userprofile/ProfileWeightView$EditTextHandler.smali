.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;
.super Ljava/lang/Object;
.source "ProfileWeightView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextHandler"
.end annotation


# instance fields
.field pos:I

.field preData:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeFocus:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$1100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeValue:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$1202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    .line 427
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 431
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->pos:I

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->preData:Ljava/lang/String;

    .line 433
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 9
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 380
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 381
    .local v2, "str":Ljava/lang/String;
    const-string v4, "\\."

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, "array":[Ljava/lang/String;
    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 383
    aget-object v4, v0, v7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_0

    .line 384
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v0, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v7

    invoke-virtual {v6, v8, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 386
    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->pos:I

    if-lt v4, v7, :cond_0

    .line 389
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->pos:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getValue()F

    move-result v4

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    .line 400
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getValue()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 401
    .local v3, "value":Ljava/lang/Float;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mWeightUnit:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)I

    move-result v4

    const v5, 0x1fbd1

    if-ne v4, v5, :cond_2

    .line 403
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x43fa0000    # 500.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 405
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setMinValueText()V

    .line 406
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 420
    .end local v3    # "value":Ljava/lang/Float;
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->preData:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->pos:I

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDatalimit(Ljava/lang/String;I)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Ljava/lang/String;I)V

    .line 421
    return-void

    .line 390
    :catch_0
    move-exception v1

    .line 391
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 392
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 411
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v3    # "value":Ljava/lang/Float;
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const v5, 0x4489c99a    # 1102.3f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 413
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setMinValueText()V

    .line 414
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_1
.end method
