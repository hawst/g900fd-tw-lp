.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;
.super Ljava/lang/Object;
.source "AMapExerciseProMapActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

.field final synthetic val$typesMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1300
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->val$typesMap:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V
    .locals 6
    .param p1, "itemIndex"    # I
    .param p2, "itemContent"    # Ljava/lang/String;
    .param p3, "popupWindow"    # Landroid/widget/PopupWindow;

    .prologue
    .line 1303
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->dismiss()V

    .line 1304
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setCurrentItem(I)V

    .line 1305
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->val$typesMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1306
    .local v2, "type":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->curDataType:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 1318
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mTvClock:Landroid/widget/TextView;
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3802(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1322
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V

    .line 1323
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->updateDataText()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1900(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)V

    .line 1324
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3900()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "type chosen: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1325
    return-void

    .line 1308
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1310
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->viewIdx:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->access$3700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1311
    .local v1, "orgType":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1312
    .local v0, "chgIdx":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1313
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->viewIdx:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->access$3700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;)I

    move-result v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1315
    .end local v0    # "chgIdx":I
    .end local v1    # "orgType":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->viewIdx:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;->access$3700(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$DropDownClickListener;)I

    move-result v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method
