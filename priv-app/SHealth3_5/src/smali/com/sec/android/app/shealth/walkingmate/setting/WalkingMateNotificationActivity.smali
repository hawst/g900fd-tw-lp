.class public Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateNotificationActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final NOTIFICATION_FRAGMENT:Ljava/lang/String; = "notification_fragment"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mNotificationConfig:Z

.field private mSwitch:Landroid/widget/Switch;

.field mSwitchView:Landroid/view/View;

.field private mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 18
    const-string v0, "WalkingMateNotificationActivity"

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->TAG:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mNotificationConfig:Z

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907be

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030004

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitchView:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitchView:Landroid/view/View;

    const v1, 0x7f080025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitchView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addCustomView(Landroid/view/View;)V

    .line 84
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 105
    .local v0, "currentView":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0x16

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    .line 107
    const v3, 0x7f080bed

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 108
    .local v1, "resView":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    if-ne v3, v0, :cond_2

    if-eqz v1, :cond_2

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 121
    .end local v1    # "resView":Landroid/view/View;
    :cond_0
    :goto_0
    return v2

    .line 115
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0x15

    if-ne v3, v4, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f080304

    if-eq v3, v4, :cond_0

    .line 121
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "compoundButton"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 88
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getNotificationONOFF()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v1

    if-eq v1, p2, :cond_0

    .line 90
    invoke-static {p2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setPedometerNotification(Z)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->setEnable(Z)V

    .line 94
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;-><init>()V

    .line 95
    .local v0, "worker":Lcom/sec/android/app/shealth/common/utils/FocusWorker;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->refreshFocusables(Landroid/view/View;)V

    .line 97
    .end local v0    # "worker":Lcom/sec/android/app/shealth/common/utils/FocusWorker;
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v1, 0x7f0302a6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->setContentView(I)V

    .line 31
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 34
    .local v0, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f080ba7

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    const-string/jumbo v3, "notification_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 35
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 36
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mNotificationConfig:Z

    .line 38
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 42
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mNotificationConfig:Z

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mNotificationConfig:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mNotificationConfig:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->setEnable(Z)V

    .line 59
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getNotificationONOFF()Z

    move-result v0

    if-nez v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->setEnable(Z)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 63
    const-string v0, "NOTIBOX"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disable function, noti status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationActivity;->mWalkingMateNotificationFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->updateInactiveTimeConfiguration()V

    .line 70
    :goto_1
    return-void

    .line 49
    :cond_0
    const-string v0, "WalkingMateNotificationActivity"

    const-string v1, "There is no switch instance."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 55
    :cond_1
    const-string v0, "WalkingMateNotificationActivity"

    const-string v1, "There is not notification fragment instance."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 65
    :cond_2
    const-string v0, "NOTIBOX"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enable function, noti status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
