.class public Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;
.super Landroid/widget/FrameLayout;
.source "LoadingView.java"


# instance fields
.field private failView:Landroid/view/View;

.field private loaderView:Landroid/view/View;

.field private receivingView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->init()V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->init()V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->init()V

    .line 40
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03029b

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    const v0, 0x7f08064b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->receivingView:Landroid/view/View;

    .line 45
    const v0, 0x7f080bc0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->failView:Landroid/view/View;

    .line 46
    const v0, 0x7f080bbf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->loaderView:Landroid/view/View;

    .line 47
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->setVisibility(I)V

    .line 48
    return-void
.end method

.method private setFailViewVisible(Z)V
    .locals 3
    .param p1, "isFail"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 81
    if-eqz p1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->failView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->loaderView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->failView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->loaderView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public fail()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->receivingView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 77
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->setFailViewVisible(Z)V

    .line 78
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->receivingView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 69
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->setVisibility(I)V

    .line 70
    return-void
.end method

.method public show()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 54
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->setFailViewVisible(Z)V

    .line 55
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->setVisibility(I)V

    .line 56
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 57
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 58
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 59
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 60
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/tools/LoadingView;->receivingView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 62
    return-void
.end method
