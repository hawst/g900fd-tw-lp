.class public Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;
.super Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.source "HorizontalGradationView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method


# virtual methods
.method protected drawGradationLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;III)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "position"    # I
    .param p4, "start"    # I
    .param p5, "length"    # I

    .prologue
    .line 88
    int-to-float v1, p3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    sub-int/2addr v0, p4

    int-to-float v2, v0

    int-to-float v3, p3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    sub-int/2addr v0, p4

    sub-int/2addr v0, p5

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 89
    return-void
.end method

.method protected drawGradationText(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/lang/String;II)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "position"    # I
    .param p5, "heightOffset"    # I

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v1, p5

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getTextHalfHeight(Landroid/graphics/Paint;)F

    move-result v2

    sub-float v0, v1, v2

    .line 63
    .local v0, "textY":F
    int-to-float v1, p4

    invoke-virtual {p1, p3, v1, v0, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 64
    return-void
.end method

.method protected drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "from"    # I
    .param p3, "to"    # I
    .param p4, "rangePaint"    # Landroid/graphics/Paint;

    .prologue
    .line 68
    int-to-float v1, p2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getBottomLineHeight()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v2, v0

    int-to-float v3, p3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getMeasuredHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 69
    return-void
.end method

.method protected getBottomLineHeight()I
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_horizontal_bottomline_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected getDistanceBetweenGradations()I
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_horizontal_between_gradationbars_distance:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected getLength()I
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/HorizontalGradationView;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method protected getOrientationTouchCoord(Landroid/view/MotionEvent;)F
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    return v0
.end method
