.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;
.super Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMoreFoodInfoDataTaskForSearch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final nextRequest:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)V
    .locals 2
    .param p2, "nextRequest"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .line 1375
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;
    invoke-static {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$3300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;-><init>(Landroid/content/Context;Landroid/widget/ListView;)V

    .line 1376
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->nextRequest:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    .line 1377
    return-void
.end method


# virtual methods
.method protected doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1381
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->nextRequest:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performFoodSearch(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    return-object v0
.end method

.method protected finish()V
    .locals 2

    .prologue
    .line 1386
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->finish()V

    .line 1387
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mLoadMoreTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2702(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .line 1388
    return-void
.end method

.method protected onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1394
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1398
    :goto_0
    return-void

    .line 1397
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addSearchResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$3400(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V

    goto :goto_0
.end method

.method protected bridge synthetic onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 1370
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;->onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V

    return-void
.end method
