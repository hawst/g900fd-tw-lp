.class Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;
.super Landroid/os/AsyncTask;
.source "FoodLogListConcreteDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SkippedCalculatorTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field mCalorieValue:Ljava/lang/String;

.field mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

.field mMealId:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Ljava/lang/String;J)V
    .locals 0
    .param p2, "row"    # Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    .param p3, "calorieValue"    # Ljava/lang/String;
    .param p4, "mealId"    # J

    .prologue
    .line 552
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 553
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    .line 554
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mCalorieValue:Ljava/lang/String;

    .line 555
    iput-wide p4, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mMealId:J

    .line 556
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 561
    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mCalorieValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mMealId:J

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    # getter for: Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mFoodDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->access$300(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->this$0:Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;

    # getter for: Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;->access$400(Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate;)Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isMealSkipped(JLcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 567
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 544
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const v2, 0x7f0909c8

    const v3, 0x7f0900b9

    .line 575
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getCenterTextView()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 593
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 595
    return-void

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mCalorieValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mCalorieValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getCenterTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getCenterTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->mLogListChildRow:Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getCenterTextView()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 544
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/log/FoodLogListConcreteDelegate$SkippedCalculatorTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
