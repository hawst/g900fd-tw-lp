.class Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$4;
.super Ljava/lang/Object;
.source "SearchEditText.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mSearchIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$400(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 159
    if-nez p2, :cond_0

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->handleFocus()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$500(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V

    .line 165
    return-void
.end method
