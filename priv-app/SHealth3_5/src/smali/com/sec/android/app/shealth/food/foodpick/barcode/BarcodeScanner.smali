.class public Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.super Landroid/view/SurfaceView;
.source "BarcodeScanner.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingUncaughtExceptionHandler;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingThreadFactory;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$LoggingRejectedExecutionHandler;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String;

.field public static final TIME_OUT_SEC:I = 0xa


# instance fields
.field private barcodeExecutor:Ljava/util/concurrent/ExecutorService;

.field private camera:Landroid/hardware/Camera;

.field private cameraExecutor:Ljava/util/concurrent/ExecutorService;

.field private cameraTasksQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private focusCallback:Landroid/hardware/Camera$AutoFocusCallback;

.field private focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

.field private final focusStateLock:Ljava/lang/Object;

.field private holder:Landroid/view/SurfaceHolder;

.field private isCameraInited:Z

.field private resultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

.field private volatile surfaceCreated:Z

.field private timeOutThread:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusStateLock:Ljava/lang/Object;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraInited:Z

    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->init()V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusStateLock:Ljava/lang/Object;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraInited:Z

    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->init()V

    .line 121
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/view/SurfaceHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->holder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusStateLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;II)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getRatio(II)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;DLandroid/hardware/Camera$Parameters;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p1, "x1"    # D
    .param p3, "x2"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->findClosestPreviewSize(DLandroid/hardware/Camera$Parameters;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getDisplayRotation()I

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->barcodeExecutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->autoFocus()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->resultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p1, "x1"    # [B

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->takeBarcode([B)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera$AutoFocusCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Landroid/hardware/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p1, "x1"    # Landroid/hardware/Camera;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->surfaceCreated:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->stopPreviewAndReleaseCamera()V

    return-void
.end method

.method private autoFocus()V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string v1, "auto focus called"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$AutoFocusRunnable;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 240
    :cond_0
    return-void
.end method

.method private findClosestPreviewSize(DLandroid/hardware/Camera$Parameters;)V
    .locals 12
    .param p1, "ratio"    # D
    .param p3, "parameters"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 264
    invoke-virtual {p3}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v7

    .line 265
    .local v7, "supportedPreviewSizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-nez v7, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    const/4 v2, 0x0

    .line 269
    .local v2, "closestSize":Landroid/hardware/Camera$Size;
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 270
    .local v0, "closestRatio":D
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/Camera$Size;

    .line 271
    .local v6, "previewSize":Landroid/hardware/Camera$Size;
    if-nez v2, :cond_3

    .line 272
    if-eqz v6, :cond_2

    .line 273
    move-object v2, v6

    .line 274
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getRatio(Landroid/hardware/Camera$Size;)D

    move-result-wide v0

    goto :goto_1

    .line 277
    :cond_3
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getRatio(Landroid/hardware/Camera$Size;)D

    move-result-wide v3

    .line 278
    .local v3, "currentRatio":D
    invoke-direct {p0, p1, p2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->module(DD)D

    move-result-wide v8

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->module(DD)D

    move-result-wide v10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_2

    .line 279
    move-object v2, v6

    .line 280
    move-wide v0, v3

    goto :goto_1

    .line 284
    .end local v3    # "currentRatio":D
    .end local v6    # "previewSize":Landroid/hardware/Camera$Size;
    :cond_4
    if-eqz v2, :cond_0

    .line 285
    iget v8, v2, Landroid/hardware/Camera$Size;->width:I

    iget v9, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p3, v8, v9}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto :goto_0
.end method

.method private getDisplayRotation()I
    .locals 2

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method private getRatio(II)D
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 296
    int-to-double v0, p1

    int-to-double v2, p2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private getRatio(Landroid/hardware/Camera$Size;)D
    .locals 2
    .param p1, "size"    # Landroid/hardware/Camera$Size;

    .prologue
    .line 307
    iget v0, p1, Landroid/hardware/Camera$Size;->width:I

    iget v1, p1, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getRatio(II)D

    move-result-wide v0

    return-wide v0
.end method

.method private init()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 127
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string v2, "init"

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraTasksQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 129
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraTasksQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v7, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingThreadFactory;

    invoke-direct {v7, v9}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingThreadFactory;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    new-instance v8, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$LoggingRejectedExecutionHandler;

    invoke-direct {v8, v9}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$LoggingRejectedExecutionHandler;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    move v2, v1

    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraExecutor:Ljava/util/concurrent/ExecutorService;

    .line 134
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->barcodeExecutor:Ljava/util/concurrent/ExecutorService;

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->holder:Landroid/view/SurfaceHolder;

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->holder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 137
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;

    invoke-direct {v0, p0, v9}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusCallback;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    .line 138
    return-void
.end method

.method private module(DD)D
    .locals 2
    .param p1, "d1"    # D
    .param p3, "d2"    # D

    .prologue
    .line 314
    sub-double v0, p1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private stopPreviewAndReleaseCamera()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stopPreviewAndReleaseCamera "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraTasksQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 252
    iput-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    .line 253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraInited:Z

    .line 255
    :cond_0
    return-void
.end method

.method private takeBarcode([B)V
    .locals 11
    .param p1, "data"    # [B

    .prologue
    .line 327
    invoke-static {}, Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;->getInstance()Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;

    move-result-object v0

    .line 329
    .local v0, "barcodeLib":Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getDisplayRotation()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 335
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getHeight()I

    move-result v10

    .line 337
    .local v10, "roiHeight":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v8

    .line 339
    .local v8, "params":Landroid/hardware/Camera$Parameters;
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string v2, "decode barcode"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "complete data: getHeight(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; getWidth(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; roiHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    iget v2, v1, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    iget v3, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    iget v4, v1, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    iget v5, v1, Landroid/hardware/Camera$Size;->height:I

    const/4 v6, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;->decodeBarcode([BIIIII)I

    move-result v9

    .line 346
    .local v9, "resultCode":I
    if-lez v9, :cond_0

    const/16 v1, 0x101

    if-ne v9, v1, :cond_1

    .line 347
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string v2, "barcode was not detected"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :goto_1
    return-void

    .line 332
    .end local v8    # "params":Landroid/hardware/Camera$Parameters;
    .end local v9    # "resultCode":I
    .end local v10    # "roiHeight":I
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->getHeight()I

    move-result v1

    div-int/lit8 v10, v1, 0x3

    .line 333
    .restart local v10    # "roiHeight":I
    goto :goto_0

    .line 349
    .restart local v8    # "params":Landroid/hardware/Camera$Parameters;
    .restart local v9    # "resultCode":I
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;->getBarcodeResult()Ljava/lang/String;

    move-result-object v7

    .line 350
    .local v7, "contents":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->resultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

    invoke-static {v9}, Lcom/sec/android/app/shealth/core/barcode/BarcodeUtil;->getBarcodeType(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v7, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;->onBarcodeTaken(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "barcode was detected, result code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; contents: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public initCamera()V
    .locals 3

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraAlreadyInited()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraInited:Z

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$CameraInitializer;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 179
    return-void

    .line 177
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "initCamera must invoke only one time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isCameraAlreadyInited()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraInited:Z

    return v0
.end method

.method public releaseCamera()V
    .locals 1

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->stopPreviewAndReleaseCamera()V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->timeOutThread:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->timeOutThread:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;->interrupt()V

    .line 200
    :cond_0
    return-void
.end method

.method public setResultListener(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;)V
    .locals 0
    .param p1, "resultListener"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->resultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

    .line 190
    return-void
.end method

.method public startPreview(Z)V
    .locals 4
    .param p1, "isBarcodeScanningEnable"    # Z

    .prologue
    const/4 v3, 0x0

    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "startPreview"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->resultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

    if-nez v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "result listener should be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    if-eqz p1, :cond_1

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;ZLcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 162
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->timeOutThread:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->timeOutThread:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;->start()V

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$StartPreviewRunnable;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;ZLcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public stopFocusing()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->timeOutThread:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->timeOutThread:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$TimeOutThread;->interrupt()V

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraAlreadyInited()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraTasksQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->camera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 213
    :cond_1
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;
    .param p2, "i"    # I
    .param p3, "i2"    # I
    .param p4, "i3"    # I

    .prologue
    .line 223
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 217
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "surface created"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->surfaceCreated:Z

    .line 219
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "surface destroyed"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->surfaceCreated:Z

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->cameraTasksQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 230
    return-void
.end method
