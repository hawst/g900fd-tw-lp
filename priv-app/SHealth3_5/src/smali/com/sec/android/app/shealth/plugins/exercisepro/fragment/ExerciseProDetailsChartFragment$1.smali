.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;
.super Ljava/lang/Object;
.source "ExerciseProDetailsChartFragment.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)V
    .locals 0

    .prologue
    .line 810
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 841
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f080528

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 842
    .local v2, "first_text":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->getPaceMode()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 843
    const v7, 0x7f090a4d

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 848
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    if-eqz v7, :cond_1

    .line 849
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f080529

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 850
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0807da

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 853
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidElevChart:Z

    if-eqz v7, :cond_2

    .line 854
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0807dc

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 855
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0807db

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 857
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v7

    if-nez v7, :cond_3

    .line 858
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    .line 860
    .local v3, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 861
    const/high16 v7, 0x41f80000    # 31.0f

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->density:F

    mul-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v7, v8

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 863
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0057

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x3f000000    # 0.5f

    mul-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0058

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 867
    const/high16 v7, 0x41a00000    # 20.0f

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 868
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 870
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 872
    .end local v3    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_3
    if-eqz p1, :cond_12

    .line 874
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v8, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    long-to-double v9, v9

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v7

    invoke-virtual {v8, v9, v10, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 875
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f080528

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 876
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mLegendView:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0807d9

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 877
    const/4 v6, 0x1

    .line 878
    .local v6, "zeroElev":Z
    const/4 v0, 0x0

    .local v0, "chartIndex":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v0, v7, :cond_e

    .line 879
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .line 882
    .local v1, "dataInChart":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesId()I

    move-result v7

    if-nez v7, :cond_8

    .line 884
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProGraphFragment;->getPaceMode()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 885
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmpg-double v7, v7, v9

    if-gez v7, :cond_6

    .line 886
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const-string v8, "0\'00\""

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mContext:Landroid/app/Activity;
    invoke-static {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090a4d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 845
    .end local v0    # "chartIndex":I
    .end local v1    # "dataInChart":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .end local v6    # "zeroElev":Z
    :cond_5
    const v7, 0x7f090a49

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 888
    .restart local v0    # "chartIndex":I
    .restart local v1    # "dataInChart":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .restart local v6    # "zeroElev":Z
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm2:Ljava/text/DecimalFormat;
    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceValue(D)Ljava/lang/String;
    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;D)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getPaceBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mContext:Landroid/app/Activity;
    invoke-static {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090a4d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 895
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm:Ljava/text/DecimalFormat;
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v8

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getSpeedBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mContext:Landroid/app/Activity;
    invoke-static {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090a49

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 901
    :cond_8
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesId()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_b

    .line 902
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    if-nez v7, :cond_9

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    if-eqz v7, :cond_a

    .line 904
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm:Ljava/text/DecimalFormat;
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v8

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getBPMBubbleAreaUnit()Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 907
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    if-eqz v7, :cond_4

    goto/16 :goto_2

    .line 911
    :cond_b
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesId()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_4

    .line 912
    const/4 v6, 0x0

    .line 913
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 914
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v8, "km"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    const/4 v4, 0x1

    .line 915
    .local v4, "unitMile":Z
    :goto_3
    if-eqz v4, :cond_d

    .line 916
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm3:Ljava/text/DecimalFormat;
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v8

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "ft"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 914
    .end local v4    # "unitMile":Z
    :cond_c
    const/4 v4, 0x0

    goto :goto_3

    .line 919
    .restart local v4    # "unitMile":Z
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->oneDForm3:Ljava/text/DecimalFormat;
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Ljava/text/DecimalFormat;

    move-result-object v8

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "m"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 925
    .end local v1    # "dataInChart":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .end local v4    # "unitMile":Z
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v8, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    long-to-double v9, v9

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v7

    invoke-virtual {v8, v9, v10, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 926
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->refreshInformationAreaView()V

    .line 934
    .end local v0    # "chartIndex":I
    .end local v6    # "zeroElev":Z
    :cond_f
    :goto_4
    const/4 v5, 0x0

    .line 935
    .local v5, "viewCount":I
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    if-eqz v7, :cond_11

    .line 937
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setFirstInformationVisible(Z)V

    .line 940
    add-int/lit8 v5, v5, 0x1

    .line 943
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidExtHrChart:Z

    if-nez v7, :cond_10

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidDeviceHrChart:Z

    if-eqz v7, :cond_13

    .line 944
    :cond_10
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformationVisible(Z)V

    .line 945
    add-int/lit8 v5, v5, 0x1

    .line 951
    :goto_5
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->isValidElevChart:Z

    if-eqz v7, :cond_14

    .line 952
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformationVisible(Z)V

    .line 953
    add-int/lit8 v5, v5, 0x1

    .line 958
    :goto_6
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setInformationWeight(I)V

    .line 962
    :cond_11
    return-void

    .line 928
    .end local v5    # "viewCount":I
    :cond_12
    if-nez p1, :cond_f

    .line 929
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$1100()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Handler Data :NULL"

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->dimInformationAreaView()V

    goto :goto_4

    .line 947
    .restart local v5    # "viewCount":I
    :cond_13
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setSecondInformationVisible(Z)V

    goto :goto_5

    .line 955
    :cond_14
    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-object v7, v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->exerciseproInformationArea:Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->setThirdInformationVisible(Z)V

    goto :goto_6
.end method

.method public OnReleaseTimeOut()V
    .locals 2

    .prologue
    .line 828
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 830
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 833
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 835
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 836
    return-void
.end method

.method public OnVisible()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 813
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 815
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 816
    const/high16 v1, 0x41f80000    # 31.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->density:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 818
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 820
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 821
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 823
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 824
    return-void
.end method
