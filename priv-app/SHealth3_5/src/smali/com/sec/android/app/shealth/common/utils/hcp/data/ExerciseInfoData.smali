.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
.super Ljava/lang/Object;
.source "ExerciseInfoData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private createTime:J

.field private favorite:I

.field private id:J

.field private met:F

.field private name:Ljava/lang/String;

.field private sorting1:Ljava/lang/String;

.field private sorting2:Ljava/lang/String;

.field private sourceType:I

.field private timeZone:I

.field private updateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->id:J

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->name:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting1:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting2:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public constructor <init>(JFLjava/lang/String;IILjava/lang/String;Ljava/lang/String;JJI)V
    .locals 4
    .param p1, "id"    # J
    .param p3, "met"    # F
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "sourceType"    # I
    .param p6, "favorite"    # I
    .param p7, "sorting1"    # Ljava/lang/String;
    .param p8, "sorting2"    # Ljava/lang/String;
    .param p9, "createTime"    # J
    .param p11, "updateTime"    # J
    .param p13, "timeZone"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/16 v1, -0x2

    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->id:J

    .line 14
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->name:Ljava/lang/String;

    .line 17
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting1:Ljava/lang/String;

    .line 18
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting2:Ljava/lang/String;

    .line 25
    const-wide/16 v1, -0x2

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    .line 27
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id should not be "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 29
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->id:J

    .line 30
    iput p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->met:F

    .line 31
    iput-object p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->name:Ljava/lang/String;

    .line 32
    iput p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sourceType:I

    .line 33
    iput p6, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->favorite:I

    .line 34
    iput-object p7, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting1:Ljava/lang/String;

    .line 35
    iput-object p8, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting2:Ljava/lang/String;

    .line 36
    iput-wide p9, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->createTime:J

    .line 37
    iput-wide p11, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->updateTime:J

    .line 38
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->timeZone:I

    .line 39
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->createTime:J

    return-wide v0
.end method

.method public getFavorite()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->favorite:I

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->id:J

    return-wide v0
.end method

.method public getMet()F
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->met:F

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSorting1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting1:Ljava/lang/String;

    return-object v0
.end method

.method public getSorting2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting2:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceType()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sourceType:I

    return v0
.end method

.method public getTimeZone()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->timeZone:I

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->updateTime:J

    return-wide v0
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 122
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->createTime:J

    .line 123
    return-void
.end method

.method public setFavorite(I)V
    .locals 0
    .param p1, "favorite"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->favorite:I

    .line 93
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->id:J

    .line 53
    return-void
.end method

.method public setMet(F)V
    .locals 0
    .param p1, "met"    # F

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->met:F

    .line 63
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->name:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setSorting1(Ljava/lang/String;)V
    .locals 0
    .param p1, "sorting1"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting1:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setSorting2(Ljava/lang/String;)V
    .locals 0
    .param p1, "sorting2"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting2:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setSourceType(I)V
    .locals 0
    .param p1, "sourceType"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sourceType:I

    .line 83
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timeZone"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->timeZone:I

    .line 143
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J

    .prologue
    .line 132
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->updateTime:J

    .line 133
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 149
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->met:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 151
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sourceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 152
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->favorite:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->sorting2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 155
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->createTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 156
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->updateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 157
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->timeZone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 158
    return-void
.end method
