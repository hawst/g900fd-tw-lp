.class public Lcom/sec/android/app/shealth/framework/repository/database/utils/SqlParser;
.super Ljava/lang/Object;
.source "SqlParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static execSqlFile(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 3
    .param p0, "sqlFile"    # Ljava/lang/String;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    const-string v2, "  exec sql file: {}"

    invoke-static {v2, p0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/framework/repository/database/utils/SqlParser;->parseSqlFile(Ljava/lang/String;Landroid/content/res/AssetManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 153
    .local v1, "sqlInstruction":Ljava/lang/String;
    const-string v2, "    sql: {}"

    invoke-static {v2, v1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    .line 157
    .end local v1    # "sqlInstruction":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public static parseSqlFile(Ljava/io/InputStream;)Ljava/util/List;
    .locals 2
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {p0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/SqlParser;->removeComments(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "script":Ljava/lang/String;
    const/16 v1, 0x3b

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/repository/database/utils/SqlParser;->splitSqlScript(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static parseSqlFile(Ljava/lang/String;Landroid/content/res/AssetManager;)Ljava/util/List;
    .locals 3
    .param p0, "sqlFile"    # Ljava/lang/String;
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/res/AssetManager;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v1, 0x0

    .line 37
    .local v1, "sqlIns":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 39
    .local v0, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/repository/database/utils/SqlParser;->parseSqlFile(Ljava/io/InputStream;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 41
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 43
    return-object v1

    .line 41
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v2
.end method

.method private static removeComments(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .local v4, "sql":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 73
    .local v1, "isReader":Ljava/io/InputStreamReader;
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 76
    .local v0, "buffReader":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 77
    .local v3, "multiLineComment":Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .local v2, "line":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 78
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 80
    if-nez v3, :cond_3

    .line 81
    const-string v5, "/*"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 82
    const-string/jumbo v5, "}"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 83
    const-string v3, "/*"

    goto :goto_0

    .line 85
    :cond_1
    const-string/jumbo v5, "{"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 86
    const-string/jumbo v5, "}"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 87
    const-string/jumbo v3, "{"

    goto :goto_0

    .line 89
    :cond_2
    const-string v5, "--"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, ""

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 90
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 104
    .end local v2    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 108
    .end local v0    # "buffReader":Ljava/io/BufferedReader;
    .end local v3    # "multiLineComment":Ljava/lang/String;
    :catchall_1
    move-exception v5

    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    throw v5

    .line 92
    .restart local v0    # "buffReader":Ljava/io/BufferedReader;
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v3    # "multiLineComment":Ljava/lang/String;
    :cond_3
    :try_start_3
    const-string v5, "/*"

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 93
    const-string v5, "*/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 94
    const/4 v3, 0x0

    goto :goto_0

    .line 96
    :cond_4
    const-string/jumbo v5, "{"

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 97
    const-string/jumbo v5, "}"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v5

    if-eqz v5, :cond_0

    .line 98
    const/4 v3, 0x0

    goto :goto_0

    .line 104
    :cond_5
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 108
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    .line 111
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private static splitSqlScript(Ljava/lang/String;C)Ljava/util/List;
    .locals 7
    .param p0, "script"    # Ljava/lang/String;
    .param p1, "delim"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "C)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v4, "statements":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 127
    .local v2, "inLiteral":Z
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 128
    .local v0, "content":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 129
    aget-char v5, v0, v1

    const/16 v6, 0x27

    if-ne v5, v6, :cond_0

    .line 130
    if-nez v2, :cond_2

    const/4 v2, 0x1

    .line 132
    :cond_0
    :goto_1
    aget-char v5, v0, v1

    if-ne v5, p1, :cond_3

    if-nez v2, :cond_3

    .line 133
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 134
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    .end local v3    # "sb":Ljava/lang/StringBuilder;
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .restart local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 138
    :cond_3
    aget-char v5, v0, v1

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 141
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 142
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_5
    return-object v4
.end method
