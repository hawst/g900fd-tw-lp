.class public final Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;
.super Ljava/lang/Object;
.source "SharedPreferencesHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WALKING_STATISTICS"
.end annotation


# static fields
.field public static final BASIC_RESPONSE:Ljava/lang/String; = "basic_response"

.field public static final BUTTON_STATE:Ljava/lang/String; = "BUTTON_STATE"

.field public static final BY_AGE_RESPONSE:Ljava/lang/String; = "by_age_response"

.field private static final CHART_SELECTED_TAB:Ljava/lang/String; = "pedometer_selected_tab"

.field private static final CIGNA_MODE:Ljava/lang/String; = "cigna_mode"

.field private static final CIGNA_TEXT:Ljava/lang/String; = "cigna_text"

.field public static final CURRENT_CONNECT_DEVICE:Ljava/lang/String; = "current_connect_device"

.field public static final DEVICE_VIEW_GUIDE_POPUP_CHECK:Ljava/lang/String; = "device_view_guide_popup_check"

.field public static final EVERYONE_RANKING_UPDATED_TIME:Ljava/lang/String; = "everyone_rankings_updated_time"

.field private static final FIRST_DEVICE_CONNECTION:Ljava/lang/String; = "first_device_"

.field private static final FIRST_START:Ljava/lang/String; = "first_start"

.field public static final GEAR_CONNECTED_TIME:Ljava/lang/String; = "gear_connected_time"

.field public static final GOAL_STATUS:Ljava/lang/String; = "goal_status"

.field public static final INITIALIZATION_NEEDED:Ljava/lang/String; = "initialization_needed"

.field public static final IS_EVERYONE_RANKINGS_TOP_WALKERS_DATA_AVAILABLE:Ljava/lang/String; = "is_everyone_rankings_top_walkers_data_available"

.field public static final IS_EVERYONE_RANKINGS_TOTAL_DISTANCE_DATA_AVAILABLE:Ljava/lang/String; = "is_everyone_rankings_total_distance_data_available"

.field public static final IS_MY_RANKINGS_DATA_AVAILABLE:Ljava/lang/String; = "is_my_rankings_data_available"

.field public static final LAST_SYNCHED_DEVICE_TYPE:Ljava/lang/String; = "last_synched_device_type"

.field public static final MY_RANKING_UPDATED_TIME:Ljava/lang/String; = "my_rankings_updated_time"

.field static final NOTIFIED_CONTENT_PROVIDER_ACCESSIBLE:Ljava/lang/String; = "notified_content_provider_accessible"

.field public static final PEDOMETER_NOTIFICATION_ONOFF:Ljava/lang/String; = "pedometer_notification_onoff"

.field public static final PIN_ON:Ljava/lang/String; = "pin_on"

.field public static final PREDOMETER_INACTIVE_TIME:Ljava/lang/String; = "pedometer_inactive_time"

.field private static final PREDOMETER_INACTIVE_TIME_AVAILABLE_TIME_ALWAYS:Ljava/lang/String; = "pedometer_inactive_time_always"

.field private static final PREDOMETER_INACTIVE_TIME_AVAILABLE_TIME_FROM:Ljava/lang/String; = "pedometer_inactive_time_from"

.field private static final PREDOMETER_INACTIVE_TIME_AVAILABLE_TIME_TO:Ljava/lang/String; = "pedometer_inactive_time_to"

.field private static final PREDOMETER_INACTIVE_TIME_PERIOD:Ljava/lang/String; = "pedometer_inactive_time_period"

.field public static final PREDOMETER_NOTIFICATION:Ljava/lang/String; = "pedometer_notification"

.field public static final TIZEN_GEAR_CONNECTED_TIME:Ljava/lang/String; = "tizen_gear_connected_time"

.field public static final TOP_WALKERS_LIST:Ljava/lang/String; = "walkers_list"

.field public static final TOTAL_DISTANCE:Ljava/lang/String; = "total_distance"

.field public static final UPDATE_TIME_OF_ACTIVITY_TRACKER:Ljava/lang/String; = "update_time_of_activity_tracker"

.field public static final UPDATE_TIME_OF_ALL_GEARS:Ljava/lang/String; = "update_time_of_all_gears"

.field public static final UPDATE_TIME_OF_GEAR:Ljava/lang/String; = "update_time_of_gear"

.field public static final UPDATE_TIME_OF_GEAR2:Ljava/lang/String; = "update_time_of_gear2"

.field public static final UPDATE_TIME_OF_GEAR3:Ljava/lang/String; = "update_time_of_gear3"

.field public static final UPDATE_TIME_OF_GEARO:Ljava/lang/String; = "update_time_of_gearo"

.field public static final UPDATE_TIME_OF_GEAR_FIT:Ljava/lang/String; = "update_time_of_gear_fit"

.field public static final UPDATE_TIME_OF_TIZEN_GEAR:Ljava/lang/String; = "update_time_of_tizen_gear"

.field public static final USER_POSITION:Ljava/lang/String; = "user_position"

.field public static final VIEW_STEP_COUNT:Ljava/lang/String; = "view_step_count"

.field public static final WALKMATE_COVER_STATE:Ljava/lang/String; = "walkmate_cover_state"

.field public static final WALK_SCOREBOARD_SELECTED_TAB:Ljava/lang/String; = "walk_score_selected_tab"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearGoalNotification()V
    .locals 3

    .prologue
    .line 2412
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string v1, "goal_status"

    const-string v2, "empty"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2413
    return-void
.end method

.method public static getCignaMode()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2496
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "cigna_mode"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2500
    .local v0, "e":Ljava/lang/ClassCastException;
    :goto_0
    return v1

    .line 2497
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_0
    move-exception v0

    .line 2498
    .restart local v0    # "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "cigna_mode"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getCignaText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2504
    const-string v1, ""

    .line 2506
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "cigna_text"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2510
    :goto_0
    return-object v1

    .line 2507
    :catch_0
    move-exception v0

    .line 2508
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "cigna_text"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getCurrentConnectDevice()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2263
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v1

    const-string v2, "current_connect_device"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2264
    .local v0, "mValue":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[**] getCurrentConnectDevice = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2265
    return-object v0
.end method

.method public static getEveryoneRankingsTotalDistance()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 1959
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "total_distance"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static getFullGoalNotificationStatusForToday()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    .line 2454
    const/4 v1, 0x0

    .line 2455
    .local v1, "retStatus":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 2457
    .local v2, "start_time":J
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v6

    const-string v7, "goal_status"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2458
    .local v4, "status":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 2476
    .end local v1    # "retStatus":Z
    :cond_0
    :goto_0
    return v1

    .line 2462
    .restart local v1    # "retStatus":Z
    :cond_1
    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2464
    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2466
    .local v0, "data":[Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v6, v0, v6

    const-string v7, "full"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2467
    aget-object v6, v0, v5

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-nez v6, :cond_0

    move v1, v5

    .line 2468
    goto :goto_0
.end method

.method public static getGearConnectedTime()J
    .locals 6

    .prologue
    .line 2279
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v2

    const-string v3, "gear_connected_time"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2280
    .local v0, "mValue":J
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[**] getGearConnectedTime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2281
    return-wide v0
.end method

.method public static getHalfGoalNotificationStatusForToday()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    .line 2422
    const/4 v1, 0x0

    .line 2423
    .local v1, "retStatus":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 2425
    .local v2, "start_time":J
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v6

    const-string v7, "goal_status"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2426
    .local v4, "status":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 2443
    .end local v1    # "retStatus":Z
    :cond_0
    :goto_0
    return v1

    .line 2430
    .restart local v1    # "retStatus":Z
    :cond_1
    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2432
    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2434
    .local v0, "data":[Ljava/lang/String;
    aget-object v6, v0, v5

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-nez v6, :cond_0

    move v1, v5

    .line 2435
    goto :goto_0
.end method

.method public static getInactiveTimeTrackingAvailableTimeAlways()Z
    .locals 4

    .prologue
    .line 2360
    const/4 v1, 0x0

    .line 2362
    .local v1, "value":Z
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "pedometer_inactive_time_always"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2367
    :goto_0
    return v1

    .line 2363
    :catch_0
    move-exception v0

    .line 2364
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "pedometer_inactive_time_always"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2328
    const-string v1, "09:00"

    .line 2330
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "pedometer_inactive_time_from"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2335
    :goto_0
    return-object v1

    .line 2331
    :catch_0
    move-exception v0

    .line 2332
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "pedometer_inactive_time_from"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2344
    const-string v1, "17:00"

    .line 2346
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "pedometer_inactive_time_to"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2351
    :goto_0
    return-object v1

    .line 2347
    :catch_0
    move-exception v0

    .line 2348
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "pedometer_inactive_time_to"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getInactiveTimeTrackingPeriod()I
    .locals 4

    .prologue
    .line 2312
    const/16 v1, 0x3c

    .line 2314
    .local v1, "value":I
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "pedometer_inactive_time_period"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2319
    :goto_0
    return v1

    .line 2315
    :catch_0
    move-exception v0

    .line 2316
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "pedometer_inactive_time_period"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getLastConnectedDeviceType()I
    .locals 4

    .prologue
    .line 2094
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "last_synched_device_type"

    const/16 v3, 0x2719

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2095
    .local v0, "deviceType":I
    const-string v1, "PEDOCHECKER"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getLastConnectedDeviceType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2096
    return v0
.end method

.method public static getLboardRankingLastUpdatedTime()J
    .locals 4

    .prologue
    .line 1909
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "everyone_rankings_updated_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getMyRankingLastUpdatedTime()J
    .locals 4

    .prologue
    .line 1901
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "my_rankings_updated_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getMyRankingsBasicResponse()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1943
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "basic_response"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMyRankingsByAgeResponse()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1951
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "by_age_response"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNotificationONOFF()Z
    .locals 3

    .prologue
    .line 1893
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "pedometer_notification_onoff"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getTizenGearConnectedTime()J
    .locals 6

    .prologue
    .line 2290
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v2

    const-string/jumbo v3, "tizen_gear_connected_time"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2291
    .local v0, "mValue":J
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[**] getTizenGearConnectedTime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2292
    return-wide v0
.end method

.method public static getTopWalkersListRecoveryString()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1971
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "walkers_list"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getTopWalkersListUserPosition()I
    .locals 3

    .prologue
    .line 1975
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "user_position"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getUpdateTimeOfActivityTracker()J
    .locals 6

    .prologue
    .line 2236
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_activity_tracker"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2237
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getUpdateTimeOfActivityTracker : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2238
    return-wide v0
.end method

.method public static getUpdateTimeOfAllGears()J
    .locals 6

    .prologue
    .line 2138
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_all_gears"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2139
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE_TIME_OF_ALL_GEARS : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2140
    return-wide v0
.end method

.method public static getUpdateTimeOfGear()J
    .locals 6

    .prologue
    .line 2166
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_gear"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2167
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE_TIME_OF_GEAR : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2168
    return-wide v0
.end method

.method public static getUpdateTimeOfGear2()J
    .locals 6

    .prologue
    .line 2194
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_gear2"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2195
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getUpdateTimeOfGear2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2196
    return-wide v0
.end method

.method public static getUpdateTimeOfGear3()J
    .locals 6

    .prologue
    .line 2208
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_gear3"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2209
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getUpdateTimeOfGear3 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2210
    return-wide v0
.end method

.method public static getUpdateTimeOfGearFit()J
    .locals 6

    .prologue
    .line 2152
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_gear_fit"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2153
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE_TIME_OF_GEAR_FIT : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2154
    return-wide v0
.end method

.method public static getUpdateTimeOfGearO()J
    .locals 6

    .prologue
    .line 2222
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_gearo"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2223
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE_TIME_OF_GEARO : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2224
    return-wide v0
.end method

.method public static getUpdateTimeOfTizenGear()J
    .locals 6

    .prologue
    .line 2180
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "update_time_of_tizen_gear"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2181
    .local v0, "time":J
    const-string v2, "SharedPreferencesHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE_TIME_OF_TIZEN_GEAR : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2182
    return-wide v0
.end method

.method public static getViewStepCount()I
    .locals 5

    .prologue
    const/16 v4, 0x2719

    .line 2064
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "view_step_count"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2065
    .local v0, "view":I
    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isValidSahredPrefValue(I)Z

    move-result v1

    if-nez v1, :cond_0

    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$200()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2067
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$200()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/datamigration/versionk/KtoTSharedPrefMigration;->convertViewStepCountPrefValue(Landroid/content/Context;)V

    .line 2068
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "view_step_count"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2070
    :cond_0
    const-string v1, "PEDOCHECKER"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current view = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2073
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v1

    if-nez v1, :cond_1

    if-ne v0, v4, :cond_1

    .line 2074
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getLastConnectedDeviceType()I

    move-result v0

    .line 2076
    :cond_1
    return v0
.end method

.method public static getWalkScoreBoardSelectedTab()I
    .locals 3

    .prologue
    .line 1885
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "walk_score_selected_tab"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static isEveryoneRankingTopWalkersListDataAvailable()Z
    .locals 3

    .prologue
    .line 1935
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "is_everyone_rankings_top_walkers_data_available"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isEveryoneRankingTotalDistanceDataAvailable()Z
    .locals 3

    .prologue
    .line 1927
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "is_everyone_rankings_total_distance_data_available"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isFirstDeviceConnectedCheck(Ljava/lang/String;)Z
    .locals 4
    .param p0, "connectedDevice"    # Ljava/lang/String;

    .prologue
    .line 2116
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2117
    .local v0, "mCheckValue":Z
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[**] isFirstDeviceConnectedCheck = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2118
    return v0
.end method

.method public static isFirstDeviceConnection(I)Z
    .locals 3
    .param p0, "deviceType"    # I

    .prologue
    .line 2106
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "first_device_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isFirstStart()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2482
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "first_start"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2486
    .local v0, "e":Ljava/lang/ClassCastException;
    :goto_0
    return v1

    .line 2483
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_0
    move-exception v0

    .line 2484
    .restart local v0    # "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "first_start"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static isInactiveTimeTracking()Z
    .locals 4

    .prologue
    .line 2296
    const/4 v1, 0x1

    .line 2298
    .local v1, "value":Z
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "pedometer_inactive_time"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2303
    :goto_0
    return v1

    .line 2299
    :catch_0
    move-exception v0

    .line 2300
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "pedometer_inactive_time"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static isInitializationNeeded()Z
    .locals 3

    .prologue
    .line 2015
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "initialization_needed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static final isInitializationNeeded(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2019
    if-nez p0, :cond_0

    .line 2020
    const/4 v3, 0x0

    .line 2038
    :goto_0
    return v3

    .line 2022
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2023
    .local v1, "extras":Landroid/os/Bundle;
    const-string v3, "key"

    const-string v4, "initialization_needed"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2024
    const/4 v0, 0x0

    .line 2026
    .local v0, "bundle":Landroid/os/Bundle;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2030
    :goto_1
    const/4 v2, 0x0

    .line 2032
    .local v2, "value":Ljava/lang/Object;
    if-eqz v0, :cond_1

    .line 2033
    const-string/jumbo v3, "value"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 2035
    .end local v2    # "value":Ljava/lang/Object;
    :cond_1
    if-nez v2, :cond_2

    .line 2036
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .local v2, "value":Ljava/lang/Boolean;
    move-object v3, v2

    .line 2038
    .end local v2    # "value":Ljava/lang/Boolean;
    :goto_2
    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto :goto_0

    .line 2027
    :catch_0
    move-exception v3

    goto :goto_1

    :cond_2
    move-object v3, v2

    goto :goto_2
.end method

.method public static isMyRankingDataAvailable()Z
    .locals 3

    .prologue
    .line 1919
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "is_my_rankings_data_available"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isNeedDeviceSyncGuidePopup()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2242
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v4

    const-string v5, "device_view_guide_popup_check"

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2243
    .local v0, "mCheckValue":Z
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    .line 2245
    .local v1, "mConnectAccessory":I
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[**] mCheckValue = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mConnectAccessory = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2247
    if-eqz v0, :cond_0

    .line 2248
    const/16 v4, 0x2719

    if-ne v1, v4, :cond_1

    .line 2253
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    .line 2251
    goto :goto_0
.end method

.method public static isPedometerNotificationOn()Z
    .locals 4

    .prologue
    .line 2376
    const/4 v1, 0x1

    .line 2378
    .local v1, "value":Z
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "pedometer_notification"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2383
    :goto_0
    return v1

    .line 2379
    :catch_0
    move-exception v0

    .line 2380
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "pedometer_notification"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static isPinOn()Z
    .locals 3

    .prologue
    .line 2011
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "pin_on"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isStartWalking()Z
    .locals 5

    .prologue
    .line 1993
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "BUTTON_STATE"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1999
    .local v1, "value":Z
    :goto_0
    return v1

    .line 1994
    .end local v1    # "value":Z
    :catch_0
    move-exception v0

    .line 1995
    .local v0, "e":Ljava/lang/ClassCastException;
    const/4 v1, 0x0

    .line 1996
    .restart local v1    # "value":Z
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "BUTTON_STATE"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private static isValidSahredPrefValue(I)Z
    .locals 2
    .param p0, "view"    # I

    .prologue
    const/4 v0, 0x1

    .line 2082
    if-lt p0, v0, :cond_0

    const/4 v1, 0x6

    if-gt p0, v1, :cond_0

    .line 2084
    const/4 v0, 0x0

    .line 2086
    :cond_0
    return v0
.end method

.method public static isWalkMateCoverVisible()Z
    .locals 3

    .prologue
    .line 1983
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "walkmate_cover_state"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static notifiedContentProviderAccessible()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2525
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "notified_content_provider_accessible"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2529
    .local v0, "e":Ljava/lang/ClassCastException;
    :goto_0
    return v1

    .line 2526
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_0
    move-exception v0

    .line 2527
    .restart local v0    # "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "notified_content_provider_accessible"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static saveGearConnectedTime(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2274
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] saveGearConnectedTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2275
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string v1, "gear_connected_time"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2276
    return-void
.end method

.method public static saveTizenGearConnectedTime(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2285
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] saveTizenGearConnectedTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2286
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "tizen_gear_connected_time"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2287
    return-void
.end method

.method public static saveUpdateTimeOfActivityTracker(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2228
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2229
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_activity_tracker"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2233
    :goto_0
    return-void

    .line 2231
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveUpdateTimeOfAllGears(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2130
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2131
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_all_gears"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2135
    :goto_0
    return-void

    .line 2133
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveUpdateTimeOfGear(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2158
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2159
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_gear"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2163
    :goto_0
    return-void

    .line 2161
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveUpdateTimeOfGear2(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2186
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2187
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_gear2"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2191
    :goto_0
    return-void

    .line 2189
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveUpdateTimeOfGear3(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2200
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2201
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_gear3"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2205
    :goto_0
    return-void

    .line 2203
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveUpdateTimeOfGearFit(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2144
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2145
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_gear_fit"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2149
    :goto_0
    return-void

    .line 2147
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveUpdateTimeOfGearO(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2214
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2215
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_gearo"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2219
    :goto_0
    return-void

    .line 2217
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveUpdateTimeOfTizenGear(J)V
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 2172
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 2173
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "update_time_of_tizen_gear"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2177
    :goto_0
    return-void

    .line 2175
    :cond_0
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveViewStepCount(I)V
    .locals 3
    .param p0, "view"    # I

    .prologue
    const/16 v2, 0x272f

    .line 2042
    const/16 v0, 0x2719

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2727

    if-eq p0, v0, :cond_0

    if-nez p0, :cond_1

    .line 2045
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "view_step_count"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2046
    const-string v0, "PEDOCHECKER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "1. view change = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2061
    :goto_0
    return-void

    .line 2047
    :cond_1
    const/16 v0, 0x2723

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2728

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2724

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2726

    if-eq p0, v0, :cond_2

    const/16 v0, 0x272e

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2730

    if-eq p0, v0, :cond_2

    if-ne p0, v2, :cond_3

    .line 2054
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "view_step_count"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2055
    const-string v0, "PEDOCHECKER"

    const-string v1, "2. view change = 10031"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2059
    :cond_3
    const-string v0, "SharedPreferencesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed saveViewStepCount => wrong view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setCignaMode(Z)V
    .locals 3
    .param p0, "cignaMode"    # Z

    .prologue
    .line 2514
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string v1, "cigna_mode"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2516
    return-void
.end method

.method public static setCignaText(Ljava/lang/String;)V
    .locals 2
    .param p0, "cignaText"    # Ljava/lang/String;

    .prologue
    .line 2519
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string v1, "cigna_text"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2520
    return-void
.end method

.method public static setCurrentConnectDevice(Ljava/lang/String;)V
    .locals 3
    .param p0, "device_key"    # Ljava/lang/String;

    .prologue
    .line 2269
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setCurrentConnectDevice = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2270
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string v1, "current_connect_device"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2271
    return-void
.end method

.method public static setEveryoneRankingTotalDistanceDataAvailable(Z)V
    .locals 2
    .param p0, "isAvailable"    # Z

    .prologue
    .line 1931
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "is_everyone_rankings_total_distance_data_available"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1932
    return-void
.end method

.method public static setEveryoneRankingTotalTopWalkersLisDataAvailable(Z)V
    .locals 2
    .param p0, "isAvailable"    # Z

    .prologue
    .line 1939
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "is_everyone_rankings_top_walkers_data_available"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1940
    return-void
.end method

.method public static setEveryoneRankingsTotalDistance(J)V
    .locals 2
    .param p0, "totalDistance"    # J

    .prologue
    .line 1963
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "total_distance"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1964
    return-void
.end method

.method public static setFirstConnectionAsSuccess(IZ)V
    .locals 3
    .param p0, "deviceType"    # I
    .param p1, "value"    # Z

    .prologue
    .line 2125
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[FIRSTCONNECTION]setFirstConnectionAsSuccess - value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2126
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "first_device_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2127
    return-void
.end method

.method public static setFirstStart(Z)V
    .locals 3
    .param p0, "config"    # Z

    .prologue
    .line 2490
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string v1, "first_start"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2491
    return-void
.end method

.method public static setGoalNotification(Z)V
    .locals 5
    .param p0, "isFull"    # Z

    .prologue
    .line 2398
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    .line 2401
    .local v1, "start_time":J
    if-eqz p0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "full_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2403
    .local v0, "goal_value":Ljava/lang/String;
    :goto_0
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v3

    const-string v4, "goal_status"

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2406
    return-void

    .line 2401
    .end local v0    # "goal_value":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "half_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static setInactiveTimeTracking(Z)V
    .locals 3
    .param p0, "config"    # Z

    .prologue
    .line 2307
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTracking = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2308
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "pedometer_inactive_time"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2309
    return-void
.end method

.method public static setInactiveTimeTrackingAvailableTimeAlways(Z)V
    .locals 3
    .param p0, "config"    # Z

    .prologue
    .line 2371
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingAvailableTimeAlways = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2372
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "pedometer_inactive_time_always"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2373
    return-void
.end method

.method public static setInactiveTimeTrackingAvailableTimeFrom(Ljava/lang/String;)V
    .locals 3
    .param p0, "config"    # Ljava/lang/String;

    .prologue
    .line 2339
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingAvailableTimeFrom = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2340
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "pedometer_inactive_time_from"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2341
    return-void
.end method

.method public static setInactiveTimeTrackingAvailableTimeTo(Ljava/lang/String;)V
    .locals 3
    .param p0, "config"    # Ljava/lang/String;

    .prologue
    .line 2355
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingAvailableTimeFrom = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2356
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "pedometer_inactive_time_to"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2357
    return-void
.end method

.method public static setInactiveTimeTrackingPeriod(I)V
    .locals 3
    .param p0, "config"    # I

    .prologue
    .line 2323
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingPeriod = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2324
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "pedometer_inactive_time_period"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2325
    return-void
.end method

.method public static setLastConnectedDeviceType(I)V
    .locals 2
    .param p0, "deviceType"    # I

    .prologue
    .line 2090
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_synched_device_type"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2091
    return-void
.end method

.method public static setLboardRankingLastUpdatedTime(J)V
    .locals 2
    .param p0, "time"    # J

    .prologue
    .line 1913
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "everyone_rankings_updated_time"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1914
    return-void
.end method

.method public static setMyRankingDataAvailable(Z)V
    .locals 2
    .param p0, "isAvailable"    # Z

    .prologue
    .line 1923
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "is_my_rankings_data_available"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1924
    return-void
.end method

.method public static setMyRankingLastUpdatedTime(J)V
    .locals 2
    .param p0, "time"    # J

    .prologue
    .line 1905
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "my_rankings_updated_time"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1906
    return-void
.end method

.method public static setMyRankingsBasicResponse(Ljava/lang/String;)V
    .locals 2
    .param p0, "basicResponseRecoveryString"    # Ljava/lang/String;

    .prologue
    .line 1947
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "basic_response"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1948
    return-void
.end method

.method public static setMyRankingsByAgeResponse(Ljava/lang/String;)V
    .locals 2
    .param p0, "byAgeResponseRecoveryString"    # Ljava/lang/String;

    .prologue
    .line 1955
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "by_age_response"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1956
    return-void
.end method

.method public static setNeedDeviceSyncGuidePopup(Z)V
    .locals 3
    .param p0, "value"    # Z

    .prologue
    .line 2257
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] DEVICE_VIEW_GUIDE_POPUP_CHECK = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2259
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string v1, "device_view_guide_popup_check"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2260
    return-void
.end method

.method public static setNotificationONOFF(Z)V
    .locals 2
    .param p0, "isSetted"    # Z

    .prologue
    .line 1897
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pedometer_notification_onoff"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1898
    return-void
.end method

.method public static setNotifiedContentProviderAccessible(Z)V
    .locals 3
    .param p0, "notified"    # Z

    .prologue
    .line 2534
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "notified_content_provider_accessible"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2535
    return-void
.end method

.method public static setPedometerNotification(Z)V
    .locals 3
    .param p0, "config"    # Z

    .prologue
    .line 2387
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setPedometerNotification = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2388
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "pedometer_notification"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2389
    return-void
.end method

.method public static setStartWalking(Z)V
    .locals 4
    .param p0, "isStarted"    # Z

    .prologue
    .line 2003
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "BUTTON_STATE"

    invoke-interface {v1, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2004
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    .line 2005
    .local v0, "pedometer_status":I
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "shealth_pedometer_status_running"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2006
    const-string v1, "SHMSD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SPSR : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2007
    const-string v1, "PEDOSTART"

    const-string/jumbo v2, "shealth_pedometer_status_running"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2008
    return-void

    .line 2004
    .end local v0    # "pedometer_status":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setTopWalkersList(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1967
    .local p0, "walkersSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "walkers_list"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1968
    return-void
.end method

.method public static setTopWalkersListUserPosition(I)V
    .locals 2
    .param p0, "position"    # I

    .prologue
    .line 1979
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "user_position"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1980
    return-void
.end method

.method public static setWalkMateCoverVisible(Z)V
    .locals 2
    .param p0, "isVisible"    # Z

    .prologue
    .line 1987
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "walkmate_cover_state"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1988
    return-void
.end method

.method public static setWalkScoreBoardSelectedTab(I)V
    .locals 2
    .param p0, "tab"    # I

    .prologue
    .line 1889
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "walk_score_selected_tab"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1890
    return-void
.end method
