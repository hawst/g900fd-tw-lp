.class Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;
.super Ljava/lang/Object;
.source "AddFavoritesActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adapter":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/data/IconData;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # operator-- for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$110(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    .line 217
    invoke-static {p2, v4, v3, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 235
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$500(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->notifyDataSetChanged()V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setActionBarSpinner()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$600(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    .line 238
    return-void

    :cond_0
    move v1, v3

    .line 215
    goto :goto_0

    .line 222
    :cond_1
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    move-result v1

    if-le v0, v1, :cond_3

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/data/IconData;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/data/IconData;->setIsSelected(Z)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # operator++ for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFavoritesCount:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$108(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)I

    .line 226
    invoke-static {p2, v4, v3, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    goto :goto_1

    :cond_2
    move v1, v3

    .line 224
    goto :goto_2

    .line 231
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$2;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    const v1, 0x7f090dfa

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
