.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;
.super Ljava/lang/Object;
.source "CignaLibraryDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager$1;,
        Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager$CignaLibraryDataManagerHodler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager$1;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager$CignaLibraryDataManagerHodler;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    return-object v0
.end method


# virtual methods
.method public addFavorite(I)Z
    .locals 5
    .param p1, "articleId"    # I

    .prologue
    const/4 v2, 0x0

    .line 195
    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    .line 196
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->TAG:Ljava/lang/String;

    const-string v4, "ArticleId is -1"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :goto_0
    return v2

    .line 200
    :cond_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v1

    .line 204
    .local v1, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, p1}, Lcom/cigna/coach/interfaces/IHealthLibrary;->addFavorite(Ljava/lang/String;I)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public getArticle(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .locals 6
    .param p1, "articleId"    # I
    .param p2, "subCategoryId"    # I

    .prologue
    const/4 v3, 0x0

    .line 172
    const/4 v4, -0x1

    if-ne p1, v4, :cond_0

    .line 173
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->TAG:Ljava/lang/String;

    const-string v5, "ArticleId is -1"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :goto_0
    return-object v3

    .line 177
    :cond_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v2

    .line 181
    .local v2, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, p1, p2}, Lcom/cigna/coach/interfaces/IHealthLibrary;->getArticle(Ljava/lang/String;II)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    move-result-object v1

    .line 183
    .local v1, "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 185
    .end local v1    # "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFavoriteArticle()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .local v0, "articleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v5

    .line 243
    .local v5, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/cigna/coach/interfaces/IHealthLibrary;->getFavoritesByCategory(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 245
    .local v1, "cr":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .line 247
    .local v3, "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 251
    .end local v1    # "cr":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    .end local v3    # "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 253
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 256
    .end local v2    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_0
    return-object v0
.end method

.method public getLibraryArticle(IILjava/util/ArrayList;)V
    .locals 7
    .param p1, "categoryId"    # I
    .param p2, "subCategoryId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    .local p3, "articleListData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v4

    .line 158
    .local v4, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, p1, p2}, Lcom/cigna/coach/interfaces/IHealthLibrary;->getArticlesForSubCategory(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v1

    .line 160
    .local v1, "articleList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .line 161
    .local v0, "articleData":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 164
    .end local v0    # "articleData":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .end local v1    # "articleList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 166
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 168
    .end local v2    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_0
    return-void
.end method

.method public getLibraryCategroyTitle(I)Ljava/lang/String;
    .locals 8
    .param p1, "categoryId"    # I

    .prologue
    .line 82
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v4

    .line 85
    .local v4, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/cigna/coach/interfaces/IHealthLibrary;->getCategories(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 86
    .local v0, "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryCategory;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;

    .line 88
    .local v2, "healthLibraryCategory":Lcom/cigna/coach/apiobjects/HealthLibraryCategory;
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;->getCategoryId()I

    move-result v5

    if-ne v5, p1, :cond_0

    .line 89
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getLibraryCategroyTitle : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;->getDescription()Ljava/lang/String;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 98
    .end local v0    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryCategory;>;"
    .end local v2    # "healthLibraryCategory":Lcom/cigna/coach/apiobjects/HealthLibraryCategory;
    .end local v3    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v5

    .line 94
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 98
    .end local v1    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public getLibraryCategroys(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "categoryListData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;>;"
    .local p2, "categoryTopArticleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v9

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v8

    .line 40
    .local v8, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/cigna/coach/interfaces/IHealthLibrary;->getCategories(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 41
    .local v2, "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryCategory;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;

    .line 43
    .local v6, "healthLibraryCategory":Lcom/cigna/coach/apiobjects/HealthLibraryCategory;
    move-object v0, v6

    check-cast v0, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;

    move-object v1, v0

    .line 46
    .local v1, "category":Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 74
    .end local v1    # "category":Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    .end local v2    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryCategory;>;"
    .end local v6    # "healthLibraryCategory":Lcom/cigna/coach/apiobjects/HealthLibraryCategory;
    .end local v7    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 76
    .local v3, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v3}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 78
    .end local v3    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_0
    return-void

    .line 69
    .restart local v2    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryCategory;>;"
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/cigna/coach/interfaces/IHealthLibrary;->getFeaturedArticles(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 70
    .local v5, "healthLibraryArticleList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .line 71
    .local v4, "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public getLibrarySubCategory(ILjava/util/ArrayList;)V
    .locals 9
    .param p1, "categoryId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p2, "subCategoryListData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;>;"
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v3

    .line 132
    .local v3, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7, p1}, Lcom/cigna/coach/interfaces/IHealthLibrary;->getSubCategories(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v6

    .line 134
    .local v6, "subCategoryList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;

    .line 136
    .local v5, "subCategoryData":Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;
    move-object v0, v5

    check-cast v0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;

    move-object v4, v0

    .line 138
    .local v4, "subCategory":Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 147
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "subCategory":Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;
    .end local v5    # "subCategoryData":Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;
    .end local v6    # "subCategoryList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;>;"
    :catch_0
    move-exception v1

    .line 149
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 151
    .end local v1    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_0
    return-void
.end method

.method public getSearchArticle(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "searchText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 263
    .local v0, "articleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v5

    .line 267
    .local v5, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, p1}, Lcom/cigna/coach/interfaces/IHealthLibrary;->searchArticles(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 269
    .local v1, "cr":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .line 271
    .local v3, "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 275
    .end local v1    # "cr":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    .end local v3    # "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 277
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 280
    .end local v2    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_0
    return-object v0
.end method

.method public removeFavorite(I)Z
    .locals 5
    .param p1, "articleId"    # I

    .prologue
    const/4 v2, 0x0

    .line 216
    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    .line 217
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->TAG:Ljava/lang/String;

    const-string v4, "ArticleId is -1"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :goto_0
    return v2

    .line 221
    :cond_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;

    move-result-object v1

    .line 225
    .local v1, "ihl":Lcom/cigna/coach/interfaces/IHealthLibrary;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, p1}, Lcom/cigna/coach/interfaces/IHealthLibrary;->removeFavorite(Ljava/lang/String;I)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 227
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method
