.class public Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "WalkingMateInactiveTimeSetTimeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "WalkingMateInactiveTimeSetTimeFragment"


# instance fields
.field private mCalendar:Ljava/util/Calendar;

.field private mFromButton:Landroid/widget/Button;

.field private mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

.field private mToButton:Landroid/widget/Button;

.field private mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

.field private mView:Landroid/view/View;

.field private timeFormatter:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    .line 65
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->timeFormatter:Ljava/text/SimpleDateFormat;

    .line 172
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->configButtonStatus()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Ljava/util/Calendar;IILcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;
    .param p1, "x1"    # Ljava/util/Calendar;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->setTime(Ljava/util/Calendar;IILcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getMeasureTimeInMillis(Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Ljava/text/SimpleDateFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->timeFormatter:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->validateFromTime(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;Ljava/lang/String;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getUtilDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->validateToTime(II)Z

    move-result v0

    return v0
.end method

.method private configButtonStatus()V
    .locals 5

    .prologue
    const v4, 0x7f07017c

    const v3, 0x7f07017b

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 190
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeAlways()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 203
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0
.end method

.method private getMeasureTimeInMillis(Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;)J
    .locals 3
    .param p1, "timePicker"    # Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    .prologue
    .line 313
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 314
    .local v0, "timeCalendar":Ljava/util/Calendar;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 315
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method private getUtilDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 6
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 295
    const-string v3, "01/01/2014"

    .line 296
    .local v3, "selectedDate":Ljava/lang/String;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v4, "dd/MM/yyyy HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 297
    .local v1, "dateTimeFormatter":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 299
    .local v0, "date":Ljava/util/Date;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 303
    :goto_0
    return-object v0

    .line 300
    :catch_0
    move-exception v2

    .line 301
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method private setTime(Ljava/util/Calendar;IILcom/sec/android/app/shealth/framework/ui/common/TimePicker;)V
    .locals 2
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "hourOfDay"    # I
    .param p3, "minute"    # I
    .param p4, "timePicker"    # Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    .prologue
    .line 307
    const/16 v0, 0xb

    invoke-virtual {p1, v0, p2}, Ljava/util/Calendar;->set(II)V

    .line 308
    const/16 v0, 0xc

    invoke-virtual {p1, v0, p3}, Ljava/util/Calendar;->set(II)V

    .line 309
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p4, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->setMeasureTime(J)Z

    .line 310
    return-void
.end method

.method private validateFromTime(II)Z
    .locals 13
    .param p1, "hourOfDay"    # I
    .param p2, "minute"    # I

    .prologue
    const/4 v12, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 243
    const-string v9, "WalkingMateInactiveTimeSetTimeFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "setting hour =  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " min = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v3

    .line 245
    .local v3, "from":Ljava/lang/String;
    const-string v9, ":"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 246
    .local v6, "p":[Ljava/lang/String;
    aget-object v9, v6, v7

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 247
    .local v4, "hourTo":I
    aget-object v9, v6, v8

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 248
    .local v5, "minTo":I
    mul-int/lit8 v9, v4, 0x3c

    add-int/2addr v9, v5

    mul-int/lit8 v10, p1, 0x3c

    add-int/2addr v10, p2

    if-ne v9, v10, :cond_0

    .line 249
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    const v9, 0x7f0900d8

    invoke-direct {v1, v8, v7, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 250
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v8, 0x7f0907d0

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 251
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    .line 252
    .local v0, "alert":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 264
    .end local v0    # "alert":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .end local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :goto_0
    return v7

    .line 255
    :cond_0
    const v9, 0x7f0907ce

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v10, v8

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 257
    .local v2, "config":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 258
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTrackingAvailableTimeFrom(Ljava/lang/String;)V

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v7

    invoke-virtual {v7, v12}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 260
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-virtual {v7, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    move v7, v8

    .line 264
    goto :goto_0
.end method

.method private validateToTime(II)Z
    .locals 13
    .param p1, "hourOfDay"    # I
    .param p2, "minute"    # I

    .prologue
    const/4 v12, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 268
    const-string v9, "WalkingMateInactiveTimeSetTimeFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "setting hour =  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " min = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v6

    .line 270
    .local v6, "to":Ljava/lang/String;
    const-string v9, ":"

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 271
    .local v5, "p":[Ljava/lang/String;
    aget-object v9, v5, v7

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 272
    .local v3, "hourFrom":I
    aget-object v9, v5, v8

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 273
    .local v4, "minFrom":I
    mul-int/lit8 v9, v3, 0x3c

    add-int/2addr v9, v4

    mul-int/lit8 v10, p1, 0x3c

    add-int/2addr v10, p2

    if-ne v9, v10, :cond_0

    .line 274
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    const v9, 0x7f0900d8

    invoke-direct {v1, v8, v7, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 275
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v8, 0x7f0907d0

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 276
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    .line 277
    .local v0, "alert":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 288
    .end local v0    # "alert":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .end local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :goto_0
    return v7

    .line 280
    :cond_0
    const v9, 0x7f0907ce

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v10, v8

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, "config":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 283
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTrackingAvailableTimeTo(Ljava/lang/String;)V

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v7

    invoke-virtual {v7, v12}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 285
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-virtual {v7, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    move v7, v8

    .line 288
    goto :goto_0
.end method


# virtual methods
.method getFromButtonController()Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V

    return-object v0
.end method

.method getToButtonController()Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v5, 0x7f080bab

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 208
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 240
    :goto_0
    :pswitch_0
    return-void

    .line 210
    :pswitch_1
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v6

    :goto_1
    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_0
    move v5, v7

    goto :goto_1

    .line 214
    :pswitch_2
    new-array v4, v6, [Landroid/view/View;

    aput-object p1, v4, v7

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 215
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "from":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getUtilDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 218
    .local v1, "tempDate":Ljava/util/Date;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 220
    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    const-string v6, "from"

    invoke-direct {v4, v5, v1, v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;-><init>(Landroid/widget/Button;Ljava/util/Date;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    .line 221
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->setMeasureTime(J)Z

    .line 222
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    .line 223
    .local v2, "timeDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "from"

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 227
    .end local v0    # "from":Ljava/lang/String;
    .end local v1    # "tempDate":Ljava/util/Date;
    .end local v2    # "timeDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    :pswitch_3
    new-array v4, v6, [Landroid/view/View;

    aput-object p1, v4, v7

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 228
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v3

    .line 230
    .local v3, "to":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getUtilDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 231
    .restart local v1    # "tempDate":Ljava/util/Date;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 233
    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    const-string/jumbo v6, "to"

    invoke-direct {v4, v5, v1, v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;-><init>(Landroid/widget/Button;Ljava/util/Date;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    .line 234
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->setMeasureTime(J)Z

    .line 235
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToPicker:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    .line 236
    .restart local v2    # "timeDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v5, "to"

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x7f080baa
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f080bab

    .line 138
    const v0, 0x7f030296

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    const v1, 0x7f080baa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeAlways()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->updateDate()V

    .line 168
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->configButtonStatus()V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method updateDate()V
    .locals 4

    .prologue
    .line 177
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "fromTime":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v1

    .line 180
    .local v1, "toTime":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get24Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get24Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mFromButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->mToButton:Landroid/widget/Button;

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
