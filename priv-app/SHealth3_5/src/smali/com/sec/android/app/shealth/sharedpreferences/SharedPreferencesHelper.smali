.class public Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;
.super Ljava/lang/Object;
.source "SharedPreferencesHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;,
        Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;,
        Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$ExerciseStaticKeys;,
        Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;,
        Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$SharedPreferencesKeys;
    }
.end annotation


# static fields
.field private static DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final OBJ_LOCK:Ljava/lang/Object;

.field private static final TAG:Ljava/lang/String;

.field public static final TGH_DEFAULT_VALUE:F = -1000.0f

.field private static mContext:Landroid/content/Context;

.field public static mDevicePref:Landroid/content/SharedPreferences;

.field private static prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

.field public static prefs:Landroid/content/SharedPreferences;

.field static unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    const-class v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->OBJ_LOCK:Ljava/lang/Object;

    .line 42
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    .line 43
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth_devicePref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    .line 45
    sput-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 46
    sput-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    .line 47
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInstance()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    .line 52
    new-instance v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$2;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1831
    return-void
.end method

.method public static ClearRealtimeAllGoalType()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1102
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_running_goal_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1103
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_walking_goal_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1104
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_cycling_goal_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1105
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_hiking_goal_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1106
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_goal_type"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1107
    return-void
.end method

.method public static ClearRealtimeAllGoalValue()V
    .locals 5

    .prologue
    const/16 v4, 0x1e

    .line 1116
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_distance_value"

    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1117
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_time_value"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1118
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_calories_value"

    const/16 v2, 0x12c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1119
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_training_level"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1120
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_training_time_value"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1130
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    return-object v0
.end method

.method static synthetic access$200()Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static clearDisplayDataStatus()V
    .locals 4

    .prologue
    .line 1110
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1111
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pro_realtime_disp_data_status"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1113
    :cond_0
    return-void
.end method

.method public static clearRealTimeManualyMaxHR()V
    .locals 11

    .prologue
    .line 784
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 785
    .local v2, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 786
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 787
    .local v1, "birthDate":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getDay()I

    move-result v3

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v5

    add-int/lit16 v5, v5, 0x76c

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/firstbeat/utils/Utils;->getAgeValue(III)I

    move-result v0

    .line 788
    .local v0, "age":I
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v4, "manualy_max_hr"

    const-wide v5, 0x406a400000000000L    # 210.0

    const-wide v7, 0x3fe4cccccccccccdL    # 0.65

    int-to-double v9, v0

    mul-double/2addr v7, v9

    sub-double/2addr v5, v7

    double-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 789
    return-void
.end method

.method public static ex_getLocationPopupWasShown()Z
    .locals 3

    .prologue
    .line 1583
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_location_popup_flag"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ex_saveLocationPopupWasShown(Z)V
    .locals 3
    .param p0, "val"    # Z

    .prologue
    .line 1579
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_location_popup_flag"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1580
    return-void
.end method

.method public static getAntAvailablilty(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 852
    if-nez p0, :cond_0

    .line 856
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v2, "ant_device_type_available"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final getAutoBackupInterval(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v0, 0x2a30

    .line 245
    if-nez p0, :cond_0

    .line 247
    :goto_0
    return-wide v0

    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v3, "auto_backup_interval"

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static final getAutoBackupSwitchStatus(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    if-nez p0, :cond_0

    .line 259
    const/4 v4, 0x0

    .line 273
    :goto_0
    return v4

    .line 260
    :cond_0
    const/4 v1, 0x0

    .line 262
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x80

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 266
    :goto_1
    const/4 v3, 0x0

    .line 267
    .local v3, "isAutoBackupEnable":Z
    if-eqz v1, :cond_1

    .line 269
    iget-object v0, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 270
    .local v0, "aBundle":Landroid/os/Bundle;
    const-string v4, "AutoBackupON"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 273
    .end local v0    # "aBundle":Landroid/os/Bundle;
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v5, "auto_backup_switch_status"

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    goto :goto_0

    .line 263
    .end local v3    # "isAutoBackupEnable":Z
    :catch_0
    move-exception v2

    .line 264
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public static final getAutoBackupWifiEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 296
    if-nez p0, :cond_0

    .line 298
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "auto_backup_wifi_enable"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static getChartFlag()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1517
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_chartFlagButton"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1518
    .local v0, "val":Ljava/lang/String;
    return-object v0
.end method

.method public static final getCignaRestoreTriggerPopup(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 122
    if-nez p0, :cond_0

    .line 126
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "coach_restore_trigger"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final getCurrentSamsungAccountSyncedTime(Landroid/content/Context;)Ljava/lang/Long;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v2, 0x0

    .line 407
    if-nez p0, :cond_0

    .line 408
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 411
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "samsungaccount_last_synced_time"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDeleteDashboardItem(Ljava/lang/String;)J
    .locals 5
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 192
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-wide/16 v3, -0x1

    invoke-virtual {v2, p0, v3, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 193
    .local v0, "val":J
    const-string v2, "HomeLatestData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HomeLatestData check delete key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "long : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    return-wide v0
.end method

.method public static getDeviceTypeChecked(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 831
    if-nez p0, :cond_0

    .line 832
    const-string v0, "Device Type Shared Preferences"

    const-string v1, "Device Type Shared Preferences - getDeviceTypeChecked - Context is null "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    const/4 v0, 0x1

    .line 838
    :goto_0
    return v0

    .line 835
    :cond_0
    const-string v0, "Device Type Shared Preferences"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device Type Shared Preferences - getDeviceTypeChecked -"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v3, "device_type_available_checked"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v1, "device_type_available_checked"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static getDistanceUnitFromSharedPreferences()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1456
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-eqz v1, :cond_1

    .line 1457
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    .line 1458
    .local v0, "unit":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1461
    .end local v0    # "unit":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 1458
    .restart local v0    # "unit":Ljava/lang/String;
    :cond_0
    const-string v0, "km"

    goto :goto_0

    .line 1461
    .end local v0    # "unit":Ljava/lang/String;
    :cond_1
    const-string v0, "km"

    goto :goto_0
.end method

.method public static getEquipmentType()I
    .locals 4

    .prologue
    .line 1047
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_equipment_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1048
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getActivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    return v0
.end method

.method public static getFirstbeatAscrActivated()Z
    .locals 3

    .prologue
    .line 1421
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_firstbeat_ascr_activated"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getGrapthSelectedTab()I
    .locals 3

    .prologue
    .line 2558
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "pedometer_selected_tab"

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getHRAvailablilty(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 869
    if-nez p0, :cond_0

    .line 873
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v2, " hr_device_type_available"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static getHRAvailabliltyFalseIfUnknown(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 878
    if-nez p0, :cond_0

    .line 882
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v2, " hr_device_type_available"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static getHomePageMark()I
    .locals 4

    .prologue
    .line 178
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "home_page_mark"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 180
    .local v0, "val":I
    return v0
.end method

.method public static getHomeWallpaperId(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 170
    if-nez p0, :cond_0

    .line 171
    const/4 v0, 0x0

    .line 174
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "home_wallpaper_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static getHumidityFirst()F
    .locals 3

    .prologue
    .line 491
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_minimal"

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getHumidityMaximal()F
    .locals 5

    .prologue
    const/high16 v4, 0x428c0000    # 70.0f

    const/high16 v3, 0x41f00000    # 30.0f

    .line 499
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "humidity_maximal"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 500
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 502
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_maximal"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public static getHumidityMinimal()F
    .locals 5

    .prologue
    const/high16 v4, 0x428c0000    # 70.0f

    const/high16 v3, 0x41f00000    # 30.0f

    .line 483
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "humidity_maximal"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 484
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 486
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_maximal"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public static getHumiditySecond()F
    .locals 3

    .prologue
    .line 507
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "humidity_maximal"

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getLastHumidity()F
    .locals 3

    .prologue
    .line 523
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "last_humidity"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getLastTemperature()F
    .locals 3

    .prologue
    .line 515
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "last_temperature"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getLastUpdated(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 558
    if-nez p0, :cond_0

    .line 559
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 561
    :goto_0
    return-wide v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static getNotificationValue(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 581
    if-nez p0, :cond_0

    .line 585
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "notification_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static getPreviousGoalSelectId()J
    .locals 4

    .prologue
    .line 1092
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_key_beat_my_previous_record_exid"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getPrivacyVerionCode(Landroid/content/Context;)F
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 819
    if-nez p0, :cond_0

    .line 823
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "privacy_version_code"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public static getRealTimeAudioGuideItemIndex()I
    .locals 3

    .prologue
    .line 1392
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_audio_guide_on"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealTimeGymGuidePopupWasShown()Z
    .locals 3

    .prologue
    .line 1396
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_gym_guide_popup_was_shown"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getRealTimeManualyMaxHR()I
    .locals 11

    .prologue
    .line 776
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 777
    .local v2, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 778
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 779
    .local v1, "birthDate":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getDay()I

    move-result v3

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v5

    add-int/lit16 v5, v5, 0x76c

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/firstbeat/utils/Utils;->getAgeValue(III)I

    move-result v0

    .line 780
    .local v0, "age":I
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v4, "manualy_max_hr"

    const-wide v5, 0x406a400000000000L    # 210.0

    const-wide v7, 0x3fe4cccccccccccdL    # 0.65

    int-to-double v9, v0

    mul-double/2addr v7, v9

    sub-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v3

    return v3
.end method

.method public static getRealtimeActivityGoalType(I)I
    .locals 3
    .param p0, "activityType"    # I

    .prologue
    .line 1152
    const/4 v0, 0x1

    .line 1153
    .local v0, "value":I
    packed-switch p0, :pswitch_data_0

    .line 1167
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1169
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.sensor.barometer"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1171
    const/4 v0, 0x1

    .line 1174
    :cond_0
    return v0

    .line 1155
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeRunningGoalType()I

    move-result v0

    .line 1156
    goto :goto_0

    .line 1158
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeWalkingGoalType()I

    move-result v0

    .line 1159
    goto :goto_0

    .line 1161
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeCyclingGoalType()I

    move-result v0

    .line 1162
    goto :goto_0

    .line 1164
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeHikingGoalType()I

    move-result v0

    goto :goto_0

    .line 1153
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getRealtimeActivityType()I
    .locals 4

    .prologue
    .line 1034
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_activity_type"

    const/16 v3, 0x4653

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1036
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getActivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    return v0
.end method

.method public static getRealtimeBeatMyPreviousRecordGoalValue()I
    .locals 4

    .prologue
    .line 1320
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_beat_my_previous_record_value"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1321
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "saveRealtimeBeatMyPreviousRecordGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1322
    return v0
.end method

.method private static getRealtimeCaloriesGoalValue()I
    .locals 4

    .prologue
    .line 1260
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_calories_value"

    const/16 v3, 0x12c

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1261
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCaloriesGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    return v0
.end method

.method public static getRealtimeCyclingGoalType()I
    .locals 4

    .prologue
    .line 1192
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_cycling_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1194
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    return v0
.end method

.method public static getRealtimeDispDataBackupTypeForHRM()I
    .locals 3

    .prologue
    .line 1354
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_disp_data_backup"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealtimeDispDataMap()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1375
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1376
    .local v0, "intAry":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1377
    sget-object v4, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pro_realtime_disp_data_map"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1378
    .local v2, "val":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1376
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1380
    .end local v2    # "val":I
    :cond_0
    return-object v0
.end method

.method public static getRealtimeDispDataStatus()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1346
    .local v0, "intAry":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1347
    sget-object v4, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pro_realtime_disp_data_status"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1348
    .local v2, "val":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1346
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1350
    .end local v2    # "val":I
    :cond_0
    return-object v0
.end method

.method private static getRealtimeDistanceGoalValue()I
    .locals 6

    .prologue
    .line 1236
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v3

    const-string v4, "km"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1237
    .local v1, "isUnitKm":Z
    if-eqz v1, :cond_0

    const/16 v0, 0x3e8

    .line 1238
    .local v0, "defaultValue":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v4, "pro_realtime_distance_value"

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1239
    .local v2, "val":I
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDistanceGoalValue : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    return v2

    .line 1237
    .end local v0    # "defaultValue":I
    .end local v2    # "val":I
    :cond_0
    const/16 v0, 0xa0

    goto :goto_0
.end method

.method public static getRealtimeExerciseId()J
    .locals 4

    .prologue
    .line 1429
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "pro_realtime_exercise_id"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getRealtimeFrequencyOfAudioGuide()I
    .locals 3

    .prologue
    .line 1437
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "pro_realtime_frequency_of_audio_guide"

    const/16 v2, 0x8

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealtimeGoalType()I
    .locals 4

    .prologue
    .line 1096
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_goal_type"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1097
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1098
    return v0
.end method

.method public static getRealtimeGoalValue()I
    .locals 2

    .prologue
    .line 1206
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 1207
    .local v0, "type":I
    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue(I)I

    move-result v1

    return v1
.end method

.method public static getRealtimeGoalValue(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 1211
    const/4 v0, -0x1

    .line 1212
    .local v0, "val":I
    packed-switch p0, :pswitch_data_0

    .line 1226
    .end local v0    # "val":I
    :goto_0
    return v0

    .line 1214
    .restart local v0    # "val":I
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeDistanceGoalValue()I

    move-result v0

    goto :goto_0

    .line 1216
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTimeGoalValue()I

    move-result v0

    goto :goto_0

    .line 1218
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeCaloriesGoalValue()I

    move-result v0

    goto :goto_0

    .line 1220
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingTimeGoalValue()I

    move-result v0

    goto :goto_0

    .line 1222
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeBeatMyPreviousRecordGoalValue()I

    move-result v0

    goto :goto_0

    .line 1212
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getRealtimeHikingGoalType()I
    .locals 4

    .prologue
    .line 1199
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_hiking_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1201
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    return v0
.end method

.method public static getRealtimeMapZoomLevel()F
    .locals 4

    .prologue
    .line 1416
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_map_zoom_level"

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 1417
    .local v0, "zoom":F
    return v0
.end method

.method public static getRealtimeMode()I
    .locals 3

    .prologue
    .line 1024
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getRealtimeRunningGoalType()I
    .locals 4

    .prologue
    .line 1178
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_running_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1180
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1181
    return v0
.end method

.method private static getRealtimeTimeGoalValue()I
    .locals 4

    .prologue
    .line 1249
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_time_value"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1250
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTimeGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    return v0
.end method

.method public static getRealtimeTrainingLevel()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1271
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_training_level"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1272
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrainingLevel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1273
    if-le v0, v4, :cond_0

    .line 1274
    const/4 v0, 0x2

    .line 1275
    :cond_0
    return v0
.end method

.method public static getRealtimeTrainingTimeGoalValue()I
    .locals 4

    .prologue
    .line 1326
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_training_time_value"

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1327
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRealtimeTrainingTimeGoalValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    return v0
.end method

.method public static getRealtimeTypeOfFeedBack()J
    .locals 4

    .prologue
    .line 1445
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "pro_realtime_type_of_feedback"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getRealtimeWalkingGoalType()I
    .locals 4

    .prologue
    .line 1185
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "pro_realtime_walking_goal_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1187
    .local v0, "val":I
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGoalType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    return v0
.end method

.method public static final getReminderNotificationCount(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 283
    if-nez p0, :cond_0

    .line 285
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "reminder_notification_count"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static getSpO2Availablilty(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 937
    if-nez p0, :cond_0

    .line 938
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    const-string v3, "getSpO2Availablilty context is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    :goto_0
    return v1

    .line 942
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.spo2"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 943
    .local v0, "isSpO2Supported":Z
    if-nez v0, :cond_1

    .line 944
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setSpO2Availablilty(Landroid/content/Context;Z)V

    .line 947
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v3, " spo2_device_type_available"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method public static getTGHAvailablilty(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 960
    if-nez p0, :cond_0

    .line 964
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v2, " tgh_device_type_available"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static getTemperatureFirst()F
    .locals 3

    .prologue
    .line 459
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_minimal"

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getTemperatureMaximal()F
    .locals 5

    .prologue
    const/high16 v4, 0x41d00000    # 26.0f

    const/high16 v3, 0x41a00000    # 20.0f

    .line 467
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "temperature_maximal"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 468
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 470
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_maximal"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public static getTemperatureMinimal()F
    .locals 5

    .prologue
    const/high16 v4, 0x41d00000    # 26.0f

    const/high16 v3, 0x41a00000    # 20.0f

    .line 451
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "temperature_maximal"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 452
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_minimal"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 454
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_maximal"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public static getTemperatureSecond()F
    .locals 3

    .prologue
    .line 475
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "temperature_maximal"

    const/high16 v2, 0x41d00000    # 26.0f

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getTermsOfUseVerionCode(Landroid/content/Context;)F
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 804
    if-nez p0, :cond_0

    .line 808
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "term_of_use_version_code"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public static getUVAvailablilty(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 895
    if-nez p0, :cond_0

    .line 899
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    const-string v2, " uv_device_type_available"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final getUnsupportedListJson(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 337
    if-nez p0, :cond_0

    .line 340
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "unsupported_feature_list"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final getUpgradeStatus(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 323
    if-nez p0, :cond_0

    .line 326
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "upgrade_status"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static getValue(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    sput-object p0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    .line 80
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "PREF_VERSION"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 82
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 83
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth_devicePref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    .line 85
    return-void
.end method

.method public static final isBackupBeforeRestoreChecked(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 2547
    if-nez p0, :cond_0

    .line 2550
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "backup_before_restoring"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 154
    if-nez p0, :cond_0

    .line 158
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EXPORT_DIALOG_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isInitializationNeeded(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 218
    if-nez p0, :cond_0

    .line 221
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "initialization_needed"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isLaterButtonClickedOnAppUpdatePopup(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 693
    if-nez p0, :cond_0

    .line 697
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "update_later_button_clicked"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isLaunchMobilePopupWasShown(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 656
    if-nez p0, :cond_0

    .line 660
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "launch_mobile_popup_was_shown"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isLaunchWifiPopupWasShown(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 671
    if-nez p0, :cond_0

    .line 675
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "launch_wifi_popup_was_shown"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isManualMobilePopupWasShown(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 393
    if-nez p0, :cond_0

    .line 396
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "manual_mobile_popup_was_shown"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isManualRoamingPopupWasShown(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 386
    if-nez p0, :cond_0

    .line 389
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "manual_roaming_popup_was_shown"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isModeComfortZoneInited()Z
    .locals 3

    .prologue
    .line 531
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "initialization_comfort_zone"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isNeedMaxHeartrateConfirm()Z
    .locals 3

    .prologue
    .line 1530
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_need_max_hr_confirm"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isRealTimeMaxHRAutoUpdate()Z
    .locals 3

    .prologue
    .line 772
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "max_hr_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static final isRestoreAgainHomePopup(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 138
    if-nez p0, :cond_0

    .line 142
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "restore_again_at_home"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isRestoreOngoing(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 309
    if-nez p0, :cond_0

    .line 310
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "restore_ongoing"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isSendProfileDisclose(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 365
    if-nez p0, :cond_0

    .line 368
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "send_profile_disclose"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isTermsAndConditionsOf355Accepted(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 232
    if-nez p0, :cond_0

    .line 235
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v2, "is_upgrade_from_35x"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isUsePedometerRanking(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 351
    if-nez p0, :cond_0

    .line 354
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v2, "use_pedometer_ranking"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static isWalkMateSensorListenerRegistered()Z
    .locals 3

    .prologue
    .line 1490
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_walkmate_sensor_register_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static loadUnitsFromSharedPreferences()V
    .locals 1

    .prologue
    .line 1466
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-eqz v0, :cond_5

    .line 1468
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoHeightUnit:Ljava/lang/String;

    .line 1470
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getWeightUnit()Ljava/lang/String;

    move-result-object v0

    :goto_1
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoWeightUnit:Ljava/lang/String;

    .line 1472
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    :goto_2
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoTemperatureUnit:Ljava/lang/String;

    .line 1474
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v0

    :goto_3
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    .line 1476
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getGlucoseUnit()Ljava/lang/String;

    move-result-object v0

    :goto_4
    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoGlucoseUnit:Ljava/lang/String;

    .line 1487
    :goto_5
    return-void

    .line 1468
    :cond_0
    const-string v0, "inch"

    goto :goto_0

    .line 1470
    :cond_1
    const-string v0, "lb"

    goto :goto_1

    .line 1472
    :cond_2
    const-string v0, "F"

    goto :goto_2

    .line 1474
    :cond_3
    const-string v0, "km"

    goto :goto_3

    .line 1476
    :cond_4
    const-string/jumbo v0, "mg/dL"

    goto :goto_4

    .line 1481
    :cond_5
    const-string v0, "inch"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoHeightUnit:Ljava/lang/String;

    .line 1482
    const-string v0, "lb"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoWeightUnit:Ljava/lang/String;

    .line 1483
    const-string v0, "F"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoTemperatureUnit:Ljava/lang/String;

    .line 1484
    const-string v0, "km"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    .line 1485
    const-string/jumbo v0, "mg/dL"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoGlucoseUnit:Ljava/lang/String;

    goto :goto_5
.end method

.method public static saveBackupRealtimeGoalType(II)V
    .locals 3
    .param p0, "type"    # I
    .param p1, "val"    # I

    .prologue
    .line 1289
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveBackupRealtimeGoalType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1290
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_backup_realtime_goal_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1291
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_backup_realtime_goal_value"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1292
    return-void
.end method

.method public static saveChartFlag(Ljava/lang/String;)V
    .locals 2
    .param p0, "val"    # Ljava/lang/String;

    .prologue
    .line 1513
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_chartFlagButton"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1514
    return-void
.end method

.method public static saveCoachingSetgoalSelect(I)V
    .locals 2
    .param p0, "select_id"    # I

    .prologue
    .line 1494
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pro_coaching_setgoal_select"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1495
    return-void
.end method

.method public static saveDeleteDashboardItem(Ljava/lang/String;J)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # J

    .prologue
    .line 188
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method public static saveEquipmentType(I)V
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 1041
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveActivityType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_equipment_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1044
    return-void
.end method

.method public static saveHomeWallpaperId(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "wallpaperId"    # I

    .prologue
    .line 162
    if-nez p0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 166
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "home_wallpaper_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static saveRealtimeActivityGoalType(II)V
    .locals 3
    .param p0, "activityType"    # I
    .param p1, "typeValue"    # I

    .prologue
    .line 1133
    packed-switch p0, :pswitch_data_0

    .line 1136
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_running_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1146
    :goto_0
    return-void

    .line 1139
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_walking_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0

    .line 1142
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_cycling_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0

    .line 1145
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_hiking_goal_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0

    .line 1133
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static saveRealtimeActivityType(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1029
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveActivityType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1030
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_activity_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1031
    return-void
.end method

.method public static saveRealtimeBeatMyPreviousRecordGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1284
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveRealtimeBeatMyPreviousRecordGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1285
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_beat_my_previous_record_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1286
    return-void
.end method

.method private static saveRealtimeCaloriesGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1255
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveCaloriesGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1256
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_calories_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1257
    return-void
.end method

.method public static saveRealtimeDispDataBackupTypeForHRM(I)V
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 1358
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_disp_data_backup"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1359
    return-void
.end method

.method public static saveRealtimeDispDataMap(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1362
    .local p0, "val":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez p0, :cond_1

    .line 1363
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "saveDispDataMap Failed! length not 2 : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p0, :cond_0

    const-string/jumbo v2, "null"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1372
    :goto_1
    return-void

    .line 1363
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 1366
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "saveDispDataMap : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1368
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_2
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_MAP:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1369
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pro_realtime_disp_data_map"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1368
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1371
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method

.method public static saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1332
    .local p0, "val":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->DEFAULT_DISP_DATA_STATUS:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 1333
    :cond_0
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "saveDispDataStatus Failed! length not 4 : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p0, :cond_1

    const-string/jumbo v2, "null"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    :goto_1
    return-void

    .line 1333
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 1336
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "saveDispDataStatus : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1337
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1338
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_2
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1339
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pro_realtime_disp_data_status"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1338
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1341
    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method

.method private static saveRealtimeDistanceGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1230
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDistanceGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1231
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_distance_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1232
    return-void
.end method

.method public static saveRealtimeGoalType(II)V
    .locals 3
    .param p0, "type"    # I
    .param p1, "val"    # I

    .prologue
    .line 1053
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveGoalType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1054
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_goal_type"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1055
    packed-switch p0, :pswitch_data_0

    .line 1075
    :goto_0
    return-void

    .line 1057
    :pswitch_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDistanceGoalValue(I)V

    goto :goto_0

    .line 1060
    :pswitch_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeTimeGoalValue(I)V

    goto :goto_0

    .line 1063
    :pswitch_2
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeCaloriesGoalValue(I)V

    goto :goto_0

    .line 1066
    :pswitch_3
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeTrainingTimeGoalValue(I)V

    goto :goto_0

    .line 1069
    :pswitch_4
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeBeatMyPreviousRecordGoalValue(I)V

    goto :goto_0

    .line 1055
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static saveRealtimeMapZoomLevel(F)V
    .locals 3
    .param p0, "zoom"    # F

    .prologue
    .line 1412
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_map_zoom_level"

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1413
    return-void
.end method

.method public static saveRealtimeMode(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1019
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_mode"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1021
    return-void
.end method

.method private static saveRealtimeTimeGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1244
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveTimeGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_time_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1246
    return-void
.end method

.method public static saveRealtimeTrainingLevel(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1266
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveTrainingLevel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1267
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_training_level"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1268
    return-void
.end method

.method public static saveRealtimeTrainingTimeGoalValue(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 1279
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveRealtimeTrainingTimeGoalValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_training_time_value"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1281
    return-void
.end method

.method public static saveUiLockState(Z)V
    .locals 2
    .param p0, "isLock"    # Z

    .prologue
    .line 1522
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pro_ui_lock_state"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1523
    return-void
.end method

.method public static saveUnsupportedListJson(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "featureListJsonString"    # Ljava/lang/String;

    .prologue
    .line 330
    if-nez p0, :cond_0

    .line 334
    :goto_0
    return-void

    .line 333
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "unsupported_feature_list"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setAntAvailablilty(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 861
    if-nez p0, :cond_0

    .line 865
    :goto_0
    return-void

    .line 864
    :cond_0
    const-string v0, "ant_device_type_available"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setAutoBackupInterval(Landroid/content/Context;J)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "interval"    # J

    .prologue
    .line 239
    if-nez p0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 241
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "auto_backup_interval"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setAutoBackupSwitchStatus(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isEnable"    # Z

    .prologue
    .line 251
    if-nez p0, :cond_0

    .line 254
    :goto_0
    return-void

    .line 253
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "auto_backup_switch_status"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setAutoBackupWifiEnabled(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enable"    # Z

    .prologue
    .line 290
    if-nez p0, :cond_0

    .line 293
    :goto_0
    return-void

    .line 292
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "auto_backup_wifi_enable"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setBackupBeforeRestoreValue(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 2540
    if-nez p0, :cond_0

    .line 2544
    :goto_0
    return-void

    .line 2543
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "backup_before_restoring"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static final setCignaRestoreTriggerPopup(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 114
    if-nez p0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 118
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "coach_restore_trigger"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setCurrentSamsungAccountSyncedTime(Landroid/content/Context;J)V
    .locals 3
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    .line 400
    if-nez p0, :cond_0

    .line 404
    :goto_0
    return-void

    .line 403
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "samsungaccount_last_synced_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setDeviceTypeChecked(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 842
    if-nez p0, :cond_0

    .line 843
    const-string v0, "Device Type Shared Preferences"

    const-string v1, "Device Type Shared Preferences - setDeviceTypeChecked - Context is null "

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    :goto_0
    return-void

    .line 846
    :cond_0
    const-string v0, "Device Type Shared Preferences"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device Type Shared Preferences - setDeviceTypeChecked - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    const-string v0, "device_type_available_checked"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static final setExportDialogFeatureShown(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "flag"    # Z

    .prologue
    .line 146
    if-nez p0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 150
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EXPORT_DIALOG_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setFirstActivity(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 535
    if-nez p0, :cond_0

    .line 540
    :goto_0
    return-void

    .line 537
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 539
    .local v0, "time":J
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v3, "first_activity"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setFirstbeatAscrActivated(Z)V
    .locals 3
    .param p0, "activated"    # Z

    .prologue
    .line 1425
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_firstbeat_ascr_activated"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1426
    return-void
.end method

.method public static setFirstbeatTrainingGoal(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 718
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "firstbeat_training_goal"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 719
    return-void
.end method

.method public static setGrapthSelectedTab(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p0, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 2554
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pedometer_selected_tab"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2555
    return-void
.end method

.method public static setHRAvailablilty(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 887
    if-nez p0, :cond_0

    .line 891
    :goto_0
    return-void

    .line 890
    :cond_0
    const-string v0, " hr_device_type_available"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setHomePageMark(I)V
    .locals 3
    .param p0, "markcount"    # I

    .prologue
    .line 184
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "home_page_mark"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 185
    return-void
.end method

.method public static setHumidityMaximal(F)V
    .locals 2
    .param p0, "humidityMaximal"    # F

    .prologue
    .line 495
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "humidity_maximal"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 496
    return-void
.end method

.method public static setHumidityMinimal(F)V
    .locals 2
    .param p0, "HumidityMinimal"    # F

    .prologue
    .line 479
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "humidity_minimal"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 480
    return-void
.end method

.method public static setInitializationNeeded(Landroid/content/Context;Z)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 198
    if-nez p0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 202
    :cond_0
    if-eqz p1, :cond_2

    .line 203
    .local v0, "converted":I
    :goto_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "shealth_profile_initialized"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 204
    const-string v3, "SHMSD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SPI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    if-ne p1, v2, :cond_1

    .line 207
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "shealth_pedometer_status_running"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 208
    const-string v2, "SHMSD"

    const-string v3, "SPSR : 0"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :cond_1
    :goto_2
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v3, "initialization_needed"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0

    .end local v0    # "converted":I
    :cond_2
    move v0, v2

    .line 202
    goto :goto_1

    .line 210
    .restart local v0    # "converted":I
    :catch_0
    move-exception v1

    .line 211
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public static setIsLaterButtonClickedOnAppUpdatePopup(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 686
    if-nez p0, :cond_0

    .line 690
    :goto_0
    return-void

    .line 689
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "update_later_button_clicked"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setIsModeComfortZoneInited(Z)V
    .locals 2
    .param p0, "isInited"    # Z

    .prologue
    .line 527
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "initialization_comfort_zone"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 528
    return-void
.end method

.method public static setIsRealTimeMaxHRAutoUpdate(Z)V
    .locals 10
    .param p0, "val"    # Z

    .prologue
    const/4 v6, 0x0

    .line 755
    sget-object v4, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v5, "max_hr_mode"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 756
    .local v2, "old_val":Z
    if-eq v2, p0, :cond_0

    .line 757
    if-eqz v2, :cond_1

    .line 758
    invoke-static {v6}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->setRealTimeMaxHR(I)V

    .line 768
    :cond_0
    :goto_0
    sget-object v4, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v5, "max_hr_mode"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 769
    return-void

    .line 760
    :cond_1
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    sget-object v4, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 761
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 763
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 764
    .local v1, "birthDate":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getDay()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/Date;->getMonth()I

    move-result v5

    invoke-virtual {v1}, Ljava/util/Date;->getYear()I

    move-result v6

    add-int/lit16 v6, v6, 0x76c

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/firstbeat/utils/Utils;->getAgeValue(III)I

    move-result v0

    .line 765
    .local v0, "age":I
    const-wide v4, 0x406a400000000000L    # 210.0

    const-wide v6, 0x3fe4cccccccccccdL    # 0.65

    int-to-double v8, v0

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeManualyMaxHR(I)V

    goto :goto_0
.end method

.method public static setIsSendProfileDisclose(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 358
    if-nez p0, :cond_0

    .line 362
    :goto_0
    return-void

    .line 361
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "send_profile_disclose"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setIsUsePedometerRanking(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 344
    if-nez p0, :cond_0

    .line 348
    :goto_0
    return-void

    .line 347
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "use_pedometer_ranking"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setLastHumidity(F)V
    .locals 2
    .param p0, "lastHumidity"    # F

    .prologue
    .line 519
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_humidity"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 520
    return-void
.end method

.method public static setLastTemperature(F)V
    .locals 2
    .param p0, "lastTemperature"    # F

    .prologue
    .line 511
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_temperature"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 512
    return-void
.end method

.method public static setLastUpdated(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 550
    if-nez p0, :cond_0

    .line 555
    :goto_0
    return-void

    .line 552
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 554
    .local v0, "time":J
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v3, "last_updated"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setLaunchMobilePopupWasShown(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 664
    if-nez p0, :cond_0

    .line 668
    :goto_0
    return-void

    .line 667
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "launch_mobile_popup_was_shown"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setLaunchWifiPopupWasShown(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 679
    if-nez p0, :cond_0

    .line 683
    :goto_0
    return-void

    .line 682
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "launch_wifi_popup_was_shown"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setManualMobilePopupWasShown(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 372
    if-nez p0, :cond_0

    .line 376
    :goto_0
    return-void

    .line 375
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "manual_mobile_popup_was_shown"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setManualRoamingPopupWasShown(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 379
    if-nez p0, :cond_0

    .line 383
    :goto_0
    return-void

    .line 382
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "manual_roaming_popup_was_shown"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setNeedMaxHeartrateConfirm(Z)V
    .locals 3
    .param p0, "isNeed"    # Z

    .prologue
    .line 1534
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_need_max_hr_confirm"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1535
    return-void
.end method

.method public static setNotiAchievementEnabled(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 604
    if-nez p0, :cond_0

    .line 608
    :goto_0
    return-void

    .line 607
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "noti_achievement_enabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setNotificationEnabled(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 589
    if-nez p0, :cond_0

    .line 593
    :goto_0
    return-void

    .line 592
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "notification_enabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setPedoAvailablilty(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 978
    if-nez p0, :cond_0

    .line 982
    :goto_0
    return-void

    .line 981
    :cond_0
    const-string/jumbo v0, "pedo_device_type_available"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setPrivacyVersionCode(Landroid/content/Context;F)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "privacyVersionCode"    # F

    .prologue
    .line 812
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 816
    :cond_0
    :goto_0
    return-void

    .line 815
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "privacy_version_code"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setRealTimeAudioGuideIndex(I)V
    .locals 3
    .param p0, "index"    # I

    .prologue
    .line 1386
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_audio_guide_on"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1387
    return-void
.end method

.method public static setRealTimeGymGuidePopupWasShown(Z)V
    .locals 3
    .param p0, "guidePopupWasShown"    # Z

    .prologue
    .line 1400
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "pro_realtime_gym_guide_popup_was_shown"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1401
    return-void
.end method

.method public static setRealTimeManualyMaxHR(I)V
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 793
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "manualy_max_hr"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 794
    return-void
.end method

.method public static setRealtimeExerciseId(J)V
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 1433
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pro_realtime_exercise_id"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1434
    return-void
.end method

.method public static setReminderNotificationCount(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "count"    # I

    .prologue
    .line 277
    if-nez p0, :cond_0

    .line 280
    :goto_0
    return-void

    .line 279
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "reminder_notification_count"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static final setRestoreAgainHomePopup(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 130
    if-nez p0, :cond_0

    .line 135
    :goto_0
    return-void

    .line 134
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "restore_again_at_home"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setRestoreOngoing(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 302
    if-nez p0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 305
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "restore_ongoing"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setSpO2Availablilty(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 929
    if-nez p0, :cond_0

    .line 933
    :goto_0
    return-void

    .line 932
    :cond_0
    const-string v0, " spo2_device_type_available"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setTGHAvailablilty(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 952
    if-nez p0, :cond_0

    .line 956
    :goto_0
    return-void

    .line 955
    :cond_0
    const-string v0, " tgh_device_type_available"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setTemperatureMaximal(F)V
    .locals 2
    .param p0, "temperatureMaximal"    # F

    .prologue
    .line 463
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "temperature_maximal"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 464
    return-void
.end method

.method public static setTemperatureMinimal(F)V
    .locals 2
    .param p0, "temperatureMinimal"    # F

    .prologue
    .line 447
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "temperature_minimal"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 448
    return-void
.end method

.method public static final setTermsAndConditionsOf355Accepted(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Z

    .prologue
    .line 225
    if-nez p0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 228
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string v1, "is_upgrade_from_35x"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setTermsOfUseVersionCode(Landroid/content/Context;F)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "termsOfUseVersionCode"    # F

    .prologue
    .line 797
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 801
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "term_of_use_version_code"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setUVAvailablilty(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 904
    if-nez p0, :cond_0

    .line 908
    :goto_0
    return-void

    .line 907
    :cond_0
    const-string v0, " uv_device_type_available"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setUpgradeStatus(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # I

    .prologue
    .line 316
    if-nez p0, :cond_0

    .line 320
    :goto_0
    return-void

    .line 319
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    const-string/jumbo v1, "upgrade_status"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 89
    return-void
.end method

.method private static setValueInternal(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 985
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->OBJ_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 986
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->mDevicePref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 987
    .local v0, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    instance-of v1, p1, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 988
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 998
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 999
    monitor-exit v2

    .line 1000
    return-void

    .line 989
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 990
    check-cast p1, Ljava/lang/Float;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 999
    .end local v0    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 991
    .restart local v0    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_2
    :try_start_1
    instance-of v1, p1, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 992
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 993
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_3
    instance-of v1, p1, Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 994
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0, p0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 995
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_4
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 996
    check-cast p1, Ljava/lang/String;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
