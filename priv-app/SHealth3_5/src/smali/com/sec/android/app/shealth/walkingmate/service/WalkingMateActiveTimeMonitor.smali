.class public Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
.super Ljava/lang/Object;
.source "WalkingMateActiveTimeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$1;,
        Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;,
        Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;,
        Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final MONITORACTION:Ljava/lang/String; = "com.sec.android.app.shealth.walkingmate.walkingmateactivetimemonitor.COMMAND"

.field private static TAG:Ljava/lang/String;

.field private static mEnableSContextInactiveTimer:Z

.field private static mInstance:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private callbackList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDuration:J

.field public mHealthyStepStart:Z

.field private mInactiveDurationSetting:J

.field public mInactiveTimeStart:Z

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mSettingDate:Ljava/util/Date;

.field private mWalkingBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "WalkingMateActiveTimeMonitor"

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->sLock:Ljava/lang/Object;

    .line 159
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mEnableSContextInactiveTimer:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v1, "WalkingMateActiveMonitor created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mContext:Landroid/content/Context;

    .line 68
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    .line 70
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    .line 71
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mSettingDate:Ljava/util/Date;

    .line 73
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mIntentFilter:Landroid/content/IntentFilter;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.shealth.walkingmate.walkingmateactivetimemonitor.COMMAND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mWalkingBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mWalkingBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->updateStatus()V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mSettingDate:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->refresh(Ljava/util/Date;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;

    .line 81
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    .param p1, "x1"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveDurationSetting:J

    return-wide v0
.end method

.method private deleteCallbackList(Ljava/util/Vector;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327
    .local p1, "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deletelist size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "callback list size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 331
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 333
    .local v2, "tempList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/ref/WeakReference<Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 334
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 335
    .local v1, "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    if-eqz v1, :cond_0

    .line 336
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 339
    .end local v1    # "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    .line 342
    .end local v0    # "i":I
    .end local v2    # "tempList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/ref/WeakReference<Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;>;>;"
    :cond_2
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInstance:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    if-nez v0, :cond_0

    .line 151
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v2, "Active Monitor Created"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInstance:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    .line 154
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInstance:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setInactiveMonitor(IIII)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "startTime"    # I
    .param p3, "endTime"    # I
    .param p4, "callType"    # I

    .prologue
    const/4 v3, 0x2

    .line 223
    const/4 v0, 0x2

    .line 224
    .local v0, "DEFAULT_ALERT_COUNT":I
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v2, "Inactive register command call"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    if-ne p4, v3, :cond_0

    .line 226
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->stopInactiveTimer()V

    .line 228
    :cond_0
    const/4 v1, 0x1

    if-ne p4, v1, :cond_1

    .line 229
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->checkInactiveTimer()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    :goto_0
    return-void

    .line 234
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v1

    invoke-virtual {v1, p1, v3, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->startInactiveTimer(IIII)V

    goto :goto_0
.end method

.method private updateStatus()V
    .locals 1

    .prologue
    .line 547
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendCurrentStatus()V

    .line 548
    return-void
.end method


# virtual methods
.method public addCallBacks(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;)V
    .locals 3
    .param p1, "callback"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .prologue
    .line 296
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v1, "callback is added"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 298
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "total callback number is = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    return-void
.end method

.method public callHealthyStepCallback(J)V
    .locals 7
    .param p1, "count"    # J

    .prologue
    .line 375
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->sLock:Ljava/lang/Object;

    monitor-enter v5

    .line 376
    :try_start_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 377
    .local v0, "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 378
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 379
    .local v3, "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    if-nez v3, :cond_0

    .line 380
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 377
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 382
    :cond_0
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v6, "callHealthyStepCallback called"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    :try_start_1
    invoke-interface {v3, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;->onHealthyStepCountChanged(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 385
    :catch_0
    move-exception v1

    .line 386
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 387
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 392
    .end local v0    # "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "i":I
    .end local v3    # "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 391
    .restart local v0    # "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .restart local v2    # "i":I
    :cond_1
    :try_start_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->deleteCallbackList(Ljava/util/Vector;)V

    .line 392
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 393
    return-void
.end method

.method public callHealthyStepChanged(Z)V
    .locals 7
    .param p1, "status"    # Z

    .prologue
    .line 350
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->sLock:Ljava/lang/Object;

    monitor-enter v5

    .line 351
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    if-nez v4, :cond_0

    .line 352
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    .line 354
    :cond_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 355
    .local v0, "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 356
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 357
    .local v3, "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    if-nez v3, :cond_1

    .line 358
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 355
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 360
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v6, "callHealthyStepChanged called"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 362
    :try_start_1
    invoke-interface {v3, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;->onHealthyStepStatusChanged(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 363
    :catch_0
    move-exception v1

    .line 364
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 365
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 370
    .end local v0    # "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "i":I
    .end local v3    # "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 369
    .restart local v0    # "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .restart local v2    # "i":I
    :cond_2
    :try_start_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->deleteCallbackList(Ljava/util/Vector;)V

    .line 370
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 371
    return-void
.end method

.method public callInactiveTime(ZJZ)V
    .locals 7
    .param p1, "inactive"    # Z
    .param p2, "duration"    # J
    .param p4, "isNoti"    # Z

    .prologue
    .line 397
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->sLock:Ljava/lang/Object;

    monitor-enter v5

    .line 398
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    if-nez v4, :cond_0

    .line 399
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    .line 401
    :cond_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 402
    .local v0, "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 403
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 404
    .local v3, "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    if-nez v3, :cond_1

    .line 405
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 402
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 407
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v6, "callInactiveTime called"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    :try_start_1
    invoke-interface {v3, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;->onInactiveTimeChanged(ZJZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 410
    :catch_0
    move-exception v1

    .line 411
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 412
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 417
    .end local v0    # "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "i":I
    .end local v3    # "myCallback":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 416
    .restart local v0    # "deleteList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .restart local v2    # "i":I
    :cond_2
    :try_start_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->deleteCallbackList(Ljava/util/Vector;)V

    .line 417
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 418
    return-void
.end method

.method public deleteListbyTime([J)V
    .locals 12
    .param p1, "deleted_time"    # [J

    .prologue
    .line 526
    const-string v8, "DELETELIST"

    const-string v9, "Delete walking_info_extended"

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    .line 529
    .local v1, "deviceType":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 530
    .local v4, "selection":Ljava/lang/StringBuilder;
    const-string/jumbo v8, "start_time >= ? "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    const-string v8, " AND "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 532
    const-string/jumbo v8, "start_time <= ? "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 533
    const-string v8, " AND "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 534
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    move-object v0, p1

    .local v0, "arr$":[J
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-wide v6, v0, v2

    .line 537
    .local v6, "time":J
    const/4 v8, 0x2

    new-array v5, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    .line 541
    .local v5, "selectionArgs":[Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 536
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 544
    .end local v5    # "selectionArgs":[Ljava/lang/String;
    .end local v6    # "time":J
    :cond_0
    return-void
.end method

.method public getInactiveTime()J
    .locals 2

    .prologue
    .line 429
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J

    return-wide v0
.end method

.method public getSContextData(Ljava/util/Date;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mSettingDate:Ljava/util/Date;

    .line 434
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->refresh(Ljava/util/Date;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;

    move-result-object v0

    return-object v0
.end method

.method public isHealthyStep()Z
    .locals 1

    .prologue
    .line 421
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    return v0
.end method

.method public isInactivetime()Z
    .locals 1

    .prologue
    .line 425
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    return v0
.end method

.method public refresh(Ljava/util/Date;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;
    .locals 16
    .param p1, "selectedDate"    # Ljava/util/Date;

    .prologue
    .line 455
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyManager WalkingMateActiveTimeMonitor ======= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;-><init>()V

    .line 458
    .local v11, "data":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;
    const-wide/16 v0, 0x0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->healthy_steps:J

    .line 459
    const-wide/16 v0, 0x0

    iput-wide v0, v11, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->active_time:J

    .line 522
    :goto_0
    return-object v11

    .line 464
    .end local v11    # "data":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;
    :cond_0
    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    .line 465
    .local v12, "date":Ljava/util/Date;
    invoke-virtual/range {p1 .. p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {v12, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 466
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-virtual/range {p1 .. p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-virtual {v12, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 468
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "end time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v13

    .line 472
    .local v13, "deviceType":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 473
    .local v15, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    const-string v0, " "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    const-string v0, "SUM ( power_step ) AS power_step"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 476
    const-string v0, " , "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    const-string v0, "SUM ( active_time ) AS active_time"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    const-string v0, " FROM "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    const-string/jumbo v0, "walk_info_extended"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    const-string v0, " WHERE "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 482
    const-string v0, " AND "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    const-string v0, " AND "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 485
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time <= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    const-string v0, "PEDOCHECK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "query = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    const-wide/16 v8, -0x1

    .line 490
    .local v8, "accmulation_healthyStep":J
    const-wide/16 v6, -0x1

    .line 492
    .local v6, "accmulation_activetime":J
    const/4 v10, 0x0

    .line 494
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 496
    if-eqz v10, :cond_1

    .line 499
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursor count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 501
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "selected date have no data (healthy step, active time)"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    :cond_1
    :goto_1
    if-eqz v10, :cond_2

    .line 515
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 519
    :cond_2
    :goto_2
    new-instance v11, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;-><init>()V

    .line 520
    .restart local v11    # "data":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;
    iput-wide v8, v11, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->healthy_steps:J

    .line 521
    iput-wide v6, v11, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;->active_time:J

    goto/16 :goto_0

    .line 503
    .end local v11    # "data":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$SContextActiveData;
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 504
    const-string/jumbo v0, "power_step"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 505
    const-string v0, "active_time"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 506
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "powersetp from db = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "activate time from db = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 510
    :catch_0
    move-exception v14

    .line 511
    .local v14, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v1, "CURSOR ERROR in WalkingMateActiveTime"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 514
    if-eqz v10, :cond_2

    .line 515
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 514
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_4

    .line 515
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public removeCallbacks(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;)V
    .locals 5
    .param p1, "callback"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .prologue
    .line 306
    if-nez p1, :cond_0

    .line 319
    :goto_0
    return-void

    .line 309
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v3, "callback being removed"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 311
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;>;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 312
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 313
    .local v0, "callback2":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 314
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 318
    .end local v0    # "callback2":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "total callback number is = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callbackList:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setActiveTime()V
    .locals 1

    .prologue
    .line 277
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->startActiveTime()V

    .line 280
    :cond_0
    return-void
.end method

.method public setHealthyStep()V
    .locals 1

    .prologue
    .line 256
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    .line 258
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->startHealthyStep()V

    .line 260
    :cond_0
    return-void
.end method

.method public setInactiveMonitor(I)V
    .locals 7
    .param p1, "type"    # I

    .prologue
    const/4 v6, 0x1

    .line 166
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "type ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 167
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mEnableSContextInactiveTimer ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mEnableSContextInactiveTimer:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 170
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    const/16 v4, 0x2719

    if-eq v3, v4, :cond_0

    .line 171
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v4, "inactive timer cannot start because it is not phone device"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 177
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v4, "inactive timer cannot start before initialization"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 181
    :cond_1
    if-ne p1, v6, :cond_2

    sget-boolean v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mEnableSContextInactiveTimer:Z

    if-ne v3, v6, :cond_2

    .line 182
    const-string v3, "SKIP setInactiveMonitor, since it is already activated"

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto :goto_0

    .line 186
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 187
    const-string v3, "checkInactiveTime"

    const-string v4, "case 1"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v2

    .line 191
    .local v2, "duration":I
    const/16 v1, 0x5dc

    .line 192
    .local v1, "SContextStartTime":I
    const/16 v0, 0x5dc

    .line 194
    .local v0, "SContextEndTime":I
    const-string v3, "checkInactiveTime"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "duration = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    mul-int/lit8 v2, v2, 0x3c

    .line 197
    int-to-long v3, v2

    iput-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveDurationSetting:J

    .line 199
    const-string v3, "checkInactiveTime"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UTCtimeFrom = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-string v3, "checkInactiveTime"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UTCtimeTo = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const-string v3, "checkInactiveTime"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "duration = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-direct {p0, v2, v1, v0, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(IIII)V

    .line 204
    sput-boolean v6, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mEnableSContextInactiveTimer:Z

    goto/16 :goto_0

    .line 207
    .end local v0    # "SContextEndTime":I
    .end local v1    # "SContextStartTime":I
    .end local v2    # "duration":I
    :cond_3
    const-string v3, "checkInactiveTime"

    const-string v4, "case 2"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->unsetInactiveMonitor()V

    .line 209
    const/4 v3, 0x0

    sput-boolean v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mEnableSContextInactiveTimer:Z

    goto/16 :goto_0
.end method

.method public unsetActiveTime()V
    .locals 1

    .prologue
    .line 287
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->stopActiveTime()V

    .line 288
    return-void
.end method

.method public unsetHealthyStep()V
    .locals 1

    .prologue
    .line 267
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->stopHealthyStep()V

    .line 268
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    .line 269
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callHealthyStepChanged(Z)V

    .line 270
    return-void
.end method

.method public unsetInactiveMonitor()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;

    const-string v1, "Inactive unregister command call"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->stopInactiveTimer()V

    .line 243
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mEnableSContextInactiveTimer:Z

    .line 244
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    if-nez v0, :cond_0

    .line 245
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    .line 246
    iput-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J

    .line 247
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    invoke-virtual {p0, v0, v3, v4, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callInactiveTime(ZJZ)V

    .line 249
    :cond_0
    return-void
.end method
