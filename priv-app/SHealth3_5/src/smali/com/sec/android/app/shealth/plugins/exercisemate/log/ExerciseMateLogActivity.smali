.class public Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "ExerciseMateLogActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# static fields
.field public static final MODE_DAY_GOAL:Ljava/lang/String; = "DayGoal"

.field public static final MODE_DAY_NOTGOAL:Ljava/lang/String; = "DayNotGoal"

.field public static final MODE_DB_ALL:Ljava/lang/String; = "0==0"

.field public static final REQUEST_FOR_EXERCISE_DETAILS:I = 0x2711

.field public static SELECTED_FILTER_INDEX:I


# instance fields
.field protected adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

.field cal:Ljava/util/Calendar;

.field dateFormatter:Ljava/text/DateFormat;

.field private mCursorCount:Z

.field private s_intent_timeFilter:Landroid/content/IntentFilter;

.field sharingdateFormatter:Ljava/text/DateFormat;

.field private final timeChangedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 51
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy hh:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->dateFormatter:Ljava/text/DateFormat;

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getSystemTimeDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->sharingdateFormatter:Ljava/text/DateFormat;

    .line 53
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->cal:Ljava/util/Calendar;

    .line 72
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->s_intent_timeFilter:Landroid/content/IntentFilter;

    .line 516
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->timeChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 78
    return-void
.end method

.method private executeQuery(Ljava/lang/CharSequence;)Z
    .locals 20
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 118
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshGoalInfo(Landroid/content/Context;)V

    .line 119
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshExerciseInfo(Landroid/content/Context;)V

    .line 121
    const-string v2, "0==0"

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    const-string/jumbo v5, "select exercise.[_id], exercise.[start_time], round(sum(exercise.[total_calorie])/count(distinct(strftime(\'%d-%m-%Y\',(exercise.[start_time]/1000), \'unixepoch\',\'localtime\')))) AS AVG_KCAL,count(distinct(strftime(\'%d-%m-%Y\',(exercise.[start_time]/1000), \'unixepoch\',\'localtime\'))) as COUNT,SUM(round(total_calorie)) AS SUM_KCAL from exercise where exercise.[exercise_info__id]!=18001 group by strftime(\'%m-%Y\',(exercise.[start_time]/1000), \'unixepoch\',\'localtime\') ORDER BY start_time DESC"

    .line 127
    .local v5, "selectionClause":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursor:Landroid/database/Cursor;

    .line 176
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursorCount:Z

    .line 178
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->invalidateOptionsMenu()V

    .line 179
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursorCount:Z

    return v2

    .line 130
    .end local v5    # "selectionClause":Ljava/lang/String;
    :cond_1
    const/4 v10, 0x0

    .line 131
    .local v10, "cursor":Landroid/database/Cursor;
    const-wide/16 v14, -0x1

    .line 132
    .local v14, "startOfMonth":J
    const-wide/16 v12, -0x1

    .line 133
    .local v12, "startOfDay":J
    const/4 v11, 0x0

    .line 134
    .local v11, "dayTotalCalorie":I
    const/4 v8, 0x0

    .line 138
    .local v8, "c":Landroid/database/MatrixCursor;
    :try_start_0
    const-string/jumbo v5, "select _id, start_time from exercise where end_time > 0 and exercise_type != 20003 group by (strftime(\'%d-%m-%Y\',(start_time/1000),\'unixepoch\',\'localtime\')) order by start_time desc"

    .line 143
    .restart local v5    # "selectionClause":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 145
    if-eqz v10, :cond_7

    .line 147
    new-instance v9, Landroid/database/MatrixCursor;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "SUM_KCAL"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "start_time"

    aput-object v4, v2, v3

    invoke-direct {v9, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 149
    .end local v8    # "c":Landroid/database/MatrixCursor;
    .local v9, "c":Landroid/database/MatrixCursor;
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_6

    .line 150
    const-string/jumbo v2, "start_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 151
    .local v16, "startTime":J
    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v2, v12, v2

    if-eqz v2, :cond_4

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v2

    cmp-long v2, v14, v2

    if-eqz v2, :cond_4

    .line 152
    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v6

    invoke-static {v2, v3, v6, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getMergedCalorieGetEarlier(JJ)I

    move-result v11

    .line 153
    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v12

    .line 154
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v11}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isGoalAchived(JI)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v11}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isGoalAchived(JI)Z

    move-result v2

    if-nez v2, :cond_4

    .line 156
    :cond_3
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "_id"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v6

    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v6, v7, v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getMergedCalorieGetEarlier(JJ)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "start_time"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v9, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 161
    invoke-static/range {v16 .. v17}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v14

    .line 149
    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 168
    .end local v16    # "startTime":J
    :catchall_0
    move-exception v2

    move-object v8, v9

    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v9    # "c":Landroid/database/MatrixCursor;
    .restart local v8    # "c":Landroid/database/MatrixCursor;
    :goto_3
    if-eqz v10, :cond_5

    .line 169
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2

    .end local v8    # "c":Landroid/database/MatrixCursor;
    .restart local v5    # "selectionClause":Ljava/lang/String;
    .restart local v9    # "c":Landroid/database/MatrixCursor;
    :cond_6
    move-object v8, v9

    .line 168
    .end local v9    # "c":Landroid/database/MatrixCursor;
    .restart local v8    # "c":Landroid/database/MatrixCursor;
    :cond_7
    if-eqz v10, :cond_8

    .line 169
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 172
    :cond_8
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursor:Landroid/database/Cursor;

    goto/16 :goto_0

    .line 177
    .end local v8    # "c":Landroid/database/MatrixCursor;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "dayTotalCalorie":I
    .end local v12    # "startOfDay":J
    .end local v14    # "startOfMonth":J
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 168
    .end local v5    # "selectionClause":Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/MatrixCursor;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "dayTotalCalorie":I
    .restart local v12    # "startOfDay":J
    .restart local v14    # "startOfMonth":J
    :catchall_1
    move-exception v2

    goto :goto_3
.end method

.method public static getSystemTimeDateFormat()Ljava/text/SimpleDateFormat;
    .locals 4

    .prologue
    .line 540
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 543
    .local v0, "pattern":Ljava/lang/String;
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 544
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd/MM/yyyy HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 548
    :goto_0
    return-object v1

    .line 545
    :cond_0
    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 546
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MM/dd/yyyy HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 548
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy/MM/dd HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method


# virtual methods
.method public DeletePedoData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ID"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 356
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "start_time"

    aput-object v1, v2, v0

    const-string v0, "exercise_type"

    aput-object v0, v2, v4

    .line 358
    .local v2, "projection":[Ljava/lang/String;
    const-string v0, "="

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 359
    .local v6, "TAG":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v6, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 360
    .local v3, "selection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 362
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 364
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 365
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 366
    const-string v0, "exercise_type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 367
    .local v8, "exerciseType":J
    const-wide/16 v0, 0x4e23

    cmp-long v0, v8, v0

    if-nez v0, :cond_0

    .line 368
    const-string/jumbo v0, "start_time"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->DeletePedoData(Landroid/content/Context;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    .end local v8    # "exerciseType":J
    :cond_0
    if-eqz v7, :cond_1

    .line 373
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 376
    :cond_1
    return-void

    .line 372
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 373
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 329
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->executeQuery(Ljava/lang/CharSequence;)Z

    .line 330
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 410
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->customizeActionBar()V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f09005a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 413
    return-void
.end method

.method protected delete()V
    .locals 0

    .prologue
    .line 478
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->delete()V

    .line 479
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 2

    .prologue
    .line 110
    const-string v0, "0==0"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->executeQuery(Ljava/lang/CharSequence;)Z

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursor:Landroid/database/Cursor;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    return-object v0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    const-string v0, "comment"

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 304
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "selectedTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 336
    .local v0, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 337
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 339
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_0

    .line 340
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    aget-object v4, v4, v6

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 344
    .local v3, "tag":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    invoke-virtual {p0, p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->DeletePedoData(Landroid/content/Context;Ljava/lang/String;)V

    .line 337
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 342
    .end local v3    # "tag":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "tag":Ljava/lang/String;
    goto :goto_1

    .line 348
    .end local v2    # "i":I
    .end local v3    # "tag":Ljava/lang/String;
    :cond_1
    sget v4, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "filterType":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 350
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->applyFilter(Ljava/lang/CharSequence;)V

    .line 352
    :cond_2
    return-object v0
.end method

.method public getExerciseInfoId(JI)J
    .locals 4
    .param p1, "exerciseId"    # J
    .param p3, "exercixeInfoId"    # I

    .prologue
    .line 280
    move-object v1, p0

    .line 281
    .local v1, "mContext":Landroid/content/Context;
    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoUtilsEx;->getExerciseInfoById(Landroid/content/Context;J)Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;

    move-result-object v0

    .line 282
    .local v0, "info":Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;
    if-nez v0, :cond_0

    .line 283
    int-to-long v2, p3

    .line 285
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/core/data/ExerciseInfoDataEx;->getEXERCISE_INFO__ID()I

    move-result v2

    int-to-long v2, v2

    goto :goto_0
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 380
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 381
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v1, 0x7f090076

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    const v1, 0x7f090077

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    const v1, 0x7f090078

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 390
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    .line 391
    if-nez p1, :cond_0

    .line 392
    const-string v0, "0==0"

    .line 398
    :goto_0
    return-object v0

    .line 393
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 394
    const-string v0, "DayGoal"

    goto :goto_0

    .line 395
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 396
    const-string v0, "DayNotGoal"

    goto :goto_0

    .line 398
    :cond_2
    const-string v0, "0==0"

    goto :goto_0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 19

    .prologue
    .line 417
    const/4 v15, 0x0

    .line 419
    .local v15, "i":I
    const-string v17, ""

    .line 421
    .local v17, "shareData":Ljava/lang/String;
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "total_calorie"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "start_time"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "duration_millisecond"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "exercise_info__id"

    aput-object v2, v3, v1

    .line 422
    .local v3, "projection":[Ljava/lang/String;
    const-string v4, "_id IN ("

    .line 423
    .local v4, "selectionClause":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 424
    .local v18, "size":I
    if-nez v18, :cond_0

    .line 425
    const-string v1, ""

    .line 472
    :goto_0
    return-object v1

    .line 427
    :cond_0
    :goto_1
    add-int/lit8 v1, v18, -0x1

    if-ge v15, v1, :cond_1

    .line 428
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v5, "_"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x1

    aget-object v1, v1, v5

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 427
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 430
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v5, "_"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x1

    aget-object v1, v1, v5

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 432
    const/4 v11, 0x0

    .line 435
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const-string/jumbo v6, "start_time DESC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 437
    if-eqz v11, :cond_6

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 439
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "select name from exercise_info where _id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "exercise_info__id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 440
    .local v8, "exerciseTypeQuery":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 443
    .local v14, "exerciseTypeCursor":Landroid/database/Cursor;
    if-eqz v14, :cond_5

    .line 444
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 445
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 446
    const-string/jumbo v1, "name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 448
    .local v16, "name":Ljava/lang/String;
    const-string v1, "cycling"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 449
    const v1, 0x7f0909d0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 458
    :cond_3
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f090b5e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 460
    .end local v16    # "name":Ljava/lang/String;
    :cond_4
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 463
    :cond_5
    const-string v1, "duration_millisecond"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 464
    .local v12, "duration":J
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f090939

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "total_calorie"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0900b9

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f090a1b

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-static {v12, v13, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SecToString(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f090135

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->sharingdateFormatter:Ljava/text/DateFormat;

    const-string/jumbo v5, "start_time"

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 466
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 469
    .end local v8    # "exerciseTypeQuery":Ljava/lang/String;
    .end local v12    # "duration":J
    .end local v14    # "exerciseTypeCursor":Landroid/database/Cursor;
    :cond_6
    if-eqz v11, :cond_7

    .line 470
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object/from16 v1, v17

    .line 472
    goto/16 :goto_0

    .line 450
    .restart local v8    # "exerciseTypeQuery":Ljava/lang/String;
    .restart local v14    # "exerciseTypeCursor":Landroid/database/Cursor;
    .restart local v16    # "name":Ljava/lang/String;
    :cond_8
    :try_start_1
    const-string v1, "hiking"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 451
    const v1, 0x7f0909d1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    .line 452
    :cond_9
    const-string/jumbo v1, "walking"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 453
    const v1, 0x7f0909d2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    .line 454
    :cond_a
    const-string/jumbo v1, "running"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 455
    const v1, 0x7f0909d3

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v16

    goto/16 :goto_2

    .line 469
    .end local v8    # "exerciseTypeQuery":Ljava/lang/String;
    .end local v14    # "exerciseTypeCursor":Landroid/database/Cursor;
    .end local v16    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_b

    .line 470
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v1
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 314
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 267
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 268
    const/16 v0, 0x2711

    if-ne p1, v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->hBitmapHashmap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 274
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->executeQuery(Ljava/lang/CharSequence;)Z

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->refreshAdapter()V

    .line 277
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->SELECTED_FILTER_INDEX:I

    .line 83
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 86
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 88
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 89
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->startActivity(Landroid/content/Intent;)V

    .line 92
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 94
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    const v1, 0x7f0205b7

    const v2, 0x7f090683

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->setNoLogImageAndText(II)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->s_intent_timeFilter:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->s_intent_timeFilter:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->timeChangedReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->s_intent_timeFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 99
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 100
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000f

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->timeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 533
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->mListGoal:Ljava/util/ArrayList;

    .line 534
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.plugins.exercisemate"

    const-string v2, "PQ34"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onDestroy()V

    .line 536
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 510
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 483
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->adapter:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->isMenuDeleteMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 486
    :cond_0
    if-nez p1, :cond_2

    move v1, v2

    .line 500
    :cond_1
    :goto_0
    return v1

    .line 489
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->mCursorCount:Z

    if-eqz v3, :cond_3

    .line 496
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 497
    .local v1, "returnVal":Z
    const v3, 0x7f080c8d

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 498
    .local v0, "deleteMenuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_1

    .line 499
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 29
    .param p1, "tags"    # Ljava/lang/String;

    .prologue
    .line 184
    const-wide/16 v24, 0x0

    .line 185
    .local v24, "id":J
    const-wide/16 v27, 0x0

    .line 186
    .local v27, "kcal":J
    const-wide/16 v17, 0x0

    .line 187
    .local v17, "date":J
    const/4 v15, 0x0

    .line 188
    .local v15, "comment":Ljava/lang/String;
    const/16 v23, 0x0

    .line 189
    .local v23, "exercixeInfoId":I
    const/16 v19, 0x0

    .line 190
    .local v19, "deviceInfoId":I
    const/16 v21, 0x0

    .line 192
    .local v21, "distance":F
    const-string v3, "_"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v14, v3, v4

    .line 194
    .local v14, "_id":Ljava/lang/String;
    const-string v6, "_id=?"

    .line 195
    .local v6, "selectionClause":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v14, v7, v3

    .line 197
    .local v7, "mSelectionArgs":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 199
    .local v16, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 200
    if-eqz v16, :cond_0

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 201
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 202
    const-string v3, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 203
    const-string/jumbo v3, "total_calorie"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v27

    .line 204
    const-string/jumbo v3, "start_time"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 205
    const-string v3, "comment"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 206
    const-string v3, "exercise_info__id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 207
    const-string v3, "exercise_type"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 208
    const-string v3, "distance"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v21

    .line 213
    :cond_0
    if-eqz v16, :cond_1

    .line 214
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 216
    :cond_1
    new-instance v26, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 218
    .local v26, "intent":Landroid/content/Intent;
    const/16 v3, 0x4e21

    move/from16 v0, v19

    if-eq v0, v3, :cond_2

    const/16 v3, 0x4e22

    move/from16 v0, v19

    if-ne v0, v3, :cond_6

    .line 219
    :cond_2
    new-instance v26, Landroid/content/Intent;

    .end local v26    # "intent":Landroid/content/Intent;
    const-class v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 220
    .restart local v26    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "pick_result"

    move-object/from16 v0, v26

    move-wide/from16 v1, v24

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 224
    :goto_0
    const-string/jumbo v3, "pick_type"

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 228
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v24

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 229
    .local v11, "selectionClauseDEVICE":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "user_device__id"

    aput-object v4, v10, v3

    .line 231
    .local v10, "mSelectionArgsDEVICE":[Ljava/lang/String;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 232
    if-eqz v16, :cond_3

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    .line 233
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 234
    const-string/jumbo v3, "user_device__id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 235
    .local v20, "deviceType":Ljava/lang/String;
    if-eqz v20, :cond_3

    const-string v3, "_"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_3

    .line 236
    const-string v3, "data_type"

    const-string v4, "_"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 241
    .end local v20    # "deviceType":Ljava/lang/String;
    :cond_3
    if-eqz v16, :cond_4

    .line 242
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 245
    :cond_4
    const/16 v3, 0x4e23

    move/from16 v0, v19

    if-ne v0, v3, :cond_8

    .line 246
    const-string v3, "id_mode"

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 247
    const-string/jumbo v3, "pick_calories"

    move-object/from16 v0, v26

    move-wide/from16 v1, v27

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 248
    const-string v3, "distance"

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 249
    const-string/jumbo v3, "totalstep"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    const-string/jumbo v3, "starttime"

    move-object/from16 v0, v26

    move-wide/from16 v1, v17

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 251
    const-string v3, "edit"

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 260
    :goto_1
    const/16 v3, 0x2711

    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 264
    :goto_2
    return-void

    .line 213
    .end local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .end local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    .end local v26    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v3

    if-eqz v16, :cond_5

    .line 214
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v3

    .line 222
    .restart local v26    # "intent":Landroid/content/Intent;
    :cond_6
    new-instance v26, Landroid/content/Intent;

    .end local v26    # "intent":Landroid/content/Intent;
    const-class v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v26    # "intent":Landroid/content/Intent;
    goto/16 :goto_0

    .line 241
    .restart local v10    # "mSelectionArgsDEVICE":[Ljava/lang/String;
    .restart local v11    # "selectionClauseDEVICE":Ljava/lang/String;
    :catchall_1
    move-exception v3

    if-eqz v16, :cond_7

    .line 242
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3

    .line 253
    :cond_8
    const-string v3, "id_mode"

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 254
    move/from16 v0, v23

    int-to-long v3, v0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v3, v4, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getExerciseInfoId(JI)J

    move-result-wide v3

    long-to-int v0, v3

    move/from16 v23, v0

    .line 256
    const-string v3, "edit"

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 257
    const-string/jumbo v3, "starttime"

    move-object/from16 v0, v26

    move-wide/from16 v1, v17

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_1

    .line 261
    :catch_0
    move-exception v22

    .line 262
    .local v22, "e":Landroid/content/ActivityNotFoundException;
    const v3, 0x7f09060f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x7d0

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 325
    return-void
.end method
