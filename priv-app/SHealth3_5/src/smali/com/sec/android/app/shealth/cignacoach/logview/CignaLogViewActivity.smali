.class public Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "CignaLogViewActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private extendHashList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;>;"
        }
    .end annotation
.end field

.field private mExpandableListView:Landroid/widget/ExpandableListView;

.field private mIsTimeToReassess:Z

.field private mLastAssessmentDate:Ljava/lang/String;

.field private mLastScoreDifference:I

.field private mLastUpdateMessage:Ljava/lang/String;

.field private mLifeStyleScores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation
.end field

.field private mLogAdapter:Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;

.field private mOutOfMessage:Ljava/lang/String;

.field private mReassessGoalWarningDescription:Ljava/lang/String;

.field private mReassessGoalWarningMessage:Ljava/lang/String;

.field private mReassessMssage:Ljava/lang/String;

.field private mScoreCoachDescription:Ljava/lang/String;

.field private mScoreCoachMessage:Ljava/lang/String;

.field private mScoreMessage:Ljava/lang/String;

.field private mVarianceMessage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mIsTimeToReassess:Z

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mScoreMessage:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLastUpdateMessage:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mVarianceMessage:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mOutOfMessage:Ljava/lang/String;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mScoreCoachMessage:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mScoreCoachDescription:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mReassessMssage:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mReassessGoalWarningMessage:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mReassessGoalWarningDescription:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLastAssessmentDate:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLifeStyleScores:Ljava/util/List;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->extendHashList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const-string v1, "Log"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 93
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 94
    return-void
.end method

.method public finish()V
    .locals 0

    .prologue
    .line 79
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->finish()V

    .line 80
    return-void
.end method

.method public initializeLifeStyleScore(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V
    .locals 18
    .param p1, "peridType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    .prologue
    .line 98
    const/4 v11, 0x0

    .line 100
    .local v11, "lss":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScoreStatus(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v2

    .line 101
    .local v2, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    if-eqz v2, :cond_4

    .line 102
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/ScoreInfo;->isTimeToReassess()Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mIsTimeToReassess:Z

    .line 103
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/dataobjects/ScoreInfoData;

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getCurrentScoreMessage()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mScoreMessage:Ljava/lang/String;

    .line 104
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/ScoreInfo;->getLastUpdateMessage()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLastUpdateMessage:Ljava/lang/String;

    .line 105
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/ScoreInfo;->getOutOfMessage()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mOutOfMessage:Ljava/lang/String;

    .line 106
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/ScoreInfo;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/ScoreInfo;->getLssList()Ljava/util/List;

    move-result-object v11

    .line 113
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 116
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v13

    .line 117
    .local v13, "msgListSize":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v13, :cond_4

    .line 118
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_2

    .line 119
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v14

    sget-object v15, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v14, v15, :cond_0

    .line 120
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mScoreCoachMessage:Ljava/lang/String;

    .line 121
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mScoreCoachDescription:Ljava/lang/String;

    .line 123
    :cond_0
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v14

    sget-object v15, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v14, v15, :cond_1

    .line 124
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mReassessMssage:Ljava/lang/String;

    .line 127
    :cond_1
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v14

    sget-object v15, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WITH_GOALS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v14, v15, :cond_2

    .line 128
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mReassessGoalWarningMessage:Ljava/lang/String;

    .line 129
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mReassessGoalWarningDescription:Ljava/lang/String;

    .line 117
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 135
    .end local v7    # "i":I
    .end local v13    # "msgListSize":I
    :cond_3
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->TAG:Ljava/lang/String;

    const-string v15, "coach msg is null"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v2    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    :cond_4
    :goto_1
    if-eqz v11, :cond_7

    .line 144
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "TimeToReassess: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mIsTimeToReassess:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "score size: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-lt v14, v15, :cond_7

    .line 148
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLifeStyleScores:Ljava/util/List;

    .line 150
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->extendHashList:Ljava/util/HashMap;

    .line 152
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    .line 153
    .local v12, "lssSize":I
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    if-ge v7, v12, :cond_7

    .line 154
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLifeStyleScores:Ljava/util/List;

    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 156
    .local v1, "cal":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLifeStyleScores:Ljava/util/List;

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v1

    .line 158
    new-instance v3, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    invoke-direct {v3, v14, v15}, Ljava/util/Date;-><init>(J)V

    .line 160
    .local v3, "d":Ljava/util/Date;
    invoke-virtual {v3}, Ljava/util/Date;->getMonth()I

    move-result v14

    add-int/lit8 v4, v14, 0x1

    .line 161
    .local v4, "detailMonth":I
    invoke-virtual {v3}, Ljava/util/Date;->getYear()I

    move-result v5

    .line 163
    .local v5, "detailYear":I
    const-string v14, "%d.%d"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit16 v0, v5, 0x76c

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 169
    .local v8, "keyValue":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->extendHashList:Ljava/util/HashMap;

    invoke-virtual {v14, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    if-nez v14, :cond_6

    .line 170
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v9, "lifeStyleScoreList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLifeStyleScores:Ljava/util/List;

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->extendHashList:Ljava/util/HashMap;

    invoke-virtual {v14, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    :goto_3
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    if-ne v7, v14, :cond_5

    .line 179
    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 180
    .local v10, "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getVarianceText()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mVarianceMessage:Ljava/lang/String;

    .line 181
    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getVarianceValue()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLastScoreDifference:I

    .line 183
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "mVarianceMessage: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mVarianceMessage:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    sget-object v14, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "mLastScoreDifference: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLastScoreDifference:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    .end local v10    # "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 138
    .end local v1    # "cal":Ljava/util/Calendar;
    .end local v3    # "d":Ljava/util/Date;
    .end local v4    # "detailMonth":I
    .end local v5    # "detailYear":I
    .end local v7    # "i":I
    .end local v8    # "keyValue":Ljava/lang/String;
    .end local v9    # "lifeStyleScoreList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    .end local v12    # "lssSize":I
    :catch_0
    move-exception v6

    .line 140
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 174
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v1    # "cal":Ljava/util/Calendar;
    .restart local v3    # "d":Ljava/util/Date;
    .restart local v4    # "detailMonth":I
    .restart local v5    # "detailYear":I
    .restart local v7    # "i":I
    .restart local v8    # "keyValue":Ljava/lang/String;
    .restart local v12    # "lssSize":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->extendHashList:Ljava/util/HashMap;

    invoke-virtual {v14, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    .line 175
    .restart local v9    # "lifeStyleScoreList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLifeStyleScores:Ljava/util/List;

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 189
    .end local v1    # "cal":Ljava/util/Calendar;
    .end local v3    # "d":Ljava/util/Date;
    .end local v4    # "detailMonth":I
    .end local v5    # "detailYear":I
    .end local v7    # "i":I
    .end local v8    # "keyValue":Ljava/lang/String;
    .end local v9    # "lifeStyleScoreList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    .end local v12    # "lssSize":I
    :cond_7
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v0, 0x7f03006c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->setContentView(I)V

    .line 53
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->MONTHLY:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->initializeLifeStyleScore(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V

    .line 54
    const v0, 0x7f0801fe

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->extendHashList:Ljava/util/HashMap;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLogAdapter:Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mLogAdapter:Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->post(Ljava/lang/Runnable;)Z

    .line 69
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 85
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 75
    return-void
.end method
