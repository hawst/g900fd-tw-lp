.class Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;
.super Ljava/util/TimerTask;
.source "UvStateReady.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/state/UvStateReady;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HRUpdateViewTask"
.end annotation


# instance fields
.field private mHasAnimation:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/uv/state/UvStateReady;Z)V
    .locals 0
    .param p2, "hasAnimation"    # Z

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    .line 95
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 96
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;->mHasAnimation:Z

    .line 97
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 101
    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateViewTask, run() : isInCurrentState = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateReady;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateReady;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 104
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 105
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;->mHasAnimation:Z

    if-eqz v2, :cond_1

    :goto_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 108
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void

    .line 105
    .restart local v0    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
