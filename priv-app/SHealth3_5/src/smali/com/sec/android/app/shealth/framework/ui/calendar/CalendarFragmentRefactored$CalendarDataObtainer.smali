.class public Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CalendarDataObtainer"
.end annotation


# instance fields
.field dayButtonMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarButton;",
            ">;"
        }
    .end annotation
.end field

.field monthEnd:J

.field monthStart:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;JJ)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-wide p2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthStart:J

    iput-wide p4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthEnd:J

    iget-object v0, p1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->periodStartToDayButtonMap:Ljava/util/Map;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->dayButtonMap:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;JJLcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$1;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;JJ)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthStart:J

    iget-wide v4, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthEnd:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->getDaysStatuses(JJ)Ljava/util/TreeMap;

    move-result-object v0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->access$202(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;Ljava/util/TreeMap;)Ljava/util/TreeMap;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarFragmentRefactored$CalendarDataObtainer;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
