.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;
.super Landroid/os/CountDownTimer;
.source "SpO2SummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 1528
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 1539
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1544
    :goto_0
    return-void

    .line 1542
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isReadyScreenTimeout:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4302(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Z)Z

    .line 1543
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    const/4 v1, -0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1532
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isFingerDetected:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c05

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1533
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$19;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isRetryMeasuring:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Z

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->changeUIReadyToMeasuring(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Z)V

    .line 1535
    :cond_0
    return-void
.end method
