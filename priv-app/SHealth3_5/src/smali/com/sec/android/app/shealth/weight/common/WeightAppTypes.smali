.class public final enum Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;
.super Ljava/lang/Enum;
.source "WeightAppTypes.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

.field public static final enum DOWNLOAD_MR:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

.field public static final enum DOWNLOAD_MR_EXTENDED:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

.field public static final enum PRELOAD:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;


# instance fields
.field public final isAccessorySupported:Z

.field public final summaryFragmentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 32
    new-instance v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    const-string v1, "DOWNLOAD_MR_EXTENDED"

    const-class v2, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR_EXTENDED:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    .line 33
    new-instance v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    const-string v1, "DOWNLOAD_MR"

    const-class v2, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    .line 34
    new-instance v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    const-string v1, "PRELOAD"

    const-class v2, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->PRELOAD:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR_EXTENDED:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->PRELOAD:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->$VALUES:[Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;Z)V
    .locals 0
    .param p4, "isAccessorySupported"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p3, "summaryFragmentClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->summaryFragmentClass:Ljava/lang/Class;

    .line 41
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->isAccessorySupported:Z

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->$VALUES:[Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    return-object v0
.end method
