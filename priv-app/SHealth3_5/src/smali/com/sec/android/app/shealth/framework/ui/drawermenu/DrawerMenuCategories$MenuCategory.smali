.class public final enum Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MenuCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

.field public static final enum FAVORITE:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

.field public static final enum FUNCTIONS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

.field public static final enum SETTINGS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;


# instance fields
.field private categoryCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    const-string v1, "FAVORITE"

    const-string v2, "Favorite"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->FAVORITE:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    const-string v1, "FUNCTIONS"

    const-string v2, "Functions"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->FUNCTIONS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    const-string v1, "SETTINGS"

    const-string v2, "Settings"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->SETTINGS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->FAVORITE:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->FUNCTIONS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->SETTINGS:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->categoryCode:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    return-object v0
.end method


# virtual methods
.method public getMenuCategoryCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->categoryCode:Ljava/lang/String;

    return-object v0
.end method
