.class Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$4;
.super Landroid/os/CountDownTimer;
.source "HumidityDeviceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 464
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 474
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onFinish!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mHumiditySensorListener:Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;->onHumidityTimeout()V

    .line 476
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 468
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onTick: "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mHumiditySensorListener:Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;->onHumidityTick(J)V

    .line 470
    return-void
.end method
