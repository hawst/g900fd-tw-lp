.class public Lcom/sec/android/app/shealth/plugins/exercisepro/constants/LoggingConstants;
.super Ljava/lang/Object;
.source "LoggingConstants.java"


# static fields
.field public static final Accessory:Ljava/lang/String; = "P004"

.field public static final AudioGuideOff:Ljava/lang/String; = "P038"

.field public static final AudioGuideOn:Ljava/lang/String; = "P037"

.field public static final Auto:Ljava/lang/String; = "auto"

.field public static final BasicWorkout:Ljava/lang/String; = "G001"

.field public static final CalorieGoal:Ljava/lang/String; = "G004"

.field public static final ChangeActivityTypeStart:Ljava/lang/String; = "P001"

.field public static final ChartShare:Ljava/lang/String; = "CS01"

.field public static final DetailShare:Ljava/lang/String; = "DS01"

.field public static final DistanceGoal:Ljava/lang/String; = "G002"

.field public static final Duration:Ljava/lang/String; = "duration"

.field public static final ExerciseMate:Ljava/lang/String; = "com.sec.android.app.shealth.plugins.exercisemate"

.field public static final ExercisePro:Ljava/lang/String; = "com.sec.android.app.shealth.plugins.exercisepro"

.field public static final Help:Ljava/lang/String; = "P039"

.field public static final History:Ljava/lang/String; = "P034"

.field public static final HistoryOut:Ljava/lang/String; = "PQ34"

.field public static final Manual:Ljava/lang/String; = "manual"

.field public static final MusicPlay:Ljava/lang/String; = "P003"

.field public static final SetGoal:Ljava/lang/String; = "E002"

.field public static final StartCamera:Ljava/lang/String; = "P036"

.field public static final SummaryChart:Ljava/lang/String; = "P035"

.field public static final SummaryChartOut:Ljava/lang/String; = "PQ35"

.field public static final TimeGoal:Ljava/lang/String; = "G003"

.field public static final TrainingEffect:Ljava/lang/String; = "P002"

.field public static final TrainingEffectGoal:Ljava/lang/String; = "G005"

.field public static final WorkOutChart:Ljava/lang/String; = "E012"

.field public static final WorkOutDetail:Ljava/lang/String; = "E013"

.field public static final WorkOutDetailMap:Ljava/lang/String; = "E014"

.field public static final WorkoutStart_Cycling:Ljava/lang/String; = "P031"

.field public static final WorkoutStart_Hiking:Ljava/lang/String; = "P032"

.field public static final WorkoutStart_Running:Ljava/lang/String; = "P033"

.field public static final WorkoutStart_Walking:Ljava/lang/String; = "P030"

.field public static final WorkoutStop_Cycling:Ljava/lang/String; = "P131"

.field public static final WorkoutStop_Hiking:Ljava/lang/String; = "P132"

.field public static final WorkoutStop_Running:Ljava/lang/String; = "P133"

.field public static final WorkoutStop_Walking:Ljava/lang/String; = "P130"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
