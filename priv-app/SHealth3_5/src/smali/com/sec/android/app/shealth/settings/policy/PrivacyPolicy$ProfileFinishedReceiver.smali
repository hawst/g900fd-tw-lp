.class Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PrivacyPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProfileFinishedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V
    .locals 0

    .prologue
    .line 900
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;

    .prologue
    .line 900
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;-><init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 905
    if-eqz p2, :cond_0

    const-string v0, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$ProfileFinishedReceiver;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->finish()V

    .line 909
    :cond_0
    return-void
.end method
