.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$3;
.super Ljava/lang/Object;
.source "WalkingMateGoalActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 238
    const/4 v0, 0x0

    .line 239
    .local v0, "ACTION_BAR_BUTTON_CANCEL":I
    const/4 v1, 0x1

    .local v1, "ACTION_BAR_BUTTON_DONE":I
    move-object v2, p1

    .line 241
    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 243
    .local v2, "actionBarButtonView":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    if-eqz v2, :cond_0

    .line 244
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 246
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkChangesAndExit()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V

    goto :goto_0

    .line 249
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkAndSaveGoalToDB()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V

    .line 250
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.walkingmate"

    const-string v5, "W004"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
