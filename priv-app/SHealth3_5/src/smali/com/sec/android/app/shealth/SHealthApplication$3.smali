.class Lcom/sec/android/app/shealth/SHealthApplication$3;
.super Ljava/lang/Object;
.source "SHealthApplication.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/SHealthApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/SHealthApplication;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/SHealthApplication;)V
    .locals 0

    .prologue
    .line 1358
    iput-object p1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1362
    const-string v3, "Device Type Shared Preferences"

    const-string v4, "Device Type Shared Preferences - in Onservice connected"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1364
    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/SHealthApplication;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Binding is done - Service connected"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1366
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeviceTypeChecked(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1367
    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/SHealthApplication;->access$700()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "mSensorConnectionListener : Already setDeviceTypeChecked : true"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1369
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1700(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1370
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1700(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 1371
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # setter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1702(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 1396
    :cond_0
    :goto_0
    return-void

    .line 1377
    :cond_1
    const/4 v0, 0x0

    .line 1379
    .local v0, "isNeedToSetDeviceChecked":Z
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setPedometerAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$900(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v0

    .line 1380
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setAntAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1000(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    move v0, v1

    .line 1381
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setHRAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1100(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    move v0, v1

    .line 1382
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setUVAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1200(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    move v0, v1

    .line 1383
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setSpO2Available()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1300(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    move v0, v1

    .line 1384
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setTHGAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1400(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    move v0, v1

    .line 1386
    :goto_5
    const-string v1, "-----SIC-----"

    const-string v2, "Clearing app registry data and setting again on service connected"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->clearAppRegistryData()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1500(Lcom/sec/android/app/shealth/SHealthApplication;)V

    .line 1388
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->clear()V

    .line 1389
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->loadAppRegistryData()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1600(Lcom/sec/android/app/shealth/SHealthApplication;)V

    .line 1390
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setDeviceTypeChecked(Landroid/content/Context;Z)V

    .line 1392
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1700(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1393
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1700(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 1394
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # setter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1702(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1380
    goto :goto_1

    :cond_3
    move v0, v2

    .line 1381
    goto :goto_2

    :cond_4
    move v0, v2

    .line 1382
    goto :goto_3

    :cond_5
    move v0, v2

    .line 1383
    goto :goto_4

    :cond_6
    move v0, v2

    .line 1384
    goto :goto_5
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1401
    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/SHealthApplication;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1402
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1700(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1403
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1700(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 1404
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication$3;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1702(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 1406
    :cond_0
    return-void
.end method
