.class Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;
.super Ljava/lang/Object;
.source "MyFoodActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$IGeneralAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GeneralActionImplWithAddAndDeleteButtonsInActionBar"
.end annotation


# static fields
.field private static final ACTION_BAR_ADD_BUTTON_SELECTOR:I = 0x7f020191

.field private static final ACTION_BAR_DELETE_BUTTON_SELECTOR:I = 0x7f020192


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$1;

    .prologue
    .line 336
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;-><init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V

    return-void
.end method


# virtual methods
.method public initActionBarWithDoneButton()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 379
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$600(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 380
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 381
    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 382
    new-instance v1, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar$3;-><init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;)V

    .line 406
    .local v1, "doneListener":Landroid/view/View$OnClickListener;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$800(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f020192

    const v5, 0x7f090035

    invoke-direct {v3, v4, v6, v5, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    # setter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mDeleteModeActionBarButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$902(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 415
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1000(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 416
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Landroid/widget/Spinner;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 418
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1200(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    .line 425
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1500(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getCheckedItemsAmount()I

    move-result v3

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->updateSelectSpinner(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1600(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;I)V

    .line 428
    return-void

    .line 412
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f090044

    invoke-direct {v3, v6, v4, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    # setter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mDeleteModeActionBarButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$902(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    goto :goto_0

    .line 422
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->addSelectSpinner()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1300(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mSelectCountSpinner:Landroid/widget/Spinner;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    goto :goto_1
.end method

.method public initDefaultActionBar()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 344
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 345
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 347
    new-instance v1, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar$1;-><init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;)V

    .line 355
    .local v1, "addButtonListener":Landroid/view/View$OnClickListener;
    new-array v3, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020191

    const v6, 0x7f09003f

    invoke-direct {v4, v5, v7, v6, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v7

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 358
    new-instance v2, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar$2;-><init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;)V

    .line 366
    .local v2, "deleteButtonListener":Landroid/view/View$OnClickListener;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->isExpandableListEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 368
    new-array v3, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020192

    const v6, 0x7f090035

    invoke-direct {v4, v5, v7, v6, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v7

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 371
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0207a2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 373
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->removeSelectSpinner()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$500(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V

    .line 374
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    const v4, 0x7f090944

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->setActionBarTitle(I)V

    .line 375
    return-void
.end method

.method public prepareActionForAddMyFoodButton(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 442
    return-void
.end method

.method public refreshActionBarWithDoneButton()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 432
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteMode:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1700(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 433
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->getCheckedItemsAmount()I

    move-result v0

    .line 434
    .local v0, "checkedCount":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    const v5, 0x7f090074

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 435
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$GeneralActionImplWithAddAndDeleteButtonsInActionBar;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    if-eqz v0, :cond_1

    :goto_0
    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->setDoneButtonEnabled(Z)V
    invoke-static {v3, v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$1800(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Z)V

    .line 437
    .end local v0    # "checkedCount":I
    :cond_0
    return-void

    .restart local v0    # "checkedCount":I
    :cond_1
    move v1, v2

    .line 435
    goto :goto_0
.end method
