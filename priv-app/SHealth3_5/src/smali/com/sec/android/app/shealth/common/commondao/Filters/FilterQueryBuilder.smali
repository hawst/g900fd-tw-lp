.class public Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
.super Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
.source "FilterQueryBuilder.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private sqlQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 50
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V
    .locals 1
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 53
    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 55
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;)V
    .locals 3
    .param p2, "addRule"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            ">;",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "filters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;-><init>()V

    .line 20
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 64
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 65
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 69
    .end local v0    # "i":I
    :cond_1
    return-void
.end method


# virtual methods
.method public AND(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 1
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    .prologue
    .line 119
    if-eqz p1, :cond_0

    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->AND:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->add(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    .line 122
    :cond_0
    return-object p0
.end method

.method public OR(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 1
    .param p1, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->OR:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->add(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    .line 115
    return-object p0
.end method

.method public add(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 2
    .param p1, "addRule"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;
    .param p2, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 88
    :goto_0
    return-object p0

    .line 87
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    goto :goto_0
.end method

.method public addBrackets()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    .line 94
    :cond_0
    return-object p0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public getSQLCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->sqlQuery:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    return-void
.end method
