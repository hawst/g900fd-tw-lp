.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;
.super Ljava/lang/Object;
.source "CoachRestoreHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->showPopupWithProgressBar(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

.field final synthetic val$popup_msg:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;I)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    iput p2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->val$popup_msg:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 116
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->currentStep:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$000(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    if-ne v1, v2, :cond_0

    .line 170
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$102(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 119
    const v0, 0x7f090139

    .line 121
    .local v0, "titleId":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090139

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f030017

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$902(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$900(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setDoNotDismissOnBackPressed(Z)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$702(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$900(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "RESTORE_PROGRESS_POPUP"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
