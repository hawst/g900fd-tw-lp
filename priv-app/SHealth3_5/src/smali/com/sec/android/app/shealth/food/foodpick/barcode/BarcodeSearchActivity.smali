.class public Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "BarcodeSearchActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$8;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$IncorrectSearchDialogButtonController;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$FailedSearchDialogButtonController;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;,
        Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;
    }
.end annotation


# static fields
.field private static final BARCODE_SEARCH_FAILED_POPUP_TAG:Ljava/lang/String; = "barcode_search_failed_popup"

.field private static final BARCODE_SEARCH_SUCCESS_POPUP_TAG:Ljava/lang/String; = "barcode_search_success_popup"

.field private static final DB_SEARCH_BY_BARCODE_FAILED_POPUP_TAG:Ljava/lang/String; = "db_search_by_barcode_failed_popup"

.field private static final DISMISS_PROGRESS_DIALOG:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final SHOW_BARCODE_FAIL_DIALOG:I = 0x2

.field private static final SHOW_BARCODE_TEMP_SUCCES_DIALOG:I = 0x3

.field private static final SHOW_PROGRESS_DIALOG:I


# instance fields
.field private final barcodeResultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

.field private mBarcode:Ljava/lang/String;

.field private mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mExtraFoodInfoHolder:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation
.end field

.field private mIsPopupShown:Z

.field private mProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private mSearchFoodInfoFromBarCodeAsyncTask:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field private pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    .line 70
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 157
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->barcodeResultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;

    const-string v1, "Food pick handler"

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    .line 409
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showSuccessDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showErrorDialogAboutFailSearchByBarcodeFromDb()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->startCameraPreview(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mExtraFoodInfoHolder:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mExtraFoodInfoHolder:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->sendResultAndFinish(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V

    return-void
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->tryNewSearch()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->enterManually()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->stopCameraFocusing()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mSearchFoodInfoFromBarCodeAsyncTask:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mSearchFoodInfoFromBarCodeAsyncTask:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/utils/PauseHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showErrorDialogAboutFailBarcodeSearch()V

    return-void
.end method

.method private closeCameraPreview()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->releaseCamera()V

    .line 149
    :cond_0
    return-void
.end method

.method private enterManually()V
    .locals 1

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->sendResultAboutStartCustomFoodActivity()V

    .line 430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    .line 431
    return-void
.end method

.method private isPopupShown()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendResultAboutStartCustomFoodActivity()V
    .locals 1

    .prologue
    .line 264
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->setResult(I)V

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->finish()V

    .line 266
    return-void
.end method

.method private sendResultAndFinish(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347
    .local p1, "extraFoodInfoHolder":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;>;"
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getResultContents()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 348
    .local v1, "extraFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getResultContents()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getRequest()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;->getFoodInfoData()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v2

    .line 351
    .local v2, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    new-instance v3, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 352
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFoodInfoId(J)V

    .line 353
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert or update foodInfoData"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    new-instance v3, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 355
    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert or update extraFoodInfo"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 357
    .local v0, "data":Landroid/content/Intent;
    const-string v3, "BAR_CODE_RESULT_FOOD_INFO"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 358
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->finish()V

    .line 360
    return-void
.end method

.method private showErrorDialogAboutFailBarcodeSearch()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 226
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x6

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0909b7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0909b5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090057

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0909a4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCenterButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDismissOnAddClick(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$6;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "barcode_search_failed_popup"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 241
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    .line 242
    return-void
.end method

.method private showErrorDialogAboutFailSearchByBarcodeFromDb()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 245
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x6

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0909b7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0909b3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0909a4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0909a3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCenterButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDismissOnAddClick(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$7;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "db_search_by_barcode_failed_popup"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 260
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    .line 261
    return-void
.end method

.method private showSuccessDialog(Ljava/lang/String;)V
    .locals 5
    .param p1, "searchResult"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f09099c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "alertText":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x6

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0909b4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090057

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCenterButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDismissOnAddClick(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$5;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "barcode_search_success_popup"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 222
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    .line 223
    return-void
.end method

.method private startCameraPreview(Z)V
    .locals 2
    .param p1, "isBarcodeScanningEnable"    # Z

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->barcodeResultListener:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->setResultListener(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->isCameraAlreadyInited()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->initCamera()V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->startPreview(Z)V

    .line 134
    :cond_1
    return-void
.end method

.method private stopCameraFocusing()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->stopFocusing()V

    .line 143
    :cond_0
    return-void
.end method

.method private tryNewSearch()V
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->startCameraPreview(Z)V

    .line 435
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z

    .line 436
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0909b6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 155
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    const v0, 0x7f0300c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->setContentView(I)V

    .line 79
    const v0, 0x7f080399

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcodeScanner:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    .line 80
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$2;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 94
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->closeCameraPreview()V

    .line 114
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->closeCameraPreview()V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;->pause()V

    .line 107
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 108
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;->resume()V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->isPopupShown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->startCameraPreview(Z)V

    .line 101
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
