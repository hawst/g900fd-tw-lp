.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 4
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 868
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 869
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MIG_NETWORK_FAILED_POPUP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCurrentStep: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_2

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mNetUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$2000(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 875
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$17;->$SwitchMap$com$sec$android$app$shealth$settings$samsungAccount$DataMigrationHelper$Step:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 877
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->isConfirmed:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Z)Z

    .line 878
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    move-result-object v0

    const v1, 0x7f090888

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar(I)V

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 890
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->doMigration()Z

    goto :goto_0

    .line 899
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 906
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$14;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->exitApplication()V

    goto :goto_0

    .line 875
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
