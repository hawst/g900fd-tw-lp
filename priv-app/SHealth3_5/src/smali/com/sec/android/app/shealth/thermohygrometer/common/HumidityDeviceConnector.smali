.class public Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;
.super Ljava/lang/Object;
.source "HumidityDeviceConnector.java"


# static fields
.field private static final MEASURING_TIMEOUT:I = 0x2710

.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;


# instance fields
.field private IsDeviceStarted:Z

.field isCountDownStarted:Z

.field public isRecord:Z

.field private mContext:Landroid/content/Context;

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field private mHumiditySensorListener:Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;

.field private mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mContext:Landroid/content/Context;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->isRecord:Z

    .line 277
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 351
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$2;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 388
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$3;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 463
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->isCountDownStarted:Z

    .line 464
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$4;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$4;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 48
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->startDevice()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mHumiditySensorListener:Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    .line 43
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mInstance:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    return-object v0
.end method

.method private initSensor()V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method private scanDevice()V
    .locals 9

    .prologue
    const/16 v8, 0x272b

    .line 56
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "scanDevice"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v5, 0x0

    const/16 v6, 0x272b

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v0

    .line 62
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v0, :cond_4

    .line 63
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 64
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == isEmpty"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V

    .line 101
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_0
    :goto_0
    return-void

    .line 67
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 68
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 69
    .local v1, "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v4

    if-ne v4, v8, :cond_2

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 72
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 73
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: found- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_1

    .line 86
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 87
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 75
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "scanDevice() device: else - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_5

    goto/16 :goto_1

    .line 88
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v1    # "dvc":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v2

    .line 89
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0

    .line 78
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v4, :cond_0

    .line 79
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    .line 90
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v2

    .line 91
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 83
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    .restart local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_4
    :try_start_3
    sget-object v4, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string v5, "deviceList == null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    const/4 v5, 0x0

    const/16 v6, 0x103

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    .line 92
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :catch_3
    move-exception v2

    .line 93
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto/16 :goto_0

    .line 94
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    :catch_4
    move-exception v2

    .line 96
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 97
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v2

    .line 99
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private startDevice()V
    .locals 4

    .prologue
    .line 152
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v1, :cond_0

    .line 155
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string v2, "SHealthDeviceFinder is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 187
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-nez v1, :cond_1

    .line 159
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->scanDevice()V

    goto :goto_0

    .line 162
    :cond_1
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z

    if-nez v1, :cond_2

    .line 163
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startReceivingData"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDataListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 165
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->isRecord:Z

    if-eqz v1, :cond_2

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->startRecord()V

    .line 169
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 172
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 174
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 175
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 176
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 177
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 178
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 180
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 181
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 183
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopDevice()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 222
    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "stopDevice"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v2, :cond_0

    .line 225
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 240
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_8

    .line 251
    :goto_1
    iput-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 254
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v2, :cond_1

    .line 256
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->stopScan()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_b

    .line 266
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/util/ConcurrentModificationException; {:try_start_3 .. :try_end_3} :catch_d

    .line 272
    :goto_3
    iput-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 274
    :cond_1
    return-void

    .line 226
    :catch_0
    move-exception v1

    .line 227
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 228
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 229
    .local v1, "e1":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 230
    .end local v1    # "e1":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 231
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 232
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 234
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v0

    .line 236
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 241
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 242
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 243
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_6
    move-exception v0

    .line 244
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 245
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_7
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 247
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_8
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 257
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_9
    move-exception v1

    .line 258
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 259
    .end local v1    # "e1":Landroid/os/RemoteException;
    :catch_a
    move-exception v1

    .line 260
    .local v1, "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_2

    .line 261
    .end local v1    # "e1":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_b
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 267
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_c
    move-exception v0

    .line 268
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_3

    .line 269
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_d
    move-exception v0

    .line 270
    .local v0, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_3
.end method


# virtual methods
.method public isStarted()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z

    return v0
.end method

.method public isStripConnected()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    if-eqz v0, :cond_0

    .line 483
    :cond_0
    return v1
.end method

.method public startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;
    .param p3, "isRecordON"    # Z

    .prologue
    .line 105
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startMeasuring : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mContext:Landroid/content/Context;

    .line 107
    if-eqz p2, :cond_0

    .line 108
    iput-object p2, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mHumiditySensorListener:Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;

    .line 110
    :cond_0
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->isRecord:Z

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->startDevice()V

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->initSensor()V

    .line 113
    return-void
.end method

.method public startRecord()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 192
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->record()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_5

    .line 212
    :goto_0
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 196
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 198
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 202
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 204
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 205
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 207
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 208
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 210
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopMeasuring()V
    .locals 2

    .prologue
    .line 215
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopMeasuring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->stopDevice()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->IsDeviceStarted:Z

    .line 219
    return-void
.end method
