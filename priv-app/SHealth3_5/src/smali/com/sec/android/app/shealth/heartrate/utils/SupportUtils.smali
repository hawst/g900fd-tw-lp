.class public Lcom/sec/android/app/shealth/heartrate/utils/SupportUtils;
.super Ljava/lang/Object;
.source "SupportUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGraphStartTime(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;I)J
    .locals 7
    .param p0, "handlerTime"    # J
    .param p2, "period"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p3, "markCounts"    # I

    .prologue
    const v6, 0x3f4ccccd    # 0.8f

    .line 63
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    .line 65
    .local v1, "chartStartDateCalendar":Ljava/util/Calendar;
    invoke-static {p2}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getCalendarPeriodType()I

    move-result v0

    .line 66
    .local v0, "calendarPeriodType":I
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getStartHourToMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 67
    neg-int v4, p3

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v0, v4}, Ljava/util/Calendar;->add(II)V

    .line 69
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long v2, p0, v4

    .line 71
    .local v2, "visibleChartWidthInMilliseconds":J
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v4, :cond_0

    .line 72
    long-to-float v4, v2

    mul-float/2addr v4, v6

    const v5, 0x461c4000    # 10000.0f

    add-float/2addr v4, v5

    float-to-long v4, v4

    sub-long v4, p0, v4

    .line 76
    :goto_0
    return-wide v4

    :cond_0
    long-to-float v4, v2

    mul-float/2addr v4, v6

    float-to-long v4, v4

    sub-long v4, p0, v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v4

    goto :goto_0
.end method

.method public static getLoadTagList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v8, "userTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 40
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "device_type"

    aput-object v1, v2, v0

    .line 43
    .local v2, "projection":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 44
    if-eqz v6, :cond_1

    .line 45
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    const-string v0, "device_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 49
    :catch_0
    move-exception v7

    .line 50
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    if-eqz v6, :cond_0

    .line 53
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 58
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-object v8

    .line 52
    :cond_1
    if-eqz v6, :cond_0

    .line 53
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 52
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 53
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static setScrollContainerHeight(Landroid/app/Activity;I)V
    .locals 7
    .param p0, "activ"    # Landroid/app/Activity;
    .param p1, "rid"    # I

    .prologue
    .line 90
    :try_start_0
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 91
    .local v2, "model":Ljava/lang/String;
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-gt v5, v6, :cond_1

    const-string v5, "GT-I9305"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "GT-I9300"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SM-G9098"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SM-G9092"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SGH-T999"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SGH-I747"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SCH-R530"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SCH-I535"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SPH-L710"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SM-N910F"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 101
    :cond_0
    const v5, 0x7f080630

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 103
    .local v0, "aScrollView":Landroid/view/ViewGroup;
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 104
    .local v3, "scrollContent":Landroid/view/ViewGroup;
    if-nez v3, :cond_2

    .line 116
    .end local v0    # "aScrollView":Landroid/view/ViewGroup;
    .end local v2    # "model":Ljava/lang/String;
    .end local v3    # "scrollContent":Landroid/view/ViewGroup;
    :cond_1
    :goto_0
    return-void

    .line 107
    .restart local v0    # "aScrollView":Landroid/view/ViewGroup;
    .restart local v2    # "model":Ljava/lang/String;
    .restart local v3    # "scrollContent":Landroid/view/ViewGroup;
    :cond_2
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 109
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 111
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    .end local v0    # "aScrollView":Landroid/view/ViewGroup;
    .end local v1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "model":Ljava/lang/String;
    .end local v3    # "scrollContent":Landroid/view/ViewGroup;
    :catch_0
    move-exception v4

    .line 114
    .local v4, "t":Ljava/lang/Throwable;
    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
