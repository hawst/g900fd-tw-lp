.class Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;
.super Ljava/lang/Object;
.source "LifeStyleScoreActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 458
    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onDateChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->cancel(Z)Z

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$502(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mDateChangedTask:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->mCurrentPeriodType:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;)Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity$OnDateChangedTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 465
    return-void
.end method

.method public onGoalMissionStatusChange(II)V
    .locals 0
    .param p1, "goalId"    # I
    .param p2, "missionId"    # I

    .prologue
    .line 455
    return-void
.end method
