.class Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;
.super Ljava/lang/Object;
.source "SpO2Activity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showInfomationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0

    .prologue
    .line 614
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1400(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1400(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1402(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;

    .line 627
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1502(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 628
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 631
    :cond_1
    return-void
.end method
