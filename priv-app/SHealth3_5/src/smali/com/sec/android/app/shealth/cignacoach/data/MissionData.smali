.class public Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
.super Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
.source "MissionData.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/MissionData;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private mFrequencyDef:Ljava/lang/String;

.field private mFrequencyDefID:I

.field private mFrequencyMax:I

.field private mFrequencyMin:I

.field private mGoalID:I

.field private mMissionDetails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mMissionProgressStatus:Ljava/lang/String;

.field private mMissionProgressStatusImage:Ljava/lang/String;

.field private mMissionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field private mMissionRepeatable:Z

.field private mMissionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field private mMissionTipList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;",
            ">;"
        }
    .end annotation
.end field

.field private mProgress:I

.field private mSelectFrequencyDays:I

.field private mSelectedFrequency:Ljava/lang/String;

.field private mSpecificTracker:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field private mTrackerMission:Z

.field private mTrackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field private mWhatToDo:Ljava/lang/String;

.field private mWhatToDoImage:Ljava/lang/String;

.field private mWhyDoIt:Ljava/lang/String;

.field private mWhyDoItImage:Ljava/lang/String;

.field private startDateTimeMillis:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->TAG:Ljava/lang/String;

    .line 366
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>()V

    .line 59
    return-void
.end method

.method public constructor <init>(IILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "goalId"    # I
    .param p2, "id"    # I
    .param p3, "iconDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p4, "titleString"    # Ljava/lang/String;
    .param p5, "extraString"    # Ljava/lang/String;
    .param p6, "isRepeatable"    # Z

    .prologue
    .line 75
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 76
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mGoalID:I

    .line 77
    iput-boolean p6, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionRepeatable:Z

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>(Landroid/os/Parcel;)V

    .line 82
    return-void
.end method

.method public static isRepeatable(Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;)Z
    .locals 1
    .param p0, "stage"    # Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .prologue
    .line 458
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    if-ne p0, v0, :cond_0

    .line 459
    const/4 v0, 0x1

    .line 461
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .locals 3
    .param p0, "gmi"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo;

    .prologue
    .line 393
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;-><init>()V

    .line 394
    .local v0, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setId(I)V

    .line 395
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getGoalId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setGoalID(I)V

    .line 396
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMission()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setTitleString(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setExtraString(Ljava/lang/String;)V

    .line 398
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getFrequencyDefaultId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setFrequencyDefID(I)V

    .line 399
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getFrequencyDefault()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setFrequencyDef(Ljava/lang/String;)V

    .line 400
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getFrequencyMin()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setFrequencyMin(I)V

    .line 401
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getFrequencyMax()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setFrequencyMax(I)V

    .line 402
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getWhatToDo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setWhatToDo(Ljava/lang/String;)V

    .line 403
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getWhatToDoImage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setWhatToDoImage(Ljava/lang/String;)V

    .line 404
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getWhyDoIt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setWhyDoIt(Ljava/lang/String;)V

    .line 405
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getWhyDoItImage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setWhyDoItImage(Ljava/lang/String;)V

    .line 406
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->isTrackerMission()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setTrackerMission(Z)V

    .line 407
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setTrackerType(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)V

    .line 408
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getSpecificTracker()Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setSpecificTracker(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)V

    .line 410
    instance-of v1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;

    if-eqz v1, :cond_0

    move-object v1, p0

    .line 416
    check-cast v1, Lcom/cigna/coach/dataobjects/GoalMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getDefaultFrequency()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setFrequencyDefID(I)V

    move-object v1, p0

    .line 417
    check-cast v1, Lcom/cigna/coach/dataobjects/GoalMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getMinFrequency()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setFrequencyMin(I)V

    move-object v1, p0

    .line 418
    check-cast v1, Lcom/cigna/coach/dataobjects/GoalMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getMaxFrequency()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setFrequencyMax(I)V

    move-object v1, p0

    .line 419
    check-cast v1, Lcom/cigna/coach/dataobjects/GoalMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getPrimaryCategory()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 422
    :cond_0
    instance-of v1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    if-eqz v1, :cond_1

    move-object v1, p0

    .line 430
    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionProgressStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionProgressStatus(Ljava/lang/String;)V

    move-object v1, p0

    .line 431
    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionProgressStatusImage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionProgressStatusImage(Ljava/lang/String;)V

    move-object v1, p0

    .line 432
    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V

    move-object v1, p0

    .line 433
    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getSelectedFrequency()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setSelectedFrequency(Ljava/lang/String;)V

    move-object v1, p0

    .line 434
    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getTips()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionTipList(Ljava/util/List;)V

    move-object v1, p0

    .line 435
    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionDetails()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionDetails(Ljava/util/ArrayList;)V

    move-object v1, p0

    .line 436
    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->isMissionRepeatable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionRepeatable(Z)V

    .line 439
    :cond_1
    instance-of v1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;

    if-eqz v1, :cond_2

    move-object v1, p0

    .line 446
    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getPrimaryCategory()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    move-object v1, p0

    .line 447
    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setStartDateTimeMillis(J)V

    move-object v1, p0

    .line 448
    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    move-object v1, p0

    .line 449
    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setSelectFrequencyDays(I)V

    move-object v1, p0

    .line 450
    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionDetails()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionDetails(Ljava/util/ArrayList;)V

    .line 451
    check-cast p0, Lcom/cigna/coach/dataobjects/UserMissionData;

    .end local p0    # "gmi":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setCompleteDate(Ljava/util/Calendar;)V

    .line 454
    :cond_2
    return-object v0
.end method


# virtual methods
.method public getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public getFrequencyDef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDef:Ljava/lang/String;

    return-object v0
.end method

.method public getFrequencyDefID()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDefID:I

    return v0
.end method

.method public getFrequencyMax()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMax:I

    return v0
.end method

.method public getFrequencyMin()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMin:I

    return v0
.end method

.method public getGoalID()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mGoalID:I

    return v0
.end method

.method public getMissionCheckCount()I
    .locals 4

    .prologue
    .line 256
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionDetails:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 257
    const/4 v0, 0x0

    .line 263
    :cond_0
    return v0

    .line 259
    :cond_1
    const/4 v0, 0x0

    .line 260
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionDetails:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 261
    .local v2, "missionDetails":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v3

    add-int/2addr v0, v3

    .line 262
    goto :goto_0
.end method

.method public getMissionDetailList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionDetails:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 289
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionDetails:Ljava/util/ArrayList;

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionDetails:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMissionStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    return-object v0
.end method

.method public getMissionTipList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionTipList:Ljava/util/List;

    return-object v0
.end method

.method public getSelectFrequencyDays()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mSelectFrequencyDays:I

    return v0
.end method

.method public getStartDateTimeMillis()J
    .locals 4

    .prologue
    .line 245
    iget-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->startDateTimeMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 246
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 248
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->startDateTimeMillis:J

    goto :goto_0
.end method

.method public getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mTrackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    return-object v0
.end method

.method public getWhatToDo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getExtraString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWhyDoIt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhyDoIt:Ljava/lang/String;

    return-object v0
.end method

.method public isMissionRepeatable()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionRepeatable:Z

    return v0
.end method

.method public isTrackerMission()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mTrackerMission:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 346
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->readFromParcel(Landroid/os/Parcel;)V

    .line 348
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mGoalID:I

    .line 349
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDefID:I

    .line 350
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMin:I

    .line 351
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMax:I

    .line 352
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDef:Ljava/lang/String;

    .line 353
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionProgressStatus:Ljava/lang/String;

    .line 354
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mSelectedFrequency:Ljava/lang/String;

    .line 355
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhatToDo:Ljava/lang/String;

    .line 356
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhatToDoImage:Ljava/lang/String;

    .line 357
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhyDoIt:Ljava/lang/String;

    .line 358
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhyDoItImage:Ljava/lang/String;

    .line 359
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionRepeatable:Z

    .line 360
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mTrackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 361
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 362
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 363
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionTipList:Ljava/util/List;

    .line 364
    return-void

    .line 359
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 0
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 230
    return-void
.end method

.method public setFrequencyDef(Ljava/lang/String;)V
    .locals 0
    .param p1, "frequencyDef"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDef:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setFrequencyDefID(I)V
    .locals 0
    .param p1, "frequencyDefID"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDefID:I

    .line 90
    return-void
.end method

.method public setFrequencyMax(I)V
    .locals 0
    .param p1, "frequencyMax"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMax:I

    .line 114
    return-void
.end method

.method public setFrequencyMin(I)V
    .locals 0
    .param p1, "frequencyMin"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMin:I

    .line 98
    return-void
.end method

.method public setGoalID(I)V
    .locals 0
    .param p1, "goalID"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mGoalID:I

    .line 110
    return-void
.end method

.method public setMissionDetails(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "missionDetails":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionDetails:Ljava/util/ArrayList;

    .line 296
    return-void
.end method

.method public setMissionProgressStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressStatus"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionProgressStatus:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public setMissionProgressStatusImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressStatusImage"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionProgressStatusImage:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setMissionProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V
    .locals 0
    .param p1, "missionProgressStatusType"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 155
    return-void
.end method

.method public setMissionRepeatable(Z)V
    .locals 0
    .param p1, "missionRepeatable"    # Z

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionRepeatable:Z

    .line 163
    return-void
.end method

.method public setMissionStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V
    .locals 0
    .param p1, "missionStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 281
    return-void
.end method

.method public setMissionTipList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionTip;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "missionTipList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    if-eqz p1, :cond_0

    .line 217
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionTipList:Ljava/util/List;

    .line 218
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/MissionTip;

    .line 219
    .local v1, "tip":Lcom/cigna/coach/apiobjects/MissionTip;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionTipList:Ljava/util/List;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/MissionTip;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/MissionTip;->getTip()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 222
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "tip":Lcom/cigna/coach/apiobjects/MissionTip;
    :cond_0
    return-void
.end method

.method public setSelectFrequencyDays(I)V
    .locals 0
    .param p1, "selectFrequencyDays"    # I

    .prologue
    .line 241
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mSelectFrequencyDays:I

    .line 242
    return-void
.end method

.method public setSelectedFrequency(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedFrequency"    # Ljava/lang/String;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mSelectedFrequency:Ljava/lang/String;

    .line 234
    return-void
.end method

.method public setSpecificTracker(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)V
    .locals 0
    .param p1, "specificTracker"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mSpecificTracker:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 320
    return-void
.end method

.method public setStartDateTimeMillis(J)V
    .locals 0
    .param p1, "startDateTimeMillis"    # J

    .prologue
    .line 252
    iput-wide p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->startDateTimeMillis:J

    .line 253
    return-void
.end method

.method public setTrackerMission(Z)V
    .locals 0
    .param p1, "trackerMission"    # Z

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mTrackerMission:Z

    .line 171
    return-void
.end method

.method public setTrackerType(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)V
    .locals 0
    .param p1, "trackerType"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mTrackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 312
    return-void
.end method

.method public setWhatToDo(Ljava/lang/String;)V
    .locals 0
    .param p1, "whatToDo"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhatToDo:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public setWhatToDoImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "whatToDoImage"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhatToDoImage:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setWhyDoIt(Ljava/lang/String;)V
    .locals 0
    .param p1, "whyDoIt"    # Ljava/lang/String;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhyDoIt:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setWhyDoItImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "whyDoItImage"    # Ljava/lang/String;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhyDoItImage:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 324
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 326
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mGoalID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 327
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDefID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 328
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyMax:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mFrequencyDef:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionProgressStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mSelectedFrequency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhatToDo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhatToDoImage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhyDoIt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mWhyDoItImage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 337
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionRepeatable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mTrackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->mMissionTipList:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 342
    return-void

    .line 337
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
