.class Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;
.super Landroid/os/CountDownTimer;
.source "UvStateReady.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/state/UvStateReady;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/state/UvStateReady;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 60
    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDummyReadyTimer, onFinish() : isInCurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateReady;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->clearForNextState()V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 66
    return-void
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 52
    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDummyReadyTimer, onTick() : isInCurrentState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateReady;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateReady;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;->cancel()V

    .line 56
    :cond_0
    return-void
.end method
