.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;
.super Ljava/lang/Object;
.source "CignaImageCacheTool.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mHeight:I

.field private mMemoryCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(FIIZ)V
    .locals 3
    .param p1, "pRatio"    # F
    .param p2, "pWidth"    # I
    .param p3, "pHeight"    # I
    .param p4, "pixelUse"    # Z

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->setCacheRatio(F)V

    .line 23
    if-eqz p4, :cond_0

    .line 24
    iput p2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mWidth:I

    .line 25
    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mHeight:I

    .line 30
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    return-void

    .line 27
    :cond_0
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->convertDpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mWidth:I

    .line 28
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->convertDpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mHeight:I

    goto :goto_0
.end method

.method private addBitmapToMemoryCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 48
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->getBitmapFromMemCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_0
    return-void
.end method

.method private getBitmapFromMemCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private setCacheRatio(F)V
    .locals 6
    .param p1, "pRatio"    # F

    .prologue
    .line 34
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 36
    .local v1, "maxMemory":I
    int-to-float v2, v1

    mul-float/2addr v2, p1

    float-to-int v0, v2

    .line 37
    .local v0, "cacheSize":I
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mMemoryCache:Landroid/support/v4/util/LruCache;

    .line 45
    return-void
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mMemoryCache:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 108
    :cond_0
    return-void
.end method

.method public convertDpToPixel(I)I
    .locals 4
    .param p1, "dp"    # I

    .prologue
    .line 111
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    .local v0, "r":Landroid/content/res/Resources;
    const/4 v1, 0x1

    int-to-float v2, p1

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public loadBitmap(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "pRes"    # I

    .prologue
    .line 58
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->getBitmapFromMemCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 59
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 60
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->TAG:Ljava/lang/String;

    const-string v3, "loadBitmap from cache"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 66
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v1

    .line 63
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->TAG:Ljava/lang/String;

    const-string v3, "loadBitmap from resource"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mWidth:I

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->mHeight:I

    invoke-static {p1, p2, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaBitmapUtils;->decodeSampledBitmapFromResource(Landroid/content/res/Resources;III)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 65
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->addBitmapToMemoryCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    move-object v1, v0

    .line 66
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method
