.class Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$1;
.super Ljava/lang/Object;
.source "FavoritesListActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortFavoriteListByTitle(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;)I
    .locals 2
    .param p1, "object1"    # Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .param p2, "object2"    # Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .prologue
    .line 210
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 206
    check-cast p1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$1;->compare(Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;)I

    move-result v0

    return v0
.end method
