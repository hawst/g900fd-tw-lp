.class Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;
.super Ljava/lang/Object;
.source "HeartrateActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0

    .prologue
    .line 679
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 683
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 684
    .local v0, "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 701
    :cond_0
    :goto_0
    return-void

    .line 686
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->onLogSelected()V

    goto :goto_0

    .line 689
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->OnConnectivityActivity()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$700(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    goto :goto_0

    .line 692
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.heartrate"

    const-string v3, "HR11"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mGraphFragment:Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getChartReadyToShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$12;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->prepareShareView()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$900(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    goto :goto_0

    .line 684
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
