.class Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$2;
.super Ljava/lang/Object;
.source "LogManager.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0

    .prologue
    .line 1454
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$2;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/app/Activity;
    .param p2, "arg1"    # Landroid/os/Bundle;

    .prologue
    .line 1458
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "arg0"    # Landroid/app/Activity;

    .prologue
    .line 1462
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 9
    .param p1, "arg0"    # Landroid/app/Activity;

    .prologue
    .line 1466
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1900()Z

    move-result v7

    if-eqz v7, :cond_0

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2100(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1468
    :try_start_0
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2100(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-result-object v7

    invoke-interface {v7, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->activityStop(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1474
    :cond_0
    :goto_0
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$200()Z

    move-result v7

    if-eqz v7, :cond_1

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2200(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1476
    :try_start_1
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2200(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    move-result-object v7

    invoke-interface {v7, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;->onPause(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1483
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1484
    .local v2, "componentName":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 1508
    :cond_2
    :goto_2
    return-void

    .line 1469
    .end local v2    # "componentName":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1470
    .local v3, "e":Ljava/lang/Exception;
    const-string v7, "LogManager"

    const-string v8, "(GA) Failed to send activity stop event."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 1471
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1477
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 1478
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string v7, "LogManager"

    const-string v8, "(BA) Failed to send activity stop event."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 1479
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1488
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "componentName":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    .line 1489
    .local v4, "length":I
    if-lez v4, :cond_2

    .line 1493
    const-string v7, "."

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 1494
    .local v6, "startIndex":I
    if-lez v6, :cond_2

    if-ge v6, v4, :cond_2

    .line 1498
    add-int/lit8 v7, v6, 0x1

    add-int/lit8 v8, v4, -0x1

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1500
    .local v0, "activityName":Ljava/lang/String;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1501
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v7, "activity"

    invoke-virtual {v1, v7, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1503
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1504
    .local v5, "msg":Landroid/os/Message;
    invoke-virtual {v5, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1505
    const/16 v7, 0xa

    iput v7, v5, Landroid/os/Message;->what:I

    .line 1507
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 9
    .param p1, "arg0"    # Landroid/app/Activity;

    .prologue
    .line 1512
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$1900()Z

    move-result v7

    if-eqz v7, :cond_0

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2100(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1514
    :try_start_0
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2100(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-result-object v7

    invoke-interface {v7, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->activityStart(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1520
    :cond_0
    :goto_0
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$200()Z

    move-result v7

    if-eqz v7, :cond_1

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2200(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1522
    :try_start_1
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2200(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    move-result-object v7

    invoke-interface {v7, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;->onResume(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1529
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1530
    .local v2, "componentName":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 1554
    :cond_2
    :goto_2
    return-void

    .line 1515
    .end local v2    # "componentName":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1516
    .local v3, "e":Ljava/lang/Exception;
    const-string v7, "LogManager"

    const-string v8, "(GA) Failed to send activity start event."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 1517
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1523
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 1524
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string v7, "LogManager"

    const-string v8, "(BA) Failed to send activity stop event."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 1525
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1534
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "componentName":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    .line 1535
    .local v4, "length":I
    if-lez v4, :cond_2

    .line 1539
    const-string v7, "."

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 1540
    .local v6, "startIndex":I
    if-lez v6, :cond_2

    if-ge v6, v4, :cond_2

    .line 1544
    add-int/lit8 v7, v6, 0x1

    add-int/lit8 v8, v4, -0x1

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1546
    .local v0, "activityName":Ljava/lang/String;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1547
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v7, "activity"

    invoke-virtual {v1, v7, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 1550
    .local v5, "msg":Landroid/os/Message;
    invoke-virtual {v5, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1551
    const/16 v7, 0x9

    iput v7, v5, Landroid/os/Message;->what:I

    .line 1553
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v7

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->access$800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/app/Activity;
    .param p2, "arg1"    # Landroid/os/Bundle;

    .prologue
    .line 1558
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "arg0"    # Landroid/app/Activity;

    .prologue
    .line 1562
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "arg0"    # Landroid/app/Activity;

    .prologue
    .line 1566
    return-void
.end method
