.class public Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;
.super Ljava/lang/Object;
.source "FullVersionCountryTable.java"


# static fields
.field public static final LOG_DELIMITER:Ljava/lang/String; = "-----<<<<<"

.field private static final TAG:Ljava/lang/String;

.field public static final disableCignaMCC:[Ljava/lang/String;

.field public static final useCignaCountryLanguageNames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final useCignaLanguageName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final useCignaModelName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 15
    const-class v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->TAG:Ljava/lang/String;

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    .line 23
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "238"

    aput-object v1, v0, v5

    const-string v1, "242"

    aput-object v1, v0, v4

    const-string v1, "244"

    aput-object v1, v0, v6

    const-string v1, "274"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->disableCignaMCC:[Ljava/lang/String;

    .line 194
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "US"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "GB"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "FR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "DE"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "IT"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "ES"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "KR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "AU"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "ZA"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "PH"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "SG"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "CN"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "AE"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "JP"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "HK"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "SE"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "BR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "CA"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "NL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "TR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "IN"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "RU"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "MX"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "MY"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "AR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "TH"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "PA"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "ID"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "AT"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "CH"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "IL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "PL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "CO"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "CL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "TW"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    const-string v1, "NZ"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "en_US"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "es_US"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "en_GB"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "fr_FR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "de_DE"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "it_IT"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "es_ES"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "ko_KR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "ar_AE"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "ar_IL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "ja_JP"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "sv_SE"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "pt_BR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "fr_CA"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "nl_NL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "tr_TR"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "ru_RU"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "ms_MY"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "th_TH"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "in_ID"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "iw_IL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "zh_CN"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "pl_PL"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "zh_HK"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string/jumbo v1, "zh_TW"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    const-string v1, "hi_IN"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "US"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en_US"

    aput-object v3, v2, v5

    const-string v3, "es_US"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "CN"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "zh_CN"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "KR"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "ko"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "AE"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "ar"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "GB"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "DE"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "de_DE"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "JP"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "ja"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "IT"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "it"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "FR"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "fr_FR"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "AU"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "HK"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "zh_HK"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "SE"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "sv_SE"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "BR"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "pt_BR"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "CA"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en_US"

    aput-object v3, v2, v5

    const-string v3, "fr_CA"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "ES"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "es_ES"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "ZA"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "NL"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "nl_NL"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "TR"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "tr"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "IN"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    const-string v3, "hi_IN"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "RU"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "ru"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "MX"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "es_US"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "MY"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "en_US"

    aput-object v3, v2, v5

    const-string/jumbo v3, "zh_CN"

    aput-object v3, v2, v4

    const-string/jumbo v3, "ms"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "TW"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "zh_TW"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "AR"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "es_US"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "SG"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "TH"

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "th"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "PA"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en_US"

    aput-object v3, v2, v5

    const-string v3, "es_US"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "CH"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "en"

    aput-object v3, v2, v5

    const-string v3, "de"

    aput-object v3, v2, v4

    const-string v3, "fr"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "ID"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "in"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "AT"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "de"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "IL"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "iw"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "PH"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "CO"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "es_US"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "CL"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "es_US"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "PL"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    const-string/jumbo v3, "pl"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaCountryLanguageNames:Ljava/util/HashMap;

    const-string v1, "NZ"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "en_GB"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 503
    const/4 v2, 0x0

    .line 507
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 508
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 509
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 511
    sget-object v5, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "country code:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 513
    :catch_0
    move-exception v3

    .line 515
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->logThrowable(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getMcc()Ljava/lang/String;
    .locals 3

    .prologue
    .line 938
    const-string v1, "gsm.sim.operator.numeric"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 939
    .local v0, "retVal":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 940
    const-string v1, ""

    .line 942
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static isCignaSupportModel()Z
    .locals 4

    .prologue
    .line 570
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 571
    .local v0, "model":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device model number: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    const-string v1, "SM-G900"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G901"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G906"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G908"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G9008"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N915"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N910"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N9100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N9106"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N9109"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N916S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N916K"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N916L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 589
    :cond_0
    const/4 v1, 0x1

    .line 591
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUseCigna()Z
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 598
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isCignaSupportModel()Z

    move-result v9

    if-nez v9, :cond_0

    move v9, v10

    .line 643
    .local v2, "cscCode":Ljava/lang/String;
    :goto_0
    return v9

    .line 601
    .end local v2    # "cscCode":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    .line 602
    .restart local v2    # "cscCode":Ljava/lang/String;
    if-nez v2, :cond_1

    move v9, v10

    .line 603
    goto :goto_0

    .line 606
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 607
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 610
    .local v1, "countryCode":Ljava/lang/String;
    const-string v9, "SE"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 611
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->getMcc()Ljava/lang/String;

    move-result-object v7

    .line 612
    .local v7, "mcc":Ljava/lang/String;
    if-nez v7, :cond_2

    move v9, v10

    .line 613
    goto :goto_0

    .line 614
    :cond_2
    sget-object v9, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->disableCignaMCC:[Ljava/lang/String;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    move v9, v10

    .line 615
    goto :goto_0

    .line 618
    .end local v7    # "mcc":Ljava/lang/String;
    :cond_3
    sget-object v9, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    invoke-virtual {v9, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    .line 619
    .local v5, "keyPresent":Z
    if-eqz v5, :cond_7

    .line 620
    sget-object v9, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaModelName:Ljava/util/HashMap;

    invoke-virtual {v9, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 621
    .local v3, "isSupportedCountry":Z
    sget-object v9, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Country: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isSupportedCountry: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget-object v9, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    .line 624
    .local v8, "systemLang":Ljava/lang/String;
    if-eqz v8, :cond_7

    .line 625
    const/4 v4, 0x0

    .line 626
    .local v4, "isSupportedLanguage":Z
    const-string v9, "en_"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    const-string v9, "de_"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 627
    :cond_4
    const/4 v4, 0x1

    .line 634
    :cond_5
    :goto_1
    sget-object v9, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Locale :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v12

    iget-object v12, v12, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "  system Lang :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isSupportedLanguage: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    if-eqz v3, :cond_7

    if-eqz v4, :cond_7

    .line 638
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 629
    :cond_6
    sget-object v9, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    invoke-virtual {v9, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    .line 630
    .local v6, "langKeyPresent":Z
    if-eqz v6, :cond_5

    .line 631
    sget-object v9, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->useCignaLanguageName:Ljava/util/HashMap;

    invoke-virtual {v9, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    goto :goto_1

    .end local v3    # "isSupportedCountry":Z
    .end local v4    # "isSupportedLanguage":Z
    .end local v6    # "langKeyPresent":Z
    .end local v8    # "systemLang":Ljava/lang/String;
    :cond_7
    move v9, v10

    .line 643
    goto/16 :goto_0
.end method

.method public static logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 681
    invoke-static {p0, p1, p2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 682
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 683
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_0

    .line 685
    const-string v1, "-----<<<<<Was caused by"

    invoke-static {p0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 688
    :cond_0
    return-void
.end method

.method public static logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 668
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-----<<<<<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->logThrowable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 669
    return-void
.end method

.method public static logThrowable(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 656
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 657
    return-void
.end method
