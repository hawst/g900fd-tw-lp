.class public final Lcom/sec/android/app/shealth/framework/repository/common/DBConstants$WALKING_TYPE;
.super Ljava/lang/Object;
.source "DBConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WALKING_TYPE"
.end annotation


# static fields
.field public static final DATA_VIEW_ONLY_GEAR:I = 0x1

.field public static final DATA_VIEW_ONLY_PHONE:I = 0x0

.field public static final DATA_VIEW_TOTAL_WITHOUT_DUPLICATION:I = 0x3

.field public static final DATA_VIEW_TOTAL_WITH_DUPLICATION:I = 0x2

.field public static final ID:I = 0x8

.field public static final KCAL_UNIT:Ljava/lang/String; = "kcal"

.field public static final RUNNING:I = 0x4

.field public static final RUSH:I = 0x5

.field public static final STAY:I = 0x0

.field public static final UNIT_KM:Ljava/lang/String; = "km"

.field public static final UNIT_MILE:Ljava/lang/String; = "mile"

.field public static final UNIT_STEPS:Ljava/lang/String; = "steps"

.field public static final UPDOWN:I = 0x6

.field public static final WALKING:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
