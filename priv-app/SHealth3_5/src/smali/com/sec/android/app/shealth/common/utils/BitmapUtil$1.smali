.class final Lcom/sec/android/app/shealth/common/utils/BitmapUtil$1;
.super Ljava/lang/Object;
.source "BitmapUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->prepareGraphScreenshot(Landroid/graphics/Bitmap;Landroid/widget/LinearLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$generalViewLeft:I

.field final synthetic val$generalViewTop:I


# direct methods
.method constructor <init>(II)V
    .locals 0

    .prologue
    .line 195
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/BitmapUtil$1;->val$generalViewLeft:I

    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/BitmapUtil$1;->val$generalViewTop:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 200
    # getter for: Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->chartBitmap:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->access$000()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    # getter for: Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->canvas:Landroid/graphics/Canvas;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->access$100()Landroid/graphics/Canvas;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->chartBitmap:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->access$000()Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/common/utils/BitmapUtil$1;->val$generalViewLeft:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/common/utils/BitmapUtil$1;->val$generalViewTop:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 203
    # getter for: Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->chartBitmap:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->access$000()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 205
    # setter for: Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->chartBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->access$002(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 207
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_1

    .line 209
    # getter for: Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->canvas:Landroid/graphics/Canvas;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->access$100()Landroid/graphics/Canvas;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Canvas;->release()V

    .line 211
    :cond_1
    # setter for: Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->canvas:Landroid/graphics/Canvas;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->access$102(Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 213
    return-void
.end method
