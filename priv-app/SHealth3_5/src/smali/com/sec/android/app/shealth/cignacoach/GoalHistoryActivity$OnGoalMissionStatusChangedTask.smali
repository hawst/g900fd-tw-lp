.class Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
.super Landroid/os/AsyncTask;
.source "GoalHistoryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnGoalMissionStatusChangedTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mNewCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

.field private mNewGoalDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mNewMissionDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)V
    .locals 1

    .prologue
    .line 252
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 253
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    .line 254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 252
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getHistoryListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    .line 262
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 252
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 267
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->mNewCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$202(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->setAdapter()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)V

    .line 285
    :cond_2
    return-void
.end method
