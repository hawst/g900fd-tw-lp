.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$26;
.super Landroid/text/style/ClickableSpan;
.source "HeartrateSummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setFinishedSpannableText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 2332
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "textView"    # Landroid/view/View;

    .prologue
    .line 2335
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationFragment;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2336
    .local v0, "inf_intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startActivity(Landroid/content/Intent;)V

    .line 2337
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$3100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->playSoundEffect(I)V

    .line 2338
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1
    .param p1, "ds"    # Landroid/text/TextPaint;

    .prologue
    .line 2342
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2343
    const v0, -0xff852e

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2344
    return-void
.end method
