.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$3;
.super Ljava/util/LinkedHashMap;
.source "ExerciseProBitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;IFZ)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # F
    .param p4, "x2"    # Z

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader$3;->size()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 152
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->sSoftBitmapCache:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->access$400()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    const/4 v0, 0x1

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
