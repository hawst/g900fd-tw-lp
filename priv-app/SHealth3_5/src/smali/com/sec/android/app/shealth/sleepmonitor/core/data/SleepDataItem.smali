.class public Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;
.super Ljava/lang/Object;
.source "SleepDataItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private bed_time:J

.field private comment:Ljava/lang/String;

.field private device_id:Ljava/lang/String;

.field private quality:F

.field private rise_time:J

.field private sleep_qualty:F


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "mCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->quality:F

    .line 19
    if-eqz p1, :cond_1

    .line 20
    const-string v2, "bed_time"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->bed_time:J

    .line 21
    const-string/jumbo v2, "rise_time"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->rise_time:J

    .line 22
    iget-wide v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->bed_time:J

    iget-wide v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->rise_time:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 25
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->bed_time:J

    .line 26
    .local v0, "swapper":J
    iget-wide v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->rise_time:J

    iput-wide v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->bed_time:J

    .line 27
    iput-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->rise_time:J

    .line 30
    .end local v0    # "swapper":J
    :cond_0
    const-string/jumbo v2, "quality"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->quality:F

    .line 31
    const-string v2, "efficiency"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->sleep_qualty:F

    .line 32
    const-string v2, "comment"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->setComment(Ljava/lang/String;)V

    .line 34
    const-string/jumbo v2, "user_device__id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, "10019"

    :goto_0
    iput-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->device_id:Ljava/lang/String;

    .line 35
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->device_id:Ljava/lang/String;

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v6

    if-eqz v2, :cond_1

    const-string v2, ""

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->device_id:Ljava/lang/String;

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    const-string v2, "10019"

    iput-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->device_id:Ljava/lang/String;

    .line 38
    :cond_1
    return-void

    .line 34
    :cond_2
    const-string/jumbo v2, "user_device__id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public getBed_time()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->bed_time:J

    return-wide v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->comment:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->comment:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->comment:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDevice_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->device_id:Ljava/lang/String;

    return-object v0
.end method

.method public getQuality()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->quality:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getRise_time()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->rise_time:J

    return-wide v0
.end method

.method public getSleep_qualty()F
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->sleep_qualty:F

    return v0
.end method

.method public getTotalSleepTime()J
    .locals 4

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->rise_time:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->bed_time:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->comment:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 83
    return-void
.end method
