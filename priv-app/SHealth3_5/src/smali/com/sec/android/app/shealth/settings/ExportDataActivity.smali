.class public Lcom/sec/android/app/shealth/settings/ExportDataActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExportDataActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final EXPORT_COMPLETE_POPUP:Ljava/lang/String; = "export_complete_popup"

.field private static final EXPORT_DATA_POPUP:Ljava/lang/String; = "export_data_popup"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private actionButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private bg:Z

.field private bp:Z

.field private checkBoxBg:Landroid/widget/CheckBox;

.field private checkBoxBp:Landroid/widget/CheckBox;

.field private checkBoxHr:Landroid/widget/CheckBox;

.field private checkBoxSr:Landroid/widget/CheckBox;

.field private completeProgress:Z

.field private data:I

.field private exportDataRunnable:Ljava/lang/Runnable;

.field private exportHandler:Landroid/os/Handler;

.field private hr:Z

.field private mBackupProgress:Landroid/widget/TextView;

.field private mExportPath:Landroid/widget/TextView;

.field private mExportProgressBar:Landroid/widget/ProgressBar;

.field private mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mProgressBroadcastReceiver:Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;

.field private mTextPercentage:Landroid/widget/TextView;

.field private migrationState:I

.field private sr:Z

.field public unsupportedFeatureList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 86
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->sr:Z

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->completeProgress:Z

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->unsupportedFeatureList:Ljava/util/ArrayList;

    .line 96
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUpgradeStatus(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->migrationState:I

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mProgressBroadcastReceiver:Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;

    .line 300
    iput v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I

    .line 322
    new-instance v0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportDataRunnable:Ljava/lang/Runnable;

    .line 340
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->completeProgress:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->completeProgress:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->showExportDataPopup()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportPath:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportPath:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mBackupProgress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mBackupProgress:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->stopExport()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bg:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bp:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->hr:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->sr:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->startExport()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/ExportDataActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;
    .param p1, "x1"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mTextPercentage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mTextPercentage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;
    .param p1, "x1"    # Landroid/widget/ProgressBar;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBar:Landroid/widget/ProgressBar;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportDataRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->completeExportPopup()V

    return-void
.end method

.method private completeExportPopup()V
    .locals 5

    .prologue
    .line 344
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 345
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v3, 0x7f0907e9

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 346
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v3, 0x7f09087e

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 347
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v3, 0x7f090047

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v3, 0x7f0300bc

    new-instance v4, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 360
    new-instance v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    .line 384
    .local v1, "okListener":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;
    new-instance v0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    .line 394
    .local v0, "cancelButtonClickListener":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 395
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 396
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "export_complete_popup"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method private dataCheck()V
    .locals 7

    .prologue
    const v6, 0x7f08037a

    const v5, 0x7f080376

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 220
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getBloodPressureData()I

    move-result v0

    if-lez v0, :cond_0

    .line 221
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] getBloodPressureData is present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 223
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getBloodGlucoseData()I

    move-result v0

    if-lez v0, :cond_1

    .line 231
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] getBloodGlucoseData is present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 233
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    :goto_1
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getStressData()I

    move-result v0

    if-lez v0, :cond_2

    .line 241
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] getStressData is present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const v0, 0x7f080382

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 243
    const v0, 0x7f080385

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 244
    const v0, 0x7f080382

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 245
    const v0, 0x7f080382

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    :goto_2
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 251
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] mHeartrateDatabaseHelper is present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    const v0, 0x7f08037e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 253
    const v0, 0x7f080381

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 254
    const v0, 0x7f08037e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 255
    const v0, 0x7f08037e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    :goto_3
    return-void

    .line 225
    :cond_0
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] getBloodPressureData is not present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const v0, 0x7f08037d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 227
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 235
    :cond_1
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] getBloodGlucoseData is not present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const v0, 0x7f080379

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 237
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 247
    :cond_2
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] getStressData is not present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 257
    :cond_3
    const-string v0, "SETTINGSFRAGMENT"

    const-string v1, "[EXPORT] mHeartrateDatabaseHelper is not present "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private getBloodGlucoseData()I
    .locals 7

    .prologue
    .line 275
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 276
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 278
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 279
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 281
    if-eqz v6, :cond_0

    .line 282
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 281
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 282
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private getBloodPressureData()I
    .locals 7

    .prologue
    .line 288
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 289
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 291
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 292
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 294
    if-eqz v6, :cond_0

    .line 295
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 294
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 295
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private getStressData()I
    .locals 7

    .prologue
    .line 262
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 263
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 265
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 266
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 268
    if-eqz v6, :cond_0

    .line 269
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 268
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 269
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private initializeDbResetableAppList()V
    .locals 1

    .prologue
    .line 142
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 144
    const v0, 0x7f080378

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBg:Landroid/widget/CheckBox;

    .line 145
    const v0, 0x7f08037c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBp:Landroid/widget/CheckBox;

    .line 146
    const v0, 0x7f080380

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxHr:Landroid/widget/CheckBox;

    .line 147
    const v0, 0x7f080384

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxSr:Landroid/widget/CheckBox;

    .line 149
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->dataCheck()V

    .line 150
    return-void
.end method

.method private showExportDataPopup()V
    .locals 3

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    :goto_0
    return-void

    .line 406
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f0907e9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v1, 0x7f0300be

    new-instance v2, Lcom/sec/android/app/shealth/settings/ExportDataActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$6;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$7;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "export_data_popup"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startExport()V
    .locals 6

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "date_format"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 454
    .local v2, "sysdateformat":Ljava/lang/String;
    const-string v3, "kkmmss"

    .line 456
    .local v3, "time":Ljava/lang/String;
    new-instance v1, Ljava/text/SimpleDateFormat;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 457
    .local v1, "formatter":Ljava/text/DateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 459
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;

    invoke-direct {v5, p0, v1, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Ljava/text/DateFormat;Ljava/util/Calendar;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 494
    return-void
.end method

.method private stopExport()V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 444
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportDataRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 447
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 201
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f0907e9

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 204
    new-instance v0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    .line 213
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f090044

    invoke-direct {v1, v4, v2, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->actionButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->actionButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    .line 216
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 500
    const-string v0, "export_data_popup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    new-instance v0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$9;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    .line 517
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 155
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 157
    .local v0, "v":I
    sparse-switch v0, :sswitch_data_0

    .line 192
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bg:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bp:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->hr:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->sr:Z

    if-eqz v1, :cond_9

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->enableActionBarButton(I)V

    .line 197
    :goto_1
    return-void

    .line 160
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBg:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBg:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBg:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 162
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bg:Z

    goto :goto_0

    :cond_1
    move v1, v3

    .line 160
    goto :goto_2

    .line 164
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bg:Z

    goto :goto_0

    .line 167
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBp:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBp:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxBp:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 169
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bp:Z

    goto :goto_0

    :cond_3
    move v1, v3

    .line 167
    goto :goto_3

    .line 171
    :cond_4
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bp:Z

    goto :goto_0

    .line 174
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxHr:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxHr:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_4
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxHr:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 176
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->hr:Z

    goto :goto_0

    :cond_5
    move v1, v3

    .line 174
    goto :goto_4

    .line 178
    :cond_6
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->hr:Z

    goto :goto_0

    .line 181
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxSr:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxSr:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_5
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->checkBoxSr:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 183
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->sr:Z

    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 181
    goto :goto_5

    .line 185
    :cond_8
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->sr:Z

    goto/16 :goto_0

    .line 195
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    goto/16 :goto_1

    .line 157
    :sswitch_data_0
    .sparse-switch
        0x7f080376 -> :sswitch_0
        0x7f08037a -> :sswitch_1
        0x7f08037e -> :sswitch_2
        0x7f080382 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    const v0, 0x7f0300bd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->setContentView(I)V

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->initializeDbResetableAppList()V

    .line 106
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mProgressBroadcastReceiver:Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 138
    return-void

    .line 132
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mProgressBroadcastReceiver:Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;

    if-nez v1, :cond_0

    .line 114
    new-instance v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;-><init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mProgressBroadcastReceiver:Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;

    .line 117
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 118
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.shealth.DATA_EXPORT_PROGRESS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mProgressBroadcastReceiver:Lcom/sec/android/app/shealth/settings/ExportDataActivity$ProgressBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 121
    return-void
.end method
