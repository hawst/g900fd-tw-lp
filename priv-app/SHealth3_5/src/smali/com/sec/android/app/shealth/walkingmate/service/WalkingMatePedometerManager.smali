.class public Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;
.super Ljava/lang/Object;
.source "WalkingMatePedometerManager.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# static fields
.field public static final PEDOMETER_STEP_STATUS_MARK:I = 0x1

.field public static final PEDOMETER_STEP_STATUS_RUN:I = 0x4

.field public static final PEDOMETER_STEP_STATUS_RUN_DOWN:I = 0x9

.field public static final PEDOMETER_STEP_STATUS_RUN_UP:I = 0x8

.field public static final PEDOMETER_STEP_STATUS_RUSH:I = 0x5

.field public static final PEDOMETER_STEP_STATUS_STOP:I = 0x0

.field public static final PEDOMETER_STEP_STATUS_STROLL:I = 0x2

.field public static final PEDOMETER_STEP_STATUS_UNKNOWN:I = -0x1

.field public static final PEDOMETER_STEP_STATUS_WALK:I = 0x3

.field public static final PEDOMETER_STEP_STATUS_WALK_DOWN:I = 0x7

.field public static final PEDOMETER_STEP_STATUS_WALK_UP:I = 0x6

.field public static final SAMPLE_NOT_DEFINED:I = 0x0

.field public static final STEP_TYPE_DOWN:I = 0x7

.field public static final STEP_TYPE_MARK:I = 0x1

.field public static final STEP_TYPE_RUN:I = 0x4

.field public static final STEP_TYPE_RUSH:I = 0x5

.field public static final STEP_TYPE_STOP:I = 0x0

.field public static final STEP_TYPE_STROLL:I = 0x2

.field public static final STEP_TYPE_UNKNOWN:I = -0x1

.field public static final STEP_TYPE_UP:I = 0x6

.field public static final STEP_TYPE_WALK:I = 0x3

.field private static mManager:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;


# instance fields
.field private final TAG:Ljava/lang/String;

.field aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

.field private apiVersion:I

.field private bListenerRegistered:Z

.field private final gapOfStep:I

.field private last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

.field private mContext:Landroid/content/Context;

.field private mGender:I

.field private mHeight:F

.field private mPedometerFeatureLevel:I

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;

.field private mWeight:F

.field private mbSContextDiffMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "WalkingMatePedometerManager"

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->TAG:Ljava/lang/String;

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mPedometerFeatureLevel:I

    .line 51
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mGender:I

    .line 52
    const/high16 v0, 0x432a0000    # 170.0f

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mHeight:F

    .line 53
    const/high16 v0, 0x42820000    # 65.0f

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mWeight:F

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->bListenerRegistered:Z

    .line 55
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->apiVersion:I

    .line 57
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mbSContextDiffMode:Z

    .line 59
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->gapOfStep:I

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "scontext"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->detectPedometerMode()V

    .line 105
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    .line 106
    return-void
.end method

.method private checkUserDeviceID(Ljava/lang/String;)V
    .locals 10
    .param p1, "_deviceID"    # Ljava/lang/String;

    .prologue
    .line 165
    const-string v3, "_id=?"

    .line 166
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 169
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 171
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 173
    if-eqz v6, :cond_1

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    if-eqz v6, :cond_0

    .line 182
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    if-eqz v6, :cond_2

    .line 182
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 186
    :cond_2
    :try_start_1
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 187
    .local v9, "values":Landroid/content/ContentValues;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 188
    .local v8, "uniqueDeviceId":Ljava/lang/String;
    const-string v0, "device_id"

    invoke-virtual {v9, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v0, "_id"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v0, "connectivity_type"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 191
    const-string v0, "device_type"

    const/16 v1, 0x2719

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 192
    const-string/jumbo v0, "model"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v0, "custom_name"

    const-string v1, "SContext-Pedometer"

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v0, "device_group_type"

    const v1, 0x57e41

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 195
    const-string v0, "WalkingMatePedometerManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding to user_device DEVICE_ID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const-string v0, "WalkingMatePedometerManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding to user_device Model : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const-string v0, "WalkingMatePedometerManager"

    const-string v1, "Adding to user_device Group : Mobile"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 199
    .end local v8    # "uniqueDeviceId":Ljava/lang/String;
    .end local v9    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v7

    .line 200
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "WalkingMatePedometerManager"

    const-string v1, "Exception at the time of insertion in userdevice table"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 181
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 182
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private detectPedometerMode()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 255
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 258
    .local v3, "pm":Landroid/content/pm/PackageManager;
    if-eqz v3, :cond_0

    .line 259
    :try_start_0
    const-string v4, "com.sec.feature.sensorhub"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mPedometerFeatureLevel:I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :cond_0
    :goto_0
    const-string v4, "WalkingMatePedometerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "detectPedometerMode - ver : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mPedometerFeatureLevel:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " apiVersion : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->apiVersion:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mPedometerFeatureLevel:I

    const/4 v5, 0x3

    if-lt v4, v5, :cond_1

    .line 270
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mbSContextDiffMode:Z

    .line 271
    const-string v4, "WalkingMatePedometerManager"

    const-string v5, "StepHandler MODE - Diff value mode"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :goto_1
    return-void

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 262
    .local v1, "elements":[Ljava/lang/StackTraceElement;
    aget-object v4, v1, v7

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    .line 263
    .local v2, "methodName":Ljava/lang/String;
    const-string v4, "WalkingMatePedometerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "() NoSuchMethodError Exception :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mPedometerFeatureLevel:I

    goto :goto_0

    .line 273
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    .end local v1    # "elements":[Ljava/lang/StackTraceElement;
    .end local v2    # "methodName":Ljava/lang/String;
    :cond_1
    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->apiVersion:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_2

    .line 275
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mbSContextDiffMode:Z

    .line 276
    const-string v4, "WalkingMatePedometerManager"

    const-string v5, "StepHandler MODE - Diff value mode"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 278
    :cond_2
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mbSContextDiffMode:Z

    .line 279
    const-string v4, "WalkingMatePedometerManager"

    const-string v5, "StepHandler MODE - Total value mode"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mManager:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    if-nez v0, :cond_1

    .line 110
    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    monitor-enter v1

    .line 111
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mManager:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mManager:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    .line 114
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mManager:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    return-object v0

    .line 114
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getStepType(I)I
    .locals 4
    .param p1, "status"    # I

    .prologue
    const/4 v0, -0x1

    .line 644
    const-string v1, "WalkingMatePedometerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    packed-switch p1, :pswitch_data_0

    .line 676
    :goto_0
    :pswitch_0
    return v0

    .line 650
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 653
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 656
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 659
    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    .line 662
    :pswitch_5
    const/4 v0, 0x4

    goto :goto_0

    .line 665
    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    .line 669
    :pswitch_7
    const/4 v0, 0x6

    goto :goto_0

    .line 673
    :pswitch_8
    const/4 v0, 0x7

    goto :goto_0

    .line 645
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private updateWflDbByDiffValue(Landroid/hardware/scontext/SContextPedometer;)V
    .locals 28
    .param p1, "pedometerContext"    # Landroid/hardware/scontext/SContextPedometer;

    .prologue
    .line 285
    const/4 v14, 0x0

    .line 288
    .local v14, "mode":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getMode()I

    move-result v14

    .line 290
    const-string v1, "WalkingMatePedometerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Diff] pedometerContext.getMode() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ## 0 -> Normal_Mode ## 1 -> Logging_Mode ##"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const/4 v1, 0x1

    if-ne v14, v1, :cond_20

    .line 293
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getArraySize()I

    move-result v7

    .line 294
    .local v7, "arraySize":I
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTimeStampArray()[J

    move-result-object v20

    .line 295
    .local v20, "timestamp":[J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistanceDiffArray()[D

    move-result-object v10

    .line 296
    .local v10, "distanceDiffArray":[D
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeedArray()[D

    move-result-object v19

    .line 297
    .local v19, "speedArray":[D
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorieDiffArray()[D

    move-result-object v8

    .line 298
    .local v8, "calorieDiffArray":[D
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCountDiffArray()[J

    move-result-object v21

    .line 299
    .local v21, "totalStepDiffArray":[J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCountDiffArray()[J

    move-result-object v25

    .line 300
    .local v25, "walkFlatStepCntDiffArray":[J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCountDiffArray()[J

    move-result-object v16

    .line 302
    .local v16, "runFlatStepCntDiffArray":[J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCountDiffArray()[J

    move-result-object v27

    .line 303
    .local v27, "walkUpStepCntDiffArray":[J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCountDiffArray()[J

    move-result-object v24

    .line 304
    .local v24, "walkDownStepCntDiffArray":[J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCountDiffArray()[J

    move-result-object v18

    .line 305
    .local v18, "runUpStepCntDiffArray":[J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCountDiffArray()[J

    move-result-object v15

    .line 307
    .local v15, "runDownStepCntDiffArray":[J
    const-string v1, "WalkingMatePedometerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Diff] arraySize : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    if-lez v7, :cond_1e

    .line 310
    if-nez v20, :cond_0

    .line 311
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] timestamp is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_0
    if-nez v10, :cond_1

    .line 314
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] distanceDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_1
    if-nez v19, :cond_2

    .line 317
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] speedArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_2
    if-nez v8, :cond_3

    .line 320
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] calorieDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_3
    if-nez v21, :cond_4

    .line 323
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] totalStepDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_4
    if-nez v25, :cond_5

    .line 326
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] walkFlatStepCntDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_5
    if-nez v16, :cond_6

    .line 329
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] runFlatStepCntDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_6
    if-nez v27, :cond_7

    .line 332
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] walkUpStepCntDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_7
    if-nez v24, :cond_8

    .line 335
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] walkDownStepCntDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_8
    if-nez v18, :cond_9

    .line 338
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] runUpStepCntDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_9
    if-nez v15, :cond_a

    .line 341
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[Diff] runDownStepCntDiffArray is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_a
    new-array v9, v7, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    .line 346
    .local v9, "data":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v7, :cond_17

    .line 347
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    aput-object v1, v9, v12

    .line 348
    aget-object v1, v9, v12

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    .line 349
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    .line 350
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    .line 351
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    .line 352
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    .line 353
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    .line 354
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    .line 355
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 356
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    .line 357
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    .line 358
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    .line 359
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    .line 360
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    .line 362
    if-eqz v20, :cond_b

    .line 363
    aget-object v1, v9, v12

    aget-wide v2, v20, v12

    iput-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    .line 364
    :cond_b
    if-eqz v10, :cond_c

    .line 365
    aget-object v1, v9, v12

    aget-wide v2, v10, v12

    double-to-float v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    .line 366
    :cond_c
    if-eqz v19, :cond_d

    .line 367
    aget-object v1, v9, v12

    aget-wide v2, v19, v12

    double-to-float v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    .line 368
    :cond_d
    if-eqz v8, :cond_e

    .line 369
    aget-object v1, v9, v12

    aget-wide v2, v8, v12

    double-to-float v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    .line 370
    :cond_e
    if-eqz v21, :cond_f

    .line 371
    aget-object v1, v9, v12

    aget-wide v2, v21, v12

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    .line 372
    :cond_f
    if-eqz v25, :cond_10

    .line 373
    aget-object v1, v9, v12

    aget-wide v2, v25, v12

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    .line 374
    :cond_10
    if-eqz v16, :cond_11

    .line 375
    aget-object v1, v9, v12

    aget-wide v2, v16, v12

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    .line 377
    :cond_11
    if-eqz v27, :cond_12

    .line 378
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    int-to-long v2, v2

    aget-wide v4, v27, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 379
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    int-to-long v2, v2

    aget-wide v4, v27, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    .line 382
    :cond_12
    if-eqz v24, :cond_13

    .line 383
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    int-to-long v2, v2

    aget-wide v4, v24, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 384
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    int-to-long v2, v2

    aget-wide v4, v24, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    .line 387
    :cond_13
    if-eqz v18, :cond_14

    .line 388
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    int-to-long v2, v2

    aget-wide v4, v18, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 389
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    int-to-long v2, v2

    aget-wide v4, v18, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    .line 392
    :cond_14
    if-eqz v15, :cond_15

    .line 393
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    int-to-long v2, v2

    aget-wide v4, v15, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 394
    aget-object v1, v9, v12

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    int-to-long v2, v2

    aget-wide v4, v15, v12

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    .line 397
    :cond_15
    aget-object v1, v9, v12

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-gez v1, :cond_16

    .line 398
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus updown step is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    aget-object v1, v9, v12

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 402
    :cond_16
    const-string v1, "WalkingMatePedometerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Diff] time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " distance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " speed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " calorie = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stepStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " totalStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " upDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkUpStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runUpStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    aget-object v2, v9, v12

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    aget-object v3, v9, v12

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    aget-object v4, v9, v12

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    aget-object v5, v9, v12

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->accumulateData(IFFJ)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 346
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 415
    :cond_17
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    if-eqz v1, :cond_18

    .line 417
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    const/4 v2, 0x0

    invoke-virtual {v1, v9, v2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)Z
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_1

    .line 430
    :goto_1
    const/16 v22, 0x0

    .local v22, "totalSteps":I
    const/16 v26, 0x0

    .local v26, "walkSteps":I
    const/16 v17, 0x0

    .local v17, "runSteps":I
    const/16 v23, 0x0

    .line 431
    .local v23, "updownSteps":I
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_2
    if-ge v13, v7, :cond_1f

    .line 432
    :try_start_2
    aget-object v1, v9, v13

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int v22, v22, v1

    .line 433
    aget-object v1, v9, v13

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    add-int v26, v26, v1

    .line 434
    aget-object v1, v9, v13

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int v17, v17, v1

    .line 435
    aget-object v1, v9, v13

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I
    :try_end_2
    .catch Ljava/lang/NoSuchMethodError; {:try_start_2 .. :try_end_2} :catch_1

    add-int v23, v23, v1

    .line 431
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 421
    .end local v13    # "j":I
    .end local v17    # "runSteps":I
    .end local v22    # "totalSteps":I
    .end local v23    # "updownSteps":I
    .end local v26    # "walkSteps":I
    :cond_18
    :try_start_3
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "Aggregator is Null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 424
    :catch_0
    move-exception v11

    .line 426
    .local v11, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    :try_start_4
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "Trying to save the navlid data"

    invoke-static {v1, v2, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Ljava/lang/NoSuchMethodError; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 495
    .end local v7    # "arraySize":I
    .end local v8    # "calorieDiffArray":[D
    .end local v9    # "data":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .end local v10    # "distanceDiffArray":[D
    .end local v11    # "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    .end local v12    # "i":I
    .end local v15    # "runDownStepCntDiffArray":[J
    .end local v16    # "runFlatStepCntDiffArray":[J
    .end local v18    # "runUpStepCntDiffArray":[J
    .end local v19    # "speedArray":[D
    .end local v20    # "timestamp":[J
    .end local v21    # "totalStepDiffArray":[J
    .end local v24    # "walkDownStepCntDiffArray":[J
    .end local v25    # "walkFlatStepCntDiffArray":[J
    .end local v27    # "walkUpStepCntDiffArray":[J
    :catch_1
    move-exception v11

    .line 496
    .local v11, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "StepHandler pedometerContext.getMode() NoSuchMethodError"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    new-instance v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v9}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    .line 499
    .local v9, "data":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    .line 500
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistanceDiff()D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    .line 501
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeed()D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    .line 502
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorieDiff()D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    .line 503
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    .line 504
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    .line 505
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    .line 506
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getUpDownStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 507
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->getStepType(I)I

    move-result v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    .line 509
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    .line 510
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    .line 511
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    .line 512
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    .line 513
    const/4 v1, 0x0

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    .line 515
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-gez v1, :cond_19

    .line 516
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus updownStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :cond_19
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    if-gez v1, :cond_1a

    .line 518
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus walkUpStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :cond_1a
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    if-gez v1, :cond_1b

    .line 520
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus walkDownStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    :cond_1b
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    if-gez v1, :cond_1c

    .line 522
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus runUpStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :cond_1c
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    if-gez v1, :cond_1d

    .line 524
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus runDownStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    :cond_1d
    const-string v1, "WalkingMatePedometerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Diff] time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " distance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " speed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " calorie = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stepStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " totalStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " upDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkUpStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runUpStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    iget v2, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    iget v4, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    iget-wide v5, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->accumulateData(IFFJ)V

    .line 534
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    if-eqz v1, :cond_27

    .line 536
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    const/4 v2, 0x0

    invoke-virtual {v1, v9, v2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z

    .line 544
    :goto_3
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/lit8 v1, v1, -0xa

    iget v2, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v2, v3

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v2, v3

    if-le v1, v2, :cond_1e

    .line 545
    const-string v1, "TAG"

    const-string v2, "[StepsHandler] step count have gap"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    .end local v9    # "data":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .end local v11    # "e":Ljava/lang/NoSuchMethodError;
    :cond_1e
    :goto_4
    return-void

    .line 438
    .restart local v7    # "arraySize":I
    .restart local v8    # "calorieDiffArray":[D
    .local v9, "data":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .restart local v10    # "distanceDiffArray":[D
    .restart local v12    # "i":I
    .restart local v13    # "j":I
    .restart local v15    # "runDownStepCntDiffArray":[J
    .restart local v16    # "runFlatStepCntDiffArray":[J
    .restart local v17    # "runSteps":I
    .restart local v18    # "runUpStepCntDiffArray":[J
    .restart local v19    # "speedArray":[D
    .restart local v20    # "timestamp":[J
    .restart local v21    # "totalStepDiffArray":[J
    .restart local v22    # "totalSteps":I
    .restart local v23    # "updownSteps":I
    .restart local v24    # "walkDownStepCntDiffArray":[J
    .restart local v25    # "walkFlatStepCntDiffArray":[J
    .restart local v26    # "walkSteps":I
    .restart local v27    # "walkUpStepCntDiffArray":[J
    :cond_1f
    add-int/lit8 v1, v22, -0xa

    add-int v2, v26, v17

    add-int v2, v2, v23

    if-le v1, v2, :cond_1e

    .line 439
    :try_start_5
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[StepsHandler][logging] step count have gap"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 445
    .end local v7    # "arraySize":I
    .end local v8    # "calorieDiffArray":[D
    .end local v9    # "data":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    .end local v10    # "distanceDiffArray":[D
    .end local v12    # "i":I
    .end local v13    # "j":I
    .end local v15    # "runDownStepCntDiffArray":[J
    .end local v16    # "runFlatStepCntDiffArray":[J
    .end local v17    # "runSteps":I
    .end local v18    # "runUpStepCntDiffArray":[J
    .end local v19    # "speedArray":[D
    .end local v20    # "timestamp":[J
    .end local v21    # "totalStepDiffArray":[J
    .end local v22    # "totalSteps":I
    .end local v23    # "updownSteps":I
    .end local v24    # "walkDownStepCntDiffArray":[J
    .end local v25    # "walkFlatStepCntDiffArray":[J
    .end local v26    # "walkSteps":I
    .end local v27    # "walkUpStepCntDiffArray":[J
    :cond_20
    new-instance v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v9}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    .line 446
    .local v9, "data":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    .line 447
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistanceDiff()D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    .line 448
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeed()D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    .line 449
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorieDiff()D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    .line 450
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    .line 451
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    .line 452
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    .line 453
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getUpDownStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 454
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->getStepType(I)I

    move-result v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    .line 456
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    .line 457
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    .line 458
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    .line 459
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCountDiff()J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    .line 460
    const/4 v1, 0x0

    iput v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->samplePosition:I

    .line 462
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    if-gez v1, :cond_21

    .line 463
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus updownStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :cond_21
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    if-gez v1, :cond_22

    .line 465
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus walkUpStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    :cond_22
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    if-gez v1, :cond_23

    .line 467
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus walkDownStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    :cond_23
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    if-gez v1, :cond_24

    .line 469
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus runUpStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :cond_24
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    if-gez v1, :cond_25

    .line 471
    const-string v1, "WalkingMatePedometerManager"

    const-string/jumbo v2, "minus runDownStep is occurred"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :cond_25
    const-string v1, "WalkingMatePedometerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Diff] time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " distance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " speed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " calorie = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stepStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " totalStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " upDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkUpStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " walkDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runUpStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runDownStepCnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    iget v2, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    iget v4, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    iget-wide v5, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->accumulateData(IFFJ)V

    .line 480
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    if-eqz v1, :cond_26

    .line 482
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    const/4 v2, 0x0

    invoke-virtual {v1, v9, v2}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z

    .line 490
    :goto_5
    iget v1, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/lit8 v1, v1, -0xa

    iget v2, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v2, v3

    iget v3, v9, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v2, v3

    if-le v1, v2, :cond_1e

    .line 491
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "[StepsHandler] step count have gap"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 486
    :cond_26
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "Aggregator is Null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NoSuchMethodError; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_5

    .line 540
    .restart local v11    # "e":Ljava/lang/NoSuchMethodError;
    :cond_27
    const-string v1, "WalkingMatePedometerManager"

    const-string v2, "Aggregator is Null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method private updateWlfDbByTotalValue(Landroid/hardware/scontext/SContextPedometer;)V
    .locals 32
    .param p1, "pedometerContext"    # Landroid/hardware/scontext/SContextPedometer;

    .prologue
    .line 552
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getDistance()D

    move-result-wide v11

    .line 553
    .local v11, "distance":D
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getSpeed()D

    move-result-wide v19

    .line 554
    .local v19, "speed":D
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getCalorie()D

    move-result-wide v8

    .line 555
    .local v8, "cal":D
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getTotalStepCount()J

    move-result-wide v22

    .line 556
    .local v22, "totalStep":J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkStepCount()J

    move-result-wide v28

    .line 557
    .local v28, "walkStep":J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunStepCount()J

    move-result-wide v15

    .line 558
    .local v15, "runStep":J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getUpDownStepCount()J

    move-result-wide v24

    .line 559
    .local v24, "upDownStep":J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->getStepType(I)I

    move-result v21

    .line 561
    .local v21, "status":I
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkUpStepCount()J

    move-result-wide v30

    .line 562
    .local v30, "walkUpStep":J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getWalkDownStepCount()J

    move-result-wide v26

    .line 563
    .local v26, "walkDownStep":J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunUpStepCount()J

    move-result-wide v17

    .line 564
    .local v17, "runUpStep":J
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/scontext/SContextPedometer;->getRunDownStepCount()J

    move-result-wide v13

    .line 566
    .local v13, "runDownStep":J
    const-string v2, "WalkingMatePedometerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Total] distance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " speed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v19

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " calorie = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stepStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " totalStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v22

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " walkStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v28

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " runStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide v0, v15

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " upDownStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v24

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " walkUpStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v30

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " walkDownStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " runUpStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " runDownStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    new-instance v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>()V

    .line 572
    .local v10, "data":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    .line 573
    move/from16 v0, v21

    iput v0, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    .line 576
    const-wide/16 v2, 0x0

    cmp-long v2, v22, v2

    if-lez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    if-lez v2, :cond_2

    .line 577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    float-to-double v2, v2

    sub-double v2, v11, v2

    double-to-float v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    .line 578
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    float-to-double v2, v2

    sub-double v2, v19, v2

    double-to-float v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    .line 579
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    float-to-double v2, v2

    sub-double v2, v8, v2

    double-to-float v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    .line 580
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    int-to-long v2, v2

    sub-long v2, v22, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    .line 581
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    int-to-long v2, v2

    sub-long v2, v28, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    .line 582
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    int-to-long v2, v2

    sub-long v2, v15, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    .line 583
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    int-to-long v2, v2

    sub-long v2, v24, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    int-to-long v2, v2

    sub-long v2, v30, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    .line 586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    int-to-long v2, v2

    sub-long v2, v26, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    .line 587
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    int-to-long v2, v2

    sub-long v2, v17, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    .line 588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    int-to-long v2, v2

    sub-long v2, v13, v2

    long-to-int v2, v2

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    .line 605
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    if-eqz v2, :cond_0

    .line 606
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    double-to-float v3, v11

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    .line 607
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v19

    double-to-float v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    .line 608
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    double-to-float v3, v8

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    .line 609
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v22

    long-to-int v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    .line 610
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v28

    long-to-int v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    .line 611
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    long-to-int v3, v15

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    .line 612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v24

    long-to-int v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 614
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v30

    long-to-int v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    .line 615
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v26

    long-to-int v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    .line 616
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    move-wide/from16 v0, v17

    long-to-int v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    .line 617
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->last_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    long-to-int v3, v13

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    .line 620
    :cond_0
    const-string v2, "WalkingMatePedometerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Total] distance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " speed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " calorie = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stepStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->stepType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " totalStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " walkStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " runStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " upDownStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " walkUpStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " walkDownStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " runUpStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " runDownStepCnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v2

    iget v3, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    iget v5, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    iget-wide v6, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->time:J

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateData(IFFJ)V

    .line 627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    if-eqz v2, :cond_3

    .line 629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    const/4 v3, 0x0

    invoke-virtual {v2, v10, v3}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)Z

    .line 637
    :goto_1
    iget v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    add-int/lit8 v2, v2, -0xa

    iget v3, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    add-int/2addr v3, v4

    iget v4, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    add-int/2addr v3, v4

    if-le v2, v3, :cond_1

    .line 638
    const-string v2, "WalkingMatePedometerManager"

    const-string v3, "[StepsHandler] step count have gap"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    :cond_1
    return-void

    .line 590
    :cond_2
    double-to-float v2, v11

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->distance:F

    .line 591
    move-wide/from16 v0, v19

    double-to-float v2, v0

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->speed:F

    .line 592
    double-to-float v2, v8

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->calories:F

    .line 593
    move-wide/from16 v0, v22

    long-to-int v2, v0

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->totalStep:I

    .line 594
    move-wide/from16 v0, v28

    long-to-int v2, v0

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkStep:I

    .line 595
    long-to-int v2, v15

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runStep:I

    .line 596
    move-wide/from16 v0, v24

    long-to-int v2, v0

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->updownStep:I

    .line 598
    move-wide/from16 v0, v30

    long-to-int v2, v0

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkUpStep:I

    .line 599
    move-wide/from16 v0, v26

    long-to-int v2, v0

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->walkDownStep:I

    .line 600
    move-wide/from16 v0, v17

    long-to-int v2, v0

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runUpStep:I

    .line 601
    long-to-int v2, v13

    iput v2, v10, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;->runDownStep:I

    goto/16 :goto_0

    .line 633
    :cond_3
    const-string v2, "WalkingMatePedometerManager"

    const-string v3, "Aggregator is Null"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    .line 121
    iget-object v1, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 123
    .local v1, "scontext":Landroid/hardware/scontext/SContext;
    const-string v2, "WalkingMatePedometerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onSContextChanged type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mbSContextDiffMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mbSContextDiffMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-virtual {v1}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 126
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getPedometerContext()Landroid/hardware/scontext/SContextPedometer;

    move-result-object v0

    .line 128
    .local v0, "pedometerContext":Landroid/hardware/scontext/SContextPedometer;
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mbSContextDiffMode:Z

    if-eqz v2, :cond_1

    .line 129
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->updateWflDbByDiffValue(Landroid/hardware/scontext/SContextPedometer;)V

    .line 135
    .end local v0    # "pedometerContext":Landroid/hardware/scontext/SContextPedometer;
    :cond_0
    :goto_0
    return-void

    .line 132
    .restart local v0    # "pedometerContext":Landroid/hardware/scontext/SContextPedometer;
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->updateWlfDbByTotalValue(Landroid/hardware/scontext/SContextPedometer;)V

    goto :goto_0
.end method

.method public declared-synchronized recreateExerciseID(J)V
    .locals 6
    .param p1, "exerciseID"    # J

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "10009_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "deviceID":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    if-eqz v0, :cond_0

    .line 237
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->checkUserDeviceID(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    move-wide v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->initialize(Landroid/content/Context;Ljava/lang/String;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    :goto_0
    monitor-exit p0

    return-void

    .line 240
    :cond_0
    :try_start_1
    const-string v0, "WalkingMatePedometerManager"

    const-string/jumbo v1, "pedometer is not started"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234
    .end local v2    # "deviceID":Ljava/lang/String;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized registerPedometerListener(J)Z
    .locals 12
    .param p1, "exerciseID"    # J

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->bListenerRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x1

    .line 228
    :goto_0
    monitor-exit p0

    return v0

    .line 208
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FeatureManager;->checkFeature(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    const-string v0, "WalkingMatePedometerManager"

    const-string v1, "This device is not support pedometer"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const/4 v0, 0x0

    goto :goto_0

    .line 215
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "10009_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 217
    .local v2, "deviceID":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    invoke-static {}, Lcom/sec/android/service/health/cp/database/ValidationPolicy;->getServerPolicy()Lcom/sec/android/service/health/cp/database/ValidationPolicy;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;-><init>(Lcom/sec/android/service/health/cp/database/ValidationPolicy;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    .line 218
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->checkUserDeviceID(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->aggregator:Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    move-wide v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/service/health/cp/database/datahandler/PedometerDataAggregatorForSContext;->initialize(Landroid/content/Context;Ljava/lang/String;JI)V

    .line 221
    new-instance v11, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mContext:Landroid/content/Context;

    invoke-direct {v11, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 222
    .local v11, "shProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v11}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    invoke-virtual {v11}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v1

    invoke-virtual {v11}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->setBasicValue(IFF)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_2

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/4 v5, 0x2

    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mGender:I

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mHeight:F

    float-to-double v7, v0

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mWeight:F

    float-to-double v9, v0

    move-object v4, p0

    invoke-virtual/range {v3 .. v10}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;IIDD)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->bListenerRegistered:Z

    .line 226
    :cond_2
    const-string v0, "WalkingMatePedometerManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "registerWalkForLifeListener bListenerRegistered = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->bListenerRegistered:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->bListenerRegistered:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 204
    .end local v2    # "deviceID":Ljava/lang/String;
    .end local v11    # "shProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBasicValue(IFF)V
    .locals 3
    .param p1, "gender"    # I
    .param p2, "height"    # F
    .param p3, "weight"    # F

    .prologue
    const/4 v1, 0x0

    .line 143
    const v0, 0x2e635

    if-ne p1, v0, :cond_0

    .line 144
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mGender:I

    .line 150
    :goto_0
    cmpg-float v0, p2, v1

    if-gtz v0, :cond_1

    .line 151
    const/high16 v0, 0x432a0000    # 170.0f

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mHeight:F

    .line 156
    :goto_1
    cmpg-float v0, p3, v1

    if-gtz v0, :cond_2

    .line 157
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mWeight:F

    .line 161
    :goto_2
    const-string v0, "WalkingMatePedometerManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setBasicValue - Gender : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mGender:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Height : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mHeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Weight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mWeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mGender:I

    goto :goto_0

    .line 153
    :cond_1
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mHeight:F

    goto :goto_1

    .line 159
    :cond_2
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mWeight:F

    goto :goto_2
.end method

.method public declared-synchronized unregisterPedometerListener()V
    .locals 2

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 246
    const-string v0, "WalkingMatePedometerManager"

    const-string/jumbo v1, "unregisterPedometerListener"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->bListenerRegistered:Z

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :goto_0
    monitor-exit p0

    return-void

    .line 250
    :cond_0
    :try_start_1
    const-string v0, "WalkingMatePedometerManager"

    const-string/jumbo v1, "mSContextManager is null."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
