.class public abstract Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
.source "LoadMoreFoodInfoDataTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mFooterView:Landroid/view/View;

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListView;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 39
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;, "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask<TE;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mContext:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mListView:Landroid/widget/ListView;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mContext:Landroid/content/Context;

    const v1, 0x7f0300df

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mFooterView:Landroid/view/View;

    .line 43
    return-void
.end method


# virtual methods
.method protected finish()V
    .locals 2

    .prologue
    .line 76
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;, "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mFooterView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 77
    return-void
.end method

.method protected abstract onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation
.end method

.method protected onFinishedWithError()V
    .locals 0

    .prologue
    .line 68
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;, "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask<TE;>;"
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->onFinishedWithError()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->finish()V

    .line 70
    return-void
.end method

.method protected onTaskCanceled()V
    .locals 0

    .prologue
    .line 56
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;, "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask<TE;>;"
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->onTaskCanceled()V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->finish()V

    .line 58
    return-void
.end method

.method protected onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;, "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask<TE;>;"
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "TE;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->finish()V

    .line 63
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V

    .line 64
    return-void
.end method

.method protected onTaskStarted()V
    .locals 4

    .prologue
    .line 47
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;, "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask<TE;>;"
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->onTaskStarted()V

    .line 48
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mFooterView:Landroid/view/View;

    const v3, 0x7f0803d3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 49
    .local v1, "progressBar":Landroid/widget/ProgressBar;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mContext:Landroid/content/Context;

    const v3, 0x7f040009

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 50
    .local v0, "animation":Landroid/view/animation/Animation;
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 51
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;->mFooterView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 52
    return-void
.end method
