.class public Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;
.super Ljava/lang/Object;
.source "FoodGraphDataHolder.java"


# static fields
.field private static final NO_VALUE:F = -1.0E-7f


# instance fields
.field private mActualCount:I

.field private mCursorProjection:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;",
            ">;"
        }
    .end annotation
.end field

.field private mMealCursorList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mTotalCalorie:F


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/fooddao/MealDao;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "mealDao"    # Lcom/sec/android/app/shealth/food/fooddao/MealDao;
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mCursorProjection:Ljava/util/Set;

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    .line 58
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mCursorProjection:Ljava/util/Set;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mCursorProjection:Ljava/util/Set;

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mCursorProjection:Ljava/util/Set;

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v1, 0x0

    const v2, 0x186a1

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getGraphCursor(I)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x186a2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getGraphCursor(I)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x186a3

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getGraphCursor(I)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x186a4

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getGraphCursor(I)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    .line 64
    return-void
.end method

.method private filterValueForPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;F)F
    .locals 4
    .param p1, "periodTime"    # J
    .param p3, "period"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p4, "valueForPeriod"    # F

    .prologue
    .line 158
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v0, :cond_0

    .line 162
    const v0, -0x4c29406b    # -1.0E-7f

    cmpl-float v0, p4, v0

    if-lez v0, :cond_0

    .line 163
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDataPresentCountInPeriod(JJ)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mActualCount:I

    .line 167
    iget v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mActualCount:I

    int-to-float v0, v0

    div-float/2addr p4, v0

    .line 170
    .end local p4    # "valueForPeriod":F
    :cond_0
    return p4
.end method

.method private getCaloriesMovingCursor(JJLandroid/database/Cursor;)F
    .locals 3
    .param p1, "time"    # J
    .param p3, "minTime"    # J
    .param p5, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 208
    cmp-long v1, p1, p3

    if-nez v1, :cond_0

    invoke-interface {p5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p5, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p5, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 211
    .local v0, "calories":F
    invoke-interface {p5}, Landroid/database/Cursor;->moveToNext()Z

    .line 215
    :goto_0
    return v0

    .line 213
    .end local v0    # "calories":F
    :cond_0
    const v0, -0x4c29406b    # -1.0E-7f

    .restart local v0    # "calories":F
    goto :goto_0
.end method

.method private getGraphCursor(I)Landroid/database/Cursor;
    .locals 11
    .param p1, "mealIntakeType"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    const-wide/high16 v1, -0x8000000000000000L

    const-wide v3, 0x7fffffffffffffffL

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v5}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mCursorProjection:Ljava/util/Set;

    new-instance v7, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v8, "type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string v10, "="

    invoke-direct {v7, v8, v9, v10}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v8, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->ASC:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getListSum(Ljava/util/List;)Ljava/lang/Float;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)",
            "Ljava/lang/Float;"
        }
    .end annotation

    .prologue
    .line 181
    .local p0, "valuesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 182
    .local v1, "sum":Ljava/lang/Float;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    .line 183
    .local v2, "value":Ljava/lang/Float;
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 184
    goto :goto_0

    .line 185
    .end local v2    # "value":Ljava/lang/Float;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public areAllCursorsAfterLast()Z
    .locals 3

    .prologue
    .line 97
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 98
    .local v1, "mealCursor":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 99
    const/4 v2, 0x0

    .line 102
    .end local v1    # "mealCursor":Landroid/database/Cursor;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public closeAllCursors()V
    .locals 3

    .prologue
    .line 86
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 87
    .local v1, "mealCursor":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 89
    .end local v1    # "mealCursor":Landroid/database/Cursor;
    :cond_0
    return-void
.end method

.method public getFilteredValueCaloriesForPeriodList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "mealTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p2, "caloriesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 150
    .local v0, "filterValueCaloriesForPeriodList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 151
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-direct {p0, v3, v4, v5, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->filterValueForPeriod(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    :cond_0
    return-object v0
.end method

.method public getFoodCaloriesList(Ljava/util/List;J)Ljava/util/List;
    .locals 8
    .param p2, "minTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "mealTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 130
    .local v6, "caloriesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mTotalCalorie:F

    .line 131
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 132
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    move-object v0, p0

    move-wide v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getCaloriesMovingCursor(JJLandroid/database/Cursor;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const v1, -0x4c29406b    # -1.0E-7f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 134
    iget v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mTotalCalorie:F

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mTotalCalorie:F

    .line 131
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 137
    :cond_1
    return-object v6
.end method

.method public getMealTimeList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 112
    .local v2, "mealTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 113
    .local v1, "mealCursor":Landroid/database/Cursor;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    sget-object v4, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getTime(Landroid/database/Cursor;Ljava/lang/String;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    .end local v1    # "mealCursor":Landroid/database/Cursor;
    :cond_0
    return-object v2
.end method

.method public getTotalCalorie()D
    .locals 2

    .prologue
    .line 189
    iget v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mTotalCalorie:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public isMealCursorListContainNull()Z
    .locals 3

    .prologue
    .line 224
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 225
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 226
    const/4 v2, 0x1

    .line 229
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public moveAllCursorsToFirst()V
    .locals 3

    .prologue
    .line 77
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->mMealCursorList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 78
    .local v1, "mealCursor":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    goto :goto_0

    .line 80
    .end local v1    # "mealCursor":Landroid/database/Cursor;
    :cond_0
    return-void
.end method
