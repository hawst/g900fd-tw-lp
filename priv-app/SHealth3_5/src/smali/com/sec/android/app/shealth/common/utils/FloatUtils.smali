.class public final Lcom/sec/android/app/shealth/common/utils/FloatUtils;
.super Ljava/lang/Object;
.source "FloatUtils.java"


# static fields
.field public static final DEFAULT_EPSILON:F = 1.0E-6f


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static ceilToDigit(FI)F
    .locals 6
    .param p0, "number"    # F
    .param p1, "numberOfDigits"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 127
    float-to-double v0, p0

    int-to-double v2, p1

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    int-to-double v1, p1

    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static compare(FF)I
    .locals 1
    .param p0, "first"    # F
    .param p1, "second"    # F

    .prologue
    .line 54
    const v0, 0x358637bd    # 1.0E-6f

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FFF)I

    move-result v0

    return v0
.end method

.method public static compare(FFF)I
    .locals 1
    .param p0, "first"    # F
    .param p1, "second"    # F
    .param p2, "epsilon"    # F

    .prologue
    .line 65
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 68
    :goto_0
    return v0

    :cond_0
    cmpl-float v0, p0, p1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static floorToDigit(FI)F
    .locals 6
    .param p0, "number"    # F
    .param p1, "numberOfDigits"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 140
    float-to-double v0, p0

    int-to-double v2, p1

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    int-to-double v1, p1

    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static isEqual(FF)Z
    .locals 1
    .param p0, "first"    # F
    .param p1, "second"    # F

    .prologue
    .line 78
    const v0, 0x358637bd    # 1.0E-6f

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FFF)Z

    move-result v0

    return v0
.end method

.method public static isEqual(FFF)Z
    .locals 1
    .param p0, "first"    # F
    .param p1, "second"    # F
    .param p2, "epsilon"    # F

    .prologue
    .line 88
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FFF)I

    move-result v0

    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInRange(FFF)Z
    .locals 1
    .param p0, "value"    # F
    .param p1, "lowerLimit"    # F
    .param p2, "upperLimit"    # F

    .prologue
    .line 153
    const v0, 0x358637bd    # 1.0E-6f

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isInRange(FFFF)Z

    move-result v0

    return v0
.end method

.method public static isInRange(FFFF)Z
    .locals 1
    .param p0, "value"    # F
    .param p1, "lowerLimit"    # F
    .param p2, "upperLimit"    # F
    .param p3, "epsilon"    # F

    .prologue
    .line 167
    invoke-static {p0, p1, p3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FFF)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {p0, p2, p3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FFF)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMultiple(FF)Z
    .locals 1
    .param p0, "numerator"    # F
    .param p1, "denominator"    # F

    .prologue
    .line 208
    div-float v0, p0, p1

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isWholeNumber(F)Z

    move-result v0

    return v0
.end method

.method public static isWholeNumber(F)Z
    .locals 2
    .param p0, "number"    # F

    .prologue
    .line 196
    const/high16 v0, 0x3f800000    # 1.0f

    rem-float v0, p0, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v0

    return v0
.end method

.method public static isZero(F)Z
    .locals 1
    .param p0, "value"    # F

    .prologue
    .line 33
    const v0, 0x358637bd    # 1.0E-6f

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isZero(FF)Z

    move-result v0

    return v0
.end method

.method public static isZero(FF)Z
    .locals 1
    .param p0, "value"    # F
    .param p1, "epsilon"    # F

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FFF)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeFloatValid(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 220
    const-string v0, "."

    .line 221
    .local v0, "sDefaultNumberSeparator":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 223
    const-string v2, "\\."

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "val":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    .line 226
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 229
    .end local v1    # "val":[Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static parseValue(Ljava/lang/String;)F
    .locals 3
    .param p0, "valueString"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    .line 182
    .local v1, "numberFormat":Ljava/text/NumberFormat;
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/text/ParseException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static roundToDigit(FI)F
    .locals 6
    .param p0, "number"    # F
    .param p1, "numberOfDigits"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 114
    float-to-double v0, p0

    int-to-double v2, p1

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    int-to-double v1, p1

    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static roundToTenth(F)F
    .locals 1
    .param p0, "number"    # F

    .prologue
    .line 102
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToDigit(FI)F

    move-result v0

    return v0
.end method
