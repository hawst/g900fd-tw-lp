.class Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;
.super Ljava/lang/Object;
.source "CheckedListItemDataHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mId:J

.field private final mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-wide p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    .line 40
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder$1;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p0, p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 59
    :cond_1
    instance-of v3, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 60
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;

    .line 61
    .local v0, "that":Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;
    iget-wide v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    iget-wide v5, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 62
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getListItemId()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    return-wide v0
.end method

.method public getListItemType()Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 68
    iget-wide v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    iget-wide v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v0, v1

    .line 69
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 70
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CheckedListItemDataHolder;->mType:Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IListItem$ListItemType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    return-void
.end method
