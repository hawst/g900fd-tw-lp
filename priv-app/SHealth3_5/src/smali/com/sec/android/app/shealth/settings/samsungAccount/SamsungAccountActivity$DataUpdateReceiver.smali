.class Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SamsungAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataUpdateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 163
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.accountsync.action"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "errorCode":I
    const-string v2, "errorcode"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 168
    const-string v2, "errorcode"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 170
    :cond_0
    if-nez v0, :cond_3

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->showSyncStatus()V

    .line 181
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    if-eqz v2, :cond_1

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->dismiss()V

    .line 184
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth.syncservice"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 185
    .local v1, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->stopService(Landroid/content/Intent;)Z

    .line 188
    .end local v0    # "errorCode":I
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    return-void

    .line 178
    .restart local v0    # "errorCode":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0907ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
