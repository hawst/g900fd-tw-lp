.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewByTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;",
            ">;"
        }
    .end annotation
.end field

.field viewIndex:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x86a3ea80905586L

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p2

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->viewIndex:I

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x26fa

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x26ca

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->viewIndex:I

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->viewIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setFilterType(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$2000(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshAdapter()V

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->hideLoadingPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->loadingCompleted()V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->registerContentObserver()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->unregisterContentObserver()V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->loading:I

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showLoadingPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
