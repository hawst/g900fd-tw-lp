.class final Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData$1;
.super Ljava/lang/Object;
.source "ExerciseData.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    .locals 21
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 355
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v13

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v16

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v19

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v20

    invoke-direct/range {v0 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;-><init>(JJFJJJFFIIILjava/lang/String;Ljava/lang/String;II)V

    .line 357
    .local v0, "item":Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 362
    new-array v0, p1, [Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData$1;->newArray(I)[Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v0

    return-object v0
.end method
