.class public Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "InitSetPassword.java"

# interfaces
.implements Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;
    }
.end annotation


# static fields
.field private static final PASSWORD_REQUEST_CODE:I = 0x457


# instance fields
.field private TAG:Ljava/lang/String;

.field private mBottomLayout:Landroid/widget/FrameLayout;

.field private mBottomRightButton:Landroid/widget/RelativeLayout;

.field private mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSetPassword:Landroid/widget/Button;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field skipClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 39
    const-class v0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->TAG:Ljava/lang/String;

    .line 130
    new-instance v0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$1;-><init>(Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->skipClickListener:Landroid/view/View$OnClickListener;

    .line 187
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->moveToNextStep()V

    return-void
.end method

.method private initLayout()V
    .locals 4

    .prologue
    .line 115
    const v0, 0x7f08062f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mBottomLayout:Landroid/widget/FrameLayout;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mBottomLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f080613

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mBottomRightButton:Landroid/widget/RelativeLayout;

    .line 117
    const v0, 0x7f08062e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mSetPassword:Landroid/widget/Button;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mBottomRightButton:Landroid/widget/RelativeLayout;

    const v1, 0x7f08039d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090045

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mSetPassword:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->skipClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mBottomRightButton:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->skipClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mBottomRightButton:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901f7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    return-void
.end method

.method private moveToNextStep()V
    .locals 7

    .prologue
    const v6, 0x8000

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v2

    .line 148
    .local v2, "migrationState":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "migrationState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Has Samsung account?: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 152
    if-eqz v2, :cond_1

    .line 153
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;-><init>(Landroid/content/Context;)V

    .line 154
    .local v0, "dataMigrationHelper":Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    const-string v3, "com.sec.android.app.shealth"

    const-string v4, "MI01"

    invoke-static {p0, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->startLocalMigration()V

    .line 167
    .end local v0    # "dataMigrationHelper":Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    :goto_1
    return-void

    .line 149
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 157
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 159
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 162
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 164
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private passwordCheck()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "security_pin_enabled"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->startActivity(Landroid/content/Intent;)V

    .line 109
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v1

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "security_pin_enabled"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    .restart local v0    # "intent":Landroid/content/Intent;
    const v2, 0x8000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 105
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 109
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private registerProfileFinishedReceiver()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;-><init>(Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

    .line 207
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 212
    :cond_0
    return-void
.end method

.method private unregisterProfileFinishedReceiver()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword$ProfileFinishedReceiver;

    .line 224
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907bb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 87
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 179
    const/16 v0, 0x457

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->finish()V

    .line 184
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f03016b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->setContentView(I)V

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->initLayout()V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->registerProfileFinishedReceiver()V

    .line 69
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->unregisterProfileFinishedReceiver()V

    .line 92
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 93
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 80
    return-void
.end method

.method public restoreCompleted(Z)V
    .locals 3
    .param p1, "restored"    # Z

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->finish()V

    .line 172
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialpassword/InitSetPassword;->startActivity(Landroid/content/Intent;)V

    .line 174
    return-void
.end method
