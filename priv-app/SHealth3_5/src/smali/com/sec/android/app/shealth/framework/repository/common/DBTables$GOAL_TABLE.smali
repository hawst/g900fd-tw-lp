.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$GOAL_TABLE;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GOAL_TABLE"
.end annotation


# static fields
.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_PERIOD:Ljava/lang/String; = "period"

.field public static final KEY_PERIOD_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_PERIOD_MONTH_START:Ljava/lang/String; = "month_st"

.field public static final KEY_PERIOD_WEEK_START_MON:Ljava/lang/String; = "week_st_mon"

.field public static final KEY_PERIOD_WEEK_START_SUN:Ljava/lang/String; = "week_st_sun"

.field public static final KEY_PERIOD_YEAR_START:Ljava/lang/String; = "year_st"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_SUBTYPE:Ljava/lang/String; = "subtype"

.field public static final KEY_TYPE:Ljava/lang/String; = "type"

.field public static final KEY_VALUE:Ljava/lang/String; = "value"

.field public static final TABLE_NAME:Ljava/lang/String; = "goal"

.field public static final TRIGGER_INSERT_NAME:Ljava/lang/String; = "create_goal_trigger"

.field public static final TRIGGER_UPDATE_NAME:Ljava/lang/String; = "update_goal_trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
