.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;
.super Ljava/lang/Object;
.source "MealImagesFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public imageWasDeleted(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V
    .locals 3
    .param p1, "imagePath"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    .prologue
    .line 161
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "image was deleted with path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mMealImagesMap is contain = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$800(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$900(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$800(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutChanged()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$1000(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    .line 166
    return-void
.end method
