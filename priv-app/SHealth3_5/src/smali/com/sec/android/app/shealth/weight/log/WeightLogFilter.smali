.class public final enum Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;
.super Ljava/lang/Enum;
.source "WeightLogFilter.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/log/LogFilter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;",
        ">;",
        "Lcom/sec/android/app/shealth/logutils/log/LogFilter;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

.field public static final enum ALL:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

.field public static final enum OUTSIDE:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

.field public static final enum WITHIN:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;


# instance fields
.field private final mDisplayNameId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    const-string v1, "ALL"

    const v2, 0x7f090d13

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->ALL:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    .line 33
    new-instance v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    const-string v1, "WITHIN"

    const v2, 0x7f090ca0

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->WITHIN:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    .line 34
    new-instance v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    const-string v1, "OUTSIDE"

    const v2, 0x7f090c9f

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->OUTSIDE:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    sget-object v1, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->ALL:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->WITHIN:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->OUTSIDE:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->$VALUES:[Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "displayNameId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->mDisplayNameId:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->$VALUES:[Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayNameId()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->mDisplayNameId:I

    return v0
.end method

.method public getValues()[Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->values()[Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    move-result-object v0

    return-object v0
.end method
