.class Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
.super Ljava/lang/Object;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationOperator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;
    }
.end annotation


# static fields
.field private static final BUTTON_LONGPRESS_TIME_RATIO:F = 1.5f

.field private static final INERTIA_DECELERATION_FACTOR:I = 0x3

.field private static final INERTIA_DURATION_RATIO:I = 0xa

.field private static final INERTIA_MIN_SPEED:I = 0x1e

.field private static final INERTIA_START_SPEED_RATIO:F = 3.0f


# instance fields
.field private mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

.field private mInertiaAnimator:Landroid/animation/ObjectAnimator;

.field private mValueHolder:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;

.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V
    .locals 2

    .prologue
    .line 619
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 692
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    .line 620
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mValueHolder:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;

    .line 621
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->initObjectAnimators()V

    .line 622
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    .prologue
    .line 605
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->animateButtonLongPress(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    .param p1, "x1"    # F

    .prologue
    .line 605
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->animateInertia(F)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    .prologue
    .line 605
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->isRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    .prologue
    .line 605
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->cancelAnimation()V

    return-void
.end method

.method private animateButtonLongPress(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V
    .locals 5
    .param p1, "direction"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    .prologue
    .line 682
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_LEFT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceToMaxInputValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$2000(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)I

    move-result v1

    .line 684
    .local v1, "longPressMoveDistance":I
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_LEFT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$2200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    move-result-object v2

    iget v0, v2, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    .line 686
    .local v0, "longPressMoveDestination":F
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v0, v3, v4

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 687
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 689
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 690
    return-void

    .line 682
    .end local v0    # "longPressMoveDestination":F
    .end local v1    # "longPressMoveDistance":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceToMinInputValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$2100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)I

    move-result v1

    goto :goto_0

    .line 684
    .restart local v1    # "longPressMoveDistance":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$2200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    move-result-object v2

    iget v0, v2, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    goto :goto_1
.end method

.method private animateInertia(F)V
    .locals 5
    .param p1, "startSpeed"    # F

    .prologue
    .line 665
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41f00000    # 30.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 666
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->moveToClosestGradation()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1500(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V

    .line 675
    :goto_0
    return-void

    .line 670
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0xa

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 671
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v3, p1

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->pixelToValue(F)F
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1700(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;F)F

    move-result v1

    .line 672
    .local v1, "animationDistance":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1800(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)F

    move-result v3

    add-float/2addr v3, v1

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getClosestGradationValue(F)F
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1900(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;F)F

    move-result v0

    .line 673
    .local v0, "animationDestination":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v0, v3, v4

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 674
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private cancelAnimation()V
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 652
    return-void
.end method

.method private initObjectAnimators()V
    .locals 4

    .prologue
    .line 629
    const-string v0, "AnimationValue"

    .line 631
    .local v0, "propertyToAnimate":Ljava/lang/String;
    new-instance v1, Landroid/animation/ObjectAnimator;

    invoke-direct {v1}, Landroid/animation/ObjectAnimator;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mValueHolder:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 633
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    const-string v2, "AnimationValue"

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 635
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 637
    new-instance v1, Landroid/animation/ObjectAnimator;

    invoke-direct {v1}, Landroid/animation/ObjectAnimator;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    .line 638
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mValueHolder:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator$ValueHolder;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 639
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 640
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    const-string v2, "AnimationValue"

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 642
    return-void
.end method

.method private isRunning()Z
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mInertiaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->mButtonLongpressAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
