.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;
.super Landroid/os/Handler;
.source "HeartRateScoverActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 286
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mHrmScoverFragmentHandler handleMessage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget v2, p1, Landroid/os/Message;->arg1:I

    sparse-switch v2, :sswitch_data_0

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 290
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 291
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->createMeasurementDialog()V

    goto :goto_0

    .line 296
    :sswitch_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 297
    .local v1, "transaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 298
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$302(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    .end local v1    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    if-eqz v2, :cond_0

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->finish()V

    goto :goto_0

    .line 300
    :catch_0
    move-exception v0

    .line 301
    .local v0, "ise":Ljava/lang/IllegalStateException;
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string v3, "IllegalStateException while removing HeartRate Scover Fragment"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 288
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method
