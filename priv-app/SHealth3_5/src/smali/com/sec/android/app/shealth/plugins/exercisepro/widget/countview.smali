.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;
.super Landroid/view/View;
.source "countview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$OnAnimationFinishListener;
    }
.end annotation


# instance fields
.field Fadeout:Z

.field private Heigth:I

.field private Redius:F

.field private Redius_zoom:F

.field private final STEP01:I

.field private final STEP02:I

.field private final STEP03:I

.field private final STEP04:I

.field private Scale:I

.field private Step:I

.field private TextSize:I

.field private Width:I

.field private context:Landroid/content/Context;

.field controller:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

.field private count:I

.field public isStart:Z

.field mHandler:Landroid/os/Handler;

.field private mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$OnAnimationFinishListener;

.field private mPath:Landroid/graphics/Path;

.field private paint:Landroid/graphics/Paint;

.field showTimeDalay:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 67
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->isStart:Z

    .line 23
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    .line 25
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    .line 26
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    .line 27
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    .line 28
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    .line 29
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    .line 32
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP01:I

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP02:I

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP03:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP04:I

    .line 36
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 37
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 39
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    .line 216
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 217
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 68
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->context:Landroid/content/Context;

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Init()V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->isStart:Z

    .line 23
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    .line 25
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    .line 26
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    .line 27
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    .line 28
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    .line 29
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    .line 32
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP01:I

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP02:I

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP03:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP04:I

    .line 36
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 37
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 39
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    .line 216
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 217
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 80
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->context:Landroid/content/Context;

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Init()V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->invalidate()V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->isStart:Z

    .line 23
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    .line 25
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    .line 26
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    .line 27
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    .line 28
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    .line 29
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    .line 32
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP01:I

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP02:I

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP03:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->STEP04:I

    .line 36
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 37
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 39
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    .line 216
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 217
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->context:Landroid/content/Context;

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Init()V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->invalidate()V

    .line 77
    return-void
.end method

.method private DrawBackGorund(Landroid/graphics/Canvas;I)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "alpha"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 279
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-static {p2, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 282
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 283
    return-void
.end method

.method private DrawColorCircle(Landroid/graphics/Canvas;Ljava/lang/String;FI)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "color"    # Ljava/lang/String;
    .param p3, "redius"    # F
    .param p4, "alpha"    # I

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 286
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 292
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    div-float v2, p3, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 293
    return-void
.end method

.method private DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "count"    # Ljava/lang/String;
    .param p3, "Size"    # F
    .param p4, "Alpha"    # I

    .prologue
    const/4 v3, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    .line 296
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, p3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 303
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 304
    .local v0, "bounds":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2, v3, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 305
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    const-string v2, "#ffffff"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, p4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 308
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    int-to-float v1, v1

    div-float/2addr v1, v6

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    int-to-float v2, v2

    div-float/2addr v2, v6

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 310
    return-object p1
.end method

.method private Step01(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 206
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawBackGorund(Landroid/graphics/Canvas;I)V

    .line 207
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    const/16 v1, 0x3c

    if-ge v0, v1, :cond_0

    .line 208
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 214
    :goto_0
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 212
    const/16 v0, 0x28

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    goto :goto_0
.end method

.method private Step02(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x64

    const/4 v7, 0x1

    const/high16 v6, 0x437f0000    # 255.0f

    const-wide/16 v4, 0x0

    .line 220
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    if-nez v0, :cond_5

    .line 221
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    if-ge v0, v8, :cond_2

    .line 222
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 241
    :cond_0
    :goto_0
    const-string v0, "#00ff00"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v2, v2

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawColorCircle(Landroid/graphics/Canvas;Ljava/lang/String;FI)V

    .line 242
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    if-nez v0, :cond_4

    .line 243
    const-string v0, "Go"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v2, v2

    const/high16 v3, 0x41f00000    # 30.0f

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v4, v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v2, v2

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;

    .line 276
    :cond_1
    :goto_1
    return-void

    .line 224
    :cond_2
    iput v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 225
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    .line 226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 228
    :cond_3
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 229
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 230
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 231
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    if-nez v0, :cond_0

    .line 233
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 234
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 235
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 236
    iput v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    goto :goto_0

    .line 246
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v2, v2

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;

    goto :goto_1

    .line 250
    :cond_5
    const-string v0, "#00ff00"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v2, v2

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawColorCircle(Landroid/graphics/Canvas;Ljava/lang/String;FI)V

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v2, v2

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;

    .line 254
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    if-lez v0, :cond_6

    .line 255
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    goto/16 :goto_1

    .line 257
    :cond_6
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_7

    .line 258
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 260
    :cond_7
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    const-wide/16 v2, 0xfa

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 261
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 262
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 264
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    if-nez v0, :cond_8

    .line 265
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    .line 267
    :cond_8
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    .line 269
    iput v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 270
    const/16 v0, 0x3c

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 271
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 272
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    goto/16 :goto_1
.end method

.method private Step03(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v10, 0xff

    const/high16 v7, 0x437f0000    # 255.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 155
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    if-eqz v2, :cond_3

    .line 156
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 157
    .local v6, "tmp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 158
    .local v0, "mCanvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    const/high16 v3, 0x42700000    # 60.0f

    invoke-virtual {p0, v3, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v3, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    int-to-float v3, v2

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    int-to-float v4, v2

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 162
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mPath:Landroid/graphics/Path;

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mPath:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v2, v3}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    int-to-float v3, v3

    div-float/2addr v3, v9

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    int-to-float v4, v4

    div-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v5, v5

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    div-float/2addr v7, v9

    invoke-virtual {p0, v5, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v4, v5, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 165
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mPath:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 170
    const/4 v2, 0x0

    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    int-to-float v5, v5

    invoke-direct {v3, v1, v1, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v6, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 172
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    div-float/2addr v2, v9

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    .line 173
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    const/16 v2, 0x64

    if-ge v1, v2, :cond_1

    .line 174
    const-string v1, "#00ff00"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    invoke-direct {p0, p1, v1, v2, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawColorCircle(Landroid/graphics/Canvas;Ljava/lang/String;FI)V

    .line 175
    const-string v1, "Go"

    const/high16 v2, 0x428c0000    # 70.0f

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    invoke-direct {p0, p1, v1, v2, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;

    .line 176
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 203
    .end local v0    # "mCanvas":Landroid/graphics/Canvas;
    .end local v6    # "tmp":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 178
    .restart local v0    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v6    # "tmp":Landroid/graphics/Bitmap;
    :cond_1
    const-string v1, "#00ff00"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-direct {p0, p1, v1, v2, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawColorCircle(Landroid/graphics/Canvas;Ljava/lang/String;FI)V

    .line 179
    const-string v1, "Go"

    const/high16 v2, 0x428c0000    # 70.0f

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    invoke-direct {p0, p1, v1, v2, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;

    .line 181
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    const/16 v2, 0x190

    if-ge v1, v2, :cond_2

    .line 182
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    goto :goto_0

    .line 184
    :cond_2
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 185
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 186
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 187
    const/16 v1, 0x64

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    goto :goto_0

    .line 191
    .end local v0    # "mCanvas":Landroid/graphics/Canvas;
    .end local v6    # "tmp":Landroid/graphics/Bitmap;
    :cond_3
    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, v1, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawBackGorund(Landroid/graphics/Canvas;I)V

    .line 192
    const-string v1, "#00ff00"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v3, v3

    invoke-virtual {p0, v3, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawColorCircle(Landroid/graphics/Canvas;Ljava/lang/String;FI)V

    .line 193
    const-string v1, "Go"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v3, v3

    const/high16 v4, 0x41f00000    # 30.0f

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v5, v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v3, v3

    invoke-virtual {p0, v3, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;

    .line 194
    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    .line 195
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    .line 197
    :cond_4
    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->showTimeDalay:J

    const-wide/16 v3, 0x2bc

    add-long/2addr v1, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 198
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Fadeout:Z

    .line 199
    iput v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 200
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    goto/16 :goto_0
.end method

.method private Step04(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 142
    const-string v1, "Go"

    const/high16 v2, 0x428c0000    # 70.0f

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    int-to-float v3, v3

    const/high16 v4, 0x437f0000    # 255.0f

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawNumber(Landroid/graphics/Canvas;Ljava/lang/String;FI)Landroid/graphics/Canvas;

    .line 143
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    if-lez v1, :cond_0

    .line 144
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/16 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 152
    :goto_0
    return-void

    .line 147
    :cond_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 148
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$OnAnimationFinishListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$OnAnimationFinishListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    return p1
.end method


# virtual methods
.method public Init()V
    .locals 2

    .prologue
    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->paint:Landroid/graphics/Paint;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a097b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->TextSize:I

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a097c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    .line 89
    const/high16 v0, 0x42a00000    # 80.0f

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius:F

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Redius_zoom:F

    .line 90
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mPath:Landroid/graphics/Path;

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->invalidate()V

    .line 92
    return-void
.end method

.method public Returnpercentage(FF)F
    .locals 2
    .param p1, "percentage"    # F
    .param p2, "max"    # F

    .prologue
    .line 111
    mul-float v0, p2, p1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public Start(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V
    .locals 2
    .param p1, "exerciseProWorkoutController"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    .prologue
    const/4 v1, 0x0

    .line 95
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->controller:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    .line 96
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    .line 97
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Scale:I

    .line 98
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->count:I

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->isStart:Z

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->invalidate()V

    .line 101
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->setVisibility(I)V

    .line 102
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const-wide/16 v3, 0x2

    const/4 v2, 0x0

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Width:I

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Heigth:I

    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->isStart:Z

    if-nez v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 121
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 123
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step01(Landroid/graphics/Canvas;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 127
    :pswitch_1
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Returnpercentage(FF)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->DrawBackGorund(Landroid/graphics/Canvas;I)V

    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step02(Landroid/graphics/Canvas;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 132
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step03(Landroid/graphics/Canvas;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 136
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->Step04(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setOnAnimationFinishListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$OnAnimationFinishListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$OnAnimationFinishListener;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/countview$OnAnimationFinishListener;

    .line 64
    return-void
.end method
