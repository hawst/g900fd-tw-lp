.class public Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;
.super Ljava/lang/Object;
.source "BarcodeLib.java"


# static fields
.field private static final sBarcodeDecoderPacked:Ljava/lang/String; = "BarcodeDecoderPacked"

.field private static sBarcodeLib:Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;->sBarcodeLib:Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;->sBarcodeLib:Lcom/sec/android/app/shealth/core/barcode/BarcodeLib;

    return-object v0
.end method


# virtual methods
.method public native decodeBarcode([BIIIII)I
.end method

.method public native getBarcodeResult()Ljava/lang/String;
.end method

.method public native getPoints()[I
.end method
