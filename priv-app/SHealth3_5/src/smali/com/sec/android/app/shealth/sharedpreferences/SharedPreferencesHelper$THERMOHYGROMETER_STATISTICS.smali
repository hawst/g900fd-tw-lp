.class public final Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;
.super Ljava/lang/Object;
.source "SharedPreferencesHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "THERMOHYGROMETER_STATISTICS"
.end annotation


# static fields
.field public static final CREATE_TIME:Ljava/lang/String; = "thermo_create_time"

.field public static final HUMIDITY:Ljava/lang/String; = "thermo_humidity"

.field public static final PERIOD_TO_REFRESH:Ljava/lang/String; = "period_to_refresh"

.field public static final TEMPERATURE:Ljava/lang/String; = "thermo_temperature"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCreateTime()J
    .locals 5

    .prologue
    .line 1742
    const-wide/16 v1, 0x0

    .line 1744
    .local v1, "value":J
    :try_start_0
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "thermo_create_time"

    invoke-interface {v3, v4, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 1749
    :goto_0
    return-wide v1

    .line 1745
    :catch_0
    move-exception v0

    .line 1746
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v3, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string/jumbo v4, "thermo_create_time"

    invoke-interface {v3, v4, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getHumidity()F
    .locals 4

    .prologue
    .line 1726
    const/high16 v1, -0x3b860000    # -1000.0f

    .line 1728
    .local v1, "value":F
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "thermo_humidity"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1733
    :goto_0
    return v1

    .line 1729
    :catch_0
    move-exception v0

    .line 1730
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "thermo_humidity"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getRefreshPeriod()I
    .locals 4

    .prologue
    .line 1694
    const/16 v1, 0x3c

    .line 1696
    .local v1, "value":I
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "period_to_refresh"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1701
    :goto_0
    return v1

    .line 1697
    :catch_0
    move-exception v0

    .line 1698
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "period_to_refresh"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getTemperature()F
    .locals 4

    .prologue
    .line 1710
    const/high16 v1, -0x3b860000    # -1000.0f

    .line 1712
    .local v1, "value":F
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "thermo_temperature"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1717
    :goto_0
    return v1

    .line 1713
    :catch_0
    move-exception v0

    .line 1714
    .local v0, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "thermo_temperature"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static setCreateTime(J)V
    .locals 3
    .param p0, "createTime"    # J

    .prologue
    .line 1753
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingPeriod = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1754
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "thermo_create_time"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1755
    return-void
.end method

.method public static setHumidity(F)V
    .locals 3
    .param p0, "humidity"    # F

    .prologue
    .line 1737
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingPeriod = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1738
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "thermo_humidity"

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1739
    return-void
.end method

.method public static setRefreshPeriod(I)V
    .locals 3
    .param p0, "config"    # I

    .prologue
    .line 1705
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingPeriod = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1706
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "period_to_refresh"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1707
    return-void
.end method

.method public static setTemperature(F)V
    .locals 3
    .param p0, "temperature"    # F

    .prologue
    .line 1721
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[**] setInactiveTimeTrackingPeriod = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1722
    # getter for: Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefManager:Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->access$100()Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;

    move-result-object v0

    const-string/jumbo v1, "thermo_temperature"

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1723
    return-void
.end method
