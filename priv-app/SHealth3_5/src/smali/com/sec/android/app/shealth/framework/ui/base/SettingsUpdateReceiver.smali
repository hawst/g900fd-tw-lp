.class public Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field mAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;)V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;->mAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 35

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x300f

    aput v33, v2, v32

    const/16 v32, 0xf7c

    aput v32, v2, v31

    const/16 v31, 0x3946

    aput v31, v2, v30

    const/16 v30, 0x627f

    aput v30, v2, v29

    const/16 v29, -0x2dd3

    aput v29, v2, v28

    const/16 v28, -0x80

    aput v28, v2, v27

    const/16 v27, -0x51b6

    aput v27, v2, v26

    const/16 v26, -0xf

    aput v26, v2, v25

    const/16 v25, -0x29

    aput v25, v2, v24

    const/16 v24, -0x14

    aput v24, v2, v23

    const/16 v23, -0x24

    aput v23, v2, v22

    const/16 v22, -0x7be1

    aput v22, v2, v21

    const/16 v21, -0x2c

    aput v21, v2, v20

    const/16 v20, -0x19bb

    aput v20, v2, v19

    const/16 v19, -0x38

    aput v19, v2, v18

    const/16 v18, -0x3f

    aput v18, v2, v17

    const/16 v17, 0x5669

    aput v17, v2, v16

    const/16 v16, -0x5dc6

    aput v16, v2, v15

    const/16 v15, -0x3d

    aput v15, v2, v14

    const/16 v14, 0x4e02

    aput v14, v2, v13

    const/16 v13, -0x77da

    aput v13, v2, v12

    const/4 v12, -0x5

    aput v12, v2, v11

    const/16 v11, -0x5b

    aput v11, v2, v10

    const/16 v10, -0xb

    aput v10, v2, v9

    const/16 v9, -0x43ec

    aput v9, v2, v8

    const/16 v8, -0x31

    aput v8, v2, v7

    const/16 v7, -0x3b

    aput v7, v2, v6

    const/16 v6, -0x39

    aput v6, v2, v5

    const/16 v5, -0x3fa

    aput v5, v2, v3

    const/16 v3, -0x61

    aput v3, v2, v1

    const/16 v1, 0x1e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x304a

    aput v34, v1, v33

    const/16 v33, 0xf30

    aput v33, v1, v32

    const/16 v32, 0x390f

    aput v32, v1, v31

    const/16 v31, 0x6239

    aput v31, v1, v30

    const/16 v30, -0x2d9e

    aput v30, v1, v29

    const/16 v29, -0x2e

    aput v29, v1, v28

    const/16 v28, -0x51e6

    aput v28, v1, v27

    const/16 v27, -0x52

    aput v27, v1, v26

    const/16 v26, -0x6e

    aput v26, v1, v25

    const/16 v25, -0x48

    aput v25, v1, v24

    const/16 v24, -0x63

    aput v24, v1, v23

    const/16 v23, -0x7ba5

    aput v23, v1, v22

    const/16 v22, -0x7c

    aput v22, v1, v21

    const/16 v21, -0x19f0

    aput v21, v1, v20

    const/16 v20, -0x1a

    aput v20, v1, v19

    const/16 v19, -0x57

    aput v19, v1, v18

    const/16 v18, 0x561d

    aput v18, v1, v17

    const/16 v17, -0x5daa

    aput v17, v1, v16

    const/16 v16, -0x5e

    aput v16, v1, v15

    const/16 v15, 0x4e67

    aput v15, v1, v14

    const/16 v14, -0x77b2

    aput v14, v1, v13

    const/16 v13, -0x78

    aput v13, v1, v12

    const/16 v12, -0x75

    aput v12, v1, v11

    const/16 v11, -0x6a

    aput v11, v1, v10

    const/16 v10, -0x438f

    aput v10, v1, v9

    const/16 v9, -0x44

    aput v9, v1, v8

    const/16 v8, -0x15

    aput v8, v1, v7

    const/16 v7, -0x56

    aput v7, v1, v6

    const/16 v6, -0x397

    aput v6, v1, v5

    const/4 v5, -0x4

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;->mAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;->mAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->isProfileImageChanged(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;->mAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;->mAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->notifyDataSetChanged()V

    :cond_0
    :goto_2
    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_2
.end method
