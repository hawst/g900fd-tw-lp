.class public Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;
.super Ljava/lang/Object;
.source "TextDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private fontName:Ljava/lang/String;

.field private outlineAlpha:F

.field private outlineColor:I

.field private outlineEnabled:Z

.field private outlineRadius:I

.field private shadowAlpha:F

.field private shadowColor:I

.field private shadowDx:I

.field private shadowDy:I

.field private shadowEnabled:Z

.field private shadowRadius:I

.field private textAlpha:F

.field private textColor:I

.field private textSize:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->textSize:F

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->fontName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineEnabled:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineColor:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineRadius:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineAlpha:F

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->textColor:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->textAlpha:F

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowEnabled:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowColor:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowAlpha:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowDx:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowDy:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowRadius:I

    return v0
.end method


# virtual methods
.method public setTextSize(F)V
    .locals 0
    .param p1, "textSize"    # F

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->textSize:F

    .line 137
    return-void
.end method
