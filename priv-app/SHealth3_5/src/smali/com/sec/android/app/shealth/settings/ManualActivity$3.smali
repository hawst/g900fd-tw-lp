.class Lcom/sec/android/app/shealth/settings/ManualActivity$3;
.super Ljava/lang/Object;
.source "ManualActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ManualActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 248
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$100(Lcom/sec/android/app/shealth/settings/ManualActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 271
    :goto_1
    :pswitch_0
    return-void

    :cond_0
    move v0, v1

    .line 248
    goto :goto_0

    .line 251
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforDisableAirplane:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$502(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto :goto_1

    .line 254
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableMobile:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$602(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto :goto_1

    .line 257
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableRoaming:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$702(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableRoaming:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$700(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->extraText:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$000(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->extraText:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$000(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 265
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$202(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto :goto_1

    .line 268
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$302(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto :goto_1

    .line 249
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
