.class Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;
.super Ljava/lang/Object;
.source "AwardUserBackUpActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->setViewArea()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 120
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 124
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    const-string v1, "activity_name"

    const-class v2, Lcom/sec/android/app/shealth/award/AwardActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 129
    return-void

    .line 122
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity$1;->this$0:Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    # getter for: Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;->access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method
