.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProWebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$1;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;
    }
.end annotation


# instance fields
.field private dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 71
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    return-object p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v2, 0x7f03017e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->setContentView(I)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "url"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "url":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->finish()V

    .line 26
    :cond_0
    const v2, 0x7f08066f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->mWebView:Landroid/webkit/WebView;

    .line 28
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v4}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 29
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 30
    .local v1, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 31
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 32
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 33
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 34
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 35
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 36
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;

    invoke-direct {v3, p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebViewClient;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$1;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 37
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;

    invoke-direct {v3, p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$1;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 38
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 39
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .line 40
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->show()V

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setCancelable(Z)V

    .line 42
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->dismiss()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .line 93
    :cond_0
    return-void
.end method
