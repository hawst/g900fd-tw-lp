.class public final Lcom/sec/android/app/shealth/weight/common/RangeConstants;
.super Ljava/lang/Object;
.source "RangeConstants.java"


# static fields
.field public static final MAX_NORMAL_RANGE_BMI:F = 23.0f

.field public static final MAX_WEIGHT:F = 500.0f

.field public static final MIN_NORMAL_RANGE_BMI:F = 18.5f

.field public static final MIN_WEIGHT:F = 2.0f

.field public static final WEIGHT_INTERVAL:F = 0.1f


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static getInputRangeMax()F
    .locals 1

    .prologue
    .line 48
    const/high16 v0, 0x43fa0000    # 500.0f

    return v0
.end method

.method public static getInputRangeMin()F
    .locals 1

    .prologue
    .line 41
    const/high16 v0, 0x40000000    # 2.0f

    return v0
.end method
