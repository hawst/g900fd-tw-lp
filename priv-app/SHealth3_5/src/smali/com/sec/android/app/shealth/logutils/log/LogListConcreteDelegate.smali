.class public abstract Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;
.super Ljava/lang/Object;
.source "LogListConcreteDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field private static final DAY_GROUP_ADDITIONAL_FORMAT_PATTERN:Ljava/lang/String; = " (EEE)"

.field private static final DAY_GROUP_DATE_SEPARATOR:Ljava/lang/String; = "/"

.field private static final DAY_GROUP_DAY_FORMAT_PATTERN:Ljava/lang/String; = "dd"

.field private static final DAY_GROUP_MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MM"

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_GROUP_DATE_SEPARATOR:Ljava/lang/String; = " "

.field private static final MONTH_GROUP_MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MMM"

.field private static final MONTH_GROUP_YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field private static final TAG:Ljava/lang/String;

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"


# instance fields
.field protected final mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private mDayGroupDateFormat:Ljava/text/DateFormat;

.field private mMonthGroupDateFormat:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    .local p1, "dao":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;, "TD;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    if-nez p1, :cond_0

    .line 74
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Given dao reference is not allowed to be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    .line 77
    return-void
.end method


# virtual methods
.method protected bindChildView(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 0
    .param p1, "view"    # Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isRowChanged"    # Z

    .prologue
    .line 142
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    return-void
.end method

.method public abstract bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
.end method

.method public abstract getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
.end method

.method public getChildViewId()I
    .locals 1

    .prologue
    .line 293
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    sget v0, Lcom/sec/android/app/shealth/logutils/R$layout;->log_list_row:I

    return v0
.end method

.method public abstract getCommentColumnName()Ljava/lang/String;
.end method

.method public getContentContractAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    const-string v0, "com.sec.android.app.shealth.cp.HealthContentProvider"

    return-object v0
.end method

.method public abstract getContentUri()Landroid/net/Uri;
.end method

.method public getCreateTimeColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    const-string v0, "day_time"

    return-object v0
.end method

.method protected getDao()Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .locals 1

    .prologue
    .line 84
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mDao:Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    return-object v0
.end method

.method public getDayGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 204
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mDayGroupDateFormat:Ljava/text/DateFormat;

    if-nez v1, :cond_0

    .line 205
    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 207
    .local v0, "dateFormatPattern":Ljava/lang/String;
    const-string v1, "d+"

    const-string v2, "dd/"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208
    const-string v1, "M+"

    const-string v2, "MM/"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    const-string/jumbo v1, "y+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 211
    const-string v1, "/$"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (EEE)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 214
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mDayGroupDateFormat:Ljava/text/DateFormat;

    .line 217
    .end local v0    # "dateFormatPattern":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mDayGroupDateFormat:Ljava/text/DateFormat;

    return-object v1
.end method

.method public abstract getGroupCursor(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)Landroid/database/Cursor;
.end method

.method public getGroupViewId()I
    .locals 1

    .prologue
    .line 285
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    sget v0, Lcom/sec/android/app/shealth/logutils/R$layout;->log_list_group_header:I

    return v0
.end method

.method public getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    const-string v0, "_id"

    return-object v0
.end method

.method protected getKoreanMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # J

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 245
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 246
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 247
    const-string v1, "%d%s %d%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    sget v3, Lcom/sec/android/app/shealth/logutils/R$string;->year:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    sget v4, Lcom/sec/android/app/shealth/logutils/R$string;->month:I

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 221
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mMonthGroupDateFormat:Ljava/text/DateFormat;

    if-nez v1, :cond_0

    .line 222
    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 224
    .local v0, "dateFormatPattern":Ljava/lang/String;
    const-string v1, "d+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    const-string v1, "M+"

    const-string v2, "MMM "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    const-string/jumbo v1, "y+"

    const-string/jumbo v2, "yyyy "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    const-string v1, " $"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mMonthGroupDateFormat:Ljava/text/DateFormat;

    .line 233
    .end local v0    # "dateFormatPattern":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->mMonthGroupDateFormat:Ljava/text/DateFormat;

    return-object v1
.end method

.method protected getMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # J

    .prologue
    .line 260
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "language":Ljava/lang/String;
    const-string v1, "ko"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "zh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ja"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 263
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getKoreanMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 265
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getShareViaDescription(Landroid/database/Cursor;Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 274
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getCreateTimeColumnName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 275
    .local v0, "time":J
    sget v2, Lcom/sec/android/app/shealth/logutils/R$string;->log_share_via_description_timedate_format:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected isCursorSetOnFirstMeasureInDay(Landroid/database/Cursor;)Z
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;, "Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate<TD;>;"
    const/4 v4, 0x1

    .line 173
    invoke-interface {p1}, Landroid/database/Cursor;->isFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v4

    .line 176
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 177
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Cursor is not set to valid row"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 179
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getCreateTimeColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    .line 181
    .local v0, "currentMeasureTime":J
    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getCreateTimeColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 184
    .local v2, "previousMeasureTime":J
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 185
    cmp-long v5, v0, v2

    if-lez v5, :cond_4

    .line 187
    sget-object v4, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cursor order violation, current measure time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; previous measure time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 190
    sget-object v4, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->TAG:Ljava/lang/String;

    const-string v5, "Dumping cursor ..."

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_3

    .line 192
    sget-object v4, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->getCreateTimeColumnName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 196
    :cond_3
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Current log list item is later in time than current previous"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 198
    :cond_4
    cmp-long v5, v0, v2

    if-ltz v5, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_0
.end method
