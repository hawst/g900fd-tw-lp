.class public Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;
.super Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;
.source "CalendarDayButton.java"


# static fields
.field private static final CALENDAR_BUTTON_UNDERLINE_RESOURCE:Ljava/lang/String; = "s_health_calendar_bar"

.field public static final DAY_TYPE_DIM:I = 0x1

.field public static final DAY_TYPE_DIM_DISABLE:I = 0x8

.field public static final DAY_TYPE_REGULAR:I = 0x0

.field public static final DAY_TYPE_SELECTED:I = 0x4

.field public static final DAY_TYPE_SELECTED_DIM:I = 0x6

.field public static final DAY_TYPE_SUNDAY:I = 0x2

.field public static final DAY_TYPE_SUNDAY_DIM:I = 0x3

.field public static final DAY_TYPE_TODAY:I = 0x5

.field public static final DAY_TYPE_UNSELECTED:I = 0x7

.field public static final DAY_TYPE_UNSELECTED_SUNDAY:I = 0x9

.field public static final DAY_TYPE_UNSELECTED_TODAY:I = 0xa

.field public static final UNDERLINE_STATE_INVISIBLE:I


# instance fields
.field private final resources:Landroid/content/res/Resources;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;-><init>(Landroid/content/Context;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    .line 55
    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;-><init>(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    .line 60
    return-void
.end method


# virtual methods
.method protected getButtonLayoutResourceId()I
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f03001d

    return v0
.end method

.method public setDayType(I)V
    .locals 5
    .param p1, "dayType"    # I

    .prologue
    const v4, 0x7f0700a5

    const/4 v1, 0x1

    const v3, 0x7f0700a1

    .line 79
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mContent:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 82
    packed-switch p1, :pswitch_data_0

    .line 143
    :goto_1
    return-void

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0700a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 88
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0700a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 93
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 97
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0700a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 102
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    goto :goto_1

    .line 109
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    const v1, 0x7f020032

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    const v2, 0x7f070044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 117
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    const v2, 0x7f070106

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 123
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 128
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 132
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 137
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0700ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public setUnderlineState(I)V
    .locals 0
    .param p1, "underlineState"    # I

    .prologue
    .line 154
    return-void
.end method
