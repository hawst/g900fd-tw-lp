.class public interface abstract Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;
.super Ljava/lang/Object;
.source "WalkingMateActiveTimeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CallBacks"
.end annotation


# virtual methods
.method public abstract onActivetimeChanged(J)V
.end method

.method public abstract onHealthyStepCountChanged(J)V
.end method

.method public abstract onHealthyStepStatusChanged(Z)V
.end method

.method public abstract onInactiveTimeChanged(ZJZ)V
.end method
