.class public Lcom/sec/android/app/shealth/framework/ui/receiver/PluginIntents;
.super Ljava/lang/Object;


# static fields
.field public static final ACTIONS:Ljava/lang/String; = "actions"

.field public static final ACTION_DELETE_EMPTY_MEAL:Ljava/lang/String; = "com.sec.shealth.food.action.DELETE_EMPTY_MEAL"

.field public static final ACTION_DELETE_PLUGIN_APP_DATA:Ljava/lang/String; = "com.sec.shealth.request.plugin.DELETE_APP_DATA"

.field public static final ACTION_LAUNCH_PLUGIN_WIDGET_ACTIVITY:Ljava/lang/String; = "com.sec.shealth.action.LAUNCH_APP_WIDGET_ACTIVITY"

.field public static final ACTION_PLUGIN_APK_INFORMATION:Ljava/lang/String; = "com.sec.shealth.send.package.information"

.field public static final ACTION_RECEIVE_PLUGIN_DATA:Ljava/lang/String; = "com.sec.shealth.receive.plugin.information"

.field public static final ACTION_REQUEST_PLUGIN_APK_INFORMATION:Ljava/lang/String; = "com.sec.shealth.request.package.information"

.field public static final ACTION_REQUEST_PLUGIN_DATA:Ljava/lang/String; = "com.sec.shealth.request.plugin.information"

.field public static final ACTION_RESET_COACH_DATA:Ljava/lang/String; = "com.sec.shealth.action.RESET_COACH_DATA"

.field public static final ACTION_RESET_FOOD_DATA:Ljava/lang/String; = "com.sec.shealth.action.RESET_FOOD_DATA"

.field public static final ACTION_RESET_SPO2_DATA:Ljava/lang/String; = "com.sec.shealth.action.RESET_SPO2_DATA"

.field public static final ACTION_RESET_STRESS_DATA:Ljava/lang/String; = "com.sec.shealth.action.RESET_STRESS_DATA"

.field public static final ACTION_RESET_WEIGHT_DATA:Ljava/lang/String; = "com.sec.shealth.action.RESET_WEIGHT_DATA"

.field public static final ACTION_SHEALTH_SHUTDOWN:Ljava/lang/String; = "com.sec.shealth.action.SHEALTH_SHUTDOWN"

.field public static final ACTION_SHUTDOWN:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_STEALTH_MODE:Ljava/lang/String; = "com.sec.shealth.action.STEALTH_MODE"

.field public static final APPLICATION_CATEGORY:Ljava/lang/String; = "application_category"

.field public static final APPLICATION_NAME:Ljava/lang/String; = "application_name"

.field public static final APP_DB_DELETABLE:Ljava/lang/String; = "app_deletable_db"

.field public static final APP_DB_DELETE_INFO:Ljava/lang/String; = "app_delete_db_info"

.field public static final APP_HEALTH_MEASURE_KEY:Ljava/lang/String; = "health_measure_type"

.field public static final APP_TYPE:Ljava/lang/String; = "app_type"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final PLUGIN_DSIPLAY_ICON:Ljava/lang/String; = "icon_names"

.field public static final PLUGIN_DSIPLAY_NAME:Ljava/lang/String; = "app_names"

.field public static final PLUGIN_ID:Ljava/lang/String; = "plugin_id"

.field public static final REQUEST_LAUNCH_PLUGIN_WIDGET_ACTIVITY:Ljava/lang/String; = "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

.field public static final REQUEST_PARAMETER:Ljava/lang/String; = "request_parameter"

.field public static final RESULT_VALUE:Ljava/lang/String; = "result_value"

.field public static final SOURCE_PACKAGE:Ljava/lang/String; = "source_package"

.field public static final SPP_ENABLED_STATUS:Ljava/lang/String; = "spp_enabled"

.field public static final SUPPORT_ACCESSORY:Ljava/lang/String; = "support_accessory"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
