.class public Lcom/sec/android/app/shealth/stress/StressInformationAreaView;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "StressInformationAreaView.java"


# static fields
.field private static final DATE_SEPARATOR:Ljava/lang/String; = "/"

.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field private static final DAY_FORMAT_PATTERN:Ljava/lang/String; = "dd"

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MM"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field private static final TIME_FORMAT_12HOUR:Ljava/lang/String; = "h:mm a"

.field private static final TIME_FORMAT_24HOUR:Ljava/lang/String; = "HH:mm"

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"

.field private static final YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"


# instance fields
.field private dateFormat:Ljava/text/DateFormat;

.field private mDateFormatOrder:[C

.field private mTimeFormatPattern:Ljava/lang/String;

.field private mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private stressStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;

.field private tvDate:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 51
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mDateFormatOrder:[C

    .line 52
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "HH:mm"

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    .line 58
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->initLayout()V

    .line 59
    return-void

    .line 55
    :cond_0
    const-string v0, "h:mm a"

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    goto :goto_0
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 97
    const v0, 0x7f0809e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->tvDate:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0809e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->stressStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;

    .line 99
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030238

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 102
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mDateFormatOrder:[C

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    .line 104
    .local v0, "dateFormatPattern":Ljava/lang/String;
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 106
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_1

    .line 107
    const-string v1, ""

    .line 112
    .local v1, "dayFormatPattern":Ljava/lang/String;
    :goto_0
    const-string v2, "d+"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    const-string v2, "M+"

    const-string v3, "MM/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    const-string/jumbo v2, "y+"

    const-string/jumbo v3, "yyyy/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    const-string v2, "/$"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_0

    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 122
    return-void

    .line 109
    .end local v1    # "dayFormatPattern":Ljava/lang/String;
    :cond_1
    const-string v1, "dd/"

    .restart local v1    # "dayFormatPattern":Ljava/lang/String;
    goto :goto_0
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 7
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/4 v6, 0x0

    .line 77
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "dateValue":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInformationAreaDataByPeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;)Lcom/sec/android/app/shealth/stress/data/StressData;

    move-result-object v2

    .line 80
    .local v2, "stressData":Lcom/sec/android/app/shealth/stress/data/StressData;
    new-instance v0, Ljava/util/Date;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 81
    .local v0, "date":Ljava/util/Date;
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->setSelectedDateInChart(J)V

    .line 83
    if-eqz v2, :cond_0

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v3, v4, :cond_1

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->stressStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/data/StressData;->getScore()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;->moveToPolygon(D)V

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->stressStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;->setGraphAverageVisibility(Z)V

    .line 93
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->tvDate:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    return-void

    .line 89
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->stressStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/data/StressData;->getAverage()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;->moveToPolygon(D)V

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->stressStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/stress/custom/StressStateGraphBar;->setGraphAverageVisibility(Z)V

    goto :goto_0
.end method
