.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "HeartRateScoverActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->initSCover()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v2, 0x1

    .line 101
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->LaunchSHealthActivity()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 106
    .local v0, "scoverintent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 107
    const-string/jumbo v1, "scover"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 108
    const-string v1, "com.sec.shealth.action.HEART_RATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->finish()V

    goto :goto_0
.end method
