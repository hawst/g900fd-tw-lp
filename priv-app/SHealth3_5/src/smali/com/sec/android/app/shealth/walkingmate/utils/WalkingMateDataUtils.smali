.class public Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;
.super Ljava/lang/Object;
.source "WalkingMateDataUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$GoalData;,
        Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    }
.end annotation


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final HOURS_IN_DAY:B = 0x18t

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MINUTE:I = 0xea60

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MILLIS_IN_WEEK:I = 0x240c8400

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MONTH_IN_YEAR:I = 0xc

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field public static final SQLITE_WALKINGMATE_GROPUBY_20MIN:Ljava/lang/String; = "strftime(\"%d-%m-%Y %H\",([start_time]/1000),\'unixepoch\',\'localtime\')), (strftime(\"%M\",([start_time]/1000),\'unixepoch\',\'localtime\')/20"

.field public static final SQLITE_WALKINGMATE_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

.field public static final SQLITE_WALKINGMATE_GROUPBY_HOUR:Ljava/lang/String; = "strftime(\"%d-%m-%Y %H\",([start_time]/1000),\'unixepoch\',\'localtime\')"

.field public static final SQLITE_WALKINGMATE_GROUPBY_MONTH:Ljava/lang/String; = "strftime(\"%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"

.field private static final TAG:Ljava/lang/String; = "WalkingMateDataUtils"

.field public static mGraphCommonQuery:Ljava/lang/StringBuilder;

.field private static final sDevicesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    .line 80
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SELECT application__id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "user_device__id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SUM(total_step) AS SUM_TOTAL_STEP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SUM(walk_step) AS SUM_WALK_STEP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, "SUM(run_step) AS SUM_RUN_STEP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, "MIN(start_time) AS start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "walk_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->sDevicesList:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496
    return-void
.end method

.method public static deleteExercise([J)V
    .locals 12
    .param p0, "deleted_time"    # [J

    .prologue
    .line 1133
    const-string v8, "WalkingMateDataUtils"

    const-string v9, "delete exercise related with walkingmate"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1134
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    .line 1135
    .local v1, "deviceType":I
    const-string/jumbo v4, "start_time >= ? "

    .line 1136
    .local v4, "selection":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " AND start_time <= ? "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1137
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " AND exercise_info__id = 18001"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1138
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " AND exercise_type = 20003"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1139
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " AND _id in(SELECT EX._id FROM exercise AS EX, exercise_device_info AS EDI WHERE EX.exercise_type = 20003 AND EX._id = EDI.exercise__id AND EDI.user_device__id like \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "%\' )"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1145
    move-object v0, p0

    .local v0, "arr$":[J
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-wide v6, v0, v2

    .line 1146
    .local v6, "time":J
    const/4 v8, 0x2

    new-array v5, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    .line 1149
    .local v5, "selectionArgs":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1145
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1151
    .end local v5    # "selectionArgs":[Ljava/lang/String;
    .end local v6    # "time":J
    :cond_0
    return-void
.end method

.method public static getDayOfLastDataByMonth(J)J
    .locals 16
    .param p0, "monthTime"    # J

    .prologue
    .line 1194
    const-string v0, "WalkingMateDataUtils"

    const-string v1, "lastDataDaybyMonth"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v14

    .line 1196
    .local v14, "startMonth":J
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v9

    .line 1197
    .local v9, "endMonth":J
    const-wide/16 v11, 0x0

    .line 1198
    .local v11, "lastDataTime":J
    const/4 v6, 0x0

    .line 1200
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 1201
    .local v7, "deviceType":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 1202
    .local v13, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT start_time"

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1203
    const-string v0, " FROM walk_info"

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1204
    const-string v0, " WHERE "

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1205
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1206
    const-string v0, " AND "

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207
    const-string/jumbo v0, "total_step > 0"

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1208
    const-string v0, " AND "

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1210
    const-string v0, " AND "

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time <= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1212
    const-string v0, " ORDER BY start_time DESC LIMIT 1"

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1213
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1215
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1216
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1217
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v11

    .line 1224
    :goto_0
    if-eqz v6, :cond_0

    .line 1225
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1229
    .end local v7    # "deviceType":I
    .end local v13    # "query":Ljava/lang/StringBuilder;
    :cond_0
    :goto_1
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    return-wide v0

    .line 1219
    .restart local v7    # "deviceType":I
    .restart local v13    # "query":Ljava/lang/StringBuilder;
    :cond_1
    const-wide/16 v11, 0x0

    goto :goto_0

    .line 1221
    .end local v7    # "deviceType":I
    .end local v13    # "query":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v8

    .line 1222
    .local v8, "e":Ljava/lang/RuntimeException;
    const-wide/16 v11, 0x0

    .line 1224
    if-eqz v6, :cond_0

    .line 1225
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1224
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1225
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getGearLastUpdatedTimeFromDB(I)J
    .locals 10
    .param p0, "deviceType"    # I

    .prologue
    .line 745
    const-wide/16 v8, 0x0

    .line 747
    .local v8, "time":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 748
    .local v7, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 749
    const-string/jumbo v0, "update_time"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 750
    const-string v0, " FROM "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 751
    const-string/jumbo v0, "walk_info"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 752
    const-string v0, " WHERE "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 753
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 754
    const-string v0, " AND "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 755
    const-string/jumbo v0, "sync_status != 170004"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    const-string v0, " AND "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 757
    const-string/jumbo v0, "total_step > 0 "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 758
    const-string v0, " ORDER BY "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 759
    const-string/jumbo v0, "update_time"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 760
    const-string v0, " DESC LIMIT 1 "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 762
    const/4 v6, 0x0

    .line 764
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 766
    if-eqz v6, :cond_3

    .line 767
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 768
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 769
    const-string/jumbo v0, "update_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 777
    :goto_0
    if-eqz v6, :cond_0

    .line 778
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 781
    :cond_0
    const-string v0, "WalkingMateDataUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getGearLastUpdatedTimeFromDB - deviceType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    return-wide v8

    .line 771
    :cond_1
    :try_start_1
    const-string v0, "WalkingMateDataUtils"

    const-string/jumbo v1, "updateFromDB() - No data"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 777
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 778
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 774
    :cond_3
    :try_start_2
    const-string v0, "WalkingMateDataUtils"

    const-string/jumbo v1, "updateFromDB() - No data"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static getPassedDaysForMonthAverage(J)I
    .locals 25
    .param p0, "startTimeOfMonth"    # J

    .prologue
    .line 417
    const/4 v6, 0x0

    .line 418
    .local v6, "daysInMonth":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v3

    .line 419
    .local v3, "calendar":Ljava/util/Calendar;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshFirstDate()V

    .line 420
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshLastDate()V

    .line 422
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getFirstDate()J

    move-result-wide v11

    .line 423
    .local v11, "firstDataTime":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getLastDate()J

    move-result-wide v19

    .line 425
    .local v19, "lastDataTime":J
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v9

    .line 427
    .local v9, "firstDataMonth":J
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v17

    .line 428
    .local v17, "lastDataMonth":J
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v13

    .line 430
    .local v13, "lastDataDay":J
    const/4 v2, 0x1

    .line 432
    .local v2, "FIRST_DAY":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v4

    .line 433
    .local v4, "currentMonth":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v21

    .line 435
    .local v21, "today":J
    cmp-long v23, p0, v9

    if-nez v23, :cond_2

    .line 437
    cmp-long v23, p0, v4

    if-nez v23, :cond_1

    .line 439
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getDayOfLastDataByMonth(J)J

    move-result-wide v15

    .line 441
    .local v15, "lastDataDayForMonth":J
    cmp-long v23, v15, v21

    if-ltz v23, :cond_0

    .line 442
    move-wide v0, v15

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 447
    :goto_0
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 449
    .local v8, "daysUntilToday":I
    invoke-virtual {v3, v11, v12}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 450
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 452
    .local v7, "daysUntilFirstDataDay":I
    sub-int v23, v8, v7

    add-int/lit8 v6, v23, 0x1

    .line 487
    .end local v7    # "daysUntilFirstDataDay":I
    .end local v8    # "daysUntilToday":I
    .end local v15    # "lastDataDayForMonth":J
    :goto_1
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "daysInMonth : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 489
    return v6

    .line 444
    .restart local v15    # "lastDataDayForMonth":J
    :cond_0
    move-wide/from16 v0, v21

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0

    .line 454
    .end local v15    # "lastDataDayForMonth":J
    :cond_1
    invoke-virtual {v3, v11, v12}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 455
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v23

    const/16 v24, 0x5

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v24

    sub-int v23, v23, v24

    add-int/lit8 v6, v23, 0x1

    goto :goto_1

    .line 458
    :cond_2
    cmp-long v23, p0, v17

    if-nez v23, :cond_4

    cmp-long v23, p0, v4

    if-nez v23, :cond_4

    .line 460
    cmp-long v23, v13, v21

    if-ltz v23, :cond_3

    .line 461
    invoke-virtual {v3, v13, v14}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 466
    :goto_2
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v6

    goto :goto_1

    .line 463
    :cond_3
    move-wide/from16 v0, v21

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_2

    .line 470
    :cond_4
    cmp-long v23, p0, v4

    if-nez v23, :cond_6

    .line 472
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getDayOfLastDataByMonth(J)J

    move-result-wide v15

    .line 474
    .restart local v15    # "lastDataDayForMonth":J
    cmp-long v23, v15, v21

    if-ltz v23, :cond_5

    .line 475
    move-wide v0, v15

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 479
    :goto_3
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 481
    goto :goto_1

    .line 477
    :cond_5
    move-wide/from16 v0, v21

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_3

    .line 482
    .end local v15    # "lastDataDayForMonth":J
    :cond_6
    move-wide/from16 v0, p0

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 483
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v6

    goto/16 :goto_1
.end method

.method public static getQueryConditionByDeviceType(I)Ljava/lang/String;
    .locals 3
    .param p0, "deviceType"    # I

    .prologue
    .line 1181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1182
    .local v0, "selection":Ljava/lang/StringBuilder;
    const/16 v1, 0x272f

    if-ne p0, v1, :cond_0

    .line 1183
    const-string/jumbo v1, "user_device__id NOT LIKE \'10009%\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1184
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1185
    const-string/jumbo v1, "user_device__id NOT LIKE \'10023%\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1190
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1187
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "user_device__id LIKE \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\' "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getSynchronizedWearableList(J)Ljava/util/ArrayList;
    .locals 3
    .param p0, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 908
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->sDevicesList:Ljava/util/List;

    monitor-enter v1

    .line 909
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->sDevicesList:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    .line 910
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getTotalStepsFromDeviceType(IJ)I
    .locals 9
    .param p0, "deviceType"    # I
    .param p1, "time"    # J

    .prologue
    .line 787
    const/4 v8, 0x0

    .line 789
    .local v8, "totalSteps":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 790
    .local v7, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 791
    const-string v0, "SUM(total_step) AS total_step"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    const-string v0, " , "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793
    const-string v0, "MAX(update_time) AS update_time"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794
    const-string v0, " FROM "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    const-string/jumbo v0, "walk_info"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    const-string v0, " WHERE "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 797
    const-string/jumbo v0, "sync_status != 170004"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    const-string v0, " AND "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    const-string/jumbo v0, "total_step > 0 "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 800
    const-string v0, " AND "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 801
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    const-string v0, " AND "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 803
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time <= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804
    const-string v0, " AND "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "user_device__id LIKE \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 807
    const/4 v6, 0x0

    .line 809
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 812
    if-eqz v6, :cond_2

    .line 813
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 814
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 815
    const-string/jumbo v0, "total_step"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 822
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 823
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 826
    :cond_1
    const-string v0, "WalkingMateDataUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTotalStepsFromDeviceType() - totalSteps:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    return v8

    .line 818
    :cond_2
    :try_start_1
    const-string v0, "WalkingMateDataUtils"

    const-string v1, "getTotalStepsFromDeviceType() - No data"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 819
    const/4 v8, 0x0

    goto :goto_0

    .line 822
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 823
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getTotalValuesFromAllDevice(JI)Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;
    .locals 14
    .param p0, "time"    # J
    .param p2, "deviceType"    # I

    .prologue
    .line 676
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;-><init>(IFFJ)V

    .line 677
    .local v0, "utilsData":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;
    const/4 v11, 0x0

    .line 678
    .local v11, "totalSteps":I
    const/4 v9, 0x0

    .line 679
    .local v9, "distance":F
    const/4 v7, 0x0

    .line 680
    .local v7, "calories":F
    const-wide/16 v12, 0x0

    .line 682
    .local v12, "updateTime":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 684
    .local v10, "query":Ljava/lang/StringBuilder;
    const-string v1, "SELECT "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 685
    const-string v1, "SUM(total_step) AS total_step"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    const-string v1, " , "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    const-string v1, "SUM(distance) AS distance"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    const-string v1, " , "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    const-string v1, "SUM(calorie) AS calorie"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    const-string v1, " , "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    const-string v1, "MAX(update_time) AS update_time"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    const-string v1, " FROM "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    const-string/jumbo v1, "walk_info"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    const-string v1, " WHERE "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    const-string v1, " AND "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    const-string/jumbo v1, "sync_status != 170004"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    const-string v1, " AND "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start_time >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    const-string v1, " AND "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start_time <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    const-string v1, " AND "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 705
    const-string/jumbo v1, "total_step > 0 "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 707
    const/4 v8, 0x0

    .line 709
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 711
    if-eqz v8, :cond_2

    .line 712
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 713
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 714
    const-string/jumbo v1, "update_time"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 715
    const-string/jumbo v1, "total_step"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 716
    const-string v1, "distance"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    .line 717
    const-string v1, "calorie"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 726
    :cond_0
    :goto_0
    if-eqz v8, :cond_1

    .line 727
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 731
    :cond_1
    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->setTotalSteps(I)V

    .line 732
    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->setCalories(F)V

    .line 733
    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->setDistance(F)V

    .line 734
    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->setUpdateTime(J)V

    .line 736
    const-string v1, "WalkingMateDataUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTotalValuesFromAllDevice() - totalSteps:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getTotalSteps()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", cal:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getCalories()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", distance:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getDistance()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", updateTime:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getUpdateTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    return-object v0

    .line 720
    :cond_2
    :try_start_1
    const-string v1, "WalkingMateDataUtils"

    const-string/jumbo v2, "updateFromDB() - No data"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 721
    const/4 v11, 0x0

    .line 722
    const/4 v9, 0x0

    .line 723
    const/4 v7, 0x0

    goto :goto_0

    .line 726
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_3

    .line 727
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public static getWalkInfoDataCount()I
    .locals 10

    .prologue
    .line 1100
    const/4 v6, 0x0

    .line 1101
    .local v6, "count":I
    const/4 v7, 0x0

    .line 1102
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v8

    .line 1104
    .local v8, "deviceType":I
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 1108
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v3

    .line 1109
    .local v3, "selection":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sync_status != 170004"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "total_step > 0 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1114
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1116
    if-eqz v7, :cond_0

    .line 1117
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 1118
    const-string v0, "WalkingMateDataUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WalkInfo data count = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1123
    :cond_0
    if-eqz v7, :cond_1

    .line 1124
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1128
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    :cond_1
    :goto_0
    return v6

    .line 1120
    :catch_0
    move-exception v9

    .line 1121
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1123
    if-eqz v7, :cond_1

    .line 1124
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1123
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 1124
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getWalkInfoDataForHourChart(I)Ljava/util/Vector;
    .locals 35
    .param p0, "deviceType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    const/4 v15, 0x0

    .line 104
    .local v15, "cursor":Landroid/database/Cursor;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    .line 105
    .local v12, "c":Ljava/util/Calendar;
    new-instance v19, Ljava/util/Vector;

    invoke-direct/range {v19 .. v19}, Ljava/util/Vector;-><init>()V

    .line 107
    .local v19, "hourDataVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;>;"
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .local v21, "query":Ljava/lang/StringBuilder;
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 111
    const-string v3, " WHERE "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v3, "total_step > 0 "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string v3, " AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v3, " ( "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string v3, "application__id = \'shealth2.5\' "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string v3, " OR "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string v3, "application__id = \'shealth2.6\' "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v3, " ) "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string v3, " AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string v3, " GROUP BY (strftime(\"%d-%m-%Y %H\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string v3, " UNION "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->mGraphCommonQuery:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 125
    const-string v3, " WHERE "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    const-string/jumbo v3, "total_step > 0 "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v3, " AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    const-string v3, " ( "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v3, "application__id != \'shealth2.5\' "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string v3, " AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-string v3, "application__id != \'shealth2.6\' "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string v3, " ) "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string v3, " AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string v3, " GROUP BY (strftime(\"%d-%m-%Y %H\",([start_time]/1000),\'unixepoch\',\'localtime\')), (strftime(\"%M\",([start_time]/1000),\'unixepoch\',\'localtime\')/20)"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 143
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Hour step query : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 144
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cursor Count =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 145
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_f

    .line 146
    :cond_0
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 147
    const-string/jumbo v3, "start_time"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 148
    const-string v3, "application__id"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 150
    .local v16, "dataVersion":Ljava/lang/String;
    const-string/jumbo v3, "shealth2.5"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 151
    const/16 v3, 0xc

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 152
    const/16 v3, 0xd

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 153
    const/16 v3, 0xe

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 183
    :cond_1
    :goto_1
    const-string v3, "SUM_RUN_STEP"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v23

    .line 184
    .local v23, "runStep":D
    const-string v3, "SUM_WALK_STEP"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v31

    .line 185
    .local v31, "walkStep":D
    const-string v3, "SUM_TOTAL_STEP"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v27

    .line 186
    .local v27, "totalStep":D
    sub-double v31, v27, v23

    .line 201
    const-string/jumbo v3, "shealth2.5"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "shealth2.6"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 205
    :cond_2
    const/4 v11, 0x3

    .line 206
    .local v11, "DIVISOR":I
    move-wide/from16 v0, v31

    double-to-int v3, v0

    div-int/lit8 v34, v3, 0x3

    .line 207
    .local v34, "walkStepTemp":I
    move-wide/from16 v0, v23

    double-to-int v3, v0

    div-int/lit8 v25, v3, 0x3

    .line 210
    .local v25, "runStepTemp":I
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_2
    const/4 v3, 0x3

    move/from16 v0, v20

    if-ge v0, v3, :cond_9

    .line 212
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    mul-int/lit8 v5, v20, 0x14

    const v6, 0xea60

    mul-int/2addr v5, v6

    int-to-long v5, v5

    add-long/2addr v3, v5

    move/from16 v0, v34

    int-to-double v5, v0

    move/from16 v0, v25

    int-to-double v7, v0

    const-wide/16 v9, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>(JDDD)V

    .line 213
    .local v2, "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 210
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 156
    .end local v2    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v11    # "DIVISOR":I
    .end local v20    # "i":I
    .end local v23    # "runStep":D
    .end local v25    # "runStepTemp":I
    .end local v27    # "totalStep":D
    .end local v31    # "walkStep":D
    .end local v34    # "walkStepTemp":I
    :cond_3
    const/16 v3, 0xd

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 157
    const/16 v3, 0xe

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 158
    const/16 v3, 0xc

    invoke-virtual {v12, v3}, Ljava/util/Calendar;->get(I)I

    move-result v14

    .line 160
    .local v14, "currentMinutes":I
    rem-int/lit8 v22, v14, 0xa

    .line 163
    .local v22, "remainder":I
    if-eqz v22, :cond_4

    .line 165
    const/16 v3, 0xc

    div-int/lit8 v4, v14, 0xa

    mul-int/lit8 v4, v4, 0xa

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 170
    :cond_4
    if-eqz v14, :cond_1

    div-int/lit8 v3, v14, 0xa

    rem-int/lit8 v3, v3, 0x2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 171
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x927c0

    sub-long/2addr v3, v5

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 271
    .end local v14    # "currentMinutes":I
    .end local v16    # "dataVersion":Ljava/lang/String;
    .end local v22    # "remainder":I
    :catch_0
    move-exception v17

    .line 272
    .local v17, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    if-eqz v15, :cond_5

    .line 275
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 279
    .end local v17    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_3
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$1;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$1;-><init>()V

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 292
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 293
    const-string v3, "SELECT"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    const-string v3, " "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string v3, "MIN(start_time) AS start_time"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    const-string v3, " , "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const-string v3, "ifnull(SUM(power_step), 0) AS SUM_HEALTHY_STEP"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string v3, " FROM walk_info_extended"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string v3, " WHERE "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    const-string v3, " GROUP BY (strftime(\"%d-%m-%Y %H\",([start_time]/1000),\'unixepoch\',\'localtime\')), (strftime(\"%M\",([start_time]/1000),\'unixepoch\',\'localtime\')/20)"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    const-string v3, " ORDER BY start_time ASC"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 307
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Healthy step query : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 308
    new-instance v13, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-direct {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>()V

    .line 310
    .local v13, "comparetime":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_10

    .line 311
    :cond_6
    :goto_4
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 312
    const-string/jumbo v3, "start_time"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 313
    const/16 v3, 0xd

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 314
    const/16 v3, 0xe

    const/4 v4, 0x0

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 315
    const/16 v3, 0xc

    invoke-virtual {v12, v3}, Ljava/util/Calendar;->get(I)I

    move-result v14

    .line 318
    .restart local v14    # "currentMinutes":I
    if-eqz v14, :cond_7

    div-int/lit8 v3, v14, 0xa

    rem-int/lit8 v3, v3, 0x2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 319
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x927c0

    sub-long/2addr v3, v5

    invoke-virtual {v12, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 322
    :cond_7
    const-string v3, "SUM_HEALTHY_STEP"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 323
    .local v18, "healthy_step":I
    const-string v3, "healthy step time from healthy step DB"

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->printDate(Ljava/lang/String;J)V

    .line 330
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v13, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setStartTime(J)V

    .line 331
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$2;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$2;-><init>()V

    move-object/from16 v0, v19

    invoke-static {v0, v13, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v30

    .line 344
    .local v30, "walkInfoIndex":I
    if-ltz v30, :cond_6

    .line 345
    move-object/from16 v0, v19

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    move/from16 v0, v18

    int-to-double v4, v0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->setHealthyStep(D)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_4

    .line 349
    .end local v13    # "comparetime":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v14    # "currentMinutes":I
    .end local v18    # "healthy_step":I
    .end local v30    # "walkInfoIndex":I
    :catch_1
    move-exception v17

    .line 350
    .restart local v17    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 352
    if-eqz v15, :cond_8

    .line 353
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 361
    .end local v17    # "e":Ljava/lang/Exception;
    :cond_8
    :goto_5
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->handleDuplication(Ljava/util/Vector;)Ljava/util/Vector;

    move-result-object v19

    .line 363
    return-object v19

    .line 218
    .restart local v11    # "DIVISOR":I
    .restart local v16    # "dataVersion":Ljava/lang/String;
    .restart local v20    # "i":I
    .restart local v23    # "runStep":D
    .restart local v25    # "runStepTemp":I
    .restart local v27    # "totalStep":D
    .restart local v31    # "walkStep":D
    .restart local v34    # "walkStepTemp":I
    :cond_9
    const/16 v33, 0x0

    .line 219
    .local v33, "walkStepRemainder":I
    const/16 v26, 0x0

    .line 220
    .local v26, "runStepTempRemainder":I
    const/16 v29, 0x0

    .line 222
    .local v29, "upStepTempRemainder":I
    const-wide/high16 v3, 0x4008000000000000L    # 3.0

    cmpg-double v3, v31, v3

    if-gez v3, :cond_c

    .line 223
    move-wide/from16 v0, v31

    double-to-int v0, v0

    move/from16 v33, v0

    .line 228
    :goto_6
    const-wide/high16 v3, 0x4008000000000000L    # 3.0

    cmpg-double v3, v23, v3

    if-gez v3, :cond_d

    .line 229
    move-wide/from16 v0, v23

    double-to-int v0, v0

    move/from16 v26, v0

    .line 241
    :goto_7
    if-nez v33, :cond_a

    if-eqz v26, :cond_0

    .line 243
    :cond_a
    :try_start_4
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    move/from16 v0, v33

    int-to-double v5, v0

    move/from16 v0, v26

    int-to-double v7, v0

    const-wide/16 v9, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>(JDDD)V

    .line 244
    .restart local v2    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 274
    .end local v2    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v11    # "DIVISOR":I
    .end local v16    # "dataVersion":Ljava/lang/String;
    .end local v20    # "i":I
    .end local v23    # "runStep":D
    .end local v25    # "runStepTemp":I
    .end local v26    # "runStepTempRemainder":I
    .end local v27    # "totalStep":D
    .end local v29    # "upStepTempRemainder":I
    .end local v31    # "walkStep":D
    .end local v33    # "walkStepRemainder":I
    .end local v34    # "walkStepTemp":I
    :catchall_0
    move-exception v3

    if-eqz v15, :cond_b

    .line 275
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v3

    .line 225
    .restart local v11    # "DIVISOR":I
    .restart local v16    # "dataVersion":Ljava/lang/String;
    .restart local v20    # "i":I
    .restart local v23    # "runStep":D
    .restart local v25    # "runStepTemp":I
    .restart local v26    # "runStepTempRemainder":I
    .restart local v27    # "totalStep":D
    .restart local v29    # "upStepTempRemainder":I
    .restart local v31    # "walkStep":D
    .restart local v33    # "walkStepRemainder":I
    .restart local v34    # "walkStepTemp":I
    :cond_c
    move-wide/from16 v0, v31

    double-to-int v3, v0

    :try_start_5
    rem-int/lit8 v33, v3, 0x3

    goto :goto_6

    .line 231
    :cond_d
    move-wide/from16 v0, v23

    double-to-int v3, v0

    rem-int/lit8 v26, v3, 0x3

    goto :goto_7

    .line 266
    .end local v11    # "DIVISOR":I
    .end local v20    # "i":I
    .end local v25    # "runStepTemp":I
    .end local v26    # "runStepTempRemainder":I
    .end local v29    # "upStepTempRemainder":I
    .end local v33    # "walkStepRemainder":I
    .end local v34    # "walkStepTemp":I
    :cond_e
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide/16 v9, 0x0

    move-wide/from16 v5, v31

    move-wide/from16 v7, v23

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;-><init>(JDDD)V

    .line 267
    .restart local v2    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 274
    .end local v2    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .end local v16    # "dataVersion":Ljava/lang/String;
    .end local v23    # "runStep":D
    .end local v27    # "totalStep":D
    .end local v31    # "walkStep":D
    :cond_f
    if-eqz v15, :cond_5

    .line 275
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 352
    .restart local v13    # "comparetime":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    :cond_10
    if-eqz v15, :cond_8

    .line 353
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 352
    .end local v13    # "comparetime":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    :catchall_1
    move-exception v3

    if-eqz v15, :cond_11

    .line 353
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_11
    throw v3
.end method

.method private static handleDuplication(Ljava/util/Vector;)Ljava/util/Vector;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
            ">;)",
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    .local p0, "original":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;>;"
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 369
    .local v2, "newDataVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;>;"
    const/4 v3, 0x0

    .line 370
    .local v3, "previous":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    invoke-virtual {p0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .line 371
    .local v0, "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    if-nez v3, :cond_0

    .line 372
    move-object v3, v0

    .line 373
    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 377
    :cond_0
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 402
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$100(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D

    move-result-wide v4

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mHealthyStep:D
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$118(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D

    .line 403
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$200(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D

    move-result-wide v4

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mRunStep:D
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$218(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D

    .line 404
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$300(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D

    move-result-wide v4

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mUpStep:D
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$318(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D

    .line 405
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$400(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)D

    move-result-wide v4

    # += operator for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mWalkStep:D
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$418(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;D)D

    goto :goto_0

    .line 407
    :cond_1
    move-object v3, v0

    .line 408
    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 412
    .end local v0    # "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    :cond_2
    invoke-virtual {p0}, Ljava/util/Vector;->clear()V

    .line 413
    return-object v2
.end method

.method public static loadGoalAndSearch(J)I
    .locals 20
    .param p0, "time"    # J

    .prologue
    .line 626
    const/16 v16, 0x2710

    .line 627
    .local v16, "returnGoal":I
    invoke-static/range {p0 .. p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v8

    .line 629
    .local v8, "checkTime":J
    const/4 v10, 0x0

    .line 630
    .local v10, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v12

    .line 632
    .local v12, "deviceType":I
    const/4 v2, 0x3

    :try_start_0
    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "user_device__id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "value"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "set_time"

    aput-object v3, v4, v2

    .line 635
    .local v4, "projection":[Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 637
    .local v17, "selection":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 638
    const-string v2, "goal_type = 40001"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    const-string v2, " AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 640
    invoke-static {v12}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 642
    const-string/jumbo v7, "set_time DESC"

    .line 644
    .local v7, "orderby":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 646
    const/4 v15, 0x0

    .line 647
    .local v15, "isGoal":Z
    :cond_0
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 648
    const-string/jumbo v2, "user_device__id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 649
    .local v11, "device":Ljava/lang/String;
    const-string/jumbo v2, "value"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 650
    .local v14, "goal":I
    const-string/jumbo v2, "set_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 651
    .local v18, "settime":J
    const-string v2, "WalkingMateDataUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "device:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", goal:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", settime:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v18

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    if-nez v15, :cond_0

    cmp-long v2, v18, v8

    if-gtz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v2, v18, v2

    if-gtz v2, :cond_0

    .line 653
    move/from16 v16, v14

    .line 654
    const/4 v15, 0x1

    goto :goto_0

    .line 660
    .end local v11    # "device":Ljava/lang/String;
    .end local v14    # "goal":I
    .end local v18    # "settime":J
    :cond_1
    if-eqz v10, :cond_2

    .line 661
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 665
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v7    # "orderby":Ljava/lang/String;
    .end local v15    # "isGoal":Z
    .end local v17    # "selection":Ljava/lang/StringBuilder;
    :cond_2
    :goto_1
    const-string v2, "WalkingMateDataUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deviceType:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", goal:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    return v16

    .line 657
    :catch_0
    move-exception v13

    .line 658
    .local v13, "ex":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 660
    if-eqz v10, :cond_2

    .line 661
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 660
    .end local v13    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_3

    .line 661
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method public static updateSynchronizedWearableList()V
    .locals 15

    .prologue
    .line 835
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 905
    .local v6, "cursor":Landroid/database/Cursor;
    .local v8, "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local v10, "query":Ljava/lang/StringBuilder;
    .local v13, "time":J
    :goto_0
    return-void

    .line 839
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v10    # "query":Ljava/lang/StringBuilder;
    .end local v13    # "time":J
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 841
    .restart local v13    # "time":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 842
    .restart local v10    # "query":Ljava/lang/StringBuilder;
    const-string v0, " SELECT "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 843
    const-string/jumbo v0, "user_device__id"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 844
    const-string v0, " FROM "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    const-string/jumbo v0, "walk_info"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 846
    const-string v0, " WHERE "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 848
    const-string v0, " AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 849
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time <= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 850
    const-string v0, " GROUP BY "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 851
    const-string/jumbo v0, "user_device__id"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    const-string v0, " ORDER BY "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 853
    const-string/jumbo v0, "update_time"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 854
    const-string v0, " DESC "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 856
    const/4 v8, 0x0

    .line 858
    .restart local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 860
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 862
    if-eqz v6, :cond_7

    .line 863
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 864
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 865
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 867
    .end local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local v9, "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    :try_start_1
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 869
    .local v12, "temp":Ljava/lang/String;
    const-string v0, "_"

    invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v11, v0, v1

    .line 870
    .local v11, "synchedDevice":Ljava/lang/String;
    const-string v0, "WalkingMateDataUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SyncedDevice:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 872
    .local v7, "deviceType":I
    packed-switch v7, :pswitch_data_0

    .line 885
    :pswitch_0
    const-string v0, "WalkingMateDataUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not wearable."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    move-object v8, v9

    .line 894
    .end local v7    # "deviceType":I
    .end local v9    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v11    # "synchedDevice":Ljava/lang/String;
    .end local v12    # "temp":Ljava/lang/String;
    .restart local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_2
    :goto_2
    if-eqz v6, :cond_3

    .line 895
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 899
    :cond_3
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->sDevicesList:Ljava/util/List;

    monitor-enter v1

    .line 900
    if-eqz v8, :cond_4

    :try_start_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 901
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->sDevicesList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 902
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->sDevicesList:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 904
    :cond_4
    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 879
    .end local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v7    # "deviceType":I
    .restart local v9    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v11    # "synchedDevice":Ljava/lang/String;
    .restart local v12    # "temp":Ljava/lang/String;
    :pswitch_1
    :try_start_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 880
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 894
    .end local v7    # "deviceType":I
    .end local v11    # "synchedDevice":Ljava/lang/String;
    .end local v12    # "temp":Ljava/lang/String;
    :catchall_1
    move-exception v0

    move-object v8, v9

    .end local v9    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_3
    if-eqz v6, :cond_5

    .line 895
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 882
    .end local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v7    # "deviceType":I
    .restart local v9    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v11    # "synchedDevice":Ljava/lang/String;
    .restart local v12    # "temp":Ljava/lang/String;
    :cond_6
    :try_start_4
    const-string v0, "WalkingMateDataUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is already in deviceTypeList"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 891
    .end local v7    # "deviceType":I
    .end local v9    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v11    # "synchedDevice":Ljava/lang/String;
    .end local v12    # "temp":Ljava/lang/String;
    .restart local v8    # "deviceTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_7
    :try_start_5
    const-string v0, "WalkingMateDataUtils"

    const-string v1, "getSynchedDeviceList() - No data"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_2

    .line 894
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 872
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
