.class Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;
.super Ljava/lang/Object;
.source "WeightSetGoalWeightFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 249
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mDatePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$500(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    .line 250
    .local v1, "dialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mDatePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$500(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f080325

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/DatePicker;

    .line 253
    .local v2, "localInstance":Landroid/widget/DatePicker;
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Ljava/util/Calendar;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mDatePicker:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;
    invoke-static {v4}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$500(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Ljava/util/Calendar;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getYear()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Ljava/util/Calendar;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Ljava/util/Calendar;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 257
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 258
    .local v0, "dateFormat":Ljava/text/DateFormat;
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mTargetDateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$600(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    # getter for: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->mCal:Ljava/util/Calendar;
    invoke-static {v4}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->access$300(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;)Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 261
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    .end local v2    # "localInstance":Landroid/widget/DatePicker;
    :cond_0
    return-void
.end method
