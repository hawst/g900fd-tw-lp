.class Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;
.super Ljava/lang/Object;
.source "AddCustomFoodActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->initListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    .line 459
    .local v6, "currentFocusedView":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    if-ne v6, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->processEmptyField()V

    .line 471
    :goto_0
    return-void

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    const/4 v1, 0x0

    const/4 v5, 0x1

    move v3, v2

    move v4, v2

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->setVisibilityForNutrientFields(IFFFZ)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$700(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;IFFFZ)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$5;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$800(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    goto :goto_0
.end method
