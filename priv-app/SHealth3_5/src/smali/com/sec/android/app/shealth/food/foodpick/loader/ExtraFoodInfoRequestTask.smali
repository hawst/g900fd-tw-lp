.class public Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
.source "ExtraFoodInfoRequestTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;-><init>()V

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mContext:Landroid/content/Context;

    .line 49
    return-void
.end method


# virtual methods
.method protected accompanyWithLoadingDialog()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 60
    .local v1, "request":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchFactory;->getSearchApiBySourceType(Landroid/content/Context;I)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    .line 61
    .local v0, "foodSearchApi":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    if-nez v0, :cond_0

    .line 63
    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "foodSearchApi is null Bad ServerSourceType :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 67
    :cond_0
    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performExtraFoodInfoRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v2

    return-object v2
.end method

.method public onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V
    .locals 4
    .param p1, "extraFoodInfoResult"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 80
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mFoodInfoData:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setFoodInfoId(J)V

    .line 82
    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 83
    return-void
.end method

.method public bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 34
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ExtraFoodInfoRequestTask;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;)V

    return-void
.end method
