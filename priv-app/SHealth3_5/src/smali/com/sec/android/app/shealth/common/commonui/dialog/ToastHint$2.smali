.class Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;
.super Ljava/lang/Object;
.source "ToastHint.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

.field final synthetic val$popupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;->val$popupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->ctx:Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v2

    .line 86
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v3, :cond_2

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 89
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;->val$popupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->released(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
