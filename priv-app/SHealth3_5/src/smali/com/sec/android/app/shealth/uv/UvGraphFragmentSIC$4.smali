.class Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;
.super Ljava/lang/Object;
.source "UvGraphFragmentSIC.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)V
    .locals 0

    .prologue
    .line 693
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/4 v4, 0x1

    .line 727
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 728
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 730
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 731
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$400(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_2

    .line 732
    const/high16 v1, 0x426c0000    # 59.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 736
    :goto_0
    const/high16 v1, 0x427c0000    # 63.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 737
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 738
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 739
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 741
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 744
    .end local v0    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    if-eqz p1, :cond_1

    .line 745
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/uv/UvInformationAreaView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/sec/android/app/shealth/uv/UvInformationAreaView;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->update(Ljava/util/ArrayList;)V

    .line 747
    :cond_1
    return-void

    .line 734
    .restart local v0    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_2
    const/high16 v1, 0x41f80000    # 31.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0
.end method

.method public OnReleaseTimeOut()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 715
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 716
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const-string v1, "Handler info"

    const-string v2, "Release Timeout"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 718
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 719
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 720
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 722
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 723
    return-void
.end method

.method public OnVisible()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 697
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 699
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 700
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$400(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_0

    .line 701
    const/high16 v1, 0x426c0000    # 59.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 705
    :goto_0
    const/high16 v1, 0x427c0000    # 63.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 706
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 707
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 708
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 710
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 711
    return-void

    .line 703
    :cond_0
    const/high16 v1, 0x41f80000    # 31.0f

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC$4;->this$0:Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->density:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/uv/UvGraphFragmentSIC;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0
.end method
