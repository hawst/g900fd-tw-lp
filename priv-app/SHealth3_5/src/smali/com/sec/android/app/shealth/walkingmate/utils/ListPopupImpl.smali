.class public Lcom/sec/android/app/shealth/walkingmate/utils/ListPopupImpl;
.super Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;
.source "ListPopupImpl.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;IZ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "focusable"    # Z

    .prologue
    .line 43
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;IZ)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I[Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "additionalItems"    # [Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I[Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "focusable"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Z)V

    .line 50
    return-void
.end method


# virtual methods
.method protected getAdditionalTextViewId()I
    .locals 1

    .prologue
    .line 110
    const v0, 0x7f080800

    return v0
.end method

.method protected getAdditionalUnitViewId()I
    .locals 1

    .prologue
    .line 115
    const v0, 0x7f0807ff

    return v0
.end method

.method protected getBackgroundEmptySpaceHeight()I
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopupImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a08e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    return v0
.end method

.method protected getBackgroundResource()I
    .locals 1

    .prologue
    .line 90
    const v0, 0x7f02086f

    return v0
.end method

.method protected getBelowTextViewId()I
    .locals 1

    .prologue
    .line 105
    const v0, 0x7f0807fe

    return v0
.end method

.method protected getBottomItemDrawableId()I
    .locals 1

    .prologue
    .line 85
    const v0, 0x7f020146

    return v0
.end method

.method protected getClickableViewId()I
    .locals 1

    .prologue
    .line 136
    const v0, 0x7f0807fc

    return v0
.end method

.method protected getDefaultItemColorId()I
    .locals 1

    .prologue
    .line 70
    const v0, 0x7f07007a

    return v0
.end method

.method protected getDividerDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopupImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020756

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected getDividerHeight()I
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopupImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getGreyItemColorId()I
    .locals 1

    .prologue
    .line 126
    const v0, 0x7f07009e

    return v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 95
    const v0, 0x7f03029a

    return v0
.end method

.method protected getListPopupItemHeight()I
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopupImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method protected getMiddleItemDrawableId()I
    .locals 1

    .prologue
    .line 80
    const v0, 0x7f020146

    return v0
.end method

.method protected getRecetTextViewId()I
    .locals 1

    .prologue
    .line 131
    const v0, 0x7f0807fb

    return v0
.end method

.method protected getSelectedItemColorId()I
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f070079

    return v0
.end method

.method protected getTextViewId()I
    .locals 1

    .prologue
    .line 100
    const v0, 0x7f08033a

    return v0
.end method

.method protected getTopItemDrawableId()I
    .locals 1

    .prologue
    .line 75
    const v0, 0x7f020146

    return v0
.end method
