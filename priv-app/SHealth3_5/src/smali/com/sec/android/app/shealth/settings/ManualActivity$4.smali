.class Lcom/sec/android/app/shealth/settings/ManualActivity$4;
.super Ljava/lang/Object;
.source "ManualActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ManualActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 7
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const-wide/16 v5, 0xbb8

    const-wide/16 v3, 0x12c

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$100(Lcom/sec/android/app/shealth/settings/ManualActivity;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 278
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforDisableAirplane:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$500(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforDisableAirplane:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$500(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->setAirplaneModeEnabled(Z)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v2, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforDisableAirplane:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$502(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto :goto_0

    :cond_1
    move v0, v1

    .line 279
    goto :goto_1

    .line 286
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableMobile:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$600(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableMobile:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$600(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->setMobileEnabled(Z)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v3, 0x2710

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableMobile:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$602(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto :goto_0

    .line 294
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableRoaming:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$700(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableRoaming:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$700(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->setDataRoamingEnabled(Z)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableRoaming:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$702(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto/16 :goto_0

    .line 302
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 306
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 310
    :pswitch_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$200(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 311
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setManualMobilePopupWasShown(Landroid/content/Context;Z)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$202(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto/16 :goto_0

    .line 316
    :pswitch_6
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$300(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 317
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setManualRoamingPopupWasShown(Landroid/content/Context;Z)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/ManualActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->access$302(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z

    goto/16 :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
