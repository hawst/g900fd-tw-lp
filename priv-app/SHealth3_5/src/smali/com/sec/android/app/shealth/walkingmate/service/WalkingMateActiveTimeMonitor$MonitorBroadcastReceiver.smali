.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WalkingMateActiveTimeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MonitorBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$1;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v7, 0x3e8

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$100(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "callback is disabled"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    const-string v2, "data"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "data":Ljava/lang/String;
    const-string v2, "healthy_step"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "healthy step"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    const-string/jumbo v3, "status"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-boolean v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callHealthyStepChanged(Z)V

    .line 101
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\t\t healthy step status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-boolean v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 102
    :cond_2
    const-string v2, "inactive_time"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 103
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "inactive time"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const-string/jumbo v2, "status"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 105
    .local v1, "status":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    const-string/jumbo v3, "value"

    invoke-virtual {p2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    # setter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$302(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;J)J

    .line 106
    if-eqz v1, :cond_3

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    .line 110
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveDurationSetting:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J

    move-result-wide v4

    mul-long/2addr v4, v7

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    .line 111
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\t\t Recover since duration < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveDurationSetting:J
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J

    move-result-wide v3

    mul-long/2addr v3, v7

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iput-boolean v6, v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    .line 113
    const/4 v1, 0x0

    .line 117
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 118
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInactiveTimeTracking()Z

    move-result v2

    if-nez v2, :cond_5

    .line 119
    const/4 v1, 0x0

    .line 120
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isInactiveTimeTracking is false - Inactive notification : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-boolean v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callInactiveTime(ZJZ)V

    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\t\t inactive time status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mInactiveTimeStart = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-boolean v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    :cond_5
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isInactiveTimeTracking is true - Inactive notification : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 125
    :cond_6
    const/4 v1, 0x0

    .line 126
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPedometerNotificationOn is false - Inactive notification : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 132
    .end local v1    # "status":Z
    :cond_7
    const-string v2, "current_status"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    const-string/jumbo v2, "scontext_update"

    const-string/jumbo v3, "receive current status"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    const-string v3, "healthy_step"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    const-string v3, "inactive_time"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    const-string v3, "inactive_time_duration"

    invoke-virtual {p2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    # setter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$302(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;J)J

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-boolean v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mInactiveTimeStart:Z

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mDuration:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->access$300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callInactiveTime(ZJZ)V

    .line 138
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$MonitorBroadcastReceiver;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    iget-boolean v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->mHealthyStepStart:Z

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->callHealthyStepChanged(Z)V

    goto/16 :goto_0
.end method
