.class public abstract Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;
.super Landroid/os/AsyncTask;
.source "MyRankingsUpdater.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

.field private byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, [Landroid/content/Intent;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->doInBackground([Landroid/content/Intent;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/lang/Void;
    .locals 13
    .param p1, "params"    # [Landroid/content/Intent;

    .prologue
    .line 33
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->DEFAULT:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 34
    .local v9, "requestResult":Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    const/4 v10, 0x0

    aget-object v4, p1, v10

    .line 35
    .local v4, "intent":Landroid/content/Intent;
    const-string v10, "access_token"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "accessToken":Ljava/lang/String;
    const-string v10, "client_id"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "clientId":Ljava/lang/String;
    const-string v10, "device_id"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, "deviceId":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->isCancelled()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 41
    const/4 v10, 0x0

    .line 104
    :goto_0
    return-object v10

    .line 43
    :cond_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 44
    .local v7, "jsonTypeWorld":Lorg/json/JSONObject;
    const-string/jumbo v10, "type"

    const-string/jumbo v11, "world"

    invoke-virtual {v7, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 46
    const/4 v10, 0x0

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v3, v0, v2, v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getWalkingMateRanking(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 54
    .local v8, "jsonWorldResult":Lorg/json/JSONObject;
    if-nez v8, :cond_1

    .line 55
    sget-object v10, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    const-string v11, "jsonWorldResult is null"

    invoke-static {v10, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const/4 v10, 0x0

    goto :goto_0

    .line 58
    :cond_1
    sget-object v10, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "jsonWorldResult : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    new-instance v10, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    invoke-direct {v10, v8}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;-><init>(Lorg/json/JSONObject;)V

    iput-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->isCancelled()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 62
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    .line 63
    const/4 v10, 0x0

    goto :goto_0

    .line 66
    :cond_2
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 67
    .local v6, "jsonTypeAge":Lorg/json/JSONObject;
    const-string/jumbo v10, "type"

    const-string v11, "age"

    invoke-virtual {v6, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 69
    const/4 v10, 0x0

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v3, v0, v2, v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getWalkingMateRanking(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 77
    .local v5, "jsonAgeResult":Lorg/json/JSONObject;
    if-nez v5, :cond_3

    .line 78
    sget-object v10, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    const-string v11, "jsonAgeResult is null"

    invoke-static {v10, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v10, 0x0

    goto :goto_0

    .line 82
    :cond_3
    sget-object v10, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "jsonAgeResult : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    new-instance v10, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    invoke-direct {v10, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;-><init>(Lorg/json/JSONObject;)V

    iput-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    .line 85
    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    if-eqz v10, :cond_4

    .line 86
    sget-object v10, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    const-string v11, "Your rankings data is successfully received from server"

    invoke-static {v10, v11}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    :try_end_0
    .catch Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 103
    .end local v5    # "jsonAgeResult":Lorg/json/JSONObject;
    .end local v6    # "jsonTypeAge":Lorg/json/JSONObject;
    .end local v7    # "jsonTypeWorld":Lorg/json/JSONObject;
    .end local v8    # "jsonWorldResult":Lorg/json/JSONObject;
    :cond_4
    :goto_1
    const/4 v10, 0x1

    new-array v10, v10, [Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    const/4 v11, 0x0

    aput-object v9, v10, v11

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->publishProgress([Ljava/lang/Object;)V

    .line 104
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 90
    :catch_0
    move-exception v1

    .line 92
    .local v1, "catchMeIfYouCan":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;->getStatusCode()I

    move-result v10

    const/16 v11, 0x191

    if-ne v10, v11, :cond_5

    .line 93
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->INVALID_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 97
    :goto_2
    sget-object v10, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    invoke-static {v10, v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 95
    :cond_5
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SERVER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    goto :goto_2

    .line 98
    .end local v1    # "catchMeIfYouCan":Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
    :catch_1
    move-exception v1

    .line 99
    .local v1, "catchMeIfYouCan":Ljava/lang/Exception;
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->OTHER_ERROR:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 100
    sget-object v10, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->TAG:Ljava/lang/String;

    invoke-static {v10, v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V
    .locals 3
    .param p1, "values"    # [Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .prologue
    .line 109
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->basicResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->byAgeResponse:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->onUpdate(Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V

    .line 111
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, [Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/MyRankingsUpdater;->onProgressUpdate([Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V

    return-void
.end method

.method public abstract onUpdate(Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V
.end method
