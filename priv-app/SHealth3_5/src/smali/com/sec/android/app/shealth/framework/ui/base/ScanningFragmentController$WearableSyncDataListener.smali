.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WearableSyncDataListener"
.end annotation


# instance fields
.field private mSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

.field private mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->mSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    return-void
.end method

.method public static wat9ygX()Ljava/lang/String;
    .locals 60

    const/16 v0, 0x39

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x4221

    aput v58, v1, v57

    const/16 v57, -0x7e81

    aput v57, v1, v56

    const/16 v56, -0x5f

    aput v56, v1, v55

    const/16 v55, 0x6d0b

    aput v55, v1, v54

    const/16 v54, -0x45fe

    aput v54, v1, v53

    const/16 v53, -0x38

    aput v53, v1, v52

    const/16 v52, -0x52

    aput v52, v1, v51

    const/16 v51, -0x17

    aput v51, v1, v50

    const/16 v50, -0xd

    aput v50, v1, v49

    const/16 v49, -0x39

    aput v49, v1, v48

    const/16 v48, -0x2e

    aput v48, v1, v47

    const/16 v47, -0x7

    aput v47, v1, v46

    const/16 v46, -0x2ba7

    aput v46, v1, v45

    const/16 v45, -0x5c

    aput v45, v1, v44

    const/16 v44, 0x570f

    aput v44, v1, v43

    const/16 v43, 0x1803

    aput v43, v1, v42

    const/16 v42, -0x1d87

    aput v42, v1, v41

    const/16 v41, -0x6a

    aput v41, v1, v40

    const/16 v40, -0x38

    aput v40, v1, v39

    const/16 v39, -0x76

    aput v39, v1, v38

    const/16 v38, 0x6d18

    aput v38, v1, v37

    const/16 v37, 0x1d44

    aput v37, v1, v36

    const/16 v36, 0x4b35

    aput v36, v1, v35

    const/16 v35, 0x482f

    aput v35, v1, v34

    const/16 v34, -0x1ad3

    aput v34, v1, v33

    const/16 v33, -0x6b

    aput v33, v1, v32

    const/16 v32, -0x42

    aput v32, v1, v31

    const/16 v31, -0x10f9

    aput v31, v1, v30

    const/16 v30, -0x65

    aput v30, v1, v29

    const/16 v29, -0x23

    aput v29, v1, v28

    const/16 v28, -0x45a6

    aput v28, v1, v27

    const/16 v27, -0x2b

    aput v27, v1, v26

    const/16 v26, -0x53ca

    aput v26, v1, v25

    const/16 v25, -0x22

    aput v25, v1, v24

    const/16 v24, -0x49

    aput v24, v1, v23

    const/16 v23, -0x21

    aput v23, v1, v22

    const/16 v22, -0xb

    aput v22, v1, v21

    const/16 v21, 0x432

    aput v21, v1, v20

    const/16 v20, -0xd89

    aput v20, v1, v19

    const/16 v19, -0x65

    aput v19, v1, v18

    const/16 v18, 0x187e

    aput v18, v1, v17

    const/16 v17, 0x579

    aput v17, v1, v16

    const/16 v16, 0x7c71

    aput v16, v1, v15

    const/16 v15, 0x521d

    aput v15, v1, v14

    const/16 v14, -0x15ea

    aput v14, v1, v13

    const/16 v13, -0x77

    aput v13, v1, v12

    const/16 v12, -0x1e

    aput v12, v1, v11

    const/16 v11, -0x3eeb

    aput v11, v1, v10

    const/16 v10, -0x6e

    aput v10, v1, v9

    const/16 v9, -0xfce

    aput v9, v1, v8

    const/16 v8, -0x64

    aput v8, v1, v7

    const/16 v7, -0x30f4

    aput v7, v1, v6

    const/16 v6, -0x52

    aput v6, v1, v5

    const/16 v5, 0x3e61

    aput v5, v1, v4

    const/16 v4, -0x3aa1

    aput v4, v1, v3

    const/16 v3, -0x60

    aput v3, v1, v2

    const/16 v2, -0x58f3

    aput v2, v1, v0

    const/16 v0, 0x39

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x4201

    aput v59, v0, v58

    const/16 v58, -0x7ebe

    aput v58, v0, v57

    const/16 v57, -0x7f

    aput v57, v0, v56

    const/16 v56, 0x6d79

    aput v56, v0, v55

    const/16 v55, -0x4593

    aput v55, v0, v54

    const/16 v54, -0x46

    aput v54, v0, v53

    const/16 v53, -0x24

    aput v53, v0, v52

    const/16 v52, -0x74

    aput v52, v0, v51

    const/16 v51, -0x2d

    aput v51, v0, v50

    const/16 v50, -0x19

    aput v50, v0, v49

    const/16 v49, -0x11

    aput v49, v0, v48

    const/16 v48, -0x27

    aput v48, v0, v47

    const/16 v47, -0x2bc4

    aput v47, v0, v46

    const/16 v46, -0x2c

    aput v46, v0, v45

    const/16 v45, 0x5776

    aput v45, v0, v44

    const/16 v44, 0x1857

    aput v44, v0, v43

    const/16 v43, -0x1de8

    aput v43, v0, v42

    const/16 v42, -0x1e

    aput v42, v0, v41

    const/16 v41, -0x57

    aput v41, v0, v40

    const/16 v40, -0x12

    aput v40, v0, v39

    const/16 v39, 0x6d38

    aput v39, v0, v38

    const/16 v38, 0x1d6d

    aput v38, v0, v37

    const/16 v37, 0x4b1d

    aput v37, v0, v36

    const/16 v36, 0x484b

    aput v36, v0, v35

    const/16 v35, -0x1ab8

    aput v35, v0, v34

    const/16 v34, -0x1b

    aput v34, v0, v33

    const/16 v33, -0x32

    aput v33, v0, v32

    const/16 v32, -0x1098

    aput v32, v0, v31

    const/16 v31, -0x11

    aput v31, v0, v30

    const/16 v30, -0x72

    aput v30, v0, v29

    const/16 v29, -0x45cc

    aput v29, v0, v28

    const/16 v28, -0x46

    aput v28, v0, v27

    const/16 v27, -0x53ea

    aput v27, v0, v26

    const/16 v26, -0x54

    aput v26, v0, v25

    const/16 v25, -0x2e

    aput v25, v0, v24

    const/16 v24, -0x4f

    aput v24, v0, v23

    const/16 v23, -0x70

    aput v23, v0, v22

    const/16 v22, 0x446

    aput v22, v0, v21

    const/16 v21, -0xdfc

    aput v21, v0, v20

    const/16 v20, -0xe

    aput v20, v0, v19

    const/16 v19, 0x1832

    aput v19, v0, v18

    const/16 v18, 0x518

    aput v18, v0, v17

    const/16 v17, 0x7c05

    aput v17, v0, v16

    const/16 v16, 0x527c

    aput v16, v0, v15

    const/16 v15, -0x15ae

    aput v15, v0, v14

    const/16 v14, -0x16

    aput v14, v0, v13

    const/16 v13, -0x74

    aput v13, v0, v12

    const/16 v12, -0x3e94

    aput v12, v0, v11

    const/16 v11, -0x3f

    aput v11, v0, v10

    const/16 v10, -0xfa9

    aput v10, v0, v9

    const/16 v9, -0x10

    aput v9, v0, v8

    const/16 v8, -0x3092

    aput v8, v0, v7

    const/16 v7, -0x31

    aput v7, v0, v6

    const/16 v6, 0x3e13

    aput v6, v0, v5

    const/16 v5, -0x3ac2

    aput v5, v0, v4

    const/16 v4, -0x3b

    aput v4, v0, v3

    const/16 v3, -0x58a6

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 11

    const-wide v9, -0x3632709979265a71L    # -3.3751734051517107E47

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 11

    const-wide v9, -0x7560c7a001c20c2fL

    const/4 v8, 0x0

    const/16 v7, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v3, v0, v7

    aget-wide v0, v2, v8

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-eqz v5, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v3

    xor-long/2addr v0, v9

    aput-wide v0, v2, v8

    return-void
.end method

.method public onStarted(II)V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    const-wide v7, 0x309b071628990383L    # 1.4938599438324332E-74

    const/16 v6, 0x20

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x2

    aput-wide v0, v2, v10

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v9}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v6

    ushr-long v3, v0, v6

    aget-wide v0, v2, v9

    cmp-long v5, v0, v11

    if-eqz v5, :cond_1

    xor-long/2addr v0, v7

    :cond_1
    ushr-long/2addr v0, v6

    shl-long/2addr v0, v6

    xor-long/2addr v0, v3

    xor-long/2addr v0, v7

    aput-wide v0, v2, v9

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-lt v10, v0, :cond_2

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    int-to-long v0, p2

    shl-long v3, v0, v6

    aget-wide v0, v2, v9

    cmp-long v5, v0, v11

    if-eqz v5, :cond_3

    xor-long/2addr v0, v7

    :cond_3
    shl-long/2addr v0, v6

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v3

    xor-long/2addr v0, v7

    aput-wide v0, v2, v9

    return-void
.end method

.method public onStopped(II)V
    .locals 31

    const/4 v1, 0x2

    new-array v4, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x2

    aput-wide v2, v4, v1

    const/4 v1, 0x0

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x17b8a754e519adfdL

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x17b8a754e519adfdL

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/4 v1, 0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-lt v1, v2, :cond_2

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const/4 v3, 0x0

    move/from16 v0, p2

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long v5, v1, v5

    aget-wide v1, v4, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_3

    const-wide v7, 0x17b8a754e519adfdL

    xor-long/2addr v1, v7

    :cond_3
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x17b8a754e519adfdL

    xor-long/2addr v1, v5

    aput-wide v1, v4, v3

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x4771

    aput v29, v2, v28

    const/16 v28, 0x4d22

    aput v28, v2, v27

    const/16 v27, -0x1fdf

    aput v27, v2, v26

    const/16 v26, -0x74

    aput v26, v2, v25

    const/16 v25, -0x4b

    aput v25, v2, v24

    const/16 v24, -0x1486

    aput v24, v2, v23

    const/16 v23, -0x61

    aput v23, v2, v22

    const/16 v22, -0x51

    aput v22, v2, v21

    const/16 v21, 0x5826

    aput v21, v2, v20

    const/16 v20, 0x251b

    aput v20, v2, v19

    const/16 v19, 0x6d51

    aput v19, v2, v18

    const/16 v18, -0x74fd

    aput v18, v2, v17

    const/16 v17, -0x12

    aput v17, v2, v16

    const/16 v16, -0x4f

    aput v16, v2, v15

    const/16 v15, -0x60

    aput v15, v2, v14

    const/16 v14, -0x6d

    aput v14, v2, v13

    const/16 v13, -0x6e

    aput v13, v2, v12

    const/16 v12, 0x241d

    aput v12, v2, v11

    const/16 v11, 0x1a43

    aput v11, v2, v10

    const/16 v10, 0x4d74

    aput v10, v2, v9

    const/16 v9, -0x23dc

    aput v9, v2, v8

    const/16 v8, -0x4e

    aput v8, v2, v7

    const/16 v7, -0x63

    aput v7, v2, v6

    const/16 v6, -0x42

    aput v6, v2, v5

    const/16 v5, -0xb

    aput v5, v2, v3

    const/16 v3, -0x3cd7

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x4703

    aput v30, v1, v29

    const/16 v29, 0x4d47

    aput v29, v1, v28

    const/16 v28, -0x1fb3

    aput v28, v1, v27

    const/16 v27, -0x20

    aput v27, v1, v26

    const/16 v26, -0x26

    aput v26, v1, v25

    const/16 v25, -0x14f8

    aput v25, v1, v24

    const/16 v24, -0x15

    aput v24, v1, v23

    const/16 v23, -0x3f

    aput v23, v1, v22

    const/16 v22, 0x5849

    aput v22, v1, v21

    const/16 v21, 0x2558

    aput v21, v1, v20

    const/16 v20, 0x6d25

    aput v20, v1, v19

    const/16 v19, -0x7493

    aput v19, v1, v18

    const/16 v18, -0x75

    aput v18, v1, v17

    const/16 v17, -0x24

    aput v17, v1, v16

    const/16 v16, -0x39

    aput v16, v1, v15

    const/16 v15, -0xe

    aput v15, v1, v14

    const/16 v14, -0x20

    aput v14, v1, v13

    const/16 v13, 0x245b

    aput v13, v1, v12

    const/16 v12, 0x1a24

    aput v12, v1, v11

    const/16 v11, 0x4d1a

    aput v11, v1, v10

    const/16 v10, -0x23b3

    aput v10, v1, v9

    const/16 v9, -0x24

    aput v9, v1, v8

    const/16 v8, -0xd

    aput v8, v1, v7

    const/16 v7, -0x21

    aput v7, v1, v6

    const/16 v6, -0x6a

    aput v6, v1, v5

    const/16 v5, -0x3c86

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->wat9ygX()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x22

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x11

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-eqz v6, :cond_9

    const-wide v6, 0x17b8a754e519adfdL

    xor-long/2addr v1, v6

    :cond_9
    const/16 v6, 0x20

    shr-long/2addr v1, v6

    long-to-int v1, v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->mSyncEventListener:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x3314

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x3325

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_c
    const/4 v1, 0x0

    aget-wide v1, v4, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_d

    const-wide v4, 0x17b8a754e519adfdL

    xor-long/2addr v1, v4

    :cond_d
    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->mSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-interface {v3, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;->onSyncStopped(ILcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    :goto_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WearableSyncDataListener;->mSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->leaveDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_7
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_7

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_7

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6
.end method
