.class Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;
.super Ljava/lang/Object;
.source "ExpandableListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->showSelectAllCheckbox()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 329
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$500(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->toggle()V

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mSelectAllCheckbox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$500(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 331
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$600(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->checkAllItems(Z)V

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnHeaderClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$700(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnHeaderClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$700(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$OnHeaderClickListener;->onHeaderClick()V

    .line 335
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$6;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->getFoodListExpandableAdapter()Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$600(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->notifyDataSetChanged()V

    .line 336
    return-void
.end method
