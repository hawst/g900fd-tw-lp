.class public final enum Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;
.super Ljava/lang/Enum;
.source "MealCategory.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

.field public static final enum EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

.field public static final enum MY_FOOD:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

.field public static final enum PLANNED:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

.field public static final enum PLANNED_CANCELED:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

.field public static final enum PLANNED_EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;


# instance fields
.field private final mConstantRepresentation:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    const-string v1, "MY_FOOD"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;-><init>(Ljava/lang/String;ILjava/lang/Integer;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->MY_FOOD:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    .line 23
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    const-string v1, "PLANNED"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;-><init>(Ljava/lang/String;ILjava/lang/Integer;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    const-string v1, "EATEN"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;-><init>(Ljava/lang/String;ILjava/lang/Integer;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    .line 25
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    const-string v1, "PLANNED_EATEN"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;-><init>(Ljava/lang/String;ILjava/lang/Integer;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED_EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    const-string v1, "PLANNED_CANCELED"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;-><init>(Ljava/lang/String;ILjava/lang/Integer;)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED_CANCELED:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    .line 21
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->MY_FOOD:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED_EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED_CANCELED:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->$VALUES:[Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Integer;)V
    .locals 0
    .param p3, "constant"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->mConstantRepresentation:Ljava/lang/Integer;

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->$VALUES:[Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    return-object v0
.end method


# virtual methods
.method public getCategoryByConstant(Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;
    .locals 5
    .param p1, "constant"    # Ljava/lang/Integer;

    .prologue
    .line 41
    invoke-static {}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->values()[Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 42
    .local v3, "type":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;
    invoke-interface {v3}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;->getConstantRepresentation()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 47
    .end local v3    # "type":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;
    :goto_1
    return-object v3

    .line 41
    .restart local v3    # "type":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    .end local v3    # "type":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public bridge synthetic getCategoryByConstant(Ljava/lang/Object;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->getCategoryByConstant(Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/ITypeContract;

    move-result-object v0

    return-object v0
.end method

.method public getConstantRepresentation()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->mConstantRepresentation:Ljava/lang/Integer;

    return-object v0
.end method

.method public bridge synthetic getConstantRepresentation()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->getConstantRepresentation()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
