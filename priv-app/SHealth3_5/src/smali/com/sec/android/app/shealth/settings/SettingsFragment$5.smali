.class Lcom/sec/android/app/shealth/settings/SettingsFragment$5;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/SettingsFragment;->checkForUpdates()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/SettingsFragment;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(ZZ)V
    .locals 5
    .param p1, "result"    # Z
    .param p2, "isForceUpdate"    # Z

    .prologue
    .line 262
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    # getter for: Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->access$100(Lcom/sec/android/app/shealth/settings/SettingsFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    # getter for: Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->access$100(Lcom/sec/android/app/shealth/settings/SettingsFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    .line 268
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/SettingsFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->access$102(Lcom/sec/android/app/shealth/settings/SettingsFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 271
    :cond_2
    if-eqz p1, :cond_4

    .line 273
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274
    .local v0, "appUpdate":Landroid/content/Intent;
    const-string v2, "FORCE_UPDATE"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 286
    .end local v0    # "appUpdate":Landroid/content/Intent;
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    # getter for: Lcom/sec/android/app/shealth/settings/SettingsFragment;->updateCheck:Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->access$200(Lcom/sec/android/app/shealth/settings/SettingsFragment;)Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->unregisterAppUpdateListener()V

    goto :goto_0

    .line 279
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 280
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0908b4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    .line 283
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SettingsFragment$5;->this$0:Lcom/sec/android/app/shealth/settings/SettingsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "check for updates dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method
