.class public Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;
.super Ljava/lang/Object;
.source "SleepDataUtil.java"


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final HOURS_IN_DAY:B = 0x18t

.field public static final LAST_DAY_IN_WEEK:B = 0x6t

.field public static final LAST_HOUR_IN_DAY:B = 0x17t

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:B = 0x3bt

.field public static final LAST_SECOND_IN_MINUTE:B = 0x3bt

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HALFDAY:I = 0x2932e00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MINUTE:I = 0xea60

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MILLIS_IN_WEEK:I = 0x240c8400

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MONTH_IN_YEAR:I = 0xc

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static final TAG:Ljava/lang/String;

.field private static final WARNING_STRING:Ljava/lang/String; = "is_warning_checked"

.field private static final tempCal:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->TAG:Ljava/lang/String;

    .line 46
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAccurateSampleTime(J)J
    .locals 5
    .param p0, "time"    # J

    .prologue
    const/16 v4, 0xc

    const/4 v3, 0x0

    .line 209
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 210
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 211
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 212
    .local v1, "min":I
    div-int/lit8 v2, v1, 0x14

    mul-int/lit8 v1, v2, 0x14

    .line 213
    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 214
    const/16 v2, 0xd

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 215
    const/16 v2, 0xe

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 216
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public static getEndOfDay(J)J
    .locals 5
    .param p0, "time"    # J

    .prologue
    .line 248
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 249
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 250
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 251
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 252
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 253
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 254
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/16 v3, 0x3e7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 255
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEndOfMonth(J)J
    .locals 3
    .param p0, "time"    # J

    .prologue
    const/16 v2, 0xc

    .line 280
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 281
    .local v0, "tempCal":Ljava/util/Calendar;
    monitor-enter v0

    .line 283
    :try_start_0
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 284
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ge v1, v2, :cond_0

    .line 285
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 287
    :cond_0
    const/4 v1, 0x5

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 288
    const/16 v1, 0xb

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 289
    const/16 v1, 0xc

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 290
    const/16 v1, 0xd

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 291
    const/16 v1, 0xe

    const/16 v2, 0x3e7

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 292
    const/16 v1, 0xb

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 293
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    monitor-exit v0

    return-wide v1

    .line 294
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static getFoundDate(J)Ljava/util/Date;
    .locals 5
    .param p0, "time"    # J

    .prologue
    .line 188
    sget-object v2, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    monitor-enter v2

    .line 189
    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 190
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v3, 0xc

    if-ge v1, v3, :cond_0

    .line 191
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/4 v3, 0x5

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 192
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v3, 0xb

    const/16 v4, 0xd

    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 194
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 195
    .local v0, "date":Ljava/util/Date;
    monitor-exit v2

    .line 196
    return-object v0

    .line 195
    .end local v0    # "date":Ljava/util/Date;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getLastUpdateTime(Landroid/content/Context;)J
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "update_time"

    aput-object v1, v2, v0

    .line 83
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 86
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "update_time DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 87
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 89
    const-string/jumbo v0, "update_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v7

    .line 96
    .local v7, "updateTime":J
    if-eqz v6, :cond_0

    .line 97
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "updateTime":J
    :cond_0
    :goto_0
    return-wide v7

    .line 93
    :cond_1
    const-wide/16 v7, 0x0

    .line 96
    if-eqz v6, :cond_0

    .line 97
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 97
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getMonth(JLandroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "Time"    # J
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 299
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f0e0000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 300
    .local v2, "months":[Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 302
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 303
    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0xc

    if-ge v3, v4, :cond_0

    .line 304
    const/4 v3, 0x5

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 306
    :cond_0
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 308
    .local v1, "i":I
    aget-object v3, v2, v1

    return-object v3
.end method

.method public static getNextDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    const/4 v7, 0x0

    .line 136
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "bed_time"

    aput-object v1, v2, v0

    .line 137
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bed_time>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getEndOfDay(J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 138
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 139
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v10, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 141
    .local v10, "today":Ljava/util/Date;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "bed_time ASC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 143
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 144
    const-string v0, "bed_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 145
    .local v8, "time":J
    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getFoundDate(J)Ljava/util/Date;

    move-result-object v7

    .line 146
    .local v7, "foundDate":Ljava/util/Date;
    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    invoke-static {p0, v10}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->hasDateGotData(Landroid/content/Context;Ljava/util/Date;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    if-eqz v6, :cond_0

    .line 162
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "foundDate":Ljava/util/Date;
    .end local v8    # "time":J
    .end local v10    # "today":Ljava/util/Date;
    :cond_0
    :goto_0
    return-object v10

    .line 160
    .restart local v7    # "foundDate":Ljava/util/Date;
    .restart local v8    # "time":J
    .restart local v10    # "today":Ljava/util/Date;
    :cond_1
    if-eqz v6, :cond_2

    .line 162
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v10, v7

    goto :goto_0

    .line 154
    .end local v7    # "foundDate":Ljava/util/Date;
    .end local v8    # "time":J
    :cond_3
    :try_start_1
    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    cmp-long v0, v0, v4

    if-lez v0, :cond_4

    .line 160
    if-eqz v6, :cond_0

    .line 162
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 160
    :cond_4
    if-eqz v6, :cond_5

    .line 162
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v10, v7

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 162
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method public static getPrevDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    const/4 v7, 0x0

    .line 104
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "bed_time"

    aput-object v1, v2, v0

    .line 105
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bed_time<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 106
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 107
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v10, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 110
    .local v10, "today":Ljava/util/Date;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "bed_time DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 111
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 113
    const-string v0, "bed_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 114
    .local v8, "time":J
    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getFoundDate(J)Ljava/util/Date;

    move-result-object v7

    .line 116
    .local v7, "foundDate":Ljava/util/Date;
    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    invoke-static {p0, v10}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->hasDateGotData(Landroid/content/Context;Ljava/util/Date;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    if-eqz v6, :cond_0

    .line 130
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "foundDate":Ljava/util/Date;
    .end local v8    # "time":J
    .end local v10    # "today":Ljava/util/Date;
    :cond_0
    :goto_0
    return-object v10

    .line 129
    .restart local v7    # "foundDate":Ljava/util/Date;
    .restart local v8    # "time":J
    .restart local v10    # "today":Ljava/util/Date;
    :cond_1
    if-eqz v6, :cond_2

    .line 130
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v10, v7

    goto :goto_0

    .line 123
    .end local v7    # "foundDate":Ljava/util/Date;
    .end local v8    # "time":J
    :cond_3
    :try_start_1
    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_4

    invoke-static {p0, v10}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->hasDateGotData(Landroid/content/Context;Ljava/util/Date;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 129
    if-eqz v6, :cond_0

    .line 130
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 129
    :cond_4
    if-eqz v6, :cond_5

    .line 130
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v10, v7

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 130
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method public static getSleepDataSelect(Landroid/content/Context;J)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    .line 200
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSleepDataSelect = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v3, "bed_time>=? AND bed_time<?"

    .line 202
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getEndOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 203
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v5, "bed_time DESC "

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 204
    .local v6, "cursor":Landroid/database/Cursor;
    return-object v6
.end method

.method public static getStartOfDay(J)J
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 233
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    monitor-enter v1

    .line 234
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 239
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 240
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 241
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 242
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getStartOfMinute(J)J
    .locals 3
    .param p0, "t1"    # J

    .prologue
    const/4 v2, 0x0

    .line 222
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 223
    .local v0, "temp":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 224
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 225
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 226
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method public static getStartOfMonth(J)J
    .locals 3
    .param p0, "time"    # J

    .prologue
    const/16 v2, 0xc

    .line 261
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 262
    .local v0, "tempCal":Ljava/util/Calendar;
    monitor-enter v0

    .line 264
    :try_start_0
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 265
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ge v1, v2, :cond_0

    .line 266
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 268
    :cond_0
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 269
    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 270
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 271
    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 272
    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 273
    const/16 v1, 0xb

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 274
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    monitor-exit v0

    return-wide v1

    .line 275
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static hasDateGotData(Landroid/content/Context;Ljava/util/Date;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 168
    const/4 v0, 0x0

    .line 170
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getSleepDataSelect(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    const/4 v1, 0x1

    .line 176
    if-eqz v0, :cond_0

    .line 178
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 183
    :cond_0
    :goto_0
    return v1

    .line 176
    :cond_1
    if-eqz v0, :cond_2

    .line 178
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 183
    :cond_2
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 174
    :catch_0
    move-exception v1

    .line 176
    if-eqz v0, :cond_2

    .line 178
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 179
    :catch_1
    move-exception v1

    goto :goto_1

    .line 176
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_3

    .line 178
    :try_start_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 180
    :cond_3
    :goto_2
    throw v1

    .line 179
    :catch_2
    move-exception v2

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v2

    goto :goto_2
.end method

.method public static loggingD(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 313
    invoke-static {p0, p1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    return-void
.end method

.method public static loggingI(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 317
    invoke-static {p0, p1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    return-void
.end method

.method public static sleepDeleteAll(Landroid/content/Context;JZ)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J
    .param p3, "daily"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 49
    if-eqz p0, :cond_0

    .line 50
    if-eqz p3, :cond_1

    .line 52
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " < "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "bed_time"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getEndOfDay(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "bed_time"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 55
    .local v0, "_id":I
    sget-object v3, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   sleepDelete = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v5

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .end local v0    # "_id":I
    .end local v2    # "where":Ljava/lang/String;
    :cond_0
    :goto_0
    return v7

    .line 58
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 59
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "is_warning_checked"

    invoke-interface {v1, v3, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 60
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 62
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 63
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static sleepItemCommitChange(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    .prologue
    .line 70
    sget-object v3, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sleepItemCommitChange  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 72
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v3, "comment"

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;->getComment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string/jumbo v3, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 74
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v2, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 76
    .local v1, "update_id":I
    sget-object v3, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sleepItemCommitChange = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return v1
.end method
