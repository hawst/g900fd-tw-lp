.class public final enum Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;
.super Ljava/lang/Enum;
.source "DefaultLogFilter.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/log/LogFilter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;",
        ">;",
        "Lcom/sec/android/app/shealth/logutils/log/LogFilter;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

.field public static final enum ALL:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

.field public static final enum GOAL_ACHIEVED:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

.field public static final enum GOAL_NOT_ACHIEVED:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;


# instance fields
.field private final mDisplayNameId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    new-instance v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    const-string v1, "ALL"

    sget v2, Lcom/sec/android/app/shealth/logutils/R$string;->all:I

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->ALL:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    const-string v1, "GOAL_ACHIEVED"

    sget v2, Lcom/sec/android/app/shealth/logutils/R$string;->goal_achieved_filter_name:I

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->GOAL_ACHIEVED:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    .line 27
    new-instance v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    const-string v1, "GOAL_NOT_ACHIEVED"

    sget v2, Lcom/sec/android/app/shealth/logutils/R$string;->goal_not_achieved_filter_name:I

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->GOAL_NOT_ACHIEVED:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    sget-object v1, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->ALL:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->GOAL_ACHIEVED:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->GOAL_NOT_ACHIEVED:Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->$VALUES:[Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "displayNameId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->mDisplayNameId:I

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->$VALUES:[Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayNameId()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->mDisplayNameId:I

    return v0
.end method

.method public getValues()[Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;->values()[Lcom/sec/android/app/shealth/logutils/log/DefaultLogFilter;

    move-result-object v0

    return-object v0
.end method
