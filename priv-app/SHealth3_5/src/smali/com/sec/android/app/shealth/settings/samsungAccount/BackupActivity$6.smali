.class Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;
.super Ljava/lang/Object;
.source "BackupActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 413
    const v0, 0x7f08008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09087c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v0, 0x7f08008b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$402(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v0, 0x7f08008d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgress:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgress:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v0, 0x7f08008c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 423
    return-void
.end method
