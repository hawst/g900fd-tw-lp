.class Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;
.super Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Landroid/app/Activity;Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;III)V
    .locals 8

    const/4 v0, 0x3

    new-array v2, v0, [J

    const/4 v0, 0x2

    const-wide/16 v3, 0x3

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p4

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_2

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v3, 0x0

    int-to-long v0, p5

    const/16 v4, 0x20

    shl-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_3

    const-wide v6, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v6

    :cond_3
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    const/4 v0, 0x2

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-lt v0, v1, :cond_4

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const/4 v3, 0x1

    int-to-long v0, p6

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_5

    const-wide v6, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v6

    :cond_5
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x23

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x13

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_7
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-eqz v3, :cond_9

    const-wide v3, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v3

    :cond_9
    const/16 v3, 0x20

    shl-long/2addr v0, v3

    const/16 v3, 0x20

    shr-long/2addr v0, v3

    long-to-int v3, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_c

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x55c9

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x55fa

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_b
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_c
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_d

    const-wide v4, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v4

    :cond_d
    const/16 v4, 0x20

    shr-long/2addr v0, v4

    long-to-int v4, v0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_10

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x4a

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x7c

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_e

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v4, v0

    if-lt v2, v4, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_e
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_f
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_10
    const/4 v0, 0x1

    aget-wide v0, v2, v0

    const-wide/16 v5, 0x0

    cmp-long v2, v0, v5

    if-eqz v2, :cond_11

    const-wide v5, -0x4ca1147e315299abL    # -3.0064893411712884E-61

    xor-long/2addr v0, v5

    :cond_11
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v5, v0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;III)V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarAppTitle()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    iput-boolean v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrawMenuOpen:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawShown:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$102(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->makeAllActionBarButtonsVisible()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCustomViewVisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getButtonContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_double_tap_to_open:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->setDrawerItemContentDescription(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setDrawMenuStateOpen(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->clearScreensViewsList()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mContent:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->refreshFocusables()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$300(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$402(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerFlag:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$502(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawShown:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$102(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;->onDrawerClosed()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerIsOpened:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$702(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/high16 v1, -0x67000000

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setScrimColor(I)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarDefaultTitle()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrawMenuOpen:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawShown:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$102(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->makeAllActionBarButtonsInvisible()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCustomViewVisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getButtonContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsTitleShown:Z
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$802(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_double_tap_to_close:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->setDrawerItemContentDescription(Ljava/lang/String;)V
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerFlag:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$502(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->ly_sub_action_title_holder:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setNextFocusRightId(I)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerIsOpened:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$702(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;->onDrawerOpened()V

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mValueSlide:F
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$902(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;F)F

    float-to-double v0, p2

    const-wide v2, 0x3fc999999999999aL    # 0.2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawerPartialShown:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1002(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    :cond_0
    float-to-double v0, p2

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawerPartialShown:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1002(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarAnimator()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;->animateActionBar(F)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerFlag:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerFlag:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$502(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->clearScreensViewsList()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->refreshFocusables()V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;->onSlide(F)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
