.class public Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;
.super Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;
.source "ThermoHygrometerSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;
.implements Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;


# static fields
.field private static COMPENSATING_ACCURACY:I

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mHumidity:F

.field private mHumidityAccuracy:I

.field private mHumidityDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

.field private mHumiditySet:Z

.field private mHumidityUpdated:Z

.field private mTempAccuracy:I

.field private mTempDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

.field private mTemperature:F

.field private mTemperatureSet:Z

.field private mTemperatureUpdated:Z

.field private mViewHelper:Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;

.field private m_bHumidityCompensating:Z

.field private m_bTemperatureCompensating:Z

.field private need_animation_humid:Z

.field private need_animation_temp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    .line 69
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->COMPENSATING_ACCURACY:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/high16 v3, -0x3b860000    # -1000.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;-><init>()V

    .line 52
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTempDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    .line 53
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    .line 55
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperature:F

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bTemperatureCompensating:Z

    .line 58
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_temp:Z

    .line 59
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_humid:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureSet:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureUpdated:Z

    .line 63
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidity:F

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bHumidityCompensating:Z

    .line 66
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumiditySet:Z

    .line 67
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityUpdated:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->startTemperatureSensor()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->startHumiditySensor()V

    return-void
.end method

.method private initializeUIComponents(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 128
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;-><init>(Landroid/view/View;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mViewHelper:Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;

    .line 132
    return-void
.end method

.method private setInterimViewHumidity()V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method private startHumiditySensor()V
    .locals 3

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startHumiditySensor"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;Z)V

    .line 234
    :cond_0
    return-void
.end method

.method private startTemperatureSensor()V
    .locals 3

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 238
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startTemperatureSensor"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTempDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/thermohygrometer/TempSensorListener;Z)V

    .line 241
    :cond_0
    return-void
.end method


# virtual methods
.method protected clearMainView()V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 75
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030257

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 76
    .local v1, "summaryFragment":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->initializeUIComponents(Landroid/view/View;)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 80
    return-object v1
.end method

.method public getSelectedDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 94
    const v0, 0x7f10002a

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 95
    return-void
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;)V
    .locals 5
    .param p1, "humidityData"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 355
    :goto_0
    return-void

    .line 330
    :cond_0
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->humidity:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidity:F

    .line 331
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->accuracy:I

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityAccuracy:I

    .line 332
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumiditySet:Z

    .line 334
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataReceived mHumidity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidity:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataReceived mHumidityAccuracy = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityAccuracy:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityAccuracy:I

    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->COMPENSATING_ACCURACY:I

    if-ge v0, v1, :cond_1

    .line 338
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bHumidityCompensating:Z

    .line 343
    :goto_1
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityAccuracy:I

    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->COMPENSATING_ACCURACY:I

    if-gt v0, v1, :cond_2

    .line 344
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_humid:Z

    .line 349
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$6;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 340
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bHumidityCompensating:Z

    goto :goto_1

    .line 346
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_humid:Z

    goto :goto_2
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;)V
    .locals 5
    .param p1, "tempData"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 313
    :goto_0
    return-void

    .line 286
    :cond_0
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->temperature:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperature:F

    .line 287
    iget v0, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;->accuracy:I

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTempAccuracy:I

    .line 288
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureSet:Z

    .line 290
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataReceived mTemperature = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperature:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDataReceived mTempAccuracy = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTempAccuracy:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTempAccuracy:I

    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->COMPENSATING_ACCURACY:I

    if-ge v0, v1, :cond_1

    .line 294
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bTemperatureCompensating:Z

    .line 299
    :goto_1
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTempAccuracy:I

    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->COMPENSATING_ACCURACY:I

    if-gt v0, v1, :cond_2

    .line 300
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_temp:Z

    .line 305
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$5;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 296
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bTemperatureCompensating:Z

    goto :goto_1

    .line 302
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_temp:Z

    goto :goto_2
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 266
    invoke-super {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->onDestroyView()V

    .line 267
    return-void
.end method

.method public onHumidityTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 370
    return-void
.end method

.method public onHumidityTimeout()V
    .locals 0

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->stopHumiditySensor()V

    .line 360
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->setInterimViewHumidity()V

    .line 361
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 99
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 100
    .local v1, "itemId":I
    const v3, 0x7f080c8a

    if-ne v1, v3, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.thermohygrometer"

    const-string v5, "TH02"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    :goto_0
    const v3, 0x7f0802db

    if-ne v1, v3, :cond_2

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->showResetDialog()V

    .line 123
    :goto_1
    return v2

    .line 104
    :cond_1
    const v3, 0x7f080c98

    if-ne v1, v3, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.thermohygrometer"

    const-string v5, "TH03"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_2
    const v3, 0x7f080cae

    if-ne v1, v3, :cond_3

    .line 118
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 119
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 123
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 271
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->stopSensors()V

    .line 273
    invoke-super {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->onPause()V

    .line 274
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->updateTemperatureInfoUnit()V

    .line 87
    invoke-super {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->onResume()V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->startSensors()V

    .line 90
    return-void
.end method

.method public onTempTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 323
    return-void
.end method

.method public onTempTimeout()V
    .locals 0

    .prologue
    .line 318
    return-void
.end method

.method public startSensors()V
    .locals 4

    .prologue
    .line 182
    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "startSensors"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 189
    .local v1, "threadTemperature":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 191
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$2;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 197
    .local v0, "threadHumidity":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 198
    return-void
.end method

.method public stopHumiditySensor()V
    .locals 2

    .prologue
    .line 220
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopHumiditySensor"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->stopMeasuring()V

    .line 222
    return-void
.end method

.method public stopSensors()V
    .locals 4

    .prologue
    .line 201
    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "stopSensors"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$3;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 208
    .local v1, "threadTemperature":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 210
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment$4;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 216
    .local v0, "threadHumidity":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 217
    return-void
.end method

.method public stopTemperatureSensor()V
    .locals 2

    .prologue
    .line 225
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopTemperatureSensor"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTempDeviceConnector:Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/TemperatureDeviceConnector;->stopMeasuring()V

    .line 227
    return-void
.end method

.method protected updateMainView()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 136
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->getLastData()Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;

    move-result-object v10

    .line 138
    .local v10, "thermoData":Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->getTemperature()F

    move-result v1

    .line 139
    .local v1, "temperature":F
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->getHumidity()F

    move-result v2

    .line 140
    .local v2, "humidity":F
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->getCreateTime()J

    move-result-wide v3

    .line 142
    .local v3, "sampleTime":J
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureSet:Z

    if-eq v0, v12, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumiditySet:Z

    if-ne v0, v12, :cond_6

    .line 143
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperature:F

    .line 144
    iget v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidity:F

    .line 146
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bTemperatureCompensating:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bHumidityCompensating:Z

    if-eqz v0, :cond_2

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mViewHelper:Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bTemperatureCompensating:Z

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bHumidityCompensating:Z

    iget-boolean v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_temp:Z

    iget-boolean v8, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_humid:Z

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->setViewValues(FFJZZZZ)V

    .line 150
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureSet:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bTemperatureCompensating:Z

    if-nez v0, :cond_3

    .line 151
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureUpdated:Z

    .line 152
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureSet:Z

    .line 153
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bTemperatureCompensating:Z

    .line 155
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumiditySet:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bHumidityCompensating:Z

    if-nez v0, :cond_4

    .line 156
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityUpdated:Z

    .line 157
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumiditySet:Z

    .line 158
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->m_bHumidityCompensating:Z

    .line 160
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureUpdated:Z

    if-ne v0, v12, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityUpdated:Z

    if-ne v0, v12, :cond_5

    .line 162
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 163
    new-instance v9, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;

    invoke-direct {v9}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;-><init>()V

    .line 164
    .local v9, "data":Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;
    invoke-virtual {v9, v1}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->setTemperature(F)V

    .line 165
    invoke-virtual {v9, v2}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->setHumidity(F)V

    .line 166
    invoke-virtual {v9, v3, v4}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->setCreateTime(J)V

    .line 167
    invoke-static {v9}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->setData(Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;)V

    .line 169
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateMainView() temperature : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "humidity : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mViewHelper:Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;

    iget-boolean v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_temp:Z

    iget-boolean v8, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_humid:Z

    move v5, v11

    move v6, v11

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->setViewValues(FFJZZZZ)V

    .line 172
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mTemperatureUpdated:Z

    .line 173
    iput-boolean v11, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mHumidityUpdated:Z

    .line 179
    .end local v9    # "data":Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;
    :cond_5
    :goto_0
    return-void

    .line 177
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->mViewHelper:Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;

    iget-boolean v7, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_temp:Z

    iget-boolean v8, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryFragment;->need_animation_humid:Z

    move v5, v11

    move v6, v11

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/shealth/thermohygrometer/summary/ThermoHygrometerSummaryViewHelper;->setViewValues(FFJZZZZ)V

    goto :goto_0
.end method
