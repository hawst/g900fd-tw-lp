.class final enum Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
.super Ljava/lang/Enum;
.source "WeightBaseGoalFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "GoalFragment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

.field public static final enum ADD_NEW_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

.field public static final enum ADD_NEW_GOAL_CALORIES:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

.field public static final enum WITH_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    const-string v1, "WITH_GOAL"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->WITH_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    .line 52
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    const-string v1, "ADD_NEW_GOAL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->ADD_NEW_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    const-string v1, "ADD_NEW_GOAL_CALORIES"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->ADD_NEW_GOAL_CALORIES:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->WITH_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->ADD_NEW_GOAL:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->ADD_NEW_GOAL_CALORIES:Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->$VALUES:[Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->$VALUES:[Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    return-object v0
.end method
