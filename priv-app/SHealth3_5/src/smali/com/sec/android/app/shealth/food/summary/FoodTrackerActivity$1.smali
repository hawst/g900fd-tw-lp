.class final Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;
.super Landroid/util/SparseArray;
.source "FoodTrackerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/SparseArray",
        "<",
        "Lcom/sec/android/app/shealth/food/constants/MealType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    .line 104
    const/16 v0, 0x1388

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->NOT_DEFINED:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;->put(ILjava/lang/Object;)V

    .line 105
    const/16 v0, 0x1389

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->BREAKFAST:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;->put(ILjava/lang/Object;)V

    .line 106
    const/16 v0, 0x138a

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->LUNCH:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;->put(ILjava/lang/Object;)V

    .line 107
    const/16 v0, 0x138b

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->DINNER:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;->put(ILjava/lang/Object;)V

    .line 108
    const/16 v0, 0x138c

    sget-object v1, Lcom/sec/android/app/shealth/food/constants/MealType;->OTHER:Lcom/sec/android/app/shealth/food/constants/MealType;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;->put(ILjava/lang/Object;)V

    .line 109
    return-void
.end method
