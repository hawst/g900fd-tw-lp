.class Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;
.super Ljava/lang/Object;
.source "EditMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)V
    .locals 0

    .prologue
    .line 664
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 5
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 667
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v2, :cond_0

    .line 668
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->mMealActivityTopFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->access$300(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->getMealType()I

    move-result v3

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->cleanMealTypeForTheDay(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->access$400(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;I)V

    .line 669
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->replaceData()J
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->access$500(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;)J

    move-result-wide v0

    .line 670
    .local v0, "mealId":J
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->onEditAccepted(J)V
    invoke-static {v2, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;J)V

    .line 671
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    const v4, 0x7f090835

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    .line 672
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity$6;->this$0:Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->finish()V

    .line 674
    .end local v0    # "mealId":J
    :cond_0
    return-void
.end method
