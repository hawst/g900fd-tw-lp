.class Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
.super Ljava/lang/Object;
.source "WalkingSContextListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "bufferDataStructure"
.end annotation


# instance fields
.field public accumlatedDuration:J

.field public accumlatedHealthyStep:J

.field public activeTimeDBid:J

.field public lastDurationUpdateTime:J

.field public lastStepsUpdateTime:J

.field public stepsDBid:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const-wide/16 v0, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    .line 87
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedDuration:J

    .line 88
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastDurationUpdateTime:J

    .line 89
    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastStepsUpdateTime:J

    .line 90
    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->stepsDBid:J

    .line 91
    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->activeTimeDBid:J

    .line 92
    return-void
.end method
