.class Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;
.super Ljava/lang/Object;
.source "InitSetTermsOfUse.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isCheck"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 113
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 114
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    const v3, 0x7f080a12

    if-ne v0, v3, :cond_1

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # setter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$402(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)Z

    .line 119
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$400(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$500(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    # invokes: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->enableNextBtn(Z)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$600(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$400(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$500(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_2
    # invokes: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->setNextBtnDescription(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$700(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)V

    .line 123
    const/4 v0, 0x0

    invoke-static {p1, v0, v2, p2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 124
    return-void

    .line 116
    :cond_1
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    const v3, 0x7f080a15

    if-ne v0, v3, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # setter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$502(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)Z

    goto :goto_0

    :cond_2
    move v0, v2

    .line 119
    goto :goto_1

    :cond_3
    move v1, v2

    .line 122
    goto :goto_2
.end method
