.class public Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;
.super Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    const/4 v2, 0x1

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->sub_tab_background_selector:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setBackgroundResource(I)V

    const-string/jumbo v0, "sec-roboto-light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$color;->sub_tab_text_color_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setTextColor(I)V

    const/high16 v0, 0x41600000    # 14.0f

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setTextSize(IF)V

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setGravity(I)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setFocusable(Z)V

    const v0, 0x3f333333    # 0.7f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setAlpha(F)V

    return-void
.end method


# virtual methods
.method protected dispatchSetSelected(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$color;->graph_yellow:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setTextColor(I)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$color;->sub_tab_text_color_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setTextColor(I)V

    const v0, 0x3f333333    # 0.7f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setAlpha(F)V

    goto :goto_0
.end method
