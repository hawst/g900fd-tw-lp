.class public Lcom/sec/android/app/shealth/SHealthApplication;
.super Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
.source "SHealthApplication.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;
.implements Lcom/sec/android/app/shealth/food/app/IFoodMaskedImageCollectorHolder;
.implements Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;
.implements Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask$IPostRepositoryInitializationTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;,
        Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;,
        Lcom/sec/android/app/shealth/SHealthApplication$LocaleReceiver;
    }
.end annotation


# static fields
.field private static final APP_CATEGORY_TYPE_FITNESS:Ljava/lang/String; = "Fitness"

.field private static final SETTING_WEARABLE_ID:Ljava/lang/String; = "content://settings/system/connected_wearable_id"

.field public static final SHEALTH_FOR_GEAR_FLAG:Ljava/lang/String; = "true"

.field public static final SHEALTH_FOR_GEAR_FLAG_FILE:Ljava/lang/String; = "ShealthForGearVersionFlag.xml"

.field public static final SHEALTH_FOR_GEAR_PRIVATE_STORAGE_PATH:Ljava/lang/String; = "/data/data/com.sec.android.app.shealth"

.field private static final TAG:Ljava/lang/String;

.field public static gIsHealthServiceUpgradedNeeded:Z

.field public static isFoodInfoCleanUpRequired:Z

.field public static isPedometerAvailabilityMismatch:Z

.field public static sdkErrorCode:I

.field public static unSupported:Z


# instance fields
.field private isInitialization:Z

.field private mCoachRcvr:Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

.field private mCoachTask:Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

.field private mContext:Landroid/content/Context;

.field private mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

.field private mFoodConfiguration:Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

.field private mFoodUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field private mKeyManagerReceiver:Lcom/sec/android/service/health/keyManager/KeyManager$IKeyManagerCallbackReceiver;

.field private mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

.field private mNoStartServiceForChina:Z

.field private mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field mNormalSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private mReceiver:Lcom/sec/android/app/shealth/SHealthApplication$LocaleReceiver;

.field mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

.field private statusObserver:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

.field private uiThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    const-class v0, Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    .line 129
    sput-boolean v1, Lcom/sec/android/app/shealth/SHealthApplication;->gIsHealthServiceUpgradedNeeded:Z

    .line 130
    sput-boolean v1, Lcom/sec/android/app/shealth/SHealthApplication;->unSupported:Z

    .line 131
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/SHealthApplication;->isFoodInfoCleanUpRequired:Z

    .line 132
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/shealth/SHealthApplication;->sdkErrorCode:I

    .line 141
    sput-boolean v1, Lcom/sec/android/app/shealth/SHealthApplication;->isPedometerAvailabilityMismatch:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;-><init>()V

    .line 118
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNoStartServiceForChina:Z

    .line 134
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->isInitialization:Z

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mReceiver:Lcom/sec/android/app/shealth/SHealthApplication$LocaleReceiver;

    .line 137
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->uiThreadHandler:Landroid/os/Handler;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachTask:Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

    .line 140
    iput-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachRcvr:Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    .line 1303
    new-instance v0, Lcom/sec/android/app/shealth/SHealthApplication$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SHealthApplication$2;-><init>(Lcom/sec/android/app/shealth/SHealthApplication;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 1357
    new-instance v0, Lcom/sec/android/app/shealth/SHealthApplication$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SHealthApplication$3;-><init>(Lcom/sec/android/app/shealth/SHealthApplication;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 1428
    new-instance v0, Lcom/sec/android/app/shealth/SHealthApplication$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/SHealthApplication$4;-><init>(Lcom/sec/android/app/shealth/SHealthApplication;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mKeyManagerReceiver:Lcom/sec/android/service/health/keyManager/KeyManager$IKeyManagerCallbackReceiver;

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/SHealthApplication;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/SHealthApplication;->updatePreinstalledPluginInfo(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/SHealthApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->setAntAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/SHealthApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->setHRAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/SHealthApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->setUVAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/SHealthApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->setSpO2Available()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/SHealthApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->setTHGAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/SHealthApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->clearAppRegistryData()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/SHealthApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->loadAppRegistryData()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/SHealthApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->loadDownloadedPLuginsData()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/SHealthApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->widgetEnableByCingaSupportLanguage()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/SHealthApplication;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/SHealthApplication;->updateAppDataAfterGearConnecton(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->uiThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/SHealthApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->setPedometerAvailable()Z

    move-result v0

    return v0
.end method

.method private clearAppRegistryData()V
    .locals 8

    .prologue
    .line 822
    const-string v3, "delete from app_registry"

    .line 823
    .local v3, "sql":Ljava/lang/String;
    const/4 v6, 0x0

    .line 825
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 831
    if-eqz v6, :cond_0

    .line 833
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 826
    :catch_0
    move-exception v7

    .line 827
    .local v7, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v1, "ClearAppRegistryData failed"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 831
    if-eqz v6, :cond_0

    .line 833
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 828
    .end local v7    # "e":Landroid/database/SQLException;
    :catch_1
    move-exception v7

    .line 829
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v0, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ClearAppRegistryData failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 831
    if-eqz v6, :cond_0

    .line 833
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 831
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 833
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private initializeSamsungId()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1411
    sget-object v3, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "setsamsungAccountID()"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "health_account"

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1413
    .local v0, "accountSharedPref":Landroid/content/SharedPreferences;
    const-string v3, "SamAccount"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 1415
    sget-object v3, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "setsamsungAccountID(): No samsung account id found in preference"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1416
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 1417
    .local v2, "mgr":Landroid/accounts/AccountManager;
    if-eqz v2, :cond_0

    .line 1418
    sget-object v3, Lcom/sec/android/service/health/cp/serversync/syncadapter/authenticator/SamsungAccSignInHandlerService;->ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 1419
    .local v1, "accounts":[Landroid/accounts/Account;
    if-eqz v1, :cond_0

    array-length v3, v1

    if-lez v3, :cond_0

    .line 1421
    sget-object v3, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setsamsungAccountID(): No samsung account id found in preference. Samsung account is present in device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v6

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "SamAccount"

    aget-object v5, v1, v6

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1426
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "mgr":Landroid/accounts/AccountManager;
    :cond_0
    return-void
.end method

.method private isFavoriteStatusToBeChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "isEnabledCurrent"    # Ljava/lang/String;
    .param p2, "isEnabled"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 1190
    const-string v0, "-----SIC-----"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PLugin name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , Old status :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": new status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    const-string v0, "false"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193
    const/4 v0, 0x1

    .line 1195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTGHAvailable()Z
    .locals 9

    .prologue
    .line 659
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    if-nez v4, :cond_0

    .line 660
    iput-object p0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    .line 662
    :cond_0
    const/4 v1, 0x1

    .line 663
    .local v1, "isHumidityAvaiable":Z
    const/4 v3, 0x1

    .line 664
    .local v3, "isTemperatureAvaiable":Z
    const/4 v2, 0x0

    .line 667
    .local v2, "isTGHAvaiable":Z
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v4, :cond_3

    .line 668
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v5, 0x4

    const/16 v6, 0x272b

    const/16 v7, 0x12

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    .line 669
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v5, 0x4

    const/16 v6, 0x272c

    const/16 v7, 0x13

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->isAvailable(IIILjava/lang/String;)Z

    move-result v3

    .line 676
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    .line 678
    const/4 v2, 0x1

    .line 689
    :cond_2
    :goto_1
    return v2

    .line 671
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v4, :cond_1

    .line 673
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v5, 0x4

    const/16 v6, 0x272b

    const/16 v7, 0x12

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    .line 674
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v5, 0x4

    const/16 v6, 0x272c

    const/16 v7, 0x13

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v3

    goto :goto_0

    .line 680
    :catch_0
    move-exception v0

    .line 681
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 682
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 683
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 684
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 685
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 686
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 687
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method private loadAppRegistryData()V
    .locals 4

    .prologue
    .line 808
    const-string v0, "-----SIC-----"

    const-string v1, "------------------Inside Load app registry"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/SHealthApplication;->updatePreinstalledPluginInfo(Landroid/content/Context;)V

    .line 810
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->loadDownloadedPLuginsData()V

    .line 811
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->setFavDefaultOrder()V

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->statusObserver:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    if-nez v0, :cond_0

    const/16 v0, 0x272e

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->wasGearConnected(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 814
    const-string v0, "-----SIC-----"

    const-string v1, "Setting wearable observer"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    new-instance v0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;-><init>(Lcom/sec/android/app/shealth/SHealthApplication;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->statusObserver:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    .line 816
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://settings/system/connected_wearable_id"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication;->statusObserver:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 818
    :cond_0
    return-void
.end method

.method private loadDownloadedPLuginsData()V
    .locals 2

    .prologue
    .line 1200
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.shealth.request.package.information"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1203
    .local v0, "packageInfoIntent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1204
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SHealthApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 1205
    return-void
.end method

.method private setAntAvailable()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 585
    const/4 v1, 0x0

    .line 586
    .local v1, "isAntAvaiable":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v3, :cond_1

    .line 587
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v4, 0x5

    const/16 v5, 0x2711

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    .line 590
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAntAvailablilty(Landroid/content/Context;Z)V

    .line 591
    const/4 v2, 0x1

    .line 601
    :goto_1
    return v2

    .line 588
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v3, :cond_0

    .line 589
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v4, 0x5

    const/16 v5, 0x2711

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    goto :goto_0

    .line 592
    :catch_0
    move-exception v0

    .line 593
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 594
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 596
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 597
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 598
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 599
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method private setFavDefaultOrder()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 840
    const/4 v0, 0x0

    .line 841
    .local v0, "counter":I
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->isInitialised()Z

    move-result v4

    if-nez v4, :cond_3

    .line 844
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v6, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 845
    .local v2, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    if-eqz v2, :cond_2

    .line 846
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 847
    .local v3, "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget v4, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    if-ne v4, v6, :cond_0

    .line 848
    iget-object v4, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    const-string v5, "com.sec.shealth.action.COACH"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 850
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    .line 851
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 854
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_2
    invoke-static {v6}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setIsInitialised(Z)V

    .line 855
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 856
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->logPrefFile()V

    .line 858
    .end local v2    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    :cond_3
    return-void
.end method

.method private setHRAvailable()Z
    .locals 7

    .prologue
    .line 607
    const/4 v1, 0x0

    .line 608
    .local v1, "isHRAvaiable":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v2, :cond_0

    .line 609
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v3, 0x4

    const/16 v4, 0x2718

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    .line 610
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setHRAvailablilty(Landroid/content/Context;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    .line 611
    const/4 v2, 0x1

    .line 621
    :goto_0
    return v2

    .line 612
    :catch_0
    move-exception v0

    .line 613
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 621
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 614
    :catch_1
    move-exception v0

    .line 615
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 616
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 617
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 618
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 619
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method private setPedometerAvailable()Z
    .locals 7

    .prologue
    .line 563
    const/4 v1, 0x0

    .line 564
    .local v1, "isPedoAvailable":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    if-eqz v2, :cond_1

    .line 565
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v3, 0x4

    const/16 v4, 0x2719

    const/4 v5, 0x5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    .line 568
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setPedoAvailablilty(Landroid/content/Context;Z)V

    .line 569
    const/4 v2, 0x1

    .line 579
    :goto_1
    return v2

    .line 566
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v2, :cond_0

    .line 567
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNormalDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v3, 0x4

    const/16 v4, 0x2719

    const/4 v5, 0x5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    goto :goto_0

    .line 570
    :catch_0
    move-exception v0

    .line 571
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 579
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_2
    const/4 v2, 0x0

    goto :goto_1

    .line 572
    :catch_1
    move-exception v0

    .line 573
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 574
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 575
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_2

    .line 576
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 577
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2
.end method

.method private setSpO2Available()Z
    .locals 3

    .prologue
    .line 637
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 638
    iput-object p0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    .line 640
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.spo2"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 641
    .local v0, "isSpO2Supported":Z
    if-nez v0, :cond_1

    .line 642
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setSpO2Availablilty(Landroid/content/Context;Z)V

    .line 647
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 644
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setSpO2Availablilty(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private setTHGAvailable()Z
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 652
    iput-object p0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    .line 654
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->isTGHAvailable()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setTGHAvailablilty(Landroid/content/Context;Z)V

    .line 655
    const/4 v0, 0x1

    return v0
.end method

.method private setUVAvailable()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 627
    const/4 v1, 0x0

    .line 629
    .local v1, "mUvSensor":Landroid/hardware/Sensor;
    const-string/jumbo v2, "sensor"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/SHealthApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 630
    .local v0, "mSensorManager":Landroid/hardware/SensorManager;
    const v2, 0x1001d

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    .line 631
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UV Sensor availability, mUVsensor = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v4, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setUVAvailablilty(Landroid/content/Context;Z)V

    .line 633
    return v3

    .line 632
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private startKeyManager()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 218
    const/4 v0, 0x0

    .line 220
    .local v0, "isDone":Z
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContext(Landroid/content/Context;)V

    .line 221
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v2, "call init()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v0, 0x0

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->init()V

    .line 246
    :goto_0
    return v0

    .line 227
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v2, "KeyManager.getInstance().initialization(getApplicationContext()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const/4 v0, 0x1

    .line 229
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mKeyManagerReceiver:Lcom/sec/android/service/health/keyManager/KeyManager$IKeyManagerCallbackReceiver;

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setKeyManagerCallbackReceiver(Lcom/sec/android/service/health/keyManager/KeyManager$IKeyManagerCallbackReceiver;)V

    .line 230
    invoke-static {p0}, Lcom/sec/android/app/shealth/KeyManagerReceiver;->setApplication(Lcom/sec/android/app/shealth/SHealthApplication;)V

    .line 231
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->checkOOBE()Z

    .line 232
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 233
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->initialization(Landroid/content/Context;I)V

    .line 234
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v2, "KeyManager.getInstance().initialization(getApplicationContext() : DBKEY_NOT_USED_DEFAULT_PASSWORD"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->preInit()V

    goto :goto_0

    .line 235
    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v1

    if-nez v1, :cond_2

    .line 236
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v2, "KeyManager.getInstance().initialization(getApplicationContext() : DBKEY_NOT_INITIALIZED"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->generateDBkey()Ljava/lang/String;

    goto :goto_1

    .line 240
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v2, "KeyManager.getInstance().initialization(getApplicationContext() : ETC"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private startPedometerService(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1208
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNoStartServiceForChina:Z

    if-nez v1, :cond_0

    .line 1209
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startSHealthService"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1210
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1211
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1213
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private updateAppDataAfterGearConnecton(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 791
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 793
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v1, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    .line 794
    .local v1, "packagename":Ljava/lang/String;
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v2, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    .line 795
    .local v2, "pluginID":I
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v0, v3, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    .line 796
    .local v0, "isFavorite":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v3, v1, v5, v2}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->updateEnabledStatus(Landroid/content/Context;Ljava/lang/String;ZI)I

    .line 797
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->isInitialised()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    if-ne v0, v5, :cond_0

    .line 799
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    .line 800
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 804
    .end local v0    # "isFavorite":I
    .end local v1    # "packagename":Ljava/lang/String;
    .end local v2    # "pluginID":I
    :cond_0
    return-void
.end method

.method private declared-synchronized updatePreinstalledPluginInfo(Landroid/content/Context;)V
    .locals 39
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 863
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v32

    .line 864
    .local v32, "packageName":Ljava/lang/String;
    const-string v2, ":"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v32

    .line 866
    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->getVersionNameForPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 868
    .local v10, "packageVersion":Ljava/lang/String;
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 869
    .local v28, "displayplugInNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 870
    .local v27, "displayPlugInIcons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 871
    .local v26, "displayPlugInActions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 872
    .local v33, "plugInfavorate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 874
    .local v30, "isEnabled":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    .line 944
    .local v37, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e000f

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 945
    const v2, 0x7f0e0010

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 946
    const v2, 0x7f0e0011

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 947
    const v2, 0x7f0e0012

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 948
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_0
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v29

    if-ge v0, v2, :cond_0

    .line 949
    const-string/jumbo v2, "true"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 948
    add-int/lit8 v29, v29, 0x1

    goto :goto_0

    .line 951
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v2, v3, :cond_12

    .line 953
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 954
    .local v36, "pos":I
    if-ltz v36, :cond_1

    .line 956
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 957
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip HeartRate"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    .end local v36    # "pos":I
    :cond_1
    :goto_1
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Sleep;

    if-ne v2, v3, :cond_2

    .line 984
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09017e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 985
    .restart local v36    # "pos":I
    if-ltz v36, :cond_2

    .line 987
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 988
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip Sleep"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 992
    .end local v36    # "pos":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isMobilePedometerDisabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 994
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 995
    .restart local v36    # "pos":I
    if-ltz v36, :cond_3

    .line 997
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/SHealthApplication;->isPedometerAvailabilityMismatch:Z

    .line 998
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 999
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip Pedometer"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    .end local v36    # "pos":I
    :cond_3
    :goto_2
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->UV:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    if-ne v2, v3, :cond_16

    .line 1011
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1012
    .restart local v36    # "pos":I
    if-ltz v36, :cond_4

    .line 1014
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1015
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip uv"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1041
    .end local v36    # "pos":I
    :cond_4
    :goto_3
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->BloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodGlucose;

    if-ne v2, v3, :cond_5

    .line 1043
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1044
    .restart local v36    # "pos":I
    if-ltz v36, :cond_5

    .line 1046
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1047
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip blood_glucose"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    .end local v36    # "pos":I
    :cond_5
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->BloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    if-ne v2, v3, :cond_6

    .line 1053
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1054
    .restart local v36    # "pos":I
    if-ltz v36, :cond_6

    .line 1056
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1057
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip blood_pressure"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    .end local v36    # "pos":I
    :cond_6
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    if-ne v2, v3, :cond_7

    .line 1063
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1064
    .restart local v36    # "pos":I
    if-ltz v36, :cond_7

    .line 1066
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1067
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip weight"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    .end local v36    # "pos":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getSpO2Availablilty(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$SPO2;

    if-ne v2, v3, :cond_9

    .line 1072
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090240

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1073
    .restart local v36    # "pos":I
    if-ltz v36, :cond_9

    .line 1074
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1075
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip SPO2"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    .end local v36    # "pos":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTGHAvailablilty(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1079
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090242

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1080
    .restart local v36    # "pos":I
    if-ltz v36, :cond_a

    .line 1081
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1082
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip TGH"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    .end local v36    # "pos":I
    :cond_a
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Stress;

    if-eq v2, v3, :cond_b

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 1101
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090026

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1102
    .restart local v36    # "pos":I
    if-ltz v36, :cond_c

    .line 1104
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1105
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip stress"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    .end local v36    # "pos":I
    :cond_c
    const/16 v29, 0x0

    :goto_4
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v29

    if-ge v0, v2, :cond_1b

    .line 1119
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "select * from app_registry where package_name= \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "actions"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "= \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 1122
    .local v5, "selectionClause":Ljava/lang/String;
    const/16 v24, 0x0

    .local v24, "cursor":Landroid/database/Cursor;
    const/16 v25, 0x0

    .line 1125
    .local v25, "cursorUpdate":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/_private/BaseContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 1126
    const/16 v23, 0x0

    .line 1127
    .local v23, "curName":Ljava/lang/String;
    const-string v31, "false"

    .line 1128
    .local v31, "isEnabledCurrent":Ljava/lang/String;
    const/16 v34, -0x1

    .line 1129
    .local v34, "pluginID":I
    if-eqz v24, :cond_d

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_d

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1131
    const-string v2, "app_name"

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 1132
    const-string v2, "is_enabled"

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 1133
    const-string/jumbo v2, "plugin_id"

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v34

    .line 1136
    :cond_d
    if-eqz v23, :cond_18

    const/4 v2, -0x1

    move/from16 v0, v34

    if-eq v0, v2, :cond_18

    add-int/lit8 v2, v29, 0x1

    move/from16 v0, v34

    if-ne v2, v0, :cond_18

    .line 1139
    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_e

    .line 1143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "update app_registry set app_name = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" where "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "package_name"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "= \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "actions"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "= \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1145
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/_private/BaseContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 1147
    :cond_e
    move-object/from16 v0, v30

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/SHealthApplication;->isFavoriteStatusToBeChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1149
    const-string v2, "-----SIC-----"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating Enabled status for :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v35

    .line 1151
    .local v35, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->updateAppDataAfterGearConnecton(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1174
    .end local v35    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    :cond_f
    :goto_5
    if-eqz v24, :cond_10

    .line 1176
    :try_start_2
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 1177
    const/16 v24, 0x0

    .line 1179
    :cond_10
    if-eqz v25, :cond_11

    .line 1181
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 1182
    const/16 v25, 0x0

    .line 1111
    :cond_11
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_4

    .line 960
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v23    # "curName":Ljava/lang/String;
    .end local v24    # "cursor":Landroid/database/Cursor;
    .end local v25    # "cursorUpdate":Landroid/database/Cursor;
    .end local v31    # "isEnabledCurrent":Ljava/lang/String;
    .end local v34    # "pluginID":I
    :cond_12
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailablilty(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 962
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 963
    .restart local v36    # "pos":I
    if-ltz v36, :cond_1

    .line 965
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v38

    .line 966
    .local v38, "type":I
    const/16 v2, 0x2726

    move/from16 v0, v38

    if-eq v0, v2, :cond_13

    const/16 v2, 0x272e

    move/from16 v0, v38

    if-eq v0, v2, :cond_13

    const/16 v2, 0x2723

    move/from16 v0, v38

    if-ne v0, v2, :cond_14

    .line 968
    :cond_13
    const/4 v2, 0x1

    move/from16 v0, v38

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->setFirstGearConnectionAsSuccess(IZ)V

    .line 969
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip HeartRate, but gear connected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 863
    .end local v10    # "packageVersion":Ljava/lang/String;
    .end local v26    # "displayPlugInActions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v27    # "displayPlugInIcons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v28    # "displayplugInNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v29    # "i":I
    .end local v30    # "isEnabled":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v32    # "packageName":Ljava/lang/String;
    .end local v33    # "plugInfavorate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v36    # "pos":I
    .end local v37    # "res":Landroid/content/res/Resources;
    .end local v38    # "type":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 973
    .restart local v10    # "packageVersion":Ljava/lang/String;
    .restart local v26    # "displayPlugInActions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v27    # "displayPlugInIcons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v28    # "displayplugInNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v29    # "i":I
    .restart local v30    # "isEnabled":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v32    # "packageName":Ljava/lang/String;
    .restart local v33    # "plugInfavorate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v36    # "pos":I
    .restart local v37    # "res":Landroid/content/res/Resources;
    .restart local v38    # "type":I
    :cond_14
    const/16 v2, 0x2723

    :try_start_3
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->wasGearConnected(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x2726

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->wasGearConnected(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x272e

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->wasGearConnected(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 975
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 976
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip HeartRate"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1004
    .end local v36    # "pos":I
    .end local v38    # "type":I
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v35

    .line 1005
    .restart local v35    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->updateAppDataAfterGearConnecton(Ljava/util/List;)V

    .line 1006
    invoke-interface/range {v35 .. v35}, Ljava/util/List;->clear()V

    goto/16 :goto_2

    .line 1018
    .end local v35    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUVAvailablilty(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1020
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v36

    .line 1021
    .restart local v36    # "pos":I
    if-ltz v36, :cond_4

    .line 1023
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v38

    .line 1024
    .restart local v38    # "type":I
    const/16 v2, 0x272e

    move/from16 v0, v38

    if-ne v0, v2, :cond_17

    .line 1026
    const/4 v2, 0x1

    move/from16 v0, v38

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->setFirstGearConnectionAsSuccess(IZ)V

    .line 1027
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip uv, but gear connected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1031
    :cond_17
    const/16 v2, 0x272e

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->wasGearConnected(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1033
    const-string v2, "false"

    move-object/from16 v0, v30

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1034
    const-string v2, "-----SIC-----"

    const-string v3, "------------------skip uv"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 1162
    .end local v36    # "pos":I
    .end local v38    # "type":I
    .restart local v5    # "selectionClause":Ljava/lang/String;
    .restart local v23    # "curName":Ljava/lang/String;
    .restart local v24    # "cursor":Landroid/database/Cursor;
    .restart local v25    # "cursorUpdate":Landroid/database/Cursor;
    .restart local v31    # "isEnabledCurrent":Ljava/lang/String;
    .restart local v34    # "pluginID":I
    :cond_18
    :try_start_4
    new-instance v6, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v30

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v11, "Fitness"

    const/4 v12, 0x1

    const/4 v13, 0x1

    const/4 v14, 0x0

    const-wide/16 v15, 0x0

    const/16 v17, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    add-int/lit8 v19, v29, 0x1

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v26

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    const/16 v22, 0x0

    move-object/from16 v7, v32

    invoke-direct/range {v6 .. v22}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZIILjava/lang/String;Ljava/lang/String;I)V

    .line 1163
    .local v6, "item":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/_private/BaseContract;->APP_REG_REPLACE_URI:Landroid/net/Uri;

    invoke-static {v6}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->prepareContentValues(Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_5

    .line 1174
    .end local v6    # "item":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    .end local v23    # "curName":Ljava/lang/String;
    .end local v31    # "isEnabledCurrent":Ljava/lang/String;
    .end local v34    # "pluginID":I
    :catchall_1
    move-exception v2

    if-eqz v24, :cond_19

    .line 1176
    :try_start_5
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 1177
    const/16 v24, 0x0

    .line 1179
    :cond_19
    if-eqz v25, :cond_1a

    .line 1181
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 1182
    const/16 v25, 0x0

    :cond_1a
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1188
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v24    # "cursor":Landroid/database/Cursor;
    .end local v25    # "cursorUpdate":Landroid/database/Cursor;
    :cond_1b
    monitor-exit p0

    return-void
.end method

.method private widgetEnableByCingaSupportLanguage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 542
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.intent.action.MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 543
    .local v0, "widgetIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v1

    if-nez v1, :cond_1

    .line 544
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v2, "Cigna Widget Disable"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->enableCignaWidgetNeeded(Z)V

    .line 546
    const-string v1, "MODE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 557
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/SHealthApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 558
    return-void

    .line 548
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v2, "Cigna Widget Enable"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->enableCignaWidgetNeeded(Z)V

    .line 550
    const-string v1, "MODE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 551
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isCignaCoachInited()Z

    move-result v1

    if-nez v1, :cond_0

    .line 553
    :cond_2
    const-string v1, "INITIAL_STATE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 554
    const-string v1, "TEXT"

    const v2, 0x7f090247

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/SHealthApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public getConfiguration()Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mFoodConfiguration:Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

    return-object v0
.end method

.method public getCurrentMeasureType()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1230
    .local v0, "listMeasures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v1, 0x2718

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1231
    const/16 v1, 0x271f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1232
    const/16 v1, 0x2712

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1233
    return-object v0
.end method

.method public getMaskedImageCollector()Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
    .locals 2

    .prologue
    .line 1297
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    if-nez v0, :cond_0

    .line 1298
    new-instance v0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    .line 1300
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    return-object v0
.end method

.method public getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1

    .prologue
    .line 1224
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mFoodUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method public init()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 276
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "[PERF] init() - START"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/SHealthApplication;->isInitialization:Z

    .line 279
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    .line 280
    .local v1, "initializationNeeded":Z
    const/4 v3, 0x0

    .line 281
    .local v3, "migrationState":I
    if-nez v1, :cond_0

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v3

    .line 283
    if-eqz v3, :cond_0

    .line 284
    const/4 v1, 0x1

    .line 288
    :cond_0
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - Migration Condition Check - START"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "onCreate() : App Type is Full."

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    move-result-object v6

    .line 294
    .local v6, "state":Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;
    sget-object v7, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;->UPGRADE_STATE_APP_UPGRADE_DONE:Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    if-ne v6, v7, :cond_6

    .line 295
    const/4 v2, 0x0

    .line 297
    .local v2, "migrationFinished":Z
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v4

    .line 299
    .local v4, "migrationState2":I
    if-nez v4, :cond_1

    .line 300
    const/4 v2, 0x1

    .line 302
    :cond_1
    if-ne v2, v9, :cond_5

    .line 303
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "onCreate() : UPGRADE & MIGRATION DONE. Call doNormalInitialization() !!"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;)Z

    .line 305
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 308
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->checkProfileItems()V

    .line 334
    .end local v2    # "migrationFinished":Z
    .end local v4    # "migrationState2":I
    :goto_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/service/health/keyManager/KeyManager;->getKeyMansgerState()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 336
    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/service/health/keyManager/KeyManager;->startSplashScreen()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :cond_2
    :goto_1
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - KeyManager.getInstance().getKeyMansgerState / calling splash screen - END"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 386
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mNoStartServiceForChina:Z

    .line 389
    :cond_3
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - ExercisePro and ShealthDeviceFinder related - START"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    .line 392
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->initialize(Landroid/content/Context;)V

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CoreUtil;->initialize(Landroid/content/Context;)V

    .line 395
    new-instance v0, Landroid/content/IntentFilter;

    const-string v7, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v0, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 396
    .local v0, "filter":Landroid/content/IntentFilter;
    new-instance v7, Lcom/sec/android/app/shealth/SHealthApplication$LocaleReceiver;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/SHealthApplication$LocaleReceiver;-><init>(Lcom/sec/android/app/shealth/SHealthApplication;)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mReceiver:Lcom/sec/android/app/shealth/SHealthApplication$LocaleReceiver;

    .line 397
    iget-object v7, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mReceiver:Lcom/sec/android/app/shealth/SHealthApplication$LocaleReceiver;

    invoke-virtual {p0, v7, v0}, Lcom/sec/android/app/shealth/SHealthApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 400
    iget-object v7, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeviceTypeChecked(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 402
    const-string v7, "-----SIC-----"

    const-string v8, "inside device type not checked"

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    new-instance v7, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    iget-object v8, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-direct {v7, v8, v9}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 438
    :goto_2
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - ExercisePro and ShealthDeviceFinder related - END"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/SamsungAccountSharedPreferencesHelper;->initialize(Landroid/content/Context;)V

    .line 443
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - COACH - START"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->widgetEnableByCingaSupportLanguage()V

    .line 446
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 447
    new-instance v7, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;-><init>(Lcom/sec/android/app/shealth/SHealthApplication$1;)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachTask:Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

    .line 448
    iget-object v7, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachTask:Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

    new-array v8, v10, [Ljava/lang/Void;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 451
    :cond_4
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - COACH - END"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - Pedometer - START"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->updatePedoWidget()V

    .line 456
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/SHealthApplication;->startPedometerService(Landroid/content/Context;)V

    .line 458
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - Pedometer - END"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - Exercise pro - START"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->initialize(Landroid/content/Context;)V

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CoreUtil;->initialize(Landroid/content/Context;)V

    .line 465
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "\t[PERF] init() - Exercise pro - END"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lcom/sec/android/app/shealth/SHealthApplication$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/SHealthApplication$1;-><init>(Lcom/sec/android/app/shealth/SHealthApplication;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 483
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->initializeSamsungId()V

    .line 485
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "[PERF] init() - END"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    return-void

    .line 311
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .restart local v2    # "migrationFinished":Z
    .restart local v4    # "migrationState2":I
    :cond_5
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "onCreate() : UPGRADE & MIGRATION is NOT Finished yet !!"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 315
    .end local v2    # "migrationFinished":Z
    .end local v4    # "migrationState2":I
    :cond_6
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "onCreate() : Upgrade is not completed."

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 412
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    :cond_7
    const-string v7, "-----SIC-----"

    const-string v8, "inside device type checked"

    invoke-static {v7, v8}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    const-string v7, "Device Type Shared Preferences"

    const-string v8, "Device Type Shared Preferences - not connecting to service"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    move-result-object v5

    .line 418
    .local v5, "provider":Landroid/content/IContentProvider;
    if-nez v5, :cond_9

    .line 420
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "ContentProvider is null. Checking if DB is initialized"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    sget-boolean v7, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->mIsInitialized:Z

    if-nez v7, :cond_8

    .line 424
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "initializing Repository"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    new-instance v7, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;-><init>(Landroid/content/Context;)V

    new-array v8, v10, [Ljava/lang/Void;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/framework/repository/RepositoryInitializationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_2

    .line 429
    :cond_8
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "Repository is already initialized"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->loadAppRegistryData()V

    goto/16 :goto_2

    .line 434
    :cond_9
    sget-object v7, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v8, "ContentProvider is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->loadAppRegistryData()V

    goto/16 :goto_2

    .line 337
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v5    # "provider":Landroid/content/IContentProvider;
    :catch_0
    move-exception v7

    goto/16 :goto_1
.end method

.method public isFinishedInitialization()Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->isInitialization:Z

    return v0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 145
    sget-object v6, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v7, "[PERF] SHealthApplication.onCreate"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-super {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->onCreate()V

    .line 149
    const-string v0, ""

    .line 150
    .local v0, "currentProcName":Ljava/lang/String;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    .line 151
    .local v4, "pid":I
    const-string v6, "activity"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/SHealthApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    .line 152
    .local v3, "manager":Landroid/app/ActivityManager;
    invoke-virtual {v3}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 154
    .local v2, "listOfRunningProcess":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v2, :cond_1

    .line 156
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 158
    .local v5, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    if-eqz v5, :cond_0

    iget v6, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, v4, :cond_0

    .line 160
    iget-object v0, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 161
    const-string v6, "com.sec.android.app.shealth:keyservice"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 215
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :goto_0
    return-void

    .line 168
    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContext(Landroid/content/Context;)V

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->setContext(Landroid/content/Context;)V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    sput-object v6, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->mContext:Landroid/content/Context;

    .line 174
    iput-object p0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    .line 175
    invoke-static {p0}, Lcom/sec/android/app/shealth/KeyManagerReceiver;->setApplication(Lcom/sec/android/app/shealth/SHealthApplication;)V

    .line 176
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v6

    iput-object p0, v6, Lcom/sec/android/service/health/keyManager/KeyManager;->application:Ljava/lang/Object;

    .line 180
    new-instance v6, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/app/FoodConfiguration;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mFoodConfiguration:Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

    .line 181
    new-instance v6, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mFoodUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 186
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->createUpgradeStatus(Landroid/content/Context;)Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil$SHealthUpgradeState;

    .line 192
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v6

    sput-boolean v6, Lcom/sec/android/app/shealth/SHealthApplication;->gIsHealthServiceUpgradedNeeded:Z

    .line 193
    sget-boolean v6, Lcom/sec/android/app/shealth/SHealthApplication;->gIsHealthServiceUpgradedNeeded:Z

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 194
    sget-object v6, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v7, "HEALTH SERVICE NEEDS TO UPGRADE !!"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 198
    :cond_2
    sget-object v6, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v7, "[PERF] startKeyManager - START"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->startKeyManager()Z

    .line 200
    sget-object v6, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v7, "[PERF] startKeyManager - END"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-static {p0, p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->init(Landroid/app/Application;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .line 207
    iget-object v6, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 208
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->blockRestore()V

    .line 211
    :cond_3
    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    invoke-direct {v6}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachRcvr:Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    .line 212
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachRcvr:Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    invoke-virtual {v6, v7, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->registerBR(Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;Landroid/content/Context;)V

    .line 214
    sget-object v6, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v7, "[PERF] SHealthApplication.onCreate- END"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDataStopped(Ljava/lang/String;II)V
    .locals 0
    .param p1, "uSensorDeviceId"    # Ljava/lang/String;
    .param p2, "dataType"    # I
    .param p3, "error"    # I

    .prologue
    .line 1246
    return-void
.end method

.method public onDeviceJoined(Ljava/lang/String;I)V
    .locals 0
    .param p1, "uSensorDeviceId"    # Ljava/lang/String;
    .param p2, "error"    # I

    .prologue
    .line 1250
    return-void
.end method

.method public onDeviceLeft(Ljava/lang/String;I)V
    .locals 0
    .param p1, "uSensorDeviceId"    # Ljava/lang/String;
    .param p2, "error"    # I

    .prologue
    .line 1254
    return-void
.end method

.method public onReceived(Ljava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "uSensorDeviceId"    # Ljava/lang/String;
    .param p2, "dataType"    # I
    .param p3, "uData"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p4, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 1242
    return-void
.end method

.method public onTerminate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 496
    invoke-super {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->onTerminate()V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachTask:Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachTask:Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachTask:Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;->cancel(Z)Z

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachRcvr:Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    if-eqz v0, :cond_1

    .line 503
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachRcvr:Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->unregisterBR(Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;Landroid/content/Context;)V

    .line 504
    iput-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mCoachRcvr:Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;

    .line 506
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication;->statusObserver:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    if-eqz v0, :cond_2

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication;->statusObserver:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 509
    iput-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->statusObserver:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    .line 511
    :cond_2
    return-void
.end method

.method public postInitialized()V
    .locals 0

    .prologue
    .line 490
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->loadAppRegistryData()V

    .line 491
    return-void
.end method

.method public preInit()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 254
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v3, "[PERF] preInit() - START"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v1

    .line 256
    .local v1, "isDefaultPassword":I
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KeyManager : Launch SetPasswordActivity - isDefaultPassword "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    if-ne v1, v5, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getKeyMansgerState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 258
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    iput-boolean v5, v2, Lcom/sec/android/service/health/keyManager/KeyManager;->isStartedFromApp:Z

    .line 259
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v3, "KeyManager : Launch SetPasswordActivity - KeyManager"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onCreate : Launch SetPasswordActivity - KeyManager"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/service/health/keyManager/KeyManager;->setApplicationPassword(Z)V

    .line 272
    :goto_0
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v3, "[PERF] preInit - END"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    return-void

    .line 264
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v3, "KeyManager : Launch init"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication;->init()V

    .line 268
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 269
    .local v0, "ii":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public setConnectedDevice(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Z
    .locals 5
    .param p1, "uContext"    # Landroid/content/Context;
    .param p2, "uDevice"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    const/high16 v4, 0x14000000

    const/4 v1, 0x0

    .line 1261
    if-nez p2, :cond_0

    .line 1263
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v3, "Illegal values provided !!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1290
    :goto_0
    return v1

    .line 1267
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 1269
    sget-object v2, Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;

    const-string v3, "This is not our cup of tea !! internal HRM has to be handled differently!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1273
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/16 v2, 0x2718

    if-eq v1, v2, :cond_2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/16 v2, 0x271f

    if-ne v1, v2, :cond_3

    .line 1276
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.shealth.action.EXERCISE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1277
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1278
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1279
    const-string v1, "DEV_ID"

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1280
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1283
    .end local v0    # "i":Landroid/content/Intent;
    :cond_3
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v1

    const/16 v2, 0x2712

    if-ne v1, v2, :cond_4

    .line 1284
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.shealth.action.WEIGHT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1285
    .restart local v0    # "i":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1286
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1287
    const-string v1, "DEV_ID"

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1288
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1290
    .end local v0    # "i":Landroid/content/Intent;
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method
