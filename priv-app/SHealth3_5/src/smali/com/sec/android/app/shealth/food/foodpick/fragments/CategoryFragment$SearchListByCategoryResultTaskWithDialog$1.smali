.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog$1;
.super Ljava/lang/Object;
.source "CategoryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnFoodClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->getFoodInfoItem()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;->onFoodInfoSelected(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z

    .line 535
    :cond_0
    return-void
.end method

.method public bridge synthetic onItemClick(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 528
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog$1;->onItemClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V

    return-void
.end method
