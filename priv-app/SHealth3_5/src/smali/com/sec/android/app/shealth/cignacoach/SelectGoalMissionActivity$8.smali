.class Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;
.super Ljava/lang/Object;
.source "SelectGoalMissionActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->showDeleteDoneDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 474
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->performDelete()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$900(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    .line 476
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 477
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "intent_delete_all"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->isAllDelete()Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1000(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->setResult(ILandroid/content/Intent;)V

    .line 479
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->finish()V

    .line 480
    return-void
.end method
