.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x2

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, 0x13faa9cd689b0a82L

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x13faa9cd689b0a82L

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isCheckAll()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setUpSelectMode()V

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isLogSelected(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setLogSelected(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->notifyDataSetChanged()V

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleSelectAllCheckBox()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    const/4 v1, 0x1

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v2, v3

    long-to-int v3, v3

    if-lt v1, v3, :cond_8

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v3, 0x0

    int-to-long v0, v0

    const/16 v4, 0x20

    shl-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_9

    const-wide v6, 0x13faa9cd689b0a82L

    xor-long/2addr v0, v6

    :cond_9
    const/16 v6, 0x20

    shl-long/2addr v0, v6

    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, 0x13faa9cd689b0a82L

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_c

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x1a

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x29

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_b
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_c
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_d

    const-wide v4, 0x13faa9cd689b0a82L

    xor-long/2addr v0, v4

    :cond_d
    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_5
    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleShareViewButton()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$800(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshFocusables()V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto :goto_5

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
