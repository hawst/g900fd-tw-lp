.class Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;
.super Ljava/lang/Object;
.source "SetQuickInputActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "clickedView"    # Landroid/view/View;

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-eqz v0, :cond_0

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    # setter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$002(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # setter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$102(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Landroid/view/View;)Landroid/view/View;

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mealTitleResourceMap:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$000(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090044

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f030118

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    # setter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$202(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;Landroid/support/v4/app/DialogFragment;)Landroid/support/v4/app/DialogFragment;

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$200(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "set_quick_input_tag"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2$1;-><init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$2;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 265
    :cond_0
    return-void
.end method
