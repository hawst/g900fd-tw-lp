.class Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;
.super Ljava/lang/Object;
.source "WeightService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/service/WeightService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeviceDataListener"
.end annotation


# instance fields
.field private mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field weightService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/weight/service/WeightService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/sec/android/app/shealth/weight/service/WeightService;)V
    .locals 1
    .param p1, "shealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .param p2, "wgtService"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 355
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    .line 356
    return-void
.end method

.method private updateProfile()V
    .locals 10

    .prologue
    .line 410
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v8

    const-string v9, "*****updateProfile()"

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    new-instance v6, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/weight/service/WeightService;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/weight/service/WeightService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v6, v8}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 412
    .local v6, "mWeightDao":Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    invoke-interface {v6}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastCreatedData()Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 413
    .local v1, "lastCreatedWeightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v2

    .line 414
    .local v2, "lastCreatedWeightDataSampleTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-interface {v6, v8, v9}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v4

    .line 416
    .local v4, "lastRecordTime":J
    cmp-long v8, v2, v4

    if-ltz v8, :cond_0

    .line 417
    new-instance v7, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/weight/service/WeightService;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/weight/service/WeightService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 419
    .local v7, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 420
    invoke-virtual {v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    .end local v7    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_0
    :goto_0
    return-void

    .line 421
    .restart local v7    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 366
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "*****onReceived() for single data"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;->onReceived()V

    .line 371
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->updateProfile()V

    .line 372
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 376
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "*****onReceived() for multi data"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;->onReceived()V

    .line 381
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->updateProfile()V

    .line 382
    return-void
.end method

.method public onStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 360
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(onStarted) mShealthSensorDevice.getState() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$702(Lcom/sec/android/app/shealth/weight/service/WeightService;Z)Z

    .line 362
    return-void
.end method

.method public onStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 386
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "*****onStopped()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "errorCode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " isStopReceivingDataCalled : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$700(Lcom/sec/android/app/shealth/weight/service/WeightService;)Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    if-eqz p2, :cond_0

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;->showErrorPopUp()V

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$700(Lcom/sec/android/app/shealth/weight/service/WeightService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 401
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "recall startReceivingData() for receiving next data"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/sec/android/app/shealth/weight/service/WeightService;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    invoke-static {v0, p0, v1}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$600(Lcom/sec/android/app/shealth/weight/service/WeightService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 406
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->isStopReceivingDataCalled:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$702(Lcom/sec/android/app/shealth/weight/service/WeightService;Z)Z

    .line 407
    return-void

    .line 404
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "skip calling startReceivingData in onStopped"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
