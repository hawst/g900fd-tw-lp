.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;
.super Ljava/lang/Thread;
.source "HeartRateScoverFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PulseViewThread"
.end annotation


# instance fields
.field currentCount:I

.field isDrawing:Z

.field isRun:Z

.field linePointX:[F

.field linePointY:[F

.field maxCount:I

.field pointX:[F

.field pointY:[F

.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x7

    const/4 v1, 0x0

    .line 1355
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1343
    const/16 v0, 0x9a

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->maxCount:I

    .line 1344
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    .line 1346
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    .line 1347
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    .line 1349
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->maxCount:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    .line 1350
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->maxCount:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    .line 1352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->isRun:Z

    .line 1353
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->isDrawing:Z

    .line 1356
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pulsPointX()V

    .line 1357
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pulsPointY()V

    .line 1358
    return-void

    .line 1346
    nop

    :array_0
    .array-data 4
        0x40d00000    # 6.5f
        0x41d00000    # 26.0f
        0x41ebb74c
        0x4203b74b
        0x4211147a    # 36.269997f
        0x42213333    # 40.3f
        0x4269ffff    # 58.499996f
    .end array-data

    .line 1347
    :array_1
    .array-data 4
        0x42188418
        0x42188418
        0x41b7b74c
        0x42421db2    # 48.529f
        0x41e83b64    # 29.029f
        0x42188418
        0x42188418
    .end array-data
.end method

.method private pulsPointX()V
    .locals 17

    .prologue
    .line 1420
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/4 v15, 0x0

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x1

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v8

    .line 1421
    .local v8, "x1":F
    const/high16 v14, 0x42400000    # 48.0f

    div-float v1, v8, v14

    .line 1422
    .local v1, "_x1":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/4 v15, 0x1

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x2

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 1423
    .local v9, "x2":F
    const/high16 v14, 0x41900000    # 18.0f

    div-float v2, v9, v14

    .line 1424
    .local v2, "_x2":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/4 v15, 0x2

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x3

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v10

    .line 1425
    .local v10, "x3":F
    const/high16 v14, 0x41900000    # 18.0f

    div-float v3, v10, v14

    .line 1426
    .local v3, "_x3":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/4 v15, 0x3

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x4

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1427
    .local v11, "x4":F
    const/high16 v14, 0x41900000    # 18.0f

    div-float v4, v11, v14

    .line 1428
    .local v4, "_x4":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/4 v15, 0x4

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x5

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v12

    .line 1429
    .local v12, "x5":F
    const/high16 v14, 0x41400000    # 12.0f

    div-float v5, v12, v14

    .line 1430
    .local v5, "_x5":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/4 v15, 0x5

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x6

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v13

    .line 1431
    .local v13, "x6":F
    const/high16 v14, 0x42200000    # 40.0f

    div-float v6, v13, v14

    .line 1433
    .local v6, "_x6":F
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->maxCount:I

    add-int/lit8 v14, v14, 0x1

    if-ge v7, v14, :cond_6

    .line 1434
    const/16 v14, 0x30

    if-ge v7, v14, :cond_1

    .line 1435
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x0

    aget v15, v15, v16

    add-int/lit8 v16, v7, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v1

    add-float v15, v15, v16

    aput v15, v14, v7

    .line 1433
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1436
    :cond_1
    const/16 v14, 0x42

    if-ge v7, v14, :cond_2

    .line 1437
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x1

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x30

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v2

    add-float v15, v15, v16

    aput v15, v14, v7

    goto :goto_1

    .line 1438
    :cond_2
    const/16 v14, 0x54

    if-ge v7, v14, :cond_3

    .line 1439
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x2

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x42

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v3

    add-float v15, v15, v16

    aput v15, v14, v7

    goto :goto_1

    .line 1440
    :cond_3
    const/16 v14, 0x66

    if-ge v7, v14, :cond_4

    .line 1441
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x3

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x54

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v4

    add-float v15, v15, v16

    aput v15, v14, v7

    goto :goto_1

    .line 1442
    :cond_4
    const/16 v14, 0x72

    if-ge v7, v14, :cond_5

    .line 1443
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x4

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x66

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v5

    add-float v15, v15, v16

    aput v15, v14, v7

    goto/16 :goto_1

    .line 1444
    :cond_5
    const/16 v14, 0x9a

    if-ge v7, v14, :cond_0

    .line 1445
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointX:[F

    const/16 v16, 0x5

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x72

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v6

    add-float v15, v15, v16

    aput v15, v14, v7

    goto/16 :goto_1

    .line 1448
    :cond_6
    return-void
.end method

.method private pulsPointY()V
    .locals 17

    .prologue
    .line 1452
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/4 v15, 0x0

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x1

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v8

    .line 1453
    .local v8, "y1":F
    const/high16 v14, 0x42400000    # 48.0f

    div-float v1, v8, v14

    .line 1454
    .local v1, "_y1":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/4 v15, 0x1

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x2

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 1455
    .local v9, "y2":F
    const/high16 v14, 0x41900000    # 18.0f

    div-float v2, v9, v14

    .line 1456
    .local v2, "_y2":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/4 v15, 0x2

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x3

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v10

    .line 1457
    .local v10, "y3":F
    const/high16 v14, 0x41900000    # 18.0f

    div-float v3, v10, v14

    .line 1458
    .local v3, "_y3":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/4 v15, 0x3

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x4

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1459
    .local v11, "y4":F
    const/high16 v14, 0x41900000    # 18.0f

    div-float v4, v11, v14

    .line 1460
    .local v4, "_y4":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/4 v15, 0x4

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x5

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v12

    .line 1461
    .local v12, "y5":F
    const/high16 v14, 0x41400000    # 12.0f

    div-float v5, v12, v14

    .line 1462
    .local v5, "_y5":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/4 v15, 0x5

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x6

    aget v15, v15, v16

    sub-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v13

    .line 1463
    .local v13, "y6":F
    const/high16 v14, 0x42200000    # 40.0f

    div-float v6, v13, v14

    .line 1465
    .local v6, "_y6":F
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->maxCount:I

    add-int/lit8 v14, v14, 0x1

    if-ge v7, v14, :cond_6

    .line 1466
    const/16 v14, 0x30

    if-ge v7, v14, :cond_1

    .line 1467
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x0

    aget v15, v15, v16

    add-int/lit8 v16, v7, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v1

    add-float v15, v15, v16

    aput v15, v14, v7

    .line 1465
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1468
    :cond_1
    const/16 v14, 0x42

    if-ge v7, v14, :cond_2

    .line 1469
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x1

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x30

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v2

    sub-float v15, v15, v16

    aput v15, v14, v7

    goto :goto_1

    .line 1470
    :cond_2
    const/16 v14, 0x54

    if-ge v7, v14, :cond_3

    .line 1471
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x2

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x42

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v3

    add-float v15, v15, v16

    aput v15, v14, v7

    goto :goto_1

    .line 1472
    :cond_3
    const/16 v14, 0x66

    if-ge v7, v14, :cond_4

    .line 1473
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x3

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x54

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v4

    sub-float v15, v15, v16

    aput v15, v14, v7

    goto :goto_1

    .line 1474
    :cond_4
    const/16 v14, 0x72

    if-ge v7, v14, :cond_5

    .line 1475
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x4

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x66

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v5

    add-float v15, v15, v16

    aput v15, v14, v7

    goto/16 :goto_1

    .line 1476
    :cond_5
    const/16 v14, 0x9a

    if-ge v7, v14, :cond_0

    .line 1477
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->pointY:[F

    const/16 v16, 0x5

    aget v15, v15, v16

    add-int/lit8 v16, v7, -0x72

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v6

    add-float v15, v15, v16

    aput v15, v14, v7

    goto/16 :goto_1

    .line 1480
    :cond_6
    return-void
.end method


# virtual methods
.method public getDrawingMode()Z
    .locals 1

    .prologue
    .line 1365
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->isDrawing:Z

    return v0
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1370
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->isRun:Z

    if-eqz v1, :cond_0

    .line 1371
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->maxCount:I

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    if-ne v1, v2, :cond_3

    .line 1372
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1374
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->isEndPulseDelay:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$4602(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)Z

    .line 1375
    const-wide/16 v1, 0x370

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1379
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->isDrawing:Z

    if-eqz v1, :cond_1

    .line 1380
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pulseHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1387
    :goto_2
    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    .line 1408
    :cond_0
    return-void

    .line 1376
    :catch_0
    move-exception v0

    .line 1377
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 1382
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pulseHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 1385
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # invokes: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopPulse()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$4700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V

    goto :goto_2

    .line 1392
    :cond_3
    const-wide/16 v1, 0x4

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1397
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mPulseView:Lcom/sec/android/app/shealth/heartrate/PulseView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/PulseView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointX:[F

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->linePointY:[F

    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    aget v3, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/PulseView;->setPath(FF)V

    .line 1399
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread$1;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1406
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    goto :goto_0

    .line 1393
    :catch_1
    move-exception v0

    .line 1394
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3
.end method

.method public setDrawingMode(Z)V
    .locals 0
    .param p1, "isDraw"    # Z

    .prologue
    .line 1361
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->isDrawing:Z

    .line 1362
    return-void
.end method

.method public threadStop()V
    .locals 1

    .prologue
    .line 1411
    monitor-enter p0

    .line 1412
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->isRun:Z

    .line 1413
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$PulseViewThread;->currentCount:I

    .line 1414
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 1415
    monitor-exit p0

    .line 1416
    return-void

    .line 1415
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
