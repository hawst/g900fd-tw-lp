.class public Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProDetailsActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$IOnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$17;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ExerciseProDetailsActivity"


# instance fields
.field public chartFlag:Z

.field public chartPace:Z

.field public chartSpeed:Z

.field private currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

.field public currentTabTag:Ljava/lang/String;

.field private dataType:I

.field exercixeInfoId:I

.field public intent:Landroid/content/Intent;

.field private isNeedInit:Z

.field public isShownDeletePopup:Z

.field private lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

.field mAMapDetailsFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

.field private mCallingActivity:Ljava/lang/String;

.field private mComment:Ljava/lang/String;

.field mCurrentFragment:Landroid/support/v4/app/Fragment;

.field private mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field mDetailsChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

.field mDetailsFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

.field mDetailsMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

.field private mEditMode:Z

.field private mEndMode:Z

.field private mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

.field private mExerciseDate:J

.field private mPlaceMode:I

.field private rowId:J

.field private saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

.field private tab1:Landroid/view/View;

.field private tab2:Landroid/view/View;

.field private tab3:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 78
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->exercixeInfoId:I

    .line 81
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .line 82
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCallingActivity:Ljava/lang/String;

    .line 98
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 99
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 100
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 103
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isNeedInit:Z

    .line 104
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartSpeed:Z

    .line 106
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartPace:Z

    .line 108
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExerciseDate:J

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->addToScreenViewsList(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    return-wide v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->savedExerciseAndExit()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->savedEndModeExercise()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->checkChangesAndExit()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->dataType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->addToScreenViewsList(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    return-object v0
.end method

.method private checkChangesAndExit()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1018
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    if-eqz v1, :cond_2

    .line 1019
    const-string v0, ""

    .line 1020
    .local v0, "comment":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v1, :cond_0

    .line 1021
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getMemoText()Ljava/lang/String;

    move-result-object v0

    .line 1027
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isDetailsChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1028
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->showResetPopup()V

    .line 1037
    .end local v0    # "comment":Ljava/lang/String;
    :goto_1
    return-void

    .line 1023
    .restart local v0    # "comment":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    goto :goto_0

    .line 1030
    :cond_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 1031
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_1

    .line 1034
    .end local v0    # "comment":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 1035
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_1
.end method

.method public static getCurrentFragment(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 917
    const-string v0, "exercise_pro_details_fragment"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private getExtra(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_workout_end_mode"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    const-string v2, "data_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->dataType:I

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v2, "pick_result"

    invoke-virtual {v1, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v2, "pick_type"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->exercixeInfoId:I

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v2, "starttime"

    invoke-virtual {v1, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExerciseDate:J

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCallingActivity:Ljava/lang/String;

    .line 147
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    if-nez v1, :cond_2

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    const-string v2, "edit"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    .line 152
    :goto_0
    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_3

    .line 153
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->updateExerciseDetails()V

    .line 159
    :goto_1
    if-eqz p1, :cond_1

    .line 160
    const-string v1, "currentTabTag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 161
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 162
    const-string v1, "currentTabTag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTabTag:Ljava/lang/String;

    .line 163
    const-string v1, "exercise_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    .line 166
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    return-void

    .line 150
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_sync_mode"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mPlaceMode:I

    goto :goto_0

    .line 155
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_1
.end method

.method private initializeFragments()V
    .locals 6

    .prologue
    .line 231
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->dataType:I

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;-><init>(IJLjava/lang/String;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDetailsFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .line 232
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-nez v0, :cond_0

    .line 233
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->dataType:I

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;-><init>(IJLandroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDetailsMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .line 237
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;-><init>(ZJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDetailsChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .line 239
    return-void

    .line 235
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->dataType:I

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->intent:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;-><init>(IJLandroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mAMapDetailsFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    goto :goto_0
.end method

.method private savedEndModeExercise()J
    .locals 5

    .prologue
    .line 751
    const-wide/16 v0, -0x1

    .line 752
    .local v0, "result":J
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v2, :cond_0

    .line 753
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getMemoText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    .line 754
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 755
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    .line 757
    :cond_0
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    invoke-static {p0, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->updateRealtimeItem(Landroid/content/Context;JLjava/lang/String;)J

    move-result-wide v0

    .line 759
    return-wide v0
.end method

.method private savedExerciseAndExit()V
    .locals 9

    .prologue
    const v8, 0x7f090835

    const/4 v7, 0x1

    const/4 v6, -0x1

    const-wide/16 v4, 0x0

    .line 763
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    if-eqz v3, :cond_1

    .line 764
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->savedEndModeExercise()J

    move-result-wide v1

    .line 765
    .local v1, "result":J
    cmp-long v3, v1, v4

    if-ltz v3, :cond_0

    .line 767
    invoke-static {p0, v8, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 768
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 769
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 770
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    .line 789
    .end local v1    # "result":J
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    if-eqz v3, :cond_0

    .line 773
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->savedEndModeExercise()J

    move-result-wide v1

    .line 774
    .restart local v1    # "result":J
    cmp-long v3, v1, v4

    if-ltz v3, :cond_2

    .line 776
    invoke-static {p0, v8, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 777
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealtimeExerciseId(J)V

    .line 778
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 781
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 782
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "pick_result"

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 783
    const-string v3, "data_type"

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->dataType:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 784
    const-string v3, "edit"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 786
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 787
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_0
.end method

.method private setCurrentTabContentDescription(Landroid/view/View;Z)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "value"    # Z

    .prologue
    const v9, 0x7f090a1a

    const v5, 0x7f090784

    const v8, 0x7f0901f2

    const v7, 0x7f0901f1

    const v6, 0x7f090203

    .line 325
    move-object v2, p1

    check-cast v2, Landroid/widget/TextView;

    .line 326
    .local v2, "tvView":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 327
    .local v1, "lang":Ljava/lang/String;
    const-string v0, "de"

    .line 328
    .local v0, "german":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 329
    const/4 v3, 0x1

    if-ne p2, v3, :cond_2

    .line 330
    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 331
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 340
    :cond_2
    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 341
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 345
    :cond_3
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    .line 607
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 609
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    const v2, 0x7f090035

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 611
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f09011a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 630
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 631
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "delete_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 632
    return-void
.end method

.method private showResetPopup()V
    .locals 4

    .prologue
    .line 993
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 994
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090f4d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090081

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090f4e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$15;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$14;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1014
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "discard_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1015
    return-void
.end method

.method private switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;J)V
    .locals 6
    .param p1, "tab"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    .param p2, "exerciseRowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 256
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-eqz v2, :cond_0

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewForTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    .line 260
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewForTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 262
    .local v1, "previousFragment":Landroid/support/v4/app/Fragment;
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne p1, v2, :cond_2

    .line 264
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDetailsFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 286
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->invalidateOptionsMenu()V

    .line 288
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->switchToFragment(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 291
    .local v0, "config":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v4, :cond_7

    .line 292
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    if-eqz v2, :cond_1

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->hideTabLayout()V

    .line 301
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->clearScreensViewsList()V

    .line 302
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 314
    return-void

    .line 265
    .end local v0    # "config":Landroid/content/res/Configuration;
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne p1, v2, :cond_4

    .line 266
    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "E014"

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_3

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mAMapDetailsFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 272
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDetailsMapFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 274
    :cond_4
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne p1, v2, :cond_6

    .line 275
    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "E012"

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getChartFlag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "chartPace"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 277
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartPace:Z

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    .line 281
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDetailsChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 279
    :cond_5
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartSpeed:Z

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    goto :goto_2

    .line 284
    :cond_6
    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "This tab should not exist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 299
    .restart local v0    # "config":Landroid/content/res/Configuration;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->customizeActionBar()V

    goto :goto_1
.end method

.method private updateExerciseDetails()V
    .locals 2

    .prologue
    .line 921
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getExerciseData(Landroid/content/Context;J)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .line 922
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getComment()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    .line 926
    :cond_0
    return-void
.end method


# virtual methods
.method public closeScreen()V
    .locals 0

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->onBackPressed()V

    .line 821
    return-void
.end method

.method protected customizeActionBar()V
    .locals 13

    .prologue
    const v10, 0x7f090040

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 636
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 638
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 639
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-static {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I

    move-result v7

    .line 640
    .local v7, "title":I
    if-lez v7, :cond_0

    .line 641
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 643
    :cond_0
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isNeedInit:Z

    if-eqz v8, :cond_2

    .line 748
    :cond_1
    :goto_0
    return-void

    .line 646
    :cond_2
    new-instance v6, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$11;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    .line 670
    .local v6, "saveButtonListener":Landroid/view/View$OnClickListener;
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    .line 684
    .local v3, "cancleButtonListener":Landroid/view/View$OnClickListener;
    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$13;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    .line 706
    .local v4, "editButtonListener":Landroid/view/View$OnClickListener;
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v8, v9, :cond_3

    .line 707
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v8

    if-nez v8, :cond_3

    .line 708
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    invoke-virtual {v8, v11}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 711
    :cond_3
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    if-eqz v8, :cond_4

    .line 712
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v8, 0x7f0207a9

    invoke-direct {v0, v8, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 714
    .local v0, "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v8, 0x7f090044

    invoke-direct {v1, v6, v8}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 716
    .local v1, "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    new-array v9, v11, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0

    .line 718
    .end local v0    # "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .end local v1    # "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_4
    iget-boolean v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    if-eqz v8, :cond_5

    .line 719
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    invoke-virtual {v8, v12}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 722
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v8, 0x7f090048

    invoke-direct {v2, v3, v8}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 725
    .local v2, "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v2, v11}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 726
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    new-array v9, v11, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 728
    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v8, 0x7f09004f

    invoke-direct {v5, v6, v8}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    .line 730
    .local v5, "saveButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v5, v11}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setISOKCancelType(Z)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 731
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    new-array v9, v11, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 733
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->hideTabLayout()V

    goto/16 :goto_0

    .line 738
    .end local v2    # "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .end local v5    # "saveButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_5
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v8, 0x7f020299

    invoke-direct {v0, v8, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 739
    .restart local v0    # "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 740
    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 741
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v8

    new-array v9, v11, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 742
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v8, v9, :cond_1

    goto/16 :goto_0
.end method

.method protected customizeActionBarWithTabs()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 353
    const v0, 0x7f080733

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    .line 354
    const v0, 0x7f080735

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    .line 355
    const v0, 0x7f080736

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 452
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabSelected()V

    .line 453
    return-void
.end method

.method protected findViewForTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)Landroid/view/View;
    .locals 2
    .param p1, "tab"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .prologue
    .line 899
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$17;->$SwitchMap$com$sec$android$app$shealth$plugins$exercisepro$ExerciseProDetailsActivity$Tab:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 907
    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "this tab does not exist"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 901
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    .line 905
    :goto_0
    return-object v0

    .line 903
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    goto :goto_0

    .line 905
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    goto :goto_0

    .line 899
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 930
    const-string/jumbo v0, "share_via_popup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 931
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;-><init>(Landroid/content/Context;)V

    .line 933
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 913
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 4
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1041
    const-string v2, "exercisePrimaryDisplay"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1042
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$16;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    .line 1080
    :cond_0
    :goto_0
    return-object v1

    .line 1066
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v2, v3, :cond_0

    .line 1067
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1068
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_2

    .line 1069
    instance-of v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    if-eqz v2, :cond_0

    .line 1070
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v1

    goto :goto_0

    .line 1074
    :cond_2
    instance-of v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    if-eqz v2, :cond_0

    .line 1075
    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v1

    goto :goto_0
.end method

.method public hideTabLayout()V
    .locals 2

    .prologue
    .line 242
    const v0, 0x7f080732

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 243
    return-void
.end method

.method public isDetailsChanged()Z
    .locals 4

    .prologue
    .line 1194
    const-string v0, ""

    .line 1195
    .local v0, "comment":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isPhotoChanged()Z

    move-result v1

    .line 1196
    .local v1, "isPhotoChanged":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v2, :cond_0

    .line 1198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getMemoText()Ljava/lang/String;

    move-result-object v0

    .line 1202
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->refreshFocusables()V

    .line 1203
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->areStringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1204
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->areStringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 1208
    :goto_1
    return v2

    .line 1200
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    goto :goto_0

    .line 1204
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    move v2, v1

    .line 1208
    goto :goto_1
.end method

.method public isPhotoChanged()Z
    .locals 1

    .prologue
    .line 1214
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v0, :cond_0

    .line 1215
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isPhotChanged()Z

    move-result v0

    .line 1216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 938
    const-string v1, "ExerciseProDetailsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onActivityResult  requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    const/16 v1, 0x99

    if-ne p1, v1, :cond_0

    .line 940
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 941
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    .line 943
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 944
    .local v0, "f":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v1, :cond_1

    .line 945
    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .end local v0    # "f":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 947
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 948
    return-void
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/16 v3, 0xc8

    const/4 v4, 0x0

    .line 848
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 849
    .local v0, "currentFragment":Landroid/support/v4/app/Fragment;
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_1

    instance-of v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    if-eqz v2, :cond_1

    .line 850
    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->isAbleFinished()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 896
    :cond_0
    :goto_0
    return-void

    .line 853
    .restart local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :cond_1
    instance-of v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    if-eqz v2, :cond_4

    .line 854
    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->isAbleFinished()Z

    move-result v2

    if-nez v2, :cond_0

    .line 890
    :cond_2
    :goto_1
    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCallingActivity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 891
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 892
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 894
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 895
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_0

    .line 857
    .restart local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :cond_4
    instance-of v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v2, :cond_9

    .line 858
    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->onBackPress()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 859
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    if-eqz v2, :cond_6

    .line 860
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isDetailsChanged()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 861
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->savedExerciseAndExit()V

    goto :goto_0

    .line 863
    :cond_5
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 864
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_0

    .line 867
    :cond_6
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    if-eqz v2, :cond_7

    .line 868
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->savedEndModeExercise()J

    .line 869
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 870
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_0

    .line 872
    :cond_7
    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProWorkoutInfoActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCallingActivity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 873
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 874
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 875
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_0

    .line 880
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_8
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 881
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_0

    .line 885
    .restart local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :cond_9
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    if-eqz v2, :cond_2

    .line 886
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->savedEndModeExercise()J

    .line 887
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 251
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 253
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 119
    const v1, 0x7f03019f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setContentView(I)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 122
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_0

    .line 124
    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 130
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getExtra(Landroid/os/Bundle;)V

    .line 131
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mPlaceMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 132
    const v1, 0x7f080734

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 134
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isNeedInit:Z

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->recreateActionBar()V

    .line 136
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 460
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    if-eqz v4, :cond_1

    .line 519
    :cond_0
    :goto_0
    return v3

    .line 463
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 465
    .local v1, "inflater":Landroid/view/MenuInflater;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v4, v5, :cond_7

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v4

    if-nez v4, :cond_3

    .line 467
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 472
    :goto_1
    const v3, 0x7f10001b

    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 473
    const v3, 0x7f080ca8

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 474
    .local v0, "bedMenuItem":Landroid/view/MenuItem;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v3

    const-string v4, "km"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 477
    .local v2, "kmUnit":Z
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getChartFlag()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getChartFlag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "chartPace"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 479
    if-eqz v2, :cond_4

    .line 480
    const v3, 0x7f090f5d

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 519
    .end local v0    # "bedMenuItem":Landroid/view/MenuItem;
    .end local v2    # "kmUnit":Z
    :cond_2
    :goto_2
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    goto :goto_0

    .line 469
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_1

    .line 482
    .restart local v0    # "bedMenuItem":Landroid/view/MenuItem;
    .restart local v2    # "kmUnit":Z
    :cond_4
    const v3, 0x7f090f71

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_2

    .line 489
    :cond_5
    if-eqz v2, :cond_6

    .line 490
    const v3, 0x7f090f5c

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_2

    .line 492
    :cond_6
    const v3, 0x7f090f5b

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_2

    .line 498
    .end local v0    # "bedMenuItem":Landroid/view/MenuItem;
    .end local v2    # "kmUnit":Z
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v4, v5, :cond_9

    .line 499
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v4

    if-nez v4, :cond_8

    .line 500
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 504
    :goto_3
    const v3, 0x7f10001c

    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_2

    .line 502
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_3

    .line 506
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v4, v5, :cond_b

    .line 507
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v4

    if-nez v4, :cond_a

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 512
    :goto_4
    const v3, 0x7f10001d

    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_2

    .line 510
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_4

    .line 515
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 825
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 826
    .local v0, "currentFragment":Landroid/support/v4/app/Fragment;
    instance-of v1, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v1, :cond_0

    .line 827
    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->onBackPress()Z

    .line 829
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 830
    return-void
.end method

.method public onDismiss()V
    .locals 0

    .prologue
    .line 1086
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 834
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 835
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z

    if-eqz v0, :cond_0

    .line 836
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->onBackPressed()V

    .line 841
    :goto_0
    const/4 v0, 0x1

    .line 843
    :goto_1
    return v0

    .line 838
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 839
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    goto :goto_0

    .line 843
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 524
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    move v1, v2

    .line 602
    :goto_0
    return v1

    .line 554
    :sswitch_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getChartFlag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getChartFlag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "chartPace"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 556
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartSpeed:Z

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    .line 557
    const-string v2, "chartSpeed"

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveChartFlag(Ljava/lang/String;)V

    .line 566
    :goto_1
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const v2, 0x7f09060f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x7d0

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 561
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartPace:Z

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    .line 562
    const-string v2, "chartPace"

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveChartFlag(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 586
    :sswitch_1
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->printSnapShot(Ljava/lang/String;Landroid/app/Activity;)V
    :try_end_2
    .catch Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 587
    :catch_1
    move-exception v0

    .line 589
    .local v0, "e":Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
    const v3, 0x7f0900e5

    invoke-static {p0, v3, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 594
    .end local v0    # "e":Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->showDeleteDialog()V

    goto :goto_0

    .line 598
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->shareContents()V

    goto :goto_0

    .line 524
    :sswitch_data_0
    .sparse-switch
        0x7f080c8d -> :sswitch_2
        0x7f080ca7 -> :sswitch_1
        0x7f080ca8 -> :sswitch_0
        0x7f080ca9 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isShownDeletePopup:Z

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 192
    :cond_0
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 196
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->hideTabLayout()V

    .line 200
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->initializeFragments()V

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->customizeActionBarWithTabs()V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->showTabLayout()V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 211
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->customizeActionBarWithTabs()V

    .line 213
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getChartFlag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chartPace"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartPace:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    .line 219
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->initializeFragments()V

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_4

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)V

    goto :goto_0

    .line 209
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    goto :goto_1

    .line 216
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartSpeed:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->chartFlag:Z

    goto :goto_2

    .line 222
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-eq v0, v1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 974
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 976
    const-string v1, "ExerciseProDetailsActivity"

    const-string/jumbo v2, "onRestoreInstanceState.. "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    const-string v1, "currentTabTag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 978
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 979
    const-string v1, "currentTabTag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTabTag:Ljava/lang/String;

    .line 980
    const-string v1, "comment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    .line 981
    const-string v1, "exercise_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    .line 983
    :cond_0
    const-string v1, "delete_popup"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isShownDeletePopup:Z

    .line 984
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->values()[Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    move-result-object v1

    const-string/jumbo v2, "selected_tab"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 985
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->values()[Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    move-result-object v1

    const-string/jumbo v2, "selected_tab"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 986
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v1, :cond_1

    .line 987
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->setMenoText(Ljava/lang/String;)V

    .line 989
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 171
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 172
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isShownDeletePopup:Z

    if-eqz v0, :cond_0

    .line 173
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isShownDeletePopup:Z

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->showDeleteDialog()V

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v0, v1, :cond_1

    .line 177
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab1:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab2:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->tab3:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 182
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 952
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 953
    const-string v1, "ExerciseProDetailsActivity"

    const-string/jumbo v2, "onSaveInstanceState.. "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 956
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    if-eqz v1, :cond_0

    .line 957
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->getMemoText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    .line 959
    :cond_0
    const-string v1, "comment"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    const-string v1, "exercise_id"

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 961
    const-string v1, "currentTabTag"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 962
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->saveTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-eqz v1, :cond_1

    .line 963
    const-string/jumbo v1, "selected_tab"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->ordinal()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 965
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-nez v1, :cond_2

    .line 966
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 968
    :cond_2
    const-string v1, "delete_popup"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->isShownDeletePopup:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 969
    const-string v1, "action_tab"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->lastActionTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->ordinal()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 970
    return-void
.end method

.method recreateActionBar()V
    .locals 0

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->customizeActionBar()V

    .line 322
    return-void
.end method

.method protected setCurrentTabSelected()V
    .locals 2

    .prologue
    .line 792
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewForTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 793
    return-void
.end method

.method shareContents()V
    .locals 28

    .prologue
    .line 1089
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v2, v5, :cond_2

    .line 1090
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    move-object/from16 v16, v0

    check-cast v16, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;

    .line 1092
    .local v16, "fg":Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getView()Landroid/view/View;

    move-result-object v25

    .line 1093
    .local v25, "view":Landroid/view/View;
    if-nez v25, :cond_1

    .line 1191
    .end local v16    # "fg":Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;
    .end local v25    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 1096
    .restart local v16    # "fg":Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;
    .restart local v25    # "view":Landroid/view/View;
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1097
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v13

    .line 1098
    .local v13, "cache":Landroid/graphics/Bitmap;
    if-eqz v13, :cond_0

    .line 1101
    invoke-static {v13}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1102
    .local v3, "returnedBitmap":Landroid/graphics/Bitmap;
    new-instance v12, Landroid/graphics/Canvas;

    invoke-direct {v12, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1103
    .local v12, "c":Landroid/graphics/Canvas;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getChartView()Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v15

    .line 1104
    .local v15, "chartView":Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;->getLegendView()Landroid/view/View;

    move-result-object v19

    .line 1106
    .local v19, "legendView":Landroid/view/View;
    const/4 v14, 0x0

    .line 1108
    .local v14, "chartBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v15}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getWidth()I

    move-result v2

    invoke-virtual {v15}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getHeight()I

    move-result v5

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v5, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 1109
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v5

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v5, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 1110
    invoke-virtual {v15, v14}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1111
    const/4 v2, 0x0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v15}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getHeight()I

    move-result v7

    sub-int/2addr v5, v7

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v7

    sub-int/2addr v5, v7

    int-to-float v5, v5

    const/4 v7, 0x0

    invoke-virtual {v12, v14, v2, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I

    move-result v4

    .line 1114
    .local v4, "title":I
    const-wide/16 v5, -0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareView(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;)V

    .line 1116
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1117
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->destroyDrawingCache()V

    .line 1118
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v5, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v7, "CS01"

    invoke-static {v2, v5, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1120
    .end local v3    # "returnedBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "title":I
    .end local v12    # "c":Landroid/graphics/Canvas;
    .end local v13    # "cache":Landroid/graphics/Bitmap;
    .end local v14    # "chartBitmap":Landroid/graphics/Bitmap;
    .end local v15    # "chartView":Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .end local v16    # "fg":Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsChartFragment;
    .end local v19    # "legendView":Landroid/view/View;
    .end local v25    # "view":Landroid/view/View;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v2, v5, :cond_3

    .line 1121
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_0

    goto/16 :goto_0

    .line 1129
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->currentTab:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    if-ne v2, v5, :cond_0

    .line 1130
    const/16 v2, 0xd

    new-array v0, v2, [Landroid/graphics/Bitmap;

    move-object/from16 v23, v0

    .line 1131
    .local v23, "tempBmp":[Landroid/graphics/Bitmap;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    const/16 v2, 0xd

    move/from16 v0, v17

    if-ge v0, v2, :cond_4

    .line 1132
    const/4 v2, 0x0

    aput-object v2, v23, v17

    .line 1131
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 1135
    :cond_4
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 1137
    .local v22, "resultItemViewArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const v2, 0x7f08073a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .line 1138
    .local v24, "typeView":Landroid/view/View;
    if-eqz v24, :cond_5

    .line 1139
    const v2, 0x7f070100

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1140
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1142
    :cond_5
    const v2, 0x7f0806e5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1143
    const v2, 0x7f0806e6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1144
    const v2, 0x7f080428

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1145
    const v2, 0x7f0806e7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1146
    const v2, 0x7f0806e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1147
    const v2, 0x7f0806e9

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1148
    const v2, 0x7f0806ea

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1149
    const v2, 0x7f0806eb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1150
    const v2, 0x7f0806ec

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1151
    const v2, 0x7f0806ed

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153
    const v2, 0x7f08073d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 1154
    .local v20, "memo":Landroid/view/View;
    if-eqz v20, :cond_6

    .line 1155
    const v2, 0x7f070100

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1156
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1159
    :cond_6
    const/4 v10, 0x0

    .line 1160
    .local v10, "height":I
    const/16 v26, 0x0

    .line 1161
    .local v26, "visibleCount":I
    const/16 v18, 0x0

    .local v18, "index":I
    :goto_2
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_8

    .line 1162
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_7

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_7

    .line 1163
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->buildDrawingCache()V

    .line 1164
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v23, v26

    .line 1165
    aget-object v2, v23, v26

    if-eqz v2, :cond_7

    .line 1166
    add-int/lit8 v27, v26, 0x1

    .end local v26    # "visibleCount":I
    .local v27, "visibleCount":I
    aget-object v2, v23, v26

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    add-int/2addr v10, v2

    move/from16 v26, v27

    .line 1161
    .end local v27    # "visibleCount":I
    .restart local v26    # "visibleCount":I
    :cond_7
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 1169
    :cond_8
    const/4 v2, 0x0

    aget-object v2, v23, v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    const/4 v5, 0x0

    aget-object v5, v23, v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v2, v10, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1170
    .local v6, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v21, Landroid/graphics/Canvas;

    move-object/from16 v0, v21

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1171
    .local v21, "newCanvas":Landroid/graphics/Canvas;
    const/4 v10, 0x0

    .line 1172
    const/16 v18, 0x0

    :goto_3
    move/from16 v0, v18

    move/from16 v1, v26

    if-ge v0, v1, :cond_9

    .line 1173
    aget-object v2, v23, v18

    const/4 v5, 0x0

    int-to-float v7, v10

    const/4 v8, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v5, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1174
    aget-object v2, v23, v18

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    add-int/2addr v10, v2

    .line 1172
    add-int/lit8 v18, v18, 0x1

    goto :goto_3

    .line 1187
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mExercise:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I

    move-result v4

    .line 1188
    .restart local v4    # "title":I
    const-wide/16 v8, -0x1

    const/4 v11, 0x0

    move-object/from16 v5, p0

    move v7, v4

    invoke-static/range {v5 .. v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareView(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;)V

    .line 1189
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v5, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v7, "DS01"

    invoke-static {v2, v5, v7}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public showTabLayout()V
    .locals 2

    .prologue
    .line 246
    const v0, 0x7f080732

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 247
    return-void
.end method

.method public switchToFragment(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1, "currentFragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "previousFragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 797
    if-ne p1, p2, :cond_0

    .line 816
    :goto_0
    return-void

    .line 801
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 802
    .local v0, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v1, :cond_1

    .line 803
    const/16 v1, 0x1003

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 807
    :cond_1
    if-eqz p2, :cond_2

    .line 808
    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 809
    :cond_2
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_3

    .line 810
    const v1, 0x7f0806f4

    const-string v2, "exercise_pro_details_fragment"

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 815
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 813
    :cond_3
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method

.method protected switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;)V
    .locals 2
    .param p1, "tab"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .prologue
    .line 317
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->rowId:J

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->switchToTab(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;J)V

    .line 318
    return-void
.end method
