.class Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceDialogButtonController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateServiceDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceDialogButtonController;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$3;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;

    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getIntentForHealthServiceUpdate()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->finish()V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;->finish()V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
