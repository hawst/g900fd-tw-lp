.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;
.super Ljava/lang/Object;
.source "UnitSettingActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mWearableDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0

    .prologue
    .line 755
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 9
    .param p1, "error"    # I

    .prologue
    .line 769
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v5}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1400(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x2711

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v1

    .line 770
    .local v1, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 806
    .end local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_0
    :goto_0
    return-void

    .line 775
    .restart local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_1
    const/4 v4, 0x0

    .line 776
    .local v4, "isFound":Z
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 778
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v5

    const/16 v6, 0x2724

    if-eq v5, v6, :cond_3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v5

    const/16 v6, 0x2726

    if-eq v5, v6, :cond_3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v5

    const/16 v6, 0x2723

    if-ne v5, v6, :cond_2

    .line 782
    :cond_3
    const/4 v4, 0x1

    .line 783
    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;->mWearableDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 788
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :cond_4
    if-eqz v4, :cond_0

    .line 793
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;->mWearableDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    new-instance v6, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$WrapperListener;

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$9;->mWearableDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {v6, v7}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$WrapperListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 795
    .end local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "isFound":Z
    :catch_0
    move-exception v5

    goto :goto_0

    .line 800
    :catch_1
    move-exception v2

    .line 802
    .local v2, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 799
    .end local v2    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_2
    move-exception v5

    goto :goto_0

    .line 798
    :catch_3
    move-exception v5

    goto :goto_0

    .line 797
    :catch_4
    move-exception v5

    goto :goto_0

    .line 796
    :catch_5
    move-exception v5

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 0
    .param p1, "error"    # I

    .prologue
    .line 763
    return-void
.end method
