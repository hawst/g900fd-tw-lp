.class Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;
.super Ljava/lang/Object;
.source "AssessmentView.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->showRangeDialog(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V
    .locals 0

    .prologue
    .line 862
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 8
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 867
    const v2, 0x7f080836

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 868
    .local v0, "dialogText":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090846

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 869
    const v2, 0x7f080837

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 870
    .local v1, "rangeText":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->mAssessmentWeightView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->access$800(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getWeightUnit()I

    move-result v2

    const v3, 0x1fbd1

    if-ne v2, v3, :cond_0

    .line 871
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090841

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/16 v5, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 876
    :goto_0
    return-void

    .line 873
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090844

    new-array v4, v5, [Ljava/lang/Object;

    const-string v5, "4.4"

    aput-object v5, v4, v6

    const-string v5, "1102.3"

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
