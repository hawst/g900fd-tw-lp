.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;
.super Landroid/database/ContentObserver;
.source "WalkingMateActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WearableStatusObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "observerHandler"    # Landroid/os/Handler;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .line 326
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 327
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 331
    const-string v0, "WearableStatusObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WearableStatusObserver.onChange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 334
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 338
    const-string v0, "WearableStatusObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WearableStatusObserver.onChange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") URI = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->uiThreadHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 353
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 354
    return-void
.end method
