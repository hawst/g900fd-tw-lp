.class Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;
.super Ljava/lang/Object;
.source "ListChooseDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getGeneralParentContainer(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

.field final synthetic val$parentContent:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;->val$parentContent:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 421
    const/4 v3, 0x2

    new-array v4, v3, [I

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;->val$parentContent:Landroid/view/View;

    sget v6, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    aput v3, v4, v5

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;->val$parentContent:Landroid/view/View;

    sget v5, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    aput v3, v4, v7

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->getMax([I)I

    move-result v2

    .line 422
    .local v2, "maxLinesAmount":I
    if-le v2, v7, :cond_0

    .line 424
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;->val$parentContent:Landroid/view/View;

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_ok_buttons_container:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 425
    .local v1, "cancelOkButtonsContainer":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 426
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 427
    .local v0, "buttonsContainerLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$8;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->ok_cancel_buttons_double_line_height:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 428
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 429
    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->swapWidthAndHeight(Landroid/view/View;)V

    .line 430
    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->swapWidthAndHeight(Landroid/view/View;)V

    .line 431
    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->btns_margin:I

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->swapWidthAndHeight(Landroid/view/View;)V

    .line 433
    .end local v0    # "buttonsContainerLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "cancelOkButtonsContainer":Landroid/widget/LinearLayout;
    :cond_0
    return-void
.end method
