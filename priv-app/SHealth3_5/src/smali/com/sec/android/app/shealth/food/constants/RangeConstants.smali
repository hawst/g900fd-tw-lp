.class public final Lcom/sec/android/app/shealth/food/constants/RangeConstants;
.super Ljava/lang/Object;
.source "RangeConstants.java"


# static fields
.field public static final CUSTOM_FOOD_MAX_CALORIE_VALUE:I = 0x1869f

.field public static final CUSTOM_FOOD_MAX_NUTRIENT_VALUE:F = 500.0f

.field public static final CUSTOM_FOOD_MIN_CALORIE_VALUE:I = 0x0

.field public static final CUSTOM_FOOD_MIN_CALORIE_VALUE_TOAST:I = 0x2

.field public static final CUSTOM_FOOD_MIN_NUTRIENT_VALUE:I = 0x0

.field public static final DEFAULT_CALORIE_TO_BURN_PER:I = 0x294

.field public static final FAVORITES_MAX_NUMBER:I = 0x1e

.field public static final FREQUENT_MAX_NUMBER:I = 0x1e

.field public static final LOW_INTAKE_PROGRESS:F = 0.1f

.field public static final MAX_CALORIES_TO_DISPLAY:I = 0x1869f

.field public static final MAX_DEVIATION_FOR_MEDAL_IN_PERCENT:I = 0xa

.field public static final MAX_DISPLAYED_IMAGES_COUNT:I = 0x4

.field public static final MAX_GOAL_CALORIES:I = 0x1869f

.field public static final MAX_IMAGES_AMOUNT:I = 0x9

.field public static final MAX_NUMBER_OF_MEAL_ITEM:I = 0xf

.field public static final MAX_NUMBER_OF_MY_MEAL:I = 0x1e

.field public static final MAX_PROGRESS_FOR_MEDAL:F = 1.1f

.field public static final MEAL_CALORIES_MAX:I = 0x1869f

.field public static final MIN_CALORIES_TO_DISPLAY:I = 0x0

.field public static final MIN_GOAL_BURNT_CALORIES:I = 0xa

.field public static final MIN_GOAL_CALORIES:I = 0x1

.field public static final MIN_PROGRESS_FOR_MEDAL:F = 0.9f

.field public static final MY_FOOD_MAX_NUMBER:I = 0x1e

.field public static final PORTION_SIZE_INPUT_MAX_VALUE:F = 9999.0f

.field public static final PORTION_SIZE_INPUT_MIN_VALUE:F = 0.1f

.field public static final PORTION_SIZE_INPUT_MIN_VALUE_GRAM:F = 1.0f

.field public static final PORTION_SIZE_INPUT_MIN_VALUE_KCAL:F = 0.1f

.field public static final QUICK_INPUT_MAX_CALORIE_VALUE:I = 0x270f

.field public static final QUICK_INPUT_MIN_CALORIE_VALUE:I = 0x0

.field public static final SET_QUICK_INPUT_MAX_CALORIE_VALUE:I = 0x1388

.field public static final SET_QUICK_INPUT_MIN_CALORIE_VALUE:I = 0x2


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    return-void
.end method
