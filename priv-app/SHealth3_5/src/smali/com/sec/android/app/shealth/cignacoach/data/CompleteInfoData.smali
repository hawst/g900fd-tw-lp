.class public Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;
.super Ljava/lang/Object;
.source "CompleteInfoData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBadgeDesc:Ljava/lang/String;

.field private mBadgeIds:[I

.field private mBadgeImage:Ljava/lang/String;

.field private mBadgeName:Ljava/lang/String;

.field private mBadgeSize:I

.field private mGoalFrequency:Ljava/lang/String;

.field private mGoalId:I

.field private mGoalProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

.field private mGoalTitle:Ljava/lang/String;

.field private mMissionFrequency:I

.field private mMissionId:I

.field private mMissionProgressFrequency:I

.field private mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field private mMissionRepeatable:Z

.field private mMissionTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionRepeatable:Z

    .line 19
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeSize:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionRepeatable:Z

    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->readFromParacel(Landroid/os/Parcel;)V

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData$1;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static valueOf(Lcom/sec/android/app/shealth/cignacoach/data/MissionData;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;
    .locals 2
    .param p0, "missionData"    # Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .prologue
    .line 138
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;-><init>()V

    .line 139
    .local v0, "completeInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setGoalId(I)V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getSelectFrequencyDays()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setMissionFrequency(I)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setMissionId(I)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getTitleString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setMissionTitle(Ljava/lang/String;)V

    .line 144
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public getGoalFrequency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalFrequency:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalId()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalId:I

    return v0
.end method

.method public getGoalTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionFrequency()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionFrequency:I

    return v0
.end method

.method public getMissionId()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionId:I

    return v0
.end method

.method public getMissionProgressFrequency()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressFrequency:I

    return v0
.end method

.method public getMissionProgressStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    return-object v0
.end method

.method public getMissionTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionTitle:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParacel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalId:I

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionId:I

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionFrequency:I

    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressFrequency:I

    .line 176
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeSize:I

    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionTitle:Ljava/lang/String;

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalTitle:Ljava/lang/String;

    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalFrequency:Ljava/lang/String;

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeName:Ljava/lang/String;

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeDesc:Ljava/lang/String;

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeImage:Ljava/lang/String;

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeIds:[I

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionRepeatable:Z

    .line 187
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBadgeSize(I)V
    .locals 0
    .param p1, "badgeSize"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeSize:I

    .line 111
    return-void
.end method

.method public setGoalFrequency(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalFrequency"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalFrequency:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setGoalId(I)V
    .locals 0
    .param p1, "goalId"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalId:I

    .line 31
    return-void
.end method

.method public setGoalProgressStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;)V
    .locals 0
    .param p1, "goalProgressStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    .line 79
    return-void
.end method

.method public setGoalTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalTitle"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalTitle:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setMissionFrequency(I)V
    .locals 0
    .param p1, "missionFrequency"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionFrequency:I

    .line 63
    return-void
.end method

.method public setMissionId(I)V
    .locals 0
    .param p1, "missionId"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionId:I

    .line 55
    return-void
.end method

.method public setMissionProgressFrequency(I)V
    .locals 0
    .param p1, "missionProgressFrequency"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressFrequency:I

    .line 71
    return-void
.end method

.method public setMissionProgressStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V
    .locals 0
    .param p1, "missionProgressStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 87
    return-void
.end method

.method public setMissionRepeatable(Z)V
    .locals 0
    .param p1, "missionRepeatable"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionRepeatable:Z

    .line 103
    return-void
.end method

.method public setMissionTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMissionTitle"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionTitle:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 155
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionFrequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressFrequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 158
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionProgressStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mGoalFrequency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeDesc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeImage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mBadgeIds:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 168
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->mMissionRepeatable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 169
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
