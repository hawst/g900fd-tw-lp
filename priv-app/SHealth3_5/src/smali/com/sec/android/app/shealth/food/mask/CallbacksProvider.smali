.class public abstract Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;
.super Ljava/lang/Object;
.source "CallbacksProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        "Callback:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mCallbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TKey;TCallback;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    .local p0, "this":Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;, "Lcom/sec/android/app/shealth/food/mask/CallbacksProvider<TKey;TCallback;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->mCallbacks:Ljava/util/HashMap;

    .line 28
    return-void
.end method


# virtual methods
.method public addCallBack(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;TCallback;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;, "Lcom/sec/android/app/shealth/food/mask/CallbacksProvider<TKey;TCallback;>;"
    .local p1, "key":Ljava/lang/Object;, "TKey;"
    .local p2, "callback":Ljava/lang/Object;, "TCallback;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public abstract onRemoveCallback(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCallback;)V"
        }
    .end annotation
.end method

.method public removeCallback(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;, "Lcom/sec/android/app/shealth/food/mask/CallbacksProvider<TKey;TCallback;>;"
    .local p1, "key":Ljava/lang/Object;, "TKey;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->onRemoveCallback(Ljava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public removeCallbacks()V
    .locals 4

    .prologue
    .line 58
    .local p0, "this":Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;, "Lcom/sec/android/app/shealth/food/mask/CallbacksProvider<TKey;TCallback;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 59
    .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<TKey;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 60
    .local v1, "key":Ljava/lang/Object;, "TKey;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->onRemoveCallback(Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    .end local v1    # "key":Ljava/lang/Object;, "TKey;"
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 63
    return-void
.end method
