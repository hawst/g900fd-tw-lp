.class Lcom/sec/android/app/shealth/uv/UvActivity$12;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 701
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$12;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 705
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0804ee

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$12;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 706
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$12;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$12;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1100(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 710
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 706
    goto :goto_0

    .line 707
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f080581

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$12;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 708
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$12;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$12;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$1200(Lcom/sec/android/app/shealth/uv/UvActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
