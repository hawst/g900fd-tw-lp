.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;
.super Ljava/lang/Object;
.source "ProfileInformationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckBoxCheckedListener"
.end annotation


# instance fields
.field mAddedTextView:Landroid/widget/TextView;

.field mAddedTextView10:Landroid/widget/TextView;

.field mAddedTextView11:Landroid/widget/TextView;

.field mAddedTextView12:Landroid/widget/TextView;

.field mAddedTextView13:Landroid/widget/TextView;

.field mAddedTextView14:Landroid/widget/TextView;

.field mAddedTextView2:Landroid/widget/TextView;

.field mAddedTextView3:Landroid/widget/TextView;

.field mAddedTextView4:Landroid/widget/TextView;

.field mAddedTextView5:Landroid/widget/TextView;

.field mAddedTextView6:Landroid/widget/TextView;

.field mAddedTextView7:Landroid/widget/TextView;

.field mAddedTextView8:Landroid/widget/TextView;

.field mAddedTextView9:Landroid/widget/TextView;

.field private mAnimatedContainerView:Landroid/view/View;

.field private final mAnimationDuration:I

.field private final mCheckBoxId:I

.field private mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

.field mDetailTextView:Landroid/widget/TextView;

.field private mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

.field private mHeight:I

.field private mInnerContainer:Landroid/widget/LinearLayout;

.field private mTitle:Landroid/widget/TextView;

.field private mUpperContainer:Landroid/view/View;

.field private mWithoutAnimation:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;Landroid/view/View;I)V
    .locals 7
    .param p2, "container"    # Landroid/view/View;
    .param p3, "checkBoxId"    # I

    .prologue
    const v3, 0x7f080891

    const v2, 0x7f08088f

    const v6, 0x7f09085f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 188
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimationDuration:I

    .line 189
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f0807c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f0807c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView:Landroid/widget/TextView;

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f0807c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView2:Landroid/widget/TextView;

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView3:Landroid/widget/TextView;

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080890

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView4:Landroid/widget/TextView;

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView5:Landroid/widget/TextView;

    .line 201
    if-ne p3, v5, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView3:Landroid/widget/TextView;

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView5:Landroid/widget/TextView;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080892

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView6:Landroid/widget/TextView;

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080893

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView7:Landroid/widget/TextView;

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080894

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView8:Landroid/widget/TextView;

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080895

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView9:Landroid/widget/TextView;

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080896

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView10:Landroid/widget/TextView;

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080897

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView11:Landroid/widget/TextView;

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080898

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView12:Landroid/widget/TextView;

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f080899

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView13:Landroid/widget/TextView;

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f08089a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView14:Landroid/widget/TextView;

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView3:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1.2"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView5:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1.375"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView7:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1.55"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView9:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1.725"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView11:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1.9"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    :cond_0
    iput p3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    .line 221
    const v0, 0x7f080632

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    .line 222
    const v0, 0x7f08004d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mHeight:I

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 228
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;Landroid/view/View;IZ)V
    .locals 0
    .param p2, "container"    # Landroid/view/View;
    .param p3, "checkBoxId"    # I
    .param p4, "isWithoutAnimation"    # Z

    .prologue
    .line 231
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;Landroid/view/View;I)V

    .line 232
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    .line 233
    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method private initializeDropDownAnimation()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1f4

    const v3, 0x7f0a0926

    .line 271
    const/4 v0, 0x0

    .line 273
    .local v0, "animatedViewHeight":I
    iget v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-nez v1, :cond_0

    .line 274
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView2:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView3:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView4:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView5:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x6

    add-int v0, v1, v2

    .line 303
    :goto_0
    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener$1;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;Landroid/view/View;IZ)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->setDuration(J)V

    .line 317
    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener$2;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;Landroid/view/View;IZ)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->setDuration(J)V

    .line 337
    return-void

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView2:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView3:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView4:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView5:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView6:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView7:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView8:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView9:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView10:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView11:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView12:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView13:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAddedTextView14:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x9

    add-int v0, v1, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v8, 0x7f09020b

    const v7, 0x7f0804e6

    const/4 v3, 0x0

    .line 238
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 239
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    if-eqz v2, :cond_2

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const/4 v4, -0x2

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 242
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    .line 262
    :cond_0
    return-void

    :cond_1
    move v2, v3

    .line 238
    goto :goto_0

    .line 246
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    if-nez v2, :cond_3

    .line 247
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->initializeDropDownAnimation()V

    .line 248
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    :goto_1
    invoke-virtual {v2, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 250
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 252
    .local v1, "mGuideListCurrentVisibility":I
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v0, v4, :cond_4

    const/16 v1, 0x8

    .end local v1    # "mGuideListCurrentVisibility":I
    :cond_4
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 254
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901ef

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 250
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 248
    .end local v0    # "i":I
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    goto/16 :goto_1

    .line 257
    .restart local v0    # "i":I
    .restart local v1    # "mGuideListCurrentVisibility":I
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v0, v4, :cond_7

    move v1, v3

    .end local v1    # "mGuideListCurrentVisibility":I
    :cond_7
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090217

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
