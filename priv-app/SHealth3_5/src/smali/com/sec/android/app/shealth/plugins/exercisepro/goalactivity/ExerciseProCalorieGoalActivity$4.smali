.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;
.super Ljava/lang/Object;
.source "ExerciseProCalorieGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->showAbnormalGoalWarningPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v2, 0x0

    .line 212
    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v0

    const v1, 0x461c1800    # 9990.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    const/16 v1, 0x2706

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;I)I

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    .line 220
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 221
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;I)I

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->calorieInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->goalValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProCalorieGoalActivity;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    goto :goto_0
.end method
