.class public Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;
.super Ljava/lang/Object;
.source "FocusUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$1;,
        Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$OnKeyListenerConsumingKeyEventIfNoFocusChangeWillAppear;,
        Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$ViewIdUniquizeHelper;,
        Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$SHealthFocusabilityComparator;,
        Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$OnFocusChangeListenerLoggingCurrentFocus;,
        Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$VisibleChildrenProvider;
    }
.end annotation


# static fields
.field public static final ALLOWED_VERTICAL_DEVIATION_OF_CENTER_FOR_FOCUSABLES_IN_SAME_LINE_IN_PX:I = 0x46

.field private static final TAG:Ljava/lang/String;

.field public static final UNSPECIFIED_VIEW_ID:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V
    .locals 5
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "motionEventSplittingEnabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->setMotionEventSplittingEnabled(Z)V

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 52
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 53
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->isFocusable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 57
    instance-of v4, v2, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$VisibleChildrenProvider;

    if-eqz v4, :cond_1

    .line 58
    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$VisibleChildrenProvider;

    .end local v2    # "view":Landroid/view/View;
    invoke-interface {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$VisibleChildrenProvider;->getVisibleChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 59
    .local v3, "visibleChild":Landroid/view/View;
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 61
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "visibleChild":Landroid/view/View;
    .restart local v2    # "view":Landroid/view/View;
    :cond_1
    instance-of v4, v2, Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    .line 62
    check-cast v2, Landroid/view/ViewGroup;

    .end local v2    # "view":Landroid/view/View;
    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->addAllFocusableChildrenToListAndDisableMotionEventSplitting(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 51
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_3
    return-void
.end method

.method public static addAllFocusableChildrenToListAndDisableMotionEventSplitting(Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 1
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "focusableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->addAllFocusableChildrenToList(Landroid/view/ViewGroup;Ljava/util/List;Z)V

    .line 47
    return-void
.end method

.method public static addAllFocusablesToList(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "viewsToAdd":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    .local p1, "focusableViews":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 70
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public static checkAndCleanUpFocusablesList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "viewsListToCheck":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 85
    .local v2, "viewsToBeRemoved":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 86
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->isViewReallyVisibleToUser(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 87
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 89
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    invoke-interface {p0, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 90
    return-void
.end method

.method public static clearDirectionalFocuses(Landroid/view/View;Z)V
    .locals 1
    .param p0, "viewToLooseDirectionalFocuses"    # Landroid/view/View;
    .param p1, "setToSelf"    # Z

    .prologue
    .line 206
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v0

    .line 207
    .local v0, "viewId":I
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 208
    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 209
    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 210
    invoke-virtual {p0, v0}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 211
    return-void

    .line 206
    .end local v0    # "viewId":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static clearDirectionalFocuses(Ljava/util/Collection;Z)V
    .locals 3
    .param p1, "setToSelf"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 214
    .local p0, "viewsToLooseDirectionalFocuses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 215
    .local v1, "view":Landroid/view/View;
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->clearDirectionalFocuses(Landroid/view/View;Z)V

    goto :goto_0

    .line 217
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public static fixAndroidIdiotism(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p0, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 154
    .local v0, "focusable":Landroid/view/View;
    instance-of v3, v0, Landroid/widget/ListView;

    if-nez v3, :cond_1

    instance-of v3, v0, Landroid/widget/ScrollView;

    if-nez v3, :cond_1

    instance-of v3, v0, Landroid/widget/NumberPicker;

    if-eqz v3, :cond_2

    .line 157
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 158
    :cond_2
    instance-of v3, v0, Landroid/widget/ListView;

    if-eqz v3, :cond_0

    move-object v2, v0

    .line 159
    check-cast v2, Landroid/widget/ListView;

    .line 160
    .local v2, "listView":Landroid/widget/ListView;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    goto :goto_0

    .line 163
    .end local v0    # "focusable":Landroid/view/View;
    .end local v2    # "listView":Landroid/widget/ListView;
    :cond_3
    return-void
.end method

.method public static focusWillChange(Landroid/view/View;I)Z
    .locals 6
    .param p0, "v"    # Landroid/view/View;
    .param p1, "keyCode"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 336
    if-nez p0, :cond_1

    .line 340
    :cond_0
    :goto_0
    return v3

    .line 337
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v2

    .line 338
    .local v2, "viewId":I
    invoke-virtual {p0}, Landroid/view/View;->getNextFocusUpId()I

    move-result v1

    .line 339
    .local v1, "nextFocusUpId":I
    invoke-virtual {p0}, Landroid/view/View;->getNextFocusDownId()I

    move-result v0

    .line 340
    .local v0, "nextFocusDownId":I
    const/16 v4, 0x13

    if-ne p1, v4, :cond_2

    if-eq v1, v5, :cond_2

    if-eq v2, v1, :cond_3

    :cond_2
    const/16 v4, 0x14

    if-ne p1, v4, :cond_0

    if-eq v0, v5, :cond_0

    if-ne v2, v0, :cond_0

    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;
    .locals 1
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 41
    .local v0, "focusableChildren":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->addAllFocusableChildrenToListAndDisableMotionEventSplitting(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 42
    return-object v0
.end method

.method public static getFocusLines(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p0, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v10, 0x0

    .line 102
    if-nez p0, :cond_0

    .line 103
    new-instance v8, Ljava/lang/NullPointerException;

    const-string v9, "Focusables list is null."

    invoke-direct {v8, v9}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 104
    :cond_0
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4, p0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 105
    .local v4, "focusablesCopy":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Landroid/view/View;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v3, "focusLinesList":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->checkAndCleanUpFocusablesList(Ljava/util/List;)V

    .line 107
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 108
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->sortFocusablesList(Ljava/util/List;)V

    .line 109
    const/4 v5, 0x0

    .line 111
    .local v5, "lastLineIndex":I
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    invoke-virtual {v4}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 113
    .local v6, "mostTopMostLeftFocusable":Landroid/view/View;
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    :goto_0
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 115
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 116
    .local v7, "viewsInCurrentLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 117
    .local v1, "currentView":Landroid/view/View;
    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getViewCenterY(Landroid/view/View;)I

    move-result v0

    .line 118
    .local v0, "actualLineY":I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getViewCenterY(Landroid/view/View;)I

    move-result v2

    .line 119
    .local v2, "currentViewY":I
    sub-int v8, v2, v0

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    const/16 v9, 0x46

    if-ge v8, v9, :cond_1

    .line 121
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 125
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    .end local v0    # "actualLineY":I
    .end local v1    # "currentView":Landroid/view/View;
    .end local v2    # "currentViewY":I
    .end local v5    # "lastLineIndex":I
    .end local v6    # "mostTopMostLeftFocusable":Landroid/view/View;
    .end local v7    # "viewsInCurrentLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_2
    return-object v3
.end method

.method public static getRectDescribingViewFully(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 149
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getRectDescribingViewFully(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private static getRectDescribingViewFully(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "tempRect"    # Landroid/graphics/Rect;

    .prologue
    .line 142
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getRelativeLeft(Landroid/view/View;)I

    move-result v0

    .line 143
    .local v0, "relativeLeft":I
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getRelativeTop(Landroid/view/View;)I

    move-result v1

    .line 144
    .local v1, "relativeTop":I
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 145
    return-object p1
.end method

.method private static getRelativeLeft(Landroid/view/View;)I
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 220
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 221
    .local v0, "viewParent":Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 222
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 224
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getRelativeLeft(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method private static getRelativeTop(Landroid/view/View;)I
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 228
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 229
    .local v0, "viewParent":Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 230
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    .line 232
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getRelativeTop(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method private static getViewCenterY(Landroid/view/View;)I
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 134
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getViewCenterY(Landroid/view/View;Landroid/graphics/Rect;)I

    move-result v0

    return v0
.end method

.method private static getViewCenterY(Landroid/view/View;Landroid/graphics/Rect;)I
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "tempRect"    # Landroid/graphics/Rect;

    .prologue
    .line 138
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getRectDescribingViewFully(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    return v0
.end method

.method public static isViewReallyVisibleToUser(Landroid/view/View;)Z
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 93
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    :goto_0
    return v3

    .line 94
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 95
    .local v0, "checkRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 96
    iget v4, v0, Landroid/graphics/Rect;->left:I

    if-nez v4, :cond_1

    iget v4, v0, Landroid/graphics/Rect;->right:I

    if-nez v4, :cond_1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    if-nez v4, :cond_1

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    if-nez v4, :cond_1

    move v1, v2

    .line 98
    .local v1, "viewOrOneOfParentsVisibilityIsNotVisible":Z
    :goto_1
    if-nez v1, :cond_2

    :goto_2
    move v3, v2

    goto :goto_0

    .end local v1    # "viewOrOneOfParentsVisibilityIsNotVisible":Z
    :cond_1
    move v1, v3

    .line 96
    goto :goto_1

    .restart local v1    # "viewOrOneOfParentsVisibilityIsNotVisible":Z
    :cond_2
    move v2, v3

    .line 98
    goto :goto_2
.end method

.method public static refreshFocusables(Landroid/view/View;)V
    .locals 10
    .param p0, "content"    # Landroid/view/View;

    .prologue
    .line 236
    instance-of v8, p0, Landroid/view/ViewGroup;

    if-eqz v8, :cond_4

    .line 237
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "content":Landroid/view/View;
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v3

    .line 238
    .local v3, "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->fixAndroidIdiotism(Ljava/util/List;)V

    .line 239
    const/4 v8, 0x1

    invoke-static {v3, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->clearDirectionalFocuses(Ljava/util/Collection;Z)V

    .line 240
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getFocusLines(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 241
    .local v1, "focusLines":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v4, v8, :cond_0

    .line 242
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    add-int/lit8 v9, v4, 0x1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->setRequiredDirectionalFocuses(Ljava/util/List;Ljava/util/List;)V

    .line 243
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->setInnerLinkFocuses(Ljava/util/List;)V

    .line 241
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 245
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 246
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->setInnerLinkFocuses(Ljava/util/List;)V

    .line 247
    :cond_1
    new-instance v7, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$OnKeyListenerConsumingKeyEventIfNoFocusChangeWillAppear;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$OnKeyListenerConsumingKeyEventIfNoFocusChangeWillAppear;-><init>()V

    .line 248
    .local v7, "onKeyListener":Landroid/view/View$OnKeyListener;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 249
    .local v0, "focusLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 250
    .local v2, "focusable":Landroid/view/View;
    invoke-virtual {v2, v7}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_1

    .line 253
    .end local v0    # "focusLine":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v2    # "focusable":Landroid/view/View;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_3
    sget-object v8, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->TAG:Ljava/lang/String;

    const-string v9, "focusables refreshed"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    .end local v1    # "focusLines":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Landroid/view/View;>;>;"
    .end local v3    # "focusables":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v4    # "i":I
    .end local v7    # "onKeyListener":Landroid/view/View$OnKeyListener;
    :cond_4
    return-void
.end method

.method public static setInnerLinkFocuses(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p0, "sortedViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 167
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 168
    add-int/lit8 v1, v0, 0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_0
    return-void
.end method

.method public static setRequiredDirectionalFocuses(Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, "upperContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p1, "lowerContainerSorted":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-nez p1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    const/4 v8, 0x0

    :try_start_0
    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    .line 177
    .local v7, "mostLeftViewInUpperContainer":Landroid/view/View;
    const/4 v8, 0x0

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 178
    .local v6, "mostLeftViewInLowerContainer":Landroid/view/View;
    if-eqz v7, :cond_0

    .line 179
    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v5

    .line 180
    .local v5, "mostLeftUpperContainerButtonId":I
    if-eqz v6, :cond_2

    .line 181
    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v4

    .line 182
    .local v4, "mostLeftLowerContainerButtonId":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 184
    .local v0, "currentlyVisibleUpperContainerButton":Landroid/view/View;
    :try_start_1
    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 185
    :catch_0
    move-exception v1

    .line 186
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v8, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 200
    .end local v0    # "currentlyVisibleUpperContainerButton":Landroid/view/View;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "mostLeftLowerContainerButtonId":I
    .end local v5    # "mostLeftUpperContainerButtonId":I
    .end local v6    # "mostLeftViewInLowerContainer":Landroid/view/View;
    .end local v7    # "mostLeftViewInUpperContainer":Landroid/view/View;
    :catch_1
    move-exception v1

    .line 201
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v8, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->TAG:Ljava/lang/String;

    const-string v9, "Cannot set required directional focuses"

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 190
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v5    # "mostLeftUpperContainerButtonId":I
    .restart local v6    # "mostLeftViewInLowerContainer":Landroid/view/View;
    .restart local v7    # "mostLeftViewInUpperContainer":Landroid/view/View;
    :cond_2
    :try_start_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-eqz v8, :cond_0

    .line 191
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 193
    .local v2, "focusableViewInLowerContainer":Landroid/view/View;
    :try_start_4
    invoke-virtual {v2, v5}, Landroid/view/View;->setNextFocusUpId(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 194
    :catch_2
    move-exception v1

    .line 195
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v8, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2
.end method

.method public static sortFocusablesList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "viewsListToSort":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$SHealthFocusabilityComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$SHealthFocusabilityComparator;-><init>(Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$1;)V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 81
    return-void
.end method

.method public static uniquizeViewsIds(Landroid/view/View;I)I
    .locals 1
    .param p0, "content"    # Landroid/view/View;
    .param p1, "startFromId"    # I

    .prologue
    .line 280
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->uniquizeViewsIds(Landroid/view/View;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public static uniquizeViewsIds(Landroid/view/View;Ljava/util/Collection;I)I
    .locals 7
    .param p0, "content"    # Landroid/view/View;
    .param p2, "startFromId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "additionalFocusables":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    const/4 v1, 0x0

    .line 260
    .local v1, "focusablesList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :try_start_0
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "content":Landroid/view/View;
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v1

    .line 261
    if-eqz v1, :cond_0

    .line 262
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 263
    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->addAllFocusablesToList(Ljava/util/Collection;Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :cond_0
    :goto_0
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 269
    .local v4, "viewsIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_4

    .line 270
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 271
    .local v0, "focusable":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 272
    :cond_2
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "startFromId":I
    .local v3, "startFromId":I
    invoke-virtual {v0, p2}, Landroid/view/View;->setId(I)V

    move p2, v3

    .end local v3    # "startFromId":I
    .restart local p2    # "startFromId":I
    goto :goto_1

    .line 274
    .end local v0    # "focusable":Landroid/view/View;
    :cond_3
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->TAG:Ljava/lang/String;

    const-string v6, "Focusables IDs were uniquized"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    return p2

    .line 265
    .end local v4    # "viewsIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v5

    goto :goto_0
.end method
