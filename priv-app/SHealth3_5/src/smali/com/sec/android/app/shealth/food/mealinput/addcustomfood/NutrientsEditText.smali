.class public Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;
.super Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
.source "NutrientsEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;
    }
.end annotation


# static fields
.field private static mSystemNumberSeparator:Ljava/lang/String; = null

.field private static final sDefaultNumberSeparator:Ljava/lang/String; = "."


# instance fields
.field private final DECIMAL_DEFAULT:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mSystemNumberSeparator:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;-><init>(Landroid/content/Context;)V

    .line 22
    const-string v0, "0."

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->DECIMAL_DEFAULT:Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mContext:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->initialize()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-string v0, "0."

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->DECIMAL_DEFAULT:Ljava/lang/String;

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mContext:Landroid/content/Context;

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->initialize()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    const-string v0, "0."

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->DECIMAL_DEFAULT:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mContext:Landroid/content/Context;

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->initialize()V

    .line 45
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mSystemNumberSeparator:Ljava/lang/String;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$NutrientEditTextWatcher;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$1;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 50
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setOnSpecialCharacterInput(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;)V

    .line 60
    return-void
.end method


# virtual methods
.method public getValueString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v2, "."

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEditorAction(I)V
    .locals 1
    .param p1, "actionCode"    # I

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->onEditorAction(I)V

    .line 95
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->requestFocus()Z

    .line 99
    :cond_0
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "value":Ljava/lang/String;
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 68
    if-nez p1, :cond_0

    .line 70
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/NutrientsEditText;->setText(Ljava/lang/CharSequence;)V

    .line 75
    :cond_0
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method
