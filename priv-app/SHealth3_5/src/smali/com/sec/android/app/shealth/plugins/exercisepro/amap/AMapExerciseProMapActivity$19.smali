.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;
.super Ljava/lang/Object;
.source "AMapExerciseProMapActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->animate(Landroid/widget/ImageView;[IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

.field final synthetic val$forever:Z

.field final synthetic val$imageIndex:I

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$images:[I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;[IILandroid/widget/ImageView;Z)V
    .locals 0

    .prologue
    .line 1988
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$images:[I

    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$imageIndex:I

    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$imageView:Landroid/widget/ImageView;

    iput-boolean p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$forever:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 5
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1990
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$images:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$imageIndex:I

    if-le v0, v1, :cond_1

    .line 1991
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$imageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$images:[I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$imageIndex:I

    add-int/lit8 v3, v3, 0x1

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$forever:Z

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->animate(Landroid/widget/ImageView;[IIZ)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;Landroid/widget/ImageView;[IIZ)V

    .line 1998
    :cond_0
    :goto_0
    return-void

    .line 1994
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$forever:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1995
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$imageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$images:[I

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$19;->val$forever:Z

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->animate(Landroid/widget/ImageView;[IIZ)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$4300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;Landroid/widget/ImageView;[IIZ)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2001
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 2004
    return-void
.end method
