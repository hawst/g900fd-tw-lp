.class public Lcom/sec/android/app/shealth/stress/StressActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "StressActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# static fields
.field public static final ACTIONBAR_LOG:I = 0x0

.field public static final ACTIONBAR_SHARE:I = 0x1

.field private static final INFO_DIAG_TAG:Ljava/lang/String; = "stm_information_dialog"

.field public static isDialogDissmised:Z


# instance fields
.field private InfoDiagContentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field animationFadeIn:Landroid/view/animation/AnimationSet;

.field animationFadeOut:Landroid/view/animation/AnimationSet;

.field private backFinish:Z

.field private callFromHome:Z

.field fadeIn:Landroid/view/animation/Animation;

.field fadeOut:Landroid/view/animation/Animation;

.field private informationImages:[I

.field private informationImagesArray:[Ljava/lang/String;

.field isImage1Found:Z

.field isImage2Found:Z

.field public isMeasureCompleted:Z

.field private isShownPopup:Z

.field private mAnimationViewInfo0:Landroid/widget/ImageView;

.field private mAnimationViewInfo1:Landroid/widget/ImageView;

.field mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

.field private mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mDialogHandler:Landroid/os/Handler;

.field private mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

.field private mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

.field private mInfoView:Landroid/view/View;

.field private mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mIsActivityOnConfigChanged:Z

.field private mShowAgain:Landroid/widget/CheckBox;

.field private mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

.field private mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

.field private mTextView1:Landroid/widget/TextView;

.field private mTextView11:Landroid/widget/TextView;

.field private mTextView2:Landroid/widget/TextView;

.field private mTextView22:Landroid/widget/TextView;

.field private mTextView3:Landroid/widget/TextView;

.field private mTextView33:Landroid/widget/TextView;

.field private mTextView4:Landroid/widget/TextView;

.field private mTextView44:Landroid/widget/TextView;

.field private mTextView5:Landroid/widget/TextView;

.field private mTextView55:Landroid/widget/TextView;

.field private mTextView6:Landroid/widget/TextView;

.field private mTextView66:Landroid/widget/TextView;

.field private mTextViewCb:Landroid/widget/TextView;

.field private mTextViewClinicalInfo:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/stress/StressActivity;->isDialogDissmised:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 75
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    .line 77
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "hr_guide_t_1"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "hr_guide_t_2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "hr_guide_t_3"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hr_guide_t_4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "stress_guide_t_5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "stress_guide_t_6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "stress_guide_t_5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "stress_guide_t_6"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    .line 84
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage1Found:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage2Found:Z

    .line 87
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isMeasureCompleted:Z

    .line 89
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->backFinish:Z

    .line 90
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->callFromHome:Z

    .line 92
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$1;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    .line 158
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$2;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDialogHandler:Landroid/os/Handler;

    .line 485
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$9;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 495
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$10;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 720
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$15;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->InfoDiagContentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    .line 790
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$16;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    return-void

    .line 75
    nop

    :array_0
    .array-data 4
        0x7f0201fa
        0x7f0201fb
        0x7f0201fc
        0x7f0201fd
        0x7f020784
        0x7f020785
        0x7f020784
        0x7f020785
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/app/Activity;
    .param p3, "x3"    # Landroid/app/Dialog;
    .param p4, "x4"    # Landroid/os/Bundle;
    .param p5, "x5"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 56
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/stress/StressActivity;->infoDiagContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->infoDiagOKBtnHandler()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # [I
    .param p4, "x4"    # [Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # Z

    .prologue
    .line 56
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/stress/StressActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/stress/StressActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/stress/StressActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->callFromHome:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/stress/StressActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/stress/StressActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->showInfomationDialog()V

    return-void
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/stress/StressActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->backFinish:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/stress/StressActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/stress/StressActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/stress/StressActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isShownPopup:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/stress/StressActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "imageView1"    # Landroid/widget/ImageView;
    .param p2, "imageView2"    # Landroid/widget/ImageView;
    .param p3, "images"    # [I
    .param p4, "ImageArr"    # [Ljava/lang/String;
    .param p5, "imageIndex"    # I
    .param p6, "forever"    # Z

    .prologue
    .line 604
    sget-boolean v1, Lcom/sec/android/app/shealth/stress/StressActivity;->isDialogDissmised:Z

    if-eqz v1, :cond_0

    .line 688
    :goto_0
    return-void

    .line 606
    :cond_0
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->initFadingAnimationEffect(I)V

    .line 607
    invoke-virtual {p1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 608
    .local v9, "ImgTag1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 609
    .local v10, "ImgTag2":Ljava/lang/String;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage1Found:Z

    .line 610
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage2Found:Z

    .line 611
    aget-object v1, p4, p5

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 612
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage1Found:Z

    .line 613
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 614
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 615
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v1, v0, :cond_3

    .line 616
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 617
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 622
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 623
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 625
    :cond_1
    aget-object v1, p4, p5

    invoke-virtual {v10, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 626
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage2Found:Z

    .line 627
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 628
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 629
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v0, v1, :cond_4

    .line 631
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 632
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 637
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 638
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 641
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    new-instance v2, Lcom/sec/android/app/shealth/stress/StressActivity$13;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/android/app/shealth/stress/StressActivity$13;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 661
    iget-object v11, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$14;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move/from16 v6, p5

    move-object/from16 v7, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/stress/StressActivity$14;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[II[Ljava/lang/String;Z)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto/16 :goto_0

    .line 619
    :cond_3
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 620
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    .line 634
    :cond_4
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 635
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private infoDiagContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const v3, 0x7f0804ef

    .line 747
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    .line 748
    const/4 v0, 0x0

    .line 749
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    .line 750
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 752
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    .line 753
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809d6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 754
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809d7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 756
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809d8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView1:Landroid/widget/TextView;

    .line 757
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809d9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView11:Landroid/widget/TextView;

    .line 758
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809da

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView2:Landroid/widget/TextView;

    .line 759
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809db

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView22:Landroid/widget/TextView;

    .line 760
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809dc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView3:Landroid/widget/TextView;

    .line 761
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809dd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView33:Landroid/widget/TextView;

    .line 762
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809de

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView4:Landroid/widget/TextView;

    .line 763
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809df

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView44:Landroid/widget/TextView;

    .line 764
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809e0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView5:Landroid/widget/TextView;

    .line 765
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809e1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView55:Landroid/widget/TextView;

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809e2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView6:Landroid/widget/TextView;

    .line 767
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0809e3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView66:Landroid/widget/TextView;

    .line 768
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080557

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextViewCb:Landroid/widget/TextView;

    .line 769
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f0804ee

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    .line 770
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    const v2, 0x7f080565

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextViewClinicalInfo:Landroid/widget/TextView;

    .line 772
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    .line 773
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 774
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 775
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 776
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/StressActivity;->initDialogAnimation(Landroid/view/View;)V

    .line 777
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->startDialogAnimation()V

    .line 778
    return-void
.end method

.method private infoDiagOKBtnHandler()V
    .locals 3

    .prologue
    .line 781
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 783
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "stress_warning_checked"

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 785
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 786
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 787
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/stress/StressActivity;->isDialogDissmised:Z

    .line 788
    return-void
.end method

.method private initDialogAnimation(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 556
    if-eqz p1, :cond_0

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 558
    const v0, 0x7f0809d6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 559
    const v0, 0x7f0809d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 564
    :cond_0
    return-void
.end method

.method private initFadingAnimationEffect(I)V
    .locals 7
    .param p1, "imageIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 578
    const/16 v0, 0x1f4

    .line 579
    .local v0, "fadeInDuration":I
    const/16 v2, 0x5dc

    .line 580
    .local v2, "timeBetween":I
    const/16 v1, 0x1f4

    .line 582
    .local v1, "fadeOutDuration":I
    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    const/4 v3, 0x5

    if-eq p1, v3, :cond_0

    const/4 v3, 0x6

    if-ne p1, v3, :cond_1

    .line 583
    :cond_0
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeIn:Landroid/view/animation/Animation;

    .line 584
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeOut:Landroid/view/animation/Animation;

    .line 590
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeIn:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 591
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeIn:Landroid/view/animation/Animation;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 592
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeIn:Landroid/view/animation/Animation;

    const/16 v4, 0x514

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 594
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeOut:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 595
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 596
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 597
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    .line 598
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    .line 599
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 600
    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 601
    return-void

    .line 586
    :cond_1
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeIn:Landroid/view/animation/Animation;

    .line 587
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->fadeOut:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    .line 315
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->terminateCurrentState()V

    .line 316
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 317
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 318
    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 320
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 321
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$6;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 333
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$7;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 343
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$8;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 354
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 355
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 356
    return-void
.end method

.method private showInfomationDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 522
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 523
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 524
    const v1, 0x7f090c39

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 525
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 526
    const v1, 0x7f030233

    new-instance v2, Lcom/sec/android/app/shealth/stress/StressActivity$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$11;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 538
    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$12;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 545
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoDiagDismissHandler:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 547
    if-eqz p0, :cond_1

    .line 549
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 550
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->startDialogAnimation()V

    .line 551
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "stm_information_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 553
    :cond_1
    return-void
.end method


# virtual methods
.method public callShowInformationDialog()V
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDialogHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 152
    return-void
.end method

.method protected customizeActionBar()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 438
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 439
    const/4 v0, 0x0

    .line 440
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const v3, 0x7f090026

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207c0

    const v6, 0x7f09005a

    iget-object v7, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v9

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    .line 444
    .local v1, "graphFragment":Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getDataCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207cf

    const v6, 0x7f090033

    iget-object v7, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v3, v9

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 448
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 449
    return-void

    .line 447
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 694
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 696
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 697
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f080304

    if-ne v1, v2, :cond_0

    .line 699
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08030e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 700
    const/4 v1, 0x1

    .line 705
    .end local v0    # "currentView":Landroid/view/View;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCallFromHome()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->callFromHome:Z

    return v0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 711
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    .line 712
    .local v0, "ret":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    if-eqz v0, :cond_0

    .line 717
    .end local v0    # "ret":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :goto_0
    return-object v0

    .line 714
    .restart local v0    # "ret":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :cond_0
    const-string/jumbo v1, "stm_information_dialog"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->InfoDiagContentInitializationListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    goto :goto_0

    .line 717
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 433
    const-string v0, "com.sec.shealth.help.action.STRESS"

    return-object v0
.end method

.method public getSummaryFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->callFromHome:Z

    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->finish()V

    .line 254
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 237
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->backFinish:Z

    if-eqz v0, :cond_2

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->terminateCurrentState()V

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->finish()V

    .line 244
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$4;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 241
    :cond_2
    const v0, 0x7f090c37

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->backFinish:Z

    goto :goto_1

    .line 253
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mIsActivityOnConfigChanged:Z

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->clearAndRefreshDrawerMenu()V

    .line 272
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$5;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 281
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 284
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 115
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 118
    .local v1, "model":Ljava/lang/String;
    const-string v2, "SM-G850F"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-G850S"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-G850A"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-G850L"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-G850W"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v3, "hr_guide_alpha_1"

    aput-object v3, v2, v4

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v3, "hr_guide_alpha_2"

    aput-object v3, v2, v5

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v3, "hr_guide_alpha_3"

    aput-object v3, v2, v6

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    const-string v3, "hr_guide_alpha_4"

    aput-object v3, v2, v7

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    const v3, 0x7f0201f6

    aput v3, v2, v4

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    const v3, 0x7f0201f7

    aput v3, v2, v5

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    const v3, 0x7f0201f8

    aput v3, v2, v6

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    const v3, 0x7f0201f9

    aput v3, v2, v7

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "show_graph_fragment"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 133
    .local v0, "isShowGraphFrag":Z
    new-instance v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initStressState()V

    .line 135
    new-instance v2, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 138
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "stress_warning_checked"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->callShowInformationDialog()V

    .line 148
    :goto_0
    return-void

    .line 142
    :cond_2
    if-eqz v0, :cond_3

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 144
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->callFromHome:Z

    goto :goto_0

    .line 146
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100026

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    const/4 v0, 0x0

    .line 366
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->clearDeviceConnector()V

    .line 206
    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    if-eqz v0, :cond_2

    .line 209
    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_3

    .line 211
    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 213
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 214
    return-void
.end method

.method protected onLogSelected()V
    .locals 3

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.stress"

    const-string v2, "ST03"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->startActivity(Landroid/content/Intent;)V

    .line 455
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 405
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isMeasureCompleted:Z

    if-nez v2, :cond_0

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->terminateCurrentState()V

    .line 408
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 426
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_1
    return v1

    .line 410
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->showDeleteDialog()V

    goto :goto_1

    .line 413
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.stress"

    const-string v4, "ST07"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->prepareShareView()V

    goto :goto_1

    .line 417
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 418
    .local v0, "intentSetting":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 419
    const-string v2, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 424
    .end local v0    # "intentSetting":Landroid/content/Intent;
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.stress"

    const-string v3, "ST06"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 408
    :sswitch_data_0
    .sparse-switch
        0x7f0802db -> :sswitch_0
        0x7f080c8a -> :sswitch_3
        0x7f080c90 -> :sswitch_1
        0x7f080ca4 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPause()V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isShownPopup:Z

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 225
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 372
    if-eqz p1, :cond_1

    .line 373
    :try_start_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v5

    const v6, 0x7f100026

    invoke-virtual {v5, v6, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    .line 376
    .local v1, "graphFragment":Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->isDrawerMenuShown()Z

    move-result v5

    if-nez v5, :cond_0

    .line 377
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->customizeActionBar()V

    .line 378
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getSummaryFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .line 379
    .local v2, "summaryFragment":Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 380
    const v3, 0x7f0802db

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 381
    const v3, 0x7f080ca4

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 382
    const v3, 0x7f080c8a

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 383
    const v3, 0x7f080c90

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 399
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
    .end local v2    # "summaryFragment":Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    :cond_1
    :goto_0
    return v4

    .line 389
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
    .restart local v2    # "summaryFragment":Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    :cond_2
    const v5, 0x7f0802db

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 390
    const v5, 0x7f080ca4

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 391
    const v5, 0x7f080c8a

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 392
    const v5, 0x7f080c90

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getState()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v6

    instance-of v6, v6, Lcom/sec/android/app/shealth/stress/state/StressStateFinished;

    if-eqz v6, :cond_3

    move v3, v4

    :cond_3
    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 395
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
    .end local v2    # "summaryFragment":Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 264
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 265
    const-string v0, "delete_popup"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isShownPopup:Z

    .line 266
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 167
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->isDrawerMenuShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->customizeActionBar()V

    .line 170
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isShownPopup:Z

    if-eqz v0, :cond_1

    .line 171
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->showDeleteDialog()V

    .line 175
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mIsActivityOnConfigChanged:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 179
    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mDialogHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/stress/StressActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/stress/StressActivity$3;-><init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 190
    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    .line 191
    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInfoView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->initDialogAnimation(Landroid/view/View;)V

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->startDialogAnimation()V

    .line 196
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mIsActivityOnConfigChanged:Z

    .line 197
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 258
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 259
    const-string v0, "delete_popup"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->isShownPopup:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 260
    return-void
.end method

.method public startDialogAnimation()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 568
    sput-boolean v5, Lcom/sec/android/app/shealth/stress/StressActivity;->isDialogDissmised:Z

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    aget v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 573
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImages:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->informationImagesArray:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/stress/StressActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    .line 575
    :cond_0
    return-void
.end method

.method public switchFragmentToGraph()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 472
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->callFromHome:Z

    .line 473
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.stress"

    const-string v2, "ST04"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    invoke-virtual {p0, v0, v3, v3}, Lcom/sec/android/app/shealth/stress/StressActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 475
    return-void
.end method

.method public switchFragmentToSummary()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 468
    return-void
.end method

.method public updateStringOnLocaleChange()V
    .locals 3

    .prologue
    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    .line 289
    .local v0, "content":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 290
    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 291
    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090c39

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextViewCb:Landroid/widget/TextView;

    const v2, 0x7f09078c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView1:Landroid/widget/TextView;

    const v2, 0x7f090c1a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView11:Landroid/widget/TextView;

    const v2, 0x7f090c3a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView2:Landroid/widget/TextView;

    const v2, 0x7f090c1b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView22:Landroid/widget/TextView;

    const v2, 0x7f090c14

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView3:Landroid/widget/TextView;

    const v2, 0x7f090c1c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView33:Landroid/widget/TextView;

    const v2, 0x7f090c15

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView4:Landroid/widget/TextView;

    const v2, 0x7f090c1d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView44:Landroid/widget/TextView;

    const v2, 0x7f090c16

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView5:Landroid/widget/TextView;

    const v2, 0x7f090c1e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView55:Landroid/widget/TextView;

    const v2, 0x7f090c25

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView6:Landroid/widget/TextView;

    const v2, 0x7f090c1f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextView66:Landroid/widget/TextView;

    const v2, 0x7f090c2d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity;->mTextViewClinicalInfo:Landroid/widget/TextView;

    const v2, 0x7f090c08

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 312
    .end local v0    # "content":Landroid/view/View;
    :cond_0
    return-void
.end method
