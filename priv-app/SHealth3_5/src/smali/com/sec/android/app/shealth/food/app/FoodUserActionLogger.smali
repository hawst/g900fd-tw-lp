.class public Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;
.super Ljava/lang/Object;
.source "FoodUserActionLogger.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mAppId:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;->mContext:Landroid/content/Context;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".food"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;->mAppId:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V
    .locals 3
    .param p1, "userAction"    # Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/app/UserActionLog;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;->mAppId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/app/UserActionLog;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/food/app/FoodUserActionLogger;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/app/UserActionLog;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method
