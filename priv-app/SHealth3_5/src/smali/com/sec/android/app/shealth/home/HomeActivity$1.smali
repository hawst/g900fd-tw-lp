.class Lcom/sec/android/app/shealth/home/HomeActivity$1;
.super Landroid/database/ContentObserver;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final SETTING_WEARABLE_ID:Ljava/lang/String; = "content://settings/system/connected_wearable_id"


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$1;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 0
    .param p1, "selfChange"    # Z

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 248
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 253
    if-eqz p2, :cond_1

    const-string v0, "content://settings/system/connected_wearable_id"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "content://com.samsung.android.app.atracker.shealthsync.SHealthProvider"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$1;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->setSyncActionBarButton()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$000(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$1;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$100(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    .line 260
    :cond_1
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 261
    return-void
.end method
