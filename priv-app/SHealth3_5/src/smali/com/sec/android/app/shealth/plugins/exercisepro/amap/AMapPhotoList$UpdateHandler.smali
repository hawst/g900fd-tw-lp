.class public Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;
.super Landroid/os/Handler;
.source "AMapPhotoList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UpdateHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 68
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_1

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v3, :cond_1

    .line 69
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/util/ArrayList;

    iput-object v3, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->mPhotoList:Ljava/util/ArrayList;

    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->mPhotoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 72
    .local v2, "photo":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v3, v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->setPhotoItem(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;II)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    .line 74
    goto :goto_0

    .line 76
    .end local v2    # "photo":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 77
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->mPhotoListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->getMarkerOPList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;->onUpdatePhotoList(Ljava/util/List;)V

    .line 80
    .end local v0    # "i":I
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method
