.class public abstract Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;
.super Landroid/widget/PopupWindow;
.source "ListPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;,
        Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;,
        Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$InternalOnItemClickedListener;,
        Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$OnItemClickedListener;,
        Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;
    }
.end annotation


# static fields
.field public static final ANCHOR_CENTER:I = 0x1

.field public static final ANCHOR_LEFT:I = 0x2

.field public static final ANCHOR_RIGHT:I = 0x4

.field public static final AT_ABOVE:I = 0x10000

.field public static final AT_BELOW:I = 0x20000

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdditionalItems:[Ljava/lang/String;

.field private mAdditionalItemsPosition:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

.field private mAreTextPaddingsDefault:Z

.field private mCaller:Landroid/view/View;

.field protected mContext:Landroid/content/Context;

.field private mDescriptions:[Ljava/lang/String;

.field private mHasContentDescription:Z

.field private mIsAdditionalTextVisible:Z

.field private mItems:[Ljava/lang/String;

.field private mListView:Landroid/widget/ListView;

.field private mListeners:[Landroid/view/View$OnClickListener;

.field private mOnItemClickedListener:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$OnItemClickedListener;

.field private mPopupView:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemIndex:I

.field private mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

.field private mTextSelection:Ljava/lang/String;

.field private mTitleText:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;

    .prologue
    .line 102
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Z)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I

    .prologue
    .line 116
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;IZ)V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;IZ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "focusable"    # Z

    .prologue
    .line 120
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Z)V

    .line 121
    if-eqz p4, :cond_0

    .line 122
    iput p4, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    .line 123
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setWidth(I)V

    .line 125
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I[Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "additionalItems"    # [Ljava/lang/String;

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I)V

    .line 129
    iput-object p5, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAdditionalItems:[Ljava/lang/String;

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mIsAdditionalTextVisible:Z

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "focusable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mSelectedItemIndex:I

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mHasContentDescription:Z

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->NONE:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAdditionalItemsPosition:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAreTextPaddingsDefault:Z

    .line 107
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    .line 108
    iput-object p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    .line 109
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    .line 110
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    .line 111
    array-length v0, p2

    new-array v0, v0, [Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListeners:[Landroid/view/View$OnClickListener;

    .line 112
    invoke-direct {p0, p4}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->init(Z)V

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$OnItemClickedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mOnItemClickedListener:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$OnItemClickedListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAdditionalItems:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mHasContentDescription:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mDescriptions:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mSelectedItemIndex:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextSelection:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setHeightAndWeightPopup()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;Landroid/widget/TextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mIsAdditionalTextVisible:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTitleText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListeners:[Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAdditionalItemsPosition:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    return-object v0
.end method

.method private adjustPopupWidth()V
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAreTextPaddingsDefault:Z

    if-nez v0, :cond_0

    .line 330
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    iget v1, v1, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;->paddingLeft:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    iget v2, v2, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;->paddingRight:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00eb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    .line 332
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setWidth(I)V

    .line 333
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAreTextPaddingsDefault:Z

    .line 335
    :cond_0
    return-void
.end method

.method private applyPaddinsToTextView(Landroid/widget/TextView;)V
    .locals 4
    .param p1, "text"    # Landroid/widget/TextView;

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    iget v0, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;->paddingLeft:I

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    iget v2, v2, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;->paddingRight:I

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 300
    return-void
.end method

.method private checkFocus()V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 357
    :cond_0
    return-void
.end method

.method private init(Z)V
    .locals 12
    .param p1, "focusable"    # Z

    .prologue
    .line 157
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 158
    .local v3, "inflater":Landroid/view/LayoutInflater;
    new-instance v9, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$1;

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-direct {v9, p0, v10}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    .line 169
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getBackgroundResource()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 170
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getDividerDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 171
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getDividerHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 172
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    invoke-direct {v0, p0, v9, v10}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;-><init>(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;Landroid/content/Context;[Ljava/lang/String;)V

    .line 173
    .local v0, "adapter":Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setClickable(Z)V

    .line 174
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 175
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    if-nez v9, :cond_2

    .line 176
    const/4 v6, 0x0

    .line 177
    .local v6, "maxWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getLayoutId()I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 178
    .local v8, "view":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getTextViewId()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 179
    .local v7, "text":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v4, v1, v2

    .line 180
    .local v4, "item":Ljava/lang/String;
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/widget/LinearLayout;->measure(II)V

    .line 182
    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    .line 183
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    if-le v9, v6, :cond_0

    .line 184
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    .line 179
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 187
    .end local v4    # "item":Ljava/lang/String;
    :cond_1
    add-int/lit8 v9, v6, 0x14

    iput v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    .line 189
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "maxWidth":I
    .end local v7    # "text":Landroid/widget/TextView;
    .end local v8    # "view":Landroid/widget/LinearLayout;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 190
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    .line 192
    :cond_3
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mWidth:I

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setWidth(I)V

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->isHeightSetToWrapContent()Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v9, -0x2

    :goto_1
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setHeight(I)V

    .line 196
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setContentView(Landroid/view/View;)V

    .line 197
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isExternalKeyBoardDisabled()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 199
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 200
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 207
    :goto_2
    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const/4 v9, 0x0

    check-cast v9, Landroid/graphics/Bitmap;

    invoke-direct {v10, v11, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 208
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->initOnItemClickListeners()V

    .line 209
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->setFocusable(Z)V

    .line 210
    return-void

    .line 193
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getListPopupItemHeight()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    array-length v10, v10

    mul-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getDividerHeight()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    array-length v11, v11

    mul-int/2addr v10, v11

    add-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getBackgroundEmptySpaceHeight()I

    move-result v10

    add-int/2addr v9, v10

    goto :goto_1

    .line 204
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 205
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    goto :goto_2
.end method

.method private initOnItemClickListeners()V
    .locals 4

    .prologue
    .line 217
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListeners:[Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$InternalOnItemClickedListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v0, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$InternalOnItemClickedListener;-><init>(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;ILcom/sec/android/app/shealth/walkingmate/utils/ListPopup$1;)V

    aput-object v2, v1, v0

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_0
    return-void
.end method

.method private setHeightAndWeightPopup()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 379
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    .line 380
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTouchables()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mPopupView:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 387
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract getAdditionalTextViewId()I
.end method

.method protected abstract getAdditionalUnitViewId()I
.end method

.method protected abstract getBackgroundEmptySpaceHeight()I
.end method

.method protected abstract getBackgroundResource()I
.end method

.method protected abstract getBelowTextViewId()I
.end method

.method protected abstract getBottomItemDrawableId()I
.end method

.method protected abstract getClickableViewId()I
.end method

.method protected abstract getDefaultItemColorId()I
.end method

.method protected abstract getDividerDrawable()Landroid/graphics/drawable/Drawable;
.end method

.method protected abstract getDividerHeight()I
.end method

.method protected abstract getGreyItemColorId()I
.end method

.method public getItemAmount()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method protected abstract getLayoutId()I
.end method

.method protected abstract getListPopupItemHeight()I
.end method

.method protected abstract getMiddleItemDrawableId()I
.end method

.method protected abstract getRecetTextViewId()I
.end method

.method protected abstract getSelectedItemColorId()I
.end method

.method public getSelectedItemIndex()I
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mSelectedItemIndex:I

    return v0
.end method

.method protected abstract getTextViewId()I
.end method

.method protected abstract getTopItemDrawableId()I
.end method

.method protected isHeightSetToWrapContent()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    return v0
.end method

.method public setCurrentItem(I)V
    .locals 0
    .param p1, "itemIndex"    # I

    .prologue
    .line 360
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mSelectedItemIndex:I

    .line 361
    return-void
.end method

.method public setOnItemClickedListener(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$OnItemClickedListener;)V
    .locals 0
    .param p1, "onItemClickedListener"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$OnItemClickedListener;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mOnItemClickedListener:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$OnItemClickedListener;

    .line 135
    return-void
.end method

.method public setTextPaddings(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;)V
    .locals 1
    .param p1, "textPaddings"    # Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAreTextPaddingsDefault:Z

    .line 93
    return-void
.end method

.method public setTextSelection(Ljava/lang/String;)V
    .locals 0
    .param p1, "textSelection"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextSelection:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 303
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->show(II)V

    .line 304
    return-void
.end method

.method public show(II)V
    .locals 1
    .param p1, "xOffset"    # I
    .param p2, "yOffset"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->adjustPopupWidth()V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->showAsDropDown(Landroid/view/View;II)V

    .line 310
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->checkFocus()V

    .line 315
    :goto_0
    return-void

    .line 312
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->dismiss()V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->refreshFocusables(Landroid/view/View;)V

    goto :goto_0
.end method

.method public showAtLocation(III)V
    .locals 1
    .param p1, "gravity"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mItems:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 319
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->adjustPopupWidth()V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->showAtLocation(Landroid/view/View;III)V

    .line 321
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->checkFocus()V

    .line 326
    :goto_0
    return-void

    .line 323
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->dismiss()V

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mCaller:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->refreshFocusables(Landroid/view/View;)V

    goto :goto_0
.end method
