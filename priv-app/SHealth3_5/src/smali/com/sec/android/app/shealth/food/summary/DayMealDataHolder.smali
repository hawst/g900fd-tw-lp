.class public Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
.super Ljava/lang/Object;
.source "DayMealDataHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;,
        Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;
    }
.end annotation


# instance fields
.field private mBitmapsPath:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDataState:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

.field private mDayTime:J

.field private mIsSkipped:Z

.field private mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

.field private mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

.field private mMealViewsHolder:Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/constants/MealType;)V
    .locals 1
    .param p1, "mealType"    # Lcom/sec/android/app/shealth/food/constants/MealType;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    .line 100
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->NO_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDataState:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mBitmapsPath:Ljava/util/List;

    .line 102
    return-void
.end method


# virtual methods
.method public addBitmap(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mBitmapsPath:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->MEAL_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDataState:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    .line 173
    return-void
.end method

.method public getBitmapsPaths()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mBitmapsPath:Ljava/util/List;

    return-object v0
.end method

.method public getDataState()Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDataState:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    return-object v0
.end method

.method public getDayTime()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDayTime:J

    return-wide v0
.end method

.method public getMealData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    return-object v0
.end method

.method public getMealDataHolder()Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    .locals 3

    .prologue
    .line 152
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>()V

    .line 153
    .local v0, "result":Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v1

    iget v1, v1, Lcom/sec/android/app/shealth/food/constants/MealType;->mealTypeId:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealType(J)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-nez v1, :cond_0

    const-wide/high16 v1, -0x8000000000000000L

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealId(J)V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-nez v1, :cond_1

    iget-wide v1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDayTime:J

    :goto_1
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealTime(J)V

    .line 156
    return-object v0

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v1

    goto :goto_0

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v1

    goto :goto_1
.end method

.method public getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealType:Lcom/sec/android/app/shealth/food/constants/MealType;

    return-object v0
.end method

.method public getMealViewsHolder()Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealViewsHolder:Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;

    return-object v0
.end method

.method public getNameResourceId()I
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    return v0
.end method

.method public getViewId()I
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->getResourcedTypeByMealType(Lcom/sec/android/app/shealth/food/constants/MealType;)Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->mLayoutResourceId:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;->access$000(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$ResourcedMealType;)I

    move-result v0

    return v0
.end method

.method public isSkipped()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mIsSkipped:Z

    return v0
.end method

.method public resetData(J)V
    .locals 1
    .param p1, "dayTime"    # J

    .prologue
    .line 199
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mBitmapsPath:Ljava/util/List;

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->NO_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDataState:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    .line 202
    iput-wide p1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDayTime:J

    .line 203
    return-void
.end method

.method public setIsSkipped(Z)V
    .locals 0
    .param p1, "mIsSkipped"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mIsSkipped:Z

    .line 44
    return-void
.end method

.method public setMealData(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)V
    .locals 2
    .param p1, "mealData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .prologue
    .line 180
    if-nez p1, :cond_0

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MealData should be not null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 184
    sget-object v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->MEAL_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mDataState:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    .line 185
    return-void
.end method

.method public setMealViewsHolder(Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;)V
    .locals 0
    .param p1, "mealViewsHolder"    # Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->mMealViewsHolder:Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;

    .line 111
    return-void
.end method
