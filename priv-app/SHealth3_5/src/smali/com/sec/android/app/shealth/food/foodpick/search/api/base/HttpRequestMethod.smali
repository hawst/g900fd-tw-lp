.class public final enum Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;
.super Ljava/lang/Enum;
.source "HttpRequestMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

.field public static final enum DELETE:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

.field public static final enum GET:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

.field public static final enum POST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->GET:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->DELETE:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    const-string v1, "POST"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->POST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->GET:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->DELETE:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->POST:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/foodpick/search/api/base/HttpRequestMethod;

    return-object v0
.end method
