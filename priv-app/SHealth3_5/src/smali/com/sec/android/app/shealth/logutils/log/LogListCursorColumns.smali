.class public interface abstract Lcom/sec/android/app/shealth/logutils/log/LogListCursorColumns;
.super Ljava/lang/Object;
.source "LogListCursorColumns.java"


# static fields
.field public static final AMOUNT:Ljava/lang/String; = "amount"

.field public static final AVG_VALUE:Ljava/lang/String; = "avg_value"

.field public static final DAY_TIME:Ljava/lang/String; = "day_time"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final MEMO_TEXT:Ljava/lang/String; = "memo_text"

.field public static final MONTH_TIME:Ljava/lang/String; = "month_time"

.field public static final NAME:Ljava/lang/String; = "food_name"
