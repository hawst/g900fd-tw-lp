.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;
.super Landroid/widget/FrameLayout;
.source "RealtimeHorizontalSliderView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final SMOOTH_EXPAND_SIZE:I = 0x14


# instance fields
.field private bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

.field private bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

.field private handler:Landroid/view/View;

.field private handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

.field private handlerPosition:F

.field private handlerPositionListener:Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;

.field private mContext:Landroid/content/Context;

.field private maxCount:I

.field private oldHandlerPostion:F

.field private progressBar:Landroid/widget/ProgressBar;

.field private scaleFrameLayout:Landroid/widget/FrameLayout;

.field private scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

.field private scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

.field private sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

.field private step:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->mContext:Landroid/content/Context;

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->postInit()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->mContext:Landroid/content/Context;

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->postInit()V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->init()V

    return-void
.end method

.method private dropHandlerAt(I)V
    .locals 6
    .param p1, "px"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 197
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getRoundedHandlerPosition2(I)F

    move-result v0

    .line 198
    .local v0, "newPosition":F
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->oldHandlerPostion:F

    .line 200
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 210
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPositionListener:Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPositionListener:Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;->onPositionChanged(F)V

    .line 213
    :cond_0
    return-void

    .line 203
    :cond_1
    cmpl-float v1, v0, v5

    if-nez v1, :cond_2

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->leftPadding:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->step:F

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v3, v3

    div-float/2addr v3, v5

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    goto :goto_0

    .line 207
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->moveHandler()V

    goto :goto_0
.end method

.method private getAbsoluteHandlerPosition()I
    .locals 2

    .prologue
    .line 191
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 192
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 193
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    return v1
.end method

.method private getRoundedHandlerPosition(I)F
    .locals 3
    .param p1, "px"    # I

    .prologue
    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->leftPadding:I

    sub-int v1, p1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x14

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->step:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41200000    # 10.0f

    div-float v0, v1, v2

    .line 248
    .local v0, "newPosition":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 249
    const/4 v0, 0x0

    .line 253
    :cond_0
    :goto_0
    return v0

    .line 250
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    mul-int/lit8 v1, v1, 0x14

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 251
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    mul-int/lit8 v1, v1, 0x14

    int-to-float v0, v1

    goto :goto_0
.end method

.method private getRoundedHandlerPosition2(I)F
    .locals 9
    .param p1, "px"    # I

    .prologue
    const-wide/high16 v7, 0x4004000000000000L    # 2.5

    const-wide/high16 v5, 0x3ff8000000000000L    # 1.5

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->leftPadding:I

    sub-int v1, p1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0xa

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->step:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41200000    # 10.0f

    div-float v0, v1, v2

    .line 258
    .local v0, "newPosition":F
    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    .line 259
    const/4 v0, 0x0

    .line 269
    :cond_0
    :goto_0
    return v0

    .line 260
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    .line 261
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    int-to-float v0, v1

    goto :goto_0

    .line 262
    :cond_2
    float-to-double v1, v0

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_3

    float-to-double v1, v0

    cmpg-double v1, v1, v5

    if-gez v1, :cond_3

    .line 263
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 264
    :cond_3
    float-to-double v1, v0

    cmpl-double v1, v1, v5

    if-ltz v1, :cond_4

    float-to-double v1, v0

    cmpg-double v1, v1, v7

    if-gez v1, :cond_4

    .line 265
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0

    .line 266
    :cond_4
    float-to-double v1, v0

    cmpl-double v1, v1, v7

    if-ltz v1, :cond_0

    .line 267
    const/high16 v0, 0x40400000    # 3.0f

    goto :goto_0
.end method

.method private init()V
    .locals 13

    .prologue
    .line 63
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    if-nez v9, :cond_0

    .line 64
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "can\'t initialize without adapter"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 66
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getScaleLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    .line 67
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    if-nez v9, :cond_1

    .line 68
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v9, v10, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    .line 70
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    const/16 v10, 0x33

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 71
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0902

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v10, v10

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 72
    new-instance v9, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleFrameLayout:Landroid/widget/FrameLayout;

    .line 73
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleFrameLayout:Landroid/widget/FrameLayout;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getCount()I

    move-result v1

    .line 75
    .local v1, "count":I
    const/4 v9, 0x2

    if-ge v1, v9, :cond_2

    .line 76
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "slider adapter should return count greater than 1"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 78
    :cond_2
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    .line 79
    new-instance v9, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x0

    const v12, 0x1010078

    invoke-direct {v9, v10, v11, v12}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->progressBar:Landroid/widget/ProgressBar;

    .line 80
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f020277

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->progressBar:Landroid/widget/ProgressBar;

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    mul-int/lit8 v10, v10, 0x14

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 83
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->progressBar:Landroid/widget/ProgressBar;

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    float-to-int v10, v10

    mul-int/lit8 v10, v10, 0x14

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 84
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->progressBar:Landroid/widget/ProgressBar;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getScalePaddings()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    .line 86
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    if-nez v9, :cond_3

    .line 87
    new-instance v9, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;-><init>(II)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    .line 90
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getHandler()Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    .line 91
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    if-nez v9, :cond_4

    .line 92
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "slider adapter should return non-null handler"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 94
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getHandlerSize()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;

    move-result-object v4

    .line 95
    .local v4, "handlerSize":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;->width:I

    iget v11, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;->height:I

    invoke-direct {v9, v10, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    .line 96
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v10, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 97
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0902

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v10, v10

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 99
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 101
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getDivider()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 102
    .local v2, "divider":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getWidth()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->rightPadding:I

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v10, v10, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->leftPadding:I

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v10, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v10, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    iput v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->step:F

    .line 104
    if-eqz v2, :cond_8

    .line 105
    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 106
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "slider adapter should return divider that has not parent"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 108
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getDividerWidth()I

    move-result v3

    .line 109
    .local v3, "dividerWidth":I
    if-gtz v3, :cond_6

    .line 110
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "slider adapter should return divider width greater than 0"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 112
    :cond_6
    const/4 v5, 0x1

    .local v5, "i":I
    :goto_0
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->maxCount:I

    if-ge v5, v9, :cond_8

    .line 113
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->step:F

    int-to-float v10, v5

    mul-float v7, v9, v10

    .line 114
    .local v7, "position":F
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a092d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    invoke-direct {v6, v3, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 115
    .local v6, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v9, v9, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->leftPadding:I

    int-to-float v9, v9

    add-float/2addr v9, v7

    int-to-float v10, v3

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a092b

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v10, v10

    add-int/2addr v9, v10

    iput v9, v6, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a092c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v6, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 118
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v9, v9, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iput v9, v6, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 119
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v9, v9, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iput v9, v6, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 120
    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 121
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getDivider()Landroid/view/View;

    move-result-object v2

    .end local v2    # "divider":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 123
    .restart local v2    # "divider":Landroid/widget/ImageView;
    :cond_7
    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    invoke-virtual {v2}, Landroid/widget/ImageView;->bringToFront()V

    .line 112
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 128
    .end local v3    # "dividerWidth":I
    .end local v5    # "i":I
    .end local v6    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v7    # "position":F
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;->getBubbleViewHandler()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    .line 129
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    if-eqz v9, :cond_b

    .line 130
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;->getView()Landroid/view/View;

    move-result-object v0

    .line 131
    .local v0, "bubbleView":Landroid/view/View;
    if-nez v0, :cond_9

    .line 132
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "bubble view handler should return non-null view"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 134
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;->getSize()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;

    move-result-object v8

    .line 135
    .local v8, "size":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;
    if-nez v8, :cond_a

    .line 136
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "bubble view handler should return non-null size"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 138
    :cond_a
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;->width:I

    iget v11, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;->height:I

    invoke-direct {v9, v10, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    .line 139
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v10, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 140
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v10, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    iget v11, v4, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;->height:I

    add-int/2addr v10, v11

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 149
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    iget v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    invoke-interface {v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;->onPositionChanged(F)V

    .line 150
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->addView(Landroid/view/View;)V

    .line 153
    .end local v0    # "bubbleView":Landroid/view/View;
    .end local v8    # "size":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;
    :cond_b
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_c

    .line 154
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v10, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v11, v11, Landroid/widget/FrameLayout$LayoutParams;->width:I

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 155
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->requestLayout()V

    .line 162
    :goto_1
    invoke-virtual {p0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 163
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9, p0}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 164
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    iput v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->oldHandlerPostion:F

    .line 165
    return-void

    .line 156
    :cond_c
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    const/high16 v10, 0x40000000    # 2.0f

    cmpl-float v9, v9, v10

    if-nez v9, :cond_d

    .line 157
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v10, v10, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v11, v11, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->leftPadding:I

    add-int/2addr v10, v11

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->step:F

    iget v12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v11, v11, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    add-int/lit8 v10, v10, 0x6

    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 158
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->requestLayout()V

    goto :goto_1

    .line 160
    :cond_d
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->moveHandler()V

    goto :goto_1
.end method

.method private moveBubble()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;->onPositionChanged(F)V

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->progressBar:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 299
    return-void
.end method

.method private moveBubbleAt()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->progressBar:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getAbsoluteHandlerPosition()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getRoundedHandlerPosition(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->bubbleViewHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;->onPositionChanged(F)V

    .line 289
    :cond_0
    return-void
.end method

.method private moveHandler()V
    .locals 5

    .prologue
    .line 273
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 275
    .local v0, "width":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->leftPadding:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->step:F

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    int-to-float v3, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 278
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->moveBubble()V

    .line 279
    return-void
.end method

.method private moveHandlerAt(F)V
    .locals 5
    .param p1, "px"    # F

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getAbsoluteHandlerPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getRoundedHandlerPosition2(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 220
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v0, v2, v3

    .line 224
    .local v0, "leftEdgeValue":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    int-to-float v2, v2

    cmpg-float v2, v2, v0

    if-gez v2, :cond_0

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 232
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scaleFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->scalePaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;->rightPadding:I

    int-to-float v3, v3

    sub-float v1, v2, v3

    .line 233
    .local v1, "rightEdgeValue":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    int-to-float v2, v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_1

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 238
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handler:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 239
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->moveBubbleAt()V

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPositionListener:Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->oldHandlerPostion:F

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_2

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPositionListener:Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;->onPositionChanged(F)V

    .line 242
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->oldHandlerPostion:F

    .line 244
    :cond_2
    return-void
.end method

.method private postInit()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->post(Ljava/lang/Runnable;)Z

    .line 60
    return-void
.end method


# virtual methods
.method public initialize(FLcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;)V
    .locals 0
    .param p1, "handlerPosition"    # F
    .param p2, "sliderAdapter"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    .prologue
    .line 306
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPosition:F

    .line 307
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->sliderAdapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    .line 308
    return-void
.end method

.method public initialize(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;)V
    .locals 1
    .param p1, "sliderAdapter"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->initialize(FLcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;)V

    .line 303
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 187
    :goto_0
    return v0

    .line 175
    :pswitch_0
    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 176
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->moveHandlerAt(F)V

    goto :goto_0

    .line 179
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->moveHandlerAt(F)V

    goto :goto_0

    .line 182
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getAbsoluteHandlerPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->dropHandlerAt(I)V

    .line 183
    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    move v0, v1

    .line 184
    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setHandlerPositionListener(Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;)V
    .locals 0
    .param p1, "handlerPositionListener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->handlerPositionListener:Lcom/sec/android/app/shealth/plugins/exercisepro/listener/HandlerPositionListener;

    .line 312
    return-void
.end method

.method public up()V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->getAbsoluteHandlerPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/RealtimeHorizontalSliderView;->dropHandlerAt(I)V

    .line 169
    return-void
.end method
