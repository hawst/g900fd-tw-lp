.class public Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
.super Ljava/lang/Object;
.source "CignaLibraryBaseListData.java"


# instance fields
.field private mCategoryString:Ljava/lang/CharSequence;

.field private mIconResID:I

.field private mId:I

.field private mSubInformaion:Ljava/lang/CharSequence;

.field private mTitleString:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mIconResID:I

    .line 13
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mTitleString:Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mSubInformaion:Ljava/lang/CharSequence;

    .line 14
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mCategoryString:Ljava/lang/CharSequence;

    .line 18
    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;)V
    .locals 1
    .param p1, "iconResID"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 26
    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "iconResID"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "subInfomation"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mIconResID:I

    .line 13
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mTitleString:Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mSubInformaion:Ljava/lang/CharSequence;

    .line 14
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mCategoryString:Ljava/lang/CharSequence;

    .line 33
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mIconResID:I

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mTitleString:Ljava/lang/CharSequence;

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mSubInformaion:Ljava/lang/CharSequence;

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    .locals 5
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 78
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;-><init>()V

    .line 80
    .local v0, "cignaLibraryBaseListData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    instance-of v4, p0, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;

    if-eqz v4, :cond_1

    move-object v2, p0

    .line 82
    check-cast v2, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;

    .line 83
    .local v2, "healthLibraryCategory":Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->getCategoryId()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->setId(I)V

    .line 84
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->setTitle(Ljava/lang/CharSequence;)V

    .line 85
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->getCategoryId()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryImageResource;->getLibraryCategoryImageResource(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->setIcon(I)V

    .line 99
    .end local v2    # "healthLibraryCategory":Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    :cond_0
    :goto_0
    return-object v0

    .line 87
    :cond_1
    instance-of v4, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;

    if-eqz v4, :cond_2

    move-object v3, p0

    .line 89
    check-cast v3, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;

    .line 90
    .local v3, "healthLibrarySubCategory":Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;->getSubCategoryId()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->setId(I)V

    .line 91
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 93
    .end local v3    # "healthLibrarySubCategory":Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;
    :cond_2
    instance-of v4, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    if-eqz v4, :cond_0

    move-object v1, p0

    .line 95
    check-cast v1, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .line 96
    .local v1, "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getIcon()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mIconResID:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mId:I

    return v0
.end method

.method public getSubInformaion()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mSubInformaion:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mTitleString:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setCategoryString(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "groupString"    # Ljava/lang/CharSequence;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mCategoryString:Ljava/lang/CharSequence;

    .line 70
    return-void
.end method

.method public setIcon(I)V
    .locals 0
    .param p1, "resourceId"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mIconResID:I

    .line 52
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mId:I

    .line 44
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 59
    if-nez p1, :cond_0

    const-string p1, ""

    .end local p1    # "title":Ljava/lang/CharSequence;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->mTitleString:Ljava/lang/CharSequence;

    .line 60
    return-void
.end method
