.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;
.super Ljava/lang/Object;
.source "WalkingMateActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V
    .locals 0

    .prologue
    .line 840
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 844
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 845
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "Date"

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mIntent:Landroid/content/Intent;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SelectedDate"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 846
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;-><init>()V

    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$202(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .line 847
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 848
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 849
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    .line 850
    return-void
.end method
