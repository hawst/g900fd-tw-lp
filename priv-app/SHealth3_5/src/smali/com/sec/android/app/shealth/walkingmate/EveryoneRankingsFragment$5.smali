.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;
.super Ljava/lang/Object;
.source "EveryoneRankingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->init(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 373
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/String;

    .line 374
    .local v0, "items":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090b9f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 375
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090b9e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 376
    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b9a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b9b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 379
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$700(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0, p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V

    .line 380
    .local v1, "popup":Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->period_select_idx:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1900(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)I

    move-result v2

    aget-object v2, v0, v2

    const v3, 0x7f07019d

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setTextSelection(Ljava/lang/String;I)V

    .line 381
    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;Lcom/sec/android/app/shealth/common/commonui/ListPopup;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setOnItemClickedListener(Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;)V

    .line 410
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTV_dropdown_day_seleter:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    rsub-int/lit8 v2, v2, 0x0

    invoke-virtual {v1, v6, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->show(II)V

    .line 412
    return-void
.end method
