.class public Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$24;,
        Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;,
        Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;,
        Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;,
        Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$LoadMoreFoodInfoDataTaskForSearch;,
        Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;
    }
.end annotation


# static fields
.field private static final ADD_CUSTOM_FOOD_CODE:I = 0x223d

.field private static final BAIDU_SEARCH_BASE_QUERY_CN:Ljava/lang/String; = "http://www.baidu.com/s?word="

.field private static final BAR_CODE_REQUEST_CODE:I = 0x223e

.field public static final DEFAULT_INITIAL_FRAGMENT_NAME:Ljava/lang/String; = "category"

.field private static final DISCARD_CHANGES_DIALOG_TAG:Ljava/lang/String; = "DISCARD_CHANGES_DIALOG_TAG"

.field private static final GOOGLE_SEARCH_BASE_QUERY:Ljava/lang/String; = "https://www.google.com/?#q="

.field private static final LOG_ID_BY_TAB_NAME:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/app/UserActionLog;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final NO_MEAL_ITEMS_COUNT_OBSERVED_VALUE:I = -0x1

.field private static final OK_CANCEL_DELETE_DIALOG_TAG:Ljava/lang/String; = "OK_CANCEL_DELETE_DIALOG_TAG"

.field private static final OUT_OF_RANGE_POPUP:Ljava/lang/String; = "OUT_OF_RANGE_POPUP"

.field private static final TAB_NAME_CATEGORY:Ljava/lang/String; = "category"

.field private static final TAB_NAME_FREQUENT:Ljava/lang/String; = "frequent"

.field private static final TAB_NAME_MY_FOOD:Ljava/lang/String; = "my_food"

.field private static final VOICE_SEARCH_ACTIVITY_REQUEST_CODE:I = 0x1

.field private static final sFoodInfoForPortionSize:Ljava/lang/String; = "sFoodInfoForPortionSize"


# instance fields
.field private isFromSelectPanel:Z

.field private mActionBarDoneCancelAccessor:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;

.field private mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

.field private mAutoCompleteTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

.field private mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

.field private mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

.field private mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

.field private mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

.field private mIsStateSaved:Z

.field private mLoadMoreTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

.field private mMaxAcceptableCaloriesValue:F

.field private mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

.field private mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

.field private mMealsItemsInMealCount:I

.field private mNoFoodSelectedToast:Landroid/widget/Toast;

.field private mOnAddMyOnAddCustomFoodListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

.field private mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

.field private mSearchListView:Landroid/widget/ListView;

.field private mSearchResultAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchTextWatcher:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;

.field private mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

.field private mSelectedPanelHolderForPortionSize:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

.field private mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field private searchButton:Landroid/widget/Button;

.field protected searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_TAG:Ljava/lang/String;

    .line 172
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_ID_BY_TAB_NAME:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 188
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z

    .line 190
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mIsStateSaved:Z

    .line 1455
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->finishFoodPickActivityWithResult(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFood(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mOnAddMyOnAddCustomFoodListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->performSearchFood()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->cancelAutoCompleteSearch()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mNoFoodSelectedToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showPortionSizePopup(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mNoFoodSelectedToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->setSearchText(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->saveToDbAndShow(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    return-void
.end method

.method static synthetic access$2502(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showAutoCompletePopup(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mLoadMoreTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mLoadMoreTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->performAutoCompleteSearch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isSelectedPanelEmpty()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showPortionSizePopup(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMaxAcceptableCaloriesValue:F

    return v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addSearchResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mIsStateSaved:Z

    return v0
.end method

.method static synthetic access$3600()Ljava/util/Map;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_ID_BY_TAB_NAME:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showDiscardChangesPopup()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->finishFoodPick()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanelHolderForPortionSize:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshDoneButtonState()V

    return-void
.end method

.method private actionBack()Z
    .locals 21

    .prologue
    .line 733
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isSelectedPanelEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 734
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->checkCaloriesRangeAndShowPopup()Z

    move-result v4

    if-nez v4, :cond_0

    .line 735
    const/4 v4, 0x1

    .line 769
    :goto_0
    return v4

    .line 737
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "edit_meal_back_press_save"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 740
    .local v13, "extra":Ljava/lang/String;
    if-eqz v13, :cond_1

    const-string v4, "edit_meal_back_press_save"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 741
    new-instance v14, Landroid/os/Bundle;

    invoke-direct {v14}, Landroid/os/Bundle;-><init>()V

    .line 742
    .local v14, "extras":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v16

    .line 744
    .local v16, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    const-string v4, "MEAL_ITEM_LIST"

    move-object/from16 v0, v16

    invoke-virtual {v14, v4, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 745
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->finishFoodPickActivityWithResult(Landroid/os/Bundle;)V

    .line 746
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090080

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 747
    const/4 v4, 0x1

    goto :goto_0

    .line 749
    .end local v14    # "extras":Landroid/os/Bundle;
    .end local v16    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    :cond_1
    new-instance v19, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-direct {v0, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 751
    .local v19, "mealItemDao":Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v4

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 752
    new-instance v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    const-wide/16 v4, -0x1

    const-string v6, ""

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getCaloriesValueInSelectedPanel()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealType()J

    move-result-wide v8

    long-to-int v8, v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealTime()J

    move-result-wide v9

    const-string v11, ""

    invoke-direct/range {v3 .. v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>(JLjava/lang/String;FIJLjava/lang/String;)V

    .line 755
    .local v3, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 756
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v17

    .line 763
    .end local v3    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .local v17, "mealId":J
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 764
    .local v20, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    move-object/from16 v0, v20

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setMealId(J)V

    goto :goto_2

    .line 758
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v17    # "mealId":J
    .end local v20    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v17

    .line 759
    .restart local v17    # "mealId":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    move-wide/from16 v0, v17

    invoke-interface {v4, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 760
    .local v12, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v4

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getCaloriesValueInSelectedPanel()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual {v12, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 761
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-static {v12, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    goto :goto_1

    .line 766
    .end local v12    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .restart local v15    # "i$":Ljava/util/Iterator;
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->insertOrUpdateData(Ljava/util/List;)Ljava/util/List;

    .line 767
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f090080

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 769
    .end local v13    # "extra":Ljava/lang/String;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v17    # "mealId":J
    .end local v19    # "mealItemDao":Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method private addCustomFood()V
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x0

    check-cast v0, Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFood(Landroid/os/Bundle;)V

    .line 500
    return-void
.end method

.method private addCustomFood(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 503
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 504
    .local v1, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 505
    invoke-virtual {v1, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 508
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 511
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 514
    .end local v0    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    const/16 v2, 0x223d

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 515
    return-void
.end method

.method private addCustomFood(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 493
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 494
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "FOOD_NAME_NOT_FOUND"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFood(Landroid/os/Bundle;)V

    .line 496
    return-void
.end method

.method private addSearchResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1043
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->generateListItems(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)Ljava/util/List;

    move-result-object v0

    .line 1044
    .local v0, "foodItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->setHighlightToListItems(Ljava/lang/String;Ljava/util/List;)V

    .line 1045
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchResultAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->addListItems(Ljava/util/List;)V

    .line 1046
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchResultAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->notifyDataSetChanged()V

    .line 1047
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->updateScrollListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    .line 1048
    return-void
.end method

.method private cancelAutoCompleteSearch()V
    .locals 2

    .prologue
    .line 977
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompleteTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompleteTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->removeTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Z

    .line 979
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompleteTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .line 981
    :cond_0
    return-void
.end method

.method private cancelSearchFood()V
    .locals 2

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    if-eqz v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->removeTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Z

    .line 939
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .line 941
    :cond_0
    return-void
.end method

.method private checkCaloriesRangeAndShowPopup()Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 781
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v8

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataForPeriod(JJ)Ljava/util/List;

    move-result-object v3

    .line 784
    .local v3, "mealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    const v2, 0x47c34f80    # 99999.0f

    .line 785
    .local v2, "max":F
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 786
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v5

    sub-float/2addr v2, v5

    .line 787
    goto :goto_0

    .line 788
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getCaloriesValueInSelectedPanel()F

    move-result v5

    cmpl-float v5, v5, v2

    if-lez v5, :cond_1

    .line 789
    iput v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMaxAcceptableCaloriesValue:F

    .line 790
    new-instance v5, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const v7, 0x7f0900e3

    invoke-direct {v5, v6, v4, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v6, 0x7f030009

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "OUT_OF_RANGE_POPUP"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 795
    :goto_1
    return v4

    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private findMyFoodItems(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1032
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1033
    .local v3, "myFoodItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 1034
    .local v1, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMyFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 1035
    .local v0, "foodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1036
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    invoke-direct {v4, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1039
    .end local v0    # "foodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_1
    return-object v3
.end method

.method private findViews(Landroid/view/View;)V
    .locals 8
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 353
    const v5, 0x7f080462

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    .line 354
    const v5, 0x7f080463

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/app/FragmentTabHost;

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    .line 356
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-virtual {v5, v7}, Landroid/support/v4/app/FragmentTabHost;->setFocusable(Z)V

    .line 357
    const v5, 0x7f08045c

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    .line 358
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$5;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->setFocusHandler(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$FocusHandler;)V

    .line 366
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$6;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$6;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 380
    const v5, 0x7f08046a

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchButton:Landroid/widget/Button;

    .line 381
    const v5, 0x7f080469

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$7;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$7;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 389
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchButton:Landroid/widget/Button;

    const v6, 0x7f09095e

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(I)V

    .line 393
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchButton:Landroid/widget/Button;

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$8;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$8;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 414
    const/4 v3, 0x0

    .line 415
    .local v3, "needToShowLeftDivider":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;

    invoke-interface {v5}, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;->getConfiguration()Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

    move-result-object v2

    .line 416
    .local v2, "foodConfiguration":Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;->isBarcodeSearchEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 417
    const v5, 0x7f08045f

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 418
    .local v1, "barcodeButton":Landroid/view/View;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$9;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$9;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 426
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 427
    const/4 v3, 0x1

    .line 430
    .end local v1    # "barcodeButton":Landroid/view/View;
    :cond_0
    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;->isVoiceSearchEnabled()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 431
    const v5, 0x7f08045d

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 432
    .local v4, "voiceSearchButton":Landroid/view/View;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$10;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$10;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 441
    if-eqz v3, :cond_1

    .line 442
    const v5, 0x7f08045e

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 444
    :cond_1
    const/4 v3, 0x1

    .line 447
    .end local v4    # "voiceSearchButton":Landroid/view/View;
    :cond_2
    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;->isAddFoodEnabled()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 448
    const v5, 0x7f080461

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 449
    .local v0, "addFoodButton":Landroid/view/View;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$11;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$11;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 458
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 459
    if-eqz v3, :cond_3

    .line 460
    const v5, 0x7f080460

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 464
    .end local v0    # "addFoodButton":Landroid/view/View;
    :cond_3
    const v5, 0x7f08045b

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->removeFocusabilityOnFirstFocusLost(Landroid/view/View;)V

    .line 465
    return-void

    .line 391
    .end local v2    # "foodConfiguration":Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    .end local v3    # "needToShowLeftDivider":Z
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchButton:Landroid/widget/Button;

    const v6, 0x7f09095d

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0
.end method

.method private finishFoodPick()V
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 334
    return-void
.end method

.method private finishFoodPickActivityWithResult(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "CALLING_ACTIVITY_CLASS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 339
    .local v0, "activityClass":Ljava/lang/Class;
    if-nez v0, :cond_0

    .line 340
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    .line 342
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 343
    .local v1, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_1

    .line 344
    invoke-virtual {v1, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 346
    :cond_1
    const-string v2, "MEAL_DATA_HOLDER"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 347
    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 348
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->startActivity(Landroid/content/Intent;)V

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 350
    return-void
.end method

.method public static generateListItems(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            "*>;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1082
    .local p1, "foodInfoData":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;*>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1083
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 1084
    .local v0, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getSearchResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 1085
    .local v1, "foodInfoDataItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    invoke-direct {v4, v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1087
    .end local v1    # "foodInfoDataItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_0
    return-object v3
.end method

.method private getCaloriesValueInSelectedPanel()F
    .locals 4

    .prologue
    .line 773
    const/4 v2, 0x0

    .line 774
    .local v2, "mealKcal":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 775
    .local v1, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorie(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 776
    goto :goto_0

    .line 777
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_0
    return v2
.end method

.method private getCurrentFragmentName(Landroid/os/Bundle;Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 579
    .local p2, "tabViewMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    if-eqz p1, :cond_0

    const-string v2, "CURRENT_FOOD_PICK_FRAGMENT_TAG"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 580
    const-string v2, "CURRENT_FOOD_PICK_FRAGMENT_TAG"

    const-string v3, "category"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 587
    .local v0, "currentFragmentName":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 583
    .end local v0    # "currentFragmentName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->getFoodPickLastFragmentName()Ljava/lang/String;

    move-result-object v0

    .line 584
    .restart local v0    # "currentFragmentName":Ljava/lang/String;
    const-string v2, "category"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;

    iget-object v1, v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;->tabView:Landroid/view/View;

    check-cast v1, Landroid/widget/TextView;

    .line 585
    .local v1, "tabTextView":Landroid/widget/TextView;
    const-string/jumbo v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method

.method private getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1206
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMealItemsInMealCount()I
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 1302
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "MEAL_ITEMS_COUNT"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1303
    .local v1, "mealItemsCount":I
    if-eq v1, v4, :cond_0

    .line 1309
    .end local v1    # "mealItemsCount":I
    :goto_0
    return v1

    .line 1305
    .restart local v1    # "mealItemsCount":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1306
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 1307
    .local v0, "mealItemDao":Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    .line 1309
    .end local v0    # "mealItemDao":Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getTabViewInformationMap(Landroid/view/ViewGroup;)Ljava/util/Map;
    .locals 17
    .param p1, "parentView"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 557
    .local v8, "frequentArgs":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealType()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/food/constants/MealType;->getMealTypeByMealTypeId(I)Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v9

    .line 558
    .local v9, "mMealType":Lcom/sec/android/app/shealth/food/constants/MealType;
    const-string v2, "MEAL_TYPE"

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/constants/MealType;->ordinal()I

    move-result v3

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 560
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->prepareMyFoodParams()Landroid/os/Bundle;

    move-result-object v10

    .line 562
    .local v10, "myFoodArgs":Landroid/os/Bundle;
    new-instance v11, Ljava/util/LinkedHashMap;

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_ID_BY_TAB_NAME:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v11, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 563
    .local v11, "tabViewMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    const-string v12, "category"

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;

    const v3, 0x7f090086

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->inflateTab(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v13, 0x7f090086

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v13, 0x7f090fb0

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const/16 v16, 0x3

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v7, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;-><init>(Landroid/view/View;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;)V

    invoke-interface {v11, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    const-string v12, "frequent"

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;

    const v3, 0x7f090085

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->inflateTab(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090085

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090fb0

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v6, v7, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object v5, v8

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;-><init>(Landroid/view/View;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;)V

    invoke-interface {v11, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    const-string/jumbo v12, "my_food"

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;

    const v3, 0x7f090944

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->inflateTab(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090944

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090fb0

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v6, v7, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object v5, v10

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;-><init>(Landroid/view/View;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;)V

    invoke-interface {v11, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    return-object v11
.end method

.method private inflateTab(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4
    .param p1, "parentView"    # Landroid/view/ViewGroup;
    .param p2, "stringId"    # I

    .prologue
    .line 644
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03010b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 646
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f08046c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 647
    return-object v0
.end method

.method private init(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "saveInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 220
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->initPortionSizePopup(Landroid/os/Bundle;)V

    .line 221
    if-eqz p1, :cond_0

    const-string v0, "MAX_ACCEPTABLE_CALORIES_VALUE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "MAX_ACCEPTABLE_CALORIES_VALUE"

    iget v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMaxAcceptableCaloriesValue:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMaxAcceptableCaloriesValue:F

    .line 224
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "MEAL_DATA_HOLDER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunnerAccessor;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunnerAccessor;->getTaskRunner()Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 227
    new-instance v0, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mActionBarDoneCancelAccessor:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mActionBarDoneCancelAccessor:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;->setOnDoneClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mActionBarDoneCancelAccessor:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;->setOnCancelClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;)V

    .line 263
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getMealItemsInMealCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealsItemsInMealCount:I

    .line 264
    return-void
.end method

.method private initPortionSizePopup(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "saveInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 271
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;-><init>(Landroid/app/Activity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .line 272
    if-eqz p1, :cond_0

    const-string/jumbo v0, "sFoodInfoForPortionSize"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 274
    const-string/jumbo v0, "sFoodInfoForPortionSize"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizePopupResultListener(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;)V

    .line 323
    return-void
.end method

.method private initSearch()V
    .locals 2

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchFactory;->getSearchApi(Landroid/content/Context;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .line 652
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchTextWatcher:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchTextWatcher:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->setOnSearchKeyClickListener(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;)V

    .line 676
    return-void
.end method

.method private initSelectedPanel()V
    .locals 3

    .prologue
    .line 679
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;->getConfiguration()Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

    move-result-object v0

    .line 680
    .local v0, "foodConfig":Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;->isEditSelectedItemEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 681
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$14;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->setOnSelectedItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemClickListener;)V

    .line 688
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$15;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->setOnSelectedItemRemovedListener(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;)V

    .line 695
    return-void
.end method

.method private initTabWidget()V
    .locals 2

    .prologue
    .line 570
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    .line 572
    .local v0, "tabWidget":Landroid/widget/TabWidget;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->setFocusable(Z)V

    .line 573
    const v1, 0x7f02089c

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->setDividerDrawable(I)V

    .line 574
    const v1, 0x7f0207da

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->setBackgroundResource(I)V

    .line 575
    return-void
.end method

.method private initTabs(Landroid/view/ViewGroup;Landroid/os/Bundle;)V
    .locals 12
    .param p1, "parentView"    # Landroid/view/ViewGroup;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 519
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    const v11, 0x7f080464

    invoke-virtual {v8, v9, v10, v11}, Landroid/support/v4/app/FragmentTabHost;->setup(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;I)V

    .line 520
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentTabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TabWidget;->setShowDividers(I)V

    .line 521
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getTabViewInformationMap(Landroid/view/ViewGroup;)Ljava/util/Map;

    move-result-object v7

    .line 522
    .local v7, "tabViewMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 523
    .local v6, "tabEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;

    .line 524
    .local v5, "tab":Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    iget-object v10, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Landroid/support/v4/app/FragmentTabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v8

    iget-object v10, v5, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;->tabView:Landroid/view/View;

    invoke-virtual {v8, v10}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v8

    iget-object v10, v5, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;->fragmentClass:Ljava/lang/Class;

    iget-object v11, v5, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;->startArguments:Landroid/os/Bundle;

    invoke-virtual {v9, v8, v10, v11}, Landroid/support/v4/app/FragmentTabHost;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 527
    .end local v5    # "tab":Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;
    .end local v6    # "tabEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$TabViewInformation;>;"
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->initTabWidget()V

    .line 529
    invoke-direct {p0, p2, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getCurrentFragmentName(Landroid/os/Bundle;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 530
    .local v1, "currentFragmentName":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    new-instance v9, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;

    invoke-direct {v9, p0, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$OnTabChangeListenerForFoodPick;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/util/Map;)V

    invoke-virtual {v8, v9}, Landroid/support/v4/app/FragmentTabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 531
    const/4 v0, -0x1

    .line 532
    .local v0, "currentFragment":I
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 533
    .local v2, "fragmentNameIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 534
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 535
    move v0, v3

    .line 539
    :cond_1
    if-gez v0, :cond_3

    .line 540
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    const-string v9, "category"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->putFoodPickLastFragmentName(Ljava/lang/String;)V

    .line 541
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Wrong fragment name: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 533
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 544
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-virtual {v8, v0}, Landroid/support/v4/app/FragmentTabHost;->setCurrentTab(I)V

    .line 546
    if-nez v0, :cond_4

    .line 547
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v8, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_ID_BY_TAB_NAME:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v9, v8}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 553
    :cond_4
    return-void
.end method

.method private isFoodSearchRunning()Z
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSelectedPanelEmpty()Z
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performAutoCompleteSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 944
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->isAutoCompleteSupported()Z

    move-result v0

    if-nez v0, :cond_1

    .line 974
    :cond_0
    :goto_0
    return-void

    .line 948
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->cancelAutoCompleteSearch()V

    .line 949
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getAutoCompleteMinLength()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 953
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompleteTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .line 973
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompleteTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0
.end method

.method private performSearchFood()V
    .locals 5

    .prologue
    .line 859
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 860
    .local v0, "editText":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 861
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->cancelAutoCompleteSearch()V

    .line 862
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 863
    .local v1, "requestString":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "performSearchFood: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 865
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 866
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/app/Activity;)V

    .line 867
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->cancelSearchFood()V

    .line 868
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .line 890
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 891
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TEXT_SEARCH_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 897
    :cond_0
    :goto_0
    return-void

    .line 893
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f080466

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 894
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f080467

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private prepareMyFoodParams()Landroid/os/Bundle;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 620
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 621
    .local v3, "params":Landroid/os/Bundle;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 623
    .local v0, "holdersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;>;"
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    const v4, 0x7f090944

    invoke-direct {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>(I)V

    .line 624
    .local v1, "myFoodCategory":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFavoriteStarsVisibility(Z)V

    .line 625
    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 626
    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsExpandable(Z)V

    .line 627
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;-><init>()V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V

    .line 628
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 630
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    const v4, 0x7f090977

    invoke-direct {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>(I)V

    .line 631
    .local v2, "myMealCategory":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFavoriteStarsVisibility(Z)V

    .line 632
    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 633
    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsExpandable(Z)V

    .line 634
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;-><init>()V

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V

    .line 635
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    const-string v4, "CATEGORY_HOLDERS_LIST"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 638
    const-string/jumbo v4, "single_category_title_visible"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 639
    const-string v4, "ADD_CUSTOM_FOOD_LAYOUT_IS_VISIBLE"

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 640
    return-object v3
.end method

.method private prepareParamsForExpandableListFragment()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1485
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1486
    .local v3, "params":Landroid/os/Bundle;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1487
    .local v0, "holdersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;>;"
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    const v4, 0x7f090944

    invoke-direct {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>(I)V

    .line 1488
    .local v1, "myFoodCategory":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 1489
    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsExpandable(Z)V

    .line 1490
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;-><init>()V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V

    .line 1491
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1492
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    const v4, 0x7f090977

    invoke-direct {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>(I)V

    .line 1493
    .local v2, "myMealCategory":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 1494
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsExpandable(Z)V

    .line 1495
    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyMealCategoryCreator;-><init>()V

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V

    .line 1496
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1497
    const-string v4, "CATEGORY_HOLDERS_LIST"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1498
    const-string/jumbo v4, "single_category_title_visible"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1499
    return-object v3
.end method

.method private refreshDoneButtonState()V
    .locals 3

    .prologue
    .line 1318
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mActionBarDoneCancelAccessor:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;->setDoneButtonEnabled(Z)V

    .line 1319
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1321
    .local v0, "listOfView":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1322
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1325
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1326
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1328
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshFragmentFocusable(Ljava/util/List;)V

    .line 1330
    return-void
.end method

.method private refreshFrequentFragment()V
    .locals 3

    .prologue
    .line 1174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "frequent"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;

    .line 1175
    .local v0, "frequentFragment":Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;
    if-eqz v0, :cond_0

    .line 1176
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/FrequentFragment;->reload()V

    .line 1178
    :cond_0
    return-void
.end method

.method private refreshMyFoodFragment()V
    .locals 3

    .prologue
    .line 1167
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "my_food"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .line 1168
    .local v0, "myFoodFragment":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1169
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->prepareMyFoodParams()Landroid/os/Bundle;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->updateFragmentState(Landroid/os/Bundle;Z)V

    .line 1171
    :cond_0
    return-void
.end method

.method private removeFocusabilityOnFirstFocusLost(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 468
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$12;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 477
    return-void
.end method

.method private saveToDbAndShow(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 911
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->updateFavoritesFromDB(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    .line 912
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showSearchResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    .line 913
    return-void
.end method

.method private static setHighlightToListItems(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p0, "highlightedSelection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1091
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .line 1092
    .local v0, "foodListItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;
    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->setSelection(Ljava/lang/String;)V

    goto :goto_0

    .line 1094
    .end local v0    # "foodListItem":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;
    :cond_0
    return-void
.end method

.method private setSearchText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 904
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchTextWatcher:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->setEnabled(Z)V

    .line 905
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->editSearchPhrase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 906
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchTextWatcher:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$SearchTextWatcher;->setEnabled(Z)V

    .line 907
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->setCursorPosition()V

    .line 908
    return-void
.end method

.method private showAutoCompletePopup(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "searchResult":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult<Ljava/util/List<Ljava/lang/String;>;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;>;"
    const/4 v4, 0x1

    .line 814
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFoodSearchRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 852
    :cond_0
    :goto_0
    return-void

    .line 817
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 818
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 819
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    goto :goto_0

    .line 823
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 824
    .local v0, "editText":Landroid/widget/EditText;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    if-nez v1, :cond_3

    .line 825
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v2, v3, v1, v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .line 826
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$16;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setOnItemClickedListener(Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;)V

    .line 840
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setInputMethodMode(I)V

    .line 841
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setOutsideTouchable(Z)V

    .line 842
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a077a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setHeight(I)V

    .line 846
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->getRequest()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0701d1

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setTextSelection(Ljava/lang/String;I)V

    .line 848
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 849
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setFocusable(Z)V

    .line 850
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->show()V

    goto/16 :goto_0

    .line 844
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;->getSearchResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setNewItems(Ljava/util/List;)V

    goto :goto_1
.end method

.method private showDiscardChangesPopup()V
    .locals 4

    .prologue
    .line 326
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    const v3, 0x7f090081

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "DISCARD_CHANGES_DIALOG_TAG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 330
    return-void
.end method

.method private showPortionSizePopup(IFLcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 23
    .param p1, "mealUnitType"    # I
    .param p2, "mealAmount"    # F
    .param p3, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 1244
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v21

    .line 1245
    .local v21, "selectedCount":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealsItemsInMealCount:I

    add-int v5, v5, v21

    const/16 v6, 0xf

    if-lt v5, v6, :cond_0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z

    if-eqz v5, :cond_5

    .line 1246
    :cond_0
    new-instance v17, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-direct {v0, v5}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 1247
    .local v17, "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v5

    move-object/from16 v0, v17

    invoke-interface {v0, v5, v6}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v18

    .line 1248
    .local v18, "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    if-eqz v18, :cond_3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1251
    const v5, 0x7f0900bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1252
    .local v19, "gramShortName":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1254
    const v4, 0x1d4c2

    .line 1255
    .local v4, "mealUnit":I
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v3

    .line 1262
    .local v3, "quantity":F
    :goto_0
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v5

    const/4 v6, 0x1

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v8

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v9

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v11

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v12

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v13

    invoke-direct/range {v2 .. v13}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;-><init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V

    .line 1268
    .local v2, "mPortionSizeHolder":Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0900c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getUnitName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1272
    const v5, 0x1d4c2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->setUnit(I)V

    .line 1273
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getUnit()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->setQuantity(F)V

    .line 1274
    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->setUnitName(Ljava/lang/String;)V

    .line 1277
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizeData(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    .line 1293
    .end local v2    # "mPortionSizeHolder":Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
    .end local v3    # "quantity":F
    .end local v4    # "mealUnit":I
    .end local v19    # "gramShortName":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 1294
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->showPortionSizePopup(Landroid/support/v4/app/FragmentManager;)V

    .line 1299
    .end local v17    # "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    .end local v18    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :goto_2
    return-void

    .line 1258
    .restart local v17    # "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    .restart local v18    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .restart local v19    # "gramShortName":Ljava/lang/String;
    :cond_2
    move/from16 v4, p1

    .line 1259
    .restart local v4    # "mealUnit":I
    move/from16 v3, p2

    .restart local v3    # "quantity":F
    goto/16 :goto_0

    .line 1279
    .end local v3    # "quantity":F
    .end local v4    # "mealUnit":I
    .end local v19    # "gramShortName":Ljava/lang/String;
    :cond_3
    if-eqz v18, :cond_4

    .line 1281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    move-object/from16 v22, v0

    new-instance v5, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v8

    const/4 v9, 0x1

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const v6, 0x7f09017b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v14

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v15

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v16

    move/from16 v6, p2

    move/from16 v7, p1

    invoke-direct/range {v5 .. v16}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;-><init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizeData(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    goto :goto_1

    .line 1286
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    move-object/from16 v22, v0

    new-instance v5, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v8

    const/4 v9, 0x1

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const v6, 0x7f09017b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v6, p2

    move/from16 v7, p1

    invoke-direct/range {v5 .. v16}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;-><init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizeData(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    goto :goto_1

    .line 1296
    .end local v17    # "extraFoodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;
    .end local v18    # "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :cond_5
    const v5, 0x7f090925

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/16 v8, 0xf

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 1297
    .local v20, "message":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-static {v5, v0, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2
.end method

.method private showPortionSizePopup(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 2
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 1237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z

    .line 1238
    const v0, 0x1d4c1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showPortionSizePopup(IFLcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 1240
    return-void
.end method

.method private showPortionSizePopup(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)V
    .locals 5
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    .line 1229
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getMealItemData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    move-result-object v1

    .line 1230
    .local v1, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    .line 1231
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanelHolderForPortionSize:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .line 1232
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z

    .line 1233
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v3

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showPortionSizePopup(IFLcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 1234
    return-void
.end method

.method public static updateFavoritesFromDB(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 922
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;*>;"
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFavoritesFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 923
    .local v1, "favorites":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getSearchResult()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 924
    .local v2, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 925
    .local v0, "favoriteFoodData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v5

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v6

    if-ne v5, v6, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 927
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 928
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setId(J)V

    .line 929
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setkCal(F)V

    goto :goto_0

    .line 934
    .end local v0    # "favoriteFoodData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method private updateScrollListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1051
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$20;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1071
    return-void
.end method


# virtual methods
.method public addCustomFoodSavedToMyFood()V
    .locals 3

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isMyFoodItemsLimitExceed(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->showToastAboutMyFoodItemsLimitExceeded(Landroid/content/Context;)V

    .line 490
    :goto_0
    return-void

    .line 487
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 488
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v1, "SHOULD_ALWAYS_SAVE_TO_MY_FOOD"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 489
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFood(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public addFoodItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 4
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 1190
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSubCategory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 1191
    .local v0, "isFoodValid":Z
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isExtraFoodInfoDataDownloadRequired(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_2

    .line 1192
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$21;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 1203
    :goto_1
    return-void

    .line 1190
    .end local v0    # "isFoodValid":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1201
    .restart local v0    # "isFoodValid":Z
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showPortionSizePopup(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    goto :goto_1
.end method

.method public addFoodItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)V
    .locals 4
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    .line 1216
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    if-ne v2, v3, :cond_0

    .line 1218
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 1219
    .local v0, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 1220
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addFoodItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 1226
    .end local v0    # "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :goto_0
    return-void

    .line 1222
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->putItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z

    .line 1223
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshFragmentFocusables()V

    .line 1224
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshDoneButtonState()V

    goto :goto_0
.end method

.method public backPressed()Z
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 727
    const/4 v0, 0x1

    .line 729
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->actionBack()Z

    move-result v0

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1334
    const-string v0, "OUT_OF_RANGE_POPUP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1335
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$22;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 1346
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1351
    const-string v1, "DISCARD_CHANGES_DIALOG_TAG"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1352
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$23;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 1367
    :cond_0
    :goto_0
    return-object v0

    .line 1360
    :cond_1
    const-string v1, "OUT_OF_RANGE_POPUP"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1362
    const-string v1, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1363
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0

    .line 1364
    :cond_2
    const-string v1, "OK_CANCEL_DELETE_DIALOG_TAG"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1365
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$EditMealDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$1;)V

    move-object v0, v1

    goto :goto_0

    .line 1367
    :cond_3
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDialogNotSupported(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method public hideSearchResults()Z
    .locals 3

    .prologue
    .line 803
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080465

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 804
    .local v0, "searchedItemHolder":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 805
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 806
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 807
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshFrequentFragment()V

    .line 808
    const/4 v1, 0x1

    .line 810
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAutoCompleteShowing()Z
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1122
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1123
    const/4 v5, -0x1

    if-ne p2, v5, :cond_0

    .line 1124
    sparse-switch p1, :sswitch_data_0

    .line 1161
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Wrong request code"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1126
    :sswitch_0
    if-eqz p3, :cond_0

    .line 1127
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshMyFoodFragment()V

    .line 1128
    const-string/jumbo v5, "result_my_food_id"

    const-wide/16 v6, -0x1

    invoke-virtual {p3, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1129
    .local v2, "id":J
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 1130
    .local v0, "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 1131
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz v1, :cond_1

    .line 1132
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addFoodItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 1136
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->hideSearchResults()Z

    .line 1164
    .end local v0    # "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "id":J
    :cond_0
    :goto_0
    return-void

    .line 1134
    .restart local v0    # "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .restart local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .restart local v2    # "id":J
    :cond_1
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FoodInfoData was not found in DB by id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1140
    .end local v0    # "db":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v2    # "id":J
    :sswitch_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1141
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "FAT_SECRET_RESULT_KEY"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1142
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFood(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1144
    :cond_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "FAT_SECRET_RESULT_KEY"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;

    .line 1145
    .local v4, "res":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;>;"
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getRequest()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->setSearchText(Ljava/lang/String;)V

    .line 1146
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showSearchResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    goto :goto_0

    .line 1149
    .end local v4    # "res":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;>;"
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFood()V

    goto :goto_0

    .line 1153
    :sswitch_2
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 1154
    const-string v5, "BAR_CODE_RESULT_FOOD_INFO"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 1155
    .restart local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addFoodItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    goto :goto_0

    .line 1157
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFood()V

    goto :goto_0

    .line 1124
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x223d -> :sswitch_0
        0x223e -> :sswitch_2
    .end sparse-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 213
    instance-of v0, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 214
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mOnAddMyOnAddCustomFoodListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;

    .line 216
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    .line 217
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 194
    const v1, 0x7f03010a

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 195
    .local v0, "contentView":Landroid/view/ViewGroup;
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mIsStateSaved:Z

    .line 196
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->init(Landroid/os/Bundle;)V

    .line 197
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->findViews(Landroid/view/View;)V

    .line 198
    invoke-direct {p0, v0, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->initTabs(Landroid/view/ViewGroup;Landroid/os/Bundle;)V

    .line 199
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->initSearch()V

    .line 200
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->initSelectedPanel()V

    .line 201
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 700
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 701
    .local v0, "currentAndroidVersion":I
    const/4 v1, 0x0

    .line 703
    .local v1, "isAttachedToWindow":Z
    const/16 v2, 0x13

    if-lt v0, v2, :cond_2

    .line 705
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTabHost;->isAttachedToWindow()Z

    move-result v1

    .line 711
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 712
    const-string v2, "CURRENT_FOOD_PICK_FRAGMENT_TAG"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodPickTabHost:Landroid/support/v4/app/FragmentTabHost;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTabHost;->getCurrentTab()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 713
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    if-eqz v2, :cond_0

    .line 714
    const-string/jumbo v2, "sFoodInfoForPortionSize"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 716
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 717
    const-string v2, "MAX_ACCEPTABLE_CALORIES_VALUE"

    iget v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mMaxAcceptableCaloriesValue:F

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 719
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mIsStateSaved:Z

    .line 720
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 721
    return-void

    .line 709
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 206
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 207
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshDoneButtonState()V

    .line 208
    return-void
.end method

.method public showSearchResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;>;"
    const v9, 0x7f080467

    const v8, 0x7f080466

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 987
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080465

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 988
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->generateListItems(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)Ljava/util/List;

    move-result-object v3

    .line 989
    .local v3, "foodItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getRequest()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->findMyFoodItems(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 990
    .local v2, "myFoodItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->hasLogo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 991
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08046b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 992
    .local v6, "logo":Landroid/widget/ImageView;
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 993
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getLogoResourceId()I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 995
    .end local v6    # "logo":Landroid/widget/ImageView;
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 996
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getRequest()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->setSearchText(Ljava/lang/String;)V

    .line 997
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->setHighlightToListItems(Ljava/lang/String;Ljava/util/List;)V

    .line 998
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->setHighlightToListItems(Ljava/lang/String;Ljava/util/List;)V

    .line 999
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    .line 1000
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1001
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1003
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v4, 0x7f090944

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f090945

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchResultAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    .line 1005
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchResultAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    new-instance v4, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    invoke-direct {v4, v5, v8, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;)V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->addOnFavoriteClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;)V

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchResultAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$19;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->setOnItemClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)V

    .line 1017
    new-instance v7, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1018
    .local v7, "view":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchResultAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1020
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 1021
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->updateScrollListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSearchListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 1029
    .end local v7    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 1026
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1027
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public toggleActivityState(Z)V
    .locals 4
    .param p1, "dataSetWasInvalidated"    # Z

    .prologue
    .line 1474
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->prepareParamsForExpandableListFragment()Landroid/os/Bundle;

    move-result-object v1

    .line 1475
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "my_food"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    .line 1476
    .local v0, "myFoodFragment":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;
    if-eqz v0, :cond_0

    .line 1478
    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->updateFragmentState(Landroid/os/Bundle;Z)V

    .line 1481
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshMyFoodFragment()V

    .line 1482
    return-void
.end method
