.class public final Lcom/sec/android/app/shealth/help/HelpTextView;
.super Landroid/widget/TextView;
.source "HelpTextView.java"


# static fields
.field private static final ICON_HTML_END:Ljava/lang/String; = "\"/>"

.field private static final ICON_HTML_START:Ljava/lang/String; = "<img src=\"@drawable/"

.field static bNewLineCheck:Z

.field static bNewLineOrentation:Z

.field static finalString:Ljava/lang/String;

.field static mNewLineString:Ljava/lang/String;

.field static mShowIntegerString:Ljava/lang/String;

.field static mShowString:Ljava/lang/String;


# instance fields
.field private mAddItemIds:[I

.field private final mContext:Landroid/content/Context;

.field private final mDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/help/HelpDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mImageYDiff:F

.field private final mImgGetter:Landroid/text/Html$ImageGetter;

.field private mInsideImageGravity:I

.field private mInsideImageHeight:F

.field private mInsideImagePadding:Landroid/graphics/Rect;

.field private mInsideImageWidth:F

.field mStorevalue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mImageYDiff:F

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mDrawables:Ljava/util/ArrayList;

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    .line 51
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageGravity:I

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mStorevalue:Ljava/lang/String;

    .line 245
    new-instance v0, Lcom/sec/android/app/shealth/help/HelpTextView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/help/HelpTextView$1;-><init>(Lcom/sec/android/app/shealth/help/HelpTextView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    .line 76
    iput-object p1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mImageYDiff:F

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mDrawables:Ljava/util/ArrayList;

    .line 46
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    .line 51
    const/16 v1, 0x11

    iput v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageGravity:I

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mStorevalue:Ljava/lang/String;

    .line 245
    new-instance v1, Lcom/sec/android/app/shealth/help/HelpTextView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/help/HelpTextView$1;-><init>(Lcom/sec/android/app/shealth/help/HelpTextView;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    .line 91
    iput-object p1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 95
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineOrentation:Z

    .line 100
    :goto_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->initSelfResources(Landroid/util/AttributeSet;)V

    .line 101
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->CheckInteger(Ljava/lang/String;Landroid/util/AttributeSet;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->IntegerinitSelfResources(Landroid/util/AttributeSet;)V

    .line 103
    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->CheckString(Ljava/lang/String;Landroid/util/AttributeSet;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->StringinitSelfResources(Landroid/util/AttributeSet;)V

    .line 105
    :cond_1
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->CheckNewLine(Ljava/lang/String;Landroid/util/AttributeSet;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 106
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->NewLineinitSelfResources(Landroid/util/AttributeSet;)V

    .line 108
    :cond_2
    return-void

    .line 97
    :cond_3
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineOrentation:Z

    goto :goto_0
.end method

.method private Changespecialchartointeger(Ljava/lang/String;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x0

    .line 295
    iget-object v7, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v8, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v7, p2, v8, v9, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 300
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v7, 0x9

    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 301
    .local v2, "id":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 303
    iget-object v7, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 305
    .local v4, "mStrings":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 306
    .local v3, "index":I
    const-string v5, "$d"

    .line 307
    .local v5, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 308
    .local v6, "pos":I
    :goto_0
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v7, 0x3

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 310
    .local v1, "builder":Ljava/lang/StringBuilder;
    add-int/lit8 v7, v6, -0x2

    invoke-virtual {p1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    array-length v7, v4

    if-ge v3, v7, :cond_0

    .line 312
    aget-object v7, v4, v3

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :cond_0
    add-int/lit8 v7, v6, 0x2

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 317
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 318
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    add-int/lit8 v3, v3, 0x1

    .line 320
    goto :goto_0

    .line 322
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_1
    return-void
.end method

.method private Changespecialchartostring(Ljava/lang/String;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x0

    .line 351
    iget-object v7, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v8, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v7, p2, v8, v9, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 354
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v7, 0xa

    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 355
    .local v2, "id":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 357
    iget-object v7, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 359
    .local v4, "mStrings":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 360
    .local v3, "index":I
    const-string v5, "$s"

    .line 361
    .local v5, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 362
    .local v6, "pos":I
    :goto_0
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 363
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v7, 0x3

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 364
    .local v1, "builder":Ljava/lang/StringBuilder;
    add-int/lit8 v7, v6, -0x2

    invoke-virtual {p1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    array-length v7, v4

    if-ge v3, v7, :cond_0

    .line 366
    aget-object v7, v4, v3

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    :cond_0
    add-int/lit8 v7, v6, 0x2

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 371
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 372
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    add-int/lit8 v3, v3, 0x1

    .line 374
    goto :goto_0

    .line 375
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_1
    return-void
.end method

.method private CheckInteger(Ljava/lang/String;Landroid/util/AttributeSet;)Z
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v3, p2, v4, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 271
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v3, 0x9

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 273
    .local v1, "iconsArrayId":I
    if-lez v1, :cond_1

    .line 274
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/help/HelpTextView;->checkspecialcharacterforinteger(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->Changespecialchartointeger(Ljava/lang/String;Landroid/util/AttributeSet;)V

    .line 277
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 278
    const/4 v2, 0x1

    .line 281
    :goto_0
    return v2

    .line 280
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private CheckNewLine(Ljava/lang/String;Landroid/util/AttributeSet;)Z
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 378
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v4, p2, v5, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 380
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v4, 0xb

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 382
    .local v1, "iconsArrayId":I
    if-lez v1, :cond_0

    .line 383
    sput-boolean v2, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineCheck:Z

    .line 384
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 389
    :goto_0
    return v2

    .line 387
    :cond_0
    sput-boolean v3, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineCheck:Z

    .line 388
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    move v2, v3

    .line 389
    goto :goto_0
.end method

.method private CheckString(Ljava/lang/String;Landroid/util/AttributeSet;)Z
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 325
    iget-object v3, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v3, p2, v4, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 327
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v3, 0xa

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 329
    .local v1, "iconsArrayId":I
    if-lez v1, :cond_1

    .line 330
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/help/HelpTextView;->checkspecialcharacterforstring(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 331
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/help/HelpTextView;->Changespecialchartostring(Ljava/lang/String;Landroid/util/AttributeSet;)V

    .line 333
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 334
    const/4 v2, 0x1

    .line 337
    :goto_0
    return v2

    .line 336
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private IntegerHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 423
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 426
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 428
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(I)V

    .line 431
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 434
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 435
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->applyInteger(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 439
    :cond_1
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    return-void
.end method

.method private IntegerinitSelfResources(Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x0

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v2, p1, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 184
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/shealth/help/HelpTextView;->mShowIntegerString:Ljava/lang/String;

    .line 187
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 189
    .local v1, "iconId":I
    if-lez v1, :cond_0

    .line 190
    const/4 v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    aput v1, v2, v4

    .line 195
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->IntegerHtmlText(Landroid/content/res/TypedArray;)V

    .line 197
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 198
    return-void
.end method

.method private NewLineHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 467
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 470
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 472
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(I)V

    .line 475
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 478
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 479
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->applyNewLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 483
    :cond_1
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 484
    return-void
.end method

.method private NewLineinitSelfResources(Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x0

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v2, p1, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 229
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/shealth/help/HelpTextView;->mNewLineString:Ljava/lang/String;

    .line 232
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 234
    .local v1, "iconId":I
    if-lez v1, :cond_0

    .line 235
    const/4 v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    aput v1, v2, v4

    .line 240
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->NewLineHtmlText(Landroid/content/res/TypedArray;)V

    .line 242
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 243
    return-void
.end method

.method private StringHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 445
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 448
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 450
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(I)V

    .line 453
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 456
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 457
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->applyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 461
    :cond_1
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 462
    return-void
.end method

.method private StringinitSelfResources(Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x0

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 203
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 205
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v4, p1, v5, v6, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 208
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/shealth/help/HelpTextView;->mShowString:Ljava/lang/String;

    .line 211
    invoke-virtual {v0, v7, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 213
    .local v2, "iconId":I
    if-lez v2, :cond_0

    .line 214
    const/4 v4, 0x1

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    aput v2, v4, v6

    .line 219
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->StringHtmlText(Landroid/content/res/TypedArray;)V

    .line 221
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 222
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/help/HelpTextView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpTextView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/help/HelpTextView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpTextView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mDrawables:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/help/HelpTextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/help/HelpTextView;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    return-void
.end method

.method private applyImages(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 493
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 494
    .local v9, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 496
    .local v2, "index":I
    const-string v10, "%s"

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 498
    .local v5, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 499
    .local v3, "m":Ljava/util/regex/Matcher;
    iget-object v10, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 500
    .local v7, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    .line 502
    .local v8, "result":Z
    :goto_0
    if-eqz v8, :cond_1

    .line 503
    iget-object v10, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    array-length v10, v10

    if-ge v2, v10, :cond_0

    .line 504
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<img src=\"@drawable/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    aget v11, v11, v2

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\"/>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 511
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 512
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    goto :goto_0

    .line 516
    :cond_1
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 517
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    .line 520
    const-string v4, "$s"

    .line 521
    .local v4, "newTemplate":Ljava/lang/String;
    sget-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 522
    .local v6, "pos":I
    :goto_1
    const/4 v10, -0x1

    if-eq v6, v10, :cond_2

    .line 523
    sget-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    add-int/lit8 v11, v6, -0x1

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    add-int/lit8 v10, v10, -0x30

    add-int/lit8 v1, v10, -0x1

    .line 524
    .local v1, "image":I
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v10, 0x3

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 525
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    const/4 v11, 0x0

    add-int/lit8 v12, v6, -0x2

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    const-string v10, "<img src=\"@drawable/"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527
    iget-object v10, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    aget v10, v10, v1

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    const-string v10, "\"/>"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    sget-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    add-int/lit8 v11, v6, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    .line 531
    sget-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 532
    goto :goto_1

    .line 534
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "image":I
    :cond_2
    sget-object v10, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    return-object v10
.end method

.method private applyInteger(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 538
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 540
    .local v4, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 542
    .local v0, "index":I
    const-string v5, "%d"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 544
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 546
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    .line 549
    .local v3, "result":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 550
    iget-object v5, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 551
    sget-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->mShowIntegerString:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 554
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 555
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    goto :goto_0

    .line 560
    :cond_1
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 561
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    .line 564
    sget-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    const-string v6, "[\n]"

    const-string v7, "<br>"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    .line 566
    sget-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    return-object v5
.end method

.method private applyNewLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 602
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 604
    .local v6, "sb":Ljava/lang/StringBuffer;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 606
    .local v2, "locale":Ljava/lang/String;
    new-instance v0, Ljava/lang/String;

    const-string v7, "ko"

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 608
    .local v0, "KOREA":Ljava/lang/String;
    const/4 v1, 0x0

    .line 610
    .local v1, "index":I
    const-string v7, " "

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 612
    .local v4, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 614
    .local v3, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    .line 617
    .local v5, "result":Z
    :goto_0
    if-eqz v5, :cond_3

    .line 618
    iget-object v7, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    array-length v7, v7

    if-ge v1, v7, :cond_1

    .line 619
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    sget-boolean v7, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineOrentation:Z

    if-eqz v7, :cond_2

    .line 620
    :cond_0
    const-string v7, " "

    invoke-virtual {v3, v6, v7}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 627
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 628
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    goto :goto_0

    .line 622
    :cond_2
    const-string v7, "<BR>"

    invoke-virtual {v3, v6, v7}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_1

    .line 633
    :cond_3
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 634
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    .line 639
    sget-object v7, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    return-object v7
.end method

.method private applyString(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 570
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 572
    .local v4, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 574
    .local v0, "index":I
    const-string v5, "%s"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 576
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 578
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    .line 581
    .local v3, "result":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 582
    iget-object v5, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 583
    sget-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->mShowString:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 586
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 587
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    goto :goto_0

    .line 592
    :cond_1
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 593
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    .line 596
    sget-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    const-string v6, "[\n]"

    const-string v7, "<br>"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    .line 598
    sget-object v5, Lcom/sec/android/app/shealth/help/HelpTextView;->finalString:Ljava/lang/String;

    return-object v5
.end method

.method private checkspecialcharacterforinteger(Ljava/lang/String;)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 285
    const-string v0, "$d"

    .line 286
    .local v0, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 287
    .local v1, "pos":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 288
    const/4 v2, 0x1

    .line 290
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private checkspecialcharacterforstring(Ljava/lang/String;)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 341
    const-string v0, "$s"

    .line 342
    .local v0, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 343
    .local v1, "pos":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 344
    const/4 v2, 0x1

    .line 346
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private initHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 399
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 402
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 404
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(I)V

    .line 407
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 410
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 411
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->applyImages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 415
    :cond_1
    const-string v2, "[\n]"

    const-string v3, "<br>"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 418
    iget-object v2, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 419
    return-void
.end method

.method private initImgPadding(Landroid/content/res/TypedArray;)V
    .locals 3
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 643
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    .line 646
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 649
    .local v0, "padding":I
    if-eqz v0, :cond_0

    .line 650
    iget-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 653
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 656
    if-eqz v0, :cond_1

    .line 657
    iget-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 660
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 663
    if-eqz v0, :cond_2

    .line 664
    iget-object v1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 666
    :cond_2
    return-void
.end method

.method private initSelfResources(Landroid/util/AttributeSet;)V
    .locals 11
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v10, -0x40800000    # -1.0f

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 117
    iget-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v1, v6, Landroid/util/DisplayMetrics;->density:F

    .line 119
    .local v1, "density":F
    iget-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/sec/android/app/shealth/R$styleable;->HelpTextView:[I

    invoke-virtual {v6, p1, v7, v8, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 123
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v6, 0x7

    invoke-virtual {v0, v6, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    .line 125
    iget v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    cmpg-float v6, v6, v9

    if-gez v6, :cond_0

    .line 128
    iget v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    div-float/2addr v6, v1

    iput v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    .line 132
    :cond_0
    const/4 v6, 0x6

    invoke-virtual {v0, v6, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    .line 134
    iget v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    cmpg-float v6, v6, v9

    if-gez v6, :cond_1

    .line 137
    iget v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    div-float/2addr v6, v1

    iput v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    .line 140
    :cond_1
    const/16 v6, 0xc

    const/16 v7, 0x11

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageGravity:I

    .line 143
    const/16 v6, 0x8

    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mImageYDiff:F

    .line 145
    const/16 v6, 0xd

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 148
    .local v4, "iconsArrayId":I
    if-lez v4, :cond_4

    .line 149
    iget-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 152
    .local v5, "iconsTypedArray":Landroid/content/res/TypedArray;
    if-eqz v5, :cond_3

    .line 153
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 154
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->length()I

    move-result v6

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    .line 156
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    array-length v6, v6

    if-ge v2, v6, :cond_2

    .line 157
    iget-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    invoke-virtual {v5, v2, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    aput v7, v6, v2

    .line 156
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 160
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 172
    .end local v5    # "iconsTypedArray":Landroid/content/res/TypedArray;
    :cond_3
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->initImgPadding(Landroid/content/res/TypedArray;)V

    .line 174
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->initHtmlText(Landroid/content/res/TypedArray;)V

    .line 176
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 177
    return-void

    .line 163
    :cond_4
    const/16 v6, 0xe

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 165
    .local v3, "iconId":I
    if-lez v3, :cond_3

    .line 166
    const/4 v6, 0x1

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    .line 167
    iget-object v6, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mAddItemIds:[I

    aput v3, v6, v8

    goto :goto_1
.end method

.method private invalidateDrawables()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 669
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mDrawables:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 673
    .local v0, "count":I
    :goto_0
    if-lez v0, :cond_5

    .line 674
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mDrawables:Ljava/util/ArrayList;

    add-int/lit8 v5, v0, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/help/HelpDrawable;

    .line 676
    .local v1, "currentDrawable":Lcom/sec/android/app/shealth/help/HelpDrawable;
    if-eqz v1, :cond_2

    .line 677
    iget v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageGravity:I

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/help/HelpDrawable;->setGravity(I)V

    .line 679
    iget v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    cmpl-float v4, v4, v7

    if-ltz v4, :cond_3

    .line 680
    iget v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    float-to-int v4, v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/help/HelpDrawable;->setWidth(I)V

    .line 687
    :goto_1
    iget v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    cmpl-float v4, v4, v7

    if-ltz v4, :cond_4

    .line 688
    iget v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    float-to-int v4, v4

    neg-int v4, v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/help/HelpDrawable;->setHeight(I)V

    .line 696
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v3, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 697
    .local v3, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 698
    .local v2, "strLanguage":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v4, "ko"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 700
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getLineHeight()I

    move-result v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/help/HelpDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->getTextSize()F

    move-result v5

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/help/HelpDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mImageYDiff:F

    add-float/2addr v4, v5

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/help/HelpDrawable;->setBitmapY(F)V

    .line 702
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/help/HelpDrawable;->setPadding(Landroid/graphics/Rect;)V

    .line 705
    .end local v2    # "strLanguage":Ljava/lang/String;
    .end local v3    # "systemLocale":Ljava/util/Locale;
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 683
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/help/HelpDrawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    neg-int v4, v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/help/HelpDrawable;->setWidth(I)V

    goto :goto_1

    .line 693
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/help/HelpDrawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/help/HelpDrawable;->setHeight(I)V

    goto :goto_2

    .line 707
    .end local v1    # "currentDrawable":Lcom/sec/android/app/shealth/help/HelpDrawable;
    :cond_5
    return-void
.end method


# virtual methods
.method public getImgGravity()I
    .locals 1

    .prologue
    .line 758
    iget v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageGravity:I

    return v0
.end method

.method public getImgHeight()I
    .locals 1

    .prologue
    .line 749
    iget v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    float-to-int v0, v0

    return v0
.end method

.method public getImgWidth()I
    .locals 1

    .prologue
    .line 740
    iget v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    float-to-int v0, v0

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 763
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 764
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineOrentation:Z

    .line 768
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mStorevalue:Ljava/lang/String;

    .line 770
    .local v0, "text":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineCheck:Z

    if-eqz v1, :cond_0

    .line 771
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/help/HelpTextView;->applyNewLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 772
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/help/HelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 774
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 775
    return-void

    .line 766
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/help/HelpTextView;->bNewLineOrentation:Z

    goto :goto_0
.end method

.method public setImgGravity(I)V
    .locals 0
    .param p1, "g"    # I

    .prologue
    .line 753
    iput p1, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageGravity:I

    .line 754
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 755
    return-void
.end method

.method public setImgHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 744
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageHeight:F

    .line 745
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 746
    return-void
.end method

.method public setImgPadding(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "padding"    # Landroid/graphics/Rect;

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 711
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 712
    return-void
.end method

.method public setImgPaddingBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 730
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    .line 731
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 732
    return-void
.end method

.method public setImgPaddingLeft(I)V
    .locals 1
    .param p1, "left"    # I

    .prologue
    .line 715
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->left:I

    .line 716
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 717
    return-void
.end method

.method public setImgPaddingRight(I)V
    .locals 1
    .param p1, "right"    # I

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->right:I

    .line 721
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 722
    return-void
.end method

.method public setImgPaddingTop(I)V
    .locals 1
    .param p1, "top"    # I

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->top:I

    .line 726
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 727
    return-void
.end method

.method public setImgWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 735
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/help/HelpTextView;->mInsideImageWidth:F

    .line 736
    invoke-direct {p0}, Lcom/sec/android/app/shealth/help/HelpTextView;->invalidateDrawables()V

    .line 737
    return-void
.end method
