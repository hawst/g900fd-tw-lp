.class public interface abstract Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/SliderAdapter;
.super Ljava/lang/Object;
.source "SliderAdapter.java"


# virtual methods
.method public abstract getBubbleViewHandler()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/BubbleViewHandler;
.end method

.method public abstract getCount()I
.end method

.method public abstract getDivider()Landroid/view/View;
.end method

.method public abstract getDividerWidth()I
.end method

.method public abstract getHandler()Landroid/view/View;
.end method

.method public abstract getHandlerSize()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/Size;
.end method

.method public abstract getScaleLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
.end method

.method public abstract getScalePaddings()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/sliderview/ScalePaddings;
.end method
