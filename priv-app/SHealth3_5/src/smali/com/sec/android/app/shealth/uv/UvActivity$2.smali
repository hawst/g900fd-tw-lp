.class Lcom/sec/android/app/shealth/uv/UvActivity$2;
.super Landroid/os/Handler;
.source "UvActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 212
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 226
    :goto_0
    return-void

    .line 214
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->hasNoUVSensor:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$000(Lcom/sec/android/app/shealth/uv/UvActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # invokes: Lcom/sec/android/app/shealth/uv/UvActivity;->showInfomationDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$100(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    goto :goto_0

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # invokes: Lcom/sec/android/app/shealth/uv/UvActivity;->showNoSensorInformationDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$200(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    goto :goto_0

    .line 221
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$2;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # invokes: Lcom/sec/android/app/shealth/uv/UvActivity;->showSkinTypeDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$300(Lcom/sec/android/app/shealth/uv/UvActivity;)V

    goto :goto_0

    .line 212
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
