.class public final enum Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;
.super Ljava/lang/Enum;
.source "BackupNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/backup/server/BackupNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BackupStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

.field public static final enum Backup:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

.field public static final enum None:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

.field public static final enum Restore:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    const-string v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->None:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    new-instance v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    const-string v1, "Backup"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->Backup:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    new-instance v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    const-string v1, "Restore"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->Restore:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->None:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->Backup:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->Restore:Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->$VALUES:[Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupStatus;

    return-object v0
.end method
