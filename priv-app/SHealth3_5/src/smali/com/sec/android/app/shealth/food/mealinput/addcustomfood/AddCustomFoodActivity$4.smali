.class Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4;
.super Ljava/lang/Object;
.source "AddCustomFoodActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->initViews(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "focus"    # Z

    .prologue
    .line 405
    if-eqz p2, :cond_1

    instance-of v1, p1, Landroid/widget/EditText;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 406
    check-cast v0, Landroid/widget/EditText;

    .line 407
    .local v0, "et":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 425
    .end local v0    # "et":Landroid/widget/EditText;
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$4;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
