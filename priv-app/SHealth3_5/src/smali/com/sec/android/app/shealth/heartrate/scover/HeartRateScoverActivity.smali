.class public Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "HeartRateScoverActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$TeleListener;
    }
.end annotation


# static fields
.field private static final HRM_EXIT:I = 0x5

.field private static final HRM_MEASURE_OPEN:I = 0x1

.field public static final SVIEW_COVER_DIM_TIMEOUT_DEFAULT:I = 0x0

.field public static final SVIEW_COVER_DISPLAY_TIMEOUT_DEFAULT:I = 0x1770

.field public static final SVIEW_COVER_DISPLAY_TIMEOUT_TALKBACK:I = 0x3a98

.field public static final TAG:Ljava/lang/String;

.field private static final TOAST_FADEOUT_TIME:I = 0x1388


# instance fields
.field againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private intentCloseToast:Landroid/content/Intent;

.field public isAutoStartup:Z

.field public isMeasureCompleted:Z

.field mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

.field public mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mDeviceSupportCoverSDK:Z

.field mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

.field private mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

.field public mHrmScoverFragmentHandler:Landroid/os/Handler;

.field private mInfoView:Landroid/view/View;

.field private mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field private mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mShowAgain:Landroid/widget/CheckBox;

.field private mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

.field private mTeleListener:Landroid/telephony/PhoneStateListener;

.field private mTelephonyMgr:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isMeasureCompleted:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isAutoStartup:Z

    .line 67
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mDeviceSupportCoverSDK:Z

    .line 76
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$TeleListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$TeleListener;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mTeleListener:Landroid/telephony/PhoneStateListener;

    .line 284
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$3;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHrmScoverFragmentHandler:Landroid/os/Handler;

    .line 334
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$4;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 480
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$7;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method private LaunchSHealthActivity()V
    .locals 3

    .prologue
    .line 252
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 253
    .local v0, "scoverintent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 254
    const-string/jumbo v1, "scover"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 256
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string v2, "LaunchSHealthActivity"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->finish()V

    .line 261
    return-void
.end method

.method private ShowHeartrateDilaog()V
    .locals 4

    .prologue
    .line 474
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string v2, "ShowHeartrateDilaog "

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 476
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 477
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHrmScoverFragmentHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xa

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 478
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->LaunchSHealthActivity()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->intentCloseToast:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->saveScoverDialogPreference()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->ShowHeartrateDilaog()V

    return-void
.end method

.method private addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    .locals 2
    .param p1, "_fragment"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 318
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 319
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 320
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 321
    return-void
.end method

.method private initSCover()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string v2, "initSCover"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 96
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mDeviceSupportCoverSDK:Z

    if-eqz v1, :cond_0

    .line 97
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 98
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 118
    :cond_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mDeviceSupportCoverSDK:Z

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 94
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mDeviceSupportCoverSDK:Z

    goto :goto_0
.end method

.method private needProfileToast()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 125
    sget-object v7, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "needProfileToast"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 128
    .local v2, "context":Landroid/content/Context;
    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.cover.REMOTEVIEWS_UPDATE"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->intentCloseToast:Landroid/content/Intent;

    .line 129
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->intentCloseToast:Landroid/content/Intent;

    const-string/jumbo v8, "type"

    const-string/jumbo v9, "shealth_warning_dialog"

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->intentCloseToast:Landroid/content/Intent;

    const-string/jumbo v8, "visibility"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 131
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->intentCloseToast:Landroid/content/Intent;

    invoke-virtual {v2, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 133
    new-instance v6, Landroid/widget/RemoteViews;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f030156

    invoke-direct {v6, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 135
    .local v6, "views":Landroid/widget/RemoteViews;
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->intentCloseToast:Landroid/content/Intent;

    invoke-static {v2, v10, v7, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 136
    .local v0, "closeToast":Landroid/app/PendingIntent;
    const v7, 0x7f0805cc

    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 138
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.samsung.cover.REMOTEVIEWS_UPDATE"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 139
    .local v3, "intentBoot":Landroid/content/Intent;
    const/high16 v7, 0x10000000

    invoke-static {p0, v10, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 140
    .local v1, "content":Landroid/app/PendingIntent;
    const-string/jumbo v7, "type"

    const-string/jumbo v8, "shealth_warning_dialog"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string/jumbo v7, "visibility"

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 142
    const-string v7, "contentIntent"

    invoke-virtual {v3, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 143
    const-string/jumbo v7, "remote"

    invoke-virtual {v3, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 144
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 146
    new-instance v4, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    .line 153
    .local v4, "task":Ljava/util/TimerTask;
    new-instance v5, Ljava/util/Timer;

    invoke-direct {v5}, Ljava/util/Timer;-><init>()V

    .line 154
    .local v5, "timer":Ljava/util/Timer;
    const-wide/16 v7, 0x1388

    invoke-virtual {v5, v4, v7, v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->LaunchSHealthActivity()V

    .line 156
    return-void
.end method

.method private saveScoverDialogPreference()V
    .locals 3

    .prologue
    .line 489
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 490
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "heartrate_warning_checked"

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 491
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 492
    return-void
.end method

.method private setWindowFlags()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 227
    const/high16 v4, 0x400000

    .line 228
    .local v4, "secure_flags":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "automatic_unlock"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 229
    .local v0, "isAutoUnlock":I
    const-string v5, "keyguard"

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/KeyguardManager;

    .line 230
    .local v3, "mKeyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v3}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    .line 232
    .local v1, "isSecureLock":Z
    if-nez v1, :cond_0

    if-ne v0, v7, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Window;->addFlags(I)V

    .line 235
    :cond_0
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->requestWindowFeature(I)Z

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/high16 v6, 0x80000

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x800

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/high16 v6, 0x4000000

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    const-wide/16 v6, 0x1770

    iput-wide v6, v5, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    const-wide/16 v6, 0x0

    iput-wide v6, v5, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    .line 246
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 247
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 249
    return-void
.end method

.method private showInfomationDialog()V
    .locals 15

    .prologue
    .line 345
    sget-object v11, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v12, "showInfomationDialog"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isFinishing()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 347
    sget-object v11, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string v12, "Finish called on activity, aborting showInfomationDialog display!"

    invoke-static {v11, v12}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :goto_0
    return-void

    .line 352
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Landroid/view/Window;->clearFlags(I)V

    .line 354
    const-string v11, "accessibility"

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 355
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v4

    .line 356
    .local v4, "isAccessibilityEnabled":Z
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v5

    .line 357
    .local v5, "isExploreByTouchEnabled":Z
    if-eqz v5, :cond_1

    if-eqz v4, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v11

    const-wide/16 v12, 0x3a98

    iput-wide v12, v11, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    .line 360
    :cond_1
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v11, 0x7

    invoke-direct {v1, p0, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 361
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 362
    const-string v11, ""

    invoke-virtual {v1, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 363
    const v11, 0x7f030151

    new-instance v12, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;

    invoke-direct {v12, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    invoke-virtual {v1, v11, v12}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 411
    if-eqz p0, :cond_3

    .line 413
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v11, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 414
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v12

    const-string/jumbo v13, "show_dialog"

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 417
    :cond_3
    const/4 v10, 0x0

    .line 418
    .local v10, "window":Landroid/view/Window;
    if-nez v10, :cond_4

    .line 419
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v10

    .line 424
    :cond_4
    if-eqz v10, :cond_6

    .line 427
    const v11, 0x43908000    # 289.0f

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v12, v12, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 428
    .local v3, "height":I
    const v11, 0x43a28000    # 325.0f

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v12, v12, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 429
    .local v9, "width":I
    invoke-virtual {v10, v9, v3}, Landroid/view/Window;->setLayout(II)V

    .line 431
    invoke-virtual {v10}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    .line 432
    .local v6, "lp":Landroid/view/WindowManager$LayoutParams;
    const/4 v11, 0x0

    iput v11, v6, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 433
    const/16 v11, 0x31

    iput v11, v6, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 435
    invoke-virtual {v10, v6}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 436
    const/high16 v11, 0x80000

    invoke-virtual {v10, v11}, Landroid/view/Window;->addFlags(I)V

    .line 437
    const/16 v11, 0x800

    invoke-virtual {v10, v11}, Landroid/view/Window;->addFlags(I)V

    .line 438
    const/high16 v11, 0x4000000

    invoke-virtual {v10, v11}, Landroid/view/Window;->addFlags(I)V

    .line 439
    invoke-virtual {v10}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v11

    const/16 v12, 0x500

    invoke-virtual {v11, v12}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 442
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v11, :cond_5

    .line 443
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    const/4 v12, 0x1

    invoke-virtual {v11, v10, v12}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 445
    :cond_5
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v8

    .line 446
    .local v8, "v":Landroid/view/View;
    const v11, 0x7f080033

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 447
    .local v2, "flP":Landroid/widget/FrameLayout;
    if-eqz v2, :cond_6

    .line 448
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v2, v11, v12, v13, v14}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 449
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 450
    .local v7, "mlp":Landroid/view/ViewGroup$MarginLayoutParams;
    const v11, 0x43a28000    # 325.0f

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v12, v12, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    iput v11, v7, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 452
    const v11, 0x43908000    # 289.0f

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v12, v12, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    iput v11, v7, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 454
    invoke-virtual {v2, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 458
    .end local v2    # "flP":Landroid/widget/FrameLayout;
    .end local v3    # "height":I
    .end local v6    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v7    # "mlp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v8    # "v":Landroid/view/View;
    .end local v9    # "width":I
    :cond_6
    new-instance v11, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$6;

    invoke-direct {v11, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$6;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    invoke-virtual {v1, v11}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto/16 :goto_0
.end method


# virtual methods
.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 325
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onBackPressed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHrmScoverFragmentHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 328
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 329
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->needProfileToast()V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->finish()V

    .line 167
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->isAutoStartup:Z

    .line 170
    iput-object p0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    .line 171
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->setWindowFlags()V

    .line 172
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 173
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 265
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPause"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mTeleListener:Landroid/telephony/PhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 270
    const-string/jumbo v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 271
    .local v0, "powerManager":Landroid/os/PowerManager;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-gt v1, v2, :cond_1

    .line 272
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->finish()V

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->finish()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 177
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResume"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 181
    const-string/jumbo v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mTeleListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 184
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->initSCover()V

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 191
    .local v0, "state":Lcom/samsung/android/sdk/cover/ScoverState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 192
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->LaunchSHealthActivity()V

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    if-nez v1, :cond_1

    .line 197
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverActivity:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHrmScoverFragmentHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setFragmentHandler(Landroid/os/Handler;)V

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->setScoverManger(Lcom/samsung/android/sdk/cover/ScoverManager;)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mHeartRateScoverFragment:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const-string v2, "HeartRateScoverFragment"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V

    .line 203
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "heartrate_warning_checked"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->showInfomationDialog()V

    .line 210
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 211
    return-void

    .line 206
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->ShowHeartrateDilaog()V

    goto :goto_0
.end method
