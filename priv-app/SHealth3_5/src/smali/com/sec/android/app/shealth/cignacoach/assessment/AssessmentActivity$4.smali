.class Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;
.super Ljava/lang/Object;
.source "AssessmentActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->showTrackingDialog(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

.field final synthetic val$cagetory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->val$cagetory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 441
    const v1, 0x7f08029f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 442
    .local v0, "icon":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->val$cagetory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getCategoryIconRes(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 444
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;-><init>()V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mTrackerDataLoadingTask:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->access$202(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;)Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    .line 445
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mTrackerDataLoadingTask:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;)Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    move-result-object v2

    const v1, 0x7f0802a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 446
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mTrackerDataLoadingTask:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;)Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->setOnFinishListener(Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;)V

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mTrackerDataLoadingTask:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;)Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 448
    return-void
.end method
