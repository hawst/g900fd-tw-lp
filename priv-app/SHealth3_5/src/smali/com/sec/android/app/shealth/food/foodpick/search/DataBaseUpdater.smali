.class public final Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;
.super Ljava/lang/Object;
.source "DataBaseUpdater.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;",
            "Dao::",
            "Lcom/sec/android/app/shealth/common/commondao/CommonDao",
            "<TE;>;>(TE;TDao;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;, "TE;"
    .local p1, "dao":Lcom/sec/android/app/shealth/common/commondao/CommonDao;, "TDao;"
    invoke-interface {p1, p0}, Lcom/sec/android/app/shealth/common/commondao/CommonDao;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-interface {p1, p0}, Lcom/sec/android/app/shealth/common/commondao/CommonDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v0

    .line 48
    .local v0, "id":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 49
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;->setId(J)V

    goto :goto_0

    .line 52
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Data "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  doesn\'t inserted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
