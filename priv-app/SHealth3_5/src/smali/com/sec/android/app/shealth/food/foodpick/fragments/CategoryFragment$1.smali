.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;
.super Ljava/lang/Object;
.source "CategoryFragment.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->initHierarchyListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 4
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->ROOT_CATEGORIES:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    if-eq v1, v2, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mDisplayedListsMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$200(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Ljava/util/Map;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->values()[Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->ordinal()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 114
    .local v0, "prevView":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 115
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 122
    .end local v0    # "prevView":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 118
    .restart local v0    # "prevView":Landroid/view/View;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->logStagesAndThrowNpe()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    goto :goto_0
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 3
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 126
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mDisplayedListsMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$200(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->currentStage:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$100(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 127
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 128
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->logStagesAndThrowNpe()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)V

    goto :goto_0
.end method
