.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->performSearchFood()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

.field final synthetic val$requestString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 868
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->val$requestString:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;-><init>()V

    return-void
.end method


# virtual methods
.method public accompanyWithLoadingDialog()Z
    .locals 1

    .prologue
    .line 872
    const/4 v0, 0x1

    return v0
.end method

.method protected doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 877
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->val$requestString:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V

    .line 878
    .local v0, "request":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performFoodSearch(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    return-object v1
.end method

.method protected onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 883
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 888
    :goto_0
    return-void

    .line 886
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->saveToDbAndShow(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2400(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchTask:Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2502(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    goto :goto_0
.end method

.method protected bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 868
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$17;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult;)V

    return-void
.end method
