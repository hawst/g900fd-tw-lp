.class public Lcom/sec/android/app/shealth/award/AwardSummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "AwardSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;
    }
.end annotation


# static fields
.field private static final NO_MEDIA_FILE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field private awardSummaryFragmentView:Landroid/view/View;

.field private isImage:Z

.field private mBirthDay:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

.field private mGenderPhoto:Landroid/widget/ImageView;

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mProfileLev:Landroid/widget/TextView;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private mSince:Landroid/widget/TextView;

.field private mUserName:Landroid/widget/TextView;

.field private mUserPrivate:Landroid/widget/ImageView;

.field public myPagePhoto:Landroid/widget/ImageView;

.field oldText:Ljava/lang/String;

.field private original_file:Ljava/io/File;

.field private photo:Landroid/graphics/Bitmap;

.field protected profileImage:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 86
    const-class v0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->TAG:Ljava/lang/String;

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nomedia"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->NO_MEDIA_FILE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mUserPrivate:Landroid/widget/ImageView;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->isImage:Z

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->photo:Landroid/graphics/Bitmap;

    .line 116
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 117
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->oldText:Ljava/lang/String;

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mContext:Landroid/content/Context;

    .line 127
    return-void
.end method

.method private SaveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 643
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 644
    .local v1, "fileCacheItem":Ljava/io/File;
    const/4 v2, 0x0

    .line 647
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 648
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 649
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 659
    if-eqz v3, :cond_0

    .line 660
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 668
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :cond_1
    :goto_0
    return-void

    .line 663
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 665
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 667
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 651
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 653
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 659
    if-eqz v2, :cond_1

    .line 660
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 663
    :catch_2
    move-exception v0

    .line 665
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 657
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 659
    :goto_2
    if-eqz v2, :cond_2

    .line 660
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 666
    :cond_2
    :goto_3
    throw v4

    .line 663
    :catch_3
    move-exception v0

    .line 665
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 657
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 651
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/award/AwardSummaryFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/award/AwardSummaryFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/AwardSummaryFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->initializeTodayActivityArea(Landroid/view/View;)V

    return-void
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 4
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    .line 672
    const/4 v2, 0x0

    .line 675
    .local v2, "result":Z
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    .local v1, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 682
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 689
    .end local v1    # "in":Ljava/io/InputStream;
    :goto_0
    return v2

    .line 682
    .restart local v1    # "in":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 685
    .end local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 687
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 696
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 699
    .local v3, "out":Ljava/io/OutputStream;
    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    .line 701
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    if-ltz v1, :cond_0

    .line 703
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 708
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v5

    .line 712
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 714
    :goto_1
    return v4

    .line 708
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v3    # "out":Ljava/io/OutputStream;
    :cond_0
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 710
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public static getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maskId"    # I

    .prologue
    const/4 v12, 0x0

    const/high16 v11, 0x42fa0000    # 125.0f

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 210
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 212
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-static {v9, v11, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v8

    float-to-int v7, v8

    .line 213
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-static {v9, v11, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v8

    float-to-int v1, v8

    .line 215
    .local v1, "height":I
    invoke-static {p0, v7, v1, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 216
    .local v2, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v8, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 217
    .local v3, "mask":Landroid/graphics/Bitmap;
    invoke-static {v3, v7, v1, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 218
    .local v5, "resizedBitmap":Landroid/graphics/Bitmap;
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v1, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 220
    .local v6, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 221
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 222
    .local v4, "paint":Landroid/graphics/Paint;
    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 223
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 225
    invoke-virtual {v0, v2, v10, v10, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 226
    new-instance v8, Landroid/graphics/PorterDuffXfermode;

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v8, v9}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 227
    invoke-virtual {v0, v5, v10, v10, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 228
    invoke-virtual {v4, v12}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 230
    return-object v6
.end method

.method public static getSystemDateFormat()Ljava/text/SimpleDateFormat;
    .locals 4

    .prologue
    .line 310
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "pattern":Ljava/lang/String;
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090714

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 325
    :goto_0
    return-object v1

    .line 318
    :cond_0
    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 320
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090715

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 325
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090716

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method

.method private initImageValue()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 436
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->isUserFile()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->photo:Landroid/graphics/Bitmap;

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->photo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->setImage(Landroid/graphics/Bitmap;)V

    .line 443
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 452
    :goto_0
    return-void

    .line 447
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->setDefaultImage()V

    .line 448
    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mImageCaptureUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method private initializeMedalActivityArea(Landroid/view/View;)V
    .locals 2
    .param p1, "awardSummaryFragmentView"    # Landroid/view/View;

    .prologue
    .line 296
    new-instance v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 297
    return-void
.end method

.method private initializePhotoArea()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v1, 0x7f08006f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mUserPrivate:Landroid/widget/ImageView;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v1, 0x7f080070

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v1, 0x7f080071

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mGenderPhoto:Landroid/widget/ImageView;

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v1, 0x7f080072

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mBirthDay:Landroid/widget/TextView;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v1, 0x7f080073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mProfileLev:Landroid/widget/TextView;

    .line 163
    return-void
.end method

.method private initializeTodayActivityArea(Landroid/view/View;)V
    .locals 2
    .param p1, "awardSummaryFragmentView"    # Landroid/view/View;

    .prologue
    .line 292
    new-instance v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 293
    return-void
.end method

.method private initializeUserBackupArea(Landroid/view/View;)V
    .locals 2
    .param p1, "awardSummaryFragmentView"    # Landroid/view/View;

    .prologue
    .line 304
    new-instance v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardUserBackUpActivity;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 305
    return-void
.end method

.method public static isNoMediaFile()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 398
    const/4 v1, 0x0

    .line 401
    .local v1, "imgFile":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->NO_MEDIA_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    .end local v1    # "imgFile":Ljava/io/File;
    .local v2, "imgFile":Ljava/io/File;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 404
    sget-object v3, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->TAG:Ljava/lang/String;

    const-string v4, "isNoMediaFile Exists"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 412
    .end local v2    # "imgFile":Ljava/io/File;
    :goto_0
    return-object v3

    .line 408
    .restart local v1    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_0

    .line 412
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "imgFile":Ljava/io/File;
    .restart local v2    # "imgFile":Ljava/io/File;
    :cond_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_0

    .line 408
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "imgFile":Ljava/io/File;
    .restart local v1    # "imgFile":Ljava/io/File;
    goto :goto_1
.end method

.method public static onCreateNoMediaFile()V
    .locals 4

    .prologue
    .line 381
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->NO_MEDIA_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 384
    .local v1, "file":Ljava/io/File;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onCreateNoMediaFile"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    :goto_0
    return-void

    .line 388
    :catch_0
    move-exception v0

    .line 390
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onCreateNoMediaFile Exception"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public isImage()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->isImage:Z

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mContext:Landroid/content/Context;

    .line 134
    const v1, 0x7f030014

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 136
    .local v0, "view":Landroid/view/View;
    if-eqz p3, :cond_0

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->photo:Landroid/graphics/Bitmap;

    .line 142
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 144
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->initializePhotoArea()V

    .line 150
    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 269
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 270
    const-string v0, "HOON"

    const-string/jumbo v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 276
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 237
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 238
    sget-object v1, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResume()"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

    if-nez v1, :cond_0

    .line 241
    new-instance v1, Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;-><init>(Lcom/sec/android/app/shealth/award/AwardSummaryFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

    .line 244
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.shealth.pedometer.viewstepcount"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 245
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mDeviceCheckedReceiver:Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v2, 0x7f080076

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v2, 0x7f080079

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v2, 0x7f080081

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->setMyPagePhoto()V

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->initImageValue()V

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->initializeMedalActivityArea(Landroid/view/View;)V

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->initializeTodayActivityArea(Landroid/view/View;)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->initializeUserBackupArea(Landroid/view/View;)V

    .line 265
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 283
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onStop()V

    .line 284
    const-string v0, "HOON"

    const-string/jumbo v1, "onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    return-void
.end method

.method public setDefaultImage()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 732
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020138

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 733
    .local v1, "newBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 734
    .local v2, "profilephoto":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 735
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 736
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v6, v6, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 737
    if-eqz v1, :cond_0

    .line 739
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 740
    const/4 v1, 0x0

    .line 742
    :cond_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-le v3, v4, :cond_1

    .line 744
    invoke-virtual {v0}, Landroid/graphics/Canvas;->release()V

    .line 746
    :cond_1
    const/4 v0, 0x0

    .line 747
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0205b6

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 749
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901fe

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901ff

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 750
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->deleteProfileBitmap()Ljava/lang/Boolean;

    .line 751
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->isImage:Z

    .line 752
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 722
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0205b6

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 725
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 726
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090200

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 727
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->isImage:Z

    .line 728
    return-void
.end method

.method public setImageClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->myPagePhoto:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 456
    return-void
.end method

.method protected setMyPagePhoto()V
    .locals 10

    .prologue
    const v9, 0x7f09020b

    const v8, 0x7f0901fd

    .line 166
    invoke-static {}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getSystemDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v2

    .line 167
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 169
    .local v3, "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/sql/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/sql/Date;-><init>(J)V

    .line 170
    .local v1, "date":Ljava/sql/Date;
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mBirthDay:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mBirthDay:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/sql/Date;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 174
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v4

    const v5, 0x2e635

    if-ne v4, v5, :cond_1

    .line 175
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mGenderPhoto:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0205ef

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 176
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mGenderPhoto:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09081e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 183
    :goto_0
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v0

    .line 185
    .local v0, "activityType":I
    const v4, 0x2bf21

    if-ne v0, v4, :cond_2

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mProfileLev:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090827

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isUsePedometerRanking(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mUserPrivate:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 203
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v5, 0x7f080078

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0908f7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;

    const v5, 0x7f080080

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0908f8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 206
    return-void

    .line 178
    .end local v0    # "activityType":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mGenderPhoto:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0205de

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 179
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mGenderPhoto:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09081f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 187
    .restart local v0    # "activityType":I
    :cond_2
    const v4, 0x2bf22

    if-ne v0, v4, :cond_3

    .line 188
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mProfileLev:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090828

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 189
    :cond_3
    const v4, 0x2bf23

    if-ne v0, v4, :cond_4

    .line 190
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mProfileLev:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090829

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 191
    :cond_4
    const v4, 0x2bf24

    if-ne v0, v4, :cond_5

    .line 192
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mProfileLev:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09082a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 193
    :cond_5
    const v4, 0x2bf25

    if-ne v0, v4, :cond_0

    .line 194
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mProfileLev:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09082b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 200
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->mUserPrivate:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2
.end method
