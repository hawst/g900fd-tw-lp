.class public Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "CignaLoadGeneralActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;
.implements Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

.field private mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 27
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    return-object v0
.end method

.method private startCignaCoachActivity()V
    .locals 3

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 108
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 110
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->startActivity(Landroid/content/Intent;)V

    .line 111
    return-void
.end method


# virtual methods
.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 117
    const-string v0, "COACH_PROMPT_RESTORE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "RESTORE_PROGRESS_POPUP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "RESTORE_ERROR_POPUP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "RESTORE_SUCCESS_POPUP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onChangeLanguage(Z)V
    .locals 3
    .param p1, "changeLanguage"    # Z

    .prologue
    .line 94
    if-eqz p1, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_CHANGE_LANGUAGE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 32
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->killProcessIfDBNotInitialised(Landroid/app/Activity;)V

    .line 35
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->finishIfCignaUnsupported(Landroid/app/Activity;)V

    .line 38
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    move-result-object v2

    invoke-virtual {v2, p0, p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->registerReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 41
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "EXTRA_NAME_CHANGE_LANGUAGE"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 42
    .local v0, "changeLanguage":Z
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CignaLoadGeneralActivity onCreate() changeLanguage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    if-eqz v0, :cond_0

    .line 46
    const-string v2, "EXTRA_NAME_CHANGE_LANGUAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->startCignaCoachActivity()V

    .line 52
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getCignaRestoreTriggerPopup(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 53
    instance-of v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    if-nez v2, :cond_1

    .line 54
    const-string v2, "LAUNCHED_FROM_WIDGET"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->showDialog()V

    .line 57
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerRestoreSuccessListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;)V

    .line 58
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 85
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->destroyDialog()V

    .line 88
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    move-result-object v0

    invoke-virtual {v0, p0, p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->unRegisterReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V

    .line 89
    invoke-static {p0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterRestoreSuccessListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$OnCoachRestoreSuccessListener;)V

    .line 90
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 75
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LAUNCHED_FROM_WIDGET"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dismissRestoreTriggerPopup()V

    .line 78
    :cond_0
    return-void
.end method

.method public onRestoreSuccess()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->startCignaCoachActivity()V

    .line 102
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 65
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->restoreState()V

    .line 67
    :cond_0
    return-void
.end method
