.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$USER_DEVICE_TABLE;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "USER_DEVICE_TABLE"
.end annotation


# static fields
.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_CUSTOM_NAME:Ljava/lang/String; = "custom_name"

.field public static final KEY_DEVICE_TYPE:Ljava/lang/String; = "device_type"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final KEY_UUID:Ljava/lang/String; = "uuid"

.field public static final TABLE_NAME:Ljava/lang/String; = "user_device"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
