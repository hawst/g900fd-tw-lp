.class public final enum Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;
.super Ljava/lang/Enum;
.source "MealDao.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/fooddao/MealDao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MealContract"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;",
        ">;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum AVG_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum TYPE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

.field public static final enum _ID:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 35
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "SAMPLE_TIME"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 36
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "MAX_SAMPLE_TIME"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 37
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "TYPE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TYPE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 38
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 39
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "_ID"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->_ID:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 40
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "TOTAL_KILO_CALORIE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 41
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "AVG_TOTAL_KILO_CALORIE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->AVG_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 42
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "SUM_TOTAL_KILO_CALORIE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    .line 34
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TYPE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->_ID:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->AVG_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->$VALUES:[Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->$VALUES:[Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    return-object v0
.end method
