.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$2;
.super Ljava/lang/Object;
.source "AMapExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/amap/api/maps2d/AMap$OnMapClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setUpMapIfNeeded(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapClick(Lcom/amap/api/maps2d/model/LatLng;)V
    .locals 2
    .param p1, "arg0"    # Lcom/amap/api/maps2d/model/LatLng;

    .prologue
    .line 358
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapClickListener:Lcom/amap/api/maps2d/AMap$OnMapClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 359
    new-instance v0, Landroid/view/View;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 360
    .local v0, "v":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 361
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->mMapClickListener:Lcom/amap/api/maps2d/AMap$OnMapClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/maps2d/AMap$OnMapClickListener;->onMapClick(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 363
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method
