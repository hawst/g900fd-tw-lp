.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;
.super Ljava/lang/Object;
.source "CignaCoachUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static volatile myLock:Ljava/lang/Object;

.field private static sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;


# instance fields
.field private mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

.field private mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field private mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

.field private mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->myLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 80
    return-void
.end method

.method public static capitalizeFirstLetter(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 882
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 883
    :cond_0
    const-string p0, ""

    .line 891
    .end local p0    # "string":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 884
    .restart local p0    # "string":Ljava/lang/String;
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 887
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 888
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 889
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 890
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private clearCoachData()V
    .locals 1

    .prologue
    .line 620
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->clearCoachData()V

    .line 621
    return-void
.end method

.method private clearPreference(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 625
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setIsCignaCoachInited(Z)V

    .line 627
    return-void
.end method

.method public static convertDecimalFormat(F)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # F

    .prologue
    .line 632
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v2, "#.#"

    invoke-direct {v0, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 633
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setDecimalSeparatorAlwaysShown(Z)V

    .line 634
    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 635
    float-to-double v2, p0

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 636
    .local v1, "value":Ljava/lang/String;
    return-object v1
.end method

.method public static deCapitalizeFirstLetter(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 895
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 896
    :cond_0
    const-string p0, ""

    .line 904
    .end local p0    # "string":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 897
    .restart local p0    # "string":Ljava/lang/String;
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 900
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 901
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 902
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 903
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;
    .locals 2

    .prologue
    .line 71
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->myLock:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    .line 75
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 873
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 874
    .local v0, "intentSyncStart":Landroid/content/Intent;
    const-string v2, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 875
    const-string/jumbo v2, "widgetActivityAction"

    const-string v3, "com.sec.shealth.action.COACH"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 876
    const-string/jumbo v2, "widgetActivityPackage"

    const-string v3, "com.sec.android.app.shealth"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 877
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x98e

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 878
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    return-object v1
.end method


# virtual methods
.method public convertCategoryToRequestCode(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I
    .locals 3
    .param p1, "category"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 261
    const/4 v0, -0x1

    .line 262
    .local v0, "requestCode":I
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 281
    :goto_0
    return v0

    .line 264
    :pswitch_0
    const/16 v0, 0x64

    .line 265
    goto :goto_0

    .line 267
    :pswitch_1
    const/16 v0, 0xc8

    .line 268
    goto :goto_0

    .line 270
    :pswitch_2
    const/16 v0, 0x12c

    .line 271
    goto :goto_0

    .line 273
    :pswitch_3
    const/16 v0, 0x190

    .line 274
    goto :goto_0

    .line 276
    :pswitch_4
    const/16 v0, 0x1f4

    .line 277
    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public convertCategoryTypeToSpecific(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    .locals 3
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 238
    const/4 v0, 0x0

    .line 239
    .local v0, "messageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 256
    :goto_0
    return-object v0

    .line 241
    :pswitch_0
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_EXERCISE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 242
    goto :goto_0

    .line 244
    :pswitch_1
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_FOOD:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 245
    goto :goto_0

    .line 247
    :pswitch_2
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_SLEEP:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 248
    goto :goto_0

    .line 250
    :pswitch_3
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_STRESS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 251
    goto :goto_0

    .line 253
    :pswitch_4
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_WEIGHT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    goto :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public declared-synchronized dismissCignaWidgetNotification(Ljava/lang/String;)V
    .locals 3
    .param p1, "widgetType"    # Ljava/lang/String;

    .prologue
    .line 791
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 793
    .local v0, "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    const-string v1, "SHEALTH_WIDGET"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 794
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->dismissWidgetNotification(Lcom/cigna/coach/interfaces/IWidget$WidgetType;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 801
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 802
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 803
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 804
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 805
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 806
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 807
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 808
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809
    :cond_0
    monitor-exit p0

    return-void

    .line 795
    :cond_1
    :try_start_1
    const-string v1, "COACH_WIDGET"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->dismissWidgetNotification(Lcom/cigna/coach/interfaces/IWidget$WidgetType;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 791
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public enableCignaWidgetNeeded(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    .line 711
    :try_start_0
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardWidget;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 712
    .local v2, "widgetName":Landroid/content/ComponentName;
    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->enableWidgets(Landroid/content/ComponentName;Z)V

    .line 713
    new-instance v1, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 714
    .local v1, "plainWidgetName":Landroid/content/ComponentName;
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->enableWidgets(Landroid/content/ComponentName;Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 718
    .end local v1    # "plainWidgetName":Landroid/content/ComponentName;
    .end local v2    # "widgetName":Landroid/content/ComponentName;
    :goto_0
    return-void

    .line 715
    :catch_0
    move-exception v0

    .line 716
    .local v0, "npe":Ljava/lang/NullPointerException;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public finishIfCignaUnsupported(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 101
    if-nez p1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Killed activity cause coach not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public declared-synchronized getAppropriatePendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetType"    # Ljava/lang/String;
    .param p3, "requestCode"    # I

    .prologue
    .line 837
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 838
    .local v0, "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    const/4 v1, 0x0

    .line 839
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "SHEALTH_WIDGET"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v2, :cond_0

    .line 842
    sget-object v2, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 843
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 845
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageIntent()Landroid/content/Intent;

    move-result-object v1

    .line 846
    const-string/jumbo v2, "widgetType"

    const-string v3, "SHEALTH_WIDGET"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 847
    const-string v2, "COACH_WIDGET"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 861
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v2, p3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :goto_1
    monitor-exit p0

    return-object v2

    .line 848
    :cond_1
    :try_start_1
    const-string v2, "COACH_WIDGET"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 849
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v2, :cond_2

    .line 851
    sget-object v2, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 852
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 854
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageIntent()Landroid/content/Intent;

    move-result-object v1

    .line 855
    const-string/jumbo v2, "widgetType"

    const-string v3, "COACH_WIDGET"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 856
    const-string v2, "COACH_WIDGET"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 837
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    .end local v1    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 858
    .restart local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getCignaWebLinkPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 865
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 866
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "http://www.cigna.com/cignacoach"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 867
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 868
    const-string v2, "COACH_WIDGET"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 869
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x98e

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    return-object v2
.end method

.method public getMaskedImage(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgResId"    # I
    .param p3, "maskImgResId"    # I

    .prologue
    const/4 v9, 0x0

    .line 669
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 670
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 671
    .local v3, "orgImg":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 672
    .local v2, "maskImg":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v8, 0x1

    invoke-static {v2, v6, v7, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 673
    .local v1, "maskBmp":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 674
    const/4 v2, 0x0

    .line 675
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 677
    .local v5, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v5}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 678
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 679
    .local v4, "paint":Landroid/graphics/Paint;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 681
    invoke-virtual {v0, v3, v9, v9, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 682
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 684
    invoke-virtual {v0, v1, v9, v9, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 685
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 687
    if-eq v5, v3, :cond_0

    if-eq v5, v1, :cond_0

    .line 688
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 689
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 691
    const/4 v3, 0x0

    .line 692
    const/4 v1, 0x0

    .line 695
    :cond_0
    return-object v5
.end method

.method public getWeek(J)Ljava/util/List;
    .locals 5
    .param p1, "startTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 323
    .local v2, "timemilli":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v1

    .line 325
    .local v1, "sc":Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x7

    if-ge v0, v3, :cond_0

    .line 326
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    const/4 v3, 0x5

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->add(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 329
    :cond_0
    return-object v2
.end method

.method public declared-synchronized getWidgetInfo(Landroid/content/Context;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 814
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetType:[I

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 832
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return-object v1

    .line 816
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v1, :cond_0

    .line 818
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 819
    .local v0, "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 820
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 822
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    goto :goto_0

    .line 824
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v1, :cond_1

    .line 826
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 827
    .restart local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 828
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 830
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 814
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized getWidgetText(Landroid/content/Context;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 766
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetType:[I

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 784
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return-object v1

    .line 768
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v1, :cond_0

    .line 770
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 771
    .local v0, "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 772
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 774
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 776
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v1, :cond_1

    .line 778
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 779
    .restart local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 780
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 782
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageText()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 766
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hasCoachDBFile(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 908
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/data/data/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/databases/Coach.db"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 909
    .local v0, "coachDBFileFullPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 910
    .local v1, "dbFile":Ljava/io/File;
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Is exist coach db file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    return v2
.end method

.method public declared-synchronized initializeWidgetInfo(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetType"    # Ljava/lang/String;

    .prologue
    .line 736
    monitor-enter p0

    :try_start_0
    const-string v1, "SHEALTH_WIDGET"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 737
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v1, :cond_0

    .line 739
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 740
    .local v0, "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 741
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 751
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 743
    :cond_1
    :try_start_1
    const-string v1, "COACH_WIDGET"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 744
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    if-nez v1, :cond_0

    .line 746
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 747
    .restart local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 748
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 736
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized invalidateWidgets()V
    .locals 2

    .prologue
    .line 756
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    .line 757
    .local v0, "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 758
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mSHealthWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 759
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .line 760
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachAndroidWidgetInfo:Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->mCoachWidgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 761
    monitor-exit p0

    return-void

    .line 756
    .end local v0    # "ccdm":Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public isAccessHealthServiceDB()Z
    .locals 1

    .prologue
    .line 730
    const/4 v0, 0x1

    return v0
.end method

.method public isCignaGoalMatchedCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)Z
    .locals 3
    .param p1, "goalCategory"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p2, "currentCategory"    # I

    .prologue
    .line 201
    const/4 v0, 0x0

    .line 203
    .local v0, "isMatchedCategory":Z
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 233
    :cond_0
    :goto_0
    return v0

    .line 205
    :pswitch_0
    const/16 v1, 0xc8

    if-ne p2, v1, :cond_0

    .line 206
    const/4 v0, 0x1

    goto :goto_0

    .line 210
    :pswitch_1
    const/16 v1, 0x64

    if-ne p2, v1, :cond_0

    .line 211
    const/4 v0, 0x1

    goto :goto_0

    .line 215
    :pswitch_2
    const/16 v1, 0x12c

    if-ne p2, v1, :cond_0

    .line 216
    const/4 v0, 0x1

    goto :goto_0

    .line 220
    :pswitch_3
    const/16 v1, 0x190

    if-ne p2, v1, :cond_0

    .line 221
    const/4 v0, 0x1

    goto :goto_0

    .line 225
    :pswitch_4
    const/16 v1, 0x1f4

    if-ne p2, v1, :cond_0

    .line 226
    const/4 v0, 0x1

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public isCoachBREnabled(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 135
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    const/4 v1, 0x0

    .line 138
    .local v1, "cignaBackupRestoreEnable":Z
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 144
    :goto_0
    if-eqz v0, :cond_0

    .line 146
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "CoachBackup"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 151
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cignaBackupRestoreEnable: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    return v1

    .line 139
    :catch_0
    move-exception v2

    .line 140
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string v4, "Error in getting Application info"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 148
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string v4, "Application Info is null, hence unable to get the value cignaBackupRestoreEnable which is set to false by default"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isFuture(J)Z
    .locals 13
    .param p1, "timeMill"    # J

    .prologue
    const/16 v12, 0xe

    const/16 v11, 0xd

    const/16 v10, 0xc

    const/16 v9, 0xb

    const/4 v5, 0x0

    .line 356
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 357
    .local v0, "sc":Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 358
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 359
    invoke-virtual {v0, v11, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 360
    invoke-virtual {v0, v12, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 362
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v3

    .line 364
    .local v3, "todayTimeInMillis":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 365
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 366
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 367
    invoke-virtual {v0, v11, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 368
    invoke-virtual {v0, v12, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 370
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v1

    .line 372
    .local v1, "targetTimeInMillis":J
    cmp-long v6, v1, v3

    if-lez v6, :cond_0

    .line 373
    const/4 v5, 0x1

    .line 376
    :cond_0
    return v5
.end method

.method public isLocalDataPresent()Z
    .locals 14

    .prologue
    .line 157
    const/4 v5, 0x1

    .line 159
    .local v5, "isLocalDataPresent":Z
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getFavoriteArticle()Ljava/util/ArrayList;

    move-result-object v0

    .line 160
    .local v0, "articles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-eqz v11, :cond_4

    .line 161
    sget-object v11, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string v12, "isLocalDataPresent, fav articles present"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :goto_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v11

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v4

    .line 166
    .local v4, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v10

    .line 168
    .local v10, "userId":Ljava/lang/String;
    const/4 v6, 0x0

    .line 170
    .local v6, "isScoreAvailable":Z
    :try_start_0
    sget-object v11, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v12

    const/4 v13, 0x0

    invoke-interface {v4, v10, v11, v12, v13}, Lcom/cigna/coach/interfaces/ILifeStyle;->getScores(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Ljava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v9

    .line 171
    .local v9, "scores":Lcom/cigna/coach/apiobjects/Scores;
    if-eqz v9, :cond_5

    .line 172
    sget-object v11, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v12, "scores NOT null"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v8

    .line 175
    .local v8, "score":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-virtual {v8}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 177
    .local v2, "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 178
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 179
    .local v7, "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {v8, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 180
    .local v3, "i":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ltz v11, :cond_0

    .line 181
    sget-object v11, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "scores available for category: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    const/4 v6, 0x1

    .line 193
    .end local v2    # "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .end local v3    # "i":Ljava/lang/Integer;
    .end local v7    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v8    # "score":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    .end local v9    # "scores":Lcom/cigna/coach/apiobjects/Scores;
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-nez v11, :cond_3

    :cond_2
    if-nez v6, :cond_3

    .line 194
    const/4 v5, 0x0

    .line 196
    :cond_3
    sget-object v11, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "isLocalDataPresent: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    return v5

    .line 163
    .end local v4    # "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    .end local v6    # "isScoreAvailable":Z
    .end local v10    # "userId":Ljava/lang/String;
    :cond_4
    sget-object v11, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string v12, "isLocalDataPresent, NO fav articles"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 187
    .restart local v4    # "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    .restart local v6    # "isScoreAvailable":Z
    .restart local v9    # "scores":Lcom/cigna/coach/apiobjects/Scores;
    .restart local v10    # "userId":Ljava/lang/String;
    :cond_5
    :try_start_1
    sget-object v11, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v12, "scores null"

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 189
    .end local v9    # "scores":Lcom/cigna/coach/apiobjects/Scores;
    :catch_0
    move-exception v1

    .line 190
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_1
.end method

.method public isSameDay(JJ)Z
    .locals 11
    .param p1, "timeMill"    # J
    .param p3, "timeMill2"    # J

    .prologue
    const/16 v10, 0xe

    const/16 v9, 0xd

    const/16 v8, 0xc

    const/16 v7, 0xb

    const/4 v5, 0x0

    .line 468
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 469
    .local v0, "sc":Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    invoke-virtual {v0, v7, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 470
    invoke-virtual {v0, v8, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 471
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 472
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 474
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v3

    .line 476
    .local v3, "targetTimeInMillis":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-virtual {v6, p3, p4}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 477
    invoke-virtual {v0, v7, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 478
    invoke-virtual {v0, v8, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 479
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 480
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 482
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v1

    .line 484
    .local v1, "target2TimeInMillis":J
    cmp-long v6, v3, v1

    if-nez v6, :cond_0

    .line 485
    const/4 v5, 0x1

    .line 488
    :cond_0
    return v5
.end method

.method public isToday(J)Z
    .locals 13
    .param p1, "timeMill"    # J

    .prologue
    const/16 v12, 0xe

    const/16 v11, 0xd

    const/16 v10, 0xc

    const/16 v9, 0xb

    const/4 v5, 0x0

    .line 383
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 384
    .local v0, "sc":Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 385
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 386
    invoke-virtual {v0, v11, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 387
    invoke-virtual {v0, v12, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 389
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v3

    .line 391
    .local v3, "todayTimeInMillis":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 392
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 393
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 394
    invoke-virtual {v0, v11, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 395
    invoke-virtual {v0, v12, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 397
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v1

    .line 399
    .local v1, "targetTimeInMillis":J
    cmp-long v6, v1, v3

    if-nez v6, :cond_0

    .line 400
    const/4 v5, 0x1

    .line 403
    :cond_0
    return v5
.end method

.method public isTomorrow(J)Z
    .locals 13
    .param p1, "timeMill"    # J

    .prologue
    const/16 v12, 0xe

    const/16 v11, 0xd

    const/16 v10, 0xc

    const/16 v9, 0xb

    const/4 v5, 0x0

    .line 410
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 411
    .local v0, "sc":Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 412
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 413
    invoke-virtual {v0, v11, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 414
    invoke-virtual {v0, v12, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 416
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v3

    .line 418
    .local v3, "todayTimeInMillis":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getInstance()Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->init(J)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    move-result-object v0

    .line 419
    invoke-virtual {v0, v9, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 420
    invoke-virtual {v0, v10, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 421
    invoke-virtual {v0, v11, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 422
    invoke-virtual {v0, v12, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->set(II)Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;

    .line 424
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/calendar/SingletonCalendar;->getTimeInMillis()J

    move-result-wide v1

    .line 426
    .local v1, "targetTimeInMillis":J
    sub-long v6, v1, v3

    const-wide/32 v8, 0x5265c00

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 427
    const/4 v5, 0x1

    .line 430
    :cond_0
    return v5
.end method

.method public killProcessIfDBNotInitialised(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 114
    if-nez p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 120
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 122
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 123
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 124
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 126
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Killed SHealth cause db uninitialised"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0
.end method

.method public registerBR(Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;Landroid/content/Context;)V
    .locals 2
    .param p1, "rcvr"    # Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 85
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 86
    const-string v1, "com.sec.android.app.shealth.cignacoach.HANDLE_MIDNIGHT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 87
    const-string v1, "com.cigna.mobile.coach.GOAL_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 88
    const-string v1, "com.cigna.mobile.coach.GOAL_CANCELLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 89
    const-string v1, "com.cigna.mobile.coach.MISSION_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 90
    const-string v1, "com.cigna.mobile.coach.MISSION_FAILED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2, p1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 92
    return-void
.end method

.method public resetData(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 546
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isFirstLoadingComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "resetData"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->clearCoachData()V

    .line 550
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->clearPreference(Landroid/content/Context;)V

    .line 551
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.cignacoach.RESET_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 556
    :goto_0
    return-void

    .line 554
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->TAG:Ljava/lang/String;

    const-string v1, "Because cigna don\'t have DB, no resetData~~"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startWeb(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 702
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 703
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 704
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 705
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 706
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 707
    return-void
.end method

.method public unregisterBR(Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;Landroid/content/Context;)V
    .locals 0
    .param p1, "rcvr"    # Lcom/sec/android/app/shealth/cignacoach/receiver/CignaCoachDateChageReceiver;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    invoke-virtual {p2, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 97
    return-void
.end method
