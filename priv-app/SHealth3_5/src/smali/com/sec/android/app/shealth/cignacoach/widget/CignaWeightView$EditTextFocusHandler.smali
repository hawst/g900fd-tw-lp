.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;
.super Ljava/lang/Object;
.source "CignaWeightView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextFocusHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 387
    if-nez p2, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->checkValueText()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;->onCignaDialog(Landroid/view/View;)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setMinValueText()V

    .line 396
    :cond_0
    return-void
.end method
