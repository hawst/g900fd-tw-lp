.class Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;
.super Landroid/os/AsyncTask;
.source "FirstTimeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CreateUserAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mContry:Ljava/lang/String;

.field private mIntroMsg:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 185
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 222
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER_HELLO:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCoachesCornerMsg(Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->mIntroMsg:Ljava/lang/String;

    .line 223
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getCignaIntroMsg()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->mIntroMsg:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setCignaIntroMsg(Ljava/lang/String;)V

    .line 228
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->mContry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->createAndUpdateUser(Ljava/lang/String;)V

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->isFirstTimeNoScore()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    # setter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mIsScorePresent:Z
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$002(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;Z)Z

    .line 231
    const/4 v0, 0x0

    return-object v0

    .line 230
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 185
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 236
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 238
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setFirstLoadingComplete(Z)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstLoadingLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$200(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mCignaFirstMessageLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$300(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getCignaIntroMsg()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->updateIntroMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$400(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->refreshFragmentFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$500(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    .line 253
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 192
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 195
    .local v0, "locale":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->mContry:Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync$1;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$102(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment$CreateUserAsync;->this$0:Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/FirstTimeFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    .line 210
    return-void
.end method
