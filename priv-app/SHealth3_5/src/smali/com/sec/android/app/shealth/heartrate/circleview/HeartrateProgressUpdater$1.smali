.class Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;
.super Ljava/lang/Object;
.source "HeartrateProgressUpdater.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->isRun:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$100(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->messagesCount:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$300(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0x2d

    if-ge v1, v2, :cond_2

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 66
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$500(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # operator++ for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$208(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 51
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0x5a

    if-ge v1, v2, :cond_3

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_1

    .line 53
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0x87

    if-ge v1, v2, :cond_4

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_1

    .line 55
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0xb4

    if-ge v1, v2, :cond_5

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_1

    .line 57
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0xe1

    if-ge v1, v2, :cond_6

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    div-int/lit8 v1, v1, 0x5

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_1

    .line 59
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0x10e

    if-ge v1, v2, :cond_7

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto/16 :goto_1

    .line 61
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0x13b

    if-ge v1, v2, :cond_8

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto/16 :goto_1

    .line 63
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    const/16 v2, 0x168

    if-ge v1, v2, :cond_1

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$400(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 71
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->messagesCount:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$300(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # setter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$202(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;I)I

    goto/16 :goto_0

    .line 75
    :cond_a
    return-void
.end method
