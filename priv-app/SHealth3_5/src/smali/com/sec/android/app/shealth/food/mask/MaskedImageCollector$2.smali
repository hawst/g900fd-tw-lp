.class Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;
.super Ljava/lang/Object;
.source "MaskedImageCollector.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->collectBitmaps(Lcom/sec/android/app/shealth/food/mask/MaskTools;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

.field final synthetic val$images:Ljava/util/List;

.field final synthetic val$listener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;Ljava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->val$images:Ljava/util/List;

    iput-object p3, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->val$listener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawingResult(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "resultBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    # getter for: Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->mMaskedBitmapsProvider:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->access$100(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;)Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->val$images:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;->addToCache(Landroid/graphics/Bitmap;Ljava/util/List;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->val$listener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    # invokes: Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->invokeCallback(Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Landroid/graphics/Bitmap;)V
    invoke-static {v0, v1, p1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->access$200(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;Landroid/graphics/Bitmap;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$2;->val$listener:Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->removeCallback(Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    .line 126
    return-void
.end method
