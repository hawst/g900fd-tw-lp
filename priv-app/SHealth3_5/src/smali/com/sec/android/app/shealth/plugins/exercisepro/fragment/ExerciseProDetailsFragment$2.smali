.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$2;
.super Ljava/lang/Object;
.source "ExerciseProDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->initPhotoController(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)V
    .locals 0

    .prologue
    .line 1013
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->isNinePopup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1019
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->btnGallery:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)Landroid/widget/ImageButton;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 1020
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->doTakePhotoAction()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsFragment;)V

    .line 1022
    :cond_0
    return-void
.end method
