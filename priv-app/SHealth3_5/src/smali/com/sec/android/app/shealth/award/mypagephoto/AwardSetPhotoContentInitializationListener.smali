.class public Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener;
.super Ljava/lang/Object;
.source "AwardSetPhotoContentInitializationListener.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;,
        Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$GetContentAdapter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    return-void
.end method

.method private getPhotoDesiredSize(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    .line 90
    .local v0, "parentActivity":Landroid/app/Activity;
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/app/Activity;->getWallpaperDesiredMinimumWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/app/Activity;->getWallpaperDesiredMinimumHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    return-object v1
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 11
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 53
    new-instance v3, Ljava/util/ArrayList;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 55
    .local v3, "requestInfos":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;>;"
    sget-object v6, Lcom/sec/android/app/shealth/common/utils/UDR;->NORMAL_PHOTO_PATH:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getTempUriFromString(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 56
    .local v5, "tempImageUri":Landroid/net/Uri;
    if-nez v5, :cond_0

    .line 57
    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/UDR;->NORMAL_PHOTO_PATH:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 59
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener;->getPhotoDesiredSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    .line 60
    .local v2, "photoSize":Landroid/graphics/Point;
    const/4 v6, 0x2

    new-array v4, v6, [F

    fill-array-data v4, :array_0

    .line 62
    .local v4, "spotlight":[F
    new-instance v6, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x1

    const-string v9, "Camera"

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;-><init>(Landroid/content/Intent;ILjava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    new-instance v6, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;

    iget v7, v2, Landroid/graphics/Point;->x:I

    iget v8, v2, Landroid/graphics/Point;->y:I

    const/4 v9, 0x0

    aget v9, v4, v9

    const/4 v10, 0x1

    aget v10, v4, v10

    invoke-static {v5, v7, v8, v9, v10}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getGalleryIntentCustomExtraOutput(Landroid/net/Uri;IIFF)Landroid/content/Intent;

    move-result-object v7

    const/4 v8, 0x2

    const-string v9, "Gallery"

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;-><init>(Landroid/content/Intent;ILjava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    new-instance v6, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const/4 v8, 0x3

    const-string v9, "Delete"

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$RequestInfo;-><init>(Landroid/content/Intent;ILjava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v0, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$GetContentAdapter;

    invoke-direct {v0, p2, v3}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$GetContentAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 70
    .local v0, "adapter":Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$GetContentAdapter;
    const v6, 0x7f080331

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 71
    .local v1, "gridView":Landroid/widget/ListView;
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 72
    new-instance v6, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$1;

    invoke-direct {v6, p0, v3, p3, p2}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener$1;-><init>(Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener;Ljava/util/List;Landroid/app/Dialog;Landroid/app/Activity;)V

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 86
    return-void

    .line 60
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
