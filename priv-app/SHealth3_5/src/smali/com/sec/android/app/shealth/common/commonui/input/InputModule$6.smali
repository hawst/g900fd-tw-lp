.class Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onControllerTapped(Z)V
    .locals 2
    .param p1, "isStart"    # Z

    .prologue
    .line 374
    if-eqz p1, :cond_0

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$700(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->isInputWithEmptyListener()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$700(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    move-result-object v0

    const v1, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValue(F)V

    .line 391
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 393
    return-void

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->hideKeyboard()V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$6;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    goto :goto_0
.end method
