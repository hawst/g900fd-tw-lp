.class Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceDialogButtonController;
.super Ljava/lang/Object;
.source "SplashScreenActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/SplashScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateServiceDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V
    .locals 0

    .prologue
    .line 1030
    iput-object p1, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/SplashScreenActivity;Lcom/sec/android/app/shealth/SplashScreenActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/SplashScreenActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/SplashScreenActivity$1;

    .prologue
    .line 1030
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceDialogButtonController;-><init>(Lcom/sec/android/app/shealth/SplashScreenActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 1037
    sget-object v0, Lcom/sec/android/app/shealth/SplashScreenActivity$13;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1049
    :goto_0
    return-void

    .line 1040
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getIntentForHealthServiceUpdate()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 1041
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finishApplication()V

    goto :goto_0

    .line 1044
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/SplashScreenActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/SplashScreenActivity;->finishApplication()V

    goto :goto_0

    .line 1037
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
