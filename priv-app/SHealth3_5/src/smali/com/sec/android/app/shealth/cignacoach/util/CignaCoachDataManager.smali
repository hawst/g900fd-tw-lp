.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
.super Ljava/lang/Object;
.source "CignaCoachDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;


# instance fields
.field private mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 69
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    .line 76
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mInstance:Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    return-object v0
.end method

.method private getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v0, :cond_0

    .line 590
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isAccessHealthServiceDB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    return-object v0
.end method

.method private savePrefSpecificMessage(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;)V
    .locals 2
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 429
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 448
    :goto_0
    return-void

    .line 431
    :pswitch_0
    const-string v0, "COACH_MSG_SPECIFIC_EXERCISE"

    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 434
    :pswitch_1
    const-string v0, "COACH_MSG_SPECIFIC_FOOD"

    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 437
    :pswitch_2
    const-string v0, "COACH_MSG_SPECIFIC_SLEEP"

    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 440
    :pswitch_3
    const-string v0, "COACH_MSG_SPECIFIC_STRESS"

    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 443
    :pswitch_4
    const-string v0, "COACH_MSG_SPECIFIC_WEIGHT"

    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 429
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setUserMetrics(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics;)Z
    .locals 5
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "userMetrics"    # Lcom/cigna/coach/apiobjects/UserMetrics;

    .prologue
    .line 566
    const/4 v2, 0x0

    .line 567
    .local v2, "ret":Z
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v1

    .line 570
    .local v1, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/cigna/coach/interfaces/ILifeStyle;->setUserMetrics(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics;)Z
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 575
    :goto_0
    return v2

    .line 571
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public clearCoachData()V
    .locals 4

    .prologue
    .line 145
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v1

    .line 147
    .local v1, "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-interface {v1}, Lcom/cigna/coach/interfaces/ILifeStyle;->clearUserSelections()V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    .end local v1    # "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public createAndUpdateUser(Ljava/lang/String;)V
    .locals 3
    .param p1, "contry"    # Ljava/lang/String;

    .prologue
    .line 543
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    if-nez v1, :cond_0

    .line 544
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    const-string v2, "createAndUpdateUser() UserProfile is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    :goto_0
    return-void

    .line 555
    :cond_0
    new-instance v0, Lcom/cigna/coach/apiobjects/UserMetrics;

    invoke-direct {v0}, Lcom/cigna/coach/apiobjects/UserMetrics;-><init>()V

    .line 556
    .local v0, "userMetrics":Lcom/cigna/coach/apiobjects/UserMetrics;
    const v1, 0x2e635

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v2

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->MALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    :goto_1
    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setGender(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;)V

    .line 557
    invoke-virtual {v0, p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setCountry(Ljava/lang/String;)V

    .line 558
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(J)Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setDateOfBirth(Ljava/util/Calendar;)V

    .line 559
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setHeight(F)V

    .line 560
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v1

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setWeight(F)V

    .line 562
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->setUserMetrics(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics;)Z

    goto :goto_0

    .line 556
    :cond_1
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->FEMALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    goto :goto_1
.end method

.method public dismissWidgetNotification(Lcom/cigna/coach/interfaces/IWidget$WidgetType;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .locals 5
    .param p1, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .param p2, "widgetInfoType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .prologue
    .line 683
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getWidget(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IWidget;

    move-result-object v1

    .line 684
    .local v1, "iWidget":Lcom/cigna/coach/interfaces/IWidget;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v2

    .line 686
    .local v2, "userId":Ljava/lang/String;
    :try_start_0
    invoke-interface {v1, v2, p1, p2}, Lcom/cigna/coach/interfaces/IWidget;->dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 690
    :goto_0
    return-void

    .line 687
    :catch_0
    move-exception v0

    .line 688
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAboutMsg()Ljava/lang/String;
    .locals 5

    .prologue
    .line 157
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v2

    .line 159
    .local v2, "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_ABOUT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    invoke-interface {v2, v3, v4}, Lcom/cigna/coach/interfaces/ILifeStyle;->getCoachMsg(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    .line 160
    .local v0, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 168
    .end local v0    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v2    # "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v3

    .line 164
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 168
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getAssessmentCategoryCount()I
    .locals 7

    .prologue
    .line 693
    const/4 v0, 0x0

    .line 694
    .local v0, "assessmentCategoryCount":I
    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v5

    .line 695
    .local v5, "scores":Lcom/cigna/coach/apiobjects/Scores;
    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v1

    .line 697
    .local v1, "categoryInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 699
    .local v2, "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 700
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 701
    .local v4, "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {v1, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 703
    .local v3, "i":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ltz v6, :cond_0

    .line 704
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 708
    .end local v3    # "i":Ljava/lang/Integer;
    .end local v4    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_1
    return v0
.end method

.method public getAssessmentData(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .locals 1
    .param p1, "type"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 216
    if-nez p1, :cond_0

    sget-object p1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .end local p1    # "type":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory;->createDatas(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    move-result-object v0

    return-object v0
.end method

.method public varargs getAssessmentData([Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "type"    # [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory;->createDatas([Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getBadgeDataById(Landroid/content/Context;I)Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "badgeIds"    # I

    .prologue
    .line 514
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v13

    .line 515
    .local v13, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    const/4 v9, 0x0

    .line 517
    .local v9, "badgeData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v15

    .line 518
    .local v15, "userId":Ljava/lang/String;
    invoke-interface {v13, v15}, Lcom/cigna/coach/interfaces/ILifeStyle;->getBadgeCount(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/BadgeInfo;

    move-result-object v10

    .line 520
    .local v10, "badgeInfo":Lcom/cigna/coach/apiobjects/BadgeInfo;
    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/BadgeInfo;->getBadges()Ljava/util/List;

    move-result-object v11

    .line 521
    .local v11, "badgesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 522
    .local v14, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/Badge;>;"
    :cond_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 523
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/cigna/coach/apiobjects/Badge;

    .line 526
    .local v8, "badge":Lcom/cigna/coach/apiobjects/Badge;
    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v2

    move/from16 v0, p2

    if-ne v2, v0, :cond_0

    .line 527
    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeEarnedDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 528
    .local v5, "achieveDate":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeEarnedDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 529
    .local v6, "achieveTime":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v2

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadge()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeImage()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .end local v9    # "badgeData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    .local v1, "badgeData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    move-object v2, v1

    .line 538
    .end local v5    # "achieveDate":Ljava/lang/String;
    .end local v6    # "achieveTime":Ljava/lang/String;
    .end local v8    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    .end local v10    # "badgeInfo":Lcom/cigna/coach/apiobjects/BadgeInfo;
    .end local v11    # "badgesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v14    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v15    # "userId":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 534
    .end local v1    # "badgeData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    .restart local v9    # "badgeData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    :catch_0
    move-exception v12

    .line 535
    .local v12, "e":Ljava/lang/Throwable;
    invoke-virtual {v12}, Ljava/lang/Throwable;->printStackTrace()V

    .line 538
    .end local v12    # "e":Ljava/lang/Throwable;
    :cond_1
    const/4 v1, 0x0

    move-object v2, v1

    move-object v1, v9

    .end local v9    # "badgeData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    .restart local v1    # "badgeData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    goto :goto_0
.end method

.method public getBadgeIds(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    .local p1, "badgeList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 501
    .local v1, "earnedBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 502
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Badge;

    .line 504
    .local v0, "badge":Lcom/cigna/coach/apiobjects/Badge;
    if-eqz v0, :cond_0

    .line 505
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 509
    .end local v0    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    :cond_1
    return-object v1
.end method

.method public getBadgeInfo()Lcom/cigna/coach/apiobjects/BadgeInfo;
    .locals 5

    .prologue
    .line 415
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v1

    .line 416
    .local v1, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v3

    .line 419
    .local v3, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/cigna/coach/interfaces/ILifeStyle;->getBadgeCount(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/BadgeInfo;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 425
    :goto_0
    return-object v0

    .line 422
    :catch_0
    move-exception v2

    .line 423
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 425
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCoachCategoryMessage(Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 4
    .param p1, "meesageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .prologue
    .line 404
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 405
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v2

    .line 407
    .local v2, "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Lcom/cigna/coach/interfaces/ILifeStyle;->getCoachMsg(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 411
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v2    # "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v3

    .line 408
    :catch_0
    move-exception v1

    .line 409
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 411
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCoachMessage()Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 5

    .prologue
    .line 97
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v2

    .line 99
    .local v2, "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    invoke-interface {v2, v3, v4}, Lcom/cigna/coach/interfaces/ILifeStyle;->getCoachMsg(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 108
    .end local v2    # "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v0

    .line 103
    :catch_0
    move-exception v1

    .line 105
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 108
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCoachMessage(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 4
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;)",
            "Lcom/cigna/coach/apiobjects/CoachMessage;"
        }
    .end annotation

    .prologue
    .line 474
    .local p2, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-direct {v0}, Lcom/cigna/coach/apiobjects/CoachMessage;-><init>()V

    .line 476
    .local v0, "cancelUnalignedGoalCoachMsg":Lcom/cigna/coach/apiobjects/CoachMessage;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 486
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v2, v3, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 489
    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v0, v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 490
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->setMessage(Ljava/lang/String;)V

    .line 491
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->setMessageDescription(Ljava/lang/String;)V

    .line 476
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 495
    :cond_1
    return-object v0
.end method

.method public getCoachMsgsForLifestyleReassessment()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 355
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v2

    .line 357
    .local v2, "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->COACH_MESSAGE_LIFESTYLE_REASSESSMENT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    invoke-interface {v2, v3, v4}, Lcom/cigna/coach/interfaces/ILifeStyle;->getCoachMsgs(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;)Ljava/util/List;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 361
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v2    # "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v3

    .line 358
    :catch_0
    move-exception v1

    .line 359
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception in getCoachMsgsForLifestyleReassessment, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCoachesCornerMsg()Ljava/lang/String;
    .locals 5

    .prologue
    .line 113
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v2

    .line 115
    .local v2, "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    invoke-interface {v2, v3, v4}, Lcom/cigna/coach/interfaces/ILifeStyle;->getCoachMsg(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    .line 119
    .local v0, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 124
    .end local v0    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v2    # "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v3

    .line 120
    :catch_0
    move-exception v1

    .line 122
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 124
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCoachesCornerMsg(Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Ljava/lang/String;
    .locals 5
    .param p1, "coachSpecificMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .prologue
    .line 129
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v2

    .line 131
    .local v2, "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Lcom/cigna/coach/interfaces/ILifeStyle;->getCoachMsg(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    .line 135
    .local v0, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 140
    .end local v0    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v2    # "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v3

    .line 136
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 140
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCurrentGoalCount()I
    .locals 1

    .prologue
    .line 202
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getCurrentGoalCount()I

    move-result v0

    return v0
.end method

.method public getCurrentGoalListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "goalDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local p2, "missionDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getCurrentGoalData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentGoalMissionCount()[I
    .locals 1

    .prologue
    .line 207
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getCurrentGoalMissionCount()[I

    move-result-object v0

    return-object v0
.end method

.method public getEngagingMessage()Ljava/lang/String;
    .locals 7

    .prologue
    .line 366
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 367
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v3

    .line 371
    .local v3, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    :try_start_0
    invoke-interface {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions;->getMissionNotificationFailureMessage()Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v1

    .line 372
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachMessage;
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "responese type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "engage msg: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "engage desc: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 382
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachMessage;
    :goto_0
    return-object v4

    .line 377
    :catch_0
    move-exception v2

    .line 379
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in getEngagingMessage, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getHistoryListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;)",
            "Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "goalDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    .local p2, "missionDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getGoalMissionHistory(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    move-result-object v0

    return-object v0
.end method

.method public getScoreBGImage()Ljava/lang/String;
    .locals 5

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 174
    .local v0, "bgName":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v2

    .line 176
    .local v2, "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/cigna/coach/interfaces/ILifeStyle;->getScoreBackgroundImage(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 182
    .end local v2    # "ls":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v0

    .line 178
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public getScoreStatus(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 7
    .param p1, "periodType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/ScoreInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 343
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v3

    .line 344
    .local v3, "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, p1}, Lcom/cigna/coach/interfaces/ILifeStyle;->getScoresHistory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 349
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v3    # "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    :goto_0
    return-object v1

    .line 346
    :catch_0
    move-exception v2

    .line 347
    .local v2, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in getScoreStatus, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;
    .locals 6
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 81
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v1

    .line 84
    .local v1, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v1, v3, p1, v4, v5}, Lcom/cigna/coach/interfaces/ILifeStyle;->getScores(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Ljava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/Scores;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 92
    :goto_0
    return-object v2

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 92
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 579
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v0

    if-nez v0, :cond_0

    .line 580
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    const-string v1, "getUserId() UserProfile is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    const-string v0, ""

    .line 585
    :goto_0
    return-object v0

    .line 584
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User Id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserProfile()Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getWidgetInfo(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .locals 6
    .param p1, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 658
    new-instance v3, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    invoke-direct {v3}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;-><init>()V

    .line 659
    .local v3, "widgetInfo":Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/factory/CoachFactory;->getWidget(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IWidget;

    move-result-object v1

    .line 660
    .local v1, "iWidget":Lcom/cigna/coach/interfaces/IWidget;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v2

    .line 662
    .local v2, "userId":Ljava/lang/String;
    :try_start_0
    invoke-interface {v1, v2, p1}, Lcom/cigna/coach/interfaces/IWidget;->getWidgetInfo(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 675
    :goto_0
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageText()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 676
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "widgetInfo message is null "

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    :cond_0
    return-object v3

    .line 663
    :catch_0
    move-exception v0

    .line 664
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto :goto_0
.end method

.method public isCurrentUserMission(I)Z
    .locals 2
    .param p1, "missionId"    # I

    .prologue
    .line 650
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createUserMissionDetailData(I)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v0

    .line 651
    .local v0, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    if-eqz v0, :cond_0

    .line 652
    const/4 v1, 0x1

    .line 654
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFirstTimeNoScore()Z
    .locals 8

    .prologue
    .line 633
    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v4

    .line 634
    .local v4, "scores":Lcom/cigna/coach/apiobjects/Scores;
    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v0

    .line 636
    .local v0, "categoryInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 638
    .local v1, "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 639
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 640
    .local v3, "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 641
    .local v2, "i":Ljava/lang/Integer;
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isFirstTimeNoScore "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ltz v5, :cond_0

    .line 643
    const/4 v5, 0x0

    .line 646
    .end local v2    # "i":Ljava/lang/Integer;
    .end local v3    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public isTimeToReassessment()Z
    .locals 8

    .prologue
    .line 329
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 330
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v3

    .line 331
    .local v3, "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->DAILY:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    invoke-interface {v3, v5, v6}, Lcom/cigna/coach/interfaces/ILifeStyle;->getScoresHistory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 332
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/apiobjects/ScoreInfo;

    .line 333
    .local v4, "scoreInof":Lcom/cigna/coach/apiobjects/ScoreInfo;
    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/ScoreInfo;->isTimeToReassess()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 337
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    .end local v3    # "il":Lcom/cigna/coach/interfaces/ILifeStyle;
    .end local v4    # "scoreInof":Lcom/cigna/coach/apiobjects/ScoreInfo;
    :goto_0
    return v5

    .line 334
    :catch_0
    move-exception v2

    .line 335
    .local v2, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception in isTimeToReassessment, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public sendAnswerData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 18
    .param p1, "assessmentData"    # Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 224
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v15

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v8

    .line 225
    .local v8, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 229
    .local v14, "questionsAnswerGroupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQADataCount()I

    move-result v10

    .line 230
    .local v10, "qaCount":I
    const/4 v5, -0x1

    .line 231
    .local v5, "curQuestionGroupId":I
    const/4 v9, -0x1

    .line 232
    .local v9, "preQuestionGroupId":I
    const/4 v13, 0x0

    .line 233
    .local v13, "questionsAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    const/4 v12, 0x0

    .line 234
    .local v12, "questionAnswerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v10, :cond_5

    .line 235
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQAData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    move-result-object v3

    .line 236
    .local v3, "assessmentQAData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionGroupID()I

    move-result v5

    .line 238
    const/4 v15, -0x1

    if-eq v5, v15, :cond_0

    if-eq v5, v9, :cond_1

    .line 240
    :cond_0
    new-instance v13, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    .end local v13    # "questionsAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    invoke-direct {v13}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;-><init>()V

    .line 241
    .restart local v13    # "questionsAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    new-instance v12, Ljava/util/ArrayList;

    .end local v12    # "questionAnswerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 244
    .restart local v12    # "questionAnswerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    :cond_1
    new-instance v11, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    invoke-direct {v11}, Lcom/cigna/coach/apiobjects/QuestionAnswer;-><init>()V

    .line 247
    .local v11, "questionAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    new-instance v1, Lcom/cigna/coach/apiobjects/Answer;

    invoke-direct {v1}, Lcom/cigna/coach/apiobjects/Answer;-><init>()V

    .line 248
    .local v1, "answer":Lcom/cigna/coach/apiobjects/Answer;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v15

    invoke-virtual {v1, v15}, Lcom/cigna/coach/apiobjects/Answer;->setAnswerType(Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;)V

    .line 250
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnsweredValue()Ljava/lang/String;

    move-result-object v2

    .line 253
    .local v2, "answerValue":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v15

    sget-object v16, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 254
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getAnsweredID()I

    move-result v15

    invoke-virtual {v1, v15}, Lcom/cigna/coach/apiobjects/Answer;->setAnswerId(I)V

    .line 260
    :cond_2
    :goto_1
    invoke-virtual {v11, v1}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->setAnswer(Lcom/cigna/coach/apiobjects/Answer;)V

    .line 261
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionID()I

    move-result v15

    invoke-virtual {v11, v15}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->setQuestionId(I)V

    .line 263
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    if-ne v5, v9, :cond_4

    .line 234
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 255
    :cond_3
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v15

    sget-object v16, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 256
    invoke-virtual {v1, v2}, Lcom/cigna/coach/apiobjects/Answer;->setAnswer(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 299
    .end local v1    # "answer":Lcom/cigna/coach/apiobjects/Answer;
    .end local v2    # "answerValue":Ljava/lang/String;
    .end local v3    # "assessmentQAData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;
    .end local v5    # "curQuestionGroupId":I
    .end local v7    # "i":I
    .end local v9    # "preQuestionGroupId":I
    .end local v10    # "qaCount":I
    .end local v11    # "questionAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v12    # "questionAnswerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    .end local v13    # "questionsAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :catch_0
    move-exception v6

    .line 300
    .local v6, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v6}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 303
    const/4 v15, 0x0

    .end local v6    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :goto_3
    return-object v15

    .line 272
    .restart local v1    # "answer":Lcom/cigna/coach/apiobjects/Answer;
    .restart local v2    # "answerValue":Ljava/lang/String;
    .restart local v3    # "assessmentQAData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;
    .restart local v5    # "curQuestionGroupId":I
    .restart local v7    # "i":I
    .restart local v9    # "preQuestionGroupId":I
    .restart local v10    # "qaCount":I
    .restart local v11    # "questionAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .restart local v12    # "questionAnswerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    .restart local v13    # "questionsAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :cond_4
    :try_start_1
    invoke-virtual {v13, v12}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->setQuestionAnswer(Ljava/util/List;)V

    .line 273
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;->getQuestionGroupID()I

    move-result v15

    invoke-virtual {v13, v15}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->setQuestionGroupId(I)V

    .line 274
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    move v9, v5

    goto :goto_2

    .line 280
    .end local v1    # "answer":Lcom/cigna/coach/apiobjects/Answer;
    .end local v2    # "answerValue":Ljava/lang/String;
    .end local v3    # "assessmentQAData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;
    .end local v11    # "questionAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    :cond_5
    new-instance v4, Lcom/cigna/coach/apiobjects/CategoryGroupQA;

    invoke-direct {v4}, Lcom/cigna/coach/apiobjects/CategoryGroupQA;-><init>()V

    .line 281
    .local v4, "categoryGroupQA":Lcom/cigna/coach/apiobjects/CategoryGroupQA;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v15

    invoke-virtual {v4, v15, v14}, Lcom/cigna/coach/apiobjects/CategoryGroupQA;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v15

    sget-object v16, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 285
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v15

    const-string v16, "com.sec.android.app.shealth.cignacoach"

    const-string v17, "0020"

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_6
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v8, v15, v4}, Lcom/cigna/coach/interfaces/ILifeStyle;->setAnswersByCategory(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CategoryGroupQA;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v15

    goto :goto_3

    .line 286
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v15

    sget-object v16, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 287
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v15

    const-string v16, "com.sec.android.app.shealth.cignacoach"

    const-string v17, "0021"

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 288
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v15

    sget-object v16, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 289
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v15

    const-string v16, "com.sec.android.app.shealth.cignacoach"

    const-string v17, "0022"

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 290
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v15

    sget-object v16, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 291
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v15

    const-string v16, "com.sec.android.app.shealth.cignacoach"

    const-string v17, "0023"

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 292
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v15

    sget-object v16, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 293
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v15

    const-string v16, "com.sec.android.app.shealth.cignacoach"

    const-string v17, "0024"

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method public setEngagingReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Ljava/lang/String;
    .locals 7
    .param p1, "missionSequenceId"    # I
    .param p2, "missionFailedReason"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .prologue
    .line 386
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 387
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v3

    .line 390
    .local v3, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    :try_start_0
    invoke-interface {v3, p1, p2}, Lcom/cigna/coach/interfaces/IGoalsMissions;->setMissionFailureReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v1

    .line 391
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachMessage;
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "reason type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "reason msg: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "reason desc: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 399
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachMessage;
    :goto_0
    return-object v4

    .line 395
    :catch_0
    move-exception v2

    .line 397
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    .line 399
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public setUserMissionDetail(Lcom/sec/android/app/shealth/cignacoach/data/MissionData;)Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .locals 8
    .param p1, "missionData"    # Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .prologue
    .line 309
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v0

    .line 310
    .local v0, "cf":Lcom/cigna/coach/factory/CoachFactory;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/cigna/coach/factory/CoachFactory;->getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;

    move-result-object v4

    .line 312
    .local v4, "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v6

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getMissionDetailList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-interface {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions;->setUserMissionDetail(Ljava/lang/String;ILjava/util/List;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v1

    .line 313
    .local v1, "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    .end local v0    # "cf":Lcom/cigna/coach/factory/CoachFactory;
    .end local v1    # "cr":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    .end local v4    # "lgm":Lcom/cigna/coach/interfaces/IGoalsMissions;
    :goto_0
    return-object v3

    .line 319
    :catch_0
    move-exception v2

    .line 320
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 321
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception in setUserMissionDetail, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public updateUserProfile(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 600
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isFirstLoadingComplete()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 602
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isAccessHealthServiceDB()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 603
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 608
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v1, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 609
    .local v1, "locale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 611
    .local v0, "contry":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->createAndUpdateUser(Ljava/lang/String;)V

    .line 618
    .end local v0    # "contry":Ljava/lang/String;
    .end local v1    # "locale":Ljava/util/Locale;
    :goto_0
    return-void

    .line 613
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "updateUserProfile() no update profile ~"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 616
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->TAG:Ljava/lang/String;

    const-string v3, "Because cigna don\'t have DB, cigna account no update~~"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
