.class public Lcom/sec/android/app/shealth/service/HandleAppDataService;
.super Landroid/app/IntentService;
.source "HandleAppDataService.java"


# static fields
.field public static final INSERT_TO_DATABASE:Ljava/lang/String; = "insert_to_database"

.field public static final PACKAGE_NAME_TO_REMOVE:Ljava/lang/String; = "package_name_to_remove"

.field public static final PLUGIN_ID_TO_REMOVE:Ljava/lang/String; = "plugin_id_to_remove"

.field private static final TAG:Ljava/lang/String; = "HandleAppDataService"

.field private static final THIRDPARTY_APP_TYPE:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "HandleAppDataService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method private insertAppDataInDB(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 43
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    const/4 v4, 0x0

    .line 94
    .local v4, "plugInNames":Ljava/lang/String;
    const/16 v16, 0x0

    .line 95
    .local v16, "plugInIcons":Ljava/lang/String;
    const/4 v14, 0x1

    .line 96
    .local v14, "isFavorite":I
    const-string/jumbo v2, "package_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, "trim":Ljava/lang/String;
    const-string v2, "app_type"

    const/4 v5, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 98
    .local v18, "appType":I
    const-string v2, "application_category"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 99
    .local v7, "appCategory":Ljava/lang/String;
    const-string v2, "app_deletable_db"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 100
    .local v38, "hasDeletableDb":Ljava/lang/String;
    const-string v2, "app_delete_db_info"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 101
    .local v10, "deleteDbInfo":Ljava/lang/String;
    const-string/jumbo v2, "support_accessory"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 102
    .local v36, "accessorySupport":Ljava/lang/String;
    const-string/jumbo v2, "spp_enabled"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 103
    .local v40, "isPushEnabled":Ljava/lang/String;
    const-string v2, "actions"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 104
    .local v17, "action":Ljava/lang/String;
    const-string v2, "icon_names"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 105
    const-string v2, "app_names"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 109
    if-nez v17, :cond_0

    .line 111
    const-string v17, "com.sec.shealth.action.STEALTH_MODE"

    .line 116
    :cond_0
    if-nez v16, :cond_1

    .line 118
    const-string v16, "ic_launcher"

    .line 121
    :cond_1
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->getVersionNameForPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 123
    .local v6, "packageVersion":Ljava/lang/String;
    const-string v2, "health_measure_type"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    .line 132
    .local v11, "measureType":J
    if-eqz v7, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/app/shealth/service/HandleAppDataService;->isNewCategory(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 134
    :cond_2
    const-string v7, "Other"

    .line 137
    :cond_3
    const/4 v2, 0x2

    move/from16 v0, v18

    if-ne v0, v2, :cond_4

    .line 139
    const-string v7, "Fitness"

    .line 142
    :cond_4
    if-nez v38, :cond_5

    .line 144
    const-string/jumbo v38, "no"

    .line 147
    :cond_5
    if-nez v10, :cond_6

    .line 149
    const-string v10, ""

    .line 152
    :cond_6
    if-nez v36, :cond_7

    .line 154
    const-string/jumbo v36, "no"

    .line 157
    :cond_7
    if-nez v40, :cond_8

    .line 159
    const-string/jumbo v40, "no"

    .line 162
    :cond_8
    const-string v2, "InstalledReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "onReceive called APK info : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " App name : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "application_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryData(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v42

    .line 164
    .local v42, "temp":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    const/16 v31, 0x1

    .line 165
    .local v31, "tempFavStatus":I
    if-eqz v42, :cond_a

    .line 167
    invoke-interface/range {v42 .. v42}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_f

    .line 169
    const-string v2, "HandleAppDataService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "displayAppName[i] - "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    const-string/jumbo v5, "true"

    const-string/jumbo v8, "yes"

    move-object/from16 v0, v36

    invoke-virtual {v0, v8}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_b

    const/4 v8, 0x1

    :goto_0
    const-string/jumbo v9, "yes"

    move-object/from16 v0, v38

    invoke-virtual {v0, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_c

    const/4 v9, 0x1

    :goto_1
    const-string/jumbo v13, "yes"

    move-object/from16 v0, v40

    invoke-virtual {v0, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_d

    const/4 v13, 0x1

    :goto_2
    const/4 v15, 0x1

    invoke-direct/range {v2 .. v18}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZIILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->addAppRegistryData(Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_e

    .line 172
    const-string v2, "HandleAppDataService"

    const-string/jumbo v5, "not able to register plugin with s-health"

    invoke-static {v2, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_9
    :goto_3
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->logPrefFile()V

    .line 214
    :cond_a
    return-void

    .line 170
    :cond_b
    const/4 v8, 0x0

    goto :goto_0

    :cond_c
    const/4 v9, 0x0

    goto :goto_1

    :cond_d
    const/4 v13, 0x0

    goto :goto_2

    .line 177
    :cond_e
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->isInitialised()Z

    move-result v2

    if-eqz v2, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 179
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v37

    .line 180
    .local v37, "count":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move/from16 v0, v37

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setOrder(Ljava/lang/String;I)V

    .line 181
    add-int/lit8 v2, v37, 0x1

    invoke-static {v2}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    goto :goto_3

    .line 188
    .end local v37    # "count":I
    :cond_f
    const/16 v41, 0x0

    .local v41, "j":I
    :goto_4
    invoke-interface/range {v42 .. v42}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v41

    if-ge v0, v2, :cond_10

    .line 191
    const-string v2, "InstalledReceiver"

    const-string v5, " Deleting old data"

    invoke-static {v2, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    move-object/from16 v0, v42

    move/from16 v1, v41

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget v0, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    move/from16 v31, v0

    .line 194
    add-int/lit8 v39, v41, 0x1

    .line 195
    .local v39, "id":I
    move/from16 v0, v39

    move-object/from16 v1, p1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->deleteAppRegistryData(Ljava/lang/String;ILandroid/content/Context;)I

    .line 188
    add-int/lit8 v41, v41, 0x1

    goto :goto_4

    .line 199
    .end local v39    # "id":I
    :cond_10
    new-instance v19, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    const-string/jumbo v22, "true"

    const-string/jumbo v2, "yes"

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_11

    const/16 v25, 0x1

    :goto_5
    const-string/jumbo v2, "yes"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_12

    const/16 v26, 0x1

    :goto_6
    const-string/jumbo v2, "yes"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_13

    const/16 v30, 0x1

    :goto_7
    const/16 v32, 0x1

    move-object/from16 v20, v3

    move-object/from16 v21, v4

    move-object/from16 v23, v6

    move-object/from16 v24, v7

    move-object/from16 v27, v10

    move-wide/from16 v28, v11

    move-object/from16 v33, v16

    move-object/from16 v34, v17

    move/from16 v35, v18

    invoke-direct/range {v19 .. v35}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZIILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->addAppRegistryData(Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_14

    .line 201
    const-string v2, "HandleAppDataService"

    const-string/jumbo v5, "not able to register plugin with s-health"

    invoke-static {v2, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 199
    :cond_11
    const/16 v25, 0x0

    goto :goto_5

    :cond_12
    const/16 v26, 0x0

    goto :goto_6

    :cond_13
    const/16 v30, 0x0

    goto :goto_7

    .line 205
    :cond_14
    const-string v2, "InstalledReceiver"

    const-string v5, " Adding again..."

    invoke-static {v2, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method private isNewCategory(Ljava/lang/String;)Z
    .locals 5
    .param p1, "categoryName"    # Ljava/lang/String;

    .prologue
    .line 227
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->values()[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 229
    .local v3, "menuCategory":Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;->getMenuCategoryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    .line 231
    const/4 v4, 0x0

    .line 235
    .end local v3    # "menuCategory":Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
    :goto_1
    return v4

    .line 227
    .restart local v3    # "menuCategory":Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 235
    .end local v3    # "menuCategory":Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuCategories$MenuCategory;
    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    if-nez p1, :cond_1

    .line 57
    const-string v4, "HandleAppDataService"

    const-string v5, "Intent is null"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const-string/jumbo v4, "package_name_to_remove"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 63
    const-string/jumbo v4, "package_name_to_remove"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "packageName":Ljava/lang/String;
    const-string/jumbo v4, "plugin_id_to_remove"

    const/4 v5, 0x1

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 66
    .local v1, "pluginId":I
    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->deleteAppRegistryData(Ljava/lang/String;ILandroid/content/Context;)I

    .line 68
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->isInitialised()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 69
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 71
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getOrder(Ljava/lang/String;)I

    move-result v2

    .line 72
    .local v2, "pos":I
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->getTotalFavoriteCount()I

    move-result v3

    .line 73
    .local v3, "total":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->remove(Ljava/lang/String;)V

    .line 74
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->updateOrderOnDelete(I)V

    .line 75
    add-int/lit8 v4, v3, -0x1

    invoke-static {v4}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->setTotalFavoriteCount(I)V

    .line 76
    const-string v4, "EditFavorites"

    const-string v5, "Logging pref file after Package removed.."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->logPrefFile()V

    goto :goto_0

    .line 83
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "pluginId":I
    .end local v2    # "pos":I
    .end local v3    # "total":I
    :cond_2
    const-string v4, "insert_to_database"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    invoke-direct {p0, p0, p1}, Lcom/sec/android/app/shealth/service/HandleAppDataService;->insertAppDataInDB(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
