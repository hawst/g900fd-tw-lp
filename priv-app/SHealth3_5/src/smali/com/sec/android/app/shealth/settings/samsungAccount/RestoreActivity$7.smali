.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;
.super Ljava/lang/Object;
.source "RestoreActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->startBackup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(II)V
    .locals 9
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    const-wide/16 v7, 0x64

    const v6, 0x7f090d15

    const/16 v5, 0x31

    const/4 v4, 0x1

    .line 495
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Shealth Backup Completed. Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const/4 v0, 0x0

    .line 498
    .local v0, "cignaStatus":Z
    if-nez p2, :cond_3

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[SUCCESS] Shealth Backup Completed."

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCignaBackupRestoreEnable:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const-string v2, "BACKUP"

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->cignaBackupRestore(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)Z

    move-result v0

    .line 560
    :goto_0
    return-void

    .line 504
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 505
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 507
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 508
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 511
    :cond_2
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;)V

    invoke-virtual {v1, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 522
    :cond_3
    const/16 v1, 0x11

    if-ne p2, v1, :cond_6

    .line 524
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[SUCCESS] Shealth Backup Completed. but exercise failed"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 529
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    :cond_5
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;)V

    invoke-virtual {v1, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 539
    :cond_6
    const/16 v1, 0xb

    if-ne p2, v1, :cond_7

    .line 541
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[FAILURE] error related to samsung account"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 543
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 546
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "3.0 Backup error received : else case"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 549
    const/16 v1, 0x10

    if-ne p2, v1, :cond_8

    .line 550
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 551
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    .line 556
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v2, 0x7f0808b1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 557
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    goto/16 :goto_0

    .line 553
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 554
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto :goto_1
.end method

.method public onProgress(II)V
    .locals 5
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 482
    const-wide v1, 0x3feccccccccccccdL    # 0.9

    div-int/lit8 v3, p2, 0x2

    int-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-int v0, v1

    .line 483
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 484
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 487
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 488
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 491
    :cond_1
    return-void
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 6
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0xc8

    const/4 v3, 0x1

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Backup Started. Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    if-nez p2, :cond_0

    .line 477
    :goto_0
    return-void

    .line 439
    :cond_0
    const/16 v0, 0x10

    if-ne p2, v0, :cond_1

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 441
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 471
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v1, 0x7f0808b1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    goto :goto_0

    .line 448
    :cond_1
    const/16 v0, 0x12

    if-ne p2, v0, :cond_2

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isServerUnderMaintenance:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$902(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 451
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 459
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 460
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
