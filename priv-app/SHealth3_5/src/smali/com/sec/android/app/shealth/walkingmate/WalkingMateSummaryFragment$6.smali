.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCurrentStepsStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 1013
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 1017
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSteps:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v1

    const v4, 0x1869f

    if-le v1, v4, :cond_2

    const/4 v1, 0x1

    :goto_0
    # setter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z
    invoke-static {v3, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1502(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1018
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSteps:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v1

    div-int/lit16 v0, v1, 0x3e8

    .line 1019
    .local v0, "steps":I
    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    .line 1020
    const/16 v0, 0x270f

    .line 1023
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1025
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-ne v1, v5, :cond_1

    .line 1026
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1027
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1033
    .end local v0    # "steps":I
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setPercentValue()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    .line 1034
    return-void

    :cond_2
    move v1, v2

    .line 1017
    goto :goto_0

    .line 1030
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSteps:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1031
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
