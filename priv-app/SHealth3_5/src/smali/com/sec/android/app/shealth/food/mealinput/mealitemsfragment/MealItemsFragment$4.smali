.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;
.super Ljava/lang/Object;
.source "MealItemsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->addMealItemViewToContainerByMealItemData(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

.field final synthetic val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

.field final synthetic val$mealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iput-object p3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->val$mealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 389
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isAnyDialogShown()Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_DELETE_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 393
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "removing meal item with id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->val$mealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->val$mealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->findMealItemViewPosition(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$600(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)I

    move-result v0

    .line 395
    .local v0, "position":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;->val$mealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getMealItemName()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->showMealDeleteConfirmDialog(ILjava/lang/String;)V
    invoke-static {v1, v0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$700(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;ILjava/lang/String;)V

    goto :goto_0
.end method
