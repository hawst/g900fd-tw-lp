.class Lcom/sec/android/app/shealth/SHealthApplication$2;
.super Ljava/lang/Object;
.source "SHealthApplication.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/SHealthApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/SHealthApplication;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/SHealthApplication;)V
    .locals 0

    .prologue
    .line 1304
    iput-object p1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1308
    const-string v3, "Device Type Shared Preferences"

    const-string v4, "Device Type Shared Preferences - in Onservice connected"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1310
    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/SHealthApplication;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Binding is done - Service connected"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1312
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDeviceTypeChecked(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1313
    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/SHealthApplication;->access$700()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "mSensorConnectionListener : Already setDeviceTypeChecked : true"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$800(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1316
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$800(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V

    .line 1317
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # setter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/SHealthApplication;->access$802(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 1344
    :cond_0
    :goto_0
    return-void

    .line 1325
    :cond_1
    const/4 v0, 0x0

    .line 1327
    .local v0, "isNeedToSetDeviceChecked":Z
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setPedometerAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$900(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v0

    .line 1328
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setAntAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1000(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    move v0, v1

    .line 1329
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setHRAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1100(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    move v0, v1

    .line 1330
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setUVAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1200(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    move v0, v1

    .line 1331
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setSpO2Available()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1300(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    move v0, v1

    .line 1332
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->setTHGAvailable()Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1400(Lcom/sec/android/app/shealth/SHealthApplication;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    move v0, v1

    .line 1334
    :goto_5
    const-string v1, "-----SIC-----"

    const-string v2, "Clearing app registry data and setting again on service connected"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->clearAppRegistryData()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1500(Lcom/sec/android/app/shealth/SHealthApplication;)V

    .line 1336
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->clear()V

    .line 1337
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->loadAppRegistryData()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$1600(Lcom/sec/android/app/shealth/SHealthApplication;)V

    .line 1338
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setDeviceTypeChecked(Landroid/content/Context;Z)V

    .line 1340
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$800(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1341
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$800(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V

    .line 1342
    iget-object v1, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # setter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/SHealthApplication;->access$802(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1328
    goto :goto_1

    :cond_3
    move v0, v2

    .line 1329
    goto :goto_2

    :cond_4
    move v0, v2

    .line 1330
    goto :goto_3

    :cond_5
    move v0, v2

    .line 1331
    goto :goto_4

    :cond_6
    move v0, v2

    .line 1332
    goto :goto_5
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1349
    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/SHealthApplication;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1350
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SHealthApplication;->access$800(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1351
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v0}, Lcom/sec/android/app/shealth/SHealthApplication;->access$800(Lcom/sec/android/app/shealth/SHealthApplication;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->close()V

    .line 1352
    iget-object v0, p0, Lcom/sec/android/app/shealth/SHealthApplication$2;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/SHealthApplication;->mDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$802(Lcom/sec/android/app/shealth/SHealthApplication;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .line 1354
    :cond_0
    return-void
.end method
