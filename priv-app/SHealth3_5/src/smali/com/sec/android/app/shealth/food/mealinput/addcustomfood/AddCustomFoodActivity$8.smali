.class Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;
.super Ljava/lang/Object;
.source "AddCustomFoodActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V
    .locals 0

    .prologue
    .line 667
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->mCaloriePerServingEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->processEmptyField()V

    .line 692
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isInvalidState()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->isFoodDataChanged()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$1200(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 685
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->onSaveButtonClick()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->access$1300(Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;)V

    goto :goto_0

    .line 689
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity$8;->this$0:Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/addcustomfood/AddCustomFoodActivity;->finish()V

    goto :goto_0
.end method
