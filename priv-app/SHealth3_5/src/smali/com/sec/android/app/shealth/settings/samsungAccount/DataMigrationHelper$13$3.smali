.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$3;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;)V
    .locals 0

    .prologue
    .line 820
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$3;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 824
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$3;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$3;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$402(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$3;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$3;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mAppID:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MI05"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->cancelMigration()V

    .line 828
    return-void
.end method
