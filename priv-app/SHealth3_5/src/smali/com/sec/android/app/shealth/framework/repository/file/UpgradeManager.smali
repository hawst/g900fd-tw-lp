.class public Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;
.super Ljava/lang/Object;
.source "UpgradeManager.java"


# static fields
.field public static final APPLICATION_CACHE_ABSOLUTE_PATH:Ljava/lang/String;

.field public static final BACKGROUND_IMAGE_PORTRAIT:Ljava/lang/String; = "background_portrait_image.jpg"

.field public static final CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

.field public static final CACHED_IMAGES_PATH:Ljava/lang/String; = "SHealth2/cache/img"

.field public static final CACHE_PATH:Ljava/lang/String; = "SHealth2/cache"

.field public static final DB_FOLDER_PATH:Ljava/lang/String; = "/data/data/com.sec.android.app.shealth/databases/"

.field public static final IMAGE_FOLDER_NAME:Ljava/lang/String; = "img"

.field public static final LANDSCAPE_IMAGE_PORTRAIT:Ljava/lang/String; = "landscape_portrait_image.jpg"

.field public static final ONE_STEP_SIZE:J = 0x1000L

.field public static final PROFILE_IMAGE:Ljava/lang/String; = "user_photo.jpg"

.field public static final SHEALTH2_PATH:Ljava/lang/String; = "SHealth2"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    .line 33
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->APPLICATION_CACHE_ABSOLUTE_PATH:Ljava/lang/String;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->APPLICATION_CACHE_ABSOLUTE_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SHealth2/cache/img"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->mContext:Landroid/content/Context;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public static doFileCopy(Ljava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p0, "fromPath"    # Ljava/lang/String;
    .param p1, "toPath"    # Ljava/lang/String;

    .prologue
    .line 130
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " doFileCopy From  "

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v19, " to "

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 133
    .local v8, "fileFrom":Ljava/io/File;
    const/4 v1, 0x0

    .line 134
    .local v1, "srcChannel":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .line 135
    .local v6, "dstChannel":Ljava/nio/channels/FileChannel;
    const/4 v9, 0x0

    .line 136
    .local v9, "fileInputStream":Ljava/io/FileInputStream;
    const/4 v11, 0x0

    .line 141
    .local v11, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 142
    .local v13, "fileTo":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 144
    new-instance v14, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_old"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v14, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 145
    .local v14, "fileToOld":Ljava/io/File;
    invoke-virtual {v13, v14}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 156
    .end local v14    # "fileToOld":Ljava/io/File;
    :cond_0
    new-instance v13, Ljava/io/File;

    .end local v13    # "fileTo":Ljava/io/File;
    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    .restart local v13    # "fileTo":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 160
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_18
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_16
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .local v10, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_19
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_17
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 162
    .end local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v12, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v10}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 163
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 164
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v15

    .line 165
    .local v15, "sizeFull":J
    const-wide/16 v2, 0x0

    .line 166
    .local v2, "sizeCurrent":J
    :goto_0
    cmp-long v4, v2, v15

    if-gez v4, :cond_f

    .line 168
    const-wide/16 v4, 0x1000

    add-long/2addr v4, v2

    cmp-long v4, v4, v15

    if-gez v4, :cond_a

    .line 170
    const-wide/16 v4, 0x1000

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v17

    .line 171
    .local v17, "trans":J
    const-wide/16 v4, -0x1

    cmp-long v4, v17, v4

    if-nez v4, :cond_9

    .line 173
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v19, ": File Copy failed"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 190
    .end local v2    # "sizeCurrent":J
    .end local v15    # "sizeFull":J
    .end local v17    # "trans":J
    :catch_0
    move-exception v7

    move-object v11, v12

    .end local v12    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .line 192
    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v13    # "fileTo":Ljava/io/File;
    .local v7, "e":Ljava/io/FileNotFoundException;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    :goto_1
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 200
    if-eqz v1, :cond_1

    .line 203
    :try_start_4
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b

    .line 209
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_2
    if-eqz v6, :cond_2

    .line 212
    :try_start_5
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_c

    .line 218
    :cond_2
    :goto_3
    if-eqz v9, :cond_3

    .line 221
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_d

    .line 227
    :cond_3
    :goto_4
    if-eqz v11, :cond_4

    .line 230
    :try_start_7
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_e

    .line 237
    :cond_4
    :goto_5
    return-void

    .line 147
    .restart local v13    # "fileTo":Ljava/io/File;
    :cond_5
    :try_start_8
    invoke-virtual {v13}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v13}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 149
    invoke-virtual {v13}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_0

    .line 151
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v5, " Could not create Destinationdirecory "

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_18
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_16
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 200
    if-eqz v1, :cond_6

    .line 203
    :try_start_9
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 209
    :cond_6
    :goto_6
    if-eqz v6, :cond_7

    .line 212
    :try_start_a
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 218
    :cond_7
    :goto_7
    if-eqz v9, :cond_8

    .line 221
    :try_start_b
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    .line 227
    :cond_8
    :goto_8
    if-eqz v11, :cond_4

    .line 230
    :try_start_c
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1

    goto :goto_5

    .line 232
    :catch_1
    move-exception v7

    .line 234
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 205
    .end local v7    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 207
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 214
    .end local v7    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    .line 216
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 223
    .end local v7    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v7

    .line 225
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 175
    .end local v7    # "e":Ljava/io/IOException;
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "sizeCurrent":J
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v12    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v15    # "sizeFull":J
    .restart local v17    # "trans":J
    :cond_9
    const-wide/16 v4, 0x1000

    add-long/2addr v2, v4

    .line 176
    goto/16 :goto_0

    .line 179
    .end local v17    # "trans":J
    :cond_a
    sub-long v4, v15, v2

    :try_start_d
    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v17

    .line 180
    .restart local v17    # "trans":J
    const-wide/16 v4, -0x1

    cmp-long v4, v17, v4

    if-nez v4, :cond_e

    .line 182
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v19, ": File Copy failed"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 194
    .end local v2    # "sizeCurrent":J
    .end local v15    # "sizeFull":J
    .end local v17    # "trans":J
    :catch_5
    move-exception v7

    move-object v11, v12

    .end local v12    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .line 196
    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v13    # "fileTo":Ljava/io/File;
    .restart local v7    # "e":Ljava/io/IOException;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    :goto_9
    :try_start_e
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 200
    if-eqz v1, :cond_b

    .line 203
    :try_start_f
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_f

    .line 209
    :cond_b
    :goto_a
    if-eqz v6, :cond_c

    .line 212
    :try_start_10
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_10

    .line 218
    :cond_c
    :goto_b
    if-eqz v9, :cond_d

    .line 221
    :try_start_11
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_11

    .line 227
    :cond_d
    :goto_c
    if-eqz v11, :cond_4

    .line 230
    :try_start_12
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_6

    goto/16 :goto_5

    .line 232
    :catch_6
    move-exception v7

    .line 234
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 184
    .end local v7    # "e":Ljava/io/IOException;
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "sizeCurrent":J
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v12    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v13    # "fileTo":Ljava/io/File;
    .restart local v15    # "sizeFull":J
    .restart local v17    # "trans":J
    :cond_e
    move-wide v2, v15

    .line 185
    goto/16 :goto_0

    .end local v17    # "trans":J
    :cond_f
    move-object v11, v12

    .end local v12    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .line 200
    .end local v2    # "sizeCurrent":J
    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v15    # "sizeFull":J
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_10
    if-eqz v1, :cond_11

    .line 203
    :try_start_13
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_8

    .line 209
    :cond_11
    :goto_d
    if-eqz v6, :cond_12

    .line 212
    :try_start_14
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_9

    .line 218
    :cond_12
    :goto_e
    if-eqz v9, :cond_13

    .line 221
    :try_start_15
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_a

    .line 227
    :cond_13
    :goto_f
    if-eqz v11, :cond_4

    .line 230
    :try_start_16
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_7

    goto/16 :goto_5

    .line 232
    :catch_7
    move-exception v7

    .line 234
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 205
    .end local v7    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v7

    .line 207
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 214
    .end local v7    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v7

    .line 216
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 223
    .end local v7    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v7

    .line 225
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 205
    .end local v13    # "fileTo":Ljava/io/File;
    .local v7, "e":Ljava/io/FileNotFoundException;
    :catch_b
    move-exception v7

    .line 207
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 214
    .end local v7    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v7

    .line 216
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 223
    .end local v7    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v7

    .line 225
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 232
    .end local v7    # "e":Ljava/io/IOException;
    :catch_e
    move-exception v7

    .line 234
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 205
    :catch_f
    move-exception v7

    .line 207
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 214
    :catch_10
    move-exception v7

    .line 216
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 223
    :catch_11
    move-exception v7

    .line 225
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 200
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_10
    if-eqz v1, :cond_14

    .line 203
    :try_start_17
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_12

    .line 209
    :cond_14
    :goto_11
    if-eqz v6, :cond_15

    .line 212
    :try_start_18
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_13

    .line 218
    :cond_15
    :goto_12
    if-eqz v9, :cond_16

    .line 221
    :try_start_19
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_14

    .line 227
    :cond_16
    :goto_13
    if-eqz v11, :cond_17

    .line 230
    :try_start_1a
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_15

    .line 235
    :cond_17
    :goto_14
    throw v4

    .line 205
    :catch_12
    move-exception v7

    .line 207
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_11

    .line 214
    .end local v7    # "e":Ljava/io/IOException;
    :catch_13
    move-exception v7

    .line 216
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_12

    .line 223
    .end local v7    # "e":Ljava/io/IOException;
    :catch_14
    move-exception v7

    .line 225
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_13

    .line 232
    .end local v7    # "e":Ljava/io/IOException;
    :catch_15
    move-exception v7

    .line 234
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_14

    .line 200
    .end local v7    # "e":Ljava/io/IOException;
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v13    # "fileTo":Ljava/io/File;
    :catchall_1
    move-exception v4

    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_10

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v12    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v4

    move-object v11, v12

    .end local v12    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutputStream":Ljava/io/FileOutputStream;
    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_10

    .line 194
    .end local v13    # "fileTo":Ljava/io/File;
    :catch_16
    move-exception v7

    goto/16 :goto_9

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v13    # "fileTo":Ljava/io/File;
    :catch_17
    move-exception v7

    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .line 190
    .end local v13    # "fileTo":Ljava/io/File;
    :catch_18
    move-exception v7

    goto/16 :goto_1

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v13    # "fileTo":Ljava/io/File;
    :catch_19
    move-exception v7

    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method private getImageList(Ljava/io/File;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "fileDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p2, "imgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 116
    .local v4, "listFiles":[Ljava/io/File;
    if-eqz v4, :cond_1

    .line 118
    move-object v0, v4

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 120
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 121
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 123
    :cond_0
    invoke-direct {p0, v1, p2}, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->getImageList(Ljava/io/File;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 126
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    return-void
.end method

.method private upgradeFrom1to3()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method private upgradeFrom3to4()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method private upgradeFrom4to5()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method private upgradeFrom5to6()V
    .locals 11

    .prologue
    .line 77
    new-instance v2, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->APPLICATION_CACHE_ABSOLUTE_PATH:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "SHealth2/cache"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .local v2, "fileDir":Ljava/io/File;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v6, "imgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v2, v6}, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->getImageList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 80
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 82
    .local v3, "filePath":Ljava/lang/String;
    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 83
    .local v5, "imageName":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " imageName "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v0, 0x0

    .line 86
    .local v0, "destFilePath":Ljava/lang/String;
    new-instance v7, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;

    iget-object v8, p0, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;-><init>(Landroid/content/Context;)V

    .line 87
    .local v7, "profile":Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;->isExist()Z

    move-result v8

    if-nez v8, :cond_2

    new-instance v8, Ljava/io/File;

    sget-object v9, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_2

    .line 89
    const-string/jumbo v8, "user_photo.jpg"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    .line 97
    :cond_1
    :goto_1
    if-eqz v0, :cond_0

    .line 99
    sget-object v8, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " destFilePath "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :try_start_0
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->doFileCopy(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v1

    .line 106
    .local v1, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " Exception occured while Copying file "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 93
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v8, "background_portrait_image.jpg"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 94
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "background_portrait_image.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 95
    :cond_3
    const-string v8, "landscape_portrait_image.jpg"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 96
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_FULL_PATH:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "background_landscape_image.jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 111
    .end local v0    # "destFilePath":Ljava/lang/String;
    .end local v3    # "filePath":Ljava/lang/String;
    .end local v5    # "imageName":Ljava/lang/String;
    .end local v7    # "profile":Lcom/sec/android/app/shealth/framework/repository/database/HShealthProfile;
    :cond_4
    return-void
.end method


# virtual methods
.method public upgrade(II)Z
    .locals 1
    .param p1, "oldVersion"    # I
    .param p2, "newVersion"    # I

    .prologue
    .line 45
    packed-switch p1, :pswitch_data_0

    .line 57
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 48
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->upgradeFrom1to3()V

    .line 50
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->upgradeFrom3to4()V

    .line 53
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->upgradeFrom4to5()V

    .line 55
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/file/UpgradeManager;->upgradeFrom5to6()V

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
