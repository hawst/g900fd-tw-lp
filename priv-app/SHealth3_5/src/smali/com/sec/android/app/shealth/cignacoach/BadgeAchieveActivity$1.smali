.class Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;
.super Ljava/lang/Object;
.source "BadgeAchieveActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->setCustomActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 235
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 236
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 238
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->prepareShareView()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)V

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.cignacoach"

    const-string v4, "0030"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 244
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :catch_0
    move-exception v1

    .line 245
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
