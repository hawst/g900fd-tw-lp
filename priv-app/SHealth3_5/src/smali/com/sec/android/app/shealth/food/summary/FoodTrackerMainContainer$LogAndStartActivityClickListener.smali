.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;
.source "FoodTrackerMainContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LogAndStartActivityClickListener"
.end annotation


# instance fields
.field private mIntent:Landroid/content/Intent;

.field private mLogDetail:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field private mLogGeneral:Lcom/sec/android/app/shealth/food/app/UserActionLog;

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Landroid/content/Intent;Lcom/sec/android/app/shealth/food/app/UserActionLog;Lcom/sec/android/app/shealth/food/app/UserActionLog;)V
    .locals 0
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "logGeneral"    # Lcom/sec/android/app/shealth/food/app/UserActionLog;
    .param p4, "logDetail"    # Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .prologue
    .line 725
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;-><init>()V

    .line 727
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->mIntent:Landroid/content/Intent;

    .line 729
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->mLogGeneral:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 731
    iput-object p4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->mLogDetail:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    .line 733
    return-void
.end method


# virtual methods
.method public onClickAction(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 741
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->mLogGeneral:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 743
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->access$200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->mLogDetail:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 747
    return-void
.end method
