.class public final enum Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;
.super Ljava/lang/Enum;
.source "ShareUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SnsType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

.field public static final enum FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

.field public static final enum Twitter:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    const-string v1, "FaceBook"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    const-string v1, "Twitter"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->Twitter:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->Twitter:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->$VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->$VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    return-object v0
.end method
