.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;
.super Ljava/lang/Object;
.source "WalkingMateGraphFragmentSIC.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)V
    .locals 0

    .prologue
    .line 728
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGetBitmapIndexStringXY(I[Ljava/lang/String;[F)[I
    .locals 3
    .param p1, "seriesIdx"    # I
    .param p2, "xarr"    # [Ljava/lang/String;
    .param p3, "yarr"    # [F

    .prologue
    .line 775
    array-length v2, p2

    if-lez v2, :cond_0

    .line 776
    array-length v2, p2

    new-array v1, v2, [I

    .line 777
    .local v1, "ret":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 778
    const/4 v2, -0x1

    aput v2, v1, v0

    .line 777
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 782
    .end local v0    # "i":I
    .end local v1    # "ret":[I
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method public OnGetBitmapIndexTimeXY(I[J[F)[I
    .locals 10
    .param p1, "seriesIdx"    # I
    .param p2, "xarr"    # [J
    .param p3, "yarr"    # [F

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 733
    const/4 v1, 0x0

    .line 735
    .local v1, "index":I
    array-length v3, p2

    if-lez v3, :cond_8

    .line 737
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 739
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v3

    aget-wide v5, p2, v9

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    .line 744
    :cond_0
    array-length v3, p2

    new-array v2, v3, [I

    .line 745
    .local v2, "ret":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p2

    if-ge v0, v3, :cond_1

    .line 747
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lt v1, v3, :cond_3

    .line 770
    .end local v0    # "i":I
    .end local v2    # "ret":[I
    :cond_1
    :goto_2
    return-object v2

    .line 737
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 750
    .restart local v0    # "i":I
    .restart local v2    # "ret":[I
    :cond_3
    aget-wide v4, p2, v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    .line 751
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "differnt - chart data : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-wide v4, p2, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "memory data : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->getStartTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 755
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->isGoalAchived()Z

    move-result v3

    if-ne v3, v8, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->isMaxSteps()Z

    move-result v3

    if-ne v3, v8, :cond_5

    .line 756
    const/4 v3, 0x2

    aput v3, v2, v0

    .line 745
    :goto_3
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 759
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->isMaxSteps()Z

    move-result v3

    if-ne v3, v8, :cond_6

    .line 760
    aput v8, v2, v0

    goto :goto_3

    .line 761
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->mDayDataList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->isGoalAchived()Z

    move-result v3

    if-ne v3, v8, :cond_7

    .line 762
    aput v9, v2, v0

    goto :goto_3

    .line 764
    :cond_7
    const/4 v3, -0x1

    aput v3, v2, v0

    goto :goto_3

    .line 770
    .end local v0    # "i":I
    .end local v2    # "ret":[I
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_2
.end method
