.class Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;
.super Ljava/lang/Object;
.source "UpdateCheckActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/UpdateCheckActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateServiceDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/UpdateCheckActivity;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/UpdateCheckActivity;Lcom/sec/android/app/shealth/UpdateCheckActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/UpdateCheckActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/UpdateCheckActivity$1;

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;-><init>(Lcom/sec/android/app/shealth/UpdateCheckActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 7
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const v6, 0x7f090237

    const/4 v5, 0x0

    .line 125
    sget-object v2, Lcom/sec/android/app/shealth/UpdateCheckActivity$3;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 129
    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.samsungapps"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 130
    .local v0, "appinfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_1

    iget-boolean v2, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    # invokes: Lcom/sec/android/app/shealth/UpdateCheckActivity;->getIntentForSHealthUpdate()Landroid/content/Intent;
    invoke-static {v3}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->access$200(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    .end local v0    # "appinfo":Landroid/content/pm/ApplicationInfo;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->finish()V

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    # getter for: Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsForceUpdate:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->access$300(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    # invokes: Lcom/sec/android/app/shealth/UpdateCheckActivity;->destoryApp()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->access$400(Lcom/sec/android/app/shealth/UpdateCheckActivity;)V

    goto :goto_0

    .line 136
    .restart local v0    # "appinfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090237

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 138
    .end local v0    # "appinfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 139
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 140
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 149
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    # getter for: Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsFromInitialSettings:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->access$500(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    # getter for: Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsForceUpdate:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->access$300(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsLaterButtonClickedOnAppUpdatePopup(Landroid/content/Context;Z)V

    .line 152
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->finish()V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    # getter for: Lcom/sec/android/app/shealth/UpdateCheckActivity;->mIsForceUpdate:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->access$300(Lcom/sec/android/app/shealth/UpdateCheckActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/shealth/UpdateCheckActivity$UpdateServiceDialogButtonController;->this$0:Lcom/sec/android/app/shealth/UpdateCheckActivity;

    # invokes: Lcom/sec/android/app/shealth/UpdateCheckActivity;->destoryApp()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/UpdateCheckActivity;->access$400(Lcom/sec/android/app/shealth/UpdateCheckActivity;)V

    goto/16 :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
