.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;
.super Ljava/lang/Object;
.source "MealImagesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 110
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "native camera button clicked"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->canAddMoreImages()Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$100(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/app/Activity;)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getThirdPartyURI()Landroid/net/Uri;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$202(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Landroid/net/Uri;)Landroid/net/Uri;

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_PHOTO_BY_CAMERA:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 115
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/view/View;

    aput-object p1, v1, v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mGalleryButton:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 116
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 117
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 118
    const-string/jumbo v1, "return-data"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v1, v0, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 123
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->showMaxImagesToast()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$500(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    goto :goto_0
.end method
