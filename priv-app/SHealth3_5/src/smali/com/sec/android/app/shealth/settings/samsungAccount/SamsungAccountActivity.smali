.class public Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "SamsungAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field public ACCOUNT_TYPE_HEALTH_SERVICE:Ljava/lang/String;

.field private dataUpdateReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;

.field dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

.field private mBackupGoingOn:Z

.field private mRestoreGoingOn:Z

.field private mShealthContentManager:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

.field private mSyncText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 46
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mBackupGoingOn:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mRestoreGoingOn:Z

    .line 48
    const-string v0, "com.samsung.health.auth"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->ACCOUNT_TYPE_HEALTH_SERVICE:Ljava/lang/String;

    .line 158
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mBackupGoingOn:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mBackupGoingOn:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mShealthContentManager:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mRestoreGoingOn:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mRestoreGoingOn:Z

    return p1
.end method


# virtual methods
.method public closeScreen()V
    .locals 3

    .prologue
    .line 231
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->closeScreen()V

    .line 234
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->startActivity(Landroid/content/Intent;)V

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->finish()V

    .line 239
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907b9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 227
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 243
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    .line 244
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dataUpdateReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;

    if-nez v1, :cond_0

    .line 57
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dataUpdateReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;

    .line 58
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.shealth.accountsync.action"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dataUpdateReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 61
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .line 63
    const v1, 0x7f0301f8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->setContentView(I)V

    .line 65
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mShealthContentManager:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 99
    const v1, 0x7f0808bc

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    const v1, 0x7f0808bd

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    const v1, 0x7f0808be

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    const v1, 0x7f0808b7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const v1, 0x7f0808bb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090877

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 211
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dataUpdateReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->dataUpdateReceiver:Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity$DataUpdateReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 216
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.syncservice"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 217
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->stopService(Landroid/content/Intent;)Z

    .line 219
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 194
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 196
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 198
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 200
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->startActivity(Landroid/content/Intent;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->finish()V

    .line 206
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->showSyncStatus()V

    goto :goto_0
.end method

.method public showSyncStatus()V
    .locals 17

    .prologue
    .line 248
    new-instance v5, Landroid/content/IntentFilter;

    const-string v13, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v5, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 249
    .local v5, "filter":Landroid/content/IntentFilter;
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 252
    const/4 v3, 0x0

    .line 253
    .local v3, "email":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 255
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 257
    if-eqz v3, :cond_0

    .line 259
    const v13, 0x7f0808b9

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    :cond_0
    :goto_0
    const v13, 0x7f0808ba

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mSyncText:Landroid/widget/TextView;

    .line 268
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 269
    .local v4, "extras":Landroid/os/Bundle;
    const-string v13, "key"

    const-string v14, "last_backup_time"

    invoke-virtual {v4, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string/jumbo v13, "value"

    const-wide/16 v14, 0x0

    invoke-virtual {v4, v13, v14, v15}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 271
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v15, "CONFIG_OPTION_GET"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v15, v0, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v9

    .line 272
    .local v9, "result":Landroid/os/Bundle;
    const-wide/16 v7, 0x0

    .line 273
    .local v7, "lastBackupTime":J
    if-eqz v9, :cond_1

    .line 274
    const-string/jumbo v13, "value"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 277
    :cond_1
    const-wide/16 v13, 0x0

    cmp-long v13, v7, v13

    if-nez v13, :cond_3

    .line 278
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mSyncText:Landroid/widget/TextView;

    const v14, 0x7f090d41

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    :goto_1
    return-void

    .line 262
    .end local v4    # "extras":Landroid/os/Bundle;
    .end local v7    # "lastBackupTime":J
    .end local v9    # "result":Landroid/os/Bundle;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->finish()V

    goto :goto_0

    .line 280
    .restart local v4    # "extras":Landroid/os/Bundle;
    .restart local v7    # "lastBackupTime":J
    .restart local v9    # "result":Landroid/os/Bundle;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "date_format"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 281
    .local v10, "sysdateformat":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string/jumbo v14, "time_12_24"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 282
    .local v11, "systimeformat":Ljava/lang/String;
    const-string v13, "-"

    const-string v14, "/"

    invoke-virtual {v10, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 283
    .local v2, "date":Ljava/lang/String;
    const/4 v12, 0x0

    .line 284
    .local v12, "time":Ljava/lang/String;
    if-eqz v11, :cond_4

    const-string v13, "12"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 285
    const-string v12, "hh:mm a"

    .line 288
    :goto_2
    new-instance v6, Ljava/text/SimpleDateFormat;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 290
    .local v6, "formatter":Ljava/text/DateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 291
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-virtual {v1, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 292
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->mSyncText:Landroid/widget/TextView;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const v15, 0x7f090d3e

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v15

    invoke-virtual {v6, v15}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 287
    .end local v1    # "calendar":Ljava/util/Calendar;
    .end local v6    # "formatter":Ljava/text/DateFormat;
    :cond_4
    const-string v12, "HH:mm"

    goto :goto_2
.end method
