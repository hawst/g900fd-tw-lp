.class public Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;
.super Landroid/widget/BaseAdapter;
.source "CignaBadgeListAdapter.java"


# instance fields
.field private mBadgeDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p2, "badgeDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mContext:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    .line 26
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->getItem(I)Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    int-to-long v0, p1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 49
    if-nez p2, :cond_0

    .line 50
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f03002c

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 53
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    .line 54
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListAdapter;->mBadgeDataList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;

    .line 56
    .local v5, "listData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    const v6, 0x7f0800f1

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 57
    .local v4, "badgeNameTextView":Landroid/widget/TextView;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getTitleString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    const v6, 0x7f0800f2

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 60
    .local v2, "badgeDescriptionTextView":Landroid/widget/TextView;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getExtraString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    const v6, 0x7f0800f3

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    .local v0, "achieveDateTextView":Landroid/widget/TextView;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getAchieveDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const v6, 0x7f0800f4

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 66
    .local v1, "achieveTimeTextView":Landroid/widget/TextView;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getAchieveTime()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    const v6, 0x7f0800f0

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 68
    .local v3, "badgeIcon":Landroid/widget/ImageView;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->getBadgeImage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaBadgeIconByName(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    .end local v0    # "achieveDateTextView":Landroid/widget/TextView;
    .end local v1    # "achieveTimeTextView":Landroid/widget/TextView;
    .end local v2    # "badgeDescriptionTextView":Landroid/widget/TextView;
    .end local v3    # "badgeIcon":Landroid/widget/ImageView;
    .end local v4    # "badgeNameTextView":Landroid/widget/TextView;
    .end local v5    # "listData":Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
    :cond_1
    return-object p2
.end method
