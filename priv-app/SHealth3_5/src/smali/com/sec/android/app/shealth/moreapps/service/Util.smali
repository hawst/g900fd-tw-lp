.class public Lcom/sec/android/app/shealth/moreapps/service/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field public static final DATE_TYPE_DEFAULT:Ljava/lang/String; = "yyyyMMddHHmmssSSS"

.field public static final DATE_TYPE_YMD:Ljava/lang/String; = "yyyyMMdd"

.field public static PREF_API_JSON_KEY:Ljava/lang/String; = null

.field public static PREF_API_TIME_KEY:Ljava/lang/String; = null

.field public static PREF_CACHE_TIME_KEY:Ljava/lang/String; = null

.field private static final PREF_NAME:Ljava/lang/String; = "pref"

.field public static PREF_SUB_TAB_KEY:Ljava/lang/String;

.field public static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-string v0, "MoreAppsLog"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/Util;->TAG:Ljava/lang/String;

    .line 100
    const-string/jumbo v0, "sub_tab"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_SUB_TAB_KEY:Ljava/lang/String;

    .line 103
    const-string v0, "cache_time"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_CACHE_TIME_KEY:Ljava/lang/String;

    .line 104
    const-string v0, "api_time"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_TIME_KEY:Ljava/lang/String;

    .line 105
    const-string v0, "api_json"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_JSON_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static combineJson(Ljava/util/ArrayList;)Lorg/json/JSONArray;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/moreapps/service/JsonData;",
            ">;>;)",
            "Lorg/json/JSONArray;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 247
    .local p0, "arrayBody":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/moreapps/service/JsonData;>;>;"
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 248
    .local v1, "arrayobj":Lorg/json/JSONArray;
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 249
    .local v0, "adobj":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/moreapps/service/JsonData;>;"
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 250
    .local v5, "obj":Lorg/json/JSONObject;
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    .line 251
    .local v2, "dobj":Lcom/sec/android/app/shealth/moreapps/service/JsonData;
    iget v6, v2, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->bodyType:I

    if-nez v6, :cond_1

    .line 252
    iget-object v6, v2, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->paramName:Ljava/lang/String;

    iget-object v7, v2, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->paramValue:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 254
    :cond_1
    iget v6, v2, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->bodyType:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 255
    iget-object v6, v2, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->paramName:Ljava/lang/String;

    iget-object v7, v2, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->subBody:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/sec/android/app/shealth/moreapps/service/Util;->combineJson(Ljava/util/ArrayList;)Lorg/json/JSONArray;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 258
    .end local v2    # "dobj":Lcom/sec/android/app/shealth/moreapps/service/JsonData;
    :cond_2
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 260
    .end local v0    # "adobj":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/moreapps/service/JsonData;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "obj":Lorg/json/JSONObject;
    :cond_3
    return-object v1
.end method

.method public static convertErrString(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 10
    .param p0, "e"    # Ljava/lang/Exception;

    .prologue
    .line 327
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 329
    .local v5, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 331
    invoke-virtual {p0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v6

    .line 332
    .local v6, "st":[Ljava/lang/StackTraceElement;
    const/4 v2, 0x0

    .line 333
    .local v2, "i":I
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/StackTraceElement;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 334
    .local v1, "element":Ljava/lang/StackTraceElement;
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " ==> "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 335
    add-int/lit8 v2, v2, 0x1

    .line 333
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 338
    .end local v1    # "element":Ljava/lang/StackTraceElement;
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public static convertGregorianCalendarToString(Ljava/util/GregorianCalendar;)Ljava/lang/String;
    .locals 1
    .param p0, "date"    # Ljava/util/GregorianCalendar;

    .prologue
    .line 307
    const-string/jumbo v0, "yyyyMMdd"

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/moreapps/service/Util;->convertGregorianCalendarToString(Ljava/util/GregorianCalendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static convertGregorianCalendarToString(Ljava/util/GregorianCalendar;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # Ljava/util/GregorianCalendar;
    .param p1, "dateFormat"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 313
    const/4 v1, 0x0

    .line 315
    .local v1, "strdate":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 316
    const/4 v2, 0x0

    .line 323
    :goto_0
    return-object v2

    .line 317
    :cond_0
    if-nez p1, :cond_1

    .line 318
    const-string/jumbo p1, "yyyyMMdd"

    .line 321
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 322
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {p0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 323
    goto :goto_0
.end method

.method public static convertStringDateToGregorianCalendar(Ljava/lang/String;)Ljava/util/GregorianCalendar;
    .locals 1
    .param p0, "stringDate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 283
    const-string/jumbo v0, "yyyyMMddHHmmssSSS"

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/moreapps/service/Util;->convertStringDateToGregorianCalendar(Ljava/lang/String;Ljava/lang/String;)Ljava/util/GregorianCalendar;

    move-result-object v0

    return-object v0
.end method

.method public static convertStringDateToGregorianCalendar(Ljava/lang/String;Ljava/lang/String;)Ljava/util/GregorianCalendar;
    .locals 3
    .param p0, "stringDate"    # Ljava/lang/String;
    .param p1, "dateFormat"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 290
    if-nez p0, :cond_0

    .line 291
    const/4 v0, 0x0

    .line 303
    :goto_0
    return-object v0

    .line 292
    :cond_0
    if-nez p1, :cond_1

    .line 293
    const-string/jumbo p1, "yyyyMMddHHmmssSSS"

    .line 296
    :cond_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 298
    .local v2, "format":Ljava/text/DateFormat;
    invoke-virtual {v2, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 300
    .local v1, "date":Ljava/util/Date;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 302
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    goto :goto_0
.end method

.method public static getInstalledApp(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 165
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 168
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    const-string/jumbo v2, "y"

    :goto_0
    return-object v2

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/moreapps/service/Util;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/moreapps/service/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string/jumbo v2, "n"

    goto :goto_0
.end method

.method public static getPrefInt(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 140
    const-string/jumbo v1, "pref"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 141
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getPrefLong(Landroid/content/Context;Ljava/lang/String;J)J
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 109
    const-string/jumbo v1, "pref"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 110
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 124
    const-string/jumbo v1, "pref"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getSystemTimeMillis(Ljava/lang/String;)J
    .locals 4
    .param p0, "date"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 272
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyyMMddHHmmssSSS"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 273
    .local v0, "sdFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 274
    .local v1, "tempDate":Ljava/util/Date;
    if-nez v1, :cond_0

    .line 275
    const-wide/16 v2, 0x0

    .line 277
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    goto :goto_0
.end method

.method public static getyyyyMMddHHmmssSSS(J)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # J
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyyMMddHHmmssSSS"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 266
    .local v0, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/sql/Timestamp;

    invoke-direct {v2, p0, p1}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "outString":Ljava/lang/String;
    return-object v1
.end method

.method public static isInternetOn(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 192
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 194
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    .line 195
    .local v4, "wifi":Landroid/net/NetworkInfo$State;
    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v4, v7, :cond_0

    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v4, v7, :cond_2

    :cond_0
    move v5, v6

    .line 212
    .end local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v4    # "wifi":Landroid/net/NetworkInfo$State;
    :cond_1
    :goto_0
    return v5

    .line 199
    .restart local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v4    # "wifi":Landroid/net/NetworkInfo$State;
    :cond_2
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 200
    .local v3, "mobile":Landroid/net/NetworkInfo$State;
    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v3, v7, :cond_3

    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v3, v7, :cond_4

    :cond_3
    move v5, v6

    .line 201
    goto :goto_0

    .line 204
    :cond_4
    const/4 v7, 0x7

    invoke-virtual {v1, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    .line 205
    .local v0, "btdata":Landroid/net/NetworkInfo$State;
    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v0, v7, :cond_5

    sget-object v7, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v7, :cond_1

    :cond_5
    move v5, v6

    .line 206
    goto :goto_0

    .line 209
    .end local v0    # "btdata":Landroid/net/NetworkInfo$State;
    .end local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v3    # "mobile":Landroid/net/NetworkInfo$State;
    .end local v4    # "wifi":Landroid/net/NetworkInfo$State;
    :catch_0
    move-exception v2

    .line 210
    .local v2, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static makeJsonBody(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/moreapps/service/JsonData;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 225
    .local p0, "arrayBody":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/moreapps/service/JsonData;>;"
    const-string v4, ""

    .line 226
    .local v4, "postParam":Ljava/lang/String;
    if-eqz p0, :cond_1

    .line 227
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 229
    .local v3, "obj":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 230
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    .line 231
    .local v1, "bodyData":Lcom/sec/android/app/shealth/moreapps/service/JsonData;
    iget v5, v1, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->bodyType:I

    packed-switch v5, :pswitch_data_0

    .line 229
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 234
    :pswitch_0
    iget-object v5, v1, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->paramName:Ljava/lang/String;

    iget-object v6, v1, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->paramValue:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 237
    :pswitch_1
    iget-object v5, v1, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->subBody:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/sec/android/app/shealth/moreapps/service/Util;->combineJson(Ljava/util/ArrayList;)Lorg/json/JSONArray;

    move-result-object v0

    .line 238
    .local v0, "arrayobj":Lorg/json/JSONArray;
    iget-object v5, v1, Lcom/sec/android/app/shealth/moreapps/service/JsonData;->paramName:Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 242
    .end local v0    # "arrayobj":Lorg/json/JSONArray;
    .end local v1    # "bodyData":Lcom/sec/android/app/shealth/moreapps/service/JsonData;
    :cond_0
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 244
    .end local v2    # "i":I
    .end local v3    # "obj":Lorg/json/JSONObject;
    :cond_1
    return-object v4

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static removePreferences(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 156
    :try_start_0
    const-string/jumbo v2, "pref"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 157
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 158
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 159
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static setPrefInt(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 146
    :try_start_0
    const-string/jumbo v2, "pref"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 147
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 148
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 149
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static setPrefLong(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 115
    :try_start_0
    const-string/jumbo v2, "pref"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 116
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 117
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 118
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static setPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 130
    :try_start_0
    const-string/jumbo v2, "pref"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 131
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 132
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 133
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static showLog(Z)V
    .locals 0
    .param p0, "showlog"    # Z

    .prologue
    .line 216
    sput-boolean p0, Lcom/sec/android/app/shealth/moreapps/service/Log;->showLog:Z

    .line 217
    return-void
.end method
