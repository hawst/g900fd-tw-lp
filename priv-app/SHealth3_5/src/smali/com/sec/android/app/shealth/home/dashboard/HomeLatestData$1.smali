.class Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;
.super Ljava/lang/Object;
.source "HomeLatestData.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivetimeChanged(J)V
    .locals 0
    .param p1, "duration"    # J

    .prologue
    .line 168
    return-void
.end method

.method public onHealthyStepCountChanged(J)V
    .locals 0
    .param p1, "steps"    # J

    .prologue
    .line 143
    return-void
.end method

.method public onHealthyStepStatusChanged(Z)V
    .locals 4
    .param p1, "isHealthyStep"    # Z

    .prologue
    .line 147
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    const/16 v1, 0x2719

    if-eq v0, v1, :cond_0

    .line 154
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$002(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 151
    const-string v0, "HomeLatestData"

    const-string v1, "\t[PERF] calling mUpdateViewRunnable from onHealthyStepStatusChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onInactiveTimeChanged(ZJZ)V
    .locals 4
    .param p1, "isInactive"    # Z
    .param p2, "duration"    # J
    .param p4, "isNoti"    # Z

    .prologue
    .line 158
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    const/16 v1, 0x2719

    if-eq v0, v1, :cond_0

    .line 164
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$002(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 162
    const-string v0, "HomeLatestData"

    const-string v1, "\t[PERF] calling mUpdateViewRunnable from onInactiveTimeChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$1;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mUpdateViewRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
