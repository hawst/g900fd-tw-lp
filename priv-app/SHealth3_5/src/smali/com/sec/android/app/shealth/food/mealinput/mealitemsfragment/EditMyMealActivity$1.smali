.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$1;
.super Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;
.source "EditMyMealActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/TextWatcherStub;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 51
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->setDoneButtonState(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;Z)V

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->access$100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;)Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->setDoneButtonState(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/EditMyMealActivity;Z)V

    goto :goto_0
.end method
