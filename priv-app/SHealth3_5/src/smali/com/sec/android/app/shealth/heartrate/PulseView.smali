.class public Lcom/sec/android/app/shealth/heartrate/PulseView;
.super Landroid/view/View;
.source "PulseView.java"


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private canvas:Landroid/graphics/Canvas;

.field private mIsEraserMode:Z

.field private paint:Landroid/graphics/Paint;

.field private path:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->mIsEraserMode:Z

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->initPaint()V

    .line 47
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->initPosition()V

    .line 49
    return-void
.end method

.method private convertDptoPx(F)F
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 130
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 131
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 132
    .local v1, "px":F
    return v1
.end method

.method private initPaint()V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070188

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/heartrate/PulseView;->convertDptoPx(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 58
    return-void
.end method


# virtual methods
.method public initPosition()V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/heartrate/PulseView;->convertDptoPx(F)F

    move-result v1

    const v2, 0x426aa3d7    # 58.66f

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/PulseView;->convertDptoPx(F)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 62
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->bitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 123
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->mIsEraserMode:Z

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 126
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 115
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 116
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->bitmap:Landroid/graphics/Bitmap;

    .line 117
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    .line 118
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->initPosition()V

    .line 102
    return-void
.end method

.method public setChangeColor(Z)V
    .locals 4
    .param p1, "isGreen"    # Z

    .prologue
    const v3, 0x7f070189

    const v2, 0x7f070188

    .line 78
    if-eqz p1, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->mIsEraserMode:Z

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 93
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->invalidate()V

    .line 94
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->mIsEraserMode:Z

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/PulseView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method public setMode(Z)V
    .locals 3
    .param p1, "isDrawing"    # Z

    .prologue
    .line 105
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->mIsEraserMode:Z

    .line 106
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->mIsEraserMode:Z

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->OVERLAY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 111
    :goto_1
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_1
.end method

.method public setPath(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    if-nez v0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/PulseView;->convertDptoPx(F)F

    move-result v1

    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/heartrate/PulseView;->convertDptoPx(F)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->mIsEraserMode:Z

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->canvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->path:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/PulseView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method
