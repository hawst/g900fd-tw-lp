.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$15;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$SamsungAccountDialogController;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$ResetDialogController;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$PauseDialogController;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;
    }
.end annotation


# static fields
.field private static final CONNECTION_STATE_CONNECTED:Ljava/lang/String; = "1"

.field public static final DEVICE_DISCONNECTED:I = 0x3

.field private static final HEALTHY_STEP_COLOR:I = -0xff4125

.field private static final INACTIVE_TIME_COLOR:I = -0xa85fe

.field private static final INACTIVE_TIME_TEXT_COLOR:I = -0xcb4f7

.field private static INTENT_FILTER:Landroid/content/IntentFilter; = null

.field private static final LOGGING_PEDOMETER_OFF:I = 0x0

.field private static final LOGGING_PEDOMETER_ON:I = 0x3e8

.field private static final NORMAL_STEP_COLOR:I = -0x8c46f1

.field private static final NOT_ASSIGNED:I = -0x1

.field public static final REQUEST_CODE_CALENDAR:I = 0x64

.field static final SPACE:Ljava/lang/String; = " "

.field public static final START_SAMUNG_SIGN_IN_ACTIVITY:I = 0x65

.field private static final SYNC_FAIL_MESSAGE:I = 0x1f

.field public static final SYNC_PROCESSING:I = 0x2

.field private static final SYNC_REQUEST_MESSAGE:I = 0xb

.field private static final SYNC_SUCCESS_MESSAGE:I = 0x15

.field public static final TIME_OUT:I = 0x0

.field public static final WRONG_DATA:I = 0x1

.field private static mContext:Landroid/content/Context;

.field private static mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# instance fields
.field private final SHOW_PROGRESS_PERCENTAGE:I

.field private TAG:Ljava/lang/String;

.field private alreadyInitDelay:I

.field private mAdditionalInfo:Landroid/widget/TextView;

.field private mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

.field private mBtnWalkingGraph:Landroid/widget/ImageButton;

.field private mCal:I

.field private mCalroryTxt:Landroid/widget/TextView;

.field private mConnectAccessory:I

.field public mConnectionStatus:Landroid/view/View;

.field private mCurrentEndofDate:J

.field private mCurrentStepContainer:Landroid/widget/LinearLayout;

.field private mCurrentStepTxt:Landroid/widget/TextView;

.field private mCurrentStepTxtK:Landroid/widget/TextView;

.field private mCurrentTextColor:I

.field private mDate:Ljava/util/Date;

.field private mDayStepCounterConnection:Landroid/content/ServiceConnection;

.field private mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

.field private mDistance:F

.field private mDistanceTxt:Landroid/widget/TextView;

.field private mDistanceUnit:Ljava/lang/String;

.field private mDistanceUnitTxt:Landroid/widget/TextView;

.field private mFirstSyncedGearDevice:Landroid/widget/TextView;

.field private mGoalTxt:Landroid/widget/TextView;

.field private mGoalValue:I

.field mHandler:Landroid/os/Handler;

.field mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

.field private mIconCombinedView:Landroid/widget/ImageView;

.field private mIconDefaultView:Landroid/widget/ImageView;

.field private mIconGearView:Landroid/widget/ImageView;

.field private mIconMedalView:Landroid/widget/ImageView;

.field private mIconPausedView:Landroid/widget/ImageView;

.field private mIsProjectK:Z

.field private mIsResumed:Z

.field private mIsRunRankingActivity:Z

.field private mLongDate:J

.field private mMergeCalorie:Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

.field private mNewRecord:Landroid/widget/TextView;

.field private mNewRecordCrown:Landroid/widget/ImageView;

.field private mOtherSyncedGearDevices:Landroid/widget/TextView;

.field private mPausedState:Landroid/widget/TextView;

.field private mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

.field private mSecondSyncedGearDevice:Landroid/widget/TextView;

.field private mShowedCal:I

.field private mShowedDistance:F

.field private mShowedSteps:I

.field private mSteps:I

.field private mSyncButton:Landroid/widget/ImageView;

.field private mSyncDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;

.field private mSyncLayout:Landroid/view/View;

.field private mSyncProgressImage:Landroid/view/View;

.field private mSyncStarted:Z

.field private mSyncTimeoutRunnable:Ljava/lang/Runnable;

.field private mToast:Landroid/widget/Toast;

.field private mTotalStep:I

.field private mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

.field private mUpdateTimeTxtView:Landroid/widget/TextView;

.field private mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

.field private mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

.field private final m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

.field private pedoMeterScoreText:Landroid/widget/TextView;

.field private syncHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 152
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.shealth.command.resettotalstep"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.shealth.command.deletetotalstep"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 158
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 110
    const-string v0, "WalkingMateSummaryFragment"

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    .line 112
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    .line 113
    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    .line 114
    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    .line 115
    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I

    .line 117
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->SHOW_PROGRESS_PERCENTAGE:I

    .line 126
    const v0, -0x8c46f1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    .line 132
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I

    .line 135
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSteps:I

    .line 136
    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    .line 137
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedDistance:F

    .line 138
    iput v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedCal:I

    .line 145
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    .line 146
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 161
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentEndofDate:J

    .line 171
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    .line 172
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .line 178
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    .line 180
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsRunRankingActivity:Z

    .line 186
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    .line 187
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mPausedState:Landroid/widget/TextView;

    .line 188
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    .line 189
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSecondSyncedGearDevice:Landroid/widget/TextView;

    .line 190
    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mOtherSyncedGearDevices:Landroid/widget/TextView;

    .line 198
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncStarted:Z

    .line 1450
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$8;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    .line 1507
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 1666
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$13;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->syncHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    .line 1690
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHandler:Landroid/os/Handler;

    .line 1692
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$14;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    .line 205
    const-string v0, "WalkingMateSummaryFragment is created"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 206
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .line 207
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "date"    # J

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;-><init>()V

    .line 211
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    .line 212
    const-string v0, "WalkingMateSummaryFragment is created"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 213
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncProgressImage:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsResumed:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsResumed:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSteps:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setPercentValue()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncStarted:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedCal:I

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCalroryTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # F

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->syncHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->registerListener()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startSync(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isStartWalking()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentEndofDate:J

    return-wide v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # J

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentEndofDate:J

    return-wide p1
.end method

.method static synthetic access$3202(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Ljava/util/Date;)Ljava/util/Date;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Ljava/util/Date;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->stepDataLoad()V

    return-void
.end method

.method static synthetic access$3902(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnitTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->dismissSyncDialog()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->showFailMessage()V

    return-void
.end method

.method static synthetic access$4400()Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFragmentContext:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$4802(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Ljava/util/Date;)Ljava/util/Date;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # Ljava/util/Date;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object p1
.end method

.method static synthetic access$4900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$600()Landroid/content/Context;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->displayGearState()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I

    return v0
.end method

.method private dismissSyncDialog()V
    .locals 1

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1661
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->dismiss()V

    .line 1662
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;

    .line 1664
    :cond_0
    return-void
.end method

.method private displayGearState()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x0

    const/16 v11, 0x2727

    const/16 v10, 0x2719

    const/16 v9, 0x8

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-nez v5, :cond_0

    .line 418
    :goto_0
    return-void

    .line 367
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->getLastUpdatedTime()Ljava/lang/String;

    move-result-object v2

    .line 368
    .local v2, "lastUpdatedTime":Ljava/lang/String;
    const-string v1, ""

    .line 370
    .local v1, "emptyString":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 371
    const-wide/16 v3, 0x0

    .line 372
    .local v3, "time":J
    const/16 v5, 0x272f

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getGearLastUpdatedTimeFromDB(I)J

    move-result-wide v3

    .line 373
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09081b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 374
    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_3

    .line 375
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string v8, "/"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getSystemDateFormat(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 376
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 377
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 383
    .end local v3    # "time":J
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setMedals()V

    .line 385
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSCSConnection(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 386
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-eq v5, v10, :cond_2

    .line 387
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mUpdateTimeTxtView:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;

    invoke-virtual {v5, v13}, Landroid/view/View;->setVisibility(I)V

    .line 390
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 379
    .restart local v3    # "time":J
    :cond_3
    const-string v2, ""

    goto :goto_1

    .line 394
    .end local v3    # "time":J
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-eq v5, v10, :cond_7

    .line 395
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mUpdateTimeTxtView:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;

    invoke-virtual {v5, v12}, Landroid/view/View;->setVisibility(I)V

    .line 401
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v0

    .line 403
    .local v0, "connectedDevice":I
    const/4 v5, -0x1

    if-eq v5, v0, :cond_9

    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-eq v5, v10, :cond_9

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncStarted:Z

    if-nez v5, :cond_9

    .line 406
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "under displayGearState() API , before calling WalkingMateSummaryUtils.isWintipConnected() API "

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    if-ne v0, v11, :cond_5

    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-eq v5, v11, :cond_6

    :cond_5
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-eq v5, v11, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isGearDeviceConnected(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateSummaryUtils;->isWintipConnected(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 410
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 414
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "under displayGearState() API , after calling WalkingMateSummaryUtils.isWintipConnected() API "

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 398
    .end local v0    # "connectedDevice":I
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;

    invoke-virtual {v5, v13}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 412
    .restart local v0    # "connectedDevice":I
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 416
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private doBindWalkStepService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1415
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1416
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1417
    .local v0, "iService":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1418
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string v2, "RealtimeHealthService is started"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421
    .end local v0    # "iService":Landroid/content/Intent;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 1423
    return-void
.end method

.method private doUnbindWalkStepService()V
    .locals 2

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 1427
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1429
    :cond_0
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1287
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getDeviceStepStringValue(I)Ljava/lang/String;
    .locals 8
    .param p1, "deviceType"    # I

    .prologue
    const v7, 0x7f090bc7

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 329
    const/4 v0, 0x0

    .line 330
    .local v0, "retValue":Ljava/lang/String;
    const/4 v1, 0x0

    .line 332
    .local v1, "tempSteps":I
    packed-switch p1, :pswitch_data_0

    .line 358
    :goto_0
    :pswitch_0
    return-object v0

    .line 334
    :pswitch_1
    const/16 v2, 0x2723

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v1

    .line 335
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090bc6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 336
    goto :goto_0

    .line 338
    :pswitch_2
    const/16 v2, 0x2728

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v1

    .line 339
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 340
    goto :goto_0

    .line 342
    :pswitch_3
    const/16 v2, 0x2724

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v1

    .line 343
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 344
    goto :goto_0

    .line 346
    :pswitch_4
    const/16 v2, 0x2726

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v1

    .line 347
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090bc8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 348
    goto/16 :goto_0

    .line 350
    :pswitch_5
    const/16 v2, 0x272e

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v1

    .line 351
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090bc4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 352
    goto/16 :goto_0

    .line 354
    :pswitch_6
    const/16 v2, 0x2730

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v1

    .line 355
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090bc5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 332
    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private isBest()Z
    .locals 12

    .prologue
    .line 1550
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    if-nez v9, :cond_0

    .line 1551
    const/4 v4, 0x0

    .line 1584
    :goto_0
    return v4

    .line 1554
    :cond_0
    const/4 v4, 0x0

    .line 1555
    .local v4, "rtn":Z
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getFirstDate()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 1556
    .local v2, "firstdate":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v7

    .line 1557
    .local v7, "today":J
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    .line 1558
    .local v5, "selectDate":J
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    .line 1560
    .local v0, "bestRecordDate":J
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "firstdate : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", selectDate : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", today : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mBestValue : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getSteps()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", mTotalStep : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1563
    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-lez v9, :cond_1

    cmp-long v9, v2, v7

    if-eqz v9, :cond_1

    cmp-long v9, v2, v5

    if-nez v9, :cond_3

    .line 1564
    :cond_1
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getSteps()I

    move-result v10

    if-lt v9, v10, :cond_2

    .line 1565
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1567
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1570
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getSteps()I

    move-result v9

    if-nez v9, :cond_4

    .line 1571
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1572
    :cond_4
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getSteps()I

    move-result v10

    if-le v9, v10, :cond_5

    .line 1573
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1574
    :cond_5
    iget v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getSteps()I

    move-result v10

    if-ne v9, v10, :cond_7

    .line 1575
    cmp-long v9, v5, v0

    if-nez v9, :cond_6

    .line 1576
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1578
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1581
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method private isStartWalking()Z
    .locals 1

    .prologue
    .line 1407
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v0

    return v0
.end method

.method private refreshValue()V
    .locals 2

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "refreshValue !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1308
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncDialog:Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$SyncingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->syncHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 1318
    :cond_0
    return-void
.end method

.method private registerListener()V
    .locals 2

    .prologue
    .line 1432
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    .line 1433
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->registerListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V

    .line 1434
    return-void
.end method

.method private setCaloriesText()V
    .locals 2

    .prologue
    .line 1135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$7;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1142
    return-void
.end method

.method private setCrown()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x4

    .line 1117
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setCrown"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isBest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1119
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecord:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecordCrown:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1126
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1127
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1128
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecord:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1129
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecordCrown:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1132
    :cond_0
    return-void

    .line 1122
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecord:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecordCrown:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setCurrentStepsStatus()V
    .locals 3

    .prologue
    .line 1007
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f080091

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 1009
    .local v0, "currentFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    instance-of v1, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    if-nez v1, :cond_0

    .line 1038
    :goto_0
    return-void

    .line 1013
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$6;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setDimmingSummaryProgess(Z)V
    .locals 7
    .param p1, "isDimming"    # Z

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 1717
    const/4 v0, 0x0

    .line 1718
    .local v0, "alpha":F
    if-eqz p1, :cond_5

    .line 1719
    const/high16 v0, 0x3f000000    # 0.5f

    .line 1720
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isStartWalking()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1721
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mPausedState:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1723
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v2

    .line 1724
    .local v2, "monitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    const/4 v1, 0x0

    .line 1725
    .local v1, "checkCondition":Z
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    const/16 v4, 0x2719

    if-eq v3, v4, :cond_0

    .line 1726
    const/4 v1, 0x0

    .line 1729
    :cond_0
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I

    if-ge v3, v4, :cond_1

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z

    if-eqz v3, :cond_4

    :cond_1
    if-nez v1, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1731
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string v4, "Skip to draw paused icon"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1743
    .end local v1    # "checkCondition":Z
    .end local v2    # "monitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->getAlpha()F

    move-result v3

    cmpl-float v3, v3, v0

    if-eqz v3, :cond_3

    .line 1744
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setAlpha(F)V

    .line 1745
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxt:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1747
    :cond_3
    return-void

    .line 1733
    .restart local v1    # "checkCondition":Z
    .restart local v2    # "monitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconPausedView:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1734
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1738
    .end local v1    # "checkCondition":Z
    .end local v2    # "monitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1739
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mPausedState:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1740
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconPausedView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setDistanceValueAndUnitText()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/high16 v5, 0x42c80000    # 100.0f

    .line 1588
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    const-string/jumbo v4, "mi"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1589
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedDistance:F

    const v4, 0x3a22e36f

    mul-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    int-to-float v3, v3

    div-float v0, v3, v5

    .line 1591
    .local v0, "convert_distance":F
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1592
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%.2f"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1596
    .local v2, "value":Ljava/lang/String;
    :goto_0
    const v3, 0x7f0900cc

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1597
    .local v1, "unit":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$10;

    invoke-direct {v4, p0, v2, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$10;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1626
    :goto_1
    return-void

    .line 1594
    .end local v1    # "unit":Ljava/lang/String;
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    float-to-double v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_0

    .line 1608
    .end local v0    # "convert_distance":F
    .end local v2    # "value":Ljava/lang/String;
    :cond_1
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedDistance:F

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    div-float v0, v3, v5

    .line 1609
    .restart local v0    # "convert_distance":F
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1610
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%.2f"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1614
    .restart local v2    # "value":Ljava/lang/String;
    :goto_2
    const v3, 0x7f0900c7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1615
    .restart local v1    # "unit":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;

    invoke-direct {v4, p0, v2, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1612
    .end local v1    # "unit":Ljava/lang/String;
    .end local v2    # "value":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    float-to-double v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_2
.end method

.method private setMedals()V
    .locals 8

    .prologue
    const/16 v7, 0x2719

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x4

    .line 1060
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "setMedals"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-nez v3, :cond_0

    .line 1113
    :goto_0
    return-void

    .line 1065
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v3

    if-ne v3, v2, :cond_7

    move v0, v2

    .line 1066
    .local v0, "checkCondition":Z
    :goto_1
    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v3

    if-ne v3, v2, :cond_8

    :cond_1
    move v0, v2

    .line 1068
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v2

    if-eq v2, v7, :cond_2

    .line 1069
    const/4 v0, 0x0

    .line 1072
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    if-nez v2, :cond_3

    .line 1073
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 1074
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 1075
    const/4 v0, 0x0

    .line 1078
    :cond_4
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I

    if-ge v2, v3, :cond_5

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z

    if-eqz v2, :cond_a

    :cond_5
    if-nez v0, :cond_a

    .line 1079
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconMedalView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1080
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-ne v1, v7, :cond_9

    .line 1081
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconMedalView:Landroid/widget/ImageView;

    const v2, 0x7f0206d1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1085
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v1, :cond_6

    .line 1086
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1087
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconGearView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1088
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconCombinedView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1089
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconPausedView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1112
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCrown()V

    goto/16 :goto_0

    .end local v0    # "checkCondition":Z
    :cond_7
    move v0, v1

    .line 1065
    goto :goto_1

    .restart local v0    # "checkCondition":Z
    :cond_8
    move v0, v1

    .line 1066
    goto :goto_2

    .line 1083
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconMedalView:Landroid/widget/ImageView;

    const v2, 0x7f020696

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 1091
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconMedalView:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1092
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-ne v2, v7, :cond_d

    .line 1093
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isStartWalking()Z

    move-result v2

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v2, :cond_c

    .line 1094
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1097
    :cond_b
    :goto_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconGearView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1098
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconCombinedView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 1095
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v2, :cond_b

    .line 1096
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 1099
    :cond_d
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-nez v2, :cond_f

    .line 1100
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v2, :cond_e

    .line 1101
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1102
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconGearView:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1103
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconCombinedView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 1105
    :cond_f
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v2, :cond_10

    .line 1106
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1107
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconGearView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1108
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconCombinedView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method

.method private setPercentValue()V
    .locals 2

    .prologue
    .line 886
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 887
    const-string/jumbo v0, "summary fragment is not added"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 950
    :goto_0
    return-void

    .line 890
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setTextSwitcherColor()V
    .locals 2

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxt:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1301
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1302
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1304
    :cond_0
    return-void
.end method

.method private showFailMessage()V
    .locals 4

    .prologue
    .line 1630
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1631
    .local v0, "mAlertDialog_Fail":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f0902b0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1632
    const v1, 0x7f0902b2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1633
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$12;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1656
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1657
    return-void
.end method

.method private startSync(Z)V
    .locals 6
    .param p1, "start"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1702
    if-eqz p1, :cond_0

    .line 1703
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0x88b8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1704
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1705
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncProgressImage:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncStarted:Z

    .line 1713
    :goto_0
    return-void

    .line 1709
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1710
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncProgressImage:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1711
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncStarted:Z

    goto :goto_0
.end method

.method private stepDataLoad()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    .line 1378
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-nez v3, :cond_1

    .line 1379
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1380
    .local v0, "tempCal":Ljava/util/Calendar;
    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1381
    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    .line 1382
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->loadLatestData(Ljava/util/Date;)V

    .line 1399
    .end local v0    # "tempCal":Ljava/util/Calendar;
    :goto_0
    return-void

    .line 1384
    .restart local v0    # "tempCal":Ljava/util/Calendar;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->loadLatestData(Ljava/util/Date;)V

    goto :goto_0

    .line 1387
    .end local v0    # "tempCal":Ljava/util/Calendar;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 1388
    .local v1, "time":J
    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    .line 1389
    iget-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    .line 1391
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    .line 1392
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshgoal()V

    .line 1393
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshValue()V

    .line 1394
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateWithoutAnimation()V

    goto :goto_0

    .line 1396
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->loadLatestData(Ljava/util/Date;)V

    goto :goto_0
.end method

.method private unregisterListener()V
    .locals 2

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    if-eqz v0, :cond_0

    .line 1438
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->unregisterListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V

    .line 1439
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    .line 1441
    :cond_0
    return-void
.end method

.method private updateSyncedDeviceAdditionalInfomation()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    const/16 v12, 0x8

    .line 275
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 276
    .local v0, "curViewStepCount":I
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v8, "syncedDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getSynchronizedWearableList(J)Ljava/util/ArrayList;

    move-result-object v8

    .line 278
    if-eqz v8, :cond_5

    .line 279
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 280
    .local v4, "numSyncedDeivces":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 281
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "updateSyncedDeviceAdditionalInfomation - syncedDevices : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 284
    :cond_0
    if-eqz v4, :cond_1

    if-eq v4, v14, :cond_1

    const/16 v9, 0x2719

    if-ne v0, v9, :cond_2

    .line 287
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSecondSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mOtherSyncedGearDevices:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 326
    .end local v3    # "i":I
    .end local v4    # "numSyncedDeivces":I
    :goto_1
    return-void

    .line 290
    .restart local v3    # "i":I
    .restart local v4    # "numSyncedDeivces":I
    :cond_2
    const/4 v9, 0x2

    if-ne v4, v9, :cond_3

    .line 291
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSecondSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 293
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mOtherSyncedGearDevices:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 295
    const/4 v2, 0x0

    .line 296
    .local v2, "firstSyncedDeviceSteps":Ljava/lang/String;
    const/4 v7, 0x0

    .line 297
    .local v7, "secondSyncedDeviceSteps":Ljava/lang/String;
    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDeviceStepStringValue(I)Ljava/lang/String;

    move-result-object v2

    .line 298
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDeviceStepStringValue(I)Ljava/lang/String;

    move-result-object v7

    .line 299
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSecondSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 303
    .end local v2    # "firstSyncedDeviceSteps":Ljava/lang/String;
    .end local v7    # "secondSyncedDeviceSteps":Ljava/lang/String;
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 304
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSecondSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 305
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mOtherSyncedGearDevices:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    const/4 v6, 0x0

    .line 308
    .local v6, "otherSteps":I
    const/4 v1, 0x0

    .line 309
    .local v1, "firstDeviceSteps":Ljava/lang/String;
    const/4 v5, 0x0

    .line 310
    .local v5, "otherDeviceSteps":Ljava/lang/String;
    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDeviceStepStringValue(I)Ljava/lang/String;

    move-result-object v1

    .line 311
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    const/4 v3, 0x1

    :goto_2
    if-ge v3, v4, :cond_4

    .line 314
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalStepsFromDeviceType(IJ)I

    move-result v9

    add-int/2addr v6, v9

    .line 313
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 316
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090bc9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v14, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v13

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 317
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mOtherSyncedGearDevices:Landroid/widget/TextView;

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 321
    .end local v1    # "firstDeviceSteps":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "numSyncedDeivces":I
    .end local v5    # "otherDeviceSteps":Ljava/lang/String;
    .end local v6    # "otherSteps":I
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "updateSyncedDeviceAdditionalInfomation - syncedDevices is null"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSecondSyncedGearDevice:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mOtherSyncedGearDevices:Landroid/widget/TextView;

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 1546
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateCalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1504
    const-string/jumbo v0, "start_time"

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1499
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 543
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->initCirclePercentValue()V

    .line 545
    if-eqz v0, :cond_2

    .line 546
    const-string v6, "Date"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 547
    const-string v6, "Date"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    .line 548
    const-string v6, "direct_best"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getContentView date = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    :cond_0
    const-string/jumbo v6, "syncstart"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 551
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v6

    const/16 v7, 0x2719

    if-eq v6, v7, :cond_1

    .line 552
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    iget v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getWearableSyncIntent(I)Landroid/content/Intent;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v6, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 553
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/32 v8, 0x88b8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 555
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "sync start"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    :cond_2
    iget-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    .line 559
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 560
    .local v5, "tempCal":Ljava/util/Calendar;
    const-string v6, "direct_best"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getContentView date = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    iget-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-virtual {v5, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 562
    new-instance v6, Ljava/util/Date;

    iget-wide v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 564
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v6

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setText(Ljava/util/Date;)V

    .line 567
    .end local v5    # "tempCal":Ljava/util/Calendar;
    :cond_3
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsRunRankingActivity:Z

    .line 569
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    .line 570
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getBestRecordInfo()Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .line 571
    sput-object p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mContext:Landroid/content/Context;

    .line 572
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWwflListener:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;

    .line 573
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->doBindWalkStepService(Landroid/content/Context;)V

    .line 574
    new-instance v6, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-direct {v6, p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 575
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    .line 576
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    if-nez v6, :cond_4

    .line 577
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900c7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    .line 580
    :cond_4
    const-string v6, "layout_inflater"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 581
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f0302b2

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 583
    .local v3, "rootView":Landroid/view/View;
    new-instance v2, Landroid/text/SpannableString;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090ba2

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 585
    .local v2, "newRecord":Landroid/text/SpannableString;
    new-instance v6, Landroid/text/style/StyleSpan;

    const/4 v7, 0x2

    invoke-direct {v6, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v7, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v2, v6, v7, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 586
    const v6, 0x7f080c07

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecord:Landroid/widget/TextView;

    .line 587
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecord:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 590
    const v6, 0x7f080c06

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mNewRecordCrown:Landroid/widget/ImageView;

    .line 592
    const v6, 0x7f080c0f

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    .line 593
    const v6, 0x7f080c10

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mPausedState:Landroid/widget/TextView;

    .line 594
    const v6, 0x7f080c12

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mFirstSyncedGearDevice:Landroid/widget/TextView;

    .line 595
    const v6, 0x7f080c13

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSecondSyncedGearDevice:Landroid/widget/TextView;

    .line 596
    const v6, 0x7f080c14

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mOtherSyncedGearDevices:Landroid/widget/TextView;

    .line 597
    const v6, 0x7f0804bc

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    .line 598
    const v6, 0x7f080c08

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconCombinedView:Landroid/widget/ImageView;

    .line 599
    const v6, 0x7f080c09

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconGearView:Landroid/widget/ImageView;

    .line 600
    const v6, 0x7f0804bd

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconMedalView:Landroid/widget/ImageView;

    .line 601
    const v6, 0x7f080c0a

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconPausedView:Landroid/widget/ImageView;

    .line 603
    const v6, 0x7f080c21

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mUpdateTimeTxtView:Landroid/widget/TextView;

    .line 604
    const v6, 0x7f080c20

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;

    .line 605
    const v6, 0x7f080520

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncProgressImage:Landroid/view/View;

    .line 608
    const v6, 0x7f080c22

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    .line 609
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    const v7, 0x7f090bda

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 610
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    new-instance v7, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 636
    const v6, 0x7f080c0b

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxt:Landroid/widget/TextView;

    .line 637
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxt:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->makeView(Landroid/widget/TextView;)V

    .line 638
    const v6, 0x7f080c0c

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;

    .line 639
    const v6, 0x7f0804be

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepContainer:Landroid/widget/LinearLayout;

    .line 643
    const v6, 0x7f080c0e

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalTxt:Landroid/widget/TextView;

    .line 645
    const v6, 0x7f080c18

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceTxt:Landroid/widget/TextView;

    .line 646
    const v6, 0x7f080c19

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnitTxt:Landroid/widget/TextView;

    .line 647
    const v6, 0x7f080c1d

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCalroryTxt:Landroid/widget/TextView;

    .line 649
    const v6, 0x7f0804ba

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .line 650
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const/high16 v7, -0x3d4c0000    # -90.0f

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setRotation(F)V

    .line 651
    const v6, 0x7f080986

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectionStatus:Landroid/view/View;

    .line 653
    const v6, 0x7f080c24

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->pedoMeterScoreText:Landroid/widget/TextView;

    .line 655
    const v6, 0x7f080c23

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 656
    .local v4, "scoreBoardButton":Landroid/view/View;
    if-eqz v4, :cond_6

    .line 657
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->pedoMeterScoreText:Landroid/widget/TextView;

    if-eqz v6, :cond_5

    .line 658
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->pedoMeterScoreText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090bd5

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 660
    :cond_5
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 661
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090bd5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 662
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v4, v6, v7, v8}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_6
    const v6, 0x7f080c25

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBtnWalkingGraph:Landroid/widget/ImageButton;

    .line 666
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBtnWalkingGraph:Landroid/widget/ImageButton;

    invoke-virtual {v6, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 667
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->loadLatestData(Ljava/util/Date;)V

    .line 669
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateAnimation(I)V

    .line 670
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    .line 672
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentEndofDate:J

    .line 675
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setHasOptionsMenu(Z)V

    .line 676
    const/4 v6, 0x1

    sput-boolean v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isSummaryFragment:Z

    .line 677
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->onUpdateScreen()V

    .line 679
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    const/16 v7, 0x2719

    if-ne v6, v7, :cond_7

    .line 680
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 685
    :goto_0
    new-instance v6, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 711
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->addCallBacks(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;)V

    .line 714
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateCurrentMode()V

    .line 715
    return-object v3

    .line 682
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->displayGearState()V

    goto :goto_0
.end method

.method public getInactiveTimeComment(JJ)Ljava/lang/String;
    .locals 9
    .param p1, "hour"    # J
    .param p3, "mins"    # J

    .prologue
    const/4 v4, 0x2

    const-wide/16 v7, 0x0

    const/4 v6, 0x1

    const-wide/16 v2, 0x1

    const/4 v5, 0x0

    .line 719
    const-string v0, ""

    .line 721
    .local v0, "formattedTitle":Ljava/lang/String;
    cmp-long v1, p1, v2

    if-gez v1, :cond_2

    .line 722
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 723
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f090be7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 766
    :cond_0
    :goto_0
    return-object v0

    .line 725
    :cond_1
    const v1, 0x7f090be7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 727
    :cond_2
    cmp-long v1, p1, v2

    if-nez v1, :cond_4

    cmp-long v1, p3, v7

    if-nez v1, :cond_4

    .line 728
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 729
    const v1, 0x7f090bf5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 731
    :cond_3
    const v1, 0x7f090be8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 733
    :cond_4
    cmp-long v1, p1, v2

    if-nez v1, :cond_6

    cmp-long v1, p3, v2

    if-nez v1, :cond_6

    .line 734
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 735
    const v1, 0x7f090bf6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 737
    :cond_5
    const v1, 0x7f090be9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 739
    :cond_6
    cmp-long v1, p1, v2

    if-nez v1, :cond_8

    cmp-long v1, p3, v7

    if-eqz v1, :cond_8

    cmp-long v1, p3, v2

    if-eqz v1, :cond_8

    .line 740
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 741
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f090bf7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 743
    :cond_7
    const v1, 0x7f090bea

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 746
    :cond_8
    cmp-long v1, p1, v2

    if-lez v1, :cond_a

    cmp-long v1, p3, v7

    if-nez v1, :cond_a

    .line 747
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 748
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f090beb

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 750
    :cond_9
    const v1, 0x7f090beb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 752
    :cond_a
    cmp-long v1, p1, v2

    if-lez v1, :cond_c

    cmp-long v1, p3, v2

    if-nez v1, :cond_c

    .line 753
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 754
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f090bf8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 756
    :cond_b
    const v1, 0x7f090bec

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 758
    :cond_c
    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    cmp-long v1, p3, v2

    if-lez v1, :cond_0

    .line 759
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 760
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f090bed

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 762
    :cond_d
    const v1, 0x7f090bed

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public getSelectedDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method public initCirclePercentValue()V
    .locals 1

    .prologue
    .line 953
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsResumed:Z

    .line 954
    return-void
.end method

.method public loadLatestData(Ljava/util/Date;)V
    .locals 8
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    const/4 v2, 0x0

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadLatestData !!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mMergeCalorie:Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    if-nez v1, :cond_0

    .line 226
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mMergeCalorie:Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    .line 230
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateConnectionStatus()V

    .line 232
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sput-wide v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    .line 233
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "summary selectedTime : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 235
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v7

    .line 236
    .local v7, "deviceType":I
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;-><init>(IFFJ)V

    .line 237
    .local v0, "data":Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2, v7}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getTotalValuesFromAllDevice(JI)Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getTotalSteps()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    .line 240
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getCalories()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    .line 241
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->getDistance()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    .line 242
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDistance(F)V

    .line 243
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    int-to-float v1, v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCalories(F)V

    .line 244
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setSteps(I)V

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mTotalStep="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mCal="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mDistance="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateWithoutAnimation()V

    .line 251
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 252
    .local v6, "calForWidget":Ljava/util/Calendar;
    invoke-virtual {v6, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 253
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 255
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 270
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateSyncedDeviceAdditionalInfomation()V

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateCurrentMode()V

    .line 272
    return-void

    .line 257
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public makeView(Landroid/widget/TextView;)V
    .locals 4
    .param p1, "textFormat"    # Landroid/widget/TextView;

    .prologue
    const/4 v3, 0x0

    .line 1291
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1292
    const/4 v0, 0x1

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1293
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1294
    const-string v0, "Roboto_Light"

    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1295
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 1296
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setImportantForAccessibility(I)V

    .line 1297
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1470
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1471
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onActivityResult Code :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1472
    const/16 v1, 0x64

    if-ne v1, p1, :cond_2

    .line 1473
    if-nez p3, :cond_1

    .line 1485
    :cond_0
    :goto_0
    return-void

    .line 1476
    :cond_1
    const-string v1, "calendar_result"

    const-wide/16 v2, -0x1

    invoke-virtual {p3, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1477
    .local v0, "timeInMili":Ljava/lang/Long;
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 1478
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    goto :goto_0

    .line 1480
    .end local v0    # "timeInMili":Ljava/lang/Long;
    :cond_2
    const/16 v1, 0x65

    if-ne v1, p1, :cond_0

    const/4 v1, -0x1

    if-ne v1, p2, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsRunRankingActivity:Z

    if-nez v1, :cond_0

    .line 1481
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string v2, "Run WalkingMateRankingActivity"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1482
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsRunRankingActivity:Z

    .line 1483
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 217
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onAttach(Landroid/app/Activity;)V

    .line 218
    check-cast p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    .line 219
    const-string v0, "WalkingMateSummaryFragment is attached"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 220
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 456
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onBackPressed()V

    .line 457
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1769
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f080c23

    if-ne v2, v3, :cond_2

    .line 1776
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->checkForSamsungAccounts(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1777
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startActivity(Landroid/content/Intent;)V

    .line 1792
    :cond_0
    :goto_0
    return-void

    .line 1779
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f09086e

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v3, 0x7f090ba5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 1782
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "SAMSUNGACCOUNT"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1785
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f080c25

    if-ne v2, v3, :cond_0

    .line 1787
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1788
    :catch_0
    move-exception v1

    .line 1789
    .local v1, "exception":Ljava/lang/IllegalStateException;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "This error can be ignored(after onSaveInstanceState)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1403
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1404
    return-void
.end method

.method public onDateChanged(Ljava/util/Date;)V
    .locals 3
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 1263
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 1264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsResumed:Z

    .line 1265
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 1266
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    .line 1267
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->loadLatestData(Ljava/util/Date;)V

    .line 1268
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DATE UPDATE +"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getBestRecordInfo()Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .line 1270
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateAnimation(I)V

    .line 1271
    return-void
.end method

.method public onDateChangedFromNotification(J)V
    .locals 4
    .param p1, "time"    # J

    .prologue
    .line 1274
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1284
    :cond_0
    :goto_0
    return-void

    .line 1277
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 1278
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSystemDate(Ljava/util/Date;)V

    .line 1279
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setSelectedText(Ljava/util/Date;)V

    .line 1281
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->stepDataLoad()V

    .line 1282
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->alreadyInitDelay:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateAnimation(I)V

    .line 1283
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 872
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 873
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->unregisterListener()V

    .line 875
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepCounterConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-eqz v0, :cond_0

    .line 876
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->doUnbindWalkStepService()V

    .line 877
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 448
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroyView()V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    if-eqz v0, :cond_0

    .line 450
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 452
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1203
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->dismissSyncDialog()V

    .line 1204
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1205
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1206
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onPause()V

    .line 1207
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1146
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 1147
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->INTENT_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->initCirclePercentValue()V

    .line 1150
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    .line 1152
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIsLaunchedFromWidget()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSyncing()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    const/16 v2, 0x2719

    if-eq v1, v2, :cond_0

    .line 1154
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string v2, "Start to sync wearable device"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setSyncing(Z)Z

    .line 1156
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getWearableSyncIntent(I)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1160
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsRunRankingActivity:Z

    .line 1162
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getIsLaunchedFromWidget()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1163
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mWalkingMateActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->setIsLaunchedFromWidget(Z)V

    .line 1164
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startSync(Z)V

    .line 1167
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isWalkingMateEnter:Z

    if-eqz v1, :cond_2

    .line 1168
    sput-boolean v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->isWalkingMateEnter:Z

    .line 1170
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsResumed:Z

    .line 1173
    :cond_2
    iget-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    cmp-long v1, v1, v6

    if-eqz v1, :cond_3

    .line 1174
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1175
    .local v0, "tempCal":Ljava/util/Calendar;
    const-string v1, "direct_best"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "date = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1176
    iget-wide v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1177
    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 1178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 1179
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setText(Ljava/util/Date;)V

    .line 1180
    iput-wide v6, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mLongDate:J

    .line 1182
    .end local v0    # "tempCal":Ljava/util/Calendar;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    .line 1183
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1184
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDistanceValueAndUnitText()V

    .line 1187
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I

    .line 1188
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getBestRecordInfo()Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .line 1190
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 1191
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshgoal()V

    .line 1192
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshValue()V

    .line 1193
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    .line 1194
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setMedals()V

    .line 1195
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setPercentValue()V

    .line 1196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1197
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateCurrentMode()V

    .line 1199
    :cond_5
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 0

    .prologue
    .line 1908
    return-void
.end method

.method public onUpdateScreen()V
    .locals 8

    .prologue
    const/16 v7, 0x2719

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1750
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 1751
    .local v0, "today":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    .line 1752
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-ne v3, v7, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isStartWalking()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDimmingSummaryProgess(Z)V

    .line 1756
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 1752
    goto :goto_0

    .line 1754
    :cond_1
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    if-ne v3, v7, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isStartWalking()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDimmingSummaryProgess(Z)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method pausePedometer()V
    .locals 4

    .prologue
    .line 526
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "pausePedometer()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    if-nez v1, :cond_0

    .line 528
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_PAUSE:I

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->playSound(I)V

    .line 530
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    .line 531
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 533
    .local v0, "pauseDialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 534
    const v1, 0x7f090054

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 535
    const v1, 0x7f090b67

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 536
    const v1, 0x7f090053

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 537
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "PAUSE"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 538
    return-void
.end method

.method public refreshgoal()V
    .locals 1

    .prologue
    .line 441
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getBestRecordInfo()Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mBestRecordInfo:Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .line 443
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->refreshgoal(Z)V

    .line 444
    return-void
.end method

.method public refreshgoal(Z)V
    .locals 2
    .param p1, "refreshData"    # Z

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 422
    const-string v0, "fragment is not attached"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 438
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I

    .line 427
    if-eqz p1, :cond_1

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->loadLatestData(Ljava/util/Date;)V

    .line 431
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    .line 433
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I

    const/16 v1, 0x2719

    if-ne v0, v1, :cond_2

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 436
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->displayGearState()V

    goto :goto_0
.end method

.method public setCalories(F)V
    .locals 1
    .param p1, "cal2"    # F

    .prologue
    .line 1045
    float-to-int v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedCal:I

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    .line 1046
    return-void
.end method

.method public setDateFromLog(Landroid/content/Intent;)V
    .locals 5
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 1488
    const-string v1, "SelectedDate"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1490
    .local v0, "timeInMili":Ljava/lang/Long;
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 1491
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 1492
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->stepDataLoad()V

    .line 1493
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setNextAndPrevDates()V

    .line 1495
    return-void
.end method

.method public setDistance(F)V
    .locals 0
    .param p1, "distance2"    # F

    .prologue
    .line 1041
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedDistance:F

    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    .line 1042
    return-void
.end method

.method protected setNextAndPrevDates()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 1211
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 1212
    .local v6, "cal":Ljava/util/Calendar;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1215
    const-wide/16 v8, 0x0

    .line 1216
    .local v8, "lastDataDate":J
    const/4 v7, 0x0

    .line 1218
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "start_time DESC  LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1223
    if-eqz v7, :cond_0

    .line 1224
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1225
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1226
    const-string/jumbo v0, "start_time"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 1230
    :cond_0
    if-eqz v7, :cond_1

    .line 1231
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1235
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 1237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorUtils;->getPrevDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v11

    .line 1238
    .local v11, "prevdate":Ljava/util/Date;
    new-instance v10, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {v10, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 1243
    .local v10, "nextdate":Ljava/util/Date;
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevDate(Ljava/util/Date;)V

    .line 1244
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 1245
    if-eqz v11, :cond_5

    .line 1246
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    .line 1250
    :goto_1
    if-eqz v10, :cond_6

    .line 1251
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 1255
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getNextDate()Ljava/util/Date;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    if-ne v0, v1, :cond_2

    .line 1256
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 1257
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 1259
    :cond_2
    return-void

    .line 1230
    .end local v10    # "nextdate":Ljava/util/Date;
    .end local v11    # "prevdate":Ljava/util/Date;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 1231
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1240
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorUtils;->getPrevDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v11

    .line 1241
    .restart local v11    # "prevdate":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/ActivityMonitorUtils;->getNextDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v10

    .restart local v10    # "nextdate":Ljava/util/Date;
    goto :goto_0

    .line 1248
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    goto :goto_1

    .line 1253
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    goto :goto_2
.end method

.method public setSelectedDate(Ljava/util/Date;)V
    .locals 3
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSelectedDate > date : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDate:Ljava/util/Date;

    .line 475
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 476
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 477
    return-void
.end method

.method public setSelectedDate(Ljava/util/Date;Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 3
    .param p1, "date"    # Ljava/util/Date;
    .param p2, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 1444
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSelectedDate > date : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and actionBarHelper "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1445
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDate:Ljava/util/Date;

    .line 1446
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 1447
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 1448
    return-void
.end method

.method public setSelectedText(Ljava/util/Date;)V
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setText(Ljava/util/Date;)V

    .line 481
    return-void
.end method

.method public setSteps(I)V
    .locals 1
    .param p1, "steps"    # I

    .prologue
    .line 1049
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSteps:I

    .line 1050
    const v0, 0x1869f

    if-le p1, v0, :cond_0

    .line 1051
    div-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    .line 1052
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z

    .line 1057
    :goto_0
    return-void

    .line 1054
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    .line 1055
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z

    goto :goto_0
.end method

.method startPedometer()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startPedometer()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    sget v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    if-nez v0, :cond_0

    .line 492
    sget v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_START:I

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->playSound(I)V

    .line 496
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W001"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W009"

    const/16 v3, 0x3e8

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertStatusLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 501
    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setStartWalking(Z)V

    .line 502
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 503
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    .line 504
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setMedals()V

    .line 505
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->onUpdateScreen()V

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090b90

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mToast:Landroid/widget/Toast;

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    if-eqz v0, :cond_2

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStart()V

    .line 522
    :goto_0
    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    .line 523
    return-void

    .line 518
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 519
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    goto :goto_0
.end method

.method public syncRequestByActivity()V
    .locals 1

    .prologue
    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1762
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startSync(Z)V

    .line 1764
    :cond_0
    return-void
.end method

.method public updateAnimation(I)V
    .locals 1
    .param p1, "delay"    # I

    .prologue
    .line 880
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateWithoutAnimation()V

    .line 881
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistance:F

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDistance(F)V

    .line 882
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCal:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCalories(F)V

    .line 883
    return-void
.end method

.method public updateConnectionStatus()V
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectionStatus:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 868
    return-void
.end method

.method public updateCurrentMode()V
    .locals 15

    .prologue
    .line 770
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "updateCurrentMode"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 773
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 838
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v7

    .line 779
    .local v7, "monitor":Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v9

    const/16 v10, 0x2719

    if-ne v9, v10, :cond_5

    const/4 v4, 0x1

    .line 781
    .local v4, "isDevice":Z
    :goto_1
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 782
    .local v8, "today":Ljava/util/Date;
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    if-nez v9, :cond_2

    .line 783
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 785
    :cond_2
    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v9

    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v11}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-eqz v9, :cond_3

    .line 786
    const/4 v4, 0x0

    .line 787
    const v9, -0x8c46f1

    iput v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    .line 788
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "selected date is not today"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "selected date = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v11}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "today = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    :cond_3
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v9

    if-eqz v9, :cond_6

    if-eqz v4, :cond_6

    .line 794
    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInactiveTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v1

    .line 795
    .local v1, "hour":J
    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInactiveTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v9

    sget-object v11, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInactiveTime()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v11

    sub-long v5, v9, v11

    .line 797
    .local v5, "mins":J
    invoke-virtual {p0, v1, v2, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getInactiveTimeComment(JJ)Ljava/lang/String;

    move-result-object v3

    .line 798
    .local v3, "inactiveTimeComment":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const v10, -0xcb4f7

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 799
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 800
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 801
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v9, :cond_4

    .line 802
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0206d2

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 805
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const v10, -0xa85fe

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setColor(I)V

    .line 806
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const v10, -0xa85fe

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setGoalColor(I)V

    .line 807
    const v9, -0xa85fe

    iput v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    .line 808
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setTextSwitcherColor()V

    .line 809
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string v10, "inactive time"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    .end local v1    # "hour":J
    .end local v3    # "inactiveTimeComment":Ljava/lang/String;
    .end local v5    # "mins":J
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setMedals()V

    .line 837
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->onUpdateScreen()V

    goto/16 :goto_0

    .line 779
    .end local v4    # "isDevice":Z
    .end local v8    # "today":Ljava/util/Date;
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 810
    .restart local v4    # "isDevice":Z
    .restart local v8    # "today":Ljava/util/Date;
    :cond_6
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v9

    if-eqz v9, :cond_8

    if-eqz v4, :cond_8

    .line 811
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const v10, -0xff4125

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 812
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const v10, 0x7f0907e4

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 813
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 814
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v9, :cond_7

    .line 815
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0206cf

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 818
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const v10, -0xff4125

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setColor(I)V

    .line 819
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const v10, -0xff4125

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setGoalColor(I)V

    .line 820
    const v9, -0xff4125

    iput v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    .line 821
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setTextSwitcherColor()V

    .line 822
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string v10, "healthy mode"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 824
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    if-eqz v9, :cond_9

    .line 825
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIconDefaultView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0206d0

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 828
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 829
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mAdditionalInfo:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 830
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const v10, -0x8c46f1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setColor(I)V

    .line 831
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const v10, -0x8c46f1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setGoalColor(I)V

    .line 832
    const v9, -0x8c46f1

    iput v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentTextColor:I

    .line 833
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setTextSwitcherColor()V

    .line 834
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "normal mode"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public updateWithoutAnimation()V
    .locals 11

    .prologue
    const/16 v7, 0x8

    const/4 v10, 0x6

    const v9, 0x7f0900c6

    const/4 v6, 0x0

    .line 958
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setTextSwitcherColor()V

    .line 959
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 961
    .local v2, "r":Landroid/content/res/Resources;
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    if-nez v5, :cond_1

    .line 962
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCurrentStepsStatus()V

    .line 963
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDistanceValueAndUnitText()V

    .line 964
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setMedals()V

    .line 965
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCaloriesText()V

    .line 966
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepContainer:Landroid/widget/LinearLayout;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1004
    :cond_0
    :goto_0
    return-void

    .line 970
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    .line 971
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z

    if-eqz v5, :cond_3

    move v5, v6

    :goto_1
    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 972
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepTxtK:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 975
    :cond_2
    new-array v4, v10, [I

    .line 976
    .local v4, "visible":[I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ""

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 977
    .local v3, "temp":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v10, :cond_5

    .line 978
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 979
    aput v6, v4, v1

    .line 977
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v1    # "i":I
    .end local v3    # "temp":Ljava/lang/String;
    .end local v4    # "visible":[I
    :cond_3
    move v5, v7

    .line 971
    goto :goto_1

    .line 981
    .restart local v1    # "i":I
    .restart local v3    # "temp":Ljava/lang/String;
    .restart local v4    # "visible":[I
    :cond_4
    aput v7, v4, v1

    goto :goto_3

    .line 985
    :cond_5
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v5, "###,###"

    invoke-direct {v0, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 986
    .local v0, "currency":Ljava/text/DecimalFormat;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 989
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsProjectK:Z

    if-eqz v5, :cond_7

    .line 990
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepContainer:Landroid/widget/LinearLayout;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    int-to-long v7, v7

    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f090bb4

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 996
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCurrentStepsStatus()V

    .line 998
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setCaloriesText()V

    .line 999
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    .line 1000
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnit:Ljava/lang/String;

    if-eqz v5, :cond_6

    .line 1001
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDistanceValueAndUnitText()V

    .line 1003
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setMedals()V

    goto/16 :goto_0

    .line 993
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentStepContainer:Landroid/widget/LinearLayout;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mShowedSteps:I

    int-to-long v7, v7

    invoke-virtual {v0, v7, v8}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method
