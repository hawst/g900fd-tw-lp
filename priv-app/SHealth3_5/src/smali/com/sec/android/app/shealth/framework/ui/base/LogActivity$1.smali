.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$1;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 21

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-direct/range {p0 .. p0}, Ljava/util/HashMap;-><init>()V

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x10

    aput v19, v3, v18

    const/16 v18, -0x66

    aput v18, v3, v17

    const/16 v17, -0x66e9

    aput v17, v3, v16

    const/16 v16, -0xa

    aput v16, v3, v15

    const/16 v15, -0x25

    aput v15, v3, v14

    const/16 v14, -0x3f

    aput v14, v3, v13

    const/16 v13, -0x38c3

    aput v13, v3, v12

    const/16 v12, -0x4d

    aput v12, v3, v11

    const/16 v11, -0x9ce

    aput v11, v3, v10

    const/16 v10, -0x66

    aput v10, v3, v9

    const/16 v9, -0x67

    aput v9, v3, v8

    const/16 v8, -0x5fdc

    aput v8, v3, v7

    const/4 v7, -0x1

    aput v7, v3, v6

    const/16 v6, -0xb

    aput v6, v3, v5

    const/16 v5, -0x7ef1

    aput v5, v3, v4

    const/16 v4, -0x13

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x80

    aput v20, v2, v19

    const/16 v19, -0x11

    aput v19, v2, v18

    const/16 v18, -0x6699

    aput v18, v2, v17

    const/16 v17, -0x67

    aput v17, v2, v16

    const/16 v16, -0x55

    aput v16, v2, v15

    const/16 v15, -0x62

    aput v15, v2, v14

    const/16 v14, -0x38a8

    aput v14, v2, v13

    const/16 v13, -0x39

    aput v13, v2, v12

    const/16 v12, -0x9a9

    aput v12, v2, v11

    const/16 v11, -0xa

    aput v11, v2, v10

    const/4 v10, -0x4

    aput v10, v2, v9

    const/16 v9, -0x5fc0

    aput v9, v2, v8

    const/16 v8, -0x60

    aput v8, v2, v7

    const/16 v7, -0x6e

    aput v7, v2, v6

    const/16 v6, -0x7ea0

    aput v6, v2, v5

    const/16 v5, -0x7f

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v5, v2

    if-lt v4, v5, :cond_0

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v5, v2

    if-lt v4, v5, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteDialogButtonController;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteDialogButtonController;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$1;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
