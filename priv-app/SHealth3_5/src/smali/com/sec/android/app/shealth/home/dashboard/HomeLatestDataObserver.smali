.class public Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;
.super Landroid/database/ContentObserver;
.source "HomeLatestDataObserver.java"


# static fields
.field private static CONTENT_URI:[Landroid/net/Uri;

.field private static mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "listener"    # Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 27
    sput-object p2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    .line 28
    return-void
.end method

.method public static getTotalNumberOfUri()I
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    array-length v0, v0

    return v0
.end method

.method public static registerObserver(Landroid/content/Context;[Landroid/database/ContentObserver;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "arrayOb"    # [Landroid/database/ContentObserver;

    .prologue
    .line 66
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 67
    :cond_0
    const-string v6, "HomeLatestDataObserver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "registerObserver Fail!! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :goto_0
    return-void

    .line 71
    :cond_1
    const/4 v1, 0x0

    .line 72
    .local v1, "cnt":I
    move-object v0, p1

    .local v0, "arr$":[Landroid/database/ContentObserver;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v2, v1

    .end local v1    # "cnt":I
    .local v2, "cnt":I
    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v5, v0, v3

    .line 73
    .local v5, "ob":Landroid/database/ContentObserver;
    invoke-static {}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->getTotalNumberOfUri()I

    move-result v6

    if-lt v2, v6, :cond_3

    .line 77
    .end local v5    # "ob":Landroid/database/ContentObserver;
    :cond_2
    const-string v6, "HomeLatestDataObserver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "registerObserver SuccessobArray : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, p1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 75
    .restart local v5    # "ob":Landroid/database/ContentObserver;
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "cnt":I
    .restart local v1    # "cnt":I
    aget-object v7, v7, v2

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 72
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "cnt":I
    .restart local v2    # "cnt":I
    goto :goto_1
.end method

.method public static unregisterObserver(Landroid/content/Context;[Landroid/database/ContentObserver;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "arrayOb"    # [Landroid/database/ContentObserver;

    .prologue
    .line 81
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 82
    :cond_0
    const-string v5, "HomeLatestDataObserver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unregisterObserver Fail!! "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :goto_0
    return-void

    .line 86
    :cond_1
    const/4 v1, 0x0

    .line 87
    .local v1, "cnt":I
    move-object v0, p1

    .local v0, "arr$":[Landroid/database/ContentObserver;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 88
    .local v4, "ob":Landroid/database/ContentObserver;
    invoke-static {}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->getTotalNumberOfUri()I

    move-result v5

    if-lt v1, v5, :cond_3

    .line 93
    .end local v4    # "ob":Landroid/database/ContentObserver;
    :cond_2
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    .line 94
    const-string v5, "HomeLatestDataObserver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unregisterContentObserver SuccessobArray : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    .restart local v4    # "ob":Landroid/database/ContentObserver;
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 91
    add-int/lit8 v1, v1, 0x1

    .line 87
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onChange(Z)V
    .locals 0
    .param p1, "selfChange"    # Z

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 34
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    if-eqz p2, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    invoke-interface {v0, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;->onDataChanged(I)V

    .line 58
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    goto :goto_0

    .line 44
    :cond_3
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 45
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    invoke-interface {v0, v4}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;->onDataChanged(I)V

    goto :goto_1

    .line 46
    :cond_4
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    const/16 v1, 0x15

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;->onDataChanged(I)V

    goto :goto_1

    .line 48
    :cond_5
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    aget-object v2, v2, v5

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;->onDataChanged(I)V

    goto/16 :goto_1

    .line 50
    :cond_6
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    invoke-interface {v0, v5}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;->onDataChanged(I)V

    goto/16 :goto_1

    .line 52
    :cond_7
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;->onDataChanged(I)V

    goto/16 :goto_1

    .line 54
    :cond_8
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->CONTENT_URI:[Landroid/net/Uri;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataObserver;->mHomeLatestDataListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataListener;->onDataChanged(I)V

    goto/16 :goto_1
.end method
