.class public abstract Lcom/sec/android/app/shealth/uv/state/UvState;
.super Ljava/lang/Object;
.source "UvState.java"


# instance fields
.field protected mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0
    .param p1, "uvSummaryFragment"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/state/UvState;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .line 13
    return-void
.end method


# virtual methods
.method public abstract clearForNextState()V
.end method

.method public abstract reStartStates()V
.end method

.method public abstract terminateState()V
.end method

.method public abstract updateNextState(II)V
.end method

.method public abstract updateViewWithAnimation(Z)V
.end method
