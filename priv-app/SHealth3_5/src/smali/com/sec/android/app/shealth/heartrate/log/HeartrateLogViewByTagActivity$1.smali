.class Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;
.super Landroid/database/ContentObserver;
.source "HeartrateLogViewByTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    iget v0, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tag:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "tagName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mtagName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->access$002(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    iget v2, v2, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->tagIdInfo:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->executeTagQuery(I)Landroid/database/Cursor;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mCursor:Landroid/database/Cursor;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->access$102(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 51
    const-string v0, "cursor Size"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->refreshAdapter()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->updateDetailsLayout()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity$1;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->mHeartrateLogAdapter:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogViewByTagActivity;)Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogAdapter;->notifyDataSetChanged()V

    .line 56
    return-void
.end method
