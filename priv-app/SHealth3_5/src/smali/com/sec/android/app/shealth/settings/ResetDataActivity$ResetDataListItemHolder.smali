.class Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;
.super Ljava/lang/Object;
.source "ResetDataActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/settings/ResetDataActivity$ItemViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ResetDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResetDataListItemHolder"
.end annotation


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mItemContainer:Landroid/widget/RelativeLayout;

.field private mMainTextView:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity$1;

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemCheckBox()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public getItemContainer()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->mItemContainer:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getMainTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->mMainTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public setItemCheckBox(Landroid/widget/CheckBox;)V
    .locals 0
    .param p1, "checkBox"    # Landroid/widget/CheckBox;

    .prologue
    .line 507
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->mCheckBox:Landroid/widget/CheckBox;

    .line 508
    return-void
.end method

.method public setItemContainer(Landroid/widget/RelativeLayout;)V
    .locals 0
    .param p1, "itemContainer"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 517
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->mItemContainer:Landroid/widget/RelativeLayout;

    .line 518
    return-void
.end method

.method public setMainTextView(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 497
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->mMainTextView:Landroid/widget/TextView;

    .line 498
    return-void
.end method
