.class Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;
.super Ljava/lang/Object;
.source "InitSetProfileCard.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 279
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->access$100(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationEnable(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isDefaultBirthDateSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090817

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->mFirstView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->resetFlag()V

    .line 286
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->saveProfile()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->access$300(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard$2;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;)V

    goto :goto_0
.end method
