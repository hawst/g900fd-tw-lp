.class Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;
.super Ljava/lang/Object;
.source "BaseFoodSearchApi.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->performBarcodeRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

.field final synthetic val$request:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;->val$request:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;->val$request:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->performBarcodeSearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v1

    .line 248
    .local v1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
    if-nez v1, :cond_0

    .line 253
    :goto_0
    return-object v1

    .line 251
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getRequest()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;->getFoodInfoData()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    .line 252
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->updateCategoriesData(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    invoke-static {v2, v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    goto :goto_0
.end method

.method public bridge synthetic run()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$7;->run()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    move-result-object v0

    return-object v0
.end method
