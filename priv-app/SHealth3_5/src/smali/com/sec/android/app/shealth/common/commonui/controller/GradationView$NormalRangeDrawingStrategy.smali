.class Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;
.super Ljava/lang/Object;
.source "GradationView.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NormalRangeDrawingStrategy"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getBottomLineHeight()I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawGradationLines(Landroid/graphics/Canvas;I)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$000(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Landroid/graphics/Canvas;I)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawBottomLine(Landroid/graphics/Canvas;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Landroid/graphics/Canvas;)V

    .line 146
    return-void
.end method
