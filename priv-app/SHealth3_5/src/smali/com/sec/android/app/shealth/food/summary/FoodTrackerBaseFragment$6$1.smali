.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V
    .locals 0

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1024
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    const v2, 0x7f08047e

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1025
    .local v0, "calorieEdit":Landroid/widget/EditText;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1027
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 1028
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getEmptyFieldListener()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$EmptyFieldListener;->processEmptyField()V

    .line 1038
    :goto_0
    return-void

    .line 1031
    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 1032
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 1034
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 1036
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6$1;->this$1:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->showMealTypeSelectorPopup()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;->access$1500(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$6;)V

    goto :goto_0
.end method
