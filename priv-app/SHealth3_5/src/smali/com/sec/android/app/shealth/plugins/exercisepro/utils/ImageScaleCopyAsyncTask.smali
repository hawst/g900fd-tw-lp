.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;
.super Landroid/os/AsyncTask;
.source "ImageScaleCopyAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final COMPRESS_QUALITY:I = 0x50

.field private static final DEFAULT_MAX_IMAGE_SIZE:I = 0x400

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final SIZE_NOT_INITED:F = -1.0f


# instance fields
.field private mInputImagePath:Ljava/lang/String;

.field private mMaxSize:I

.field private final mOutputImagePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "inputImagePath"    # Ljava/lang/String;
    .param p2, "outputImagePath"    # Ljava/lang/String;

    .prologue
    .line 66
    const/16 v0, 0x400

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "inputImagePath"    # Ljava/lang/String;
    .param p2, "outputImagePath"    # Ljava/lang/String;
    .param p3, "maxSize"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 54
    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private copyFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "inputFilePath"    # Ljava/lang/String;
    .param p2, "outputFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 158
    const/4 v1, 0x0

    .line 159
    .local v1, "src":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 161
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 162
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 163
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    new-array v2, v9, [Ljava/io/Closeable;

    aput-object v1, v2, v8

    aput-object v0, v2, v7

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    move v2, v7

    :goto_0
    return v2

    .line 165
    :catch_0
    move-exception v6

    .line 166
    .local v6, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    new-array v2, v9, [Ljava/io/Closeable;

    aput-object v1, v2, v8

    aput-object v0, v2, v7

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    move v2, v8

    goto :goto_0

    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    new-array v3, v9, [Ljava/io/Closeable;

    aput-object v1, v3, v8

    aput-object v0, v3, v7

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    throw v2
.end method

.method private getScaledSize(FFI)Landroid/graphics/PointF;
    .locals 3
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "maxSize"    # I

    .prologue
    .line 174
    int-to-float v2, p3

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    int-to-float v2, p3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    .line 175
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 188
    :goto_0
    return-object v2

    .line 178
    :cond_0
    cmpl-float v2, p1, p2

    if-lez v2, :cond_1

    .line 179
    int-to-float v1, p3

    .line 180
    .local v1, "scaledWidth":F
    div-float v2, v1, p1

    mul-float v0, p2, v2

    .line 188
    .local v0, "scaledHeight":F
    :goto_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0

    .line 181
    .end local v0    # "scaledHeight":F
    .end local v1    # "scaledWidth":F
    :cond_1
    cmpg-float v2, p1, p2

    if-gez v2, :cond_2

    .line 182
    int-to-float v0, p3

    .line 183
    .restart local v0    # "scaledHeight":F
    div-float v2, v0, p2

    mul-float v1, p1, v2

    .restart local v1    # "scaledWidth":F
    goto :goto_1

    .line 185
    .end local v0    # "scaledHeight":F
    .end local v1    # "scaledWidth":F
    :cond_2
    int-to-float v1, p3

    .line 186
    .restart local v1    # "scaledWidth":F
    int-to-float v0, p3

    .restart local v0    # "scaledHeight":F
    goto :goto_1
.end method

.method private resizeAndTransformBitmap(FFFF)Ljava/lang/Boolean;
    .locals 17
    .param p1, "dstWidth"    # F
    .param p2, "dstHeight"    # F
    .param p3, "inWidth"    # F
    .param p4, "inHeight"    # F

    .prologue
    .line 102
    const/4 v3, 0x0

    .line 103
    .local v3, "in":Ljava/io/InputStream;
    const/4 v8, 0x0

    .line 105
    .local v8, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    invoke-direct {v4, v14}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    .end local v3    # "in":Ljava/io/InputStream;
    .local v4, "in":Ljava/io/InputStream;
    :try_start_1
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 107
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    div-float v14, p3, p1

    div-float v15, p4, p2

    invoke-static {v14, v15}, Ljava/lang/Math;->max(FF)F

    move-result v14

    float-to-int v14, v14

    iput v14, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 108
    const/4 v14, 0x0

    invoke-static {v4, v14, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 123
    .local v11, "roughBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v14, v14

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v13

    .line 124
    .local v13, "roughtWidth":I
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    int-to-float v14, v14

    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v12

    .line 125
    .local v12, "roughtHeight":I
    const/4 v14, 0x1

    invoke-static {v11, v13, v12, v14}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 126
    .local v10, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v9, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    invoke-direct {v9, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 127
    .end local v8    # "out":Ljava/io/FileOutputStream;
    .local v9, "out":Ljava/io/FileOutputStream;
    :try_start_2
    sget-object v14, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v15, 0x50

    invoke-virtual {v10, v14, v15, v9}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 129
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 130
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 132
    new-instance v6, Landroid/media/ExifInterface;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    invoke-direct {v6, v14}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 133
    .local v6, "oldExif":Landroid/media/ExifInterface;
    const-string v14, "Orientation"

    invoke-virtual {v6, v14}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "exifOrientation":Ljava/lang/String;
    new-instance v5, Landroid/media/ExifInterface;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    invoke-direct {v5, v14}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 136
    .local v5, "newExif":Landroid/media/ExifInterface;
    if-eqz v2, :cond_0

    .line 137
    const-string v14, "Orientation"

    invoke-virtual {v5, v14, v2}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    invoke-virtual {v5}, Landroid/media/ExifInterface;->saveAttributes()V

    .line 140
    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v14

    .line 145
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/io/Closeable;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    const/16 v16, 0x1

    aput-object v9, v15, v16

    invoke-static {v15}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    move-object v8, v9

    .end local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v8    # "out":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v2    # "exifOrientation":Ljava/lang/String;
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "newExif":Landroid/media/ExifInterface;
    .end local v6    # "oldExif":Landroid/media/ExifInterface;
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v10    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "roughBitmap":Landroid/graphics/Bitmap;
    .end local v12    # "roughtHeight":I
    .end local v13    # "roughtWidth":I
    .restart local v3    # "in":Ljava/io/InputStream;
    :goto_0
    return-object v14

    .line 141
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    sget-object v14, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    invoke-static {v14, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v14

    .line 145
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/io/Closeable;

    const/16 v16, 0x0

    aput-object v3, v15, v16

    const/16 v16, 0x1

    aput-object v8, v15, v16

    invoke-static {v15}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    goto :goto_0

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    :goto_2
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/io/Closeable;

    const/16 v16, 0x0

    aput-object v3, v15, v16

    const/16 v16, 0x1

    aput-object v8, v15, v16

    invoke-static {v15}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    throw v14

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v14

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v8    # "out":Ljava/io/FileOutputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v10    # "resizedBitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "roughBitmap":Landroid/graphics/Bitmap;
    .restart local v12    # "roughtHeight":I
    .restart local v13    # "roughtWidth":I
    :catchall_2
    move-exception v14

    move-object v8, v9

    .end local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v8    # "out":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_2

    .line 141
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v10    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "roughBitmap":Landroid/graphics/Bitmap;
    .end local v12    # "roughtHeight":I
    .end local v13    # "roughtWidth":I
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_1

    .end local v3    # "in":Ljava/io/InputStream;
    .end local v8    # "out":Ljava/io/FileOutputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v10    # "resizedBitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "roughBitmap":Landroid/graphics/Bitmap;
    .restart local v12    # "roughtHeight":I
    .restart local v13    # "roughtWidth":I
    :catch_2
    move-exception v1

    move-object v8, v9

    .end local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v8    # "out":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 11
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    .line 71
    const/high16 v2, -0x40800000    # -1.0f

    .line 72
    .local v2, "dstWidth":F
    const/high16 v0, -0x40800000    # -1.0f

    .line 73
    .local v0, "dstHeight":F
    const/high16 v7, -0x40800000    # -1.0f

    .line 74
    .local v7, "inWidth":F
    const/high16 v6, -0x40800000    # -1.0f

    .line 75
    .local v6, "inHeight":F
    const/4 v4, 0x0

    .line 77
    .local v4, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    invoke-direct {v5, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    .end local v4    # "in":Ljava/io/InputStream;
    .local v5, "in":Ljava/io/InputStream;
    :try_start_1
    new-instance v8, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v8}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 79
    .local v8, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v9, 0x1

    iput-boolean v9, v8, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 80
    const/4 v9, 0x0

    invoke-static {v5, v9, v8}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 82
    iget v9, v8, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v7, v9

    .line 83
    iget v9, v8, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v6, v9

    .line 85
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    int-to-float v9, v9

    cmpg-float v9, v7, v9

    if-gtz v9, :cond_0

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    int-to-float v9, v9

    cmpg-float v9, v6, v9

    if-gtz v9, :cond_0

    .line 86
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    invoke-direct {p0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    .line 96
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    move-object v4, v5

    .line 98
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v8    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v4    # "in":Ljava/io/InputStream;
    :goto_0
    return-object v9

    .line 89
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v8    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    :try_start_2
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    invoke-direct {p0, v7, v6, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->getScaledSize(FFI)Landroid/graphics/PointF;

    move-result-object v1

    .line 90
    .local v1, "dstSize":Landroid/graphics/PointF;
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 91
    iget v0, v1, Landroid/graphics/PointF;->y:F
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 96
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    .line 98
    invoke-direct {p0, v2, v0, v7, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->resizeAndTransformBitmap(FFFF)Ljava/lang/Boolean;

    move-result-object v9

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_0

    .line 92
    .end local v1    # "dstSize":Landroid/graphics/PointF;
    .end local v8    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v3

    .line 93
    .local v3, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    invoke-static {v9, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v9

    .line 96
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_2
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    throw v9

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_2

    .line 92
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v3

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getOutputImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    return-object v0
.end method
