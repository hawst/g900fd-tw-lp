.class Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->initialize(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 150
    .local v0, "rootView":Landroid/view/View;
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "rootView":Landroid/view/View;
    # setter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$002(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 158
    return-void
.end method
