.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$LoadLatestDataThread;
.super Ljava/lang/Object;
.source "CignaDashBoardPlainWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoadLatestDataThread"
.end annotation


# instance fields
.field mWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;)V
    .locals 0
    .param p1, "cignaDashBoardPlainWidget"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$LoadLatestDataThread;->mWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    .line 94
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "COACH_WIDGET"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->initializeWidgetInfo(Landroid/content/Context;Ljava/lang/String;)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget$LoadLatestDataThread;->mWidget:Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->loadLatestData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;->access$000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaDashBoardPlainWidget;)V

    .line 103
    return-void
.end method
