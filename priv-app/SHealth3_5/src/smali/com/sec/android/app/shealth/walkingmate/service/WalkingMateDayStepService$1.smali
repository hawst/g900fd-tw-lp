.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;
.super Ljava/lang/Object;
.source "WalkingMateDayStepService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

.field final synthetic val$isStepCounting:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;->val$isStepCounting:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 227
    :try_start_0
    sget v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    const/16 v2, 0x2719

    if-ne v1, v2, :cond_0

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;->val$isStepCounting:Z

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->loadLatestData(ZZ)V

    .line 236
    :goto_0
    return-void

    .line 230
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;->val$isStepCounting:Z

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->loadLatestData(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "OPTIMIZE-DAYSTEP"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
