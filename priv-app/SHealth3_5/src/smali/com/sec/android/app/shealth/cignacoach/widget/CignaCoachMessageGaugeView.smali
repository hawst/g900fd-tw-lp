.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;
.super Landroid/widget/LinearLayout;
.source "CignaCoachMessageGaugeView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMarkerHeight:F

.field private mMarkerWidth:F

.field private mMarkingImg:Landroid/widget/ImageView;

.field private mStepGap:F

.field private mStepWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mContext:Landroid/content/Context;

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->initLayout()V

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepWidth:F

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepGap:F

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mMarkerWidth:F

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mMarkerHeight:F

    .line 35
    return-void
.end method

.method private getMargin(I)I
    .locals 5
    .param p1, "score"    # I

    .prologue
    const/high16 v4, 0x41f00000    # 30.0f

    .line 59
    const/4 v0, 0x0

    .line 61
    .local v0, "margin":I
    const/16 v1, 0x1e

    if-gt p1, v1, :cond_1

    .line 63
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepWidth:F

    int-to-float v2, p1

    mul-float/2addr v1, v2

    div-float/2addr v1, v4

    float-to-int v0, v1

    .line 74
    :goto_0
    if-nez p1, :cond_0

    .line 75
    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v0, v1

    .line 78
    :cond_0
    return v0

    .line 65
    :cond_1
    const/16 v1, 0x46

    if-gt p1, v1, :cond_2

    .line 67
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepWidth:F

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepGap:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepWidth:F

    add-int/lit8 v3, p1, -0x1e

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42200000    # 40.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v0, v1

    goto :goto_0

    .line 71
    :cond_2
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepWidth:F

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepGap:F

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mStepWidth:F

    add-int/lit8 v3, p1, -0x46

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    float-to-int v0, v1

    goto :goto_0
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mContext:Landroid/content/Context;

    const v1, 0x7f030030

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 48
    const v0, 0x7f080113

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mMarkingImg:Landroid/widget/ImageView;

    .line 49
    return-void
.end method


# virtual methods
.method public setScore(I)V
    .locals 3
    .param p1, "score"    # I

    .prologue
    .line 52
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mMarkerWidth:F

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mMarkerHeight:F

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 53
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->getMargin(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->mMarkingImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    return-void
.end method
