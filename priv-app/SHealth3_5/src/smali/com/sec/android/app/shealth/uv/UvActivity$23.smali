.class Lcom/sec/android/app/shealth/uv/UvActivity$23;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

.field final synthetic val$ImageArr:[Ljava/lang/String;

.field final synthetic val$forever:Z

.field final synthetic val$imageIndex:I

.field final synthetic val$imageView1:Landroid/widget/ImageView;

.field final synthetic val$imageView2:Landroid/widget/ImageView;

.field final synthetic val$images:[I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[II[Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1048
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView1:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView2:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$images:[I

    iput p5, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageIndex:I

    iput-object p6, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$ImageArr:[Ljava/lang/String;

    iput-boolean p7, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$forever:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 7
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x4

    .line 1050
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvActivity;->animationFadeOut:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_2

    .line 1051
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage1Found:Z

    if-eqz v0, :cond_0

    .line 1052
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1054
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/uv/UvActivity;->isImage2Found:Z

    if-eqz v0, :cond_1

    .line 1055
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1057
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$images:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageIndex:I

    if-le v0, v1, :cond_3

    .line 1059
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView1:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView2:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$images:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$ImageArr:[Ljava/lang/String;

    iget v5, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageIndex:I

    add-int/lit8 v5, v5, 0x1

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$forever:Z

    # invokes: Lcom/sec/android/app/shealth/uv/UvActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$3000(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    .line 1067
    :cond_2
    :goto_0
    return-void

    .line 1062
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$forever:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1063
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView1:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$imageView2:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$images:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$ImageArr:[Ljava/lang/String;

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/uv/UvActivity$23;->val$forever:Z

    # invokes: Lcom/sec/android/app/shealth/uv/UvActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$3000(Lcom/sec/android/app/shealth/uv/UvActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1070
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1073
    return-void
.end method
