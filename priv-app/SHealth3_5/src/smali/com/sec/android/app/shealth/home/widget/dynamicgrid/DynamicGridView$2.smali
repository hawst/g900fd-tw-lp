.class Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # getter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1200(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$2;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    # getter for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mUserItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1200(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 116
    :cond_0
    return-void
.end method
