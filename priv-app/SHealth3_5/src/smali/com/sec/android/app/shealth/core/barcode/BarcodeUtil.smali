.class public Lcom/sec/android/app/shealth/core/barcode/BarcodeUtil;
.super Ljava/lang/Object;
.source "BarcodeUtil.java"


# static fields
.field public static final QR_CODE_RESULT:I = 0x101


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBarcodeType(I)Ljava/lang/String;
    .locals 1
    .param p0, "scanningResult"    # I

    .prologue
    .line 29
    const/4 v0, 0x0

    .line 30
    .local v0, "strBarcodeType":Ljava/lang/String;
    sparse-switch p0, :sswitch_data_0

    .line 107
    const/4 v0, 0x0

    .line 109
    :goto_0
    return-object v0

    .line 32
    :sswitch_0
    const-string v0, "BCS_QR_CODE"

    .line 33
    goto :goto_0

    .line 35
    :sswitch_1
    const-string v0, "BCS_DM"

    .line 36
    goto :goto_0

    .line 38
    :sswitch_2
    const-string v0, "BCS_PDF417"

    .line 39
    goto :goto_0

    .line 41
    :sswitch_3
    const-string v0, "BCS_CODABAR"

    .line 42
    goto :goto_0

    .line 44
    :sswitch_4
    const-string v0, "BCS_CODE128"

    .line 45
    goto :goto_0

    .line 47
    :sswitch_5
    const-string v0, "BCS_CODE39"

    .line 48
    goto :goto_0

    .line 50
    :sswitch_6
    const-string v0, "BCS_CODE39_EXTEND"

    .line 51
    goto :goto_0

    .line 53
    :sswitch_7
    const-string v0, "BCS_CODE39_HIBC"

    .line 54
    goto :goto_0

    .line 56
    :sswitch_8
    const-string v0, "BCS_CODE93"

    .line 57
    goto :goto_0

    .line 59
    :sswitch_9
    const-string v0, "BCS_DATALOGIC25"

    .line 60
    goto :goto_0

    .line 62
    :sswitch_a
    const-string v0, "BCS_EAN13"

    .line 63
    goto :goto_0

    .line 65
    :sswitch_b
    const-string v0, "BCS_EAN2"

    .line 66
    goto :goto_0

    .line 68
    :sswitch_c
    const-string v0, "BCS_EAN5"

    .line 69
    goto :goto_0

    .line 71
    :sswitch_d
    const-string v0, "BCS_EAN8"

    .line 72
    goto :goto_0

    .line 74
    :sswitch_e
    const-string v0, "BCS_IATA25"

    .line 75
    goto :goto_0

    .line 77
    :sswitch_f
    const-string v0, "BCS_ITF25"

    .line 78
    goto :goto_0

    .line 80
    :sswitch_10
    const-string v0, "BCS_MSI"

    .line 81
    goto :goto_0

    .line 83
    :sswitch_11
    const-string v0, "BCS_PLESSEY"

    .line 84
    goto :goto_0

    .line 86
    :sswitch_12
    const-string v0, "BCS_POSTNET"

    .line 87
    goto :goto_0

    .line 89
    :sswitch_13
    const-string v0, "BCS_POSTNET32"

    .line 90
    goto :goto_0

    .line 92
    :sswitch_14
    const-string v0, "BCS_POSTNET52"

    .line 93
    goto :goto_0

    .line 95
    :sswitch_15
    const-string v0, "BCS_POSTNET62"

    .line 96
    goto :goto_0

    .line 98
    :sswitch_16
    const-string v0, "BCS_EANUCC128"

    .line 99
    goto :goto_0

    .line 101
    :sswitch_17
    const-string v0, "BCS_UPCA"

    .line 102
    goto :goto_0

    .line 104
    :sswitch_18
    const-string v0, "BCS_UPCE"

    .line 105
    goto :goto_0

    .line 30
    nop

    :sswitch_data_0
    .sparse-switch
        0x101 -> :sswitch_0
        0x102 -> :sswitch_1
        0x103 -> :sswitch_2
        0x1001 -> :sswitch_3
        0x1002 -> :sswitch_4
        0x1003 -> :sswitch_5
        0x1004 -> :sswitch_6
        0x1005 -> :sswitch_7
        0x1006 -> :sswitch_8
        0x1007 -> :sswitch_9
        0x1008 -> :sswitch_a
        0x1009 -> :sswitch_b
        0x100a -> :sswitch_c
        0x100b -> :sswitch_d
        0x100c -> :sswitch_e
        0x100d -> :sswitch_f
        0x100e -> :sswitch_10
        0x100f -> :sswitch_11
        0x1010 -> :sswitch_12
        0x1011 -> :sswitch_13
        0x1012 -> :sswitch_14
        0x1013 -> :sswitch_15
        0x1014 -> :sswitch_16
        0x1015 -> :sswitch_17
        0x1016 -> :sswitch_18
    .end sparse-switch
.end method

.method public static onlyDigits(Ljava/lang/String;)Z
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 113
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 114
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    const/4 v1, 0x0

    .line 118
    :goto_1
    return v1

    .line 113
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
