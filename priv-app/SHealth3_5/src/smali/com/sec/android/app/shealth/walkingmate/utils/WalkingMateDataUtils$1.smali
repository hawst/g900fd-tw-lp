.class final Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$1;
.super Ljava/lang/Object;
.source "WalkingMateDataUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getWalkInfoDataForHourChart(I)Ljava/util/Vector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)I
    .locals 4
    .param p1, "lhs"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;
    .param p2, "rhs"    # Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .prologue
    .line 282
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$000(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)J

    move-result-wide v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J
    invoke-static {p2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$000(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 283
    const/4 v0, -0x1

    .line 287
    :goto_0
    return v0

    .line 284
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$000(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)J

    move-result-wide v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->mStartTime:J
    invoke-static {p2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;->access$000(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 285
    const/4 v0, 0x1

    goto :goto_0

    .line 287
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 279
    check-cast p1, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$1;->compare(Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils$ChartData;)I

    move-result v0

    return v0
.end method
