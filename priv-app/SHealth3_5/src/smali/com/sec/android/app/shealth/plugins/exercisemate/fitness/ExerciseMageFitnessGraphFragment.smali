.class public abstract Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "ExerciseMageFitnessGraphFragment.java"


# instance fields
.field private mChangePeriodListener:Landroid/view/View$OnClickListener;

.field private mGeneralViewContainer:Landroid/widget/LinearLayout;

.field private mGraphViewHeader:Landroid/view/View;

.field private mIfGraphModified:Z

.field protected mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

.field private mInformationContainer:Landroid/widget/LinearLayout;

.field private mLegendContainer:Landroid/widget/LinearLayout;

.field private mNoDataContainer:Landroid/widget/LinearLayout;

.field private mPeriodChangeButtonsArray:[Landroid/view/View;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mRootView:Landroid/view/View;

.field private mSicGraphView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->changePeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    return-void
.end method

.method private changePeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p1, "newPeriodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mIfGraphModified:Z

    .line 193
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 194
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initGeneralView()V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;->dimInformationAreaView()V

    .line 197
    return-void
.end method

.method private initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020a

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 91
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const v4, 0x7f0804d1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 92
    .local v1, "changePeriodButtonHour":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const v4, 0x7f0804d2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "changePeriodButtonDay":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const v4, 0x7f0804d3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 94
    .local v2, "changePeriodButtonWeek":Landroid/view/View;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090212

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 95
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090213

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 96
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090211

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f09020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 98
    new-array v3, v9, [Landroid/view/View;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    aput-object v2, v3, v8

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    .line 99
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mChangePeriodListener:Landroid/view/View$OnClickListener;

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mChangePeriodListener:Landroid/view/View$OnClickListener;

    new-array v4, v9, [Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v6

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v7

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v8

    aput-object v5, v4, v8

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->setCommonOnClickListenerForViews(Landroid/view/View$OnClickListener;[Landroid/view/View;)V

    .line 126
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;)V

    new-array v4, v9, [Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v6

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v7

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v5, v5, v8

    aput-object v5, v4, v8

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->setCommonFocusChangeListenerForViews(Landroid/view/View$OnFocusChangeListener;[Landroid/view/View;)V

    .line 141
    return-void
.end method

.method private initGeneralView()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 163
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mSicGraphView:Landroid/view/View;

    .line 164
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 165
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->selectPeriodButton(I)V

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mSicGraphView:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    return-void
.end method

.method private initInformationArea()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 173
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    .line 174
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 175
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    :cond_0
    return-void
.end method

.method private initLegendArea()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initGraphLegendArea()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 184
    .local v0, "legendArea":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 185
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v0, :cond_0

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 188
    :cond_0
    return-void
.end method

.method private selectPeriodButton(I)V
    .locals 3
    .param p1, "newPeriodType"    # I

    .prologue
    .line 201
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v1, v0

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 205
    :cond_1
    return-void
.end method

.method private varargs setCommonFocusChangeListenerForViews(Landroid/view/View$OnFocusChangeListener;[Landroid/view/View;)V
    .locals 4
    .param p1, "commonFocusChangeListener"    # Landroid/view/View$OnFocusChangeListener;
    .param p2, "viewsWhichWillGetCommonFocusChangeListener"    # [Landroid/view/View;

    .prologue
    .line 153
    move-object v0, p2

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 155
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, p1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private varargs setCommonOnClickListenerForViews(Landroid/view/View$OnClickListener;[Landroid/view/View;)V
    .locals 4
    .param p1, "commonOnClickListener"    # Landroid/view/View$OnClickListener;
    .param p2, "viewsWhichWillGetCommonOnClickListener"    # [Landroid/view/View;

    .prologue
    .line 145
    move-object v0, p2

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 147
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract initGraphLegendArea()Landroid/view/View;
.end method

.method protected abstract initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
.end method

.method protected abstract initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f0804cf

    .line 39
    const v0, 0x7f0300b9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f08036d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f08036c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f080371

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGraphViewHeader:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f08036e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGraphViewHeader:Landroid/view/View;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGraphViewHeader:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initDateBar()V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initGeneralView()V

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initInformationArea()V

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->initLegendArea()V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method protected showNoData(I)V
    .locals 3
    .param p1, "noDataImageResId"    # I

    .prologue
    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMageFitnessGraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f08036f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 223
    .local v0, "noDataImg":Landroid/widget/ImageView;
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 225
    return-void
.end method
