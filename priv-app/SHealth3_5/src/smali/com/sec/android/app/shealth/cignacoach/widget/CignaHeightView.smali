.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
.super Landroid/widget/RelativeLayout;
.source "CignaHeightView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;,
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;
    }
.end annotation


# static fields
.field private static final CM:I


# instance fields
.field private inputFilter:Landroid/text/InputFilter;

.field private mContext:Landroid/content/Context;

.field private mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

.field private mEditTextFocusHandler:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;

.field private mEditTextWatcher:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;

.field private mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

.field private mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

.field private mHeightUnit:I

.field private mHeightValue:F

.field private mInputFilters:[Landroid/text/InputFilter;

.field private mInputSecondFilters:[Landroid/text/InputFilter;

.field private mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

.field private mMaxValue:F

.field private mMinValue:F

.field private mPrimeTextView:[Landroid/widget/TextView;

.field private mSystemNumberSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    .line 39
    const v0, 0x4479f99a    # 999.9f

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMaxValue:F

    .line 40
    const/high16 v0, 0x43160000    # 150.0f

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    .line 43
    const v0, 0x249f1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    .line 50
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    .line 511
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->inputFilter:Landroid/text/InputFilter;

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mContext:Landroid/content/Context;

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mEditTextWatcher:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;

    .line 66
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mEditTextFocusHandler:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->initLayout()V

    .line 72
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->isHeightValidValue()F

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->isHeighUnitCM()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightTextValue()F

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setDatalimit(Ljava/lang/String;I)V

    return-void
.end method

.method private convertComma(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 217
    :cond_0
    return-object p1
.end method

.method public static getFeetandInch(F)Ljava/util/HashMap;
    .locals 5
    .param p0, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 262
    .local v1, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertInchToCm(F)F

    move-result v3

    float-to-int v2, v3

    .line 263
    .local v2, "inch":I
    div-int/lit8 v0, v2, 0xc

    .line 264
    .local v0, "feet":I
    rem-int/lit8 v2, v2, 0xc

    .line 265
    if-nez v0, :cond_0

    const/16 v3, 0x8

    if-ge v2, v3, :cond_0

    .line 266
    const/16 v2, 0x8

    .line 267
    :cond_0
    mul-int/lit8 v3, v2, 0xa

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v3, v3

    div-int/lit8 v2, v3, 0xa

    .line 268
    const/16 v3, 0xc

    if-ne v2, v3, :cond_1

    .line 269
    const/4 v2, 0x0

    .line 270
    add-int/lit8 v0, v0, 0x1

    .line 272
    :cond_1
    const-string v3, "feet"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    const-string v3, "inch"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    return-object v1
.end method

.method private getHeightTextValue()F
    .locals 7

    .prologue
    const/16 v6, 0x9

    const/high16 v3, -0x40800000    # -1.0f

    .line 531
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    const v5, 0x249f1

    if-ne v4, v5, :cond_2

    .line 533
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 535
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 558
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 538
    .restart local v2    # "value":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    goto :goto_0

    .line 543
    .end local v2    # "value":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 544
    .local v0, "ftString":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 546
    .local v1, "inchString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    .line 547
    :cond_3
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 549
    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_5

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0x8

    if-lt v4, v5, :cond_0

    .line 551
    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v6, :cond_6

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xa

    if-gt v4, v5, :cond_0

    .line 553
    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-gt v4, v6, :cond_0

    .line 555
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xc

    if-ge v4, v5, :cond_0

    .line 558
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    goto :goto_0
.end method

.method private initLayout()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 80
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f030052

    invoke-static {v2, v3, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 82
    .local v1, "view":Landroid/view/View;
    new-array v2, v4, [Landroid/text/InputFilter;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mInputFilters:[Landroid/text/InputFilter;

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mInputFilters:[Landroid/text/InputFilter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->inputFilter:Landroid/text/InputFilter;

    aput-object v3, v2, v5

    .line 85
    new-array v2, v6, [Landroid/text/InputFilter;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->inputFilter:Landroid/text/InputFilter;

    aput-object v3, v2, v5

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v4

    .line 89
    const v2, 0x7f0801a7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 90
    const v2, 0x7f0801a9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mInputFilters:[Landroid/text/InputFilter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mInputSecondFilters:[Landroid/text/InputFilter;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mEditTextWatcher:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mEditTextWatcher:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mEditTextFocusHandler:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mEditTextFocusHandler:Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$EditTextFocusHandler;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setImeOptions(I)V

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setImeOptions(I)V

    .line 101
    new-array v2, v6, [Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    const v2, 0x7f0801a8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v5

    .line 103
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    const v2, 0x7f0801aa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 105
    new-array v0, v6, [Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0900bc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mContext:Landroid/content/Context;

    const v3, 0x7f0900bd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 106
    .local v0, "heightUnits":[Ljava/lang/String;
    const v2, 0x7f0801a5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setDropDownVerticalOffset(I)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    const v3, 0x7f02086f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setPopupBackgroundResource(I)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomBaseSpinnerAdapter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    const v6, 0x7f0301f1

    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomBaseSpinnerAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;I[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 164
    return-void
.end method

.method private isHeighUnitCM()Z
    .locals 2

    .prologue
    .line 508
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    const v1, 0x249f1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isHeightValidValue()F
    .locals 5

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 339
    .local v0, "ftString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 341
    .local v1, "inchString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 349
    :cond_0
    :goto_0
    return v2

    .line 345
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x8

    if-lt v3, v4, :cond_0

    .line 349
    :cond_2
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    goto :goto_0
.end method

.method private setDatalimit(Ljava/lang/String;I)V
    .locals 4
    .param p1, "preData"    # Ljava/lang/String;
    .param p2, "prePos"    # I

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->isHeighUnitCM()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 359
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 360
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    .line 388
    :cond_1
    :goto_0
    return-void

    .line 362
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 364
    .local v0, "data":F
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMaxValue:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 365
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_0

    .line 375
    .end local v0    # "data":F
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 376
    :cond_4
    const/4 v1, 0x0

    .line 381
    .local v1, "ftData":F
    :goto_1
    const/high16 v2, 0x42c60000    # 99.0f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    .line 382
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_0

    .line 378
    .end local v1    # "ftData":F
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-float v1, v2

    .restart local v1    # "ftData":F
    goto :goto_1
.end method


# virtual methods
.method public checkValueText()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 568
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightTextValue()F

    move-result v0

    .line 570
    .local v0, "value":F
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v0, v2

    if-nez v2, :cond_1

    .line 580
    :cond_0
    :goto_0
    return v1

    .line 574
    :cond_1
    const/high16 v2, 0x41a00000    # 20.0f

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    .line 580
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCmValue()F
    .locals 4

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 687
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 689
    .local v0, "value":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    const v3, 0x249f1

    if-ne v2, v3, :cond_1

    .line 690
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 692
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    if-eqz v2, :cond_1

    .line 694
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;->onCignaDialog(Landroid/view/View;)V

    .line 695
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setMinValueText()V

    .line 696
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    .line 707
    :cond_1
    :goto_0
    return v1

    .line 703
    :cond_2
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public getFtToCmValue()F
    .locals 5

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    .line 651
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    const v4, 0x249f2

    if-ne v3, v4, :cond_2

    .line 652
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->isHeightValidValue()F

    move-result v3

    cmpl-float v3, v3, v2

    if-nez v3, :cond_3

    .line 655
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 657
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    if-eqz v3, :cond_0

    .line 659
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;->onCignaDialog(Landroid/view/View;)V

    .line 662
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 664
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setCursorVisible(Z)V

    .line 667
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setMinValueText()V

    .line 668
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    .line 683
    :cond_2
    :goto_0
    return v2

    .line 676
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 677
    .local v0, "feet":F
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 678
    .local v1, "inch":F
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToFeet(F)F

    move-result v2

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToInch(F)F

    move-result v3

    add-float/2addr v2, v3

    goto :goto_0
.end method

.method public getHeightUnit()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    return v0
.end method

.method public getHeightValue()F
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    return v0
.end method

.method public getListValue()I
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public heightValueConvert()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 222
    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    const v6, 0x249f1

    if-ne v5, v6, :cond_1

    .line 224
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightValue()F

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 226
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->requestFocus()Z

    .line 228
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 229
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 230
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 231
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setNextFocusRightId(I)V

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setNextFocusLeftId(I)V

    .line 233
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0275

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setMinWidth(I)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    .local v0, "arr$":[Landroid/widget/TextView;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v4, v0, v2

    .line 235
    .local v4, "tv":Landroid/widget/TextView;
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 238
    .end local v0    # "arr$":[Landroid/widget/TextView;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightValue()F

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getFeetandInch(F)Ljava/util/HashMap;

    move-result-object v1

    .line 239
    .local v1, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const-string v6, "feet"

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const-string v6, "inch"

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 242
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusable(Z)V

    .line 243
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setFocusableInTouchMode(Z)V

    .line 244
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setNextFocusRightId(I)V

    .line 245
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setNextFocusLeftId(I)V

    .line 246
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0276

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setMinWidth(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mPrimeTextView:[Landroid/widget/TextView;

    .restart local v0    # "arr$":[Landroid/widget/TextView;
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 248
    .restart local v4    # "tv":Landroid/widget/TextView;
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 250
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 251
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->clearFocus()V

    .line 256
    .end local v1    # "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    .line 258
    return-void
.end method

.method public setHeightUnit(I)V
    .locals 0
    .param p1, "unit"    # I

    .prologue
    .line 282
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    .line 283
    return-void
.end method

.method public setHeightValue()V
    .locals 6

    .prologue
    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->isHeighUnitCM()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 299
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 300
    :cond_0
    const/high16 v0, 0x41a00000    # 20.0f

    .line 305
    .local v0, "data":F
    :goto_0
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    .line 334
    .end local v0    # "data":F
    :cond_1
    :goto_1
    return-void

    .line 302
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->convertComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .restart local v0    # "data":F
    goto :goto_0

    .line 311
    .end local v0    # "data":F
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_7

    .line 312
    :cond_4
    const/4 v1, 0x0

    .line 317
    .local v1, "feet":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_8

    .line 318
    :cond_5
    const/16 v3, 0x8

    .line 323
    .local v3, "inch":I
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getHeightValue()F

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->getFeetandInch(F)Ljava/util/HashMap;

    move-result-object v2

    .line 324
    .local v2, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v4, "inch"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v3, :cond_6

    const-string v4, "feet"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v1, :cond_1

    .line 326
    :cond_6
    int-to-float v4, v1

    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToFeet(F)F

    move-result v4

    int-to-float v5, v3

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToInch(F)F

    move-result v5

    add-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    .line 327
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->ceilToDigit(FI)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    goto/16 :goto_1

    .line 314
    .end local v1    # "feet":I
    .end local v2    # "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "inch":I
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .restart local v1    # "feet":I
    goto :goto_2

    .line 320
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .restart local v3    # "inch":I
    goto :goto_3
.end method

.method public setListValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mCustomSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCustomSpinner;->setSelection(I)V

    .line 293
    return-void
.end method

.method public setMinValueText()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x0

    const/high16 v6, 0x41100000    # 9.0f

    .line 585
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightUnit:I

    const v5, 0x249f1

    if-ne v4, v5, :cond_4

    .line 589
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 590
    :cond_0
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    .line 596
    .local v3, "value":F
    :goto_0
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_3

    .line 597
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 646
    .end local v3    # "value":F
    :cond_1
    :goto_1
    return-void

    .line 592
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .restart local v3    # "value":F
    goto :goto_0

    .line 599
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/high16 v5, 0x43960000    # 300.0f

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 607
    .end local v3    # "value":F
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 608
    :cond_5
    const/4 v1, 0x0

    .line 613
    .local v1, "feet":F
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 614
    :cond_6
    const/4 v2, 0x0

    .line 615
    .local v2, "inch":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 621
    :goto_3
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToFeet(F)F

    move-result v4

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToInch(F)F

    move-result v5

    add-float v0, v4, v5

    .line 623
    .local v0, "checkValue":F
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_9

    .line 624
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 625
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 610
    .end local v0    # "checkValue":F
    .end local v1    # "feet":F
    .end local v2    # "inch":F
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .restart local v1    # "feet":F
    goto :goto_2

    .line 617
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .restart local v2    # "inch":F
    goto :goto_3

    .line 626
    .restart local v0    # "checkValue":F
    :cond_9
    cmpl-float v4, v1, v6

    if-nez v4, :cond_a

    const/high16 v4, 0x41200000    # 10.0f

    cmpl-float v4, v2, v4

    if-lez v4, :cond_a

    .line 628
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 630
    :cond_a
    cmpl-float v4, v1, v6

    if-lez v4, :cond_b

    .line 632
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/16 v5, 0x9

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 633
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 635
    :cond_b
    cmpg-float v4, v1, v6

    if-gez v4, :cond_c

    const/high16 v4, 0x41300000    # 11.0f

    cmpl-float v4, v2, v4

    if-lez v4, :cond_c

    .line 637
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 639
    :cond_c
    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-nez v4, :cond_1

    .line 641
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public setOnCignaDialogListener(Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    .line 76
    return-void
.end method

.method public setValue(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    const/high16 v2, 0x43960000    # 300.0f

    const/4 v1, 0x0

    .line 189
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 190
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mMinValue:F

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    .line 200
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->heightValueConvert()V

    .line 201
    return-void

    .line 191
    :cond_0
    cmpl-float v0, p1, v2

    if-ltz v0, :cond_1

    .line 192
    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    .line 193
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setVisibility(I)V

    .line 194
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setVisibility(I)V

    goto :goto_0

    .line 196
    :cond_1
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightValue:F

    .line 197
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setVisibility(I)V

    .line 198
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 712
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setVisibility(I)V

    .line 714
    const v0, 0x7f0801a6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 715
    const v0, 0x7f0801a5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaHeightView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 716
    return-void
.end method
