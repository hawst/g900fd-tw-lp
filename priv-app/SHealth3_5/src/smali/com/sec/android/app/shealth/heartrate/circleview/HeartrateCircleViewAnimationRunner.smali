.class public Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;
.super Ljava/lang/Object;
.source "HeartrateCircleViewAnimationRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;
    }
.end annotation


# instance fields
.field private final circleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

.field private progressUpdater:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;FF)V
    .locals 0
    .param p1, "circleView"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    return-object v0
.end method


# virtual methods
.method public isRun()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->progressUpdater:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;->isRun()Z

    move-result v0

    return v0
.end method

.method public start(II)V
    .locals 1
    .param p1, "duration"    # I
    .param p2, "frequency"    # I

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;-><init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;II)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->progressUpdater:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->progressUpdater:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;->start()V

    .line 33
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->progressUpdater:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;->stop()V

    .line 37
    return-void
.end method
