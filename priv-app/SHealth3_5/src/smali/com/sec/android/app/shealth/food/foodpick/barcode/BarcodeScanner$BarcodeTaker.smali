.class final Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BarcodeTaker"
.end annotation


# instance fields
.field final data:[B

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;[B)V
    .locals 0
    .param p2, "data"    # [B

    .prologue
    .line 488
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;->data:[B

    .line 490
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;[BLcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p2, "x1"    # [B
    .param p3, "x2"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 485
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;[B)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 494
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "take barcode thread run"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;->data:[B

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->takeBarcode([B)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$2100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;[B)V

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusStateLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 497
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1202(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 498
    monitor-exit v1

    .line 499
    return-void

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
