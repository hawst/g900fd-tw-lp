.class Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;
.super Ljava/lang/Object;
.source "ExerciseMateLogAdapter.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

.field final synthetic val$imgExerciseType:Landroid/widget/ImageView;

.field final synthetic val$imgExerciseTypeImgId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;Landroid/widget/ImageView;I)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseType:Landroid/widget/ImageView;

    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseTypeImgId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotReturnLoadImage(J)V
    .locals 3
    .param p1, "ExerciseId"    # J

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->hBitmapHashmap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseType:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseTypeImgId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseType:Landroid/widget/ImageView;

    const v1, 0x7f020335

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 261
    return-void
.end method

.method public onReturnLoadImage(JLandroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "ExerciseId"    # J
    .param p3, "mdrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter;->hBitmapHashmap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseType:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseType:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogAdapter$1;->val$imgExerciseType:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 253
    :cond_0
    return-void
.end method
