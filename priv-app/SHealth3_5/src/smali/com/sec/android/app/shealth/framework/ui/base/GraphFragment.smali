.class public abstract Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;


# static fields
.field private static final GRAPH_PERIOD_KEY:Ljava/lang/String; = "period"

.field private static final PERIOD_DAYS_IN_MONTH:I = 0x3ea

.field private static final PERIOD_HOURS_IN_DAY:I = 0x3e9

.field private static final PERIOD_MONTHS_IN_YEAR:I = 0x3eb


# instance fields
.field private isNoDataGraph:Z

.field private mChangePeriodListener:Landroid/view/View$OnClickListener;

.field private mGeneralViewContainer:Landroid/widget/LinearLayout;

.field private mGraphViewHeader:Landroid/view/View;

.field private mIfGraphModified:Z

.field protected mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

.field private mInformationContainer:Landroid/widget/LinearLayout;

.field private mLegendContainer:Landroid/widget/LinearLayout;

.field private mNoDataContainer:Landroid/widget/LinearLayout;

.field private mPeriodChangeButtonsArray:[Landroid/view/View;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field protected mReadyToShown:Z

.field private mRootView:Landroid/view/View;

.field private mSicGraphView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->isNoDataGraph:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mReadyToShown:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->setPeroidType(Landroid/view/View;)V

    return-void
.end method

.method private changePeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mIfGraphModified:Z

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initGeneralView()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initInformationArea()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;->dimInformationAreaView()V

    return-void
.end method

.method private initLegendArea()V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initGraphLegendArea()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method private selectPeriodButton(I)V
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v3, v2, v0

    if-ne v0, p1, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private varargs setCommonFocusChangeListenerForViews(Landroid/view/View$OnFocusChangeListener;[Landroid/view/View;)V
    .locals 3

    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p2, v0

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private varargs setCommonOnClickListenerForViews(Landroid/view/View$OnClickListener;[Landroid/view/View;)V
    .locals 3

    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p2, v0

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setPeroidType(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_hour_btn:I

    if-ne v1, v2, :cond_2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v1, v0, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->changePeriod(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_day_btn:I

    if-ne v1, v2, :cond_3

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_month_btn:I

    if-ne v1, v2, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0
.end method


# virtual methods
.method public getChartReadyToShown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mReadyToShown:Z

    return v0
.end method

.method public getTabButtonView(I)Landroid/view/View;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v0, v0, p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initDateBar()V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGraphViewHeader:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_hour_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGraphViewHeader:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_day_btn:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGraphViewHeader:Landroid/view/View;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_month_btn:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_month_button:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_day_button:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_hour_button:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    new-array v3, v9, [Landroid/view/View;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    aput-object v2, v3, v8

    iput-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mChangePeriodListener:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mChangePeriodListener:Landroid/view/View$OnClickListener;

    new-array v1, v9, [Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v2, v6

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v2, v7

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v2, v8

    aput-object v2, v1, v8

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->setCommonOnClickListenerForViews(Landroid/view/View$OnClickListener;[Landroid/view/View;)V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;)V

    new-array v1, v9, [Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v2, v6

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v2, v7

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodChangeButtonsArray:[Landroid/view/View;

    aget-object v2, v2, v8

    aput-object v2, v1, v8

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->setCommonFocusChangeListenerForViews(Landroid/view/View$OnFocusChangeListener;[Landroid/view/View;)V

    return-void
.end method

.method protected initGeneralView()V
    .locals 5

    const/4 v3, -0x1

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->isNoDataGraph:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mSicGraphView:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->selectPeriodButton(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mSicGraphView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mSicGraphView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    const-string v2, "SIC"

    const-string/jumbo v3, "remove perents view of graph view"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mSicGraphView:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mReadyToShown:Z

    return-void
.end method

.method protected abstract initGraphLegendArea()Landroid/view/View;
.end method

.method protected abstract initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
.end method

.method protected abstract initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
.end method

.method protected initInformationArea()V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationArea:Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public isGraphModified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mIfGraphModified:Z

    return v0
.end method

.method public isNoDataGraph()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->isNoDataGraph:Z

    return v0
.end method

.method public onConfiguarationLanguageChanged()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGraphViewHeader:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_hour_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->hour:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGraphViewHeader:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_day_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->day:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGraphViewHeader:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->change_period_month_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->month:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->nodata_text:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->no_data:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->graphview:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mRootView:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->general_view_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->information_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->legend_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mLegendContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->date_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGraphViewHeader:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->nodata_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    if-eqz p3, :cond_0

    const-string/jumbo v0, "period"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "period"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initDateBar()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initGeneralView()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initInformationArea()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initLegendArea()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mRootView:Landroid/view/View;

    return-object v0

    :cond_1
    const/16 v1, 0x3ea

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "period"

    const/16 v1, 0x3e9

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "period"

    const/16 v1, 0x3ea

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "period"

    const/16 v1, 0x3eb

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method protected showNoData(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mGeneralViewContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mInformationContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->mNoDataContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->isNoDataGraph:Z

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initLegendArea()V

    return-void
.end method
