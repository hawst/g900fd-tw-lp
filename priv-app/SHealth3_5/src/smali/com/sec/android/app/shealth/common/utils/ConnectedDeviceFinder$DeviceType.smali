.class public final enum Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
.super Ljava/lang/Enum;
.source "ConnectedDeviceFinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

.field public static final enum BLOOD_GLUCOSE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

.field public static final enum BLOOD_PRESSURE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

.field public static final enum HEART_RATE_GEAR2:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

.field public static final enum HEART_RATE_GEAR3:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

.field public static final enum HEART_RATE_GEARFIT:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

.field public static final enum UV_RATE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

.field public static final enum WEIGHT:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;


# instance fields
.field private mDataType:I

.field private mDeviceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 157
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    const-string v1, "WEIGHT"

    const/16 v2, 0x2712

    invoke-direct {v0, v1, v5, v2, v6}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->WEIGHT:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 158
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    const-string v1, "BLOOD_GLUCOSE"

    const/16 v2, 0x2714

    invoke-direct {v0, v1, v6, v2, v8}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->BLOOD_GLUCOSE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 159
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    const-string v1, "BLOOD_PRESSURE"

    const/16 v2, 0x2713

    invoke-direct {v0, v1, v7, v2, v7}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->BLOOD_PRESSURE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 160
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    const-string v1, "HEART_RATE_GEAR3"

    const/16 v2, 0x272e

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEAR3:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 161
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    const-string v1, "HEART_RATE_GEAR2"

    const/16 v2, 0x2726

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEAR2:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 162
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    const-string v1, "HEART_RATE_GEARFIT"

    const/4 v2, 0x5

    const/16 v3, 0x2723

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEARFIT:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 163
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    const-string v1, "UV_RATE"

    const/4 v2, 0x6

    const/16 v3, 0x272e

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->UV_RATE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    .line 156
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->WEIGHT:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->BLOOD_GLUCOSE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->BLOOD_PRESSURE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEAR3:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEAR2:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    aput-object v1, v0, v4

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->HEART_RATE_GEARFIT:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->UV_RATE:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "deviceType"    # I
    .param p4, "dataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 170
    iput p3, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->mDeviceType:I

    .line 171
    iput p4, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->mDataType:I

    .line 172
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 156
    const-class v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->$VALUES:[Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;

    return-object v0
.end method


# virtual methods
.method public getDataType()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->mDataType:I

    return v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceType;->mDeviceType:I

    return v0
.end method
