.class Lcom/sec/android/app/shealth/stress/StressActivity$8;
.super Ljava/lang/Object;
.source "StressActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressActivity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$8;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v2, 0x0

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$8;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->deleteDailyData()V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$8;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    const/16 v1, -0x3e9

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateNextState(II)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$8;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->updateBpmDataView(Z)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$8;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # setter for: Lcom/sec/android/app/shealth/stress/StressActivity;->isShownPopup:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$802(Lcom/sec/android/app/shealth/stress/StressActivity;Z)Z

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$8;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->startSensor()V

    .line 352
    return-void
.end method
