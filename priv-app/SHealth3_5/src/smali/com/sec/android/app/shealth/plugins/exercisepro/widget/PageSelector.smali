.class public abstract Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;
.super Landroid/widget/LinearLayout;
.source "PageSelector.java"


# static fields
.field public static final NOTHING_SELECTED:I = -0x1

.field public static final TAG:Ljava/lang/String;


# instance fields
.field public isForceGone:Z

.field private selectedPage:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->isForceGone:Z

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectedPage:I

    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->init()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->isForceGone:Z

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectedPage:I

    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->init()V

    .line 25
    return-void
.end method

.method private addPage_Forced()V
    .locals 4

    .prologue
    .line 118
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 119
    .local v0, "pageIndicator":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPageIndicatorBackgroundSelectorResource()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 120
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPageIndicatorWidth()I

    move-result v2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPageIndicatorHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 121
    .local v1, "viewParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPagesCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getMarginBetweenIndicators()I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 124
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    return-void
.end method

.method private checkStateAfterAddingPages()V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPagesCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectPage_Forced(I)V

    .line 107
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->checkVisibility()V

    .line 108
    return-void
.end method

.method private checkVisibility()V
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPagesCount()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->isForceGone:Z

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->setVisibility(I)V

    .line 70
    return-void

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMarginBetweenIndicators()I
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getMarginBetweenIndicators(Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method private getPageIndicatorHeight()I
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPageIndicatorHeight(Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method private getPageIndicatorWidth()I
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPageIndicatorWidth(Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->setOrientation(I)V

    .line 35
    return-void
.end method

.method private selectPage_Forced(I)V
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 89
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectedPage:I

    .line 90
    return-void
.end method


# virtual methods
.method public addPage()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->addPage_Forced()V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->checkStateAfterAddingPages()V

    .line 101
    return-void
.end method

.method public fill(II)V
    .locals 4
    .param p1, "pagesCount"    # I
    .param p2, "selectedPage"    # I

    .prologue
    .line 44
    if-gtz p1, :cond_0

    .line 45
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pagesCount should be > 0, pagesCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_0
    if-ltz p2, :cond_1

    if-lt p2, p1, :cond_2

    .line 48
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "selectedPage should be >= 0 and < pagesCount, selectedPage = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getChildCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->removeAllViews()V

    .line 53
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_4

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->addPage_Forced()V

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectPage_Forced(I)V

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->checkVisibility()V

    .line 58
    return-void
.end method

.method protected abstract getMarginBetweenIndicators(Landroid/content/res/Resources;)I
.end method

.method protected abstract getPageIndicatorBackgroundSelectorResource()I
.end method

.method protected abstract getPageIndicatorHeight(Landroid/content/res/Resources;)I
.end method

.method protected abstract getPageIndicatorWidth(Landroid/content/res/Resources;)I
.end method

.method public getPagesCount()I
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getChildCount()I

    move-result v0

    return v0
.end method

.method public selectPage(I)V
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 78
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPagesCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "pageIndex should be >= 0 and < pages count, pageIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pages count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPagesCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectedPage:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectedPage:I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getPagesCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 82
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectedPage:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 84
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectPage_Forced(I)V

    .line 85
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 63
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->isForceGone:Z

    .line 66
    :cond_0
    return-void
.end method
