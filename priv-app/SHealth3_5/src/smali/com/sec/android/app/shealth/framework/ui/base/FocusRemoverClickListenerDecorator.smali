.class public Lcom/sec/android/app/shealth/framework/ui/base/FocusRemoverClickListenerDecorator;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnClickListenerDecorator;


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/OnClickListenerDecorator;-><init>(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 18

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    instance-of v1, v4, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    instance-of v1, v1, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x33

    aput v16, v2, v15

    const/16 v15, 0x6517

    aput v15, v2, v14

    const/16 v14, -0x4ef3

    aput v14, v2, v13

    const/16 v13, -0x3b

    aput v13, v2, v12

    const/16 v12, -0x5d

    aput v12, v2, v11

    const/16 v11, -0xe

    aput v11, v2, v10

    const/16 v10, 0x4731

    aput v10, v2, v9

    const/16 v9, -0x2ccd

    aput v9, v2, v8

    const/16 v8, -0x5a

    aput v8, v2, v7

    const/16 v7, -0x6984

    aput v7, v2, v6

    const/4 v6, -0x8

    aput v6, v2, v3

    const/16 v3, -0x52b6

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, -0x57

    aput v17, v1, v16

    const/16 v16, 0x6578

    aput v16, v1, v15

    const/16 v15, -0x4e9b

    aput v15, v1, v14

    const/16 v14, -0x4f

    aput v14, v1, v13

    const/16 v13, -0x3a

    aput v13, v1, v12

    const/16 v12, -0x61

    aput v12, v1, v11

    const/16 v11, 0x476e

    aput v11, v1, v10

    const/16 v10, -0x2cb9

    aput v10, v1, v9

    const/16 v9, -0x2d

    aput v9, v1, v8

    const/16 v8, -0x69f4

    aput v8, v1, v7

    const/16 v7, -0x6a

    aput v7, v1, v6

    const/16 v6, -0x52dd

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/FocusRemoverClickListenerDecorator;->mListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    return-void

    :cond_1
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2
.end method
