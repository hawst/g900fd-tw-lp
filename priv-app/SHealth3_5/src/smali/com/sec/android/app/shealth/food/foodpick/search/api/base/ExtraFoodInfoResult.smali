.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
.source "ExtraFoodInfoResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final NO_CALORIES_VALUE:I = -0x1


# instance fields
.field private mCalories:I

.field private mRootCategoriesId:Ljava/lang/String;

.field private mRootCategoriesName:Ljava/lang/String;

.field private mSubCategoriesId:Ljava/lang/String;

.field private mSubCategoriesName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V
    .locals 6
    .param p1, "searchResult"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 34
    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "searchResult"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .param p2, "rootCategoriesId"    # Ljava/lang/String;
    .param p3, "rootCategoriesName"    # Ljava/lang/String;
    .param p4, "subCategoriesId"    # Ljava/lang/String;
    .param p5, "subCategoriesName"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;-><init>(Ljava/lang/Object;)V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mCalories:I

    .line 48
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mRootCategoriesName:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mSubCategoriesName:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mRootCategoriesId:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mSubCategoriesId:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getCalories()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mCalories:I

    return v0
.end method

.method public getRootCategoriesId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mRootCategoriesId:Ljava/lang/String;

    return-object v0
.end method

.method public getRootCategoriesName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mRootCategoriesName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubCategoriesId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mSubCategoriesId:Ljava/lang/String;

    return-object v0
.end method

.method public getSubCategoriesName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mSubCategoriesName:Ljava/lang/String;

    return-object v0
.end method

.method public hasCalories()Z
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getCalories()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCalories(I)V
    .locals 0
    .param p1, "calories"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->mCalories:I

    .line 138
    return-void
.end method
