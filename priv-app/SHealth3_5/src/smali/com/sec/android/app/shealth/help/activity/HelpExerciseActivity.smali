.class public Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpExerciseActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 23
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090ffa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 26
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    const v0, 0x7f030208

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->setContentView(I)V

    .line 18
    return-void
.end method

.method protected onResume()V
    .locals 12

    .prologue
    const v11, 0x7f08092f

    const/16 v10, 0x8

    const/4 v9, 0x4

    const/4 v8, 0x0

    .line 30
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 31
    const v6, 0x7f0804df

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 32
    .local v1, "hideWithRTL1":Landroid/view/View;
    const v6, 0x7f080925

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 33
    .local v2, "hideWithRTL2":Landroid/view/View;
    const v6, 0x7f08092c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 34
    .local v4, "showWithRTL1":Landroid/view/View;
    const v6, 0x7f08092e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 36
    .local v5, "showWithRTL2":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 37
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 39
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 40
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 41
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 42
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 43
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 44
    .local v3, "lllp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v6, 0x5

    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 45
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/help/activity/HelpExerciseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    .end local v3    # "lllp":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 50
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 51
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 52
    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
