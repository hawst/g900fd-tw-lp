.class final enum Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;
.super Ljava/lang/Enum;
.source "RestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RestoreStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

.field public static final enum DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

.field public static final enum FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

.field public static final enum NODATA:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

.field public static final enum RESTART:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

.field public static final enum SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

.field public static final enum START:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 85
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    const-string v1, "START"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->START:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    const-string v1, "FAIL_NETWORK_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .line 89
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    const-string v1, "DEVICE_STORAGE_LOW"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .line 91
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    const-string v1, "SERVER_MAINTENANCE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .line 93
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    const-string v1, "NODATA"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->NODATA:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .line 95
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    const-string v1, "RESTART"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->RESTART:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .line 83
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->START:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->NODATA:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->RESTART:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    return-object v0
.end method
