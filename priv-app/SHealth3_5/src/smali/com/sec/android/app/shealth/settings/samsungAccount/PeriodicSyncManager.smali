.class public Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;
.super Ljava/lang/Object;
.source "PeriodicSyncManager.java"


# static fields
.field private static mInstance:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;


# instance fields
.field public ACCOUNT_SERVICE:Ljava/lang/String;

.field public ACCOUNT_TYPE_SAMSUNG_ACCOUNT:Ljava/lang/String;

.field private TAG:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->TAG:Ljava/lang/String;

    .line 41
    const-string v0, "com.osp.app.signin"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_TYPE_SAMSUNG_ACCOUNT:Ljava/lang/String;

    .line 42
    const-string v0, "account"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_SERVICE:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->mInstance:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->mInstance:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    .line 53
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->mInstance:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    return-object v0
.end method


# virtual methods
.method public cancelPeriodicSync(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 97
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_SERVICE:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accounts/AccountManager;

    .line 99
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_TYPE_SAMSUNG_ACCOUNT:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 101
    .local v0, "account":[Landroid/accounts/Account;
    array-length v4, v0

    if-eqz v4, :cond_0

    .line 102
    aget-object v4, v0, v3

    const-string v5, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v4, v5}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 103
    .local v2, "pList":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 105
    aget-object v4, v0, v3

    const-string v5, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/PeriodicSync;

    iget-object v3, v3, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {v4, v5, v3}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 106
    const-string v3, "Auto backup is cancelled"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 107
    const/4 v3, 0x1

    .line 115
    .end local v2    # "pList":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    :cond_0
    :goto_0
    return v3

    .line 111
    .restart local v2    # "pList":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->TAG:Ljava/lang/String;

    const-string v5, "No Periodic Sync Queued"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPeriodicSync(Landroid/content/Context;)J
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 57
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_SERVICE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accounts/AccountManager;

    .line 59
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_TYPE_SAMSUNG_ACCOUNT:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 61
    .local v0, "account":[Landroid/accounts/Account;
    array-length v3, v0

    if-eqz v3, :cond_0

    .line 62
    aget-object v3, v0, v5

    const-string v4, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v3, v4}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 63
    .local v2, "pList":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/PeriodicSync;

    iget-wide v3, v3, Landroid/content/PeriodicSync;->period:J

    .line 68
    .end local v2    # "pList":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    :goto_0
    return-wide v3

    :cond_0
    const-wide/16 v3, 0x0

    goto :goto_0
.end method

.method public setPeroidicSync(Landroid/content/Context;J)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "seconds"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->cancelPeriodicSync(Landroid/content/Context;)Z

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_SERVICE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accounts/AccountManager;

    .line 75
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->ACCOUNT_TYPE_SAMSUNG_ACCOUNT:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 76
    .local v0, "account":[Landroid/accounts/Account;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 81
    .local v2, "extraData":Landroid/os/Bundle;
    const-string/jumbo v3, "packageName"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string/jumbo v3, "request_type"

    const v4, 0x1869f

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 83
    const-string v3, "data_type"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    const-string v3, "action"

    const-string v4, "backup"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    array-length v3, v0

    if-eqz v3, :cond_0

    .line 88
    aget-object v3, v0, v5

    const-string v4, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v3, v4, v6}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 89
    aget-object v3, v0, v5

    const-string v4, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v3, v4, v6}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 90
    aget-object v3, v0, v5

    const-string v4, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v3, v4, v2, p2, p3}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 92
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Auto backup is set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 93
    return-void
.end method
