.class Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;
.super Ljava/lang/Object;
.source "HeartrateActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showNoSensorInformationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0

    .prologue
    .line 1113
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 2
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 1119
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const v0, 0x7f080582

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1102(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 1120
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const v0, 0x7f080581

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgainNosensorCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$2002(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1100(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mShowAgainNosensorCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$2000(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$21;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1123
    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    .line 1125
    return-void
.end method
