.class Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;
.super Landroid/os/CountDownTimer;
.source "StressStateMeasuring.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 4

    .prologue
    .line 94
    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "StressStateMeasuring mCurrentStateTimer onFinish() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->access$100(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->access$100(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->access$102(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;Z)Z

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasureFailed()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    .line 98
    .local v0, "nextState":Lcom/sec/android/app/shealth/stress/state/StressState;
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 99
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    .line 101
    .end local v0    # "nextState":Lcom/sec/android/app/shealth/stress/state/StressState;
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 86
    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StressStateMeasuring mCurrentStateTimer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->access$100(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;

    # getter for: Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->isInCurrentState:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;->access$100(Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasuring$1;->cancel()V

    .line 90
    :cond_0
    return-void
.end method
