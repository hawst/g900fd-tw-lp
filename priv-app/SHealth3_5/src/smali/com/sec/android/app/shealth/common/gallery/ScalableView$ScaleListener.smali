.class Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;
.super Ljava/lang/Object;
.source "ScalableView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/gallery/ScalableView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# static fields
.field private static final MAX_SCALE_FACTORY:F = 1.05f

.field private static final MIN_SCALE_FACTORY:F = 0.95f


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;-><init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 10
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v6, 0x40400000    # 3.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 181
    const v4, 0x3f733333    # 0.95f

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    const v5, 0x3f866666    # 1.05f

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 182
    .local v1, "scaleFactor":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v0

    .line 183
    .local v0, "lastScale":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # *= operator for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$332(Lcom/sec/android/app/shealth/common/gallery/ScalableView;F)F

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v4

    cmpl-float v4, v4, v6

    if-lez v4, :cond_3

    .line 185
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # setter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v4, v6}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$302(Lcom/sec/android/app/shealth/common/gallery/ScalableView;F)F

    .line 186
    div-float v1, v6, v0

    .line 191
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartWidthOfBitmapInImageView:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1000(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1100(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    cmpg-float v4, v4, v5

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartHeightOfBitmapInImageView:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1200(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_5

    .line 193
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1100(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    div-float/2addr v5, v9

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F
    invoke-static {v6}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v6

    div-float/2addr v6, v9

    invoke-virtual {v4, v1, v1, v5, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 194
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1400(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)[F

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->getValues([F)V

    .line 195
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1400(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)[F

    move-result-object v4

    const/4 v5, 0x2

    aget v2, v4, v5

    .line 196
    .local v2, "x":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1400(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)[F

    move-result-object v4

    const/4 v5, 0x5

    aget v3, v4, v5

    .line 197
    .local v3, "y":F
    cmpg-float v4, v1, v8

    if-gez v4, :cond_2

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartWidthOfBitmapInImageView:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1000(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1100(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_4

    .line 199
    cmpl-float v4, v3, v7

    if-lez v4, :cond_2

    .line 200
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v4

    neg-float v5, v3

    invoke-virtual {v4, v7, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 213
    .end local v2    # "x":F
    .end local v3    # "y":F
    :cond_2
    :goto_1
    const/4 v4, 0x1

    return v4

    .line 187
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v4

    cmpg-float v4, v4, v8

    if-gez v4, :cond_0

    .line 188
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # setter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v4, v8}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$302(Lcom/sec/android/app/shealth/common/gallery/ScalableView;F)F

    .line 189
    div-float v1, v8, v0

    goto/16 :goto_0

    .line 203
    .restart local v2    # "x":F
    .restart local v3    # "y":F
    :cond_4
    cmpl-float v4, v2, v7

    if-lez v4, :cond_2

    .line 204
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v4

    neg-float v5, v2

    invoke-virtual {v4, v5, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_1

    .line 209
    .end local v2    # "x":F
    .end local v3    # "y":F
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v6

    invoke-virtual {v4, v1, v1, v5, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 210
    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getPreviousSpanX()F

    move-result v6

    sub-float/2addr v5, v6

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getPreviousSpanY()F

    move-result v7

    sub-float/2addr v6, v7

    # invokes: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->correctMatrixMeasure(FF)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$800(Lcom/sec/android/app/shealth/common/gallery/ScalableView;FF)V

    goto :goto_1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 218
    return-void
.end method
