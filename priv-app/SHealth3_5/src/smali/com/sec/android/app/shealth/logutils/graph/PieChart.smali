.class public Lcom/sec/android/app/shealth/logutils/graph/PieChart;
.super Landroid/widget/ImageView;
.source "PieChart.java"


# static fields
.field private static final START_ANGLE:I = -0x5a


# instance fields
.field private lastColors:[I

.field private values:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method


# virtual methods
.method public varargs drawCircleDiagram([Landroid/util/Pair;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "valueAndColorPairs":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Integer;>;"
    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v17, v0

    aput v17, v5, v6

    const/4 v6, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    move/from16 v17, v0

    aput v17, v5, v6

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->getMax([I)I

    move-result v10

    .line 60
    .local v10, "diameter":I
    const/16 v16, 0x0

    .line 61
    .local v16, "totalValue":F
    move-object/from16 v8, p1

    .local v8, "arr$":[Landroid/util/Pair;
    array-length v12, v8

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_0

    aget-object v15, v8, v11

    .line 62
    .local v15, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Integer;>;"
    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v17, v0

    iget-object v5, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    add-double v5, v5, v17

    double-to-float v0, v5

    move/from16 v16, v0

    .line 61
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 64
    .end local v15    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Integer;>;"
    :cond_0
    const/4 v5, 0x0

    cmpg-float v5, v16, v5

    if-gtz v5, :cond_1

    .line 65
    const/16 v5, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setVisibility(I)V

    .line 83
    :goto_1
    return-void

    .line 68
    :cond_1
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setVisibility(I)V

    .line 69
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v10, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 70
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 71
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 72
    .local v7, "paint":Landroid/graphics/Paint;
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 73
    const/4 v5, 0x1

    invoke-virtual {v7, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 74
    new-instance v3, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    int-to-float v0, v10

    move/from16 v17, v0

    int-to-float v0, v10

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v3, v5, v6, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 75
    .local v3, "rectF":Landroid/graphics/RectF;
    const/high16 v4, -0x3d4c0000    # -90.0f

    .line 76
    .local v4, "lastAngle":F
    move-object/from16 v8, p1

    array-length v12, v8

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v12, :cond_2

    aget-object v15, v8, v11

    .line 77
    .restart local v15    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Integer;>;"
    const-wide v17, 0x4076800000000000L    # 360.0

    iget-object v5, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v19, v0

    div-double v5, v5, v19

    mul-double v13, v17, v5

    .line 78
    .local v13, "nextAngle":D
    iget-object v5, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v7, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 79
    double-to-float v5, v13

    const/4 v6, 0x1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 80
    float-to-double v5, v4

    add-double/2addr v5, v13

    double-to-float v4, v5

    .line 76
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 82
    .end local v13    # "nextAngle":D
    .end local v15    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Integer;>;"
    :cond_2
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public refreshValuesToColorPairs()V
    .locals 5

    .prologue
    .line 108
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->values:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    new-array v1, v2, [Landroid/util/Pair;

    .line 109
    .local v1, "pairs":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->lastColors:[I

    array-length v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->values:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 110
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->values:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 111
    new-instance v2, Landroid/util/Pair;

    iget-object v3, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->values:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->lastColors:[I

    aget v4, v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v1, v0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->drawCircleDiagram([Landroid/util/Pair;)V

    .line 115
    return-void
.end method

.method public varargs setColors([I)V
    .locals 0
    .param p1, "colors"    # [I

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->lastColors:[I

    .line 92
    return-void
.end method

.method public setValues(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->values:Ljava/util/Vector;

    .line 102
    return-void
.end method
