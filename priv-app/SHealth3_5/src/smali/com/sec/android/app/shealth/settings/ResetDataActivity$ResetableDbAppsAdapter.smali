.class Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;
.super Landroid/widget/BaseAdapter;
.source "ResetDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ResetDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResetableDbAppsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;
    }
.end annotation


# instance fields
.field private mCategoryPositions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDbResettableAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 348
    .local p2, "dbResettableAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;>;"
    .local p3, "categoryPositions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 350
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;

    .line 351
    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mCategoryPositions:Ljava/util/ArrayList;

    .line 352
    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 366
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;

    iget v0, v0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;->mType:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    .line 391
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 393
    .local v3, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_1

    .line 395
    new-instance v2, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;

    invoke-direct {v2, v10}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity$1;)V

    .line 396
    .local v2, "holder":Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;
    const v8, 0x7f0301f5

    invoke-virtual {v3, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/LinearLayout;

    .line 397
    .restart local p2    # "convertView":Landroid/view/View;
    const v8, 0x7f0808ae

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 398
    .local v7, "textView":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;

    iget-object v8, v8, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;->mAppregistryData:Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v8, v8, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->setMainTextView(Landroid/widget/TextView;)V

    .line 400
    const v8, 0x7f0808ad

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 401
    .local v1, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->setItemCheckBox(Landroid/widget/CheckBox;)V

    .line 403
    const v8, 0x7f0808ac

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 404
    .local v4, "itemContainer":Landroid/widget/RelativeLayout;
    new-instance v8, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;

    invoke-direct {v8, p0, p1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;I)V

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 405
    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->setItemContainer(Landroid/widget/RelativeLayout;)V

    .line 407
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 417
    .end local v1    # "checkBox":Landroid/widget/CheckBox;
    .end local v7    # "textView":Landroid/widget/TextView;
    :goto_0
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->getItemCheckBox()Landroid/widget/CheckBox;

    move-result-object v0

    .line 418
    .local v0, "cb":Landroid/widget/CheckBox;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mcheckedItem:Ljava/util/Map;
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/Map;

    move-result-object v8

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 419
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mcheckedItem:Ljava/util/Map;
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/Map;

    move-result-object v8

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 425
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget-object v8, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 426
    .local v5, "language":Ljava/lang/String;
    const-string v8, "ar"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 428
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->getMainTextView()Landroid/widget/TextView;

    move-result-object v6

    .line 429
    .local v6, "mTextView":Landroid/widget/TextView;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090240

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 431
    const/4 v8, 0x5

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 435
    .end local v6    # "mTextView":Landroid/widget/TextView;
    :cond_0
    return-object p2

    .line 412
    .end local v0    # "cb":Landroid/widget/CheckBox;
    .end local v2    # "holder":Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;
    .end local v4    # "itemContainer":Landroid/widget/RelativeLayout;
    .end local v5    # "language":Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;

    .line 413
    .restart local v2    # "holder":Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->getMainTextView()Landroid/widget/TextView;

    move-result-object v9

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mDbResettableAppList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;

    iget-object v8, v8, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;->mAppregistryData:Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v8, v8, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;->getItemContainer()Landroid/widget/RelativeLayout;

    move-result-object v4

    .line 415
    .restart local v4    # "itemContainer":Landroid/widget/RelativeLayout;
    new-instance v8, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;

    invoke-direct {v8, p0, p1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter$ResetItemsCheckListener;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;I)V

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 423
    .restart local v0    # "cb":Landroid/widget/CheckBox;
    :cond_2
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;->mCategoryPositions:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    const/4 v0, 0x0

    .line 385
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
