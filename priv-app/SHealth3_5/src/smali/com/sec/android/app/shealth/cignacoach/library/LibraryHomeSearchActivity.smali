.class public Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "LibraryHomeSearchActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mCignaLibraryBaseListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field

.field private mGroupHeaderView:Landroid/widget/LinearLayout;

.field private mSearchFieldEditText:Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

.field private mSearchListView:Landroid/widget/ListView;

.field private mSearchResultList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->searchData(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private getActionBarSearchFieldView()Landroid/view/View;
    .locals 4

    .prologue
    .line 83
    const v2, 0x7f030060

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 85
    .local v0, "searchField":Landroid/view/View;
    const v2, 0x7f0801d6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    .line 86
    .local v1, "searchFieldEditText":Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->setSearchFieldEditText(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)V

    .line 87
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 108
    return-object v0
.end method

.method private getEmptyView()Landroid/view/View;
    .locals 3

    .prologue
    .line 61
    const v2, 0x7f0801d5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 62
    .local v1, "emptyView":Landroid/widget/LinearLayout;
    const v2, 0x7f0801c4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    .local v0, "emptyMessage":Landroid/widget/TextView;
    const-string v2, "No articles found"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-object v1
.end method

.method private searchData(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "searchKeyword"    # Ljava/lang/CharSequence;

    .prologue
    .line 118
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getSearchArticle(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->onSearchResult(Ljava/util/ArrayList;)V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->onSearchResult(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setSearchFieldEditText(Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;)V
    .locals 0
    .param p1, "searchFieldEditText"    # Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchFieldEditText:Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    .line 114
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 152
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 155
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 159
    return-void
.end method

.method public closeScreen()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 127
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->closeScreen()V

    .line 128
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->overridePendingTransition(II)V

    .line 129
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 78
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->getActionBarSearchFieldView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addCustomView(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    .line 134
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->overridePendingTransition(II)V

    .line 135
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v1, 0x7f03005f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->setContentView(I)V

    .line 44
    const v1, 0x7f0801d3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mGroupHeaderView:Landroid/widget/LinearLayout;

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mGroupHeaderView:Landroid/widget/LinearLayout;

    const v2, 0x7f0801c5

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "groupResultsTitle":Landroid/widget/TextView;
    const-string v1, "RESULTS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchResultList:Ljava/util/ArrayList;

    .line 50
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchResultList:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mCignaLibraryBaseListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;

    .line 51
    const v1, 0x7f0801d4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchListView:Landroid/widget/ListView;

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->getEmptyView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mCignaLibraryBaseListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mGroupHeaderView:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 58
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchFieldEditText:Lcom/sec/android/app/shealth/cignacoach/widget/SearchEditText;

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 142
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 144
    .local v0, "cignaLibraryArticleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 146
    const-string v2, "extra_name_article_id"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 147
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 148
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 72
    return-void
.end method

.method public onSearchResult(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "resultData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mGroupHeaderView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchResultList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchResultList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 183
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mCignaLibraryBaseListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;->notifyDataSetChanged()V

    .line 184
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mGroupHeaderView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeSearchActivity;->mSearchResultList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 163
    return-void
.end method
