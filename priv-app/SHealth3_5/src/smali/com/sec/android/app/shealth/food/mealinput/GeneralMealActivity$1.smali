.class Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;
.super Ljava/util/HashMap;
.source "GeneralMealActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 149
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 150
    const-string v0, "discard_changes_dialog"

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DiscardChangesDialogButtonController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DiscardChangesDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string v0, "add_to_saved_meals_dialog"

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v0, "delete_dialog_tag"

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    const-string/jumbo v0, "out_of_range_popup"

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    const-string v0, "future_alert_dialog"

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    const-string/jumbo v0, "quick_input_poup"

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const-string/jumbo v0, "skipped_meal_confirm_dialog"

    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$SkippedMealConfirmController;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$SkippedMealConfirmController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    return-void
.end method
