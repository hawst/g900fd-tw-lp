.class Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;
.super Ljava/lang/Object;
.source "PinCodeActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/PinCodeActivity;->toggleKeyBoard()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mOutStateBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$500(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$600(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$700(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mKeyBoardStatus:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$602(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)Z

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->hideHeightKeyboard()V

    goto :goto_0
.end method
