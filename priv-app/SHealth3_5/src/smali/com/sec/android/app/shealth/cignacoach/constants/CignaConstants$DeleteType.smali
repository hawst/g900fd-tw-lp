.class public final enum Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;
.super Ljava/lang/Enum;
.source "CignaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeleteType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

.field public static final enum DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

.field public static final enum SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 133
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    .line 132
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 132
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    return-object v0
.end method
