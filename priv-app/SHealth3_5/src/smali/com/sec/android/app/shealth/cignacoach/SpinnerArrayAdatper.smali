.class public abstract Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;
.super Landroid/widget/ArrayAdapter;
.source "SpinnerArrayAdatper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;I[Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "spinner"    # Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;
    .param p3, "resource"    # I
    .param p4, "objects"    # [Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mContext:Landroid/content/Context;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    .line 23
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v3, 0x7f0700e3

    .line 38
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->getSpinnerChildView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 40
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f080247

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 41
    .local v0, "unitTxt":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 43
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mSpinner:Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomSpinner;->getSelectedItemPosition()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070132

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 49
    :goto_0
    return-object v1

    .line 46
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method protected getSpinnerChildView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 69
    if-nez p2, :cond_0

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mContext:Landroid/content/Context;

    const v2, 0x7f030079

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 73
    :cond_0
    const v1, 0x7f080247

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 74
    .local v0, "unitTxt":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    return-object p2
.end method

.method public abstract getSpinnerGroupTextTopMargin()I
.end method

.method protected getSpinnerGroupView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 54
    if-nez p2, :cond_0

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mContext:Landroid/content/Context;

    const v3, 0x7f03007a

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 58
    :cond_0
    const v2, 0x7f080247

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 59
    .local v1, "unitTxt":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 62
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->getSpinnerGroupTextTopMargin()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 64
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/SpinnerArrayAdatper;->getSpinnerGroupView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method
