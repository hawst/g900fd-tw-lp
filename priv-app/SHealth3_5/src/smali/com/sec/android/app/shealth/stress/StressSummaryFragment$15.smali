.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->showErrorDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 1562
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 1565
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$802(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1566
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1567
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningLeftAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/animation/ValueAnimator;

    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    .line 1568
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mLightningRightAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/animation/ValueAnimator;

    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    .line 1569
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->restartCurrentState(I)V

    .line 1570
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_0

    .line 1571
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1572
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iput-object v2, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeIn:Landroid/view/animation/AnimationSet;

    .line 1574
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_1

    .line 1575
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1576
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iput-object v2, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->animationFadeOut:Landroid/view/animation/AnimationSet;

    .line 1578
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1579
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1580
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr0:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3202(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1581
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$15;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mAnimationViewErr1:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$3302(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1582
    return-void
.end method
