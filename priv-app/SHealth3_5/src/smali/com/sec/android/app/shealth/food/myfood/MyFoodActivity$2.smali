.class Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;
.super Ljava/lang/Object;
.source "MyFoodActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)V
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 570
    sget-object v0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$3;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 581
    :goto_0
    return-void

    .line 572
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # getter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$2000(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DONE_MY_FOOD_BY_ACTION_BAR_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->mIsDeleteModeByLongPress:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$802(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Z)Z

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity$2;->this$0:Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->getExpandableListProvider()Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$400(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;)Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentProvider;->deleteCheckedItems()Z

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->toggleActivityState(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;->access$2100(Lcom/sec/android/app/shealth/food/myfood/MyFoodActivity;Z)V

    goto :goto_0

    .line 570
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
