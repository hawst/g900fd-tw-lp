.class public Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;
.super Landroid/os/AsyncTask;
.source "ImageScaleCopyAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final COMPRESS_QUALITY:I = 0x50

.field private static final DEFAULT_MAX_IMAGE_SIZE:I = 0x400

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final SIZE_NOT_INITED:F = -1.0f


# instance fields
.field private mInputImagePath:Ljava/lang/String;

.field private mMaxSize:I

.field private final mOutputImagePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "inputImagePath"    # Ljava/lang/String;
    .param p2, "outputImagePath"    # Ljava/lang/String;

    .prologue
    .line 66
    const/16 v0, 0x400

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "inputImagePath"    # Ljava/lang/String;
    .param p2, "outputImagePath"    # Ljava/lang/String;
    .param p3, "maxSize"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 54
    iput p3, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private copyFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "inputFilePath"    # Ljava/lang/String;
    .param p2, "outputFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 156
    const/4 v1, 0x0

    .line 157
    .local v1, "src":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 159
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 160
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 161
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    new-array v2, v9, [Ljava/io/Closeable;

    aput-object v1, v2, v8

    aput-object v0, v2, v7

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    move v2, v7

    :goto_0
    return v2

    .line 163
    :catch_0
    move-exception v6

    .line 164
    .local v6, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v2, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    new-array v2, v9, [Ljava/io/Closeable;

    aput-object v1, v2, v8

    aput-object v0, v2, v7

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    move v2, v8

    goto :goto_0

    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    new-array v3, v9, [Ljava/io/Closeable;

    aput-object v1, v3, v8

    aput-object v0, v3, v7

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    throw v2
.end method

.method private getScaledSize(FFI)Landroid/graphics/PointF;
    .locals 3
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "maxSize"    # I

    .prologue
    .line 172
    int-to-float v2, p3

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    int-to-float v2, p3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    .line 173
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 186
    :goto_0
    return-object v2

    .line 176
    :cond_0
    cmpl-float v2, p1, p2

    if-lez v2, :cond_1

    .line 177
    int-to-float v1, p3

    .line 178
    .local v1, "scaledWidth":F
    div-float v2, v1, p1

    mul-float v0, p2, v2

    .line 186
    .local v0, "scaledHeight":F
    :goto_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0

    .line 179
    .end local v0    # "scaledHeight":F
    .end local v1    # "scaledWidth":F
    :cond_1
    cmpg-float v2, p1, p2

    if-gez v2, :cond_2

    .line 180
    int-to-float v0, p3

    .line 181
    .restart local v0    # "scaledHeight":F
    div-float v2, v0, p2

    mul-float v1, p1, v2

    .restart local v1    # "scaledWidth":F
    goto :goto_1

    .line 183
    .end local v0    # "scaledHeight":F
    .end local v1    # "scaledWidth":F
    :cond_2
    int-to-float v1, p3

    .line 184
    .restart local v1    # "scaledWidth":F
    int-to-float v0, p3

    .restart local v0    # "scaledHeight":F
    goto :goto_1
.end method

.method private resizeAndTransformBitmap(FFFF)Ljava/lang/Boolean;
    .locals 27
    .param p1, "dstWidth"    # F
    .param p2, "dstHeight"    # F
    .param p3, "inWidth"    # F
    .param p4, "inHeight"    # F

    .prologue
    .line 102
    const/4 v6, 0x0

    .line 103
    .local v6, "in":Ljava/io/InputStream;
    const/4 v12, 0x0

    .line 105
    .local v12, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    .end local v6    # "in":Ljava/io/InputStream;
    .local v7, "in":Ljava/io/InputStream;
    :try_start_1
    new-instance v11, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v11}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 107
    .local v11, "options":Landroid/graphics/BitmapFactory$Options;
    div-float v23, p3, p1

    div-float v24, p4, p2

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(FF)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v11, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 108
    const/16 v23, 0x0

    move-object/from16 v0, v23

    invoke-static {v7, v0, v11}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 110
    .local v16, "roughBitmap":Landroid/graphics/Bitmap;
    new-instance v21, Landroid/graphics/Matrix;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Matrix;-><init>()V

    .line 111
    .local v21, "transformationMatrix":Landroid/graphics/Matrix;
    new-instance v8, Landroid/graphics/RectF;

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-direct {v8, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 112
    .local v8, "inRect":Landroid/graphics/RectF;
    new-instance v14, Landroid/graphics/RectF;

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, p1

    move/from16 v3, p2

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 113
    .local v14, "outRect":Landroid/graphics/RectF;
    sget-object v23, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v14, v1}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 114
    const/16 v23, 0x9

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v22, v0

    .line 115
    .local v22, "values":[F
    invoke-virtual/range {v21 .. v22}, Landroid/graphics/Matrix;->getValues([F)V

    .line 117
    const/16 v23, 0x0

    aget v19, v22, v23

    .line 118
    .local v19, "scaleX":F
    const/16 v23, 0x4

    aget v20, v22, v23

    .line 120
    .local v20, "scaleY":F
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v19

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v18

    .line 121
    .local v18, "roughtWidth":I
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v20

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v17

    .line 122
    .local v17, "roughtHeight":I
    const/16 v23, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    move/from16 v2, v17

    move/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 124
    .local v15, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v13, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 125
    .end local v12    # "out":Ljava/io/FileOutputStream;
    .local v13, "out":Ljava/io/FileOutputStream;
    :try_start_2
    sget-object v23, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v24, 0x50

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v15, v0, v1, v13}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 127
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    .line 128
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V

    .line 130
    new-instance v10, Landroid/media/ExifInterface;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-direct {v10, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 131
    .local v10, "oldExif":Landroid/media/ExifInterface;
    const-string v23, "Orientation"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 133
    .local v5, "exifOrientation":Ljava/lang/String;
    new-instance v9, Landroid/media/ExifInterface;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 134
    .local v9, "newExif":Landroid/media/ExifInterface;
    if-eqz v5, :cond_0

    .line 135
    const-string v23, "Orientation"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0, v5}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_0
    invoke-virtual {v9}, Landroid/media/ExifInterface;->saveAttributes()V

    .line 138
    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v23

    .line 143
    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v7, v24, v25

    const/16 v25, 0x1

    aput-object v13, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    move-object v12, v13

    .end local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "out":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v5    # "exifOrientation":Ljava/lang/String;
    .end local v7    # "in":Ljava/io/InputStream;
    .end local v8    # "inRect":Landroid/graphics/RectF;
    .end local v9    # "newExif":Landroid/media/ExifInterface;
    .end local v10    # "oldExif":Landroid/media/ExifInterface;
    .end local v11    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "outRect":Landroid/graphics/RectF;
    .end local v15    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "roughBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "roughtHeight":I
    .end local v18    # "roughtWidth":I
    .end local v19    # "scaleX":F
    .end local v20    # "scaleY":F
    .end local v21    # "transformationMatrix":Landroid/graphics/Matrix;
    .end local v22    # "values":[F
    .restart local v6    # "in":Ljava/io/InputStream;
    :goto_0
    return-object v23

    .line 139
    :catch_0
    move-exception v4

    .line 140
    .local v4, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    sget-object v23, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 141
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v23

    .line 143
    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v6, v24, v25

    const/16 v25, 0x1

    aput-object v12, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    goto :goto_0

    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v23

    :goto_2
    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v6, v24, v25

    const/16 v25, 0x1

    aput-object v12, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    throw v23

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v7    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v23

    move-object v6, v7

    .end local v7    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v6    # "in":Ljava/io/InputStream;
    .end local v12    # "out":Ljava/io/FileOutputStream;
    .restart local v7    # "in":Ljava/io/InputStream;
    .restart local v8    # "inRect":Landroid/graphics/RectF;
    .restart local v11    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v14    # "outRect":Landroid/graphics/RectF;
    .restart local v15    # "resizedBitmap":Landroid/graphics/Bitmap;
    .restart local v16    # "roughBitmap":Landroid/graphics/Bitmap;
    .restart local v17    # "roughtHeight":I
    .restart local v18    # "roughtWidth":I
    .restart local v19    # "scaleX":F
    .restart local v20    # "scaleY":F
    .restart local v21    # "transformationMatrix":Landroid/graphics/Matrix;
    .restart local v22    # "values":[F
    :catchall_2
    move-exception v23

    move-object v12, v13

    .end local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "out":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    goto :goto_2

    .line 139
    .end local v6    # "in":Ljava/io/InputStream;
    .end local v8    # "inRect":Landroid/graphics/RectF;
    .end local v11    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "outRect":Landroid/graphics/RectF;
    .end local v15    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "roughBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "roughtHeight":I
    .end local v18    # "roughtWidth":I
    .end local v19    # "scaleX":F
    .end local v20    # "scaleY":F
    .end local v21    # "transformationMatrix":Landroid/graphics/Matrix;
    .end local v22    # "values":[F
    .restart local v7    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v4

    move-object v6, v7

    .end local v7    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    goto :goto_1

    .end local v6    # "in":Ljava/io/InputStream;
    .end local v12    # "out":Ljava/io/FileOutputStream;
    .restart local v7    # "in":Ljava/io/InputStream;
    .restart local v8    # "inRect":Landroid/graphics/RectF;
    .restart local v11    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v14    # "outRect":Landroid/graphics/RectF;
    .restart local v15    # "resizedBitmap":Landroid/graphics/Bitmap;
    .restart local v16    # "roughBitmap":Landroid/graphics/Bitmap;
    .restart local v17    # "roughtHeight":I
    .restart local v18    # "roughtWidth":I
    .restart local v19    # "scaleX":F
    .restart local v20    # "scaleY":F
    .restart local v21    # "transformationMatrix":Landroid/graphics/Matrix;
    .restart local v22    # "values":[F
    :catch_2
    move-exception v4

    move-object v12, v13

    .end local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "out":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 11
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    .line 71
    const/high16 v2, -0x40800000    # -1.0f

    .line 72
    .local v2, "dstWidth":F
    const/high16 v0, -0x40800000    # -1.0f

    .line 73
    .local v0, "dstHeight":F
    const/high16 v7, -0x40800000    # -1.0f

    .line 74
    .local v7, "inWidth":F
    const/high16 v6, -0x40800000    # -1.0f

    .line 75
    .local v6, "inHeight":F
    const/4 v4, 0x0

    .line 77
    .local v4, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    invoke-direct {v5, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    .end local v4    # "in":Ljava/io/InputStream;
    .local v5, "in":Ljava/io/InputStream;
    :try_start_1
    new-instance v8, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v8}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 79
    .local v8, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v9, 0x1

    iput-boolean v9, v8, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 80
    const/4 v9, 0x0

    invoke-static {v5, v9, v8}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 82
    iget v9, v8, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v7, v9

    .line 83
    iget v9, v8, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v6, v9

    .line 85
    iget v9, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    int-to-float v9, v9

    cmpg-float v9, v7, v9

    if-gtz v9, :cond_0

    iget v9, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    int-to-float v9, v9

    cmpg-float v9, v6, v9

    if-gtz v9, :cond_0

    .line 86
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mInputImagePath:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    invoke-direct {p0, v9, v10}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    .line 96
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    move-object v4, v5

    .line 98
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v8    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v4    # "in":Ljava/io/InputStream;
    :goto_0
    return-object v9

    .line 89
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v8    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    :try_start_2
    iget v9, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mMaxSize:I

    invoke-direct {p0, v7, v6, v9}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->getScaledSize(FFI)Landroid/graphics/PointF;

    move-result-object v1

    .line 90
    .local v1, "dstSize":Landroid/graphics/PointF;
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 91
    iget v0, v1, Landroid/graphics/PointF;->y:F
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 96
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    .line 98
    invoke-direct {p0, v2, v0, v7, v6}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->resizeAndTransformBitmap(FFFF)Ljava/lang/Boolean;

    move-result-object v9

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_0

    .line 92
    .end local v1    # "dstSize":Landroid/graphics/PointF;
    .end local v8    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v3

    .line 93
    .local v3, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    sget-object v9, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->LOG_TAG:Ljava/lang/String;

    invoke-static {v9, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v9

    .line 96
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_2
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStream(Ljava/io/Closeable;)V

    throw v9

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_2

    .line 92
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v3

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getOutputImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->mOutputImagePath:Ljava/lang/String;

    return-object v0
.end method
