.class public Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "LibraryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private mCategoryListData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryTopArticleListData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryListData:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryTopArticleListData:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryListData:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryTopArticleListData:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getLibraryCategroys(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 44
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryTopArticleListData:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 45
    .local v2, "listDataSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 46
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryTopArticleListData:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 47
    .local v1, "libraryHomeArticleTipData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getCategory()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryImageResource;->getLibraryArticleImageResource(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setBackground(I)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    .end local v1    # "libraryHomeArticleTipData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 121
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 122
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->setHasOptionsMenu(Z)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->setActionBarNormalMode()V

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v6, 0x7f03005e

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 57
    .local v2, "contentView":Landroid/view/View;
    const v5, 0x7f0801d1

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;

    .line 58
    .local v0, "articleTipViewPager":Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryTopArticleListData:Ljava/util/ArrayList;

    invoke-direct {v5, v6, v7}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeArticleTipPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryIndicatorViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 60
    const v5, 0x7f0801d2

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 61
    .local v1, "categoryList":Landroid/widget/ListView;
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->mCategoryListData:Ljava/util/ArrayList;

    invoke-direct {v5, v6, v7}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 62
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 64
    const v5, 0x7f0801d0

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 65
    .local v3, "groupFeatured":Landroid/view/View;
    const v5, 0x7f0801c5

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 66
    .local v4, "groupFeaturedTitle":Landroid/widget/TextView;
    const v5, 0x7f09030e

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 72
    return-object v2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/high16 v5, 0x24000000

    .line 133
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    .line 135
    .local v0, "categoryData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v2, "subCategoryListData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getId()I

    move-result v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getLibrarySubCategory(ILjava/util/ArrayList;)V

    .line 138
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 140
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleListActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 141
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 142
    const-string v3, "extra_name_category_id"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getId()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 143
    const-string v4, "extra_name_sub_category_id"

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getId()I

    move-result v3

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 144
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->startActivity(Landroid/content/Intent;)V

    .line 151
    :goto_0
    return-void

    .line 146
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/library/SubCategoryListActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 148
    const-string v3, "extra_name_category_id"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryBaseListData;->getId()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 149
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public setActionBarNormalMode()V
    .locals 6

    .prologue
    const v4, 0x7f090c55

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    .line 78
    .local v2, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v2, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 82
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 83
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;)V

    .line 102
    .local v1, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207be

    invoke-direct {v0, v3, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 103
    .local v0, "actionBarButtonFavoriteBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 104
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 105
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method
