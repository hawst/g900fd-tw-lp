.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 52

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->disconnectToastReq:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2002(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v1

    const/16 v2, 0x2718

    if-ne v1, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$deviceID:Ljava/lang/String;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDevice(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->arg2:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_9

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x369d

    aput v50, v2, v49

    const/16 v49, -0x45

    aput v49, v2, v48

    const/16 v48, -0x3

    aput v48, v2, v47

    const/16 v47, -0x5cf3

    aput v47, v2, v46

    const/16 v46, -0x30

    aput v46, v2, v45

    const/16 v45, -0x6

    aput v45, v2, v44

    const/16 v44, -0x19

    aput v44, v2, v43

    const/16 v43, -0x8c9

    aput v43, v2, v42

    const/16 v42, -0x6e

    aput v42, v2, v41

    const/16 v41, -0x6

    aput v41, v2, v40

    const/16 v40, 0x240b

    aput v40, v2, v39

    const/16 v39, -0x58f6

    aput v39, v2, v38

    const/16 v38, -0x2c

    aput v38, v2, v37

    const/16 v37, -0x700

    aput v37, v2, v36

    const/16 v36, -0x70

    aput v36, v2, v35

    const/16 v35, -0x75

    aput v35, v2, v34

    const/16 v34, 0x60b

    aput v34, v2, v33

    const/16 v33, -0x3196

    aput v33, v2, v32

    const/16 v32, -0x42

    aput v32, v2, v31

    const/16 v31, -0x2e

    aput v31, v2, v30

    const/16 v30, 0x7641

    aput v30, v2, v29

    const/16 v29, -0x6cfe

    aput v29, v2, v28

    const/16 v28, -0x1

    aput v28, v2, v27

    const/16 v27, -0x1b

    aput v27, v2, v26

    const/16 v26, -0x6d00

    aput v26, v2, v25

    const/16 v25, -0x5

    aput v25, v2, v24

    const/16 v24, 0x4e60

    aput v24, v2, v23

    const/16 v23, -0x23a0

    aput v23, v2, v22

    const/16 v22, -0x54

    aput v22, v2, v21

    const/16 v21, 0x318

    aput v21, v2, v20

    const/16 v20, 0x1062

    aput v20, v2, v19

    const/16 v19, 0x523e

    aput v19, v2, v18

    const/16 v18, 0x6536

    aput v18, v2, v17

    const/16 v17, 0x470c

    aput v17, v2, v16

    const/16 v16, 0xc28

    aput v16, v2, v15

    const/16 v15, -0x82

    aput v15, v2, v14

    const/16 v14, -0x65

    aput v14, v2, v13

    const/16 v13, -0x38c6

    aput v13, v2, v12

    const/16 v12, -0x5a

    aput v12, v2, v11

    const/16 v11, -0x30

    aput v11, v2, v10

    const/16 v10, -0x1c

    aput v10, v2, v9

    const/16 v9, 0x7a09

    aput v9, v2, v8

    const/16 v8, -0x58f7

    aput v8, v2, v7

    const/16 v7, -0x77

    aput v7, v2, v6

    const/16 v6, -0x5ff5

    aput v6, v2, v5

    const/16 v5, -0x31

    aput v5, v2, v3

    const/16 v3, -0x52fc

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, -0x36f4

    aput v51, v1, v50

    const/16 v50, -0x37

    aput v50, v1, v49

    const/16 v49, -0x73

    aput v49, v1, v48

    const/16 v48, -0x5c98

    aput v48, v1, v47

    const/16 v47, -0x5d

    aput v47, v1, v46

    const/16 v46, -0x6d

    aput v46, v1, v45

    const/16 v45, -0x7c

    aput v45, v1, v44

    const/16 v44, -0x8bb

    aput v44, v1, v43

    const/16 v43, -0x9

    aput v43, v1, v42

    const/16 v42, -0x7e

    aput v42, v1, v41

    const/16 v41, 0x246e

    aput v41, v1, v40

    const/16 v40, -0x58dc

    aput v40, v1, v39

    const/16 v39, -0x59

    aput v39, v1, v38

    const/16 v38, -0x692

    aput v38, v1, v37

    const/16 v37, -0x7

    aput v37, v1, v36

    const/16 v36, -0x14

    aput v36, v1, v35

    const/16 v35, 0x67e

    aput v35, v1, v34

    const/16 v34, -0x31fa

    aput v34, v1, v33

    const/16 v33, -0x32

    aput v33, v1, v32

    const/16 v32, -0x4

    aput v32, v1, v31

    const/16 v31, 0x7629

    aput v31, v1, v30

    const/16 v30, -0x6c8a

    aput v30, v1, v29

    const/16 v29, -0x6d

    aput v29, v1, v28

    const/16 v28, -0x7c

    aput v28, v1, v27

    const/16 v27, -0x6c9b

    aput v27, v1, v26

    const/16 v26, -0x6d

    aput v26, v1, v25

    const/16 v25, 0x4e13

    aput v25, v1, v24

    const/16 v24, -0x23b2

    aput v24, v1, v23

    const/16 v23, -0x24

    aput v23, v1, v22

    const/16 v22, 0x368

    aput v22, v1, v21

    const/16 v21, 0x1003

    aput v21, v1, v20

    const/16 v20, 0x5210

    aput v20, v1, v19

    const/16 v19, 0x6552

    aput v19, v1, v18

    const/16 v18, 0x4765

    aput v18, v1, v17

    const/16 v17, 0xc47

    aput v17, v1, v16

    const/16 v16, -0xf4

    aput v16, v1, v15

    const/4 v15, -0x1

    aput v15, v1, v14

    const/16 v14, -0x38ac

    aput v14, v1, v13

    const/16 v13, -0x39

    aput v13, v1, v12

    const/4 v12, -0x2

    aput v12, v1, v11

    const/16 v11, -0x79

    aput v11, v1, v10

    const/16 v10, 0x7a6c

    aput v10, v1, v9

    const/16 v9, -0x5886

    aput v9, v1, v8

    const/16 v8, -0x59

    aput v8, v1, v7

    const/16 v7, -0x5f9a

    aput v7, v1, v6

    const/16 v6, -0x60

    aput v6, v1, v5

    const/16 v5, -0x5299

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, 0x6127

    aput v9, v2, v8

    const/16 v8, -0x42af

    aput v8, v2, v7

    const/16 v7, -0x12

    aput v7, v2, v6

    const/16 v6, -0x11c3

    aput v6, v2, v3

    const/16 v3, -0x5a

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/16 v10, 0x6115

    aput v10, v1, v9

    const/16 v9, -0x429f

    aput v9, v1, v8

    const/16 v8, -0x43

    aput v8, v1, v7

    const/16 v7, -0x1191

    aput v7, v1, v6

    const/16 v6, -0x12

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_5
    return-void

    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->isFromPairedDB()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x18

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v7, -0x49

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v7, v1

    if-lt v3, v7, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v7, v1

    if-lt v3, v7, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->deletePairedDeviceInDb(Landroid/content/Context;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$deviceID:Ljava/lang/String;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDevice(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_2
    :try_start_2
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_3
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$deviceID:Ljava/lang/String;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDevice(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_7
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_8
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18$3;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$18;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x2087

    aput v50, v2, v49

    const/16 v49, -0x53

    aput v49, v2, v48

    const/16 v48, -0x61

    aput v48, v2, v47

    const/16 v47, 0x642e

    aput v47, v2, v46

    const/16 v46, 0x3e17

    aput v46, v2, v45

    const/16 v45, -0x2ea9

    aput v45, v2, v44

    const/16 v44, -0x4e

    aput v44, v2, v43

    const/16 v43, -0x33d9

    aput v43, v2, v42

    const/16 v42, -0x57

    aput v42, v2, v41

    const/16 v41, -0x3c

    aput v41, v2, v40

    const/16 v40, -0x19

    aput v40, v2, v39

    const/16 v39, 0x5a63

    aput v39, v2, v38

    const/16 v38, 0x4e29

    aput v38, v2, v37

    const/16 v37, 0x5520

    aput v37, v2, v36

    const/16 v36, -0x7cc4

    aput v36, v2, v35

    const/16 v35, -0x1c

    aput v35, v2, v34

    const/16 v34, 0x4840

    aput v34, v2, v33

    const/16 v33, -0x26dc

    aput v33, v2, v32

    const/16 v32, -0x57

    aput v32, v2, v31

    const/16 v31, -0x70

    aput v31, v2, v30

    const/16 v30, 0x5268

    aput v30, v2, v29

    const/16 v29, 0x26

    aput v29, v2, v28

    const/16 v28, 0x2e6c

    aput v28, v2, v27

    const/16 v27, 0x764f

    aput v27, v2, v26

    const/16 v26, 0x5213

    aput v26, v2, v25

    const/16 v25, 0x333a

    aput v25, v2, v24

    const/16 v24, -0x7c0

    aput v24, v2, v23

    const/16 v23, -0x2a

    aput v23, v2, v22

    const/16 v22, -0x65

    aput v22, v2, v21

    const/16 v21, 0x293a

    aput v21, v2, v20

    const/16 v20, 0x7348

    aput v20, v2, v19

    const/16 v19, 0x455d

    aput v19, v2, v18

    const/16 v18, -0x58df

    aput v18, v2, v17

    const/16 v17, -0x32

    aput v17, v2, v16

    const/16 v16, -0x79

    aput v16, v2, v15

    const/16 v15, -0x12

    aput v15, v2, v14

    const/16 v14, -0x5b

    aput v14, v2, v13

    const/16 v13, -0x12

    aput v13, v2, v12

    const/16 v12, -0x50

    aput v12, v2, v11

    const/16 v11, -0x20

    aput v11, v2, v10

    const/16 v10, -0x51

    aput v10, v2, v9

    const/16 v9, 0x427f

    aput v9, v2, v8

    const/16 v8, -0x40cf

    aput v8, v2, v7

    const/16 v7, -0x6f

    aput v7, v2, v6

    const/16 v6, -0x2df0

    aput v6, v2, v5

    const/16 v5, -0x43

    aput v5, v2, v3

    const/16 v3, -0x5bae

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, -0x20ea

    aput v51, v1, v50

    const/16 v50, -0x21

    aput v50, v1, v49

    const/16 v49, -0x11

    aput v49, v1, v48

    const/16 v48, 0x644b

    aput v48, v1, v47

    const/16 v47, 0x3e64

    aput v47, v1, v46

    const/16 v46, -0x2ec2

    aput v46, v1, v45

    const/16 v45, -0x2f

    aput v45, v1, v44

    const/16 v44, -0x33ab

    aput v44, v1, v43

    const/16 v43, -0x34

    aput v43, v1, v42

    const/16 v42, -0x44

    aput v42, v1, v41

    const/16 v41, -0x7e

    aput v41, v1, v40

    const/16 v40, 0x5a4d

    aput v40, v1, v39

    const/16 v39, 0x4e5a

    aput v39, v1, v38

    const/16 v38, 0x554e

    aput v38, v1, v37

    const/16 v37, -0x7cab

    aput v37, v1, v36

    const/16 v36, -0x7d

    aput v36, v1, v35

    const/16 v35, 0x4835

    aput v35, v1, v34

    const/16 v34, -0x26b8

    aput v34, v1, v33

    const/16 v33, -0x27

    aput v33, v1, v32

    const/16 v32, -0x42

    aput v32, v1, v31

    const/16 v31, 0x5200

    aput v31, v1, v30

    const/16 v30, 0x52

    aput v30, v1, v29

    const/16 v29, 0x2e00

    aput v29, v1, v28

    const/16 v28, 0x762e

    aput v28, v1, v27

    const/16 v27, 0x5276

    aput v27, v1, v26

    const/16 v26, 0x3352

    aput v26, v1, v25

    const/16 v25, -0x7cd

    aput v25, v1, v24

    const/16 v24, -0x8

    aput v24, v1, v23

    const/16 v23, -0x15

    aput v23, v1, v22

    const/16 v22, 0x294a

    aput v22, v1, v21

    const/16 v21, 0x7329

    aput v21, v1, v20

    const/16 v20, 0x4573

    aput v20, v1, v19

    const/16 v19, -0x58bb

    aput v19, v1, v18

    const/16 v18, -0x59

    aput v18, v1, v17

    const/16 v17, -0x18

    aput v17, v1, v16

    const/16 v16, -0x64

    aput v16, v1, v15

    const/16 v15, -0x3f

    aput v15, v1, v14

    const/16 v14, -0x80

    aput v14, v1, v13

    const/16 v13, -0x2f

    aput v13, v1, v12

    const/16 v12, -0x32

    aput v12, v1, v11

    const/16 v11, -0x34

    aput v11, v1, v10

    const/16 v10, 0x421a

    aput v10, v1, v9

    const/16 v9, -0x40be

    aput v9, v1, v8

    const/16 v8, -0x41

    aput v8, v1, v7

    const/16 v7, -0x2d83

    aput v7, v1, v6

    const/16 v6, -0x2e

    aput v6, v1, v5

    const/16 v5, -0x5bcf

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x739e

    aput v9, v2, v8

    const/16 v8, -0x44

    aput v8, v2, v7

    const/16 v7, 0x4b0f

    aput v7, v2, v6

    const/16 v6, -0x1de7

    aput v6, v2, v3

    const/16 v3, -0x56

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/16 v10, -0x73b0

    aput v10, v1, v9

    const/16 v9, -0x74

    aput v9, v1, v8

    const/16 v8, 0x4b4e

    aput v8, v1, v7

    const/16 v7, -0x1db5

    aput v7, v1, v6

    const/16 v6, -0x1e

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v6, v1

    if-lt v3, v6, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v6, v1

    if-lt v3, v6, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_a
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_b
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_c
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_d
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b
.end method
