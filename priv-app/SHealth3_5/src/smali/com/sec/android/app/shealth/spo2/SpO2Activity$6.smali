.class Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;
.super Ljava/lang/Object;
.source "SpO2Activity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelButtonClick(Landroid/app/Activity;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    .line 276
    return-void
.end method
