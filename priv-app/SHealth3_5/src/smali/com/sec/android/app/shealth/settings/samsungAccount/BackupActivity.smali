.class public Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "BackupActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$11;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;
    }
.end annotation


# static fields
.field private static final BACKUP_FAILED_NETWORK:Ljava/lang/String; = "backup_failed_network"

.field private static final BACKUP_PROGRESS_POPUP:Ljava/lang/String; = "backup_progress_popup"

.field private static final LOW_STORAGE_FAILED_POPUP:Ljava/lang/String; = "LOW_STORAGE_FAILED_POPUP"

.field private static final SERVER_MAINTENANCE_FAILED_POPUP:Ljava/lang/String; = "SERVER_MAINTENANCE_FAILED_POPUP"


# instance fields
.field private SERVER_ERROR_MESSAGE:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private isDeviceStorageLow:Z

.field private isServerUnderMaintenance:Z

.field private mBackupProgress:Landroid/widget/TextView;

.field private mBackupProgressBar:Landroid/widget/ProgressBar;

.field private mBackupProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mCignaBackupRestoreEnable:Z

.field private mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mIsNetworkErrorOccured:Z

.field private mTextPercentage:Landroid/widget/TextView;

.field private netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field private shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 43
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;

    .line 57
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mIsNetworkErrorOccured:Z

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isDeviceStorageLow:Z

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isServerUnderMaintenance:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mCignaBackupRestoreEnable:Z

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mIsNetworkErrorOccured:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mCignaBackupRestoreEnable:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgress:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->startBackup()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Landroid/widget/ProgressBar;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBar:Landroid/widget/ProgressBar;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mTextPercentage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isDeviceStorageLow:Z

    return p1
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isServerUnderMaintenance:Z

    return p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    return-object p1
.end method

.method private initLayout()V
    .locals 3

    .prologue
    const v2, 0x7f080089

    .line 116
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 135
    const v0, 0x7f080087

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    return-void
.end method

.method private showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
    .locals 5
    .param p1, "backupStatus"    # Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    .prologue
    const v3, 0x7f090047

    const v2, 0x7f090879

    const/4 v4, 0x0

    .line 384
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    if-ne p1, v1, :cond_1

    .line 386
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 387
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090800

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 388
    const v1, 0x7f0907f8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 389
    const v1, 0x7f09004b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 390
    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 391
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 399
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 400
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "backup_failed_network"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 470
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->START:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    if-ne p1, v1, :cond_3

    .line 404
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 407
    :cond_2
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 410
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v2, 0x7f030017

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$6;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 426
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 427
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$7;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)V

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "backup_progress_popup"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 437
    :cond_3
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    if-ne p1, v1, :cond_4

    .line 438
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 439
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 440
    const v1, 0x7f0907f2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 441
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 442
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$8;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 450
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 451
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "LOW_STORAGE_FAILED_POPUP"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 452
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isDeviceStorageLow:Z

    goto :goto_0

    .line 453
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_4
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    if-ne p1, v1, :cond_0

    .line 454
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 455
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 457
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 458
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$9;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 466
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "SERVER_MAINTENANCE_FAILED_POPUP"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 468
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isServerUnderMaintenance:Z

    goto/16 :goto_0
.end method

.method private startBackup()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :goto_0
    return-void

    .line 352
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->stopBackup()V

    .line 355
    const v1, 0x7f080089

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 357
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mIsNetworkErrorOccured:Z

    .line 358
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    goto :goto_0
.end method


# virtual methods
.method cignaBackup()Z
    .locals 4

    .prologue
    .line 150
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;-><init>(Landroid/content/Context;)V

    .line 152
    .local v0, "helper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)V

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->backup(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v1

    .line 209
    .local v1, "result":Z
    if-nez v1, :cond_0

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;

    const-string v3, "FAILED <in backupActivity> "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_0
    return v1
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090879

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 112
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 517
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method public getInterval(J)Ljava/lang/String;
    .locals 8
    .param p1, "mIntervalSecond"    # J

    .prologue
    const-wide/32 v6, 0x15180

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 508
    div-long v0, p1, v6

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 509
    const v0, 0x7f090d23

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    div-long v2, p1, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 511
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f090d24

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-wide/16 v2, 0xe10

    div-long v2, p1, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    const v2, 0x7f030016

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->setContentView(I)V

    .line 82
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_0

    .line 83
    const v2, 0x7f090d1f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "tmpString":Ljava/lang/String;
    const-string v2, "Wi-Fi"

    const-string v3, "WLAN"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 85
    const v2, 0x7f080086

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    .end local v1    # "tmpString":Ljava/lang/String;
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 89
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->initLayout()V

    .line 93
    if-eqz p1, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "backup_progress_popup"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 96
    .local v0, "backupProgressDialog":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->stopBackup()V

    .line 99
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 104
    .end local v0    # "backupProgressDialog":Landroid/support/v4/app/DialogFragment;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mCignaBackupRestoreEnable:Z

    .line 105
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 497
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 503
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 505
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 475
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 477
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 479
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 480
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 481
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->startActivity(Landroid/content/Intent;)V

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->finish()V

    .line 485
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mIsNetworkErrorOccured:Z

    if-eqz v1, :cond_2

    .line 486
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    .line 492
    :cond_1
    :goto_0
    return-void

    .line 487
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isDeviceStorageLow:Z

    if-eqz v1, :cond_3

    .line 488
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    goto :goto_0

    .line 489
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->isServerUnderMaintenance:Z

    if-eqz v1, :cond_1

    .line 490
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    goto :goto_0
.end method

.method public stopBackup()V
    .locals 3

    .prologue
    .line 366
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopBackupOrRestore called for Backup work"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 369
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 370
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mBackupProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 373
    :cond_0
    const v1, 0x7f080089

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 376
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->stopBackup()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 380
    :goto_0
    return-void

    .line 377
    :catch_0
    move-exception v0

    .line 378
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
