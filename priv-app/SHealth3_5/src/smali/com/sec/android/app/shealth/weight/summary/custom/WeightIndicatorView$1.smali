.class Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "WeightIndicatorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initializeAnimators()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    # invokes: Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->isGoalSet()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->access$100(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    # getter for: Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAlphaAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->access$200(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 105
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$1;->this$0:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    # getter for: Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->access$000(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->resetView()V

    .line 98
    return-void
.end method
