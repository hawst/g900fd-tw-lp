.class public abstract Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "SuggestedActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field protected mCommentView:Landroid/widget/TextView;

.field protected mEmptyLayout:Landroid/widget/RelativeLayout;

.field protected mHeaderTitleView:Landroid/widget/TextView;

.field protected mHeaderView:Landroid/view/View;

.field protected mListData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation
.end field

.field protected mMoreBtn:Landroid/widget/Button;

.field protected mNoSuggestDescView:Landroid/widget/TextView;

.field protected mNoSuggestTextView:Landroid/widget/TextView;

.field protected mReassessBtn:Landroid/widget/LinearLayout;

.field protected mSuggestMoreLayout:Landroid/widget/LinearLayout;

.field protected mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

.field protected mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public clearAllData()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 250
    :cond_0
    return-void
.end method

.method protected abstract getAdapter()Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 44
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f03007c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->setContentView(I)V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->getAdapter()Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    .line 54
    const v0, 0x7f080252

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mEmptyLayout:Landroid/widget/RelativeLayout;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mEmptyLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08024b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mNoSuggestTextView:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mEmptyLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08024c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mNoSuggestDescView:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mEmptyLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08024f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mReassessBtn:Landroid/widget/LinearLayout;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mReassessBtn:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    const v0, 0x7f080251

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    .line 68
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030081

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mHeaderView:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mHeaderView:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mHeaderView:Landroid/view/View;

    const v1, 0x7f08026e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mHeaderTitleView:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mHeaderView:Landroid/view/View;

    const v1, 0x7f080270

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mCommentView:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mHeaderView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->addHeaderView(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mEmptyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setEmptyView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030054

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setHeaderView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 84
    const v0, 0x7f080253

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mSuggestMoreLayout:Landroid/widget/LinearLayout;

    .line 85
    const v0, 0x7f080254

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mMoreBtn:Landroid/widget/Button;

    .line 86
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->setHeaderMessage()V

    .line 92
    return-void
.end method

.method public reassessForSuggest()V
    .locals 2

    .prologue
    .line 253
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 254
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->startActivity(Landroid/content/Intent;)V

    .line 256
    return-void
.end method

.method public removeItem(ILjava/lang/String;)V
    .locals 5
    .param p1, "category"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v0, "existData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 226
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 228
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 232
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getTitleString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 233
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 238
    :cond_1
    new-instance v2, Landroid/util/Pair;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    iget-object v4, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-direct {v2, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 241
    .local v2, "modifiedData":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 242
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedActivity;->mListData:Ljava/util/List;

    invoke-interface {v4, p1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 244
    return-void

    .line 231
    .end local v2    # "modifiedData":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;"
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected setHeaderMessage()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method protected updateList()V
    .locals 0

    .prologue
    .line 259
    return-void
.end method
