.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;)V
    .locals 0

    .prologue
    .line 1190
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 1195
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkRestoreData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    .line 1230
    :goto_0
    return-void

    .line 1197
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1200
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isConfirmed:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1202
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showPopupWithProgressBar()V

    .line 1203
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startImport()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 1207
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkImportData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 1213
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isConfirmed:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1215
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isBackup:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startBackup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0

    .line 1221
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startRestore()V

    goto :goto_0

    .line 1226
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$12;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->checkRestoreData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    goto :goto_0
.end method
