.class Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;
.super Ljava/lang/Object;
.source "SetWallpaperContentInitializationListener.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;

.field final synthetic val$dialog:Landroid/app/Dialog;

.field final synthetic val$parentActivity:Landroid/app/Activity;

.field final synthetic val$requestInfos:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;Ljava/util/List;Landroid/app/Dialog;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->this$0:Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;

    iput-object p2, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->val$requestInfos:Ljava/util/List;

    iput-object p3, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->val$dialog:Landroid/app/Dialog;

    iput-object p4, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->val$parentActivity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->val$requestInfos:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;

    iget-object v1, v1, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;->intent:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 91
    .local v0, "targetIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->val$parentActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;->val$requestInfos:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;

    iget v1, v1, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;->requestCode:I

    invoke-virtual {v2, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 93
    return-void
.end method
