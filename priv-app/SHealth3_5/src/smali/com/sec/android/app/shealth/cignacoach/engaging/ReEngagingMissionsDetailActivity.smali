.class public Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "ReEngagingMissionsDetailActivity.java"


# static fields
.field private static final view_one_bt:I = 0x0

.field private static final view_two_bt:I = 0x1


# instance fields
.field private final TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

.field private mFailedGoalId:I

.field private mFailedMissionId:I

.field private mReasonMessage:Ljava/lang/String;

.field private mState:I

.field private viewId:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 21
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const-string v1, "#00000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->TRANSPARENT_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mReasonMessage:Ljava/lang/String;

    .line 27
    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedGoalId:I

    .line 28
    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedMissionId:I

    .line 29
    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mState:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedGoalId:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedMissionId:I

    return v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 69
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 70
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f030077

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->setContentView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "state_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "state_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->viewId:I

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "reason"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mReasonMessage:Ljava/lang/String;

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedGoalId:I

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mFailedMissionId:I

    .line 47
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->viewId:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->viewId:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->viewId:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 48
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->showView(I)V

    .line 56
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->customizeActionBar()V

    .line 57
    return-void

    .line 51
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->showView(I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 62
    return-void
.end method

.method public showView(I)V
    .locals 9
    .param p1, "state"    # I

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 74
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mState:I

    if-ne v6, p1, :cond_0

    .line 153
    :goto_0
    return-void

    .line 76
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mState:I

    .line 78
    const v6, 0x7f08023e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 79
    .local v1, "description":Landroid/widget/TextView;
    const v6, 0x7f080242

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 80
    .local v3, "one_btn_view":Landroid/widget/LinearLayout;
    const v6, 0x7f08023f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 82
    .local v5, "two_btn_view":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mReasonMessage:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->mState:I

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 86
    :pswitch_0
    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 87
    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    const v6, 0x7f080243

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 99
    .local v4, "reengagingBtn":Landroid/widget/Button;
    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)V

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 113
    .end local v4    # "reengagingBtn":Landroid/widget/Button;
    :pswitch_1
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114
    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    const v6, 0x7f080240

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 117
    .local v2, "newBtn":Landroid/widget/Button;
    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)V

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const v6, 0x7f080241

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 130
    .local v0, "againBtn":Landroid/widget/Button;
    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/engaging/ReEngagingMissionsDetailActivity;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
