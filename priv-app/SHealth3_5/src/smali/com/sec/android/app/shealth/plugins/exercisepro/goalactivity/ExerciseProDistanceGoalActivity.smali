.class public Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProDistanceGoalActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private averageValueTextView:Landroid/widget/TextView;

.field private bestRecordValueTextView:Landroid/widget/TextView;

.field private distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

.field private doneButtonListener:Landroid/view/View$OnClickListener;

.field private goalValue:I

.field private goaltitle:Landroid/widget/TextView;

.field private isUnitKm:Z

.field private mActivityType:I

.field protected recommendedValue:I

.field private unit:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 63
    const/16 v0, 0x4653

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->mActivityType:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->checkDataAndSave()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->checkChangesAndExit()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    return p1
.end method

.method private checkChangesAndExit()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 286
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getGoalValueInMetersFromEditText()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    .line 288
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isDataChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->showResetPopup()V

    .line 292
    :goto_0
    return-void

    .line 291
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->finish()V

    goto :goto_0
.end method

.method private checkDataAndSave()V
    .locals 6

    .prologue
    const v5, 0xf3e58

    const/16 v4, 0x64

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getGoalValueInMetersFromEditText()I

    move-result v1

    .line 100
    .local v1, "resultGoal":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->isValueEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    if-gt v1, v5, :cond_0

    if-ge v1, v4, :cond_2

    .line 102
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->showAbnormalGoalWarningPopup()V

    .line 116
    :cond_1
    :goto_0
    return-void

    .line 104
    :cond_2
    if-lt v1, v4, :cond_1

    if-gt v1, v5, :cond_1

    .line 105
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getCurrentDistance()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->showUnderGoalWarningPopup()V

    goto :goto_0

    .line 108
    :cond_3
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->saveGoalAndFinish()V

    .line 110
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    int-to-long v3, v3

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getKmFromMeterByLocale(J)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "goalString":Ljava/lang/String;
    :goto_1
    const v3, 0x7f090f72

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    .line 112
    .local v2, "toast":Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 110
    .end local v0    # "goalString":Ljava/lang/String;
    .end local v2    # "toast":Landroid/widget/Toast;
    :cond_4
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    int-to-long v3, v3

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getMilesFromMetersByLocale(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private getGoalValueInMetersFromEditText()I
    .locals 3

    .prologue
    .line 297
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v1

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->getFloatFromString(Ljava/lang/String;)F

    move-result v1

    float-to-int v0, v1

    .line 303
    .local v0, "value":I
    :goto_0
    return v0

    .line 300
    .end local v0    # "value":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->getFloatFromString(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->convertMilesToMeters(F)I

    move-result v0

    .restart local v0    # "value":I
    goto :goto_0
.end method

.method private initHorizontalInputModule()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v4, 0x3dcccccd    # 0.1f

    .line 323
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v5, :cond_0

    .line 324
    const v3, 0x3dcccccd    # 0.1f

    .line 325
    .local v3, "min":F
    const v2, 0x4479c000    # 999.0f

    .line 331
    .local v2, "max":F
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setMoveDistance(F)V

    .line 333
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v5, v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setInputRange(FF)V

    .line 334
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v5, v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setNormalRange(FF)V

    .line 335
    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v5, :cond_1

    .line 336
    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    int-to-long v4, v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getKmFromMeterByLocale(J)Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "goal":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 338
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    .line 349
    :goto_1
    return-void

    .line 327
    .end local v1    # "goal":Ljava/lang/String;
    .end local v2    # "max":F
    .end local v3    # "min":F
    :cond_0
    const v3, 0x3dcccccd    # 0.1f

    .line 328
    .restart local v3    # "min":F
    const/high16 v2, 0x441b0000    # 620.0f

    .restart local v2    # "max":F
    goto :goto_0

    .line 344
    :cond_1
    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    int-to-long v5, v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getMilesFromMetersByLocale(J)Ljava/lang/String;

    move-result-object v1

    .line 345
    .restart local v1    # "goal":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 347
    .local v0, "floatValue":F
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    cmpg-float v6, v0, v4

    if-gez v6, :cond_2

    move v0, v4

    .end local v0    # "floatValue":F
    :cond_2
    invoke-virtual {v5, v0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    goto :goto_1
.end method

.method private isDataChanged()Z
    .locals 2

    .prologue
    .line 269
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->recommendedValue:I

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private saveGoalAndFinish()V
    .locals 5

    .prologue
    const v3, 0xf3e58

    const/16 v2, 0x64

    const/4 v4, 0x2

    .line 119
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    if-gt v1, v2, :cond_0

    .line 120
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    .line 122
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    if-lt v1, v3, :cond_1

    .line 123
    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    .line 125
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->mActivityType:I

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    invoke-static {p0, v1, v4, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->setActivityTypeGoalValue(Landroid/content/Context;III)I

    .line 126
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->mActivityType:I

    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeActivityGoalType(II)V

    .line 128
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->TAG:Ljava/lang/String;

    const-string v2, "Distance: saveGoalAndFinish"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v3, "G002"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 132
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "value"

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 133
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 134
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->setResult(ILandroid/content/Intent;)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->finish()V

    .line 136
    return-void
.end method

.method private setLastGoalValueToEditText()V
    .locals 3

    .prologue
    .line 307
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v1

    const-string v2, "km"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    .line 308
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->mActivityType:I

    const/4 v2, 0x2

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    .line 309
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v1, :cond_0

    .line 310
    const v1, 0x7f0900c7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    .line 314
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    const v2, 0x7f080473

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 315
    .local v0, "txt":Landroid/widget/TextView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 318
    return-void

    .line 312
    .end local v0    # "txt":Landroid/widget/TextView;
    :cond_0
    const v1, 0x7f0900cc

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    goto :goto_0
.end method

.method private setRecordTimeText()V
    .locals 15

    .prologue
    .line 368
    const-wide/16 v9, 0x0

    .local v9, "best":J
    const-wide/16 v6, 0x0

    .line 369
    .local v6, "avg":J
    const-string v8, "AVG_DISTANCE"

    .line 370
    .local v8, "avgColumn":Ljava/lang/String;
    const-string v11, "BEST_DISTANCE"

    .line 371
    .local v11, "bestColumn":Ljava/lang/String;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "distance"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "MAX(distance) AS "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "AVG(distance) AS "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 375
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise_info__id == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->mActivityType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "end_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "exercise_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " == ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "exercise_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " == ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 378
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-wide/16 v13, 0x0

    invoke-static {v13, v14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const/16 v1, 0x4e21

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const/16 v1, 0x4e22

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 381
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 382
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v12, 0x0

    .line 385
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 387
    if-eqz v12, :cond_0

    .line 388
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 389
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 390
    invoke-interface {v12, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 391
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    .line 395
    :cond_0
    if-eqz v12, :cond_1

    .line 396
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 398
    :cond_1
    long-to-float v0, v6

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->setText(FZ)V

    .line 399
    long-to-float v0, v9

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->setText(FZ)V

    .line 400
    return-void

    .line 395
    :catchall_0
    move-exception v0

    if-eqz v12, :cond_2

    .line 396
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private setText(FZ)V
    .locals 5
    .param p1, "fValue"    # F
    .param p2, "isAvg"    # Z

    .prologue
    .line 404
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v1, :cond_0

    .line 405
    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr p1, v1

    .line 406
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "0.00"

    new-instance v3, Ljava/text/DecimalFormatSymbols;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v1, v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    float-to-double v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 407
    .local v0, "value":Ljava/lang/String;
    const v1, 0x7f0900c7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    .line 413
    :goto_0
    if-eqz p2, :cond_1

    .line 415
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->averageValueTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->averageValueTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 425
    :goto_1
    return-void

    .line 409
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    float-to-long v1, p1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v0

    .line 410
    .restart local v0    # "value":Ljava/lang/String;
    const v1, 0x7f0900cc

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    goto :goto_0

    .line 421
    :cond_1
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->bestRecordValueTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->bestRecordValueTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->unit:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private showAbnormalGoalWarningPopup()V
    .locals 13

    .prologue
    const-wide/32 v11, 0xf3e58

    const-wide/16 v9, 0x64

    const/4 v8, 0x3

    const/4 v7, 0x0

    .line 200
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 202
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v3, :cond_0

    const v3, 0x7f090f6b

    :goto_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 203
    .local v2, "distType":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const v4, 0x7f090f50

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v3, :cond_1

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v3

    :goto_1
    aput-object v3, v5, v7

    const/4 v6, 0x1

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v3, :cond_2

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :goto_2
    aput-object v3, v5, v6

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 224
    .local v0, "alertTextString":Ljava/lang/StringBuilder;
    const v3, 0x7f090b60

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x20000

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 251
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 252
    return-void

    .line 202
    .end local v0    # "alertTextString":Ljava/lang/StringBuilder;
    .end local v2    # "distType":Ljava/lang/String;
    :cond_0
    const v3, 0x7f090f6c

    goto :goto_0

    .line 203
    .restart local v2    # "distType":Ljava/lang/String;
    :cond_1
    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method private showResetPopup()V
    .locals 5

    .prologue
    .line 184
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->isUnitKm:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    int-to-long v2, v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getKmFromMeterByLocale(J)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "goalString":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 187
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v2, 0x7f090f4d

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f090f4c

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f090f4e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x20000

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 195
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 196
    return-void

    .line 184
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .end local v1    # "goalString":Ljava/lang/String;
    :cond_0
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goalValue:I

    int-to-long v2, v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getMilesFromMetersByLocale(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private showUnderGoalWarningPopup()V
    .locals 4

    .prologue
    .line 255
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 257
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f0900e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090a8a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 265
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 266
    return-void
.end method


# virtual methods
.method protected calculateRecommendedValue()V
    .locals 3

    .prologue
    const/16 v2, 0x64

    .line 141
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->mActivityType:I

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->recommendedValue:I

    .line 142
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->recommendedValue:I

    if-ge v0, v2, :cond_0

    .line 143
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->recommendedValue:I

    .line 146
    :cond_0
    return-void
.end method

.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 150
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090a21

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 153
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V

    .line 174
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 176
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    invoke-direct {v2, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 177
    .local v2, "actionButton3":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 179
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090044

    invoke-direct {v1, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 180
    .local v1, "actionButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 181
    return-void
.end method

.method public getKmFromMeterByLocale(J)Ljava/lang/String;
    .locals 8
    .param p1, "distance"    # J

    .prologue
    .line 352
    long-to-double v4, p1

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double v2, v4, v6

    .line 353
    .local v2, "km":D
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    new-instance v5, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 354
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "goal":Ljava/lang/String;
    return-object v1
.end method

.method public getMilesFromMetersByLocale(J)Ljava/lang/String;
    .locals 8
    .param p1, "distance"    # J

    .prologue
    .line 360
    long-to-double v4, p1

    const-wide v6, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v2, v4, v6

    .line 361
    .local v2, "mi":D
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v4, "0.00"

    new-instance v5, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v4, v5}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 362
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 364
    .local v1, "goal":Ljava/lang/String;
    return-object v1
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 429
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->finish()V

    .line 430
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 280
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->customizeActionBar()V

    .line 282
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 66
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v0, 0x7f0301a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->setContentView(I)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pick_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pick_type"

    const/16 v2, 0x4653

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->mActivityType:I

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->calculateRecommendedValue()V

    .line 72
    const v0, 0x7f0807e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goaltitle:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->goaltitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a21

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    const v0, 0x7f080740

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setFilterText(Z)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setType(I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setEnabled(Z)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setEditTextSize(I)V

    .line 79
    const v0, 0x7f0807e4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->averageValueTextView:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0807e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->bestRecordValueTextView:Landroid/widget/TextView;

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->setLastGoalValueToEditText()V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->setRecordTimeText()V

    .line 85
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->doneButtonListener:Landroid/view/View$OnClickListener;

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->initHorizontalInputModule()V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->setEditTextSize(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->removeEditTextFocus()V

    .line 95
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 434
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->distanceInputModule:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProDistanceGoalActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 437
    :cond_0
    return-void
.end method
