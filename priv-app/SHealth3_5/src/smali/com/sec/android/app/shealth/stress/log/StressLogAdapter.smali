.class public Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.source "StressLogAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private dateFormatter:Ljava/text/SimpleDateFormat;

.field dateFormatterNew:Ljava/text/SimpleDateFormat;

.field private mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    .line 36
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 37
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    .line 41
    invoke-static {p2}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    .line 42
    return-void
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 33
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isLastChild"    # Z

    .prologue
    .line 46
    const v30, 0x7f080588

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .line 47
    .local v17, "llHeader":Landroid/widget/LinearLayout;
    const v30, 0x7f080589

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/TextView;

    .line 48
    .local v29, "tvHeaderLeft":Landroid/widget/TextView;
    const v30, 0x7f0809e6

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Lcom/sec/android/app/shealth/stress/custom/StressStateLogBar;

    .line 49
    .local v23, "stressStateBar":Lcom/sec/android/app/shealth/stress/custom/StressStateLogBar;
    const v30, 0x7f08058e

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 50
    .local v28, "tvBodyTopLeft":Landroid/widget/TextView;
    const v30, 0x7f08058f

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 51
    .local v26, "tvBodyBottomLeft":Landroid/widget/TextView;
    const v30, 0x7f080591

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    .line 52
    .local v11, "ivBodyAccessory":Landroid/widget/ImageButton;
    const v30, 0x7f080592

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 53
    .local v12, "ivBodyMemo":Landroid/widget/ImageView;
    const v30, 0x7f080593

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    .line 54
    .local v27, "tvBodyRight":Landroid/widget/TextView;
    const v30, 0x7f08058d

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 55
    .local v6, "cbBody":Landroid/widget/CheckBox;
    const v30, 0x7f08058b

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 56
    .local v16, "listTopDivider":Landroid/view/View;
    const v30, 0x7f080594

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 57
    .local v14, "listBottomDivider":Landroid/view/View;
    const v30, 0x7f0809e5

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 59
    .local v15, "listLeftSpace":Landroid/view/View;
    const-string/jumbo v30, "sample_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move-object/from16 v0, p3

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 60
    .local v24, "time":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const-string v30, "AvgMonth"

    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move-object/from16 v0, p3

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 66
    .local v3, "avgMonth":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    .line 68
    .local v8, "currentTime":Ljava/lang/String;
    move-object/from16 v20, v8

    .line 69
    .local v20, "previousRecordTimeStr":Ljava/lang/String;
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isFirst()Z

    move-result v30

    if-nez v30, :cond_0

    .line 71
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 72
    const-string/jumbo v30, "sample_time"

    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move-object/from16 v0, p3

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 73
    .local v18, "previousRecordTime":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v20

    .line 74
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 78
    .end local v18    # "previousRecordTime":J
    :goto_0
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_2

    .line 80
    move-object/from16 v0, p2

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getChildFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    const/16 v30, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 82
    const/16 v30, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v5

    .line 85
    .local v5, "cal":Ljava/util/Calendar;
    move-wide/from16 v0, v24

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 87
    new-instance v30, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v30 .. v30}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v30 .. v30}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v30

    const/16 v31, 0x2

    move/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v31

    aget-object v9, v30, v31

    .line 88
    .local v9, "dMonth":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->dateFormatterNew:Ljava/text/SimpleDateFormat;

    move-object/from16 v30, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 89
    .local v10, "dateNew":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v30

    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v13

    .line 90
    .local v13, "language":Ljava/lang/String;
    const-string v30, "ko"

    move-object/from16 v0, v30

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 91
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v31, 0x2

    move/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v31

    add-int/lit8 v31, v31, 0x1

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f090212

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f090213

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    new-instance v31, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v31 .. v31}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v31 .. v31}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v31

    const/16 v32, 0x7

    move/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v32

    aget-object v31, v31, v32

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 107
    .end local v5    # "cal":Ljava/util/Calendar;
    .end local v9    # "dMonth":Ljava/lang/String;
    .end local v10    # "dateNew":Ljava/lang/String;
    .end local v13    # "language":Ljava/lang/String;
    :goto_1
    const/16 v30, 0x1

    move/from16 v0, p4

    move/from16 v1, v30

    if-ne v0, v1, :cond_3

    const/16 v30, 0x0

    :goto_2
    move/from16 v0, v30

    invoke-virtual {v14, v0}, Landroid/view/View;->setVisibility(I)V

    .line 109
    const-string/jumbo v30, "score"

    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move-object/from16 v0, p3

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v21

    .line 110
    .local v21, "stressScore":D
    move-object/from16 v0, v23

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/custom/StressStateLogBar;->moveToPolygon(D)V

    .line 112
    const-string v30, "comment"

    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move-object/from16 v0, p3

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 113
    .local v7, "comment":Ljava/lang/String;
    if-eqz v7, :cond_4

    const-string v30, ""

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v30

    if-eqz v30, :cond_4

    .line 114
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    sget-object v30, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const v31, 0x7f09007c

    move-object/from16 v0, p2

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-static {v12, v0, v1, v7}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :goto_3
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v31, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "_"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    move-object/from16 v0, p3

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 121
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    move-object/from16 v31, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "_"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    move-object/from16 v0, p3

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 122
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 123
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x106000d

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getColor(I)I

    move-result v30

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->isMenuDeleteMode()Z

    move-result v30

    if-eqz v30, :cond_7

    .line 126
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 128
    move-object/from16 v0, v23

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/custom/StressStateLogBar;->moveToPolygon(D)V

    .line 131
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 132
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->isCheckAll()Z

    move-result v30

    if-eqz v30, :cond_5

    .line 133
    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 162
    :goto_4
    return-void

    .line 77
    .end local v7    # "comment":Ljava/lang/String;
    .end local v21    # "stressScore":D
    :cond_0
    const-string v20, ""

    goto/16 :goto_0

    .line 100
    .restart local v5    # "cal":Ljava/util/Calendar;
    .restart local v9    # "dMonth":Ljava/lang/String;
    .restart local v10    # "dateNew":Ljava/lang/String;
    .restart local v13    # "language":Ljava/lang/String;
    :cond_1
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v30

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    new-instance v31, Ljava/text/DateFormatSymbols;

    invoke-direct/range {v31 .. v31}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual/range {v31 .. v31}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v31

    const/16 v32, 0x7

    move/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v32

    aget-object v31, v31, v32

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 103
    .end local v5    # "cal":Ljava/util/Calendar;
    .end local v9    # "dMonth":Ljava/lang/String;
    .end local v10    # "dateNew":Ljava/lang/String;
    .end local v13    # "language":Ljava/lang/String;
    :cond_2
    const/16 v30, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 104
    const/16 v30, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 107
    :cond_3
    const/16 v30, 0x8

    goto/16 :goto_2

    .line 117
    .restart local v7    # "comment":Ljava/lang/String;
    .restart local v21    # "stressScore":D
    :cond_4
    const/16 v30, 0x4

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 134
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v30

    invoke-virtual {v6}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    .line 135
    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_4

    .line 137
    :cond_6
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_4

    .line 139
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->isDeleteMode()Z

    move-result v30

    if-eqz v30, :cond_a

    .line 140
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 143
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->isCheckAll()Z

    move-result v30

    if-eqz v30, :cond_8

    .line 145
    const v30, 0x7f02086c

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 146
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v30

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 147
    const v30, 0x7f02086c

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 149
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x106000d

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getColor(I)I

    move-result v30

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_4

    .line 156
    :cond_a
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 157
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 158
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 159
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_4
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    .line 166
    const v7, 0x7f080583

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 167
    .local v4, "tvFirst":Landroid/widget/TextView;
    const v7, 0x7f080586

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 168
    .local v0, "ivExpand":Landroid/widget/ImageView;
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "strLanguage":Ljava/lang/String;
    const-string/jumbo v7, "sample_time"

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 172
    .local v2, "time":J
    invoke-static {p2, v2, v3}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getYearShortMonthText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 174
    .local v6, "yearMonth":Ljava/lang/String;
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    const v7, 0x7f0809e4

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 177
    .local v5, "tv_log_child_header_layout":Landroid/view/View;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09021c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p4, :cond_0

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0901ef

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_0
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 181
    if-eqz p4, :cond_1

    .line 182
    const v7, 0x7f020845

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 183
    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b6d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-static {v0, v7, v8, v9}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :goto_1
    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 195
    return-void

    .line 177
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f090217

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 188
    :cond_1
    const v7, 0x7f02084c

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 189
    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090b6e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-static {v0, v7, v8, v9}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected getColumnNameForCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    const-string/jumbo v0, "sample_time"

    return-object v0
.end method

.method protected getColumnNameForID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    const-string v0, "_id"

    return-object v0
.end method

.method protected getTotalChildCount()I
    .locals 4

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->getGroupCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 216
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->getChildrenCount(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 217
    add-int/lit8 v0, v0, 0x1

    .line 216
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 215
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 221
    .end local v2    # "j":I
    :cond_1
    return v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isLastChild"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 199
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030235

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isExpanded"    # Z
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 204
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030234

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 237
    new-instance v0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter$1;-><init>(Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;Landroid/widget/CompoundButton;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->post(Ljava/lang/Runnable;)Z

    .line 243
    return-void
.end method
