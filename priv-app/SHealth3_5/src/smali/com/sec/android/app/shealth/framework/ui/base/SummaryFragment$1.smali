.class Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickAction(Landroid/view/View;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevSelectedDate(Ljava/util/Date;)V

    new-instance v4, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getCalendarActivityClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v4, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x43

    aput v14, v2, v13

    const/16 v13, 0x7934

    aput v13, v2, v12

    const/16 v12, 0x7100

    aput v12, v2, v11

    const/16 v11, 0x3505

    aput v11, v2, v10

    const/16 v10, -0x5796

    aput v10, v2, v9

    const/16 v9, -0x34

    aput v9, v2, v8

    const/16 v8, -0x28dd

    aput v8, v2, v7

    const/16 v7, -0x42

    aput v7, v2, v6

    const/4 v6, -0x5

    aput v6, v2, v5

    const/16 v5, -0x7b0

    aput v5, v2, v3

    const/16 v3, -0x78

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x28

    aput v15, v1, v14

    const/16 v14, 0x7944

    aput v14, v1, v13

    const/16 v13, 0x7179

    aput v13, v1, v12

    const/16 v12, 0x3571

    aput v12, v1, v11

    const/16 v11, -0x57cb

    aput v11, v1, v10

    const/16 v10, -0x58

    aput v10, v1, v9

    const/16 v9, -0x28b4

    aput v9, v1, v8

    const/16 v8, -0x29

    aput v8, v1, v7

    const/16 v7, -0x77

    aput v7, v1, v6

    const/16 v6, -0x7cb

    aput v6, v1, v5

    const/4 v5, -0x8

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getContentURI()Landroid/net/Uri;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0x50eb

    aput v12, v2, v11

    const/16 v11, -0x21

    aput v11, v2, v10

    const/16 v10, -0x3f

    aput v10, v2, v9

    const/16 v9, -0x13

    aput v9, v2, v8

    const/16 v8, -0x14

    aput v8, v2, v7

    const/16 v7, -0x4a93

    aput v7, v2, v6

    const/16 v6, -0x3f

    aput v6, v2, v5

    const/16 v5, -0x11a2

    aput v5, v2, v3

    const/16 v3, -0x76

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, -0x5090

    aput v13, v1, v12

    const/16 v12, -0x51

    aput v12, v1, v11

    const/16 v11, -0x48

    aput v11, v1, v10

    const/16 v10, -0x67

    aput v10, v1, v9

    const/16 v9, -0x4d

    aput v9, v1, v8

    const/16 v8, -0x4af4

    aput v8, v1, v7

    const/16 v7, -0x4b

    aput v7, v1, v6

    const/16 v6, -0x11c1

    aput v6, v1, v5

    const/16 v5, -0x12

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getContentURI()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getColumnNameForTime()Ljava/lang/String;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0xb

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, -0x3d

    aput v14, v2, v13

    const/16 v13, 0x4749

    aput v13, v2, v12

    const/16 v12, -0x8da

    aput v12, v2, v11

    const/16 v11, -0x67

    aput v11, v2, v10

    const/16 v10, -0x7b

    aput v10, v2, v9

    const/16 v9, -0x26bb

    aput v9, v2, v8

    const/16 v8, -0x4c

    aput v8, v2, v7

    const/16 v7, -0x69d1

    aput v7, v2, v6

    const/4 v6, -0x6

    aput v6, v2, v5

    const/16 v5, -0x3f

    aput v5, v2, v3

    const/16 v3, -0x56e7

    aput v3, v2, v1

    const/16 v1, 0xb

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, -0x5a

    aput v15, v1, v14

    const/16 v14, 0x4724

    aput v14, v1, v13

    const/16 v13, -0x8b9

    aput v13, v1, v12

    const/16 v12, -0x9

    aput v12, v1, v11

    const/16 v11, -0x26

    aput v11, v1, v10

    const/16 v10, -0x26d5

    aput v10, v1, v9

    const/16 v9, -0x27

    aput v9, v1, v8

    const/16 v8, -0x69a6

    aput v8, v1, v7

    const/16 v7, -0x6a

    aput v7, v1, v6

    const/16 v6, -0x52

    aput v6, v1, v5

    const/16 v5, -0x5686

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getColumnNameForTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_8
    const/16 v1, 0xc

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, -0x74

    aput v15, v2, v14

    const/16 v14, -0x74f4

    aput v14, v2, v13

    const/16 v13, -0x1e

    aput v13, v2, v12

    const/16 v12, -0x2c

    aput v12, v2, v11

    const/16 v11, -0x2d

    aput v11, v2, v10

    const/16 v10, -0x2b

    aput v10, v2, v9

    const/16 v9, 0x3115

    aput v9, v2, v8

    const/16 v8, 0x3c54

    aput v8, v2, v7

    const/16 v7, 0x614e

    aput v7, v2, v6

    const/16 v6, -0x7eed

    aput v6, v2, v5

    const/16 v5, -0xc

    aput v5, v2, v3

    const/16 v3, -0x1c

    aput v3, v2, v1

    const/16 v1, 0xc

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, -0x17

    aput v16, v1, v15

    const/16 v15, -0x749f

    aput v15, v1, v14

    const/16 v14, -0x75

    aput v14, v1, v13

    const/16 v13, -0x60

    aput v13, v1, v12

    const/16 v12, -0x74

    aput v12, v1, v11

    const/16 v11, -0x5f

    aput v11, v1, v10

    const/16 v10, 0x317b

    aput v10, v1, v9

    const/16 v9, 0x3c31

    aput v9, v1, v8

    const/16 v8, 0x613c

    aput v8, v1, v7

    const/16 v7, -0x7e9f

    aput v7, v1, v6

    const/16 v6, -0x7f

    aput v6, v1, v5

    const/16 v5, -0x79

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->mDateSelector:Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v4, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->REQUEST_CODE_CALENDAR:I
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->access$200()I

    move-result v2

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :catch_0
    move-exception v1

    goto/16 :goto_5

    :catch_1
    move-exception v1

    goto/16 :goto_8
.end method
