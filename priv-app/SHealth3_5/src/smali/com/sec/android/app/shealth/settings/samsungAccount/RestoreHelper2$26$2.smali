.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;)V
    .locals 0

    .prologue
    .line 1867
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPress(Landroid/app/Activity;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1872
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->closeProgressDialog()V

    .line 1873
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->cancelMigration()V

    .line 1874
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->cancelImport()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    .line 1875
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->stopBackupOrRestore()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    .line 1876
    return-void
.end method
