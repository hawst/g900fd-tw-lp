.class Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;
.super Ljava/lang/Object;
.source "SpO2Activity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v2, 0x1

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->deleteDailyData()V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->updateBpmDataView(Z)V

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iput-boolean v2, v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    .line 288
    return-void
.end method
