.class Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;
.super Ljava/lang/Object;
.source "GradationView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GradationViewOnTouchListener"
.end annotation


# instance fields
.field private mBoundsRect:Landroid/graphics/Rect;

.field private mInertiaCoordList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mPrevTouchCoord:F

.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V
    .locals 1

    .prologue
    .line 515
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 517
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mBoundsRect:Landroid/graphics/Rect;

    .line 518
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;

    .prologue
    .line 515
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V

    return-void
.end method

.method private isTouchInViewBounds(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mBoundsRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method private putTouchCoords(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getOrientationTouchCoord(Landroid/view/MotionEvent;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 597
    :cond_0
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 589
    :goto_0
    return v0

    .line 526
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 588
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    move v0, v2

    .line 589
    goto :goto_0

    .line 529
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    move-result-object v0

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->cancelAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->access$900(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->requestFocus()Z

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getHitRect(Landroid/graphics/Rect;)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->stopInertia()V

    .line 533
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->putTouchCoords(Landroid/view/MotionEvent;)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mPrevTouchCoord:F

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    goto :goto_1

    .line 538
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    .line 540
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->isTouchInViewBounds(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 542
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->putTouchCoords(Landroid/view/MotionEvent;)V

    .line 543
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mPrevTouchCoord:F

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v3, v0

    float-to-int v0, v0

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->shiftGradationView(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1300(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;I)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mPrevTouchCoord:F

    goto :goto_1

    .line 547
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    move-result-object v0

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->isRunning()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->access$800(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    goto/16 :goto_1

    .line 558
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v3, 0x2

    if-le v0, v3, :cond_3

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v4, v0

    float-to-int v0, v0

    int-to-float v0, v0

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->animateInertia(F)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->access$1400(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;F)V

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->mInertiaCoordList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 574
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    move-result-object v0

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->isRunning()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->access$800(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    :goto_3
    move v0, v1

    .line 584
    goto/16 :goto_0

    .line 571
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->moveToClosestGradation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1500(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V

    goto :goto_2

    .line 580
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;->this$0:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;->onControllerTapped(Z)V

    goto :goto_3

    .line 526
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
