.class Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;
.super Ljava/lang/Object;
.source "SleepMonitorGraphFragmentSIC.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)V
    .locals 0

    .prologue
    .line 974
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 39
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1037
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const-wide/16 v2, -0x1

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSelectedTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$702(J)J

    .line 1038
    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mIsResetNeeded:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$802(Z)Z

    .line 1040
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_4

    .line 1041
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v23

    .line 1042
    .local v23, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1044
    move-object/from16 v36, v23

    .line 1045
    .local v36, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/4 v2, 0x1

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 1046
    const/high16 v2, 0x426c0000    # 59.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget v3, v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    mul-float/2addr v2, v3

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 1047
    const/4 v2, 0x1

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 1048
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 1066
    .end local v36    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    :goto_0
    if-eqz p1, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$900(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v2

    if-eqz v2, :cond_f

    .line 1068
    const/16 v26, 0x0

    .line 1069
    .local v26, "efficiency":F
    const/16 v27, 0x0

    .line 1070
    .local v27, "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .line 1071
    .local v24, "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v38

    .line 1072
    .local v38, "valueList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    const-wide/16 v33, 0x0

    .line 1074
    .local v33, "sumValue":D
    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const/4 v2, 0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    add-double v33, v3, v6

    .line 1078
    :goto_1
    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Handler Data "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v37

    .line 1080
    .local v37, "timeStamp":Ljava/lang/Long;
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    .line 1081
    .local v32, "sleepTime":Ljava/lang/Long;
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    .line 1082
    .local v31, "sleepStartTimeL":Ljava/lang/Long;
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    .line 1083
    .local v30, "sleepRiseTimeL":Ljava/lang/Long;
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 1084
    .local v18, "motoinLess":Ljava/lang/Long;
    const/16 v19, 0x0

    .line 1087
    .local v19, "falseOverlap":Z
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v22

    .line 1088
    .local v22, "c":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_5

    .line 1089
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1100(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-double v3, v3

    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v3, v4, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    .end local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    check-cast v27, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;

    .line 1090
    .restart local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    if-eqz v27, :cond_1

    .line 1092
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getTotalTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    .line 1093
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getEfficiency()F

    move-result v26

    .line 1094
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getSleepStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    .line 1095
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getSleepRiseTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    .line 1096
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getMotionLess()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 1099
    :cond_1
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1202(J)J

    .line 1100
    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1200()J

    move-result-wide v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1101
    const/16 v2, 0xe

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1102
    const/16 v2, 0xd

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1103
    invoke-virtual/range {v22 .. v22}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1302(J)J

    .line 1105
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1106
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1107
    const/4 v2, 0x5

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1108
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1109
    invoke-virtual/range {v22 .. v22}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1402(J)J

    .line 1110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartExplanatoryNotes:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1181
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget-object v6, v2, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepMonitorInformationArea:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v7

    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_e

    move-wide/from16 v0, v33

    double-to-float v0, v0

    move/from16 v16, v0

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v17

    invoke-virtual/range {v6 .. v19}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->setData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;JJJJFLandroid/content/Context;Ljava/lang/Long;Z)V

    .line 1182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget-object v2, v2, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepMonitorInformationArea:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->refreshInformationAreaView()V

    .line 1189
    .end local v18    # "motoinLess":Ljava/lang/Long;
    .end local v19    # "falseOverlap":Z
    .end local v22    # "c":Ljava/util/Calendar;
    .end local v24    # "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .end local v26    # "efficiency":F
    .end local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    .end local v30    # "sleepRiseTimeL":Ljava/lang/Long;
    .end local v31    # "sleepStartTimeL":Ljava/lang/Long;
    .end local v32    # "sleepTime":Ljava/lang/Long;
    .end local v33    # "sumValue":D
    .end local v37    # "timeStamp":Ljava/lang/Long;
    .end local v38    # "valueList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    :cond_3
    :goto_4
    return-void

    .line 1053
    .end local v23    # "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v23

    .line 1054
    .restart local v23    # "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1056
    move-object/from16 v36, v23

    .line 1057
    .restart local v36    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/4 v2, 0x1

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 1058
    const/high16 v2, 0x41f80000    # 31.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget v3, v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    mul-float/2addr v2, v3

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 1059
    const/4 v2, 0x1

    move-object/from16 v0, v36

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 1060
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    goto/16 :goto_0

    .line 1075
    .end local v36    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .restart local v24    # "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .restart local v26    # "efficiency":F
    .restart local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    .restart local v33    # "sumValue":D
    .restart local v38    # "valueList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    :catch_0
    move-exception v25

    .line 1076
    .local v25, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v33

    goto/16 :goto_1

    .line 1111
    .end local v25    # "e":Ljava/lang/Exception;
    .restart local v18    # "motoinLess":Ljava/lang/Long;
    .restart local v19    # "falseOverlap":Z
    .restart local v22    # "c":Ljava/util/Calendar;
    .restart local v30    # "sleepRiseTimeL":Ljava/lang/Long;
    .restart local v31    # "sleepStartTimeL":Ljava/lang/Long;
    .restart local v32    # "sleepTime":Ljava/lang/Long;
    .restart local v37    # "timeStamp":Ljava/lang/Long;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_7

    .line 1112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1100(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-double v3, v3

    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v3, v4, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    .end local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    check-cast v27, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;

    .line 1113
    .restart local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    if-eqz v27, :cond_6

    .line 1115
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getTotalTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    .line 1116
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getEfficiency()F

    move-result v26

    .line 1117
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getMotionLess()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 1120
    :cond_6
    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1402(J)J

    .line 1121
    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1400()J

    move-result-wide v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1123
    const/16 v2, 0xe

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1124
    const/16 v2, 0xd

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1125
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1126
    const/16 v2, 0xb

    const/16 v3, 0xc

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1127
    const/4 v2, 0x5

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1128
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1129
    invoke-virtual/range {v22 .. v22}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1402(J)J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1202(J)J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1302(J)J

    .line 1130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartExplanatoryNotes:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 1131
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_2

    .line 1133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1100(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getAccurateSampleTime(J)J

    move-result-wide v3

    long-to-double v3, v3

    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v3, v4, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    .end local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    check-cast v27, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;

    .line 1134
    .restart local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->isfalseOverlap()Z

    move-result v19

    .line 1135
    if-eqz v27, :cond_9

    .line 1137
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT rise_time as RISE_TIME, bed_time as BED_TIME, quality AS QUALITY FROM sleep WHERE sync_status != 170004 AND _id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getSleepId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1138
    .local v5, "query":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v35

    .line 1140
    .local v35, "tempCursor":Landroid/database/Cursor;
    const-wide/16 v20, 0x0

    .local v20, "bedTime":J
    const-wide/16 v28, 0x0

    .line 1143
    .local v28, "riseTime":J
    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1144
    const-string v2, "BED_TIME"

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1145
    const-string v2, "RISE_TIME"

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v35

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    .line 1147
    :cond_8
    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->close()V

    .line 1149
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getIsFirst()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getIsLast()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1150
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    .line 1151
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    .line 1164
    .end local v5    # "query":Ljava/lang/String;
    .end local v20    # "bedTime":J
    .end local v28    # "riseTime":J
    .end local v35    # "tempCursor":Landroid/database/Cursor;
    :cond_9
    :goto_5
    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1302(J)J

    .line 1165
    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1300()J

    move-result-wide v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1167
    const/16 v2, 0xe

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1168
    const/16 v2, 0xd

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1169
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1170
    const/16 v2, 0xb

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xc

    if-lt v2, v3, :cond_a

    .line 1171
    const/4 v2, 0x5

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1173
    :cond_a
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1174
    invoke-virtual/range {v22 .. v22}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1202(J)J

    .line 1176
    const/4 v2, 0x5

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1177
    const/4 v2, 0x2

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1178
    invoke-virtual/range {v22 .. v22}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1402(J)J

    .line 1179
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartExplanatoryNotes:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 1153
    .restart local v5    # "query":Ljava/lang/String;
    .restart local v20    # "bedTime":J
    .restart local v28    # "riseTime":J
    .restart local v35    # "tempCursor":Landroid/database/Cursor;
    :cond_b
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getIsFirst()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1154
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    goto :goto_5

    .line 1156
    :cond_c
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;->getIsLast()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1157
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    goto/16 :goto_5

    .line 1159
    :cond_d
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    .line 1160
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    goto/16 :goto_5

    .end local v5    # "query":Ljava/lang/String;
    .end local v20    # "bedTime":J
    .end local v28    # "riseTime":J
    .end local v35    # "tempCursor":Landroid/database/Cursor;
    :cond_e
    move/from16 v16, v26

    .line 1181
    goto/16 :goto_3

    .line 1184
    .end local v18    # "motoinLess":Ljava/lang/Long;
    .end local v19    # "falseOverlap":Z
    .end local v22    # "c":Ljava/util/Calendar;
    .end local v24    # "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    .end local v26    # "efficiency":F
    .end local v27    # "pointDataDisplay":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;
    .end local v30    # "sleepRiseTimeL":Ljava/lang/Long;
    .end local v31    # "sleepStartTimeL":Ljava/lang/Long;
    .end local v32    # "sleepTime":Ljava/lang/Long;
    .end local v33    # "sumValue":D
    .end local v37    # "timeStamp":Ljava/lang/Long;
    .end local v38    # "valueList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    :cond_f
    if-eqz p1, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$900(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v2

    if-eqz v2, :cond_3

    .line 1185
    :cond_10
    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$1000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Handler Data NULL"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget-object v2, v2, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepMonitorInformationArea:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->dimInformationAreaView()V

    goto/16 :goto_4
.end method

.method public OnReleaseTimeOut()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1018
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_0

    .line 1019
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 1020
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 1021
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 1022
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmapWithouthandler:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 1023
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 1032
    :goto_0
    return-void

    .line 1026
    .end local v0    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 1027
    .restart local v0    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 1028
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 1029
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmapWithouthandler:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 1030
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    goto :goto_0
.end method

.method public OnVisible()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 979
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 980
    .local v0, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 981
    const/16 v2, 0xff

    invoke-virtual {v0, v2, v3, v3, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 982
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 984
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_0

    .line 985
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v1

    .line 986
    .local v1, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 993
    const/high16 v2, 0x426c0000    # 59.0f

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget v3, v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 995
    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 996
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$400(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 997
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 1014
    :goto_0
    return-void

    .line 1000
    .end local v1    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v1

    .line 1001
    .restart local v1    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 1008
    const/high16 v2, 0x41f80000    # 31.0f

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget v3, v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 1010
    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 1011
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$400(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 1012
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    goto :goto_0
.end method
