.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;
.super Ljava/lang/Object;
.source "ExerciseProTrainingEffectGuideActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckBoxCheckedListener"
.end annotation


# instance fields
.field private arrow:Landroid/widget/ImageView;

.field mAddedTextView:Landroid/widget/TextView;

.field mAddedTextView2:Landroid/widget/TextView;

.field private mAnimatedContainerView:Landroid/view/View;

.field private final mAnimationDuration:I

.field private final mCheckBoxId:I

.field private mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

.field mDetailTextView:Landroid/widget/TextView;

.field private mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

.field private mTitle:Landroid/widget/TextView;

.field private mUpperContainer:Landroid/view/View;

.field private mWithoutAnimation:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;Landroid/view/View;I)V
    .locals 2
    .param p2, "container"    # Landroid/view/View;
    .param p3, "checkBoxId"    # I

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAnimationDuration:I

    .line 201
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f0807c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f0807c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAddedTextView:Landroid/widget/TextView;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mUpperContainer:Landroid/view/View;

    const v1, 0x7f0807c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAddedTextView2:Landroid/widget/TextView;

    .line 206
    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    .line 207
    const v0, 0x7f080632

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    .line 208
    const v0, 0x7f08004d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    .line 209
    const v0, 0x7f0807c2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->arrow:Landroid/widget/ImageView;

    .line 210
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;Landroid/view/View;IZ)V
    .locals 0
    .param p2, "container"    # Landroid/view/View;
    .param p3, "checkBoxId"    # I
    .param p4, "isWithoutAnimation"    # Z

    .prologue
    .line 213
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;Landroid/view/View;I)V

    .line 214
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    .line 215
    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method private initializeDropDownAnimation()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1f4

    const v3, 0x7f0a0926

    .line 267
    const/4 v0, 0x0

    .line 268
    .local v0, "animatedViewHeight":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAddedTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAddedTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAddedTextView2:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    add-int v0, v1, v2

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAddedTextView2:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020262

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 274
    :goto_0
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener$1;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;Landroid/view/View;IZ)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->setDuration(J)V

    .line 288
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener$2;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;Landroid/view/View;IZ)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;->setDuration(J)V

    .line 308
    return-void

    .line 273
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mDetailTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int v0, v1, v2

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f09021c

    const v7, 0x7f0804e6

    const/4 v3, 0x0

    .line 220
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->arrow:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02084d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 222
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->arrow:Landroid/widget/ImageView;

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09021a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5, v9}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->arrow:Landroid/widget/ImageView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090217

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 238
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 239
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    if-eqz v2, :cond_3

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const/4 v4, -0x2

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mAnimatedContainerView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 242
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mWithoutAnimation:Z

    .line 258
    :cond_0
    return-void

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->arrow:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020846

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->arrow:Landroid/widget/ImageView;

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09021b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5, v9}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->arrow:Landroid/widget/ImageView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901ef

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    move v2, v3

    .line 238
    goto :goto_1

    .line 246
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    if-nez v2, :cond_4

    .line 247
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->initializeDropDownAnimation()V

    .line 248
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mExpandAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    :goto_2
    invoke-virtual {v2, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 250
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 252
    .local v1, "mGuideListCurrentVisibility":I
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v0, v4, :cond_5

    const/16 v1, 0x8

    .end local v1    # "mGuideListCurrentVisibility":I
    :cond_5
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 250
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 248
    .end local v0    # "i":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCollapseAnimation:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/DropDownAnime;

    goto :goto_2

    .line 255
    .restart local v0    # "i":I
    .restart local v1    # "mGuideListCurrentVisibility":I
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->mGuideList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity;)Ljava/util/List;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTrainingEffectGuideActivity$CheckBoxCheckedListener;->mCheckBoxId:I

    if-ne v0, v4, :cond_8

    move v1, v3

    .end local v1    # "mGuideListCurrentVisibility":I
    :cond_8
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method
