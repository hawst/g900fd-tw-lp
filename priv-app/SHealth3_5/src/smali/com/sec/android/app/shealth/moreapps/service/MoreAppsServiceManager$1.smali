.class Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;
.super Ljava/lang/Object;
.source "MoreAppsServiceManager.java"

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->requestMoreAppsInfo(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

.field final synthetic val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    iput-object p2, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "ReRequest"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 314
    const-string v0, "MoreAppsServiceManager"

    const-string v1, "------------ Response NetException --------------"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const-string v0, "MoreAppsServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpResCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const-string v0, "MoreAppsServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const-string v0, "MoreAppsServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "e : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getException()Ljava/lang/Exception;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const-string v0, "MoreAppsServiceManager"

    const-string v1, "-------------------------------------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const-string v0, "MoreAppsServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const-string v0, "MoreAppsServiceManager"

    const-string v1, "-------------------------------------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const-string v0, "MoreAppsServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    const-string v0, "MoreAppsServiceManager"

    const-string v1, "-------------------------------------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v0

    const/4 v1, -0x5

    if-ne v0, v1, :cond_0

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v1

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    .line 329
    :goto_0
    return-void

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v1

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # J
    .param p3, "arg1"    # I
    .param p4, "arg2"    # Ljava/lang/Object;
    .param p5, "arg3"    # Ljava/lang/Object;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    const/16 v1, 0x25d

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    .line 250
    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 12
    .param p1, "arg0"    # J
    .param p3, "privateid"    # I
    .param p4, "receivedData"    # Ljava/lang/Object;
    .param p5, "arg3"    # Ljava/lang/Object;

    .prologue
    .line 255
    if-eqz p4, :cond_2

    const-string/jumbo v8, "{}"

    move-object/from16 v0, p4

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 259
    :try_start_0
    move-object/from16 v0, p4

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    .line 261
    .local v6, "receivedDataStr":Ljava/lang/String;
    sput-object v6, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    .line 262
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    iget-object v9, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # getter for: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getMoreAppsInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$100(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->checkedJson:Ljava/lang/String;

    .line 264
    const-string v8, "MoreAppsServiceManager"

    invoke-static {v8, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 268
    .local v5, "recceivedJsonObj":Lorg/json/JSONObject;
    const-string v8, "cacheTime"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-wide/16 v1, 0x0

    .line 271
    .local v1, "cacheTime":J
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-nez v8, :cond_1

    .line 272
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # getter for: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_CACHE_TIME_KEY:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/moreapps/service/Util;->removePreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 273
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # getter for: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_TIME_KEY:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/moreapps/service/Util;->removePreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 274
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # getter for: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_JSON_KEY:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/moreapps/service/Util;->removePreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 286
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # invokes: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->makeMoreAppsInfo(Lorg/json/JSONObject;)Ljava/lang/String;
    invoke-static {v8, v5}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$200(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v7

    .line 288
    .local v7, "responseInfo":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    const/4 v9, 0x1

    invoke-interface {v8, v9, v7}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    .line 309
    .end local v1    # "cacheTime":J
    .end local v5    # "recceivedJsonObj":Lorg/json/JSONObject;
    .end local v6    # "receivedDataStr":Ljava/lang/String;
    .end local v7    # "responseInfo":Ljava/lang/String;
    :goto_2
    return-void

    .line 268
    .restart local v5    # "recceivedJsonObj":Lorg/json/JSONObject;
    .restart local v6    # "receivedDataStr":Ljava/lang/String;
    :cond_0
    const-string v8, "cacheTime"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-long v1, v8

    goto :goto_0

    .line 278
    .restart local v1    # "cacheTime":J
    :cond_1
    const-wide/32 v8, 0x36ee80

    mul-long/2addr v1, v8

    .line 279
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # getter for: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_CACHE_TIME_KEY:Ljava/lang/String;

    invoke-static {v8, v9, v1, v2}, Lcom/sec/android/app/shealth/moreapps/service/Util;->setPrefLong(Landroid/content/Context;Ljava/lang/String;J)V

    .line 280
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # getter for: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_TIME_KEY:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/app/shealth/moreapps/service/Util;->setPrefLong(Landroid/content/Context;Ljava/lang/String;J)V

    .line 281
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # getter for: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_JSON_KEY:Ljava/lang/String;

    sget-object v10, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/shealth/moreapps/service/Util;->setPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 290
    .end local v1    # "cacheTime":J
    .end local v5    # "recceivedJsonObj":Lorg/json/JSONObject;
    .end local v6    # "receivedDataStr":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 291
    .local v3, "e":Lorg/json/JSONException;
    const-string v8, "MoreAppsServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/sec/android/app/shealth/moreapps/service/Util;->convertErrString(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    const/16 v9, 0x25b

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto :goto_2

    .line 293
    .end local v3    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v3

    .line 294
    .local v3, "e":Ljava/text/ParseException;
    const-string v8, "MoreAppsServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/sec/android/app/shealth/moreapps/service/Util;->convertErrString(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    const/16 v9, 0x25c

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 296
    .end local v3    # "e":Ljava/text/ParseException;
    :catch_2
    move-exception v3

    .line 297
    .local v3, "e":Ljava/lang/NumberFormatException;
    const-string v8, "MoreAppsServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/sec/android/app/shealth/moreapps/service/Util;->convertErrString(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    const/16 v9, 0x25e

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 299
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    :catch_3
    move-exception v3

    .line 300
    .local v3, "e":Ljava/lang/Exception;
    const-string v8, "MoreAppsServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/sec/android/app/shealth/moreapps/service/Util;->convertErrString(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    const/16 v9, 0x258

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 305
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 306
    .local v4, "json":Lorg/json/JSONObject;
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->this$0:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    # invokes: Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->addLanguageText(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    invoke-static {v8, v4}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->access$300(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v4

    .line 307
    iget-object v8, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;->val$listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    const/4 v9, 0x1

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto/16 :goto_2
.end method
