.class public final Lcom/sec/android/app/shealth/spo2/common/SpO2Constants;
.super Ljava/lang/Object;
.source "SpO2Constants.java"


# static fields
.field public static final ALL:Ljava/lang/String; = "*"

.field public static final AND:Ljava/lang/String; = " AND "

.field public static final ASC:Ljava/lang/String; = " ASC"

.field public static final ASC_LIMIT_1:Ljava/lang/String; = " ASC LIMIT 1"

.field public static final AVG_PULSE:Ljava/lang/String; = "AvgPulse"

.field public static final AVG_PULSE_MONTH:Ljava/lang/String; = "AvgPulseMonth"

.field public static final AVG_SPO2:Ljava/lang/String; = "AvgSpO2"

.field public static final AVG_SPO2_MONTH:Ljava/lang/String; = "AvgSpO2Month"

.field public static final COMMENT_KEY:Ljava/lang/String; = "COMMENT_KEY"

.field public static final DATE_FORMAT_ONLY_KOREA:Ljava/lang/String; = "yyyy-MM-dd"

.field public static final DATE_KEY:Ljava/lang/String; = "Date"

.field public static final DAYS_IN_MONTH_GRAPH:Ljava/lang/String; = "STRFTIME(\"%d-%m-%Y\", C.[create_time]/1000, \'unixepoch\', \'localtime\')"

.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final DEFAULT_OF_VALUE:I = 0x0

.field public static final DESC:Ljava/lang/String; = " DESC"

.field public static final DESC_LIMIT_1:Ljava/lang/String; = " DESC LIMIT 1"

.field public static final DESC_LIMIT_2:Ljava/lang/String; = " DESC LIMIT 2"

.field public static final EQUAL_THAN:Ljava/lang/String; = "=?"

.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field public static final FIRST_OF_MONTH:I = 0x1

.field public static final FRI:I = 0x6

.field public static final GRAPH_FONT:Ljava/lang/String; = "font/Roboto-Light.ttf"

.field public static final HANDLER_MESSAGE_MEASURING:I = 0x1

.field public static final HANDLER_MESSAGE_MEASURING_END:I = 0x3

.field public static final HANDLER_MESSAGE_MEASURING_FAIL:I = 0x2

.field public static final HANDLER_MESSAGE_READY:I = 0x0

.field public static final HANDLER_NOT_SHOW_ANIMATION:I = 0x1

.field public static final HANDLER_SHOW_ANIMATION:I = 0x0

.field public static final HEART_RATE_KEY:Ljava/lang/String; = "HEART_RATE_KEY"

.field public static final HOURS_IN_DAY_BYTE:B = 0x18t

.field public static final LAST_HOUR_IN_DAY:I = 0x17

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:I = 0x3b

.field public static final LAST_SECOND_IN_MINUTE:I = 0x3b

.field public static final LESS_THAN:Ljava/lang/String; = "<?"

.field public static final MAX_CREATE_TIME:Ljava/lang/String; = "MaxCreateTime"

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MINUTES_IN_DAY_GRAPH:Ljava/lang/String; = "(C.[create_time]/(1000 * 60 *5))"

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MODE_KEY:Ljava/lang/String; = "MODE_KEY"

.field public static final MON:I = 0x2

.field public static final MONTHS_IN_YEAR_GRAPH:Ljava/lang/String; = "STRFTIME(\"%m-%Y\", C.[create_time]/1000, \'unixepoch\', \'localtime\')"

.field public static final MORE_THAN:Ljava/lang/String; = ">?"

.field public static final PERIOD_TYPE:Ljava/lang/String; = "period_type_spo2"

.field public static final REQUEST_FOR_DETAIL:I = 0x45b

.field public static final REQUEST_FOR_INFORMATION:I = 0x45a

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field public static final SPO2_EFFEET_END:I = 0x4

.field public static final SPO2_EFFEET_ERROR:I = 0x3

.field public static final SPO2_EFFEET_MEASURE:I = 0x1

.field public static final SPO2_EFFEET_START:I = 0x0

.field public static final SPO2_EFFEET_STOP:I = 0x2

.field public static final SPO2_ERROR_MAX_VALUE:I = -0x9

.field public static final SPO2_ERROR_O2DATA:F = 0.0f

.field public static final SPO2_ERROR_TIMEOUT:I = -0x6

.field public static final SPO2_FINGER_DETECTED:I = -0x1

.field public static final SPO2_ID_KEY:Ljava/lang/String; = "SPO2_ID_KEY"

.field public static final SPO2_KEY:Ljava/lang/String; = "SPO2_KEY"

.field public static final SUN:I = 0x1

.field public static final SpO2_LOGGING_APP_ID:Ljava/lang/String; = "com.sec.android.app.shealth.spo2"

.field public static final SpO2_LOGGING_CHART:Ljava/lang/String; = "SP04"

.field public static final SpO2_LOGGING_ERROR:Ljava/lang/String; = "SP05"

.field public static final SpO2_LOGGING_FINISH:Ljava/lang/String; = "SP02"

.field public static final SpO2_LOGGING_HELP:Ljava/lang/String; = "SP06"

.field public static final SpO2_LOGGING_LOG:Ljava/lang/String; = "SP03"

.field public static final SpO2_LOGGING_SHARE:Ljava/lang/String; = "SP07"

.field public static final SpO2_LOGGING_SHARE_CHART:Ljava/lang/String; = "SP10"

.field public static final SpO2_LOGGING_SHARE_LOG:Ljava/lang/String; = "SP08"

.field public static final SpO2_LOGGING_SHARE_LOG_DETAIL:Ljava/lang/String; = "SP09"

.field public static final SpO2_LOGGING_START:Ljava/lang/String; = "SP01"

.field public static final THU:I = 0x5

.field public static final TIME_DATE_KEY:Ljava/lang/String; = "TIME_DATE_KEY"

.field public static final TUE:I = 0x3

.field public static final WARNING_CHECKED:Ljava/lang/String; = "warning_checked_spo2"

.field public static final WED:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
