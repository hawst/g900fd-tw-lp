.class public Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;
.super Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;
.source "TrainingProgramWorkoutEx.java"


# static fields
.field public static final DONE:I = 0x1

.field public static final MISSED:I = 0x0

.field public static final NOT_COMPLETED:I = 0x2

.field public static final SCHEDULED:I = 0x3


# instance fields
.field public completed:I


# direct methods
.method public constructor <init>(IIIDJI)V
    .locals 1
    .param p1, "phraseNumber"    # I
    .param p2, "duration"    # I
    .param p3, "trainingEffect"    # I
    .param p4, "distance"    # D
    .param p6, "date"    # J
    .param p8, "completed"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;-><init>()V

    .line 16
    iput p1, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->phraseNumber:I

    .line 17
    iput p2, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->duration:I

    .line 18
    iput p3, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->trainingEffect:I

    .line 19
    iput-wide p4, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->distance:D

    .line 20
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 21
    .local v0, "temp":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p6, p7}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->date:Ljava/util/GregorianCalendar;

    .line 23
    iput p8, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->completed:I

    .line 24
    return-void
.end method

.method public constructor <init>(Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;)V
    .locals 2
    .param p1, "workout"    # Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;

    .prologue
    .line 26
    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;-><init>()V

    .line 27
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->phraseNumber:I

    iput v0, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->phraseNumber:I

    .line 28
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->duration:I

    iput v0, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->duration:I

    .line 29
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->trainingEffect:I

    iput v0, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->trainingEffect:I

    .line 30
    iget-wide v0, p1, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->distance:D

    iput-wide v0, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->distance:D

    .line 31
    iget-object v0, p1, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->date:Ljava/util/GregorianCalendar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->date:Ljava/util/GregorianCalendar;

    .line 32
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;->completed:I

    .line 33
    return-void
.end method
