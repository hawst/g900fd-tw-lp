.class public interface abstract Lcom/sec/android/app/shealth/backup/server/BackupNotifier$OnBackupListener;
.super Ljava/lang/Object;
.source "BackupNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/backup/server/BackupNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnBackupListener"
.end annotation


# virtual methods
.method public abstract onBackupStarted(Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;)V
.end method

.method public abstract onBackupStopped(Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;)V
.end method

.method public abstract onRestoreStarted(Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;)V
.end method

.method public abstract onRestoreStopped(Lcom/sec/android/app/shealth/backup/server/BackupNotifier$BackupType;)V
.end method
