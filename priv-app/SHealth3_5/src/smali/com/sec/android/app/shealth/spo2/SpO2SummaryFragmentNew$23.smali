.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showErrorDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 1667
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 1671
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1672
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 1673
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->errorAnimation:Landroid/view/animation/AnimationSet;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4702(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;

    .line 1675
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    .line 1676
    return-void
.end method
