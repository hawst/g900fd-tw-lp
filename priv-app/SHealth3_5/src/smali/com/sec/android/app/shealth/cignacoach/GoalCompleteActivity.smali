.class public Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "GoalCompleteActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

.field private mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

.field private mFinishDelayHandler:Landroid/os/Handler;

.field private mGoalFrequencyString:Ljava/lang/String;

.field private mGoalTitle:Ljava/lang/String;

.field private mOkBtn:Landroid/widget/Button;

.field private mOneBtnArea:Landroid/widget/RelativeLayout;

.field private mRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    .line 169
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->goScoreScreen()V

    return-void
.end method

.method private goScoreScreen()V
    .locals 3

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 164
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 166
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->startActivity(Landroid/content/Intent;)V

    .line 167
    return-void
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 77
    const v0, 0x7f08019b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    .line 79
    const v0, 0x7f08019c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    .line 80
    const v0, 0x7f08019d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mOkBtn:Landroid/widget/Button;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mOkBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    return-void
.end method

.method private updateButton()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mOkBtn:Landroid/widget/Button;

    const v1, 0x7f090c61

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mOneBtnArea:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 87
    return-void
.end method

.method private updateCompleteHeaderView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->GOAL:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setCompleteType(Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090342

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setExclamations(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090344

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mGoalTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setSubTitle(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mGoalFrequencyString:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mGoalFrequencyString:Ljava/lang/String;

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "progressMessage":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 98
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    aget-object v2, v0, v4

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setProgressMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 106
    .end local v0    # "progressMessage":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 102
    .restart local v0    # "progressMessage":[Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteHeaderView:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setProgressMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c57

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 152
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->goScoreScreen()V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->finish()V

    .line 158
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v2, 0x7f03004f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 42
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    const-string v2, "intent_complete_info_data"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 49
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    if-nez v2, :cond_0

    .line 50
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->TAG:Ljava/lang/String;

    const-string v3, "Because complete info data is null, finish ~"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->finish()V

    .line 63
    :goto_1
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    .line 45
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 55
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getGoalTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mGoalTitle:Ljava/lang/String;

    .line 56
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mCompleteInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getGoalFrequency()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mGoalFrequencyString:Ljava/lang/String;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->initLayout()V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->updateButton()V

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->updateCompleteHeaderView()V

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->setCustomActionBar()V

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 70
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setCustomActionBar()V
    .locals 6

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    .line 112
    .local v2, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v2, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 116
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 117
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)V

    .line 137
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    invoke-direct {v1, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 138
    .local v1, "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f090033

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 139
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method
