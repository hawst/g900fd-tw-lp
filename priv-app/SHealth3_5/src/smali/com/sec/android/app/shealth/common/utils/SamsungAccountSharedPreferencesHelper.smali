.class public Lcom/sec/android/app/shealth/common/utils/SamsungAccountSharedPreferencesHelper;
.super Ljava/lang/Object;
.source "SamsungAccountSharedPreferencesHelper.java"


# static fields
.field private static final BACKUP_TERMS_OF_USE:Ljava/lang/String; = "backup_terms_of_use"

.field private static final RESTORE_INITIAL_PROFILE:Ljava/lang/String; = "restore_initial_profile"

.field private static final SAMSUNG_ACCOUNT_EMAIL:Ljava/lang/String; = "email"

.field private static final SAMSUNG_ACCOUNT_SHARED_FREFFERECES:Ljava/lang/String; = "com.sec.android.app.shealth_preferences_samsung_account"

.field private static prefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSamsungAccountEmail()Ljava/lang/String;
    .locals 4

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 37
    .local v0, "de_email":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/SamsungAccountSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "email"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const-string v0, "com.sec.android.app.shealth_preferences_samsung_account"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/SamsungAccountSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    .line 45
    return-void
.end method

.method public static saveSamsungAccountEmail(Ljava/lang/String;)V
    .locals 3
    .param p0, "email"    # Ljava/lang/String;

    .prologue
    .line 21
    move-object v0, p0

    .line 22
    .local v0, "en_email":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/common/utils/SamsungAccountSharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "email"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 23
    return-void
.end method
