.class public abstract Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "HealthCareData.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private mComment:Ljava/lang/String;

.field private mInputSourceType:I

.field private mSampleTime:J

.field private mUserDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mUserDeviceId:Ljava/lang/String;

    .line 28
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mSampleTime:J

    .line 29
    const v0, 0x3f7a1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mInputSourceType:I

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mComment:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .param p1, "sampleTime"    # J

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mUserDeviceId:Ljava/lang/String;

    .line 28
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mSampleTime:J

    .line 29
    const v0, 0x3f7a1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mInputSourceType:I

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mComment:Ljava/lang/String;

    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->setSampleTime(J)V

    .line 50
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;IJJJI)V
    .locals 8
    .param p1, "id"    # J
    .param p3, "userDeviceId"    # Ljava/lang/String;
    .param p4, "comment"    # Ljava/lang/String;
    .param p5, "inputSourceType"    # I
    .param p6, "sampleTime"    # J
    .param p8, "createTime"    # J
    .param p10, "updateTime"    # J
    .param p12, "timeZone"    # I

    .prologue
    .line 85
    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p8

    move-wide/from16 v5, p10

    move/from16 v7, p12

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJI)V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mUserDeviceId:Ljava/lang/String;

    .line 28
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mSampleTime:J

    .line 29
    const v0, 0x3f7a1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mInputSourceType:I

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mComment:Ljava/lang/String;

    .line 86
    iput-object p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mComment:Ljava/lang/String;

    .line 87
    iput p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mInputSourceType:I

    .line 88
    iput-wide p6, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mSampleTime:J

    .line 89
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mUserDeviceId:Ljava/lang/String;

    .line 90
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 13
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v12

    move-object v0, p0

    invoke-direct/range {v0 .. v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;-><init>(JLjava/lang/String;Ljava/lang/String;IJJJI)V

    .line 66
    return-void
.end method


# virtual methods
.method protected checkValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-gez v0, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value cannot be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mComment:Ljava/lang/String;

    return-object v0
.end method

.method public getInputSourceType()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mInputSourceType:I

    return v0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mSampleTime:J

    return-wide v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mUserDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 148
    if-nez p1, :cond_0

    const-string p1, ""

    .end local p1    # "comment":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mComment:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setSampleTime(J)V
    .locals 1
    .param p1, "sampleTime"    # J

    .prologue
    .line 122
    long-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->checkValue(F)V

    .line 123
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->mSampleTime:J

    .line 124
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getUserDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getComment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getInputSourceType()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getSampleTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getUpdateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;->getTimeZone()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    return-void
.end method
