.class Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;
.super Ljava/lang/Object;
.source "SleepMonitorActivity_Base.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 392
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 394
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 397
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.sleepmonitor"

    const-string v5, "SL05"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 399
    .local v2, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 402
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.sleepmonitor"

    const-string v5, "SL07"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090d68

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 404
    .local v1, "guide1":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    const-class v4, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 405
    .restart local v2    # "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->CONNECTIVITY_TYPE_KEY:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 406
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    const/16 v4, 0xa

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 407
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_TEXT:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090d69

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    const-string v3, "HeaderText"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 409
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_KEY:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    const v5, 0x7f090092

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 410
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->ACCESSARY_ADD_REQ_CODE:I
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$700()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 413
    .end local v1    # "guide1":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
    invoke-static {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->getChartReadyToShown()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 415
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.sleepmonitor"

    const-string v5, "SL08"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->prepareShareView()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    goto/16 :goto_0

    .line 394
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
