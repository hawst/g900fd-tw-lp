.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateCalendarActivity;
.super Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.source "WalkingMateCalendarActivity.java"


# static fields
.field public static final SQLITE_WALKINGMATE_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\')"


# instance fields
.field max_step_start_time:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;-><init>()V

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateCalendarActivity;->max_step_start_time:J

    return-void
.end method


# virtual methods
.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 22
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v11, Ljava/util/TreeMap;

    invoke-direct {v11}, Ljava/util/TreeMap;-><init>()V

    .line 50
    .local v11, "dayMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;>;"
    const v16, 0x461c4000    # 10000.0f

    .line 52
    .local v16, "setGoalValue":F
    const/4 v9, 0x0

    .line 54
    .local v9, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateCalendarActivity;->max_step_start_time:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    .line 55
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getBestRecordInfo()Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->getTime()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateCalendarActivity;->max_step_start_time:J

    .line 60
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v12

    .line 61
    .local v12, "deviceType":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .local v15, "selection":Ljava/lang/StringBuilder;
    const-string v3, "SELECT "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v3, "SUM( total_step ) AS total_step"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    const-string v3, " , "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    const-string/jumbo v3, "start_time"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    const-string v3, " FROM "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    const-string/jumbo v3, "walk_info"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    const-string v3, " WHERE "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-static {v12}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->getQueryConditionByDeviceType(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const-string v3, " AND "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time >= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    const-string v3, " AND "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time <= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string v3, " AND "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const-string/jumbo v3, "sync_status != 170004"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    const-string v3, " GROUP BY "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    const-string v3, "(strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 80
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_5

    .line 81
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 83
    const-wide/16 v13, 0x0

    .line 85
    .local v13, "max_step":D
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_5

    .line 87
    const-string/jumbo v3, "start_time"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 88
    .local v17, "time":J
    const-string/jumbo v3, "total_step"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v19

    .line 90
    .local v19, "total_step":D
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v21

    .line 91
    .local v21, "uDateOne":Ljava/util/Calendar;
    move-object/from16 v0, v21

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 93
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v3

    move-wide/from16 v0, v19

    double-to-int v4, v0

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isGoalAchived(JI)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 94
    new-instance v10, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v10}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 95
    .local v10, "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    const/4 v3, 0x1

    invoke-virtual {v10, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setGoalAchieved(Z)V

    .line 96
    const/16 v3, 0x2719

    if-ne v12, v3, :cond_3

    .line 97
    const v3, 0x7f020604

    invoke-virtual {v10, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 100
    :goto_1
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v3, v10}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    .end local v10    # "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    :cond_1
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 112
    .end local v12    # "deviceType":I
    .end local v13    # "max_step":D
    .end local v15    # "selection":Ljava/lang/StringBuilder;
    .end local v17    # "time":J
    .end local v19    # "total_step":D
    .end local v21    # "uDateOne":Ljava/util/Calendar;
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_2

    .line 113
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v3

    .line 99
    .restart local v10    # "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    .restart local v12    # "deviceType":I
    .restart local v13    # "max_step":D
    .restart local v15    # "selection":Ljava/lang/StringBuilder;
    .restart local v17    # "time":J
    .restart local v19    # "total_step":D
    .restart local v21    # "uDateOne":Ljava/util/Calendar;
    :cond_3
    const v3, 0x7f020607

    :try_start_1
    invoke-virtual {v10, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    goto :goto_1

    .line 101
    .end local v10    # "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    :cond_4
    const-wide/16 v3, 0x0

    cmpl-double v3, v19, v3

    if-lez v3, :cond_1

    .line 102
    new-instance v10, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v10}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 103
    .restart local v10    # "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    const v3, 0x7f020034

    invoke-virtual {v10, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 104
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v3, v10}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 112
    .end local v10    # "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    .end local v13    # "max_step":D
    .end local v17    # "time":J
    .end local v19    # "total_step":D
    .end local v21    # "uDateOne":Ljava/util/Calendar;
    :cond_5
    if-eqz v9, :cond_6

    .line 113
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_6
    new-instance v10, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v10}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 118
    .restart local v10    # "dayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    const v3, 0x7f0206cc

    invoke-virtual {v10, v3}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;->setMedalResourceId(I)V

    .line 119
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateCalendarActivity;->max_step_start_time:J

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v3, v10}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    return-object v11
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshGoalInfo()V

    .line 42
    return-void
.end method
