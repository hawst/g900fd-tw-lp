.class public Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
.super Ljava/lang/Object;
.source "CategoryDataHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final BOOLEAN_FIELDS_AMOUNT:I = 0x4

.field private static final CHECKABLE_BOOL_NUMBER_FOR_PARCEL:I = 0x1

.field private static final CONTAINS_FAVORITES_BOOL_NUMBER_FOR_PARCEL:I = 0x3

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;",
            ">;"
        }
    .end annotation
.end field

.field private static final EXPANDABLE_BOOL_NUMBER_FOR_PARCEL:I = 0x0

.field private static final FAVORITES_BOOL_NUMBER_FOR_PARCEL:I = 0x2


# instance fields
.field private mAreFavoriteStarsVisible:Z

.field private mAreItemsCheckable:Z

.field private mAreItemsExpandable:Z

.field private mCaptionId:I

.field private mContainsFavorites:Z

.field private mFoodCategoryListItemsCreator:Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "captionId"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    .line 42
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->readFromParcel(Landroid/os/Parcel;)V

    .line 51
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    .line 154
    const/4 v1, 0x4

    new-array v0, v1, [Z

    .line 155
    .local v0, "args":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 156
    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    .line 157
    const/4 v1, 0x1

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsCheckable:Z

    .line 158
    const/4 v1, 0x2

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    .line 159
    const/4 v1, 0x3

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    .line 160
    const-class v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mFoodCategoryListItemsCreator:Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    .line 161
    return-void
.end method


# virtual methods
.method public areItemsCheckable()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsCheckable:Z

    return v0
.end method

.method public areItemsExpandable()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    return v0
.end method

.method public containsFavorites()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    if-ne p0, p1, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v1

    .line 197
    :cond_1
    instance-of v3, p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    if-nez v3, :cond_2

    move v1, v2

    .line 198
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 200
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .line 201
    .local v0, "that":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    iget-boolean v4, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 202
    goto :goto_0

    .line 204
    :cond_3
    iget v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    iget v4, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 205
    goto :goto_0

    .line 207
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    iget-boolean v4, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 208
    goto :goto_0

    .line 210
    :cond_5
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    iget-boolean v4, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 211
    goto :goto_0
.end method

.method public getCaptionId()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    return v0
.end method

.method public getFavoriteStarsVisibility()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    return v0
.end method

.method public getFoodCategoryListItemsCreator()Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mFoodCategoryListItemsCreator:Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 218
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    .line 219
    .local v0, "result":I
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 220
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v4, v1

    .line 221
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 222
    return v0

    :cond_0
    move v1, v3

    .line 219
    goto :goto_0

    :cond_1
    move v1, v3

    .line 220
    goto :goto_1

    :cond_2
    move v2, v3

    .line 221
    goto :goto_2
.end method

.method protected initFromInstance(Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V
    .locals 1
    .param p1, "oldInstance"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    .prologue
    .line 145
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->getCaptionId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    .line 146
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->areItemsExpandable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    .line 147
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->areItemsCheckable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsCheckable:Z

    .line 148
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->getFavoriteStarsVisibility()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    .line 149
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->containsFavorites()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    .line 150
    return-void
.end method

.method public setAreItemsCheckable(Z)V
    .locals 0
    .param p1, "isCheckable"    # Z

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsCheckable:Z

    .line 124
    return-void
.end method

.method public setAreItemsExpandable(Z)V
    .locals 0
    .param p1, "isExpandable"    # Z

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    .line 110
    return-void
.end method

.method public setFavoriteStarsVisibility(Z)V
    .locals 0
    .param p1, "favoriteStarsAreVisible"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    .line 94
    return-void
.end method

.method public setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V
    .locals 0
    .param p1, "mFoodCategoryListItemsCreator"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mFoodCategoryListItemsCreator:Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    .line 73
    return-void
.end method

.method public specifyFavoritesCategory()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    .line 138
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 170
    iget v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mCaptionId:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    const/4 v1, 0x4

    new-array v0, v1, [Z

    .line 172
    .local v0, "boolArgs":[Z
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsExpandable:Z

    aput-boolean v2, v0, v1

    .line 173
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreItemsCheckable:Z

    aput-boolean v2, v0, v1

    .line 174
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mAreFavoriteStarsVisible:Z

    aput-boolean v2, v0, v1

    .line 175
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mContainsFavorites:Z

    aput-boolean v2, v0, v1

    .line 176
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->mFoodCategoryListItemsCreator:Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 178
    return-void
.end method
