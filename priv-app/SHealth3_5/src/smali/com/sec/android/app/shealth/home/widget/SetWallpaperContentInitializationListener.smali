.class public Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;
.super Ljava/lang/Object;
.source "SetWallpaperContentInitializationListener.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;,
        Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$GetContentAdapter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 12
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 57
    new-instance v4, Ljava/util/ArrayList;

    const/4 v7, 0x2

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 59
    .local v4, "requestInfos":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;>;"
    const/4 v7, 0x1

    invoke-virtual {p3, v7}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 61
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getTempUriFromString(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 62
    .local v6, "tempImageUri":Landroid/net/Uri;
    if-nez v6, :cond_0

    .line 63
    new-instance v7, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 66
    :cond_0
    invoke-virtual {p2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 67
    .local v1, "display":Landroid/view/Display;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 68
    .local v5, "size":Landroid/graphics/Point;
    invoke-virtual {v1, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 70
    new-instance v3, Landroid/content/Intent;

    const-string v7, "android.intent.action.GET_CONTENT"

    const/4 v8, 0x0

    invoke-direct {v3, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 71
    .local v3, "intentAlbum":Landroid/content/Intent;
    const-string v7, "image/*"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "aspectX"

    iget v9, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "aspectY"

    iget v9, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "scale"

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "set-as-image"

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "noFaceDetection"

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 78
    new-instance v7, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;

    const/4 v8, 0x3

    const v9, 0x7f0201c1

    const v10, 0x7f0907a9

    invoke-direct {v7, v3, v8, v9, v10}, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;-><init>(Landroid/content/Intent;III)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v7, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;

    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    const v10, 0x7f020054

    const v11, 0x7f0907a8

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;-><init>(Landroid/content/Intent;III)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    new-instance v7, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;

    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-direct {v8, p2, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v9, 0x1

    const v10, 0x7f020386

    const v11, 0x7f090aab

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$RequestInfo;-><init>(Landroid/content/Intent;III)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$GetContentAdapter;

    invoke-direct {v0, p2, v4}, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$GetContentAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 85
    .local v0, "adapter":Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$GetContentAdapter;
    const v7, 0x7f0808f7

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    .line 86
    .local v2, "gridView":Landroid/widget/GridView;
    invoke-virtual {v2, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    new-instance v7, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;

    invoke-direct {v7, p0, v4, p3, p2}, Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener$1;-><init>(Lcom/sec/android/app/shealth/home/widget/SetWallpaperContentInitializationListener;Ljava/util/List;Landroid/app/Dialog;Landroid/app/Activity;)V

    invoke-virtual {v2, v7}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 95
    return-void
.end method
