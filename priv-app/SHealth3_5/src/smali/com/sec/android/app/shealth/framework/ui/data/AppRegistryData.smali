.class public Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
.super Ljava/lang/Object;


# instance fields
.field public actions:Ljava/lang/String;

.field public appName:Ljava/lang/String;

.field public appType:I

.field public displayPlugInIcons:Ljava/lang/String;

.field public enabled:Z

.field public extra:Ljava/lang/String;

.field public hasAccessory:Z

.field public hasResettableData:Z

.field public isFavorite:I

.field public measureType:J

.field public packagename:Ljava/lang/String;

.field public pluginId:I

.field public pluginSummary:Ljava/lang/String;

.field public pushEnabled:Z

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->version:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->extra:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZI)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->version:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->extra:Ljava/lang/String;

    iput-object p8, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginSummary:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->hasResettableData:Z

    iput-boolean p6, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->hasAccessory:Z

    iput-wide p9, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->measureType:J

    iput-boolean p11, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pushEnabled:Z

    iput p12, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;JZIILjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->version:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->extra:Ljava/lang/String;

    iput-object p8, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginSummary:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->hasResettableData:Z

    iput-boolean p6, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->hasAccessory:Z

    iput-wide p9, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->measureType:J

    iput-boolean p11, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pushEnabled:Z

    iput p12, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->isFavorite:I

    iput p13, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->displayPlugInIcons:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    move/from16 v0, p16

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appType:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
