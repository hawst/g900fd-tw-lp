.class public final Lcom/sec/android/app/shealth/logutils/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/logutils/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final _1dp:I = 0x7f0a00ca

.field public static final accessory_connection_icon_size:I = 0x7f0a00ac

.field public static final accessory_connection_view_status_text_left_margin:I = 0x7f0a00ab

.field public static final accessory_connection_view_status_text_size:I = 0x7f0a00aa

.field public static final action_bar_additional_content_container_width_default:I = 0x7f0a0161

.field public static final action_bar_back_button_container_width:I = 0x7f0a0173

.field public static final action_bar_back_button_height:I = 0x7f0a0170

.field public static final action_bar_back_button_margin_left:I = 0x7f0a0171

.field public static final action_bar_back_button_margin_right:I = 0x7f0a0172

.field public static final action_bar_back_button_move_animation_range:I = 0x7f0a0175

.field public static final action_bar_back_button_width:I = 0x7f0a016f

.field public static final action_bar_btn_height:I = 0x7f0a0152

.field public static final action_bar_btn_width:I = 0x7f0a0153

.field public static final action_bar_button_left_margin_icon_and_text:I = 0x7f0a0168

.field public static final action_bar_button_left_margin_icon_only:I = 0x7f0a0165

.field public static final action_bar_button_left_margin_text_only:I = 0x7f0a0162

.field public static final action_bar_button_middle_margin_icon_and_text:I = 0x7f0a0169

.field public static final action_bar_button_middle_margin_icon_only:I = 0x7f0a0166

.field public static final action_bar_button_middle_margin_text_only:I = 0x7f0a0163

.field public static final action_bar_button_ok_cancel_text_size:I = 0x7f0a0154

.field public static final action_bar_button_right_margin_icon_and_text:I = 0x7f0a016a

.field public static final action_bar_button_right_margin_icon_only:I = 0x7f0a0167

.field public static final action_bar_button_right_margin_text_only:I = 0x7f0a0164

.field public static final action_bar_button_text_size:I = 0x7f0a0155

.field public static final action_bar_buttons_divider_height:I = 0x7f0a0174

.field public static final action_bar_height:I = 0x7f0a015f

.field public static final action_bar_height2:I = 0x7f0a0160

.field public static final action_bar_spinner_header_left_padding:I = 0x7f0a015e

.field public static final action_bar_spinner_horizontal_offset:I = 0x7f0a015b

.field public static final action_bar_spinner_left_margin:I = 0x7f0a015d

.field public static final action_bar_spinner_left_padding:I = 0x7f0a015c

.field public static final action_bar_spinner_vertical_offset:I = 0x7f0a015a

.field public static final action_bar_title_icon_margin_left:I = 0x7f0a0177

.field public static final action_bar_title_icon_margin_right:I = 0x7f0a0178

.field public static final action_bar_title_icon_size:I = 0x7f0a0176

.field public static final action_bar_up_button_margin_bottom:I = 0x7f0a016e

.field public static final action_bar_up_button_margin_left:I = 0x7f0a016c

.field public static final action_bar_up_button_margin_top:I = 0x7f0a016d

.field public static final action_bar_vertical_offset:I = 0x7f0a0159

.field public static final action_up_button_height:I = 0x7f0a0157

.field public static final action_up_button_margin:I = 0x7f0a0158

.field public static final action_up_button_width:I = 0x7f0a0156

.field public static final actionbar_button_margin:I = 0x7f0a017b

.field public static final actionbar_button_padding:I = 0x7f0a017c

.field public static final actionbar_button_width:I = 0x7f0a017d

.field public static final actionbar_divider_height:I = 0x7f0a017a

.field public static final actionbar_divider_width:I = 0x7f0a0179

.field public static final actionbar_summary_height:I = 0x7f0a0180

.field public static final actionbar_summary_height_data:I = 0x7f0a0181

.field public static final actionbar_summary_height_div:I = 0x7f0a0182

.field public static final actionbar_summary_margin:I = 0x7f0a017e

.field public static final actionbar_summary_nav_size:I = 0x7f0a017f

.field public static final actionbar_switch_width:I = 0x7f0a0183

.field public static final activity_horizontal_margin:I = 0x7f0a0000

.field public static final activity_vertical_margin:I = 0x7f0a0001

.field public static final axes_vertical_text_space:I = 0x7f0a0144

.field public static final bg_input_field_image_button_height:I = 0x7f0a01c9

.field public static final bg_input_field_image_button_margin_left:I = 0x7f0a01cb

.field public static final bg_input_field_image_button_margin_right:I = 0x7f0a01ca

.field public static final bg_input_field_image_button_width:I = 0x7f0a01c8

.field public static final blood_glucose_width_after_meal_line:I = 0x7f0a0146

.field public static final blood_glucose_width_fasting_line:I = 0x7f0a0145

.field public static final bt_item_device_image_height:I = 0x7f0a0187

.field public static final bt_item_device_image_width:I = 0x7f0a0186

.field public static final bt_item_device_name_text_size:I = 0x7f0a0188

.field public static final bt_item_height:I = 0x7f0a0184

.field public static final bt_item_paired_image_height:I = 0x7f0a0189

.field public static final bt_item_paired_image_width:I = 0x7f0a018a

.field public static final bt_item_width:I = 0x7f0a0185

.field public static final calendar_adjacent_period_btn_side_margin:I = 0x7f0a01af

.field public static final calendar_adjacent_period_button_height:I = 0x7f0a01ac

.field public static final calendar_adjacent_period_button_text_size:I = 0x7f0a01ae

.field public static final calendar_adjacent_period_button_width:I = 0x7f0a01ad

.field public static final calendar_bottom_panel_height_include_shadow:I = 0x7f0a01a5

.field public static final calendar_content_area_height:I = 0x7f0a019e

.field public static final calendar_day_button_height:I = 0x7f0a01b7

.field public static final calendar_day_button_textsize:I = 0x7f0a01b9

.field public static final calendar_day_button_width:I = 0x7f0a01b8

.field public static final calendar_day_number_text_size:I = 0x7f0a01b1

.field public static final calendar_days_of_week_container_height:I = 0x7f0a01b4

.field public static final calendar_for_six_weeks_height:I = 0x7f0a01b2

.field public static final calendar_for_six_weeks_width:I = 0x7f0a01b3

.field public static final calendar_horizontal_divider_height:I = 0x7f0a01b6

.field public static final calendar_period_panel_height:I = 0x7f0a01ab

.field public static final calendar_popup_layout_width:I = 0x7f0a01a0

.field public static final calendar_popup_month_textview_width:I = 0x7f0a019f

.field public static final calendar_popup_width:I = 0x7f0a01a1

.field public static final calendar_popup_width_include_shadow:I = 0x7f0a01a2

.field public static final calendar_selected_period_label_text_size:I = 0x7f0a01b0

.field public static final calendar_text_day_size:I = 0x7f0a01a3

.field public static final calendar_this_period_btn_height:I = 0x7f0a01a9

.field public static final calendar_this_period_btn_text_size:I = 0x7f0a01aa

.field public static final calendar_this_period_btn_width:I = 0x7f0a01a8

.field public static final calendar_this_period_button_margin_right:I = 0x7f0a01a7

.field public static final calendar_today_monthdate_textsize:I = 0x7f0a01bb

.field public static final calendar_top_panel_height_include_shadow:I = 0x7f0a01a4

.field public static final calendar_top_panel_text_size:I = 0x7f0a01a6

.field public static final calendar_toppanel_height:I = 0x7f0a01ba

.field public static final calendar_vertical_divider_width:I = 0x7f0a01b5

.field public static final chartview_tab_height:I = 0x7f0a01f1

.field public static final common_cbx_general_layout_dimen:I = 0x7f0a01c1

.field public static final common_rb_general_layout_dimen:I = 0x7f0a01c2

.field public static final common_spn_shadow_radius:I = 0x7f0a01c0

.field public static final common_spn_shadow_x:I = 0x7f0a01be

.field public static final common_spn_shadow_y:I = 0x7f0a01bf

.field public static final common_spn_txt_general_textSize:I = 0x7f0a01c3

.field public static final common_wgt_general_layout_margin_lr:I = 0x7f0a01bd

.field public static final common_wgt_general_layout_margin_tb:I = 0x7f0a01bc

.field public static final compatible_device_connect_connect_layout_height:I = 0x7f0a0197

.field public static final compatible_device_connect_description_text_size:I = 0x7f0a019d

.field public static final compatible_device_connect_device_image_size:I = 0x7f0a0198

.field public static final compatible_device_connect_image_accessory_pair_height:I = 0x7f0a019b

.field public static final compatible_device_connect_image_accessory_pair_width:I = 0x7f0a019c

.field public static final compatible_device_connect_image_point_size:I = 0x7f0a0199

.field public static final compatible_device_connect_image_shealth_icon_size:I = 0x7f0a019a

.field public static final compatible_device_detail_bottom_notice_text_size:I = 0x7f0a0196

.field public static final compatible_device_detail_company_image_height:I = 0x7f0a0193

.field public static final compatible_device_detail_connectivity_image_height:I = 0x7f0a018f

.field public static final compatible_device_detail_connectivity_image_width:I = 0x7f0a0190

.field public static final compatible_device_detail_description_text_size:I = 0x7f0a0195

.field public static final compatible_device_detail_device_image_height:I = 0x7f0a018d

.field public static final compatible_device_detail_device_image_layout_height:I = 0x7f0a018b

.field public static final compatible_device_detail_device_image_layout_width:I = 0x7f0a018c

.field public static final compatible_device_detail_device_image_margin_left:I = 0x7f0a0191

.field public static final compatible_device_detail_device_image_width:I = 0x7f0a018e

.field public static final compatible_device_detail_device_name_text_size:I = 0x7f0a0192

.field public static final compatible_device_detail_web_site_text_size:I = 0x7f0a0194

.field public static final constrollerview_gradationbar_text_size:I = 0x7f0a0106

.field public static final controllerview_big_gradationbar_height_horizontal:I = 0x7f0a0100

.field public static final controllerview_big_gradationbar_height_vertical:I = 0x7f0a0103

.field public static final controllerview_gradationbar_horizontal_between_distance:I = 0x7f0a00ff

.field public static final controllerview_gradationbar_vertical_between_distance:I = 0x7f0a00fe

.field public static final controllerview_gradationbar_width:I = 0x7f0a0108

.field public static final controllerview_horizontal_bottomline_height:I = 0x7f0a0107

.field public static final controllerview_horizontal_height:I = 0x7f0a00f0

.field public static final controllerview_horizontal_width:I = 0x7f0a00ef

.field public static final controllerview_margin:I = 0x7f0a00f1

.field public static final controllerview_middle_gradationbar_height_horizontal:I = 0x7f0a0101

.field public static final controllerview_middle_gradationbar_height_vertical:I = 0x7f0a0104

.field public static final controllerview_small_gradationbar_height_horizontal:I = 0x7f0a0102

.field public static final controllerview_small_gradationbar_height_vertical:I = 0x7f0a0105

.field public static final controllerview_vertical_height:I = 0x7f0a00ee

.field public static final controllerview_vertical_width:I = 0x7f0a00ed

.field public static final date_bar_font_size:I = 0x7f0a0131

.field public static final date_bar_margin:I = 0x7f0a0132

.field public static final date_bar_padding:I = 0x7f0a0133

.field public static final dateselector_navibtn_height:I = 0x7f0a01e4

.field public static final dateselector_navibtn_width:I = 0x7f0a01e5

.field public static final dateselector_touch_of_arrow_width:I = 0x7f0a01e6

.field public static final dialog_bottom_padding:I = 0x7f0a008a

.field public static final dialog_side_padding:I = 0x7f0a008b

.field public static final dialog_title_header_height:I = 0x7f0a008c

.field public static final drawer_icon_padding_left:I = 0x7f0a01e8

.field public static final drawermenu_list_profile_height:I = 0x7f0a01e7

.field public static final gradationview_gradationbar_text_size:I = 0x7f0a00f8

.field public static final gradationview_gradationbar_width:I = 0x7f0a00fb

.field public static final gradationview_horizontal_between_gradationbars_distance:I = 0x7f0a00fd

.field public static final gradationview_horizontal_big_gradationbar_height:I = 0x7f0a00f2

.field public static final gradationview_horizontal_bottomline_height:I = 0x7f0a00f9

.field public static final gradationview_horizontal_middle_gradationbar_height:I = 0x7f0a00f3

.field public static final gradationview_horizontal_small_gradationbar_height:I = 0x7f0a00f4

.field public static final gradationview_vertical_between_gradationbars_distance:I = 0x7f0a00fc

.field public static final gradationview_vertical_big_gradationbar_height:I = 0x7f0a00f5

.field public static final gradationview_vertical_bottomline_height:I = 0x7f0a00fa

.field public static final gradationview_vertical_middle_gradationbar_height:I = 0x7f0a00f6

.field public static final gradationview_vertical_small_gradationbar_height:I = 0x7f0a00f7

.field public static final graph_anime_bubble_body_margin:I = 0x7f0a013a

.field public static final graph_anime_bubble_d_height:I = 0x7f0a0140

.field public static final graph_anime_bubble_d_width:I = 0x7f0a013f

.field public static final graph_anime_bubble_height:I = 0x7f0a014a

.field public static final graph_anime_bubble_left_margin:I = 0x7f0a0138

.field public static final graph_anime_bubble_memo_height:I = 0x7f0a014b

.field public static final graph_anime_bubble_picker_height:I = 0x7f0a013b

.field public static final graph_anime_bubble_picker_width:I = 0x7f0a013c

.field public static final graph_anime_bubble_right_margin:I = 0x7f0a0139

.field public static final graph_anime_bubble_width:I = 0x7f0a0137

.field public static final graph_anime_column_width_day:I = 0x7f0a0141

.field public static final graph_anime_dot_size:I = 0x7f0a0136

.field public static final graph_anime_handler_text_size_day:I = 0x7f0a0150

.field public static final graph_anime_handler_width:I = 0x7f0a014f

.field public static final graph_anime_legend_height:I = 0x7f0a0149

.field public static final graph_anime_offset_axis_values_horizontal:I = 0x7f0a013e

.field public static final graph_anime_vertical_axis_text_size_wfl:I = 0x7f0a013d

.field public static final graph_anime_vertical_axis_values_padding:I = 0x7f0a0135

.field public static final graph_anime_weigh_values_line_height:I = 0x7f0a0143

.field public static final graph_anime_weigh_values_line_width:I = 0x7f0a0142

.field public static final graph_chart_abandoned_handler_item_height:I = 0x7f0a0055

.field public static final graph_chart_abandoned_handler_item_width:I = 0x7f0a0054

.field public static final graph_chart_bottom_chart_padding:I = 0x7f0a0060

.field public static final graph_chart_goal_label_text_size:I = 0x7f0a0063

.field public static final graph_chart_goal_line_thickness:I = 0x7f0a0061

.field public static final graph_chart_goal_text_margin_right:I = 0x7f0a0064

.field public static final graph_chart_goal_value_text_size:I = 0x7f0a0062

.field public static final graph_chart_handler_text_size:I = 0x7f0a0053

.field public static final graph_chart_holding_handler_item_height:I = 0x7f0a0057

.field public static final graph_chart_holding_handler_item_space:I = 0x7f0a0058

.field public static final graph_chart_holding_handler_item_width:I = 0x7f0a0056

.field public static final graph_chart_left_chart_padding:I = 0x7f0a005d

.field public static final graph_chart_line_thickness:I = 0x7f0a004c

.field public static final graph_chart_right_chart_padding:I = 0x7f0a005e

.field public static final graph_chart_separator_stroke_width:I = 0x7f0a005c

.field public static final graph_chart_separator_text_size:I = 0x7f0a0059

.field public static final graph_chart_separator_text_spacing_top:I = 0x7f0a005a

.field public static final graph_chart_separator_width:I = 0x7f0a005b

.field public static final graph_chart_top_chart_padding:I = 0x7f0a005f

.field public static final graph_chart_x_axis_stroke_width:I = 0x7f0a004d

.field public static final graph_chart_x_axis_text_size:I = 0x7f0a004e

.field public static final graph_chart_x_axis_text_space:I = 0x7f0a004f

.field public static final graph_chart_y_axis_label_text_size:I = 0x7f0a0052

.field public static final graph_chart_y_axis_text_size:I = 0x7f0a0050

.field public static final graph_chart_y_axis_text_space:I = 0x7f0a0051

.field public static final graph_general_view_height:I = 0x7f0a01ef

.field public static final graph_information_area_amount_layout_margin_top:I = 0x7f0a0044

.field public static final graph_information_area_amount_view_height:I = 0x7f0a0041

.field public static final graph_information_area_amount_view_text_size:I = 0x7f0a0043

.field public static final graph_information_area_amount_view_without_label_text_size:I = 0x7f0a0045

.field public static final graph_information_area_date_value_height:I = 0x7f0a0048

.field public static final graph_information_area_item_title_area_height:I = 0x7f0a0039

.field public static final graph_information_area_padding_top_multi_item:I = 0x7f0a0047

.field public static final graph_information_area_padding_top_single_item:I = 0x7f0a0046

.field public static final graph_information_area_pie_chart_size:I = 0x7f0a004b

.field public static final graph_information_area_subtitle_margin_top:I = 0x7f0a003d

.field public static final graph_information_area_subtitle_text_size:I = 0x7f0a003c

.field public static final graph_information_area_unit_margin_left:I = 0x7f0a0042

.field public static final graph_information_area_unit_view_average_label_text_size:I = 0x7f0a003f

.field public static final graph_information_area_unit_view_height:I = 0x7f0a003a

.field public static final graph_information_area_unit_view_margin_left:I = 0x7f0a0040

.field public static final graph_information_area_unit_view_padding_bottom:I = 0x7f0a004a

.field public static final graph_information_area_unit_view_text_size:I = 0x7f0a003e

.field public static final graph_information_container_height:I = 0x7f0a01f0

.field public static final graph_information_date_text_size:I = 0x7f0a003b

.field public static final graph_information_item_container_height:I = 0x7f0a0049

.field public static final graph_legend_area_button_size:I = 0x7f0a006b

.field public static final graph_legend_area_layout_margin_top:I = 0x7f0a006a

.field public static final graph_legend_area_legend_mark_height:I = 0x7f0a0066

.field public static final graph_legend_area_legend_mark_margin_left:I = 0x7f0a0067

.field public static final graph_legend_area_legend_mark_text_size:I = 0x7f0a006e

.field public static final graph_legend_area_legend_mark_width:I = 0x7f0a0065

.field public static final graph_legend_area_mark_margin_left:I = 0x7f0a0069

.field public static final graph_legend_area_text_mark_width:I = 0x7f0a0068

.field public static final graph_no_data_view_height:I = 0x7f0a01ee

.field public static final graph_separator_spacing_top_default:I = 0x7f0a006c

.field public static final graph_text_mark_view_left_offset:I = 0x7f0a006d

.field public static final health_board_drag_n_drop_text_padding:I = 0x7f0a016b

.field public static final horizontal_axis_vs_handler_height:I = 0x7f0a0148

.field public static final input_activity_date_button_height:I = 0x7f0a01ce

.field public static final input_activity_date_button_margin_left:I = 0x7f0a01cf

.field public static final input_activity_date_button_margin_right:I = 0x7f0a01d0

.field public static final input_activity_date_button_text_size:I = 0x7f0a01d1

.field public static final input_activity_date_button_width:I = 0x7f0a01cd

.field public static final input_activity_datetime_layout_height:I = 0x7f0a01cc

.field public static final input_activity_memo_header_height:I = 0x7f0a01d8

.field public static final input_activity_memo_header_margin_top:I = 0x7f0a01d9

.field public static final input_activity_memo_header_strip_height:I = 0x7f0a01e0

.field public static final input_activity_memo_header_text_height:I = 0x7f0a01da

.field public static final input_activity_memo_header_text_margin_bottom:I = 0x7f0a01dc

.field public static final input_activity_memo_header_text_margin_top:I = 0x7f0a01db

.field public static final input_activity_memo_header_text_padding_left:I = 0x7f0a01dd

.field public static final input_activity_memo_header_text_padding_right:I = 0x7f0a01de

.field public static final input_activity_memo_header_text_size:I = 0x7f0a01df

.field public static final input_activity_memo_height:I = 0x7f0a01d7

.field public static final input_activity_time_button_height:I = 0x7f0a01d3

.field public static final input_activity_time_button_margin_left:I = 0x7f0a01d4

.field public static final input_activity_time_button_margin_right:I = 0x7f0a01d5

.field public static final input_activity_time_button_text_size:I = 0x7f0a01d6

.field public static final input_activity_time_button_width:I = 0x7f0a01d2

.field public static final inputmodule_height:I = 0x7f0a012f

.field public static final inputmodule_horizontal_image_button_margin_left:I = 0x7f0a0111

.field public static final inputmodule_horizontal_image_button_margin_right:I = 0x7f0a0110

.field public static final inputmodule_middle_layout_margin_bottom:I = 0x7f0a0112

.field public static final inputmodule_padding_left:I = 0x7f0a012d

.field public static final inputmodule_padding_right:I = 0x7f0a012e

.field public static final inputmodule_pin_margin_left:I = 0x7f0a0130

.field public static final inputmodule_top_layout_edittext_height:I = 0x7f0a010c

.field public static final inputmodule_top_layout_edittext_margin_bottom:I = 0x7f0a010e

.field public static final inputmodule_top_layout_edittext_text_size:I = 0x7f0a010b

.field public static final inputmodule_top_layout_edittext_width:I = 0x7f0a010d

.field public static final inputmodule_top_layout_height:I = 0x7f0a0109

.field public static final inputmodule_top_layout_margin_bottom:I = 0x7f0a010a

.field public static final inputmodule_top_layout_texview_text_size:I = 0x7f0a010f

.field public static final inputmodule_vertical_image_button_margin_bottom:I = 0x7f0a0121

.field public static final inputmodule_vertical_image_button_margin_top:I = 0x7f0a0120

.field public static final inputmodule_vertical_middle_layout_button_margin_top:I = 0x7f0a011e

.field public static final inputmodule_vertical_middle_layout_button_size:I = 0x7f0a011d

.field public static final inputmodule_vertical_middle_layout_controller_view_margin_top:I = 0x7f0a011f

.field public static final inputmodule_vertical_middle_layout_margin_top:I = 0x7f0a011c

.field public static final inputmodule_vertical_top_layout_edittable_height:I = 0x7f0a0118

.field public static final inputmodule_vertical_top_layout_edittable_margin_top:I = 0x7f0a0117

.field public static final inputmodule_vertical_top_layout_edittable_text_size:I = 0x7f0a0119

.field public static final inputmodule_vertical_top_layout_layout_width:I = 0x7f0a0114

.field public static final inputmodule_vertical_top_layout_margin_bottom:I = 0x7f0a0113

.field public static final inputmodule_vertical_top_layout_textview_title_height:I = 0x7f0a0115

.field public static final inputmodule_vertical_top_layout_textview_title_text_size:I = 0x7f0a0116

.field public static final inputmodule_vertical_top_layout_textview_unit_margin_top:I = 0x7f0a011a

.field public static final inputmodule_vertical_top_layout_textview_unit_text_size:I = 0x7f0a011b

.field public static final legend_height_graph:I = 0x7f0a0134

.field public static final legend_mark_max_width:I = 0x7f0a006f

.field public static final list_popup_divider_height:I = 0x7f0a00e9

.field public static final list_popup_height:I = 0x7f0a00e7

.field public static final list_popup_item_height:I = 0x7f0a00e6

.field public static final list_popup_text_padding_left:I = 0x7f0a00ea

.field public static final list_popup_text_padding_right:I = 0x7f0a00eb

.field public static final list_popup_text_size:I = 0x7f0a00ec

.field public static final list_popup_width:I = 0x7f0a00e8

.field public static final log_list_data_row_height:I = 0x7f0a0010

.field public static final log_list_data_row_height_weight:I = 0x7f0a0011

.field public static final log_list_expand_button_height:I = 0x7f0a0017

.field public static final log_list_expand_button_margin_right:I = 0x7f0a0018

.field public static final log_list_expand_button_margin_top:I = 0x7f0a0019

.field public static final log_list_expand_button_width:I = 0x7f0a0016

.field public static final log_list_group_header_height:I = 0x7f0a0012

.field public static final log_list_left_text_margin_left:I = 0x7f0a0015

.field public static final log_list_left_text_margin_top:I = 0x7f0a0014

.field public static final log_list_left_text_size:I = 0x7f0a0013

.field public static final log_list_no_data_label_text_size:I = 0x7f0a0038

.field public static final log_list_right_row_part_margin_right:I = 0x7f0a001f

.field public static final log_list_row_accessory_image_height:I = 0x7f0a0024

.field public static final log_list_row_accessory_image_width:I = 0x7f0a0023

.field public static final log_list_row_bottom_text_size:I = 0x7f0a001e

.field public static final log_list_row_check_box_margin_left:I = 0x7f0a0029

.field public static final log_list_row_check_box_margin_right:I = 0x7f0a0028

.field public static final log_list_row_divider_height:I = 0x7f0a002a

.field public static final log_list_row_divider_left_margin:I = 0x7f0a0027

.field public static final log_list_row_left_image_margin_left:I = 0x7f0a0034

.field public static final log_list_row_left_image_size:I = 0x7f0a0035

.field public static final log_list_row_left_text_height:I = 0x7f0a001a

.field public static final log_list_row_left_text_margin_left:I = 0x7f0a001b

.field public static final log_list_row_left_text_size:I = 0x7f0a001d

.field public static final log_list_row_left_text_with_icon_margin_left:I = 0x7f0a001c

.field public static final log_list_row_medal_image_size:I = 0x7f0a0036

.field public static final log_list_row_medal_margin_left:I = 0x7f0a0037

.field public static final log_list_row_memo_image_height:I = 0x7f0a0021

.field public static final log_list_row_memo_image_width:I = 0x7f0a0020

.field public static final log_list_row_right_text_margin_top:I = 0x7f0a0026

.field public static final log_list_row_right_text_size:I = 0x7f0a0025

.field public static final log_list_row_text_margin:I = 0x7f0a0022

.field public static final log_list_team_header_height:I = 0x7f0a002b

.field public static final log_list_team_header_left_text_left_margin:I = 0x7f0a002d

.field public static final log_list_team_header_medal_bottom_margin:I = 0x7f0a0030

.field public static final log_list_team_header_right_text_height:I = 0x7f0a0033

.field public static final log_list_team_header_right_text_right_margin:I = 0x7f0a0032

.field public static final log_list_team_header_separator_height:I = 0x7f0a002c

.field public static final log_list_team_header_text_bottom_margin:I = 0x7f0a002f

.field public static final log_list_team_header_text_size:I = 0x7f0a0031

.field public static final log_list_team_header_text_top_margin:I = 0x7f0a002e

.field public static final log_no_data_popup_height:I = 0x7f0a01e1

.field public static final log_no_data_popup_right_margin:I = 0x7f0a01e2

.field public static final log_no_data_view_margin_bottom:I = 0x7f0a01e3

.field public static final max_height_listpopup:I = 0x7f0a0094

.field public static final offset_bottom_graph:I = 0x7f0a014d

.field public static final ok_cancel_buttons_double_line_height:I = 0x7f0a0098

.field public static final ok_cancel_buttons_padding_bottom:I = 0x7f0a0097

.field public static final ok_cancel_buttons_padding_side:I = 0x7f0a0096

.field public static final ok_cancel_buttons_padding_top:I = 0x7f0a0095

.field public static final ok_cancel_buttons_single_line_height:I = 0x7f0a007f

.field public static final ok_cancel_buttons_text_size:I = 0x7f0a007a

.field public static final options_menu_item_text_size:I = 0x7f0a007e

.field public static final pop_content_text_padding_bottom:I = 0x7f0a0076

.field public static final popup_bloodglucose_switch_padding_bottom:I = 0x7f0a0079

.field public static final popup_content_date_time_margin_bottom:I = 0x7f0a0077

.field public static final popup_content_padding_bottom:I = 0x7f0a0091

.field public static final popup_content_padding_left:I = 0x7f0a0074

.field public static final popup_content_padding_right:I = 0x7f0a0075

.field public static final popup_content_padding_top:I = 0x7f0a0090

.field public static final popup_datetime_title_padding:I = 0x7f0a0093

.field public static final popup_margin:I = 0x7f0a008d

.field public static final popup_shadow_size:I = 0x7f0a008e

.field public static final popup_text_content_padding:I = 0x7f0a0092

.field public static final popup_text_content_text_size:I = 0x7f0a0078

.field public static final popup_text_date_time_size:I = 0x7f0a007d

.field public static final popup_text_value__symbol_size:I = 0x7f0a007c

.field public static final popup_text_value_size:I = 0x7f0a007b

.field public static final popup_title_height:I = 0x7f0a0071

.field public static final popup_title_padding_left:I = 0x7f0a0073

.field public static final popup_title_text_size:I = 0x7f0a0072

.field public static final popup_width:I = 0x7f0a0070

.field public static final popup_width_content:I = 0x7f0a008f

.field public static final profile_attribute_item_edit_text_container_height:I = 0x7f0a00df

.field public static final progressbar_label_height:I = 0x7f0a012c

.field public static final s4_screen_height:I = 0x7f0a0147

.field public static final scanning_description_height:I = 0x7f0a01ea

.field public static final scanning_noitem_bg_height:I = 0x7f0a01e9

.field public static final scanning_sub_tabs_width:I = 0x7f0a01eb

.field public static final share_app_icon_size:I = 0x7f0a0080

.field public static final share_app_item_height:I = 0x7f0a0083

.field public static final share_app_item_width:I = 0x7f0a0082

.field public static final share_app_name_text_width:I = 0x7f0a0081

.field public static final share_grid_items_padding:I = 0x7f0a0084

.field public static final share_grid_items_padding_bottom:I = 0x7f0a0088

.field public static final share_grid_items_padding_left:I = 0x7f0a0085

.field public static final share_grid_items_padding_right:I = 0x7f0a0086

.field public static final share_grid_items_padding_top:I = 0x7f0a0087

.field public static final share_grid_items_spacing_vertical:I = 0x7f0a0089

.field public static final sharevia_home_date_text_width:I = 0x7f0a01c5

.field public static final sharevia_home_text_width:I = 0x7f0a01c4

.field public static final sharevia_other_text_width:I = 0x7f0a01c6

.field public static final sharevia_text_height:I = 0x7f0a01c7

.field public static final summary_fragment_datebar_height:I = 0x7f0a01ec

.field public static final summary_fragment_datebar_height_data:I = 0x7f0a01ed

.field public static final summary_pressure_logo_size:I = 0x7f0a00a5

.field public static final summary_screen_accessory_connection_bottom_margin:I = 0x7f0a00e2

.field public static final summary_screen_accessory_connection_left_margin:I = 0x7f0a00e1

.field public static final summary_screen_accessory_connection_text_left_margin:I = 0x7f0a00e0

.field public static final summary_screen_accessory_connection_text_size:I = 0x7f0a00e3

.field public static final summary_screen_accessory_icon_height:I = 0x7f0a00e5

.field public static final summary_screen_accessory_icon_width:I = 0x7f0a00e4

.field public static final summary_view_button_text_size:I = 0x7f0a00af

.field public static final summary_view_content_balance_statistics_text_size:I = 0x7f0a00ba

.field public static final summary_view_content_balance_text_margin_bottom:I = 0x7f0a00b8

.field public static final summary_view_content_balance_text_margin_top:I = 0x7f0a00b7

.field public static final summary_view_content_balance_text_size:I = 0x7f0a00b9

.field public static final summary_view_content_bar_height:I = 0x7f0a00b2

.field public static final summary_view_content_bar_margin:I = 0x7f0a00b3

.field public static final summary_view_content_divider_height:I = 0x7f0a00b5

.field public static final summary_view_content_divider_margin_top:I = 0x7f0a00b4

.field public static final summary_view_content_divider_width:I = 0x7f0a00b6

.field public static final summary_view_content_icon_height:I = 0x7f0a00a1

.field public static final summary_view_content_icon_width:I = 0x7f0a00a2

.field public static final summary_view_content_layout_height:I = 0x7f0a00b0

.field public static final summary_view_content_layout_margin_top:I = 0x7f0a00b1

.field public static final summary_view_content_text_margin_left:I = 0x7f0a00a4

.field public static final summary_view_content_text_size:I = 0x7f0a00a3

.field public static final summary_view_content_title_margin_bottom:I = 0x7f0a00a0

.field public static final summary_view_content_title_margin_top:I = 0x7f0a009f

.field public static final summary_view_content_title_text_height:I = 0x7f0a009e

.field public static final summary_view_content_title_text_size:I = 0x7f0a009d

.field public static final summary_view_content_value_layout_height:I = 0x7f0a009c

.field public static final summary_view_counter_text_size:I = 0x7f0a00cc

.field public static final summary_view_header_balance_state_one_line_text_size:I = 0x7f0a00a7

.field public static final summary_view_header_balance_state_text_height:I = 0x7f0a00a9

.field public static final summary_view_header_balance_state_two_lines_text_size:I = 0x7f0a00a8

.field public static final summary_view_header_height:I = 0x7f0a009b

.field public static final summary_view_header_icon_margin_bottom:I = 0x7f0a009a

.field public static final summary_view_header_icon_margin_top:I = 0x7f0a0099

.field public static final summary_view_header_view_height:I = 0x7f0a00a6

.field public static final summary_view_health_care_counter_view_text_size:I = 0x7f0a00c2

.field public static final summary_view_health_care_counter_view_units_text_size:I = 0x7f0a00c3

.field public static final summary_view_health_care_progress_bar_height:I = 0x7f0a00bc

.field public static final summary_view_health_care_progress_bar_label_side_margin:I = 0x7f0a00c8

.field public static final summary_view_health_care_progress_bar_label_text_size:I = 0x7f0a00c7

.field public static final summary_view_health_care_progress_bar_margin_top:I = 0x7f0a00c1

.field public static final summary_view_health_care_progress_bar_side_margin:I = 0x7f0a00c0

.field public static final summary_view_health_care_progress_bar_slider_height:I = 0x7f0a00be

.field public static final summary_view_health_care_progress_bar_slider_width:I = 0x7f0a00bd

.field public static final summary_view_health_care_progress_bar_text_container_width:I = 0x7f0a00c9

.field public static final summary_view_health_care_progress_bar_title_margin_top:I = 0x7f0a00c5

.field public static final summary_view_health_care_progress_bar_title_text_size:I = 0x7f0a00c6

.field public static final summary_view_health_care_progress_bar_title_width:I = 0x7f0a00c4

.field public static final summary_view_health_care_progress_bar_width:I = 0x7f0a00bf

.field public static final summary_view_health_care_progress_view_container_height:I = 0x7f0a00bb

.field public static final summary_view_single_counter_view_margin_left:I = 0x7f0a00db

.field public static final summary_view_single_degree_line_height:I = 0x7f0a00dd

.field public static final summary_view_single_degree_line_width:I = 0x7f0a00de

.field public static final summary_view_single_indicator_icon_margin_top:I = 0x7f0a00d6

.field public static final summary_view_single_indicator_icon_size:I = 0x7f0a00da

.field public static final summary_view_single_measure_text_label_height:I = 0x7f0a00cf

.field public static final summary_view_single_measure_text_label_margin_left:I = 0x7f0a00ce

.field public static final summary_view_single_measure_text_label_margin_top:I = 0x7f0a00cd

.field public static final summary_view_single_measure_text_label_size:I = 0x7f0a00d0

.field public static final summary_view_single_outside_normal_icon_text_margin:I = 0x7f0a00d2

.field public static final summary_view_single_outside_normal_range_height:I = 0x7f0a00d1

.field public static final summary_view_single_outside_normal_range_text_size:I = 0x7f0a00d4

.field public static final summary_view_single_outside_normal_text_margin:I = 0x7f0a00d3

.field public static final summary_view_single_statistics_from_text_margin:I = 0x7f0a00d5

.field public static final summary_view_single_units_text_size:I = 0x7f0a00dc

.field public static final summary_view_single_value_counter_view_height:I = 0x7f0a00d7

.field public static final summary_view_single_value_counter_view_margin_bottom:I = 0x7f0a00d9

.field public static final summary_view_single_value_counter_view_margin_left:I = 0x7f0a00d8

.field public static final summary_view_slider_width:I = 0x7f0a00cb

.field public static final summary_view_update_button_height:I = 0x7f0a00ae

.field public static final summary_view_update_button_width:I = 0x7f0a00ad

.field public static final vertical_handler_offset:I = 0x7f0a014e

.field public static final vertical_progress_area_margin:I = 0x7f0a0128

.field public static final vertical_progress_area_width:I = 0x7f0a0127

.field public static final vertical_progress_height:I = 0x7f0a0125

.field public static final vertical_progress_labels_area_margin:I = 0x7f0a0123

.field public static final vertical_progress_labels_area_width:I = 0x7f0a0122

.field public static final vertical_progress_mark_height:I = 0x7f0a0126

.field public static final vertical_progress_value_label_height:I = 0x7f0a012a

.field public static final vertical_progress_value_label_text_size:I = 0x7f0a012b

.field public static final vertical_progress_value_label_width:I = 0x7f0a0129

.field public static final vertical_progress_width:I = 0x7f0a0124

.field public static final water_graph_bottom_chart_padding:I = 0x7f0a000b

.field public static final water_graph_goal_line_text_size:I = 0x7f0a0002

.field public static final water_graph_handler_text_size:I = 0x7f0a0006

.field public static final water_graph_left_chart_padding:I = 0x7f0a0008

.field public static final water_graph_legend_item_height:I = 0x7f0a000f

.field public static final water_graph_legend_item_width:I = 0x7f0a000e

.field public static final water_graph_line_thickness:I = 0x7f0a0003

.field public static final water_graph_popup_stroke_width:I = 0x7f0a000d

.field public static final water_graph_right_chart_padding:I = 0x7f0a0009

.field public static final water_graph_top_chart_padding:I = 0x7f0a000a

.field public static final water_graph_x_axis_stroke_width:I = 0x7f0a000c

.field public static final water_graph_x_chart_text_size:I = 0x7f0a0004

.field public static final water_graph_x_sub_text_style:I = 0x7f0a0007

.field public static final water_graph_y_chart_text_size:I = 0x7f0a0005

.field public static final wfl_graph_view_circle_diagram_size:I = 0x7f0a014c

.field public static final x_axes_height_graph:I = 0x7f0a0151


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
