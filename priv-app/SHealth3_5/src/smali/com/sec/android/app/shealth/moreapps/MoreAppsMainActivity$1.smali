.class Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;
.super Ljava/lang/Object;
.source "MoreAppsMainActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(ILjava/lang/String;)V
    .locals 5
    .param p1, "event"    # I
    .param p2, "response"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 98
    sget-object v1, Lcom/sec/android/app/shealth/moreapps/service/Util;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Response : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    if-ne p1, v4, :cond_2

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    # getter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$000(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    # getter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$000(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_SUB_TAB_KEY:Ljava/lang/String;

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/shealth/moreapps/service/Util;->getPrefInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 106
    .local v0, "tab":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, p2, v2}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->newInstance(ILjava/lang/String;Landroid/content/Context;)Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$102(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Lcom/sec/android/app/shealth/moreapps/WebViewFragment;)Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    # getter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$100(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 124
    .end local v0    # "tab":I
    :cond_1
    :goto_0
    return-void

    .line 110
    :cond_2
    const/16 v1, 0x25d

    if-eq p1, v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    # getter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$000(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    # getter for: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$000(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    .line 114
    :cond_3
    const/4 v1, -0x5

    if-ne p1, v1, :cond_4

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    const-string/jumbo v2, "timeout"

    # invokes: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showErrorPopup(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$200(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;->this$0:Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    const-string/jumbo v2, "network"

    # invokes: Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showErrorPopup(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->access$200(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
