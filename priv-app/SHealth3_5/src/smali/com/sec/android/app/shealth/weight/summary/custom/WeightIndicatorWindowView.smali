.class Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;
.super Landroid/view/View;
.source "WeightIndicatorWindowView.java"


# static fields
.field private static ARC_START_ANGLE:F

.field private static ARC_SWEEP_ANGLE:F


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapCanvas:Landroid/graphics/Canvas;

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mInsetValue:I

.field private mInsideRadius:I

.field private mRadius:I

.field private mViewRect:Landroid/graphics/RectF;

.field private mWindowPaint:Landroid/graphics/Paint;

.field private mWindowTopPadding:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const/high16 v0, -0x3d1a0000    # -115.0f

    sput v0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->ARC_START_ANGLE:F

    .line 41
    const/high16 v0, -0x3d4c0000    # -90.0f

    sget v1, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->ARC_START_ANGLE:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->ARC_SWEEP_ANGLE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mRadius:I

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->initialize()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mRadius:I

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->initialize()V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mRadius:I

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->initialize()V

    .line 65
    return-void
.end method

.method private allocateBitmap()V
    .locals 3

    .prologue
    .line 100
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 101
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 102
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0206ee

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    .line 106
    return-void
.end method

.method private initialize()V
    .locals 3

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->setBackgroundColor(I)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0b11

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mWindowTopPadding:I

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0b1a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0b12

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mInsetValue:I

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->allocateBitmap()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 91
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmapCanvas:Landroid/graphics/Canvas;

    .line 93
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mViewRect:Landroid/graphics/RectF;

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->initializeWindowPaint()V

    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->initializeCirclePaint()V

    .line 96
    return-void
.end method

.method private initializeCirclePaint()V
    .locals 3

    .prologue
    .line 114
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mCirclePaint:Landroid/graphics/Paint;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    return-void
.end method

.method private initializeWindowPaint()V
    .locals 3

    .prologue
    .line 109
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mWindowPaint:Landroid/graphics/Paint;

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mWindowPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 111
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmapCanvas:Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmapCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mViewRect:Landroid/graphics/RectF;

    sget v2, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->ARC_START_ANGLE:F

    sget v3, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->ARC_SWEEP_ANGLE:F

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mWindowPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v6, v6, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v7

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mInsideRadius:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 137
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v4, 0x0

    .line 120
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 121
    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mRadius:I

    mul-int/lit8 v0, v1, 0x2

    .line 122
    .local v0, "size":I
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->setMeasuredDimension(II)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mViewRect:Landroid/graphics/RectF;

    int-to-float v2, v0

    int-to-float v3, v0

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mViewRect:Landroid/graphics/RectF;

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mWindowTopPadding:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mWindowTopPadding:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 126
    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mRadius:I

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mInsetValue:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mInsideRadius:I

    .line 127
    return-void
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 73
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmapCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->release()V

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmapCanvas:Landroid/graphics/Canvas;

    .line 81
    :cond_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 82
    return-void
.end method

.method setRadius(I)V
    .locals 4
    .param p1, "radius"    # I

    .prologue
    .line 145
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mRadius:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 146
    :cond_0
    mul-int/lit8 v0, p1, 0x2

    .line 150
    .local v0, "size":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->allocateBitmap()V

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    .line 159
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mBitmapCanvas:Landroid/graphics/Canvas;

    .line 161
    .end local v0    # "size":I
    :cond_2
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->mRadius:I

    .line 162
    return-void
.end method
