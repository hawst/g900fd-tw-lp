.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;

.field final synthetic val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 40

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1$2;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;

    iget-object v4, v4, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->startWearableSync(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;)V

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x4567

    aput v12, v2, v11

    const/16 v11, 0x6626

    aput v11, v2, v10

    const/16 v10, -0xcf1

    aput v10, v2, v9

    const/16 v9, -0x7b

    aput v9, v2, v8

    const/16 v8, 0x6163

    aput v8, v2, v7

    const/16 v7, -0x5bfc

    aput v7, v2, v6

    const/16 v6, -0x9

    aput v6, v2, v5

    const/16 v5, -0x34e8

    aput v5, v2, v4

    const/16 v4, -0x5e

    aput v4, v2, v3

    const/16 v3, -0x558e

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x4502

    aput v13, v1, v12

    const/16 v12, 0x6645

    aput v12, v1, v11

    const/16 v11, -0xc9a

    aput v11, v1, v10

    const/16 v10, -0xd

    aput v10, v1, v9

    const/16 v9, 0x6111

    aput v9, v1, v8

    const/16 v8, -0x5b9f

    aput v8, v1, v7

    const/16 v7, -0x5c

    aput v7, v1, v6

    const/16 v6, -0x3486

    aput v6, v1, v5

    const/16 v5, -0x35

    aput v5, v1, v4

    const/16 v4, -0x55e2

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x4a

    aput v38, v2, v37

    const/16 v37, -0x3b

    aput v37, v2, v36

    const/16 v36, 0x4260

    aput v36, v2, v35

    const/16 v35, 0x1c2c

    aput v35, v2, v34

    const/16 v34, 0x5a3c

    aput v34, v2, v33

    const/16 v33, 0x1029

    aput v33, v2, v32

    const/16 v32, 0x5d79

    aput v32, v2, v31

    const/16 v31, 0x787d

    aput v31, v2, v30

    const/16 v30, -0x40f6

    aput v30, v2, v29

    const/16 v29, -0x26

    aput v29, v2, v28

    const/16 v28, -0x5d96

    aput v28, v2, v27

    const/16 v27, -0x32

    aput v27, v2, v26

    const/16 v26, -0x67

    aput v26, v2, v25

    const/16 v25, 0xd3b

    aput v25, v2, v24

    const/16 v24, 0x7479

    aput v24, v2, v23

    const/16 v23, -0x10e6

    aput v23, v2, v22

    const/16 v22, -0x80

    aput v22, v2, v21

    const/16 v21, -0x33

    aput v21, v2, v20

    const/16 v20, -0x5ae5

    aput v20, v2, v19

    const/16 v19, -0x35

    aput v19, v2, v18

    const/16 v18, -0x66fe

    aput v18, v2, v17

    const/16 v17, -0xc

    aput v17, v2, v16

    const/16 v16, -0x3c

    aput v16, v2, v15

    const/4 v15, -0x6

    aput v15, v2, v14

    const/16 v14, -0x38b6

    aput v14, v2, v13

    const/16 v13, -0x7f

    aput v13, v2, v12

    const/16 v12, -0x19c

    aput v12, v2, v11

    const/16 v11, -0x70

    aput v11, v2, v10

    const/16 v10, -0x45

    aput v10, v2, v9

    const/4 v9, -0x4

    aput v9, v2, v8

    const/16 v8, 0x130

    aput v8, v2, v7

    const/16 v7, -0x64a0

    aput v7, v2, v6

    const/4 v6, -0x8

    aput v6, v2, v5

    const/16 v5, 0xb28

    aput v5, v2, v3

    const/16 v3, -0x159a

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x26

    aput v39, v1, v38

    const/16 v38, -0x57

    aput v38, v1, v37

    const/16 v37, 0x4215

    aput v37, v1, v36

    const/16 v36, 0x1c42

    aput v36, v1, v35

    const/16 v35, 0x5a1c

    aput v35, v1, v34

    const/16 v34, 0x105a

    aput v34, v1, v33

    const/16 v33, 0x5d10

    aput v33, v1, v32

    const/16 v32, 0x785d

    aput v32, v1, v31

    const/16 v31, -0x4088

    aput v31, v1, v30

    const/16 v30, -0x41

    aput v30, v1, v29

    const/16 v29, -0x5dfa

    aput v29, v1, v28

    const/16 v28, -0x5e

    aput v28, v1, v27

    const/16 v27, -0xa

    aput v27, v1, v26

    const/16 v26, 0xd49

    aput v26, v1, v25

    const/16 v25, 0x740d

    aput v25, v1, v24

    const/16 v24, -0x108c

    aput v24, v1, v23

    const/16 v23, -0x11

    aput v23, v1, v22

    const/16 v22, -0x72

    aput v22, v1, v21

    const/16 v21, -0x5a91

    aput v21, v1, v20

    const/16 v20, -0x5b

    aput v20, v1, v19

    const/16 v19, -0x6699

    aput v19, v1, v18

    const/16 v18, -0x67

    aput v18, v1, v17

    const/16 v17, -0x5d

    aput v17, v1, v16

    const/16 v16, -0x65

    aput v16, v1, v15

    const/16 v15, -0x38c8

    aput v15, v1, v14

    const/16 v14, -0x39

    aput v14, v1, v13

    const/16 v13, -0x1fd

    aput v13, v1, v12

    const/4 v12, -0x2

    aput v12, v1, v11

    const/16 v11, -0x2e

    aput v11, v1, v10

    const/16 v10, -0x6e

    aput v10, v1, v9

    const/16 v9, 0x15e

    aput v9, v1, v8

    const/16 v8, -0x64ff

    aput v8, v1, v7

    const/16 v7, -0x65

    aput v7, v1, v6

    const/16 v6, 0xb7b

    aput v6, v1, v5

    const/16 v5, -0x15f5

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :catch_1
    move-exception v1

    goto/16 :goto_0
.end method
