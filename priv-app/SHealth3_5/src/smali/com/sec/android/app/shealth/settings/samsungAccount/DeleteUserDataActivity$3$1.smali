.class Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;
.super Ljava/lang/Object;
.source "DeleteUserDataActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(II)V
    .locals 6
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    const/4 v5, 0x0

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onFinished() error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->cancel()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :cond_0
    :goto_0
    if-nez p2, :cond_1

    .line 178
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateCurrentStatus()V

    .line 181
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v1

    .line 182
    .local v1, "securitySettingsHelper":Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    const-string/jumbo v3, "security_pin_enabled"

    invoke-virtual {v1, v2, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 183
    sget-object v2, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-static {v2}, Lcom/sec/android/app/shealth/DBUtil;->clearApplicationData(Landroid/content/Context;)V

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->finishAffinity()V

    .line 187
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 194
    .end local v1    # "securitySettingsHelper":Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    :goto_1
    return-void

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception received which can be ignored.. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 191
    .end local v0    # "iae":Ljava/lang/IllegalArgumentException;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090d53

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->showToast(Landroid/content/Context;II)V

    goto :goto_1
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 163
    return-void
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 3
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted() error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted() message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    if-nez p2, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->cancel()V

    goto :goto_0
.end method
