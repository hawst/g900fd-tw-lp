.class public Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;
.super Ljava/lang/Object;
.source "FirstbeatDBHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static bFirstRead:Z

.field private static mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

.field private static mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->TAG:Ljava/lang/String;

    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mContext:Landroid/content/Context;

    .line 18
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->bFirstRead:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "shared pref manager created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    return-void
.end method

.method public static getRealTimeMaxHR()I
    .locals 1

    .prologue
    .line 102
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->readDB()V

    .line 103
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-char v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxHeartRate:C

    return v0
.end method

.method public static getRealTimeMaxMET()I
    .locals 2

    .prologue
    .line 112
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->readDB()V

    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-wide v0, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxMET:J

    long-to-int v0, v0

    return v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    sput-object p0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mContext:Landroid/content/Context;

    .line 29
    return-void
.end method

.method public static readDB()V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 32
    sget-boolean v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->bFirstRead:Z

    if-nez v0, :cond_1

    .line 33
    const/4 v6, 0x0

    .line 35
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "create_time DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 44
    :goto_0
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 45
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string v1, "ac"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-char v1, v1

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->ac:C

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "maximum_heart_rate"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-char v1, v1

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxHeartRate:C

    .line 49
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "maximum_met"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxMET:J

    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "recovery_resource"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->recourceRecovery:I

    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "start_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->startDate:J

    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "training_level"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->trainingLevel:I

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "training_level_update"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->lastTrainingLevelUpdate:J

    .line 54
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "previous_training_level"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousTrainingLevel:I

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string/jumbo v1, "previous_to_previous_training_level"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousToPreviousTrainingLevel:I

    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string v1, "latest_feedback_phrase_number"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestFeedbackPhraseNumber:I

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-string v1, "latest_exercise_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestExerciseTime:J

    .line 72
    :goto_1
    if-eqz v6, :cond_0

    .line 73
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 75
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->bFirstRead:Z

    .line 77
    :cond_1
    return-void

    .line 37
    :catch_0
    move-exception v7

    .line 38
    .local v7, "e":Ljava/lang/SecurityException;
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "SecurityException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 40
    .end local v7    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v7

    .line 42
    .local v7, "e":Ljava/lang/NullPointerException;
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "NullPointerException"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 60
    .end local v7    # "e":Ljava/lang/NullPointerException;
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const/16 v1, 0x32

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->ac:C

    .line 61
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput-char v9, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxHeartRate:C

    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxMET:J

    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput v9, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->recourceRecovery:I

    .line 64
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->startDate:J

    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput v8, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->trainingLevel:I

    .line 66
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput-wide v10, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->lastTrainingLevelUpdate:J

    .line 67
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput v8, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousTrainingLevel:I

    .line 68
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput v8, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousToPreviousTrainingLevel:I

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput v8, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestFeedbackPhraseNumber:I

    .line 70
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iput-wide v10, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestExerciseTime:J

    goto :goto_1
.end method

.method public static setRealTimeMaxHR(I)V
    .locals 2
    .param p0, "maxHR"    # I

    .prologue
    .line 97
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    int-to-char v1, p0

    iput-char v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxHeartRate:C

    .line 98
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->updateDB()V

    .line 99
    return-void
.end method

.method public static setRealTimeMaxMET(I)V
    .locals 3
    .param p0, "maxMET"    # I

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    int-to-long v1, p0

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxMET:J

    .line 108
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->updateDB()V

    .line 109
    return-void
.end method

.method public static updateDB()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 81
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "ac"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-char v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->ac:C

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 82
    const-string/jumbo v1, "maximum_heart_rate"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-char v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxHeartRate:C

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 83
    const-string/jumbo v1, "maximum_met"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->maxMET:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 84
    const-string/jumbo v1, "recovery_resource"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->recourceRecovery:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 85
    const-string/jumbo v1, "start_time"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->startDate:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 86
    const-string/jumbo v1, "training_level"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->trainingLevel:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 87
    const-string/jumbo v1, "training_level_update"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->lastTrainingLevelUpdate:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 88
    const-string/jumbo v1, "previous_training_level"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousTrainingLevel:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 89
    const-string/jumbo v1, "previous_to_previous_training_level"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->previousToPreviousTrainingLevel:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 90
    const-string v1, "latest_feedback_phrase_number"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestFeedbackPhraseNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 91
    const-string v1, "latest_exercise_time"

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mCoaching:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;->latestExerciseTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 92
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 93
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 94
    return-void
.end method
