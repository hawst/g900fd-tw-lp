.class Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$6;
.super Ljava/lang/Object;
.source "SelectFavoriteActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$6;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 508
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 510
    .local v0, "isCategory":Z
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->setFilterCategory(Z)V

    .line 512
    if-eqz v0, :cond_1

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$6;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortByCategory()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    .line 519
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$6;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->access$700(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)Landroid/widget/ExpandableListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->setSelectionAfterHeaderView()V

    .line 520
    return-void

    .line 508
    .end local v0    # "isCategory":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 516
    .restart local v0    # "isCategory":Z
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$6;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortByTitle()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    goto :goto_1
.end method
