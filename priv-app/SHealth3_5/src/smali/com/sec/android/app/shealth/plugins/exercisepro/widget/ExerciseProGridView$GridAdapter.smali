.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ExerciseProGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GridAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
        ">;"
    }
.end annotation


# instance fields
.field private layoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .line 210
    const v0, 0x7f0301c1

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 211
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 213
    return-void
.end method

.method private bindView(ILandroid/view/View;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;

    .prologue
    .line 270
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 271
    .local v0, "exerciseData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    move-object v1, p2

    check-cast v1, Landroid/view/ViewGroup;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->initViewBySize(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V
    invoke-static {v2, v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V

    .line 272
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 273
    return-void
.end method

.method private newView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 258
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0301c1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 260
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 261
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0995

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0996

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 265
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 266
    return-object v1
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 217
    if-nez p2, :cond_0

    .line 218
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->newView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 220
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->bindView(ILandroid/view/View;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->isEditMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->isEditMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnPhoto:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnPhoto:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/widget/ImageButton;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    :cond_2
    return-object p2
.end method
