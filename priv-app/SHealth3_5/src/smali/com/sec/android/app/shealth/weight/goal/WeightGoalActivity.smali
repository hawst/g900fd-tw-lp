.class public Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WeightGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerGetter;


# instance fields
.field mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    return-void
.end method

.method private isGoalExisting()Z
    .locals 3

    .prologue
    .line 166
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 168
    .local v0, "mDao":Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private showFragment(Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;)V
    .locals 5
    .param p1, "fragment"    # Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;

    .prologue
    .line 175
    instance-of v4, p1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    if-eqz v4, :cond_1

    move-object v4, p1

    .line 176
    check-cast v4, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    iput-object v4, p0, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .line 181
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->getFragmentType()Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;->name()Ljava/lang/String;

    move-result-object v1

    .line 183
    .local v1, "fragmentTag":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 185
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 187
    .local v2, "tempFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 189
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    instance-of v4, p1, Lcom/sec/android/app/shealth/weight/NonBackStackFragment;

    if-nez v4, :cond_0

    .line 194
    :cond_0
    const v4, 0x7f080bf9

    if-nez v2, :cond_2

    .end local p1    # "fragment":Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;
    :goto_1
    invoke-virtual {v3, v4, p1, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 196
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 198
    return-void

    .line 178
    .end local v0    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v1    # "fragmentTag":Ljava/lang/String;
    .end local v2    # "tempFragment":Landroid/support/v4/app/Fragment;
    .end local v3    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    .restart local p1    # "fragment":Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    goto :goto_0

    .restart local v0    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .restart local v1    # "fragmentTag":Ljava/lang/String;
    .restart local v2    # "tempFragment":Landroid/support/v4/app/Fragment;
    .restart local v3    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_2
    move-object p1, v2

    .line 194
    goto :goto_1
.end method


# virtual methods
.method public closeScreen()V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    .line 211
    return-void
.end method

.method getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method public getNegativeButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;->getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v0, 0x7f0302b9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->setContentView(I)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showGoalSummaryFragment()V

    .line 81
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method protected showGoalSummaryFragment()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->isGoalExisting()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showWeightWithGoalFragment()V

    .line 112
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showSetGoalFragment()V

    goto :goto_0
.end method

.method protected showSetGoalCaloriesFragment(Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;)V
    .locals 3
    .param p1, "goalData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .prologue
    .line 151
    new-instance v1, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;-><init>()V

    .line 153
    .local v1, "weightSetGoalCaloriesFragment":Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 155
    .local v0, "extra":Landroid/os/Bundle;
    const-string v2, "goal_data_key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 157
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->setArguments(Landroid/os/Bundle;)V

    .line 159
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showFragment(Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;)V

    .line 161
    return-void
.end method

.method protected showSetGoalFragment()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->mWgtSetGoalWgtFragment:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalWeightFragment;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showFragment(Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;)V

    .line 125
    return-void
.end method

.method protected showWeightWithGoalFragment()V
    .locals 3

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 137
    new-instance v0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;-><init>()V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showFragment(Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;)V

    .line 139
    return-void
.end method
