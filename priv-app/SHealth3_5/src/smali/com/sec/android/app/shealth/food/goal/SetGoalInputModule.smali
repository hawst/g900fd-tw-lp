.class public Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;
.super Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;
.source "SetGoalInputModule.java"


# static fields
.field private static final SET_GOAL_MOVE_DISTANCE:I = 0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;-><init>(Landroid/content/Context;)V

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;->init()V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;->init()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/input/VerticalInputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;->init()V

    .line 45
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 58
    const v0, 0x47c34f80    # 99999.0f

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;->setInputRange(FF)V

    .line 59
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/goal/SetGoalInputModule;->setMoveDistance(F)V

    .line 60
    return-void
.end method


# virtual methods
.method protected getRootLayoutId()I
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f030116

    return v0
.end method

.method protected initAdditionalViews(Landroid/view/View;)V
    .locals 0
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 50
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 66
    return-void
.end method
