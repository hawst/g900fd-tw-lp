.class public abstract Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.super Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;
.source "InformationAreaViewInterLayer.java"


# static fields
.field private static final DATE_SEPARATOR:Ljava/lang/String; = "/"

.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field private static final DAY_FORMAT_PATTERN:Ljava/lang/String; = "dd"

.field private static final EPS:D = -1.0000000116860974E-7

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MM"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"

.field private static final YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"

.field private static volatile mChartSelectedDate:J


# instance fields
.field protected dateFormat:Ljava/text/DateFormat;

.field protected final layoutInflater:Landroid/view/LayoutInflater;

.field private mDateFormatOrder:[C

.field private mTimeFormatPattern:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mChartSelectedDate:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;-><init>(Landroid/content/Context;)V

    .line 66
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->layoutInflater:Landroid/view/LayoutInflater;

    .line 67
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mDateFormatOrder:[C

    .line 69
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 70
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormatString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mTimeFormatPattern:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormatString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mTimeFormatPattern:Ljava/lang/String;

    .line 78
    return-void

    .line 72
    :cond_0
    :try_start_1
    const-string v1, "HH:mm"

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mTimeFormatPattern:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "nre":Ljava/lang/NoSuchMethodError;
    const-string v1, "HH:mm"

    iput-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mTimeFormatPattern:Ljava/lang/String;

    goto :goto_0
.end method

.method public static resetSelectedTime()V
    .locals 2

    .prologue
    .line 172
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mChartSelectedDate:J

    .line 173
    return-void
.end method


# virtual methods
.method public getSelectedDateInChart()J
    .locals 2

    .prologue
    .line 164
    sget-wide v0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mChartSelectedDate:J

    return-wide v0
.end method

.method protected getSummarizedData(Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)F
    .locals 7
    .param p1, "data"    # Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .prologue
    .line 115
    const/4 v4, 0x0

    .line 116
    .local v4, "sum":F
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    .line 117
    .local v2, "dataValues":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 118
    .local v0, "dataValue":D
    float-to-double v5, v4

    add-double/2addr v5, v0

    double-to-float v4, v5

    .line 119
    goto :goto_0

    .line 120
    .end local v0    # "dataValue":D
    :cond_0
    return v4
.end method

.method protected isNotLessThanZero(D)Z
    .locals 2
    .param p1, "value"    # D

    .prologue
    .line 131
    const-wide v0, -0x4185280d60000000L    # -1.0000000116860974E-7

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 84
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mDateFormatOrder:[C

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    .line 87
    .local v0, "dateFormatPattern":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_1

    .line 88
    const-string v1, ""

    .line 93
    .local v1, "dayFormatPattern":Ljava/lang/String;
    :goto_0
    const-string v2, "d+"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    const-string v2, "M+"

    const-string v3, "MM/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    const-string/jumbo v2, "y+"

    const-string/jumbo v3, "yyyy/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    const-string v2, "/$"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_0

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mTimeFormatPattern:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->dateFormat:Ljava/text/DateFormat;

    .line 107
    return-void

    .line 90
    .end local v1    # "dayFormatPattern":Ljava/lang/String;
    :cond_1
    const-string v1, "dd/"

    .restart local v1    # "dayFormatPattern":Ljava/lang/String;
    goto :goto_0
.end method

.method public setSelectedDateInChart(J)V
    .locals 0
    .param p1, "chartSelectedDate"    # J

    .prologue
    .line 155
    sput-wide p1, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->mChartSelectedDate:J

    .line 156
    return-void
.end method

.method public final update(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 147
    return-void
.end method

.method public abstract update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation
.end method
