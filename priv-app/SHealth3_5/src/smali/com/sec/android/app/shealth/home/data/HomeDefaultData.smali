.class public Lcom/sec/android/app/shealth/home/data/HomeDefaultData;
.super Ljava/lang/Object;
.source "HomeDefaultData.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HomeDefaultData"

.field private static instance:Lcom/sec/android/app/shealth/home/data/HomeDefaultData;


# instance fields
.field private CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F

.field private CELSIUS_TO_FAHRENHEIT_SUMMAND:I

.field private FAHRENHEIT_TO_CELSIUS_MULTIPLIER:F

.field private FAHRENHEIT_TO_CELSIUS_SUMMAND:I

.field private isNeedShowWearableIcon:Z

.field private mContext:Landroid/content/Context;

.field private mTime:J

.field private titleText:Ljava/lang/String;

.field private titleTextSpanned:Landroid/text/Spanned;

.field private unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

.field private valueText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->instance:Lcom/sec/android/app/shealth/home/data/HomeDefaultData;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 31
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleText:Ljava/lang/String;

    .line 32
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleTextSpanned:Landroid/text/Spanned;

    .line 33
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->valueText:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->isNeedShowWearableIcon:Z

    .line 36
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 311
    const v0, 0x3fe66666    # 1.8f

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F

    .line 312
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->FAHRENHEIT_TO_CELSIUS_MULTIPLIER:F

    .line 313
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->CELSIUS_TO_FAHRENHEIT_SUMMAND:I

    .line 314
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->CELSIUS_TO_FAHRENHEIT_SUMMAND:I

    neg-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->FAHRENHEIT_TO_CELSIUS_SUMMAND:I

    .line 41
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    .line 42
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 43
    return-void
.end method

.method private convertCelsiusToFahrenheit(F)F
    .locals 2
    .param p1, "temperatureInCelsius"    # F

    .prologue
    .line 338
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->CELSIUS_TO_FAHRENHEIT_MULTIPLIER:F

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->CELSIUS_TO_FAHRENHEIT_SUMMAND:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method private convertFahrenheitToCelsius(F)F
    .locals 2
    .param p1, "temperatureInFahrenheit"    # F

    .prologue
    .line 344
    iget v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->FAHRENHEIT_TO_CELSIUS_SUMMAND:I

    int-to-float v0, v0

    add-float/2addr v0, p1

    iget v1, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->FAHRENHEIT_TO_CELSIUS_MULTIPLIER:F

    mul-float/2addr v0, v1

    return v0
.end method

.method private convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F
    .locals 1
    .param p1, "data"    # F
    .param p2, "inputUnit"    # Ljava/lang/String;
    .param p3, "outputUnit"    # Ljava/lang/String;

    .prologue
    .line 318
    const-string v0, "C"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 319
    const-string v0, "C"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 334
    .end local p1    # "data":F
    :cond_0
    :goto_0
    return p1

    .line 322
    .restart local p1    # "data":F
    :cond_1
    const-string v0, "F"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 323
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->convertFahrenheitToCelsius(F)F

    move-result p1

    goto :goto_0

    .line 326
    :cond_2
    const-string v0, "F"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    const-string v0, "F"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    const-string v0, "C"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 331
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->convertCelsiusToFahrenheit(F)F

    move-result p1

    goto :goto_0

    .line 334
    :cond_3
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static getCreateTime()J
    .locals 2

    .prologue
    .line 308
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->getCreateTime()J

    move-result-wide v0

    return-wide v0
.end method

.method private static getHumidity()F
    .locals 1

    .prologue
    .line 304
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->getHumidity()F

    move-result v0

    return v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/home/data/HomeDefaultData;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->instance:Lcom/sec/android/app/shealth/home/data/HomeDefaultData;

    return-object v0
.end method

.method private static getTemperature()F
    .locals 1

    .prologue
    .line 300
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->getTemperature()F

    move-result v0

    return v0
.end method

.method private getUVComment(I)Ljava/lang/String;
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x6

    const/4 v3, 0x5

    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 252
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v0

    if-nez v0, :cond_4

    .line 253
    if-ltz p1, :cond_0

    if-gt p1, v1, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d94

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 276
    :goto_0
    return-object v0

    .line 255
    :cond_0
    if-lt p1, v2, :cond_1

    if-gt p1, v3, :cond_1

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d95

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 257
    :cond_1
    if-lt p1, v4, :cond_2

    if-gt p1, v5, :cond_2

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d96

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 259
    :cond_2
    const/16 v0, 0x8

    if-lt p1, v0, :cond_3

    const/16 v0, 0xa

    if-gt p1, v0, :cond_3

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d97

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 261
    :cond_3
    const/16 v0, 0xb

    if-lt p1, v0, :cond_9

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d98

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 265
    :cond_4
    if-ltz p1, :cond_5

    if-gt p1, v1, :cond_5

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f091158

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 267
    :cond_5
    if-lt p1, v2, :cond_6

    const/4 v0, 0x4

    if-gt p1, v0, :cond_6

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f091159

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 269
    :cond_6
    if-lt p1, v3, :cond_7

    if-gt p1, v4, :cond_7

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09115a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 271
    :cond_7
    if-lt p1, v5, :cond_8

    const/16 v0, 0x9

    if-gt p1, v0, :cond_8

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09115b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 273
    :cond_8
    const/16 v0, 0xa

    if-lt p1, v0, :cond_9

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09115c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 276
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private initValue()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 90
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 91
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleText:Ljava/lang/String;

    .line 92
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleTextSpanned:Landroid/text/Spanned;

    .line 93
    iput-object v2, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->valueText:Ljava/lang/String;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->isNeedShowWearableIcon:Z

    .line 95
    return-void
.end method

.method private setSleepData()V
    .locals 28

    .prologue
    .line 98
    const/4 v5, 0x0

    .line 99
    .local v5, "projection":[Ljava/lang/String;
    const/4 v3, 0x3

    new-array v5, v3, [Ljava/lang/String;

    .end local v5    # "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string v4, "efficiency"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "bed_time"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "rise_time"

    aput-object v4, v5, v3

    .line 100
    .restart local v5    # "projection":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 101
    .local v10, "cursor":Landroid/database/Cursor;
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 104
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string/jumbo v8, "rise_time DESC LIMIT 1"

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 105
    if-eqz v10, :cond_2

    .line 106
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 107
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_7

    .line 108
    const-string v3, "bed_time"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 109
    .local v17, "startTime":J
    const-string/jumbo v3, "rise_time"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 110
    .local v15, "endTime":J
    cmp-long v3, v17, v15

    if-gtz v3, :cond_4

    move-wide/from16 v12, v17

    .line 111
    .local v12, "daytime":J
    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->totalTimeForDay(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 112
    .local v9, "args":[Ljava/lang/String;
    const-string v3, "HomeDefaultData"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSleepData qualtyValue : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const/4 v3, 0x0

    aget-object v3, v9, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 115
    .local v20, "totalDaySleepTime":J
    const-wide/32 v3, 0xea60

    div-long v22, v20, v3

    .line 116
    .local v22, "totalMinute":J
    move-wide/from16 v0, v22

    long-to-int v3, v0

    div-int/lit8 v24, v3, 0x3c

    .line 117
    .local v24, "totalSleepHour":I
    move-wide/from16 v0, v22

    long-to-int v3, v0

    rem-int/lit8 v25, v3, 0x3c

    .line 119
    .local v25, "totalSleepMinute":I
    const-string v19, ""

    .line 121
    .local v19, "titleText":Ljava/lang/String;
    if-lez v24, :cond_0

    .line 123
    const/4 v3, 0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_5

    .line 124
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    const v6, 0x7f090d71

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 129
    :cond_0
    :goto_1
    if-lez v25, :cond_1

    .line 131
    const/4 v3, 0x1

    move/from16 v0, v25

    if-ne v0, v3, :cond_6

    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    const v6, 0x7f090d73

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 136
    :cond_1
    :goto_2
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleText:Ljava/lang/String;

    .line 138
    const/4 v3, 0x1

    aget-object v3, v9, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v11

    .line 139
    .local v11, "dayEfficiency":F
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0910bc

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    float-to-int v8, v11

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    .line 140
    .local v26, "valueText":Ljava/lang/String;
    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->valueText:Ljava/lang/String;

    .line 141
    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 142
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->isNeedShowWearableIcon:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v9    # "args":[Ljava/lang/String;
    .end local v11    # "dayEfficiency":F
    .end local v12    # "daytime":J
    .end local v15    # "endTime":J
    .end local v17    # "startTime":J
    .end local v19    # "titleText":Ljava/lang/String;
    .end local v20    # "totalDaySleepTime":J
    .end local v22    # "totalMinute":J
    .end local v24    # "totalSleepHour":I
    .end local v25    # "totalSleepMinute":I
    .end local v26    # "valueText":Ljava/lang/String;
    :cond_2
    :goto_3
    if-eqz v10, :cond_3

    .line 151
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 154
    :cond_3
    :goto_4
    return-void

    .restart local v15    # "endTime":J
    .restart local v17    # "startTime":J
    :cond_4
    move-wide v12, v15

    .line 110
    goto/16 :goto_0

    .line 126
    .restart local v9    # "args":[Ljava/lang/String;
    .restart local v12    # "daytime":J
    .restart local v19    # "titleText":Ljava/lang/String;
    .restart local v20    # "totalDaySleepTime":J
    .restart local v22    # "totalMinute":J
    .restart local v24    # "totalSleepHour":I
    .restart local v25    # "totalSleepMinute":I
    :cond_5
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    const v7, 0x7f090d72

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v7, v8

    invoke-static {v4, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_1

    .line 134
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    const v7, 0x7f090f6d

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v7, v8

    invoke-static {v4, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_2

    .line 144
    .end local v9    # "args":[Ljava/lang/String;
    .end local v12    # "daytime":J
    .end local v15    # "endTime":J
    .end local v17    # "startTime":J
    .end local v19    # "titleText":Ljava/lang/String;
    .end local v20    # "totalDaySleepTime":J
    .end local v22    # "totalMinute":J
    .end local v24    # "totalSleepHour":I
    .end local v25    # "totalSleepMinute":I
    :cond_7
    const-wide/16 v3, -0x1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 147
    :catch_0
    move-exception v14

    .line 148
    .local v14, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 150
    if-eqz v10, :cond_3

    .line 151
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 150
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v10, :cond_8

    .line 151
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v3
.end method

.method private setThermoHygrometerData()V
    .locals 7

    .prologue
    .line 280
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getCreateTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 281
    iget-wide v3, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    .line 282
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 297
    :goto_0
    return-void

    .line 286
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getHumidity()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v0, v3

    .line 287
    .local v0, "humid":F
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getTemperature()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v1, v3

    .line 289
    .local v1, "temp":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v2

    .line 290
    .local v2, "temperatureUnit":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "F"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 291
    const-string v3, "C"

    const-string v4, "F"

    invoke-direct {p0, v1, v3, v4}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v1

    .line 293
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    const v5, 0x7f0900ce

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    float-to-int v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " %"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleText:Ljava/lang/String;

    goto :goto_0

    .line 295
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    const v5, 0x7f0900cd

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    float-to-int v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " %"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleText:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private setUvdata()V
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 218
    const/4 v2, 0x0

    .line 219
    .local v2, "projection":[Ljava/lang/String;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const-string/jumbo v0, "uv_index"

    aput-object v0, v2, v1

    const-string/jumbo v0, "sample_time"

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 221
    .restart local v2    # "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 222
    .local v6, "cursor":Landroid/database/Cursor;
    iput-wide v4, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 225
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v5, "sample_time DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 226
    if-eqz v6, :cond_0

    .line 227
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 228
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 229
    const-string/jumbo v0, "uv_index"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 230
    .local v9, "index":I
    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->getUVComment(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleText:Ljava/lang/String;

    .line 231
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    .line 233
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 234
    .local v7, "deviceType":Ljava/lang/String;
    if-eqz v7, :cond_0

    const-string v0, "_"

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 235
    const-string v0, "_"

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x272d

    if-eq v0, v1, :cond_0

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->isNeedShowWearableIcon:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    .end local v7    # "deviceType":Ljava/lang/String;
    .end local v9    # "index":I
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 246
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_1
    :goto_1
    return-void

    .line 239
    :cond_2
    const-wide/16 v0, -0x1

    :try_start_1
    iput-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    :catch_0
    move-exception v8

    .line 243
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 245
    if-eqz v6, :cond_1

    .line 246
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 245
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 246
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private totalTimeForDay(J)Ljava/lang/String;
    .locals 21
    .param p1, "dayTime"    # J

    .prologue
    .line 157
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 158
    .local v9, "cal":Ljava/util/Calendar;
    move-wide/from16 v0, p1

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 159
    const/16 v2, 0xb

    invoke-virtual {v9, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xc

    if-ge v2, v3, :cond_0

    .line 160
    const/4 v2, 0x5

    const/4 v3, -0x1

    invoke-virtual {v9, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 162
    :cond_0
    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 163
    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 164
    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 165
    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-virtual {v9, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 166
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    .line 167
    .local v14, "from":J
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 168
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    .line 169
    .local v17, "to":J
    const-wide/16 v19, 0x0

    .line 170
    .local v19, "totalSleepDuration":J
    const/4 v13, 0x0

    .line 171
    .local v13, "efficiency":F
    const/16 v16, 0x0

    .line 172
    .local v16, "motionLessTime":F
    const/4 v10, 0x0

    .line 174
    .local v10, "count":I
    const-string v8, "SELECT count(*) as count, a.rise_time as RISE_TIME, a.bed_time as BED_TIME,  sum(a.rise_time-a.bed_time) AS RANGE,strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day, abs(a.bed_time-b.q) AS dayDataLong , sum(abs(a.rise_time-a.bed_time) * (a.efficiency/100)) as motionless_time_old, sum(abs(a.rise_time-a.bed_time))*ROUND((SUM(abs(a.rise_time-a.bed_time)*a.efficiency/100)*100)/sum(abs(a.rise_time-a.bed_time)),0)/100 AS motionless_time, AVG(a.efficiency) AS efficiency FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE a.sync_status != 170004 AND a.bed_time >="

    .line 184
    .local v8, "QUERY_PART":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND a.bed_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v17

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GROUP BY day ORDER BY a.bed_time ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 186
    .local v5, "query":Ljava/lang/String;
    const/4 v11, 0x0

    .line 188
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 190
    if-eqz v11, :cond_1

    .line 191
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 192
    const-string v2, "count"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 193
    const-string v2, "count"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 194
    const-string v2, "RANGE"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v19

    .line 195
    const-string v2, "efficiency"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v13

    .line 196
    move-wide/from16 v0, v19

    long-to-float v2, v0

    mul-float/2addr v2, v13

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v16, v2, v3

    .line 210
    :cond_1
    :goto_0
    if-eqz v11, :cond_2

    .line 211
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 214
    :cond_2
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v13}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 199
    :cond_3
    :try_start_1
    const-string v2, "RANGE"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    add-long v19, v19, v2

    .line 200
    const-string/jumbo v2, "motionless_time"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v16

    .line 201
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 202
    move-wide/from16 v0, v19

    long-to-float v2, v0

    div-float v2, v16, v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    float-to-int v2, v2

    int-to-float v13, v2

    goto :goto_0

    .line 207
    :catch_0
    move-exception v12

    .line 208
    .local v12, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 210
    if-eqz v11, :cond_2

    .line 211
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 210
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_4

    .line 211
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method


# virtual methods
.method public getTime()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->mTime:J

    return-wide v0
.end method

.method public getTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleText:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleTextSpanned()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->titleTextSpanned:Landroid/text/Spanned;

    return-object v0
.end method

.method public getValueText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->valueText:Ljava/lang/String;

    return-object v0
.end method

.method public isNeedShowWearableIcon()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->isNeedShowWearableIcon:Z

    return v0
.end method

.method public setLatestData(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->initValue()V

    .line 72
    sparse-switch p1, :sswitch_data_0

    .line 87
    :goto_0
    return-void

    .line 74
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->setSleepData()V

    .line 75
    const-string v0, "HomeDefaultData"

    const-string/jumbo v1, "setSleepData"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 78
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->setUvdata()V

    .line 79
    const-string v0, "HomeDefaultData"

    const-string/jumbo v1, "setUvdata"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/data/HomeDefaultData;->setThermoHygrometerData()V

    .line 83
    const-string v0, "HomeDefaultData"

    const-string/jumbo v1, "setThermoHygrometerData"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 72
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x9 -> :sswitch_2
        0x16 -> :sswitch_1
    .end sparse-switch
.end method
