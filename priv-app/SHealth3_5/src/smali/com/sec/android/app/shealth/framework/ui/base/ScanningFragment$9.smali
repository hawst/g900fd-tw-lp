.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->requestPairedDevices()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/16 v3, 0x8

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedListContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedListContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDevicesLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedDevicesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->no_connected_devices:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->no_connected_accessories_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInformationView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_3
.end method
