.class Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity$2;
.super Ljava/lang/Object;
.source "CignaBadgeListActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;->sortBadgeListByTime(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/apiobjects/Badge;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/apiobjects/Badge;Lcom/cigna/coach/apiobjects/Badge;)I
    .locals 2
    .param p1, "object1"    # Lcom/cigna/coach/apiobjects/Badge;
    .param p2, "object2"    # Lcom/cigna/coach/apiobjects/Badge;

    .prologue
    .line 170
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeEarnedDate()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeEarnedDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 166
    check-cast p1, Lcom/cigna/coach/apiobjects/Badge;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/apiobjects/Badge;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/CignaBadgeListActivity$2;->compare(Lcom/cigna/coach/apiobjects/Badge;Lcom/cigna/coach/apiobjects/Badge;)I

    move-result v0

    return v0
.end method
