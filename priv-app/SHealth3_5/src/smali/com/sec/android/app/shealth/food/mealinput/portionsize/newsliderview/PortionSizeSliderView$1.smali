.class Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;
.super Ljava/lang/Object;
.source "PortionSizeSliderView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->getWidth()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMainContainerWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$002(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;I)I

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mMainContainerWidth:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$000(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleViewWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$102(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;I)I

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mHandlerView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$400(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mScaleView:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$200(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->mStartHandlerWidth:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$300(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView$1;->this$0:Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->moveHandlerToCurrentValue()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;->access$500(Lcom/sec/android/app/shealth/food/mealinput/portionsize/newsliderview/PortionSizeSliderView;)V

    .line 127
    return-void
.end method
