.class Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;
.super Ljava/lang/Object;
.source "ListChooseDialog.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/view/View;)V
    .locals 4
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 254
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v0

    .line 255
    .local v0, "onSaveListener":Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    iget-object v2, v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 256
    .local v1, "position":I
    if-eqz v0, :cond_0

    .line 257
    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;->onSave(I)V

    .line 258
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismissAllowStateLoss()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    .line 259
    return-void
.end method
