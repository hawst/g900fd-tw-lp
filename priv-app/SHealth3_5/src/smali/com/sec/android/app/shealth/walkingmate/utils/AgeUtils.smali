.class public Lcom/sec/android/app/shealth/walkingmate/utils/AgeUtils;
.super Ljava/lang/Object;
.source "AgeUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAgeGroup(I)Ljava/lang/String;
    .locals 2
    .param p0, "age"    # I

    .prologue
    .line 26
    if-gez p0, :cond_0

    const/4 p0, 0x0

    .line 27
    :cond_0
    const/16 v0, 0x64

    if-le p0, v0, :cond_1

    const/16 p0, 0x64

    .line 28
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    div-int/lit8 v1, p0, 0xa

    aget-object v0, v0, v1

    return-object v0
.end method
