.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$3;
.super Ljava/lang/Object;
.source "ExerciseProHRMFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->createMeasurementDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 337
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sth  measureDialog onDismiss"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 339
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 341
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 342
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 343
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 344
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFragmentHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 345
    return-void
.end method
