.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Z)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

.field final synthetic val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 40

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;->val$device:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$17;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$WearableSyncEventListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$1;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->startWearableSync(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;)V

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0x4c5f

    aput v12, v2, v11

    const/16 v11, 0x2a2f

    aput v11, v2, v10

    const/16 v10, -0x3fbd

    aput v10, v2, v9

    const/16 v9, -0x4a

    aput v9, v2, v8

    const/16 v8, -0x3bea

    aput v8, v2, v7

    const/16 v7, -0x5f

    aput v7, v2, v6

    const/16 v6, -0x2d

    aput v6, v2, v5

    const/16 v5, -0x21cb

    aput v5, v2, v4

    const/16 v4, -0x49

    aput v4, v2, v3

    const/16 v3, -0x26

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x4c3a

    aput v13, v1, v12

    const/16 v12, 0x2a4c

    aput v12, v1, v11

    const/16 v11, -0x3fd6

    aput v11, v1, v10

    const/16 v10, -0x40

    aput v10, v1, v9

    const/16 v9, -0x3b9c

    aput v9, v1, v8

    const/16 v8, -0x3c

    aput v8, v1, v7

    const/16 v7, -0x80

    aput v7, v1, v6

    const/16 v6, -0x21a9

    aput v6, v1, v5

    const/16 v5, -0x22

    aput v5, v1, v4

    const/16 v4, -0x4a

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x1dc4

    aput v38, v2, v37

    const/16 v37, -0x72

    aput v37, v2, v36

    const/16 v36, -0x4f

    aput v36, v2, v35

    const/16 v35, -0x3a

    aput v35, v2, v34

    const/16 v34, 0x655

    aput v34, v2, v33

    const/16 v33, 0x4175

    aput v33, v2, v32

    const/16 v32, 0x5a28

    aput v32, v2, v31

    const/16 v31, 0xf7a

    aput v31, v2, v30

    const/16 v30, -0x4983

    aput v30, v2, v29

    const/16 v29, -0x2d

    aput v29, v2, v28

    const/16 v28, 0x5300

    aput v28, v2, v27

    const/16 v27, -0x2ac1

    aput v27, v2, v26

    const/16 v26, -0x46

    aput v26, v2, v25

    const/16 v25, -0x1a

    aput v25, v2, v24

    const/16 v24, -0x591

    aput v24, v2, v23

    const/16 v23, -0x6c

    aput v23, v2, v22

    const/16 v22, -0x6c

    aput v22, v2, v21

    const/16 v21, -0x5191

    aput v21, v2, v20

    const/16 v20, -0x26

    aput v20, v2, v19

    const/16 v19, 0x1a6d

    aput v19, v2, v18

    const/16 v18, 0x517f

    aput v18, v2, v17

    const/16 v17, -0x6dc4

    aput v17, v2, v16

    const/16 v16, -0xb

    aput v16, v2, v15

    const/16 v15, -0x43

    aput v15, v2, v14

    const/16 v14, 0x6e

    aput v14, v2, v13

    const/16 v13, -0x21ba

    aput v13, v2, v12

    const/16 v12, -0x47

    aput v12, v2, v11

    const/16 v11, -0x55db

    aput v11, v2, v10

    const/16 v10, -0x3d

    aput v10, v2, v9

    const/16 v9, 0x5a

    aput v9, v2, v8

    const/16 v8, 0x746e

    aput v8, v2, v7

    const/16 v7, 0x7c15

    aput v7, v2, v6

    const/16 v6, -0x6ce1

    aput v6, v2, v5

    const/16 v5, -0x40

    aput v5, v2, v3

    const/16 v3, 0x7f26

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x1db0

    aput v39, v1, v38

    const/16 v38, -0x1e

    aput v38, v1, v37

    const/16 v37, -0x3c

    aput v37, v1, v36

    const/16 v36, -0x58

    aput v36, v1, v35

    const/16 v35, 0x675

    aput v35, v1, v34

    const/16 v34, 0x4106

    aput v34, v1, v33

    const/16 v33, 0x5a41

    aput v33, v1, v32

    const/16 v32, 0xf5a

    aput v32, v1, v31

    const/16 v31, -0x49f1

    aput v31, v1, v30

    const/16 v30, -0x4a

    aput v30, v1, v29

    const/16 v29, 0x536c

    aput v29, v1, v28

    const/16 v28, -0x2aad

    aput v28, v1, v27

    const/16 v27, -0x2b

    aput v27, v1, v26

    const/16 v26, -0x6c

    aput v26, v1, v25

    const/16 v25, -0x5e5

    aput v25, v1, v24

    const/16 v24, -0x6

    aput v24, v1, v23

    const/16 v23, -0x5

    aput v23, v1, v22

    const/16 v22, -0x51d4

    aput v22, v1, v21

    const/16 v21, -0x52

    aput v21, v1, v20

    const/16 v20, 0x1a03

    aput v20, v1, v19

    const/16 v19, 0x511a

    aput v19, v1, v18

    const/16 v18, -0x6daf

    aput v18, v1, v17

    const/16 v17, -0x6e

    aput v17, v1, v16

    const/16 v16, -0x24

    aput v16, v1, v15

    const/16 v15, 0x1c

    aput v15, v1, v14

    const/16 v14, -0x2200

    aput v14, v1, v13

    const/16 v13, -0x22

    aput v13, v1, v12

    const/16 v12, -0x55b5

    aput v12, v1, v11

    const/16 v11, -0x56

    aput v11, v1, v10

    const/16 v10, 0x34

    aput v10, v1, v9

    const/16 v9, 0x7400

    aput v9, v1, v8

    const/16 v8, 0x7c74

    aput v8, v1, v7

    const/16 v7, -0x6c84

    aput v7, v1, v6

    const/16 v6, -0x6d

    aput v6, v1, v5

    const/16 v5, 0x7f4b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method
