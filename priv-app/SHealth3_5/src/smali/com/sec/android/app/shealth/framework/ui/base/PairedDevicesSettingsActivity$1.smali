.class Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->rename:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->popup_edittext:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/16 v2, -0x66

    aput v2, v1, v0

    const/16 v0, -0x60

    aput v0, v1, v10

    const/16 v0, 0x4c01

    aput v0, v1, v9

    const/16 v0, -0x34de

    aput v0, v1, v7

    const/16 v0, -0x52

    aput v0, v1, v8

    const/16 v0, -0x32

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v6, -0x1

    aput v6, v0, v2

    const/16 v2, -0x33

    aput v2, v0, v10

    const/16 v2, 0x4c60

    aput v2, v0, v9

    const/16 v2, -0x34b4

    aput v2, v0, v7

    const/16 v2, -0x35

    aput v2, v0, v8

    const/16 v2, -0x64

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v6, v0

    if-lt v2, v6, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
