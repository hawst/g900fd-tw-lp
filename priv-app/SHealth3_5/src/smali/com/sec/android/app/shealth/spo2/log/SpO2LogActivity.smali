.class public Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "SpO2LogActivity.java"


# static fields
.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field public static SELECTED_FILTER_INDEX:I


# instance fields
.field private mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

.field private mSpO2LogAdapter:Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

.field private mSpO2Observer:Landroid/database/ContentObserver;

.field private menu:Landroid/view/Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->SELECTED_FILTER_INDEX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 40
    new-instance v0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity$1;-><init>(Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2Observer:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->refreshAdapter()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;)Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2LogAdapter:Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

    return-object v0
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 154
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 2

    .prologue
    .line 111
    invoke-static {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    const-string v1, "0==0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->executeQuery(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mCursor:Landroid/database/Cursor;

    .line 113
    new-instance v0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2LogAdapter:Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2LogAdapter:Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

    return-object v0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    const-string v0, "comment"

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x0

    .line 131
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 132
    .local v0, "calendar":Ljava/util/Calendar;
    const-string/jumbo v6, "value"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    .line 133
    .local v3, "heartRate":F
    const-string v6, "create_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 134
    .local v4, "time":J
    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 135
    new-instance v6, Ljava/text/DateFormatSymbols;

    invoke-direct {v6}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v6}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x7

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    aget-object v2, v6, v7

    .line 136
    .local v2, "dname":Ljava/lang/String;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "dd/MM/yyyy hh:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 138
    .local v1, "date":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x5

    invoke-virtual {v1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v2, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") | "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0900d2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 159
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v1, 0x7f090076

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 165
    sput p1, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->SELECTED_FILTER_INDEX:I

    .line 166
    const-string v0, "0==0"

    return-object v0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 3

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.spo2"

    const-string v2, "SP08"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2LogAdapter:Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getShareData(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 143
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "comment":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 182
    packed-switch p1, :pswitch_data_0

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 184
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2LogAdapter:Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;->notifyDataSetChanged()V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2LogAdapter:Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogAdapter;->getTotalChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->menu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->showNoDataView()V

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x45b
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->registerObserver()V

    .line 63
    const-string v0, "com.sec.android.app.shealth.spo2"

    const-string v1, "SP03"

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const v0, 0x7f0205b7

    const v1, 0x7f090f99

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->setNoLogImageAndText(II)V

    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->setMaxItemShareLimit(I)V

    .line 68
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->unregisterObserver()V

    .line 91
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onDestroy()V

    .line 92
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 72
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPause()V

    .line 73
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 74
    .local v1, "mPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 75
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "FromSpO2LogScreen"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 76
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 77
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f080c8b

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 96
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->menu:Landroid/view/Menu;

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 98
    invoke-static {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getDataCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 99
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 100
    const v0, 0x7f080c8c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 101
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09110b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 106
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    .line 103
    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onResume()V

    .line 86
    return-void
.end method

.method public registerObserver()V
    .locals 4

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2Observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 53
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 124
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 125
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "SPO2_ID_KEY"

    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const/16 v1, 0x45b

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 127
    return-void
.end method

.method public unregisterObserver()V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2Observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 57
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    .line 150
    return-void
.end method
