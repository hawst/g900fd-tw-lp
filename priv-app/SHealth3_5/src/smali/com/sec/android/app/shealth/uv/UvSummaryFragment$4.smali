.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->initView(Landroid/view/View;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkSPF:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$302(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)Z

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateLayoutParams(Z)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateSecondIconParams(Z)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$200(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->saveSPFChecked(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$600(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)V

    .line 591
    return-void

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # setter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->checkSPF:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$302(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;Z)Z

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$400(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->spfTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->access$500(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
