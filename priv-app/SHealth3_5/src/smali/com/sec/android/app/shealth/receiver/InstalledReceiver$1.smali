.class Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;
.super Ljava/lang/Object;
.source "InstalledReceiver.java"

# interfaces
.implements Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field requestAgain:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/receiver/InstalledReceiver;

.field final synthetic val$arg0:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$moreAppsManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

.field final synthetic val$trim:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/receiver/InstalledReceiver;Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Ljava/lang/String;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->this$0:Lcom/sec/android/app/shealth/receiver/InstalledReceiver;

    iput-object p2, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$moreAppsManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    iput-object p3, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$trim:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$arg0:Landroid/content/Context;

    iput-object p5, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->requestAgain:Z

    return-void
.end method


# virtual methods
.method public onResponse(ILjava/lang/String;)V
    .locals 12
    .param p1, "event"    # I
    .param p2, "response"    # Ljava/lang/String;

    .prologue
    .line 163
    const/16 v9, 0x25d

    if-ne p1, v9, :cond_1

    iget-boolean v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->requestAgain:Z

    if-eqz v9, :cond_1

    .line 165
    iget-object v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$moreAppsManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    invoke-virtual {v9, p0}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->requestMoreAppsInfo(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;)V

    .line 166
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->requestAgain:Z

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    new-instance v2, Lcom/google/gson/GsonBuilder;

    invoke-direct {v2}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 171
    .local v2, "gsonBuilder":Lcom/google/gson/GsonBuilder;
    invoke-virtual {v2}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    .line 172
    .local v1, "gson":Lcom/google/gson/Gson;
    iget-object v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->this$0:Lcom/sec/android/app/shealth/receiver/InstalledReceiver;

    new-instance v10, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;

    iget-object v11, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->this$0:Lcom/sec/android/app/shealth/receiver/InstalledReceiver;

    invoke-direct {v10, v11}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;-><init>(Lcom/sec/android/app/shealth/receiver/InstalledReceiver;)V

    # setter for: Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->mAppList:Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;
    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->access$002(Lcom/sec/android/app/shealth/receiver/InstalledReceiver;Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;)Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;

    .line 173
    if-eqz p2, :cond_2

    .line 175
    iget-object v10, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->this$0:Lcom/sec/android/app/shealth/receiver/InstalledReceiver;

    const-class v9, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;

    invoke-virtual {v1, p2, v9}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;

    # setter for: Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->mAppList:Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;
    invoke-static {v10, v9}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->access$002(Lcom/sec/android/app/shealth/receiver/InstalledReceiver;Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;)Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;

    .line 178
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->this$0:Lcom/sec/android/app/shealth/receiver/InstalledReceiver;

    # getter for: Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->mAppList:Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;
    invoke-static {v9}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->access$000(Lcom/sec/android/app/shealth/receiver/InstalledReceiver;)Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;

    move-result-object v9

    iget-object v8, v9, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$AppList;->appList:Ljava/util/List;

    .line 179
    .local v8, "thirdPartyAppsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;>;"
    if-eqz v8, :cond_0

    .line 181
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v5, "listAppIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v6, "listPackagenames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;

    .line 185
    .local v7, "thirdPartyApp":Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;
    iget-object v9, v7, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->productID:Ljava/lang/String;

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v9, v7, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->packageName:Ljava/lang/String;

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 195
    .end local v7    # "thirdPartyApp":Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_4

    iget-object v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$trim:Ljava/lang/String;

    invoke-interface {v6, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 197
    new-instance v4, Landroid/content/Intent;

    iget-object v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$arg0:Landroid/content/Context;

    const-class v10, Lcom/sec/android/app/shealth/service/HandleAppDataService;

    invoke-direct {v4, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 198
    .local v4, "insertIntent":Landroid/content/Intent;
    const-string v9, "insert_to_database"

    const/4 v10, 0x1

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 199
    iget-object v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$intent:Landroid/content/Intent;

    invoke-virtual {v9}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 200
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 201
    iget-object v9, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$1;->val$arg0:Landroid/content/Context;

    invoke-virtual {v9, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 207
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v4    # "insertIntent":Landroid/content/Intent;
    :cond_4
    const-string v9, "InstallReceiver"

    const-string v10, "Plugin is not listed in app registry"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
