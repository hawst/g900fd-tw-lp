.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoListFactoryTask;
.super Landroid/os/AsyncTask;
.source "PhotoListUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoListFactoryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
        ">;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
        ">;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 290
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 290
    check-cast p1, [Ljava/util/List;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoListFactoryTask;->doInBackground([Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/util/List;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p1, "params":[Ljava/util/List;, "[Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    const/4 v9, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    .line 293
    if-nez p1, :cond_1

    .line 294
    const/4 v3, 0x0

    .line 319
    :cond_0
    return-object v3

    .line 296
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .local v3, "mPhotoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;>;"
    aget-object v6, p1, v9

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 298
    .local v5, "photodata":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    if-eqz v5, :cond_2

    .line 300
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v6

    cmpl-float v6, v6, v8

    if-nez v6, :cond_3

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v6

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_2

    .line 304
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 305
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 306
    .local v0, "data2":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLatitude()F

    move-result v7

    float-to-int v7, v7

    if-ne v6, v7, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getLongitude()F

    move-result v7

    float-to-int v7, v7

    if-ne v6, v7, :cond_5

    .line 308
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    .end local v0    # "data2":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v1, v6, :cond_2

    .line 313
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 314
    .local v4, "ph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    .end local v4    # "ph":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    .restart local v0    # "data2":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 290
    check-cast p1, Ljava/util/ArrayList;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoListFactoryTask;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;>;"
    const/4 v2, 0x0

    .line 324
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 325
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 326
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mIsAmap:Z

    if-eqz v0, :cond_1

    .line 327
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->aHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->access$000()Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->aHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->access$000()Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;

    move-result-object v1

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapPhotoList$UpdateHandler;->sendMessage(Landroid/os/Message;)Z

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->access$100()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->gHandler:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->access$100()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;

    move-result-object v1

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList$UpdateHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
