.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;
.super Landroid/widget/ArrayAdapter;
.source "HeartrateLogByTagDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CustomList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private age:I

.field avg_range:[I

.field private final context:Landroid/app/Activity;

.field gender:I

.field private mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

.field private mMaxHeartrate:J

.field resting_percentile:[I

.field private final tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;Landroid/app/Activity;[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;J)V
    .locals 3
    .param p2, "context"    # Landroid/app/Activity;
    .param p3, "tags"    # [Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;
    .param p4, "mMaxHeartrate"    # J

    .prologue
    const/4 v1, 0x0

    .line 115
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    .line 117
    const v0, 0x7f030161

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 106
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->age:I

    .line 111
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->gender:I

    .line 118
    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->context:Landroid/app/Activity;

    .line 119
    iput-object p3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    .line 120
    iput-wide p4, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mMaxHeartrate:J

    .line 121
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserAge()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->age:I

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMaleOrFemale()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->gender:I

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->gender:I

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->age:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAvgBPMRange(II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->avg_range:[I

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->gender:I

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->age:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->resting_percentile:[I

    .line 127
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 138
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v13

    .line 142
    .local v13, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030150

    const/4 v2, 0x0

    invoke-virtual {v13, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 145
    .local v14, "rowView":Landroid/view/View;
    const v1, 0x7f0805a9

    invoke-virtual {v14, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 147
    .local v16, "txtTitle":Landroid/widget/TextView;
    const v1, 0x7f0805a8

    invoke-virtual {v14, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 148
    .local v12, "imageView":Landroid/widget/ImageView;
    const v1, 0x7f0805ab

    invoke-virtual {v14, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 149
    .local v9, "fillView":Landroid/view/View;
    const v1, 0x7f0805aa

    invoke-virtual {v14, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 152
    .local v7, "bpmTxt":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getTagName()I

    move-result v1

    if-lez v1, :cond_0

    .line 153
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getTagName()I

    move-result v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 156
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getIcon()I

    move-result v11

    .line 157
    .local v11, "iconId":I
    const v1, 0x7f02051c

    invoke-virtual {v12, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 158
    if-eqz v11, :cond_1

    const v1, 0x7f02053d

    if-eq v11, v1, :cond_1

    .line 159
    invoke-virtual {v12, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 166
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getTagId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090f20

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getBpm()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const/4 v8, 0x0

    .line 170
    .local v8, "color":I
    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v1, v2, :cond_2

    .line 171
    const-string v1, "#73b90f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    .line 175
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->this$0:Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v15, v1, Landroid/util/DisplayMetrics;->density:F

    .line 176
    .local v15, "scale":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getBpm()J

    move-result-wide v1

    long-to-float v1, v1

    const/high16 v2, 0x43870000    # 270.0f

    mul-float/2addr v2, v15

    mul-float/2addr v1, v2

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mMaxHeartrate:J

    long-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v10, v1

    .line 177
    .local v10, "fillWidth":I
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v10, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 178
    invoke-virtual {v9, v8}, Landroid/view/View;->setBackgroundColor(I)V

    .line 179
    const/4 v1, 0x1

    invoke-virtual {v14, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 181
    const/4 v1, 0x1

    invoke-virtual {v14, v1}, Landroid/view/View;->setClickable(Z)V

    .line 184
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList$1;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;)V

    invoke-virtual {v14, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    return-object v14

    .line 155
    .end local v8    # "color":I
    .end local v10    # "fillWidth":I
    .end local v11    # "iconId":I
    .end local v15    # "scale":F
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getTagTextName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 162
    .restart local v11    # "iconId":I
    :cond_1
    const v1, 0x7f02051d

    invoke-virtual {v12, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 173
    .restart local v8    # "color":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->tags:[Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$TagListEntry;->getBpm()J

    move-result-wide v2

    long-to-int v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->age:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->gender:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->resting_percentile:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateLogByTagDetailActivity$CustomList;->avg_range:[I

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getHRMSateBarColor(III[I[I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    goto :goto_2
.end method
