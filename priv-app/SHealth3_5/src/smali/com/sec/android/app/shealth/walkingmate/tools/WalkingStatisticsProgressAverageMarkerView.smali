.class public Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;
.super Landroid/widget/LinearLayout;
.source "WalkingStatisticsProgressAverageMarkerView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;->init()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;->init()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;->init()V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0302ab

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    return-void
.end method


# virtual methods
.method public animateMe(JF)V
    .locals 2
    .param p1, "animationDuration"    # J
    .param p3, "markerValue"    # F

    .prologue
    const/4 v1, 0x0

    .line 48
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, p3, v1, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 49
    .local v0, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 50
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 51
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 52
    return-void
.end method

.method public animateMe(JFF)V
    .locals 2
    .param p1, "animationDuration"    # J
    .param p3, "markerValue"    # F
    .param p4, "textOffsetRange"    # F

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/tools/WalkingStatisticsProgressAverageMarkerView;->animateMe(JF)V

    .line 57
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, p4, v1, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 58
    .local v0, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 59
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 61
    return-void
.end method

.method public setTextString(Ljava/lang/String;)V
    .locals 0
    .param p1, "textString"    # Ljava/lang/String;

    .prologue
    .line 40
    return-void
.end method

.method public setValueString(Ljava/lang/String;)V
    .locals 0
    .param p1, "valueString"    # Ljava/lang/String;

    .prologue
    .line 36
    return-void
.end method
