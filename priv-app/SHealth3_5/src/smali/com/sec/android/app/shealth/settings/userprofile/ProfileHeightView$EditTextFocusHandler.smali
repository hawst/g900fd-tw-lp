.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;
.super Ljava/lang/Object;
.source "ProfileHeightView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextFocusHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$1;

    .prologue
    .line 488
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 491
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isChangeFocus:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1202(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;Z)Z

    .line 492
    if-nez p2, :cond_2

    .line 493
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkValueText()Z

    move-result v4

    if-nez v4, :cond_2

    .line 494
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 495
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightUnit:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)I

    move-result v4

    const v5, 0x249f1

    if-ne v4, v5, :cond_4

    .line 497
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 498
    .local v3, "valueString":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 499
    :cond_0
    const/high16 v2, -0x40800000    # -1.0f

    .line 505
    .local v2, "value":F
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mMinValue:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F

    move-result v4

    cmpg-float v4, v2, v4

    if-gez v4, :cond_1

    .line 507
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;->onProfileDialog(Landroid/view/View;)V

    .line 529
    .end local v2    # "value":F
    .end local v3    # "valueString":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setMinValueText()V

    .line 534
    :cond_2
    return-void

    .line 502
    .restart local v3    # "valueString":Ljava/lang/String;
    :cond_3
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .restart local v2    # "value":F
    goto :goto_0

    .line 513
    .end local v2    # "value":F
    .end local v3    # "valueString":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->isHeightValidValue()F
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)F

    move-result v4

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 515
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextFeet:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 516
    .local v0, "ftString":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 517
    .local v1, "inchString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    .line 519
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mHeightEditTextSecond:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 523
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->access$1000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;->onProfileDialog(Landroid/view/View;)V

    goto :goto_1
.end method
