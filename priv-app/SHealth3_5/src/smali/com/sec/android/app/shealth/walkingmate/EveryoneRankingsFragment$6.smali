.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;
.super Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;
.source "EveryoneRankingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->requestTopWalkersListUpdate(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

.field final synthetic val$isLatestIntent:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Z)V
    .locals 0

    .prologue
    .line 477
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->val$isLatestIntent:Z

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 477
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 522
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;->onPostExecute(Ljava/lang/Object;)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersUpdater:Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2202(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;)Lcom/sec/android/app/shealth/walkingmate/loaders/TopWalkersUpdater;

    .line 524
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v0, v1, :cond_0

    .line 525
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 528
    :cond_0
    return-void
.end method

.method public onUpdate(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)V
    .locals 4
    .param p2, "myHolder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p3, "result"    # Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            "Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "topTenWalkersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;>;"
    const/4 v3, 0x1

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2102(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Z)Z

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onUpdate() - requestResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isLatest : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->val$isLatestIntent:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2802(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->SUCCESS:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v0, v1, :cond_2

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mShouldSaveWalkersDataLocally:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Z)Z

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$202(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1002(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fragment dettached from Activity!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 507
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsWalkersUpdated:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2102(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Z)Z

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mRequestResult:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$2800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;->INVALID_TOKEN:Lcom/sec/android/app/shealth/walkingmate/WalkingRequestResult;

    if-ne v0, v1, :cond_3

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 514
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$6;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
