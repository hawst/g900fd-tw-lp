.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;
.super Ljava/lang/Object;
.source "HeartRateScoverFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->scaleAnimationStarter(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

.field final synthetic val$isGreen:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->val$isGreen:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 659
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsRunPulseView:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3100(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mIsGreen:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3900(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Z

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->pulseAnimationShow(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$4000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Z)V

    .line 662
    :cond_0
    invoke-virtual {p1}, Landroid/view/animation/Animation;->cancel()V

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mScaleAnimation:Landroid/view/animation/Animation;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$3002(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 664
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 655
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 650
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$9;->val$isGreen:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020526

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 651
    return-void

    .line 650
    :cond_0
    const v0, 0x7f020529

    goto :goto_0
.end method
