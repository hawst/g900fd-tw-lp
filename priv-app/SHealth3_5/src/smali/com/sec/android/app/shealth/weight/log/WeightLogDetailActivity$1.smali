.class Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;
.super Ljava/lang/Object;
.source "WeightLogDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 214
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 215
    .local v0, "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 240
    :goto_0
    return-void

    .line 218
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    const-class v3, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 219
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 220
    const-string v2, "MODE_KEY"

    const-string v3, "Edit"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    const-string v2, "WEIGHT_ID_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getId()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 222
    const-string v2, "TIME_DATE_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mTimeDate:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$100(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const-string/jumbo v2, "weight"

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeight:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$200(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    const-string v3, "COMMENT_KEY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, ""

    :goto_1
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    const-string v2, "INPUT_ACTIVITY_DATA_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-static {v3}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 226
    const-string v2, "INPUT_ACTIVITY_MODE_KEY"

    const-string v3, "Edit"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string v2, "log"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->isfromaccesory:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$300(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    const-string v2, "accessory"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 235
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    const/16 v3, 0x2711

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 224
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 232
    :cond_1
    const-string v2, "accessory"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
