.class public Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "ExerciseMateGraphFragmentSIC.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC$2;
    }
.end annotation


# static fields
.field private static final DAY_IN_MONTH:B = 0x1ft

.field private static final HOURS_IN_DAY_BYTE:B = 0x18t

.field private static final MILLIS_IN_DAY:I = 0x5265c00

.field private static final MILLIS_IN_HOUR:I = 0x36ee80

.field private static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field private static final MILLIS_IN_SECOND:I = 0x3e8

.field private static final MINUTES_IN_HOUR:B = 0x3ct

.field private static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static isDataPresent:Z

.field private static volatile mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# instance fields
.field density:F

.field private exercisemateInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;

.field private isConfigurationChanged:Z

.field private mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field private mHandleOverBitmapYelloGreen:Landroid/graphics/Bitmap;

.field private mLegendView:Landroid/view/View;

.field private mNormalBitmapYelloGreen:Landroid/graphics/Bitmap;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mRootView:Landroid/view/View;

.field private switchToWalkingSummary:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isDataPresent:Z

    .line 81
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 531
    return-void
.end method

.method private static getLatestDataTimestamp(Ljava/lang/String;J)J
    .locals 7
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "dateTime"    # J

    .prologue
    .line 456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select start_time from exercise_activity where calorie is NOT NULL AND calorie !=0  AND strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') = strftime(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000),\'unixepoch\', \'localtime\') order by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 461
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 464
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 465
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 467
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 468
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 473
    if-eqz v6, :cond_0

    .line 474
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 476
    :cond_0
    :goto_0
    return-wide v0

    .line 473
    :cond_1
    if-eqz v6, :cond_2

    .line 474
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 476
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 473
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 474
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private static getStartHourToMillis(J)J
    .locals 3
    .param p0, "time"    # J

    .prologue
    const/4 v2, 0x0

    .line 480
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 481
    .local v0, "tempCalendar":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 482
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 483
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 484
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 485
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method public static isDataPresent()Z
    .locals 1

    .prologue
    .line 639
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isDataPresent:Z

    return v0
.end method


# virtual methods
.method protected createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 22
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 324
    new-instance v12, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-direct {v12}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;-><init>()V

    .line 325
    .local v12, "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v12, v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 327
    new-instance v15, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v15, v2}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 329
    .local v15, "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_5

    .line 330
    const/4 v4, 0x0

    .line 331
    .local v4, "projection":[Ljava/lang/String;
    const-string v5, "exercise_type != 20003) GROUP BY (strftime(\"%d-%m-%Y %H\",([Exercise].[start_time]/1000),\'unixepoch\',\'localtime\')"

    .line 333
    .local v5, "selectionClause":Ljava/lang/String;
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, " SUM(total_calorie) AS Sum_Kcal"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "start_time"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, "exercise_info__id"

    aput-object v3, v4, v2

    .line 337
    .restart local v4    # "projection":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 340
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const-string/jumbo v7, "start_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 342
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_2

    .line 343
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 344
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 345
    const-wide/16 v2, 0x0

    const-string v6, "Sum_Kcal"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    cmpg-double v2, v2, v6

    if-gez v2, :cond_0

    .line 346
    new-instance v11, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v11}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 347
    .local v11, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-string/jumbo v2, "start_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 348
    .local v18, "time":J
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfHour(J)J

    move-result-wide v2

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfHour(J)J

    move-result-wide v6

    const-wide/16 v20, 0x1

    add-long v6, v6, v20

    invoke-virtual {v15, v2, v3, v6, v7}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v2

    float-to-double v13, v2

    .line 349
    .local v13, "kcal":D
    invoke-static {v13, v14}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-double v2, v2

    move-wide/from16 v0, v18

    invoke-virtual {v11, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2, v11}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 352
    .end local v11    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v13    # "kcal":D
    .end local v18    # "time":J
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 356
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_1

    .line 357
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .line 356
    :cond_2
    if-eqz v10, :cond_3

    .line 357
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 438
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_3
    :goto_1
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 439
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isDataPresent:Z

    .line 442
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuVisible()Z

    move-result v2

    if-nez v2, :cond_4

    .line 444
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->FroceRecreateActionbar()V

    .line 446
    :cond_4
    return-object v12

    .line 360
    :cond_5
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_9

    .line 361
    const/4 v4, 0x0

    .line 362
    .restart local v4    # "projection":[Ljava/lang/String;
    const-string v5, "exercise_type != 20003) GROUP BY (strftime(\"%d-%m-%Y\",([Exercise].[start_time]/1000),\'unixepoch\',\'localtime\')"

    .line 364
    .restart local v5    # "selectionClause":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, " SUM(ROUND(total_calorie)) AS Sum_Kcal"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "start_time"

    aput-object v3, v4, v2

    .line 369
    .restart local v4    # "projection":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 372
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const-string/jumbo v7, "start_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 374
    if-eqz v10, :cond_8

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_8

    .line 375
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 376
    :goto_3
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_8

    .line 377
    const-wide/16 v2, 0x0

    const-string v6, "Sum_Kcal"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    cmpg-double v2, v2, v6

    if-gez v2, :cond_6

    .line 378
    new-instance v11, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v11}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 379
    .restart local v11    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    const-string/jumbo v2, "start_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 380
    .restart local v18    # "time":J
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v6

    const-wide/16 v20, 0x1

    add-long v6, v6, v20

    invoke-virtual {v15, v2, v3, v6, v7}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v2

    float-to-double v13, v2

    .line 382
    .restart local v13    # "kcal":D
    const-string/jumbo v2, "start_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v13, v14}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-double v6, v6

    invoke-virtual {v11, v2, v3, v6, v7}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2, v11}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 386
    .end local v11    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v13    # "kcal":D
    .end local v18    # "time":J
    :cond_6
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    .line 390
    :catchall_1
    move-exception v2

    if-eqz v10, :cond_7

    .line 391
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2

    .line 390
    :cond_8
    if-eqz v10, :cond_3

    .line 391
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 394
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_9
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_3

    .line 395
    const/4 v4, 0x0

    .line 396
    .restart local v4    # "projection":[Ljava/lang/String;
    const-string v5, "exercise_type != 20003) GROUP BY (strftime(\"%m-%Y\",([Exercise].[start_time]/1000),\'unixepoch\',\'localtime\')"

    .line 398
    .restart local v5    # "selectionClause":Ljava/lang/String;
    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "round(sum(exercise.[total_calorie])/count(distinct(strftime(\'%d-%m-%Y\',(exercise.[start_time]/1000), \'unixepoch\',\'localtime\')))) AS AVG_KCAL"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "count(distinct(strftime(\'%d-%m-%Y\',(exercise.[start_time]/1000), \'unixepoch\',\'localtime\'))) as COUNT"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, " SUM(round(total_calorie)) AS SUM_Kcal"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "start_time"

    aput-object v3, v4, v2

    .line 405
    .restart local v4    # "projection":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 408
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const-string/jumbo v7, "start_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 410
    if-eqz v10, :cond_d

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_d

    .line 411
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 412
    :goto_4
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_d

    .line 413
    const-string/jumbo v2, "start_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 414
    .restart local v18    # "time":J
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v2

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v6

    const-wide/16 v20, 0x1

    add-long v6, v6, v20

    invoke-virtual {v15, v2, v3, v6, v7}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v16, v0

    .line 417
    .local v16, "sum_kcal":D
    const-wide/16 v8, 0x0

    .line 418
    .local v8, "avg_kcal":J
    const-string v2, "COUNT"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_a

    .line 419
    const-string v2, "COUNT"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-double v2, v2

    div-double v2, v16, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    .line 422
    :cond_a
    const-wide/16 v2, 0x0

    cmp-long v2, v2, v8

    if-gez v2, :cond_b

    .line 423
    new-instance v11, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v11}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 424
    .restart local v11    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    long-to-double v2, v8

    move-wide/from16 v0, v18

    invoke-virtual {v11, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2, v11}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 429
    .end local v11    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_b
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_4

    .line 433
    .end local v8    # "avg_kcal":J
    .end local v16    # "sum_kcal":D
    .end local v18    # "time":J
    :catchall_2
    move-exception v2

    if-eqz v10, :cond_c

    .line 434
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 433
    :cond_d
    if-eqz v10, :cond_3

    .line 434
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 441
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_e
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isDataPresent:Z

    goto/16 :goto_2
.end method

.method protected customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V
    .locals 3
    .param p1, "chartInteraction"    # Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 625
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 627
    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 628
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 636
    :goto_0
    return-void

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_1

    .line 630
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 631
    const v0, 0x408db6db

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_0

    .line 633
    :cond_1
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 634
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_0
.end method

.method protected customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 4
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/4 v3, 0x0

    .line 305
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 307
    const/high16 v1, 0x41800000    # 16.0f

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v3, v3, v3, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 310
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    .line 311
    .local v0, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 312
    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 315
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->density:F

    const/high16 v2, 0x42080000    # 34.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 316
    return-void
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 604
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->exercisemateInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 609
    const v0, 0x7f02078b

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 615
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 451
    const v0, 0x7f0205b7

    return v0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0900b9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 147
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 148
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 150
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 151
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 3

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 200
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0300b6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v2, 0x7f080356

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v1
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 1
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 155
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 156
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 157
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 158
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 192
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->exercisemateInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->exercisemateInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->exercisemateInformationArea:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;

    return-object v0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 3
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203c0

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mNormalBitmapYelloGreen:Landroid/graphics/Bitmap;

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203c1

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mHandleOverBitmapYelloGreen:Landroid/graphics/Bitmap;

    .line 291
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;-><init>()V

    .line 292
    .local v0, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 293
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setValueMarkingVisible(Z)V

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mNormalBitmapYelloGreen:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mHandleOverBitmapYelloGreen:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 296
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setValueMarkingSize(F)V

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070158

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setBarColor(I)V

    .line 298
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setBarWidth(F)V

    .line 299
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 301
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 175
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->onConfiguarationLanguageChanged()V

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isConfigurationChanged:Z

    .line 178
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "EXERCISE_CHARTTAB"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 111
    .local v0, "mPeriod":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 112
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 117
    :goto_0
    return-void

    .line 113
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 114
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0

    .line 116
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->density:F

    .line 123
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->setHasOptionsMenu(Z)V

    .line 130
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    .line 131
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a015f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    return-object v1
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.plugins.exercisemate"

    const-string v2, "PQ35"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mHandleOverBitmapYelloGreen:Landroid/graphics/Bitmap;

    .line 101
    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mNormalBitmapYelloGreen:Landroid/graphics/Bitmap;

    .line 102
    sput-object v3, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 103
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDestroyView()V

    .line 104
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onResume()V

    .line 184
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isConfigurationChanged:Z

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->initGeneralView()V

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isConfigurationChanged:Z

    .line 188
    :cond_0
    return-void
.end method

.method public savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 163
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_1

    .line 164
    const-string v1, "EXERCISE_CHARTTAB"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 170
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 171
    return-void

    .line 165
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_2

    .line 166
    const-string v1, "EXERCISE_CHARTTAB"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 167
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    .line 168
    const-string v1, "EXERCISE_CHARTTAB"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 13
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    .line 491
    const/4 v1, 0x0

    .line 492
    .local v1, "level":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_4

    .line 493
    const/4 v1, 0x1

    .line 500
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->getSelectedDateInChart()J

    move-result-wide v9

    .line 501
    .local v9, "selectedTime":J
    :goto_1
    const-wide/16 v7, 0x0

    .line 502
    .local v7, "dataToShow":J
    const-wide/16 v11, 0x0

    .line 503
    .local v11, "startTime":J
    const/16 v5, 0xc

    .line 504
    .local v5, "markingCount":I
    const/4 v4, 0x1

    .line 506
    .local v4, "intervel":I
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 510
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC$2;->$SwitchMap$com$sec$android$app$shealth$framework$ui$graph$PeriodH:[I

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 527
    const-string v0, "%Y-%m"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 528
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 530
    :cond_2
    const/16 v5, 0xc

    .line 534
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 535
    .local v6, "calendar":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    cmp-long v0, v7, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v0, v2, :cond_3

    .line 537
    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 538
    const/16 v0, 0xb

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 539
    const/16 v0, 0xc

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 540
    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 541
    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 542
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 544
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v7, v8, v0, v5}, Lcom/sec/android/app/shealth/heartrate/utils/SupportUtils;->getGraphStartTime(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;I)J

    move-result-wide v11

    .line 546
    long-to-double v2, v11

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 547
    long-to-double v2, v7

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 549
    const/4 v0, 0x0

    const/4 v2, 0x3

    invoke-virtual {p1, v0, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 600
    return-void

    .line 494
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v7    # "dataToShow":J
    .end local v9    # "selectedTime":J
    .end local v11    # "startTime":J
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_5

    .line 495
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 496
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_0

    .line 497
    const/4 v1, 0x5

    goto/16 :goto_0

    .line 500
    :cond_6
    const-wide/16 v9, 0x0

    goto/16 :goto_1

    .line 513
    .restart local v4    # "intervel":I
    .restart local v5    # "markingCount":I
    .restart local v7    # "dataToShow":J
    .restart local v9    # "selectedTime":J
    .restart local v11    # "startTime":J
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_8

    const-string v0, "%Y-%m-%d"

    :goto_3
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 514
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 516
    :cond_7
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setScrollRangeDepthLevel(I)V

    .line 518
    const/16 v5, 0xc

    .line 519
    goto/16 :goto_2

    .line 513
    :cond_8
    const-string v0, "%Y-%m"

    goto :goto_3

    .line 521
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_a

    const-string v0, "%Y-%m-%d"

    :goto_4
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 522
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mCalBarSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 524
    :cond_9
    const/4 v5, 0x7

    .line 525
    goto/16 :goto_2

    .line 521
    :cond_a
    const-string v0, "%Y-%m"

    goto :goto_4

    .line 510
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateGraphFragment()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 140
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->updateGraphFragment()V

    .line 142
    :cond_0
    return-void
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 320
    return-void
.end method
