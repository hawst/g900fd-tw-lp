.class Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$4;
.super Ljava/lang/Object;
.source "AutoBackupActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    .line 310
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupWifiEnabled(Landroid/content/Context;Z)V

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 313
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    .line 314
    .local v0, "shcm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->useWifiForBackup(Z)Z

    .line 319
    .end local v0    # "shcm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    :cond_0
    return-void
.end method
