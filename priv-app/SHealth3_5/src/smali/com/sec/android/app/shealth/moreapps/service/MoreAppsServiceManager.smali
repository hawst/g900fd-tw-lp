.class public Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;
.super Ljava/lang/Object;
.source "MoreAppsServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$2;,
        Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$LoginListener;,
        Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;
    }
.end annotation


# static fields
.field private static final ERROR_RESPONSE_CANCELLED:I = 0x25d

.field private static final ERROR_RESPONSE_JSON:I = 0x25b

.field private static final ERROR_RESPONSE_LOGIN_INVALID:I = 0x259

.field private static final ERROR_RESPONSE_NETWORK:I = 0x25a

.field private static final ERROR_RESPONSE_NUMBER_FORMAT:I = 0x25e

.field private static final ERROR_RESPONSE_PARSE:I = 0x25c

.field private static final ERROR_RESPONSE_UNKNOWN:I = 0x258

.field private static final KEY_SYSTEM_PROPS_CSC1:Ljava/lang/String; = "ro.csc.sales_code"

.field private static final KEY_SYSTEM_PROPS_CSC2:Ljava/lang/String; = "ril.csc.sales_code"

.field private static final KEY_SYSTEM_PROPS_CSC3:Ljava/lang/String; = "ril.sales_code"

.field private static final SERVER_ADDR_CN:Ljava/lang/String; = "cn-openapi.samsungshealth.com"

.field private static final SERVER_ADDR_EU:Ljava/lang/String; = "eu-openapi.samsungshealth.com"

.field private static final SERVER_ADDR_KR:Ljava/lang/String; = "kr-openapi.samsungshealth.com"

.field private static final SERVER_ADDR_US:Ljava/lang/String; = "openapi.samsungshealth.com"

.field public static final SORTTYPE_ASCENDING_ORDER:I = 0x1

.field public static final SORTTYPE_DESCENDING_ORDER:I = 0x2

.field private static final SUCCESS_RESPONSE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MoreAppsServiceManager"

.field private static apiServer:Ljava/lang/String;

.field public static checkedJson:Ljava/lang/String;

.field public static regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

.field public static serverJson:Ljava/lang/String;


# instance fields
.field private final EMPTY_RESPONSE_STRING:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    const-string/jumbo v0, "openapi.samsungshealth.com"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->apiServer:Ljava/lang/String;

    .line 77
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    .line 82
    sput-object v1, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    .line 83
    sput-object v1, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->checkedJson:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const-string/jumbo v0, "{}"

    iput-object v0, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->EMPTY_RESPONSE_STRING:Ljava/lang/String;

    .line 87
    iput-object p1, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getMoreAppsInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;
    .param p1, "x1"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->makeMoreAppsInfo(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;
    .param p1, "x1"    # Lorg/json/JSONObject;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->addLanguageText(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method private addLanguageText(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 4
    .param p1, "jsonObj"    # Lorg/json/JSONObject;

    .prologue
    .line 463
    :try_start_0
    const-string v1, "catWellness"

    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090cdb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 464
    const-string v1, "catCare"

    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090cdc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 465
    const-string/jumbo v1, "noApp"

    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090cdf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 473
    :goto_0
    return-object p1

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    .line 469
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_1
    move-exception v0

    .line 470
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private getMoreAppsInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "jsonString"    # Ljava/lang/String;

    .prologue
    .line 410
    const/4 v3, 0x0

    .line 413
    .local v3, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 415
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .local v4, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    const-string v6, "appList"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 417
    .local v2, "jsonArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 418
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 419
    .local v5, "subObject":Lorg/json/JSONObject;
    const-string/jumbo v6, "packageName"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 420
    const-string v6, "installedYN"

    const-string/jumbo v7, "n"

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 417
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 423
    :cond_0
    const-string v6, "installedYN"

    const-string/jumbo v7, "packageName"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/sec/android/app/shealth/moreapps/service/Util;->getInstalledApp(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 426
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "subObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 428
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v3    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 431
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_3
    if-nez v3, :cond_2

    const/4 v6, 0x0

    :goto_4
    return-object v6

    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .restart local v1    # "i":I
    .restart local v2    # "jsonArray":Lorg/json/JSONArray;
    .restart local v4    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    move-object v3, v4

    .line 429
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    .restart local v3    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3

    .line 431
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    :cond_2
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 426
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private getSystemProperties(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 107
    const/4 v2, 0x0

    .line 111
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 113
    .local v3, "systemProperties":Ljava/lang/Class;
    if-eqz v3, :cond_0

    .line 114
    const-string v4, "get"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v4, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v3    # "systemProperties":Ljava/lang/Class;
    :cond_0
    :goto_0
    return-object v2

    .line 116
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private makeMoreAppsInfo(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 6
    .param p1, "jsonObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 440
    const-string v3, "appList"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 444
    .local v1, "jsonArray":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 447
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 449
    .local v2, "subObject":Lorg/json/JSONObject;
    const-string/jumbo v3, "packageName"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 450
    const-string v3, "installedYN"

    const-string/jumbo v4, "n"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 444
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 452
    :cond_0
    const-string v3, "installedYN"

    iget-object v4, p0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "packageName"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/moreapps/service/Util;->getInstalledApp(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 456
    .end local v2    # "subObject":Lorg/json/JSONObject;
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->addLanguageText(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    .line 458
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public checkMoreAppsInfo(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 385
    const/4 v2, 0x0

    .line 387
    .local v2, "response":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->checkedJson:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 388
    sget-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getMoreAppsInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 391
    .local v1, "newJson":Ljava/lang/String;
    sput-object v1, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->checkedJson:Ljava/lang/String;

    .line 394
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    sget-object v4, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->makeMoreAppsInfo(Lorg/json/JSONObject;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 406
    .end local v1    # "newJson":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 396
    .restart local v1    # "newJson":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 398
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 399
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 401
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public clearMoreAppsInfo()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 380
    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    .line 381
    sput-object v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->checkedJson:Ljava/lang/String;

    .line 382
    return-void
.end method

.method public getCSC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getSystemProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "csc":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 96
    :cond_0
    const-string/jumbo v1, "ril.csc.sales_code"

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getSystemProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 99
    :cond_1
    const-string/jumbo v1, "ril.sales_code"

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getSystemProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_2
    return-object v0
.end method

.method public requestMoreAppsInfo(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;)V
    .locals 40
    .param p1, "listener"    # Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    .prologue
    .line 136
    const-string v25, "/platform/common/getMoreAppsList"

    .line 138
    .local v25, "API":Ljava/lang/String;
    const-string v7, ""

    .line 139
    .local v7, "mcc":Ljava/lang/String;
    const-string v12, ""

    .line 140
    .local v12, "mnc":Ljava/lang/String;
    const-string v17, ""

    .line 142
    .local v17, "csc":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Landroid/telephony/TelephonyManager;

    .line 143
    .local v39, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v39 .. v39}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v38

    .line 145
    .local v38, "simOperator":Ljava/lang/String;
    if-eqz v38, :cond_0

    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_0

    .line 146
    const/4 v3, 0x0

    const/4 v4, 0x3

    move-object/from16 v0, v38

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 147
    const/4 v3, 0x3

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 149
    :cond_0
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mDeviceId:Ljava/lang/String;

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getCSC()Ljava/lang/String;

    move-result-object v17

    .line 152
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 153
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 154
    :cond_1
    const-string v7, "460"

    .line 156
    :cond_2
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 157
    :cond_3
    const-string v12, "01"

    .line 160
    :cond_4
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_b

    :cond_5
    const-string v34, "310"

    .line 162
    .local v34, "mccRegion":Ljava/lang/String;
    :goto_0
    invoke-static/range {v34 .. v34}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->getRegionGroup(Ljava/lang/String;)Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    .line 164
    sget-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    if-nez v3, :cond_6

    .line 165
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 166
    const-string v7, "460"

    .line 167
    const-string v12, "01"

    .line 168
    sget-object v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    .line 175
    :cond_6
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$2;->$SwitchMap$com$sec$android$service$health$cp$common$HttpConnectionConstants$RegionGroup:[I

    sget-object v4, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 190
    const-string/jumbo v3, "openapi.samsungshealth.com"

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->apiServer:Ljava/lang/String;

    .line 194
    :goto_2
    if-eqz v12, :cond_7

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 195
    :cond_7
    const-string v12, "99999"

    .line 197
    :cond_8
    if-eqz v17, :cond_9

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-ge v3, v4, :cond_a

    .line 198
    :cond_9
    const-string v17, "99999"

    .line 202
    :cond_a
    sget-object v3, Lcom/sec/android/app/shealth/moreapps/service/Util;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ":: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " :: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " :: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " :: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_CACHE_TIME_KEY:Ljava/lang/String;

    const-wide/16 v5, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/moreapps/service/Util;->getPrefLong(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v30

    .line 205
    .local v30, "cacheTime":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_TIME_KEY:Ljava/lang/String;

    const-wide/16 v5, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/moreapps/service/Util;->getPrefLong(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v27

    .line 206
    .local v27, "apiTime":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_API_JSON_KEY:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/moreapps/service/Util;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 208
    .local v26, "apiJson":Ljava/lang/String;
    const-wide/16 v3, 0x0

    cmp-long v3, v30, v3

    if-eqz v3, :cond_d

    const-wide/16 v3, 0x0

    cmp-long v3, v27, v3

    if-eqz v3, :cond_d

    if-eqz v26, :cond_d

    .line 209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v3, v3, v27

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v36

    .line 211
    .local v36, "runTime":J
    cmp-long v3, v36, v30

    if-gez v3, :cond_d

    .line 213
    :try_start_0
    sput-object v26, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    .line 214
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->serverJson:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->getMoreAppsInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->checkedJson:Ljava/lang/String;

    .line 216
    new-instance v3, Lorg/json/JSONObject;

    move-object/from16 v0, v26

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->makeMoreAppsInfo(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v35

    .line 217
    .local v35, "resultJson":Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-interface {v0, v3, v1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    .end local v35    # "resultJson":Ljava/lang/String;
    .end local v36    # "runTime":J
    :goto_3
    return-void

    .end local v26    # "apiJson":Ljava/lang/String;
    .end local v27    # "apiTime":J
    .end local v30    # "cacheTime":J
    .end local v34    # "mccRegion":Ljava/lang/String;
    :cond_b
    move-object/from16 v34, v7

    .line 160
    goto/16 :goto_0

    .line 171
    .restart local v34    # "mccRegion":Ljava/lang/String;
    :cond_c
    sget-object v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    goto/16 :goto_1

    .line 178
    :pswitch_0
    const-string v3, "eu-openapi.samsungshealth.com"

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->apiServer:Ljava/lang/String;

    goto/16 :goto_2

    .line 181
    :pswitch_1
    const-string/jumbo v3, "openapi.samsungshealth.com"

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->apiServer:Ljava/lang/String;

    goto/16 :goto_2

    .line 184
    :pswitch_2
    const-string v3, "cn-openapi.samsungshealth.com"

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->apiServer:Ljava/lang/String;

    goto/16 :goto_2

    .line 187
    :pswitch_3
    const-string v3, "kr-openapi.samsungshealth.com"

    sput-object v3, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->apiServer:Ljava/lang/String;

    goto/16 :goto_2

    .line 221
    .restart local v26    # "apiJson":Ljava/lang/String;
    .restart local v27    # "apiTime":J
    .restart local v30    # "cacheTime":J
    .restart local v36    # "runTime":J
    :catch_0
    move-exception v3

    .line 244
    .end local v36    # "runTime":J
    :cond_d
    :goto_4
    new-instance v24, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$1;-><init>(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;)V

    .line 338
    .local v24, "responselistener":Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    new-instance v33, Ljava/util/HashMap;

    invoke-direct/range {v33 .. v33}, Ljava/util/HashMap;-><init>()V

    .line 345
    .local v33, "header":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "requestHeader"

    invoke-virtual/range {v33 .. v33}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 352
    .local v29, "body":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/moreapps/service/JsonData;>;"
    new-instance v3, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string/jumbo v6, "mcc"

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/moreapps/service/JsonData;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    new-instance v8, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string/jumbo v11, "mnc"

    const/4 v13, 0x0

    invoke-direct/range {v8 .. v13}, Lcom/sec/android/app/shealth/moreapps/service/JsonData;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    new-instance v13, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-string v16, "csc"

    const/16 v18, 0x0

    invoke-direct/range {v13 .. v18}, Lcom/sec/android/app/shealth/moreapps/service/JsonData;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    new-instance v18, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const-string/jumbo v21, "tag"

    const-string v22, "SHealth"

    const/16 v23, 0x0

    invoke-direct/range {v18 .. v23}, Lcom/sec/android/app/shealth/moreapps/service/JsonData;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    new-instance v18, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const-string v21, "deviceId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mDeviceId:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-direct/range {v18 .. v23}, Lcom/sec/android/app/shealth/moreapps/service/JsonData;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    new-instance v18, Lcom/sec/android/app/shealth/moreapps/service/JsonData;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const-string v21, "bannerYN"

    const-string v22, "Y"

    const/16 v23, 0x0

    invoke-direct/range {v18 .. v23}, Lcom/sec/android/app/shealth/moreapps/service/JsonData;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    :try_start_1
    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/shealth/moreapps/service/Util;->makeJsonBody(Ljava/util/List;)Ljava/lang/String;

    move-result-object v23

    .line 362
    .local v23, "jsonbody":Ljava/lang/String;
    const-string/jumbo v3, "requestBody"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "api:/platform/common/getMoreAppsList:jsonbody:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":listener:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    new-instance v18, Lcom/sec/android/app/shealth/moreapps/service/HttpConnector;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->apiServer:Ljava/lang/String;

    const-string v21, "/platform/common/getMoreAppsList"

    move-object/from16 v22, v33

    invoke-direct/range {v18 .. v24}, Lcom/sec/android/app/shealth/moreapps/service/HttpConnector;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_3

    .line 366
    .end local v23    # "jsonbody":Ljava/lang/String;
    :catch_1
    move-exception v32

    .line 367
    .local v32, "e":Lorg/json/JSONException;
    const-string v3, "MoreAppsServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v32 .. v32}, Lcom/sec/android/app/shealth/moreapps/service/Util;->convertErrString(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/16 v3, 0x25b

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 369
    .end local v32    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v32

    .line 370
    .local v32, "e":Lcom/sec/android/service/health/connectionmanager2/NetException;
    const-string v3, "MoreAppsServiceManager"

    const-string v4, "------------ Request NetException --------------"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    const-string v3, "MoreAppsServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HttpResCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const-string v3, "MoreAppsServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "errorCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const-string v3, "MoreAppsServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "e : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getException()Ljava/lang/Exception;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    const/16 v3, 0x25a

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;->onResponse(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 220
    .end local v24    # "responselistener":Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;
    .end local v29    # "body":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/moreapps/service/JsonData;>;"
    .end local v32    # "e":Lcom/sec/android/service/health/connectionmanager2/NetException;
    .end local v33    # "header":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v36    # "runTime":J
    :catch_3
    move-exception v3

    goto/16 :goto_4

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
