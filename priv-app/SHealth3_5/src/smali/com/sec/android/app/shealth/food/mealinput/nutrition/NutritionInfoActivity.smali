.class public Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "NutritionInfoActivity.java"


# instance fields
.field private footersSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFoodItemDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mNutritionAdapter:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->footersSet:Ljava/util/Set;

    return-void
.end method

.method private loadFoodInfo(Landroid/content/Context;J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "foodInfoId"    # J

    .prologue
    .line 157
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 158
    .local v0, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-interface {v0, p2, p3}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 162
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    return-object v1
.end method

.method private loadInfo()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "NUTRITION_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const v1, 0x7f090971

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 53
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->getApplication()Landroid/app/Application;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v9}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 55
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v10, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_VIEW_NUTRITION_INFO_BY_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v9, v10}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 56
    const v9, 0x7f0300fe

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->setContentView(I)V

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->loadInfo()Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mFoodItemDatas:Ljava/util/List;

    .line 58
    const v9, 0x7f080445

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 59
    .local v6, "list":Landroid/widget/ListView;
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 60
    .local v4, "header":Landroid/view/View;
    new-instance v9, Landroid/widget/AbsListView$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a07c6

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    invoke-direct {v9, v12, v10}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    invoke-virtual {v6, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 63
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v1, "foodInfoDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mFoodItemDatas:Ljava/util/List;

    if-eqz v9, :cond_8

    .line 69
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mFoodItemDatas:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v5, v9, :cond_4

    .line 71
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mFoodItemDatas:Ljava/util/List;

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 72
    .local v7, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v9

    invoke-direct {p0, p0, v9, v10}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->loadFoodInfo(Landroid/content/Context;J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    .line 74
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-nez v0, :cond_0

    .line 69
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v9

    const v10, 0x46cd2

    if-ne v9, v10, :cond_2

    .line 82
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->footersSet:Ljava/util/Set;

    const v10, 0x7f090974

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_1
    :goto_2
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isQuickInputType(I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 99
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mFoodItemDatas:Ljava/util/List;

    invoke-interface {v9, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 100
    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v9

    const v10, 0x46cd3

    if-ne v9, v10, :cond_1

    .line 88
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->footersSet:Ljava/util/Set;

    const v10, 0x7f090975

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 102
    :cond_3
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 114
    .end local v0    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .end local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .local v3, "footerText":Ljava/lang/StringBuilder;
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->footersSet:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 118
    .local v8, "stringIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_5
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 121
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 126
    const-string v9, "\n"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 132
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v9

    const v10, 0x7f0300fc

    invoke-virtual {v9, v10, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 134
    .local v2, "footer":Landroid/view/View;
    const v9, 0x7f080422

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {v6, v2, v13, v12}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 138
    new-instance v9, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    iget-object v10, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mFoodItemDatas:Ljava/util/List;

    invoke-direct {v9, p0, v10, v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mNutritionAdapter:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    .line 140
    if-eqz p1, :cond_7

    .line 143
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mNutritionAdapter:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    invoke-virtual {v9, p1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 147
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mNutritionAdapter:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    invoke-virtual {v6, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 150
    .end local v2    # "footer":Landroid/view/View;
    .end local v3    # "footerText":Ljava/lang/StringBuilder;
    .end local v5    # "i":I
    .end local v8    # "stringIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_8
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;->mNutritionAdapter:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionAdapter;->saveInstanceState(Landroid/os/Bundle;)V

    .line 175
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 176
    return-void
.end method
