.class Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SystemCloseDialogReceiver"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2000
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/home/HomeActivity$1;

    .prologue
    .line 2000
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/HomeActivity$SystemCloseDialogReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2005
    if-nez p2, :cond_1

    .line 2017
    :cond_0
    :goto_0
    return-void

    .line 2009
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2011
    const-string/jumbo v1, "reason"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2012
    .local v0, "reason":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "homekey"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2014
    const-wide/16 v1, 0x0

    sput-wide v1, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->prevWidgetClickTime:J

    goto :goto_0
.end method
