.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;
.super Ljava/lang/Object;
.source "WalkingMateStatisticServerTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ByAgeResponse"
.end annotation


# instance fields
.field private ageRange:I

.field private avgStep:I

.field private bestStep:I

.field private calorie:I

.field private createTime:Ljava/lang/String;

.field private distance:I

.field private jsonObject:Lorg/json/JSONObject;

.field private percentile:D

.field private ranking:I

.field private step:I

.field private totalUserNum:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "jsonString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->createTime:Ljava/lang/String;

    .line 399
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ageRange:I

    .line 400
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->step:I

    .line 401
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->distance:I

    .line 402
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->calorie:I

    .line 403
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ranking:I

    .line 404
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->percentile:D

    .line 405
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->totalUserNum:I

    .line 406
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->avgStep:I

    .line 407
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->bestStep:I

    .line 465
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "basicResponse"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->createTime:Ljava/lang/String;

    .line 399
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ageRange:I

    .line 400
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->step:I

    .line 401
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->distance:I

    .line 402
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->calorie:I

    .line 403
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ranking:I

    .line 404
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->percentile:D

    .line 405
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->totalUserNum:I

    .line 406
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->avgStep:I

    .line 407
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->bestStep:I

    .line 411
    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->checkJson(Lorg/json/JSONObject;)V
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->access$000(Lorg/json/JSONObject;)V

    .line 412
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->jsonObject:Lorg/json/JSONObject;

    .line 413
    const-string v0, "createTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->createTime:Ljava/lang/String;

    .line 414
    const-string v0, "ageRange"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ageRange"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ageRange:I

    .line 415
    :cond_0
    const-string/jumbo v0, "step"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->step:I

    .line 416
    const-string v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->distance:I

    .line 417
    const-string v0, "calorie"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->calorie:I

    .line 418
    const-string/jumbo v0, "ranking"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ranking:I

    .line 419
    const-string/jumbo v0, "percentile"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "percentile"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->percentile:D

    .line 420
    :cond_1
    const-string/jumbo v0, "totalUserNum"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->totalUserNum:I

    .line 421
    const-string v0, "avgStep"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->avgStep:I

    .line 422
    const-string v0, "bestStep"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->bestStep:I

    .line 423
    return-void
.end method


# virtual methods
.method public getAgeRange()I
    .locals 1

    .prologue
    .line 429
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ageRange:I

    return v0
.end method

.method public getAvgStep()I
    .locals 1

    .prologue
    .line 453
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->avgStep:I

    return v0
.end method

.method public getBestStep()I
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->bestStep:I

    return v0
.end method

.method public getCalorie()I
    .locals 1

    .prologue
    .line 438
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->calorie:I

    return v0
.end method

.method public getCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->createTime:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->distance:I

    return v0
.end method

.method public getJsonObject()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->jsonObject:Lorg/json/JSONObject;

    return-object v0
.end method

.method public getPercentile()D
    .locals 2

    .prologue
    .line 447
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->percentile:D

    return-wide v0
.end method

.method public getRanking()I
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->ranking:I

    return v0
.end method

.method public getStep()I
    .locals 1

    .prologue
    .line 432
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->step:I

    return v0
.end method

.method public getTotalUserNum()I
    .locals 1

    .prologue
    .line 450
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->totalUserNum:I

    return v0
.end method

.method public setCalorie(I)V
    .locals 0
    .param p1, "calorie"    # I

    .prologue
    .line 441
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ByAgeResponse;->calorie:I

    .line 442
    return-void
.end method
