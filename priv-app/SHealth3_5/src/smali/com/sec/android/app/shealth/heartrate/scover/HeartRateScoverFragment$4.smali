.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$4;
.super Ljava/lang/Object;
.source "HeartRateScoverFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->createMeasurementDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$4;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 330
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "sth  measureDialog onDismiss"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$4;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1700(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$4;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1702(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 334
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 335
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$4;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mHrmScoverFragmentHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$1800(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 337
    return-void
.end method
