.class public Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "GoalHistoryActivity.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

.field private mCurrentExpandedGroupPosition:I

.field private mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

.field mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

.field private mEmptyView:Landroid/view/View;

.field private mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

.field private mExpandableListView:Landroid/widget/ExpandableListView;

.field private mGoalDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaderView:Landroid/view/View;

.field private mMissionDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCurrentExpandedGroupPosition:I

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    .line 288
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->setAdapter()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;)Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;)Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    return-object p1
.end method

.method private collapseGroup(I)V
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 198
    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    .line 208
    :cond_0
    return-void

    .line 201
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v0

    .line 202
    .local v0, "adapterGroupCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 204
    if-ne p1, v1, :cond_2

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 202
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private expandGroup(I)V
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 212
    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    .line 223
    :cond_0
    return-void

    .line 216
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v0

    .line 217
    .local v0, "adapterGroupCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 219
    if-ne p1, v1, :cond_2

    .line 220
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 217
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getHeaderView(II)Landroid/view/View;
    .locals 4
    .param p1, "goalCompleteCount"    # I
    .param p2, "missionCompleteCount"    # I

    .prologue
    const v3, 0x7f0700e3

    .line 116
    const v0, 0x7f030051

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mHeaderView:Landroid/view/View;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mHeaderView:Landroid/view/View;

    const v1, 0x7f0801a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090307

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->setLeftTitleText(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090308

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->setRightTitleText(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->setLeftCountTextColor(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->setRightCountTextColor(I)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->setLeftCountText(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCustomTwoInfoView:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->setRightCountText(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mHeaderView:Landroid/view/View;

    return-object v0
.end method

.method private getListData()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 91
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getHistoryListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    .line 92
    return-void

    .line 82
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;

    goto :goto_0

    .line 88
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;

    goto :goto_1
.end method

.method private setAdapter()V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mHeaderView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->removeHeaderView(Landroid/view/View;)Z

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->getGoalCompeleteCount()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCompleteGoalMissionInfoData:Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteGoalMissionInfoData;->getMissionCompleteCount()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getHeaderView(II)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->addHeaderView(Landroid/view/View;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 98
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mGoalDataList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mMissionDataList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->HISTORY:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setMainType(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 101
    return-void
.end method

.method private updateList()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getListData()V

    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->setAdapter()V

    .line 76
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 131
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c5e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 135
    return-void
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 4
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 140
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v2, p3, p4}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 142
    .local v1, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    if-eqz v1, :cond_0

    .line 143
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 145
    const-string v2, "EXTRA_NAME_FROM_HISTORY"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    const-string v2, "EXTRA_NAME_SELECTED_MISSION_DATA"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 147
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->startActivity(Landroid/content/Intent;)V

    .line 150
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 248
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->invalidateOptionsMenu()V

    .line 250
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v1, 0x7f030050

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->setContentView(I)V

    .line 60
    const v1, 0x7f0801a2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mEmptyView:Landroid/view/View;

    .line 61
    const v1, 0x7f0801a1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ExpandableListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mEmptyView:Landroid/view/View;

    const v2, 0x7f080219

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 67
    .local v0, "noHistoryTextView":Landroid/widget/TextView;
    const v1, 0x7f09032b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->updateList()V

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 71
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 230
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity$OnGoalMissionStatusChangedTask;->cancel(Z)Z

    .line 112
    :cond_0
    return-void
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 2
    .param p1, "elv"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v0, 0x0

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v1, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 177
    :goto_0
    return v0

    .line 159
    :cond_0
    invoke-virtual {p1, v0}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    .line 160
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCurrentExpandedGroupPosition:I

    if-ne v0, p3, :cond_2

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p3}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->collapseGroup(I)V

    .line 177
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 168
    :cond_1
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->expandGroup(I)V

    goto :goto_1

    .line 173
    :cond_2
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->expandGroup(I)V

    goto :goto_1
.end method

.method public onGroupExpand(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCurrentExpandedGroupPosition:I

    if-eq v0, p1, :cond_1

    .line 185
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCurrentExpandedGroupPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 187
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCurrentExpandedGroupPosition:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->collapseGroup(I)V

    .line 190
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mCurrentExpandedGroupPosition:I

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setSelection(I)V

    .line 194
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 236
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080c90

    if-ne v0, v1, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->prepareShareView()V

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/GoalHistoryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cignacoach"

    const-string v2, "0029"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 241
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachMoreUtils;->showMoreForContextMenu(Landroid/content/Context;Landroid/view/MenuItem;)V

    goto :goto_0
.end method
