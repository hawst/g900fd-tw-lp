.class public final enum Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
.super Ljava/lang/Enum;
.source "FeatureTable.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/config/FeatureTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Weight"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;",
        ">;",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

.field public static final enum NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

.field public static final enum NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

.field public static final enum SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    const-string v1, "SUPPORTED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .line 80
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    const-string v1, "NOT_MEDICAL_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .line 81
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    const-string v1, "NOT_SUPPORTED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    const-class v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    return-object v0
.end method
