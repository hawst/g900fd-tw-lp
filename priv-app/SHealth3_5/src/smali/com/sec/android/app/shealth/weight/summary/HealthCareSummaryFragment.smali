.class public abstract Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "HealthCareSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;,
        Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$DeviceConnChkListner;
    }
.end annotation


# static fields
.field public static final RESET_DIALOG_TAG:Ljava/lang/String; = "RESET_DIALOG_TAG"


# instance fields
.field private mNeedUpdate:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->mNeedUpdate:Z

    .line 285
    return-void
.end method

.method private setNextDate(JJ)V
    .locals 4
    .param p1, "startOfCurrentDay"    # J
    .param p3, "startOfSelectedDate"    # J

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;

    move-result-object v2

    invoke-interface {v2, p3, p4}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;->getNextDayWithData(J)Ljava/lang/Long;

    move-result-object v1

    .line 215
    .local v1, "nextTime":Ljava/lang/Long;
    if-eqz v1, :cond_2

    .line 216
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 217
    cmp-long v2, p3, p1

    if-gez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 218
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 223
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p3, p4}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v0, 0x0

    .line 224
    .local v0, "nextDate":Ljava/util/Date;
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v3

    if-eqz v0, :cond_4

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 226
    return-void

    .line 220
    .end local v0    # "nextDate":Ljava/util/Date;
    :cond_2
    cmp-long v2, p3, p1

    if-gez v2, :cond_0

    .line 221
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 223
    :cond_3
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_1

    .line 225
    .restart local v0    # "nextDate":Ljava/util/Date;
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private setPrevDate(JJ)V
    .locals 4
    .param p1, "startOfCurrentDay"    # J
    .param p3, "startOfSelectedDate"    # J

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;

    move-result-object v2

    invoke-interface {v2, p3, p4}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;->getPreviousDayWithData(J)Ljava/lang/Long;

    move-result-object v0

    .line 200
    .local v0, "prevTime":Ljava/lang/Long;
    if-eqz v0, :cond_1

    .line 201
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 202
    cmp-long v2, p3, p1

    if-lez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-lez v2, :cond_0

    .line 203
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 208
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    const/4 v1, 0x0

    .line 209
    .local v1, "previousDate":Ljava/util/Date;
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevDate(Ljava/util/Date;)V

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v3

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    .line 211
    return-void

    .line 205
    .end local v1    # "previousDate":Ljava/util/Date;
    :cond_1
    cmp-long v2, p3, p1

    if-lez v2, :cond_0

    .line 206
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 208
    :cond_2
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_1

    .line 210
    .restart local v1    # "previousDate":Ljava/util/Date;
    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method protected abstract clearMainView()V
.end method

.method protected deleteAllDataForDay()V
    .locals 3

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;->deleteAllDataForDay(J)I

    .line 238
    return-void
.end method

.method protected abstract getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
.end method

.method protected abstract getDataForInputActivity(Ljava/util/Date;)Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;
.end method

.method protected getDeviceConnectionCheckListener(Landroid/view/View;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    .locals 1
    .param p1, "deviceConnectedStatusView"    # Landroid/view/View;

    .prologue
    .line 244
    new-instance v0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$DeviceConnChkListner;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$DeviceConnChkListner;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 278
    const-string v0, "RESET_DIALOG_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    new-instance v0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$ResetDialogButtonController;-><init>(Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$1;)V

    .line 281
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method protected abstract getInputActivityClass()Ljava/lang/Class;
.end method

.method protected abstract isDataForDateExist(Ljava/util/Date;)Z
.end method

.method protected launchInputActivity(Ljava/util/Date;)V
    .locals 4
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDataForInputActivity(Ljava/util/Date;)Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    move-result-object v0

    .line 303
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getInputActivityClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 304
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "INPUT_ACTIVITY_MODE_KEY"

    const-string v3, "Add"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const-string v2, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 306
    const/16 v2, 0x2711

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 307
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 143
    const/16 v0, 0x2711

    if-ne p1, v0, :cond_0

    .line 144
    if-eqz p3, :cond_0

    const-string v0, "INPUT_ACTIVITY_WAS_CHANGED"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    new-instance v0, Ljava/util/Date;

    const-string v1, "TIME"

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p3, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 148
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 149
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 119
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->updateMainView(Z)V

    .line 120
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreate(Landroid/os/Bundle;)V

    .line 99
    if-eqz p1, :cond_0

    const-string v0, "TIME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "TIME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 102
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setHasOptionsMenu(Z)V

    .line 103
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 125
    const v0, 0x7f100027

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 126
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 127
    return-void
.end method

.method public onDateChanged(Ljava/util/Date;)V
    .locals 2
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->isDataForDateExist(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isDateToday(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->mNeedUpdate:Z

    .line 171
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->launchInputActivity(Ljava/util/Date;)V

    .line 188
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 155
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 156
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 133
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 109
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->mNeedUpdate:Z

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->updateMainView(Z)V

    .line 112
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->mNeedUpdate:Z

    .line 113
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    const-string v0, "TIME"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 93
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 94
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 4

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 160
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 162
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->updateMainView(Z)V

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 165
    return-void
.end method

.method protected setNextAndPrevDates()V
    .locals 6

    .prologue
    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    .line 193
    .local v0, "startOfCurrentDay":J
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 194
    .local v2, "startOfSelectedDate":J
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setPrevDate(JJ)V

    .line 195
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setNextDate(JJ)V

    .line 196
    return-void
.end method

.method protected abstract updateMainView(Z)V
.end method
