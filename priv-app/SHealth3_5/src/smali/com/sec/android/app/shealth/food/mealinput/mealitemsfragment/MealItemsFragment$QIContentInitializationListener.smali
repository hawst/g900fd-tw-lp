.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;
.super Ljava/lang/Object;
.source "MealItemsFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "QIContentInitializationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V
    .locals 0

    .prologue
    .line 858
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 866
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    const v1, 0x7f080493

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    iput-object v1, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    .line 868
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-static {}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getCalories()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->setCalories([I)V

    .line 869
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    const v1, 0x7f080492

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1202(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 870
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealTypeSelector:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 874
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealType:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->updateCalorieValues(I)V

    .line 875
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    .line 876
    .local v0, "cal":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQIServerSourceType:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1500(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealType:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->selectIcon(III)V

    .line 879
    return-void

    .line 875
    .end local v0    # "cal":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
