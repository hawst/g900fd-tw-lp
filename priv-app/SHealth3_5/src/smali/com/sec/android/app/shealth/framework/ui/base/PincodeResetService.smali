.class public Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;
.super Landroid/app/Service;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$1;,
        Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;
    }
.end annotation


# static fields
.field private static HOME_KEY:Ljava/lang/String;

.field private static RECENT_KEY:Ljava/lang/String;

.field public static isPasswordServiceReceived:Z


# instance fields
.field private mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v4, 0x7

    const/16 v5, 0x8

    const/16 v6, 0x9

    const/16 v7, -0x7c

    aput v7, v1, v6

    const/16 v6, -0x7c9f

    aput v6, v1, v5

    const/16 v5, -0xd

    aput v5, v1, v4

    const/16 v4, -0x1e

    aput v4, v1, v2

    const/16 v2, -0x14

    aput v2, v1, v0

    const/16 v0, -0x25eb

    aput v0, v1, v12

    const/16 v0, -0x41

    aput v0, v1, v11

    const/16 v0, -0x3ba3

    aput v0, v1, v10

    const/16 v0, -0x5f

    aput v0, v1, v9

    const/16 v0, -0x30

    aput v0, v1, v3

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/16 v7, 0x9

    const/16 v8, -0x9

    aput v8, v0, v7

    const/16 v7, -0x7cef

    aput v7, v0, v6

    const/16 v6, -0x7d

    aput v6, v0, v5

    const/16 v5, -0x7d

    aput v5, v0, v4

    const/16 v4, -0x68

    aput v4, v0, v2

    const/16 v2, -0x2585

    aput v2, v0, v12

    const/16 v2, -0x26

    aput v2, v0, v11

    const/16 v2, -0x3bc2

    aput v2, v0, v10

    const/16 v2, -0x3c

    aput v2, v0, v9

    const/16 v2, -0x5e

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->RECENT_KEY:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/16 v4, -0x5a

    aput v4, v1, v2

    const/16 v2, -0x5b7

    aput v2, v1, v0

    const/16 v0, -0x6f

    aput v0, v1, v12

    const/16 v0, -0x44da

    aput v0, v1, v11

    const/16 v0, -0x2a

    aput v0, v1, v10

    const/16 v0, -0x22

    aput v0, v1, v9

    const/16 v0, 0x581a

    aput v0, v1, v3

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/16 v5, -0x21

    aput v5, v0, v4

    const/16 v4, -0x5d4

    aput v4, v0, v2

    const/4 v2, -0x6

    aput v2, v0, v12

    const/16 v2, -0x44bd

    aput v2, v0, v11

    const/16 v2, -0x45

    aput v2, v0, v10

    const/16 v2, -0x4f

    aput v2, v0, v9

    const/16 v2, 0x5872

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    move v2, v3

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->HOME_KEY:Ljava/lang/String;

    sput-boolean v3, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->isPasswordServiceReceived:Z

    return-void

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->registerHomeKeyPress()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->unRegisterHomeKeyPress()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 25

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x2

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p2

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x226b7e01eba84b84L    # -6.251967037010116E142

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x226b7e01eba84b84L    # -6.251967037010116E142

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    const/4 v1, 0x1

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-lt v1, v2, :cond_2

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const/4 v4, 0x0

    move/from16 v0, p3

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_3

    const-wide v7, -0x226b7e01eba84b84L    # -6.251967037010116E142

    xor-long/2addr v1, v7

    :cond_3
    const/16 v7, 0x20

    shl-long/2addr v1, v7

    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x226b7e01eba84b84L    # -6.251967037010116E142

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, -0x7dfc

    aput v23, v2, v22

    const/16 v22, -0x19

    aput v22, v2, v21

    const/16 v21, -0x50

    aput v21, v2, v20

    const/16 v20, 0x82f

    aput v20, v2, v19

    const/16 v19, -0x6997

    aput v19, v2, v18

    const/16 v18, -0x8

    aput v18, v2, v17

    const/16 v17, -0x1b

    aput v17, v2, v16

    const/16 v16, -0x28

    aput v16, v2, v15

    const/16 v15, -0x20

    aput v15, v2, v14

    const/16 v14, 0x7365

    aput v14, v2, v13

    const/16 v13, -0x47fd

    aput v13, v2, v12

    const/16 v12, -0x19

    aput v12, v2, v11

    const/16 v11, -0x10d9

    aput v11, v2, v10

    const/16 v10, -0x65

    aput v10, v2, v9

    const/16 v9, -0x42

    aput v9, v2, v8

    const/16 v8, 0x5a3c

    aput v8, v2, v7

    const/16 v7, 0x4a2f

    aput v7, v2, v6

    const/16 v6, 0x6529

    aput v6, v2, v5

    const/16 v5, -0x4300

    aput v5, v2, v3

    const/16 v3, -0x32

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x7da0

    aput v24, v1, v23

    const/16 v23, -0x7e

    aput v23, v1, v22

    const/16 v22, -0x24

    aput v22, v1, v21

    const/16 v21, 0x84d

    aput v21, v1, v20

    const/16 v20, -0x69f8

    aput v20, v1, v19

    const/16 v19, -0x6a

    aput v19, v1, v18

    const/16 v18, -0x80

    aput v18, v1, v17

    const/16 v17, -0x79

    aput v17, v1, v16

    const/16 v16, -0x72

    aput v16, v1, v15

    const/16 v15, 0x730c

    aput v15, v1, v14

    const/16 v14, -0x478d

    aput v14, v1, v13

    const/16 v13, -0x48

    aput v13, v1, v12

    const/16 v12, -0x10a2

    aput v12, v1, v11

    const/16 v11, -0x11

    aput v11, v1, v10

    const/16 v10, -0x29

    aput v10, v1, v9

    const/16 v9, 0x5a4e

    aput v9, v1, v8

    const/16 v8, 0x4a5a

    aput v8, v1, v7

    const/16 v7, 0x654a

    aput v7, v1, v6

    const/16 v6, -0x429b

    aput v6, v1, v5

    const/16 v5, -0x43

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x5683

    aput v19, v2, v18

    const/16 v18, -0x25

    aput v18, v2, v17

    const/16 v17, -0x68

    aput v17, v2, v16

    const/16 v16, -0x14

    aput v16, v2, v15

    const/4 v15, -0x5

    aput v15, v2, v14

    const/16 v14, -0x29

    aput v14, v2, v13

    const/16 v13, 0x2126

    aput v13, v2, v12

    const/16 v12, -0x32af

    aput v12, v2, v11

    const/16 v11, -0x6e

    aput v11, v2, v10

    const/16 v10, -0x15

    aput v10, v2, v9

    const/16 v9, 0x4832

    aput v9, v2, v8

    const/16 v8, -0x50d3

    aput v8, v2, v7

    const/16 v7, -0x26

    aput v7, v2, v6

    const/16 v6, -0x7c

    aput v6, v2, v5

    const/16 v5, -0x12f7

    aput v5, v2, v3

    const/16 v3, -0x61

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x56e7

    aput v20, v1, v19

    const/16 v19, -0x57

    aput v19, v1, v18

    const/16 v18, -0x9

    aput v18, v1, v17

    const/16 v17, -0x65

    aput v17, v1, v16

    const/16 v16, -0x78

    aput v16, v1, v15

    const/16 v15, -0x5c

    aput v15, v1, v14

    const/16 v14, 0x2147

    aput v14, v1, v13

    const/16 v13, -0x32df

    aput v13, v1, v12

    const/16 v12, -0x33

    aput v12, v1, v11

    const/16 v11, -0x61

    aput v11, v1, v10

    const/16 v10, 0x4841

    aput v10, v1, v9

    const/16 v9, -0x50b8

    aput v9, v1, v8

    const/16 v8, -0x51

    aput v8, v1, v7

    const/16 v7, -0xb

    aput v7, v1, v6

    const/16 v6, -0x1294

    aput v6, v1, v5

    const/16 v5, -0x13

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4
.end method

.method public registerHomeKeyPress()V
    .locals 47

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$1;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const/16 v1, 0x2a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, -0x38

    aput v45, v2, v44

    const/16 v44, 0x6920

    aput v44, v2, v43

    const/16 v43, 0x1c26

    aput v43, v2, v42

    const/16 v42, 0x2350

    aput v42, v2, v41

    const/16 v41, -0x1b9e

    aput v41, v2, v40

    const/16 v40, -0x53

    aput v40, v2, v39

    const/16 v39, -0x76

    aput v39, v2, v38

    const/16 v38, -0x42ff

    aput v38, v2, v37

    const/16 v37, -0x10

    aput v37, v2, v36

    const/16 v36, -0x2ce4

    aput v36, v2, v35

    const/16 v35, -0x79

    aput v35, v2, v34

    const/16 v34, -0x3796

    aput v34, v2, v33

    const/16 v33, -0x6f

    aput v33, v2, v32

    const/16 v32, 0x6d6f

    aput v32, v2, v31

    const/16 v31, 0x2432

    aput v31, v2, v30

    const/16 v30, -0x169f

    aput v30, v2, v29

    const/16 v29, -0x46

    aput v29, v2, v28

    const/16 v28, -0x33

    aput v28, v2, v27

    const/16 v27, 0x6622

    aput v27, v2, v26

    const/16 v26, 0x5325

    aput v26, v2, v25

    const/16 v25, -0x3183

    aput v25, v2, v24

    const/16 v24, -0x60

    aput v24, v2, v23

    const/16 v23, -0x65

    aput v23, v2, v22

    const/16 v22, -0x5afe

    aput v22, v2, v21

    const/16 v21, -0x2f

    aput v21, v2, v20

    const/16 v20, -0x18

    aput v20, v2, v19

    const/16 v19, 0x516

    aput v19, v2, v18

    const/16 v18, -0x72d5

    aput v18, v2, v17

    const/16 v17, -0x7

    aput v17, v2, v16

    const/16 v16, -0x37

    aput v16, v2, v15

    const/16 v15, -0x1de2

    aput v15, v2, v14

    const/16 v14, -0x6a

    aput v14, v2, v13

    const/4 v13, -0x5

    aput v13, v2, v12

    const/16 v12, -0x53

    aput v12, v2, v11

    const/16 v11, -0x65e9

    aput v11, v2, v10

    const/4 v10, -0x2

    aput v10, v2, v9

    const/16 v9, -0x53

    aput v9, v2, v8

    const/16 v8, -0x6f84

    aput v8, v2, v7

    const/16 v7, -0x1e

    aput v7, v2, v6

    const/16 v6, 0x3779

    aput v6, v2, v5

    const/16 v5, -0x30a7

    aput v5, v2, v3

    const/16 v3, -0x52

    aput v3, v2, v1

    const/16 v1, 0x2a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, -0x65

    aput v46, v1, v45

    const/16 v45, 0x6967

    aput v45, v1, v44

    const/16 v44, 0x1c69

    aput v44, v1, v43

    const/16 v43, 0x231c

    aput v43, v1, v42

    const/16 v42, -0x1bdd

    aput v42, v1, v41

    const/16 v41, -0x1c

    aput v41, v1, v40

    const/16 v40, -0x32

    aput v40, v1, v39

    const/16 v39, -0x42a2

    aput v39, v1, v38

    const/16 v38, -0x43

    aput v38, v1, v37

    const/16 v37, -0x2ca7

    aput v37, v1, v36

    const/16 v36, -0x2d

    aput v36, v1, v35

    const/16 v35, -0x37c7

    aput v35, v1, v34

    const/16 v34, -0x38

    aput v34, v1, v33

    const/16 v33, 0x6d3c

    aput v33, v1, v32

    const/16 v32, 0x246d

    aput v32, v1, v31

    const/16 v31, -0x16dc

    aput v31, v1, v30

    const/16 v30, -0x17

    aput v30, v1, v29

    const/16 v29, -0x7e

    aput v29, v1, v28

    const/16 v28, 0x666e

    aput v28, v1, v27

    const/16 v27, 0x5366

    aput v27, v1, v26

    const/16 v26, -0x31ad

    aput v26, v1, v25

    const/16 v25, -0x32

    aput v25, v1, v24

    const/16 v24, -0xc

    aput v24, v1, v23

    const/16 v23, -0x5a95

    aput v23, v1, v22

    const/16 v22, -0x5b

    aput v22, v1, v21

    const/16 v21, -0x75

    aput v21, v1, v20

    const/16 v20, 0x577

    aput v20, v1, v19

    const/16 v19, -0x72fb

    aput v19, v1, v18

    const/16 v18, -0x73

    aput v18, v1, v17

    const/16 v17, -0x59

    aput v17, v1, v16

    const/16 v16, -0x1d85

    aput v16, v1, v15

    const/16 v15, -0x1e

    aput v15, v1, v14

    const/16 v14, -0x6b

    aput v14, v1, v13

    const/16 v13, -0x3c

    aput v13, v1, v12

    const/16 v12, -0x65c7

    aput v12, v1, v11

    const/16 v11, -0x66

    aput v11, v1, v10

    const/16 v10, -0x3c

    aput v10, v1, v9

    const/16 v9, -0x6fed

    aput v9, v1, v8

    const/16 v8, -0x70

    aput v8, v1, v7

    const/16 v7, 0x371d

    aput v7, v1, v6

    const/16 v6, -0x30c9

    aput v6, v1, v5

    const/16 v5, -0x31

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x22

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x2e45

    aput v37, v2, v36

    const/16 v36, 0x1c60

    aput v36, v2, v35

    const/16 v35, 0x1b59

    aput v35, v2, v34

    const/16 v34, 0x5148

    aput v34, v2, v33

    const/16 v33, 0x3814

    aput v33, v2, v32

    const/16 v32, 0x1e6a

    aput v32, v2, v31

    const/16 v31, -0x6b2

    aput v31, v2, v30

    const/16 v30, -0x5a

    aput v30, v2, v29

    const/16 v29, -0x71a6

    aput v29, v2, v28

    const/16 v28, -0x35

    aput v28, v2, v27

    const/16 v27, -0x29

    aput v27, v2, v26

    const/16 v26, -0x5b

    aput v26, v2, v25

    const/16 v25, 0x603c

    aput v25, v2, v24

    const/16 v24, 0x580e

    aput v24, v2, v23

    const/16 v23, 0x7f37

    aput v23, v2, v22

    const/16 v22, -0x5ea

    aput v22, v2, v21

    const/16 v21, -0x72

    aput v21, v2, v20

    const/16 v20, -0x788d

    aput v20, v2, v19

    const/16 v19, -0x1a

    aput v19, v2, v18

    const/16 v18, -0x8

    aput v18, v2, v17

    const/16 v17, -0x74

    aput v17, v2, v16

    const/16 v16, -0x63d3

    aput v16, v2, v15

    const/4 v15, -0x7

    aput v15, v2, v14

    const/16 v14, -0x59

    aput v14, v2, v13

    const/16 v13, -0x12

    aput v13, v2, v12

    const/4 v12, -0x5

    aput v12, v2, v11

    const/16 v11, -0x5487

    aput v11, v2, v10

    const/16 v10, -0x31

    aput v10, v2, v9

    const/16 v9, -0x7b

    aput v9, v2, v8

    const/16 v8, -0x6a

    aput v8, v2, v7

    const/16 v7, -0x66a9

    aput v7, v2, v6

    const/4 v6, -0x3

    aput v6, v2, v5

    const/16 v5, -0x64c3

    aput v5, v2, v3

    const/4 v3, -0x6

    aput v3, v2, v1

    const/16 v1, 0x22

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x2e11

    aput v38, v1, v37

    const/16 v37, 0x1c2e

    aput v37, v1, v36

    const/16 v36, 0x1b1c

    aput v36, v1, v35

    const/16 v35, 0x511b

    aput v35, v1, v34

    const/16 v34, 0x3851

    aput v34, v1, v33

    const/16 v33, 0x1e38

    aput v33, v1, v32

    const/16 v32, -0x6e2

    aput v32, v1, v31

    const/16 v31, -0x7

    aput v31, v1, v30

    const/16 v30, -0x71f8

    aput v30, v1, v29

    const/16 v29, -0x72

    aput v29, v1, v28

    const/16 v28, -0x7c

    aput v28, v1, v27

    const/16 v27, -0x10

    aput v27, v1, v26

    const/16 v26, 0x6012

    aput v26, v1, v25

    const/16 v25, 0x5860

    aput v25, v1, v24

    const/16 v24, 0x7f58

    aput v24, v1, v23

    const/16 v23, -0x581

    aput v23, v1, v22

    const/16 v22, -0x6

    aput v22, v1, v21

    const/16 v21, -0x78f0

    aput v21, v1, v20

    const/16 v20, -0x79

    aput v20, v1, v19

    const/16 v19, -0x2a

    aput v19, v1, v18

    const/16 v18, -0x8

    aput v18, v1, v17

    const/16 v17, -0x63bd

    aput v17, v1, v16

    const/16 v16, -0x64

    aput v16, v1, v15

    const/16 v15, -0x2d

    aput v15, v1, v14

    const/16 v14, -0x80

    aput v14, v1, v13

    const/16 v13, -0x6e

    aput v13, v1, v12

    const/16 v12, -0x54a9

    aput v12, v1, v11

    const/16 v11, -0x55

    aput v11, v1, v10

    const/16 v10, -0x14

    aput v10, v1, v9

    const/4 v9, -0x7

    aput v9, v1, v8

    const/16 v8, -0x66db

    aput v8, v1, v7

    const/16 v7, -0x67

    aput v7, v1, v6

    const/16 v6, -0x64ad

    aput v6, v1, v5

    const/16 v5, -0x65

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public unRegisterHomeKeyPress()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService$CloseSystemDialogsIntentReceiver;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
