.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$9;
.super Ljava/lang/Object;
.source "MealItemsFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$9;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 587
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$9;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$9;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeleteMealItemPosition:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->deleteMealItemAt(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;I)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$9;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DELETE_FOOD_BY_CONFIRM_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 591
    :cond_0
    return-void
.end method
