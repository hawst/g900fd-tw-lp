.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v7, 0x0

    .line 298
    const/4 v3, 0x6

    if-ne p2, v3, :cond_1

    .line 299
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->hideEdittextBackground()V

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float v2, v3, v4

    .line 301
    .local v2, "value":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_0

    .line 302
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    float-to-int v4, v2

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setIncDecEnabled(IZ)V

    .line 304
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->actualMinValue:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gez v3, :cond_1

    .line 306
    const/4 v0, 0x0

    .line 307
    .local v0, "goalValue":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->type:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 309
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v3

    const-string v4, "km"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 311
    .local v1, "isUnitKm":Z
    if-eqz v1, :cond_2

    .line 312
    const/16 v0, 0x64

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    int-to-long v5, v0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getKmFromMeterByLocale(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    .line 328
    .end local v0    # "goalValue":I
    .end local v1    # "isUnitKm":Z
    .end local v2    # "value":F
    :cond_1
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 315
    .restart local v0    # "goalValue":I
    .restart local v1    # "isUnitKm":Z
    .restart local v2    # "value":F
    :cond_2
    const/16 v0, 0xa1

    .line 316
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    int-to-long v5, v0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getMilesFromMetersByLocale(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    goto :goto_0

    .line 318
    .end local v1    # "isUnitKm":Z
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->type:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 320
    const/16 v0, 0xa

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    int-to-float v4, v0

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    goto :goto_0
.end method
