.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private skipUpdate:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V
    .locals 1

    .prologue
    .line 571
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->skipUpdate:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 21
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 593
    .local v8, "changedString":Ljava/lang/String;
    const/4 v13, 0x0

    .line 594
    .local v13, "isZero":Z
    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 695
    :goto_0
    return-void

    .line 600
    :cond_0
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 601
    const-string v17, ","

    const-string v18, "."

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 602
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 603
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 607
    :cond_1
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 608
    const-string v17, ","

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 610
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 613
    :cond_2
    const-string v17, "."

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 614
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "0"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 620
    :cond_3
    const/4 v9, 0x0

    .line 621
    .local v9, "dotCount":I
    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .local v4, "arr$":[C
    array-length v14, v4

    .local v14, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_1
    if-ge v12, v14, :cond_5

    aget-char v5, v4, v12

    .line 622
    .local v5, "ch":C
    const/16 v17, 0x2e

    move/from16 v0, v17

    if-ne v5, v0, :cond_4

    .line 623
    add-int/lit8 v9, v9, 0x1

    .line 621
    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 625
    .end local v5    # "ch":C
    :cond_5
    const/16 v17, 0x1

    move/from16 v0, v17

    if-le v9, v0, :cond_6

    .line 626
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 630
    :cond_6
    const-string v16, ""

    .line 631
    .local v16, "stringAfterDec":Ljava/lang/String;
    const-string v17, "."

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 632
    const-string v17, "."

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    .line 633
    :cond_7
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_9

    .line 634
    const/16 v17, 0x0

    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    add-int/lit8 v18, v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 635
    .local v11, "finalString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 691
    .end local v11    # "finalString":Ljava/lang/String;
    :goto_2
    if-nez v13, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 692
    invoke-interface/range {p1 .. p1}, Landroid/text/Editable;->length()I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 694
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    goto/16 :goto_0

    .line 638
    :cond_9
    const/4 v6, 0x0

    .line 640
    .local v6, "changedNumber":I
    if-eqz v8, :cond_f

    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_f

    .line 642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "."

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 652
    :cond_a
    :goto_3
    :try_start_0
    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    const/high16 v18, 0x41200000    # 10.0f

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v6, v0

    .line 657
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    move/from16 v0, v17

    if-ne v6, v0, :cond_d

    .line 658
    if-nez v13, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 659
    invoke-interface/range {p1 .. p1}, Landroid/text/Editable;->length()I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 661
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I
    invoke-static {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1502(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;I)I

    .line 662
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->skipUpdate:Z

    move/from16 v18, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setIncDecEnabled(IZ)V

    goto/16 :goto_0

    .line 645
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isNumeric(Ljava/lang/String;)Z
    invoke-static {v0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->backupString:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_a

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->backupString:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 653
    :catch_0
    move-exception v10

    .line 654
    .local v10, "e":Ljava/lang/NumberFormatException;
    const-class v17, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "Input format is incorrect! Not a double"

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 664
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    move/from16 v0, v17

    if-le v6, v0, :cond_11

    .line 665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v17

    if-eqz v17, :cond_10

    const/16 v17, 0x1

    :goto_5
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;IZZ)V

    .line 667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMaxInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v6

    .line 685
    :cond_e
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mCurrentValue:I
    invoke-static {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1502(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;I)I

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->skipUpdate:Z

    move/from16 v18, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setIncDecEnabled(IZ)V

    .line 688
    :cond_f
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->skipUpdate:Z

    goto/16 :goto_2

    .line 666
    :cond_10
    const/16 v17, 0x0

    goto :goto_5

    .line 668
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_e

    .line 669
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    rem-int/lit8 v17, v17, 0xa

    if-nez v17, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    :goto_7
    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v15

    .line 671
    .local v15, "minValueLength":I
    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v7

    .line 672
    .local v7, "changedNumberLength":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->filterText:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v17

    if-nez v17, :cond_e

    .line 673
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    div-int/lit8 v17, v17, 0xa

    div-int/lit8 v18, v6, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_14

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_13

    if-ge v7, v15, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_17

    if-lt v7, v15, :cond_17

    .line 677
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->needCursorLastPosition:Z
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Z)Z

    .line 678
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isDecimalNumber()Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Z

    move-result v17

    if-eqz v17, :cond_16

    const/16 v17, 0x1

    :goto_8
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNewValue(IZZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;IZZ)V

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v6

    goto/16 :goto_6

    .line 669
    .end local v7    # "changedNumberLength":I
    .end local v15    # "minValueLength":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mMinInputRange:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x41200000    # 10.0f

    div-float v17, v17, v18

    goto/16 :goto_7

    .line 678
    .restart local v7    # "changedNumberLength":I
    .restart local v15    # "minValueLength":I
    :cond_16
    const/16 v17, 0x0

    goto :goto_8

    .line 681
    :cond_17
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->skipUpdate:Z

    goto/16 :goto_6
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 584
    if-eqz p1, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->backupString:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1302(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;Ljava/lang/String;)Ljava/lang/String;

    .line 588
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 577
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mTextChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->mTextChangedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$OnTextChangedListener;->onTextChanged(Ljava/lang/String;)V

    .line 580
    :cond_0
    return-void
.end method
