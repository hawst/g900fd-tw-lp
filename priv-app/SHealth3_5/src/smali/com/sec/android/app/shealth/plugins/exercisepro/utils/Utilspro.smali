.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;
.super Ljava/lang/Object;
.source "Utilspro.java"


# static fields
.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_GROUP_DATE_SEPARATOR:Ljava/lang/String; = " "

.field private static final MONTH_GROUP_MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MMM"

.field private static final MONTH_GROUP_YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"

.field private static final dataTextViewIDs:[I

.field private static mDialog:Landroid/app/Dialog;

.field private static mMapMode:I

.field private static mPlayServicePopupDisplayed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->dataTextViewIDs:[I

    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mPlayServicePopupDisplayed:Z

    return-void

    .line 63
    :array_0
    .array-data 4
        0x7f0806f7
        0x7f0806f9
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static DialogViewModeSetting(Landroid/app/Activity;IZZ)V
    .locals 6
    .param p0, "mActivity"    # Landroid/app/Activity;
    .param p1, "iMapMode"    # I
    .param p2, "isAMap"    # Z
    .param p3, "hasMode"    # Z

    .prologue
    .line 300
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v4, 0x7f090a97

    const/16 v5, 0xc

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 303
    .local v0, "dialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro$1;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro$1;-><init>(Landroid/app/Activity;Z)V

    .line 314
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 315
    const/4 v4, 0x3

    if-le p1, v4, :cond_0

    .line 316
    const/4 p1, 0x3

    .line 317
    :cond_0
    const/4 v4, 0x1

    if-ge p1, v4, :cond_1

    .line 318
    const/4 p1, 0x1

    .line 319
    :cond_1
    add-int/lit8 v4, p1, -0x1

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->buildDialogBooleanArrayState(I)[Z

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 321
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mMapMode:I

    .line 323
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v2

    .line 324
    .local v2, "listChooseDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    if-eqz p3, :cond_2

    const-string/jumbo v3, "map_mode"

    .line 325
    .local v3, "mapMode":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 326
    return-void

    .line 324
    .end local v3    # "mapMode":Ljava/lang/String;
    :cond_2
    const-string v3, ""

    goto :goto_0
.end method

.method private static buildDialogBooleanArrayState(I)[Z
    .locals 2
    .param p0, "filterDialogState"    # I

    .prologue
    .line 333
    const/4 v1, 0x3

    new-array v0, v1, [Z

    .line 334
    .local v0, "filterDialogBooleanArrayState":[Z
    const/4 v1, 0x1

    aput-boolean v1, v0, p0

    .line 335
    return-object v0
.end method

.method public static getActionBarTitle(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)I
    .locals 10
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "mExercise"    # Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .prologue
    .line 603
    const/4 v9, 0x0

    .line 604
    .local v9, "resId":I
    if-eqz p1, :cond_4

    .line 606
    const/4 v7, 0x0

    .line 608
    .local v7, "cursor":Landroid/database/Cursor;
    const-string v6, ""

    .line 609
    .local v6, "category":Ljava/lang/String;
    const-string v8, ""

    .line 612
    .local v8, "exercise":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "select ca._id, ca.name  ca_Name, ex._id, ex.name ex_Name from exercise_info ex left outer join exercise_info ca on ca._id = ex.exercise_info__id  where ex._id ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getExerciseId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 618
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    const-string v0, "ca_Name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 620
    const-string v0, "ex_Name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 621
    const-string v0, "Utilspro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "name_categoty = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "name_exercise ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 624
    :cond_0
    if-eqz v7, :cond_1

    .line 625
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 627
    :cond_1
    if-nez v6, :cond_2

    if-eqz v8, :cond_2

    .line 628
    move-object v6, v8

    .line 630
    :cond_2
    if-eqz v6, :cond_6

    const-string v0, "RealtimeWorkout"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 631
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v0, v8}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 635
    :cond_3
    :goto_0
    const-string v0, "Utilspro"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "resId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    .end local v6    # "category":Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "exercise":Ljava/lang/String;
    :cond_4
    return v9

    .line 624
    .restart local v6    # "category":Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "exercise":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 625
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 632
    :cond_6
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 633
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/core/constants/ExerciseNames;->namesIds:Ljava/util/TreeMap;

    invoke-virtual {v0, v6}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto :goto_0
.end method

.method public static getArabicFromEnglish(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "eng"    # Ljava/lang/String;

    .prologue
    .line 768
    const/16 v3, 0xa

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 771
    .local v1, "chars":[I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 772
    .local v0, "builder":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_1

    .line 773
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 774
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 775
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, -0x30

    aget v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 776
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "char - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    add-int/lit8 v5, v5, -0x30

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    add-int/lit8 v5, v5, -0x30

    aget v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 773
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 779
    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 783
    .end local v2    # "i":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 768
    :array_0
    .array-data 4
        0x669
        0x668
        0x667
        0x666
        0x665
        0x664
        0x663
        0x662
        0x661
        0x660
    .end array-data
.end method

.method public static getDaysPassedForDividerForAverageInExercise(J)I
    .locals 13
    .param p0, "periodStart"    # J

    .prologue
    const/4 v12, 0x5

    .line 84
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v2

    .line 85
    .local v2, "calendar":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v3

    .line 86
    .local v3, "currentMonthStart":J
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v0

    .line 88
    .local v0, "calculatingMonthStart":J
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getFirstDate()J

    move-result-wide v8

    .line 89
    .local v8, "firstTime":J
    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v6

    .line 91
    .local v6, "firstMonthStrt":J
    cmp-long v10, v3, v0

    if-nez v10, :cond_1

    .line 92
    cmp-long v10, v6, v0

    if-nez v10, :cond_0

    .line 93
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 94
    .local v5, "daysInMonth":I
    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 95
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v10

    sub-int v10, v5, v10

    add-int/lit8 v5, v10, 0x1

    .line 110
    :goto_0
    return v5

    .line 97
    .end local v5    # "daysInMonth":I
    :cond_0
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .restart local v5    # "daysInMonth":I
    goto :goto_0

    .line 100
    .end local v5    # "daysInMonth":I
    :cond_1
    cmp-long v10, v6, v0

    if-nez v10, :cond_2

    .line 101
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 102
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v5

    .line 103
    .restart local v5    # "daysInMonth":I
    invoke-virtual {v2, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 104
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v10

    sub-int v10, v5, v10

    add-int/lit8 v5, v10, 0x1

    goto :goto_0

    .line 106
    .end local v5    # "daysInMonth":I
    :cond_2
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 107
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v5

    .restart local v5    # "daysInMonth":I
    goto :goto_0
.end method

.method public static getDeltaSpeed(Ljava/util/ArrayList;II)F
    .locals 3
    .param p1, "speedIndex"    # I
    .param p2, "spaceNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
            ">;II)F"
        }
    .end annotation

    .prologue
    .line 226
    .local p0, "speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    if-nez p1, :cond_0

    .line 227
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getSpeedPerHour()F

    move-result v1

    int-to-float v2, p2

    div-float v0, v1, v2

    .line 231
    .local v0, "deltaSpeed":F
    :goto_0
    return v0

    .line 229
    .end local v0    # "deltaSpeed":F
    :cond_0
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getSpeedPerHour()F

    move-result v2

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getSpeedPerHour()F

    move-result v1

    sub-float v1, v2, v1

    int-to-float v2, p2

    div-float v0, v1, v2

    .restart local v0    # "deltaSpeed":F
    goto :goto_0
.end method

.method public static getDeltaSpeed(Ljava/util/ArrayList;IIF)F
    .locals 3
    .param p1, "speedIndex"    # I
    .param p2, "spaceNumber"    # I
    .param p3, "prevSpeed"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
            ">;IIF)F"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    if-nez p1, :cond_0

    .line 237
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getSpeedPerHour()F

    move-result v1

    int-to-float v2, p2

    div-float v0, v1, v2

    .line 241
    .local v0, "deltaSpeed":F
    :goto_0
    return v0

    .line 239
    .end local v0    # "deltaSpeed":F
    :cond_0
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getSpeedPerHour()F

    move-result v1

    sub-float/2addr v1, p3

    int-to-float v2, p2

    div-float v0, v1, v2

    .restart local v0    # "deltaSpeed":F
    goto :goto_0
.end method

.method public static getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0xa

    .line 739
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    .line 742
    .local v0, "arabicChars":[I
    new-array v2, v6, [C

    fill-array-data v2, :array_1

    .line 745
    .local v2, "engChars":[C
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 746
    .local v1, "builder":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_3

    .line 747
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v4, v6, :cond_3

    .line 748
    const/4 v3, 0x0

    .line 749
    .local v3, "found":Z
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    array-length v6, v0

    if-ge v5, v6, :cond_1

    .line 750
    if-nez v3, :cond_0

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aget v7, v0, v5

    if-ne v6, v7, :cond_0

    .line 751
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    add-int/lit16 v6, v6, -0x660

    aget-char v6, v2, v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 752
    const/4 v3, 0x1

    .line 749
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 756
    :cond_1
    if-nez v3, :cond_2

    .line 757
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 747
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 763
    .end local v3    # "found":Z
    .end local v4    # "i":I
    .end local v5    # "j":I
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 739
    nop

    :array_0
    .array-data 4
        0x660
        0x661
        0x662
        0x663
        0x664
        0x665
        0x666
        0x667
        0x668
        0x669
    .end array-data

    .line 742
    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
    .end array-data
.end method

.method public static getFirstDate()J
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 114
    const-wide/16 v8, 0x0

    .line 115
    .local v8, "time":J
    const-string/jumbo v3, "start_time<? AND exercise_type != ?"

    .line 116
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v4, v2

    const-string v0, ""

    aput-object v0, v4, v5

    .line 120
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 121
    const/16 v0, 0x4e23

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 122
    const/4 v6, 0x0

    .line 124
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string/jumbo v5, "start_time asc limit 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 129
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 131
    const-string/jumbo v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 138
    :goto_0
    if-eqz v6, :cond_0

    .line 139
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 142
    :cond_0
    :goto_1
    return-wide v8

    .line 133
    :cond_1
    const-wide/16 v8, 0x0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v7

    .line 136
    .local v7, "e":Ljava/lang/RuntimeException;
    const-wide/16 v8, 0x0

    .line 138
    if-eqz v6, :cond_0

    .line 139
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 138
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 139
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getIsPlayServicePopupDisplayed()Z
    .locals 1

    .prologue
    .line 600
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mPlayServicePopupDisplayed:Z

    return v0
.end method

.method public static getKoreanMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 719
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 720
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 721
    const-string v1, "%d%s %d%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const v3, 0x7f090066

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    const v4, 0x7f090068

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getLineColor(FFF)I
    .locals 13
    .param p0, "speed"    # F
    .param p1, "maxSpeed"    # F
    .param p2, "minSpeed"    # F

    .prologue
    .line 146
    add-float v9, p1, p2

    const/high16 v10, 0x40000000    # 2.0f

    div-float v0, v9, v10

    .line 147
    .local v0, "averageSpeed":F
    const/4 v6, 0x0

    .line 149
    .local v6, "rate":F
    const/16 v9, 0xff

    const/16 v10, 0xff

    const/16 v11, 0x42

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    .line 150
    .local v5, "minColor":I
    const/16 v9, 0xff

    const/16 v10, 0x92

    const/16 v11, 0xb2

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    .line 151
    .local v4, "midColor":I
    const/16 v9, 0xff

    const/4 v10, 0x7

    const/16 v11, 0x89

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    .line 152
    .local v3, "maxColor":I
    const/4 v8, 0x0

    .line 154
    .local v8, "targetColor":I
    float-to-double v9, p0

    const-wide/16 v11, 0x0

    cmpl-double v9, v9, v11

    if-nez v9, :cond_0

    .line 155
    move v8, v5

    .line 180
    :goto_0
    return v8

    .line 157
    :cond_0
    cmpl-float v9, v0, p0

    if-lez v9, :cond_1

    .line 158
    sub-float v9, p0, p2

    sub-float v10, v0, p2

    div-float v6, v9, v10

    .line 160
    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v9

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v10

    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    float-to-int v10, v10

    add-int v7, v9, v10

    .line 161
    .local v7, "red":I
    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v9

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v10

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    float-to-int v10, v10

    add-int v2, v9, v10

    .line 162
    .local v2, "green":I
    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v9

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v10

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    float-to-int v10, v10

    add-int v1, v9, v10

    .line 164
    .local v1, "blue":I
    const/16 v9, 0xff

    invoke-static {v9, v7, v2, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    .line 166
    goto :goto_0

    .line 167
    .end local v1    # "blue":I
    .end local v2    # "green":I
    .end local v7    # "red":I
    :cond_1
    sub-float v9, p0, v0

    sub-float v10, p1, v0

    div-float v6, v9, v10

    .line 169
    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v9

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v10

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    float-to-int v10, v10

    add-int v7, v9, v10

    .line 170
    .restart local v7    # "red":I
    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v9

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v10

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    float-to-int v10, v10

    add-int v2, v9, v10

    .line 171
    .restart local v2    # "green":I
    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v9

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v10

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    float-to-int v10, v10

    add-int v1, v9, v10

    .line 173
    .restart local v1    # "blue":I
    const/16 v9, 0xff

    invoke-static {v9, v7, v2, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    goto/16 :goto_0
.end method

.method public static getLocationItems(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;ZJ)Ljava/util/ArrayList;
    .locals 8
    .param p0, "selectionClause"    # Ljava/lang/String;
    .param p1, "sortOder"    # Ljava/lang/String;
    .param p2, "mContentResolver"    # Landroid/content/ContentResolver;
    .param p3, "mEndMode"    # Z
    .param p4, "mRowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            "ZJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    const/4 v6, 0x0

    .line 261
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p2

    move-object v3, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 262
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMapPathDB(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 264
    .local v7, "locationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 265
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 284
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 286
    :cond_1
    return-object v7

    .line 267
    :cond_2
    if-nez p3, :cond_0

    .line 268
    if-eqz v6, :cond_3

    .line 269
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 270
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND latitude > 0  AND longitude > 0 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND altitude < 1e10000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 274
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p2

    move-object v3, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 276
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMapPathDB(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v7

    .line 277
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 283
    .end local v7    # "locationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 284
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static getMapMode()I
    .locals 1

    .prologue
    .line 329
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mMapMode:I

    return v0
.end method

.method public static getMapMode(Landroid/app/Activity;ILjava/lang/String;)I
    .locals 8
    .param p0, "mActivity"    # Landroid/app/Activity;
    .param p1, "mapMode"    # I
    .param p2, "TAG"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 386
    const-string/jumbo v4, "mapMode"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 387
    .local v3, "viewMode":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 388
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v4, "mapMode"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 389
    .local v2, "savedViewMode":I
    const-string v4, "getMapMode()"

    invoke-static {p2, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    if-ne v2, v6, :cond_1

    .line 392
    if-ge p1, v7, :cond_0

    .line 393
    const/4 p1, 0x1

    .line 394
    :cond_0
    move v1, p1

    .line 395
    .local v1, "mMapMode":I
    const-string/jumbo v4, "mapMode"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 396
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 405
    :goto_0
    return v1

    .line 397
    .end local v1    # "mMapMode":I
    :cond_1
    const/4 v4, 0x3

    if-le v2, v4, :cond_2

    .line 398
    const/4 v1, 0x3

    .restart local v1    # "mMapMode":I
    goto :goto_0

    .line 400
    .end local v1    # "mMapMode":I
    :cond_2
    if-ge v2, v7, :cond_3

    .line 401
    const/4 v2, 0x1

    .line 402
    :cond_3
    move v1, v2

    .restart local v1    # "mMapMode":I
    goto :goto_0
.end method

.method public static getMonthGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 726
    new-instance v0, Ljava/lang/String;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 728
    .local v0, "dateFormatPattern":Ljava/lang/String;
    const-string v1, "d+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 729
    const-string v1, "M+"

    const-string v2, "MMM "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 731
    const-string/jumbo v1, "y+"

    const-string/jumbo v2, "yyyy "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 733
    const-string v1, " $"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 734
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static getPhotoDatas(ZLandroid/app/Activity;J)Ljava/util/ArrayList;
    .locals 8
    .param p0, "mEndMode"    # Z
    .param p1, "mActivity"    # Landroid/app/Activity;
    .param p2, "mRowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Landroid/app/Activity;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423
    const/4 v6, 0x0

    .line 424
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 426
    .local v7, "mPhotoDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    if-eqz p0, :cond_2

    .line 427
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 433
    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 434
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 435
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getExercisePhotoDataList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 438
    :cond_0
    if-eqz v6, :cond_1

    .line 439
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 442
    :cond_1
    return-object v7

    .line 430
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    goto :goto_0

    .line 438
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 439
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getSeedDataFormDB(Landroid/app/Activity;J)Ljava/util/HashMap;
    .locals 16
    .param p0, "mActivity"    # Landroid/app/Activity;
    .param p1, "mRowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "J)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    const-string v6, "AVG_SPEED"

    .line 340
    .local v6, "avgColumn":Ljava/lang/String;
    const-string v13, "MIN_SPEED"

    .line 341
    .local v13, "minColumn":Ljava/lang/String;
    const-string v12, "MAX_SPEED"

    .line 342
    .local v12, "maxColumn":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "mMaxSpeed":F
    const/4 v8, 0x0

    .local v8, "mAvgSpeed":F
    const/4 v10, 0x0

    .line 344
    .local v10, "mMinSpeed":F
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "speed_per_hour"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "MAX(speed_per_hour) AS "

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "MIN(speed_per_hour) AS "

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "AVG(speed_per_hour) AS "

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 351
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "exercise__id =?  AND update_time>? "

    .line 353
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-wide/16 v14, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 356
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 357
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v7, 0x0

    .line 360
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 362
    if-eqz v7, :cond_0

    .line 363
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 364
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 365
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    .line 366
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    long-to-float v8, v0

    .line 367
    invoke-interface {v7, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 371
    :cond_0
    if-eqz v7, :cond_1

    .line 372
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 375
    :cond_1
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 376
    .local v11, "mSpeedMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Float;>;"
    const-string v0, "MAX_SPEED"

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    const-string v0, "AVG_SPEED"

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    const-string v0, "MIN_SPEED"

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    return-object v11

    .line 371
    .end local v11    # "mSpeedMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Float;>;"
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 372
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getSelectionClause(ZJ)Ljava/lang/String;
    .locals 4
    .param p0, "mEndMode"    # Z
    .param p1, "mRowId"    # J

    .prologue
    .line 291
    if-eqz p0, :cond_0

    .line 292
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exercise__id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "selectionClause":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 294
    .end local v0    # "selectionClause":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exercise__id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "selectionClause":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getSpeedItems(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/ArrayList;
    .locals 8
    .param p0, "selectionClause"    # Ljava/lang/String;
    .param p1, "sortOder"    # Ljava/lang/String;
    .param p2, "mContentResolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 246
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p2

    move-object v3, p0

    move-object v4, v2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 248
    .local v7, "speed_cursor":Landroid/database/Cursor;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getRealTimeDB(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 249
    .local v6, "speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 252
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 253
    return-object v6
.end method

.method public static getXOffset(Landroid/view/View;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;ILandroid/content/Context;)I
    .locals 7
    .param p0, "caller"    # Landroid/view/View;
    .param p1, "popup"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    .param p2, "padding"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 679
    const/4 v3, 0x0

    .line 680
    .local v3, "xoffset":I
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 681
    .local v1, "location":[I
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 683
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .local v0, "displaymetrics":Landroid/util/DisplayMetrics;
    move-object v4, p3

    .line 684
    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 685
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 687
    .local v2, "screenWidth":I
    aget v4, v1, v6

    div-int/lit8 v5, v2, 0x2

    if-ge v4, v5, :cond_0

    .line 688
    const/high16 v4, 0x40c00000    # 6.0f

    invoke-static {p3, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    aget v5, v1, v6

    sub-int v3, v4, v5

    .line 693
    :goto_0
    return v3

    .line 690
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/2addr v4, p2

    add-int/lit8 v4, v4, -0x6

    neg-int v3, v4

    goto :goto_0
.end method

.method private static isMultiSelectSupportFromGallery(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "TAG"    # Ljava/lang/String;

    .prologue
    .line 665
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 667
    .local v0, "isSupport":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isSupport="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    return v0
.end method

.method public static launchConnectivityActivity(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 698
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 699
    .local v1, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 700
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_ID:Ljava/lang/String;

    const v4, 0x7f090ae8

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 701
    const/4 v3, 0x2

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    .line 704
    .local v0, "contentIds":[I
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_TYPE_INFO_CONTENT_IDS:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 706
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 707
    .local v2, "mDeviceTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v3, 0x2718

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 708
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPES_KEY:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 710
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v4, 0x7f0909e0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 711
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->ACTION_BAR_TITLE_RES_ID:Ljava/lang/String;

    const v4, 0x7f090023

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 712
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPE_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v4, 0x7f090a9e

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 713
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->PAIRED_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v4, 0x7f090a9f

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 715
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    const/16 v3, 0x7b

    invoke-virtual {p0, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 716
    return-void

    .line 701
    nop

    :array_0
    .array-data 4
        0x7f0300b3
        0x7f0300b4
    .end array-data
.end method

.method public static panAllRouteOnMap(Ljava/lang/Object;Landroid/util/DisplayMetrics;ZLcom/google/android/gms/maps/GoogleMap;Lcom/amap/api/maps2d/AMap;)Ljava/lang/Object;
    .locals 11
    .param p0, "bounds"    # Ljava/lang/Object;
    .param p1, "display"    # Landroid/util/DisplayMetrics;
    .param p2, "mIsMiniMode"    # Z
    .param p3, "gMap"    # Lcom/google/android/gms/maps/GoogleMap;
    .param p4, "aMap"    # Lcom/amap/api/maps2d/AMap;

    .prologue
    const/high16 v10, 0x43470000    # 199.0f

    const/high16 v9, 0x42480000    # 50.0f

    const/high16 v8, 0x41980000    # 19.0f

    const/high16 v7, 0x41a00000    # 20.0f

    const-wide/high16 v5, 0x3ff8000000000000L    # 1.5

    .line 185
    iget v1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 186
    .local v1, "width":I
    iget v2, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x42c00000    # 96.0f

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    sub-int v0, v2, v3

    .line 189
    .local v0, "height":I
    if-nez p3, :cond_2

    .line 190
    if-eqz p2, :cond_1

    .line 191
    check-cast p0, Lcom/amap/api/maps2d/model/LatLngBounds;

    .end local p0    # "bounds":Ljava/lang/Object;
    int-to-double v2, v1

    div-double/2addr v2, v5

    double-to-int v2, v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v10}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    int-to-double v3, v3

    div-double/2addr v3, v5

    double-to-int v3, v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v2, v3, v4}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngBounds(Lcom/amap/api/maps2d/model/LatLngBounds;III)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    .line 201
    :goto_0
    invoke-virtual {p4}, Lcom/amap/api/maps2d/AMap;->getCameraPosition()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v2

    iget v2, v2, Lcom/amap/api/maps2d/model/CameraPosition;->zoom:F

    cmpl-float v2, v2, v7

    if-lez v2, :cond_0

    .line 202
    invoke-static {v8}, Lcom/amap/api/maps2d/CameraUpdateFactory;->zoomTo(F)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    .line 220
    .end local p4    # "aMap":Lcom/amap/api/maps2d/AMap;
    :cond_0
    :goto_1
    return-object p4

    .line 196
    .restart local p0    # "bounds":Ljava/lang/Object;
    .restart local p4    # "aMap":Lcom/amap/api/maps2d/AMap;
    :cond_1
    check-cast p0, Lcom/amap/api/maps2d/model/LatLngBounds;

    .end local p0    # "bounds":Ljava/lang/Object;
    int-to-double v2, v1

    div-double/2addr v2, v5

    double-to-int v2, v2

    int-to-double v3, v0

    div-double/2addr v3, v5

    double-to-int v3, v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v9}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v2, v3, v4}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newLatLngBounds(Lcom/amap/api/maps2d/model/LatLngBounds;III)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/amap/api/maps2d/AMap;->moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V

    goto :goto_0

    .line 207
    .restart local p0    # "bounds":Ljava/lang/Object;
    :cond_2
    if-eqz p2, :cond_4

    .line 208
    check-cast p0, Lcom/google/android/gms/maps/model/LatLngBounds;

    .end local p0    # "bounds":Ljava/lang/Object;
    int-to-double v2, v1

    div-double/2addr v2, v5

    double-to-int v2, v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v10}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    int-to-double v3, v3

    div-double/2addr v3, v5

    double-to-int v3, v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v2, v3, v4}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 216
    :goto_2
    invoke-virtual {p3}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    iget v2, v2, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    cmpl-float v2, v2, v7

    if-lez v2, :cond_3

    .line 217
    invoke-static {v8}, Lcom/google/android/gms/maps/CameraUpdateFactory;->zoomTo(F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    :cond_3
    move-object p4, p3

    .line 220
    goto :goto_1

    .line 212
    .restart local p0    # "bounds":Ljava/lang/Object;
    :cond_4
    check-cast p0, Lcom/google/android/gms/maps/model/LatLngBounds;

    .end local p0    # "bounds":Ljava/lang/Object;
    int-to-double v2, v1

    div-double/2addr v2, v5

    double-to-int v2, v2

    int-to-double v3, v0

    div-double/2addr v3, v5

    double-to-int v3, v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v9}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v2, v3, v4}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    goto :goto_2
.end method

.method public static setAMapType(ILcom/amap/api/maps2d/AMap;)Lcom/amap/api/maps2d/AMap;
    .locals 3
    .param p0, "mode"    # I
    .param p1, "aMap"    # Lcom/amap/api/maps2d/AMap;

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 409
    if-ne p0, v0, :cond_0

    .line 410
    invoke-virtual {p1, v0}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 411
    invoke-virtual {p1, v2}, Lcom/amap/api/maps2d/AMap;->setTrafficEnabled(Z)V

    .line 419
    :goto_0
    return-object p1

    .line 412
    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    .line 413
    invoke-virtual {p1, v1}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 414
    invoke-virtual {p1, v1}, Lcom/amap/api/maps2d/AMap;->setTrafficEnabled(Z)V

    goto :goto_0

    .line 416
    :cond_1
    invoke-virtual {p1, v1}, Lcom/amap/api/maps2d/AMap;->setMapType(I)V

    .line 417
    invoke-virtual {p1, v2}, Lcom/amap/api/maps2d/AMap;->setTrafficEnabled(Z)V

    goto :goto_0
.end method

.method public static setGalleryBtnClick(IILjava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p0, "RequestCode"    # I
    .param p1, "viewSize"    # I
    .param p2, "TAG"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 641
    const/16 v3, 0x9

    if-ge p1, v3, :cond_1

    .line 643
    const-string v3, "gallery button clicked"

    invoke-static {p2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.PICK"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 647
    .local v2, "multiIntent":Landroid/content/Intent;
    const-string v3, "image/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 649
    invoke-static {p3, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->isMultiSelectSupportFromGallery(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 650
    const-string/jumbo v3, "pick-max-item"

    rsub-int/lit8 v4, p1, 0x9

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 652
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "remain add count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    rsub-int/lit8 v4, p1, 0x9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const-string/jumbo v3, "multi-pick"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 656
    :cond_0
    move-object v0, p3

    check-cast v0, Landroid/app/Activity;

    move-object v3, v0

    invoke-virtual {v3, v2, p0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 662
    .end local v2    # "multiIntent":Landroid/content/Intent;
    .end local p3    # "context":Landroid/content/Context;
    :cond_1
    :goto_0
    return-void

    .line 657
    .restart local p3    # "context":Landroid/content/Context;
    :catch_0
    move-exception v1

    .line 658
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 659
    check-cast p3, Landroid/app/Activity;

    .end local p3    # "context":Landroid/content/Context;
    invoke-static {p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->showGalleryDisabledPopup(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private static showGalleryDisabledPopup(Landroid/app/Activity;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 673
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0900d9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090936

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    check-cast p0, Landroid/support/v4/app/FragmentActivity;

    .end local p0    # "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "gallery_not_enabled"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 676
    return-void
.end method

.method public static updateDataText(ILandroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Ljava/util/ArrayList;Landroid/view/View;)V
    .locals 20
    .param p0, "type"    # I
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mWorkoutController"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    .param p4, "mView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 447
    .local p3, "dispDataTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    sget-object v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->dataTextViewIDs:[I

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v6, v0, :cond_1b

    .line 448
    const/4 v11, 0x0

    .line 449
    .local v11, "update":Z
    const-string v15, ""

    .local v15, "valueStr":Ljava/lang/String;
    const-string v10, ""

    .line 450
    .local v10, "unitStr":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0x7

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 451
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v4

    .line 452
    .local v4, "goalType":I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v2

    .line 453
    .local v2, "dataType":I
    const/16 v17, 0x7

    move/from16 v0, p0

    move/from16 v1, v17

    if-eq v0, v1, :cond_0

    move/from16 v0, p0

    if-ne v0, v2, :cond_1

    .line 454
    :cond_0
    const/4 v11, 0x1

    .line 455
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v2, v0, :cond_5

    .line 456
    const-string v15, "--"

    .line 457
    const-string v10, ""

    .line 547
    .end local v2    # "dataType":I
    .end local v4    # "goalType":I
    :cond_1
    :goto_1
    if-eqz v11, :cond_4

    .line 548
    sget-object v17, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->dataTextViewIDs:[I

    aget v17, v17, v6

    move-object/from16 v0, p4

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 550
    .local v16, "view":Landroid/view/View;
    const v17, 0x7f0806e4

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 552
    .local v9, "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    const/16 v17, 0x13

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0x13

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    :cond_2
    const/16 v17, 0x3

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_1a

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0x13

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1a

    .line 555
    :cond_3
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 560
    .local v12, "updateText":Ljava/lang/String;
    :goto_2
    invoke-static {v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setText(Ljava/lang/CharSequence;)V

    .line 561
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->invalidate()V

    .line 447
    .end local v9    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v12    # "updateText":Ljava/lang/String;
    .end local v16    # "view":Landroid/view/View;
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 458
    .restart local v2    # "dataType":I
    .restart local v4    # "goalType":I
    :cond_5
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v2, v0, :cond_6

    .line 459
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainSecToGoal(J)J

    move-result-wide v13

    .line 460
    .local v13, "val":J
    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v15

    .line 461
    const-string v10, ""

    .line 462
    goto/16 :goto_1

    .line 463
    .end local v13    # "val":J
    :cond_6
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Float;->floatValue()F

    move-result v17

    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v13

    .line 465
    .local v13, "val":F
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v15

    .line 466
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 469
    .end local v2    # "dataType":I
    .end local v4    # "goalType":I
    .end local v13    # "val":F
    :cond_7
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0xe

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, p0

    move/from16 v1, v17

    if-eq v0, v1, :cond_9

    :cond_8
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0xe

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_10

    const/16 v17, 0x18

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_10

    .line 471
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v4

    .line 472
    .restart local v4    # "goalType":I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v2

    .line 473
    .restart local v2    # "dataType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v5

    .line 474
    .local v5, "goalValue":I
    const/16 v17, 0xe

    move/from16 v0, p0

    move/from16 v1, v17

    if-eq v0, v1, :cond_a

    const/16 v17, 0x18

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 475
    :cond_a
    const/4 v11, 0x1

    .line 476
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v2, v0, :cond_b

    .line 477
    const-string v15, "--"

    .line 478
    const-string v10, ""

    goto/16 :goto_1

    .line 479
    :cond_b
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ne v4, v0, :cond_d

    .line 480
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v7

    .line 481
    .local v7, "level":I
    const/4 v8, 0x0

    .line 482
    .local v8, "progressInPercentage":F
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    const/16 v18, 0x18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_c

    .line 483
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    const/16 v18, 0x18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 486
    :cond_c
    const/16 v17, 0x18

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v15

    .line 487
    const-string v10, "%"

    .line 488
    goto/16 :goto_1

    .end local v7    # "level":I
    .end local v8    # "progressInPercentage":F
    :cond_d
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v2, v0, :cond_f

    .line 489
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ne v4, v0, :cond_e

    .line 491
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v7

    .line 492
    .restart local v7    # "level":I
    move-object/from16 v0, p1

    invoke-static {v0, v7, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectStringByType(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v15

    .line 493
    const-string v10, ""

    .line 494
    goto/16 :goto_1

    .line 495
    .end local v7    # "level":I
    :cond_e
    mul-int/lit8 v17, v5, 0x3c

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v15

    .line 496
    const-string v10, ""

    goto/16 :goto_1

    .line 499
    :cond_f
    int-to-float v0, v5

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v15

    .line 500
    int-to-float v0, v5

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 503
    .end local v2    # "dataType":I
    .end local v4    # "goalType":I
    .end local v5    # "goalValue":I
    :cond_10
    const/16 v17, 0x13

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_11

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0x13

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_12

    :cond_11
    const/16 v17, 0x3

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0x13

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_14

    .line 505
    :cond_12
    const/4 v11, 0x1

    .line 506
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    const/16 v18, 0x3

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Float;->floatValue()F

    move-result v17

    const/16 v18, 0x0

    cmpg-float v17, v17, v18

    if-gtz v17, :cond_13

    .line 507
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f090acd

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 512
    :goto_3
    const/16 v18, 0x13

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    move/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 509
    :cond_13
    const/16 v18, 0x13

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    const/16 v19, 0x3

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    move/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v15

    goto :goto_3

    .line 514
    :cond_14
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 515
    const/4 v11, 0x1

    .line 516
    const/16 v17, 0x1

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_15

    .line 517
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v15

    .line 518
    const-string v10, ""

    goto/16 :goto_1

    .line 519
    :cond_15
    const/16 v17, 0x17

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_17

    .line 520
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v4

    .line 521
    .restart local v4    # "goalType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v5

    .line 522
    .restart local v5    # "goalValue":I
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ne v4, v0, :cond_1

    .line 523
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v7

    .line 524
    .restart local v7    # "level":I
    const-string v3, ""

    .line 525
    .local v3, "goal":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    const/16 v18, 0x17

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_16

    .line 526
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v7, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectStringByType(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v3

    .line 528
    :cond_16
    move-object v15, v3

    goto/16 :goto_1

    .line 534
    .end local v3    # "goal":Ljava/lang/String;
    .end local v4    # "goalType":I
    .end local v5    # "goalValue":I
    .end local v7    # "level":I
    :cond_17
    const/16 v17, 0x5

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_18

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Float;->floatValue()F

    move-result v17

    const/16 v18, 0x0

    cmpl-float v17, v17, v18

    if-nez v17, :cond_18

    .line 535
    const-string v15, "--"

    .line 542
    :goto_4
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    move/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 536
    :cond_18
    const/16 v17, 0x6

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_19

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Float;->floatValue()F

    move-result v17

    const/16 v18, 0x0

    cmpl-float v17, v17, v18

    if-nez v17, :cond_19

    .line 538
    const-string v15, "0"

    goto :goto_4

    .line 540
    :cond_19
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v17

    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Float;

    move/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v15

    goto :goto_4

    .line 557
    .restart local v9    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .restart local v16    # "view":Landroid/view/View;
    :cond_1a
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "updateText":Ljava/lang/String;
    goto/16 :goto_2

    .line 564
    .end local v9    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v10    # "unitStr":Ljava/lang/String;
    .end local v11    # "update":Z
    .end local v12    # "updateText":Ljava/lang/String;
    .end local v15    # "valueStr":Ljava/lang/String;
    .end local v16    # "view":Landroid/view/View;
    :cond_1b
    return-void
.end method

.method public static updateGooglePlayServicesIfNeeded(ZLandroid/app/Activity;ZLjava/lang/String;)Z
    .locals 5
    .param p0, "forceCheck"    # Z
    .param p1, "mContext"    # Landroid/app/Activity;
    .param p2, "playServicePopupDisplayed"    # Z
    .param p3, "TAG"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 569
    sget-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v3, :cond_1

    move v0, v2

    .line 596
    :cond_0
    :goto_0
    return v0

    .line 573
    :cond_1
    sput-boolean p2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mPlayServicePopupDisplayed:Z

    .line 576
    const/4 v0, 0x0

    .line 577
    .local v0, "latest":Z
    invoke-static {p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    .line 578
    .local v1, "status":I
    if-nez v1, :cond_2

    .line 579
    const/4 v0, 0x1

    goto :goto_0

    .line 581
    :cond_2
    if-nez p0, :cond_3

    sget-boolean v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mPlayServicePopupDisplayed:Z

    if-nez v3, :cond_0

    .line 584
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "!!!!! ConnectionResult = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    const/4 v3, 0x2

    if-eq v1, v3, :cond_4

    const/16 v3, 0x9

    if-eq v1, v3, :cond_4

    if-eq v1, v2, :cond_4

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 590
    :cond_4
    invoke-static {v1, p1, v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mDialog:Landroid/app/Dialog;

    .line 591
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mPlayServicePopupDisplayed:Z

    .line 593
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method
