.class public Lcom/sec/android/app/shealth/stress/log/StressLogActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "StressLogActivity.java"


# static fields
.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field public static SELECTED_FILTER_INDEX:I


# instance fields
.field private mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

.field private mStressLogAdapter:Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

.field private mStressObserver:Landroid/database/ContentObserver;

.field private menu:Landroid/view/Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->SELECTED_FILTER_INDEX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 36
    new-instance v0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity$1;-><init>(Lcom/sec/android/app/shealth/stress/log/StressLogActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/stress/log/StressLogActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/log/StressLogActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/log/StressLogActivity;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/log/StressLogActivity;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/stress/log/StressLogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/log/StressLogActivity;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->refreshAdapter()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/stress/log/StressLogActivity;)Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/log/StressLogActivity;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressLogAdapter:Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

    return-object v0
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 143
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 2

    .prologue
    .line 100
    invoke-static {p0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    const-string v1, "0==0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getGroupCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mCursor:Landroid/database/Cursor;

    .line 102
    new-instance v0, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mCursor:Landroid/database/Cursor;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressLogAdapter:Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressLogAdapter:Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

    return-object v0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    const-string v0, "comment"

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x0

    .line 120
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 121
    .local v0, "calendar":Ljava/util/Calendar;
    const-string/jumbo v6, "state"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 122
    .local v3, "state":I
    const-string/jumbo v6, "sample_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 123
    .local v4, "time":J
    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 124
    new-instance v6, Ljava/text/DateFormatSymbols;

    invoke-direct {v6}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v6}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x7

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    aget-object v2, v6, v7

    .line 125
    .local v2, "dname":Ljava/lang/String;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "dd/MM/yyyy hh:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "date":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x5

    invoke-virtual {v1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v2, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") | "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0900d2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v1, 0x7f090076

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 154
    sput p1, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->SELECTED_FILTER_INDEX:I

    .line 155
    const-string v0, "0==0"

    return-object v0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 3

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.stress"

    const-string v2, "ST08"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressLogAdapter:Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getShareDataByIds(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 132
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "comment":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 171
    packed-switch p1, :pswitch_data_0

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 173
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressLogAdapter:Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->notifyDataSetChanged()V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressLogAdapter:Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/log/StressLogAdapter;->getTotalChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->menu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->showNoDataView()V

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x45b
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 65
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 66
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 67
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->startActivity(Landroid/content/Intent;)V

    .line 69
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 71
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->registerObserver()V

    .line 72
    const v1, 0x7f0205b7

    const v2, 0x7f09006a

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->setNoLogImageAndText(II)V

    .line 73
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->setMaxItemShareLimit(I)V

    .line 74
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->unregisterObserver()V

    .line 79
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onDestroy()V

    .line 80
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f080c8b

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 84
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->menu:Landroid/view/Menu;

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 87
    invoke-static {p0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getDataCount()I

    move-result v2

    if-gtz v2, :cond_0

    .line 88
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    move v0, v1

    .line 95
    :goto_0
    return v0

    .line 91
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 92
    .local v0, "result":Z
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 93
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09110b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 94
    const v1, 0x7f080c8c

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public registerObserver()V
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 49
    return-void
.end method

.method protected setUpSelectMode()V
    .locals 0

    .prologue
    .line 186
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setUpSelectMode()V

    .line 191
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 113
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/stress/log/StressLogDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "HEART_RATE_ID_KEY"

    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const/16 v1, 0x45b

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 116
    return-void
.end method

.method public unregisterObserver()V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 53
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/log/StressLogActivity;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    .line 139
    return-void
.end method
