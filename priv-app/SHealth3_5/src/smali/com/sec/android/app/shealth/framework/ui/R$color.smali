.class public final Lcom/sec/android/app/shealth/framework/ui/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final ab_spinner_item_normal_text:I = 0x7f0700d7

.field public static final ab_spinner_item_selected_text:I = 0x7f0700d6

.field public static final accesory_device_status:I = 0x7f0700cf

.field public static final accesory_device_text:I = 0x7f070048

.field public static final accessory_connection_view_state_text_color:I = 0x7f070051

.field public static final aciton_bar_select_spn_shadow:I = 0x7f0700c9

.field public static final aciton_bar_select_spn_txt:I = 0x7f0700c8

.field public static final action_bar_text_color:I = 0x7f070098

.field public static final action_bar_text_shadow_color:I = 0x7f070047

.field public static final actionbar_spinner_item_selector:I = 0x7f070297

.field public static final actionbar_summary_div:I = 0x7f0700ce

.field public static final alert_dialog_title_dark_color:I = 0x7f070067

.field public static final alert_dialog_title_light_color:I = 0x7f070068

.field public static final bg_drawer_default_profile_picture:I = 0x7f0700d5

.field public static final black_color:I = 0x7f070042

.field public static final black_with_shadow:I = 0x7f070099

.field public static final bt_item_device:I = 0x7f0700d0

.field public static final calendar_bg_color:I = 0x7f0700a1

.field public static final calendar_black:I = 0x7f0700b9

.field public static final calendar_btn_this:I = 0x7f0700d1

.field public static final calendar_button_disabled:I = 0x7f0700ad

.field public static final calendar_button_enabled:I = 0x7f0700ac

.field public static final calendar_button_pressed:I = 0x7f0700af

.field public static final calendar_button_text_sunday:I = 0x7f0700a6

.field public static final calendar_day_color:I = 0x7f0700bd

.field public static final calendar_day_selected:I = 0x7f0700be

.field public static final calendar_day_text_selector:I = 0x7f070299

.field public static final calendar_day_today:I = 0x7f0700bf

.field public static final calendar_days_of_week_container_background_color:I = 0x7f07009a

.field public static final calendar_grey:I = 0x7f0700bc

.field public static final calendar_hdivider_color:I = 0x7f0700b6

.field public static final calendar_month_text_color:I = 0x7f0700b7

.field public static final calendar_pager_container_background_color:I = 0x7f07009c

.field public static final calendar_period_switches_container_background_color:I = 0x7f07009b

.field public static final calendar_selected_day:I = 0x7f0700ae

.field public static final calendar_text_day:I = 0x7f0700a4

.field public static final calendar_text_day_dim:I = 0x7f0700a8

.field public static final calendar_text_selected:I = 0x7f0700a3

.field public static final calendar_text_selected_day:I = 0x7f0700a7

.field public static final calendar_text_sunday:I = 0x7f0700a5

.field public static final calendar_text_sunday_dim:I = 0x7f0700a9

.field public static final calendar_text_today:I = 0x7f0700a2

.field public static final calendar_today_monthdate_color:I = 0x7f0700b8

.field public static final calendar_today_pressed:I = 0x7f0700ba

.field public static final calendar_today_text_selector:I = 0x7f07029a

.field public static final calendar_top_ampm:I = 0x7f0700bb

.field public static final calendar_underline_abnormal:I = 0x7f0700ab

.field public static final calendar_underline_normal:I = 0x7f0700aa

.field public static final cancel_ok_button_text_color:I = 0x7f070041

.field public static final cancel_ok_button_text_selector:I = 0x7f07029b

.field public static final cancel_ok_green_text_color:I = 0x7f070064

.field public static final color_4c4c4a:I = 0x7f0700c0

.field public static final common_btn_color:I = 0x7f07029e

.field public static final common_spn_shadow:I = 0x7f0700c5

.field public static final common_spn_txt:I = 0x7f0702a1

.field public static final common_wgt_txt_color:I = 0x7f0702a2

.field public static final compatible_device_text:I = 0x7f0700d2

.field public static final connectivity_scanning_header_text:I = 0x7f0700d3

.field public static final cursor_color:I = 0x7f07007e

.field public static final cyan:I = 0x7f070043

.field public static final dark_green:I = 0x7f070083

.field public static final date_selector_bg_color:I = 0x7f0700cd

.field public static final day_color:I = 0x7f0700b0

.field public static final default_background_color:I = 0x7f07004f

.field public static final default_text_color:I = 0x7f07004e

.field public static final default_window_background_color:I = 0x7f070080

.field public static final dialog_item_content_text:I = 0x7f070040

.field public static final dialog_item_text:I = 0x7f07004c

.field public static final dialog_secondary_text:I = 0x7f07004d

.field public static final dialog_title:I = 0x7f07004b

.field public static final dialog_top_text_color:I = 0x7f070065

.field public static final drawer_item_selector:I = 0x7f0702a3

.field public static final drawer_menu_header_text:I = 0x7f0700d4

.field public static final drawer_menu_header_text1:I = 0x7f070096

.field public static final drawer_menu_selected_text_color:I = 0x7f070090

.field public static final graph_anime_handler_text:I = 0x7f0700d9

.field public static final graph_bg_color:I = 0x7f0700d8

.field public static final graph_black:I = 0x7f07008c

.field public static final graph_bubble_text:I = 0x7f0700da

.field public static final graph_bubble_value_text:I = 0x7f0700db

.field public static final graph_color:I = 0x7f070089

.field public static final graph_dark_grey:I = 0x7f070093

.field public static final graph_grey:I = 0x7f070088

.field public static final graph_information_nodata:I = 0x7f0700c7

.field public static final graph_legend_background_color:I = 0x7f070092

.field public static final graph_medium_grey:I = 0x7f070087

.field public static final graph_no_data_text_color:I = 0x7f0700dc

.field public static final graph_transparent:I = 0x7f07008a

.field public static final graph_weight_line_color:I = 0x7f07008b

.field public static final graph_yellow:I = 0x7f070081

.field public static final grey_text:I = 0x7f07009e

.field public static final health_care_input_background:I = 0x7f07006d

.field public static final health_care_summary_bg_dark:I = 0x7f07006c

.field public static final holo_orange_light:I = 0x7f0700c4

.field public static final horizontal_axes_color:I = 0x7f07008d

.field public static final input_activity_date_btn_txt:I = 0x7f0700cb

.field public static final input_activity_date_divider:I = 0x7f0700cc

.field public static final input_field_normal_bottom_line:I = 0x7f070077

.field public static final input_field_ubnormal_bottom_line:I = 0x7f070078

.field public static final input_memo_header_text_color:I = 0x7f0700ca

.field public static final input_view_normal_color:I = 0x7f07006e

.field public static final input_view_systolic_input_color:I = 0x7f07006f

.field public static final inputmodule_abnormal_color:I = 0x7f070075

.field public static final inputmodule_normal_color:I = 0x7f070074

.field public static final list_popup_item_default_text_color:I = 0x7f07007a

.field public static final list_popup_item_selected_text_color:I = 0x7f070079

.field public static final log_no_data_message_text_color:I = 0x7f0700de

.field public static final log_no_data_text_color:I = 0x7f0700dd

.field public static final log_select_all:I = 0x7f070095

.field public static final menu_background:I = 0x7f070046

.field public static final menu_background_white:I = 0x7f07004a

.field public static final menu_selected:I = 0x7f070045

.field public static final no_connected_accessories_text:I = 0x7f070097

.field public static final no_food_nfo_color:I = 0x7f070086

.field public static final picker_highlight:I = 0x7f07009f

.field public static final picker_text:I = 0x7f0700a0

.field public static final popup_btn_bg:I = 0x7f0700b3

.field public static final popup_title:I = 0x7f070049

.field public static final progress_text_color:I = 0x7f07007f

.field public static final regular_background:I = 0x7f07008e

.field public static final regular_text_color:I = 0x7f07008f

.field public static final s_health_setup_btn:I = 0x7f070082

.field public static final saturday_color:I = 0x7f0700b2

.field public static final scan_button_bottom_font_color_selector:I = 0x7f0702b2

.field public static final spinner_item_normal_text:I = 0x7f07007c

.field public static final spinner_item_selected_text:I = 0x7f07007d

.field public static final spinner_text_color_selector:I = 0x7f0702b3

.field public static final split_line:I = 0x7f070091

.field public static final split_line_log_list:I = 0x7f0700b4

.field public static final sub_tab_text_color_selector:I = 0x7f0702b4

.field public static final summary_animated_counter_bad_color:I = 0x7f070071

.field public static final summary_animated_counter_default_color:I = 0x7f070070

.field public static final summary_animated_counter_good_color:I = 0x7f070072

.field public static final summary_animated_counter_shadow_color:I = 0x7f070073

.field public static final summary_view_abnormal_color:I = 0x7f070069

.field public static final summary_view_background_color:I = 0x7f070055

.field public static final summary_view_balance_interval_color:I = 0x7f07006b

.field public static final summary_view_bottom_background_color:I = 0x7f070056

.field public static final summary_view_button_text_color:I = 0x7f070053

.field public static final summary_view_button_text_color_disabled:I = 0x7f070054

.field public static final summary_view_button_text_shadow_color:I = 0x7f070052

.field public static final summary_view_content_bar_state_and_info_text_color:I = 0x7f07005b

.field public static final summary_view_content_bar_title_color:I = 0x7f07005c

.field public static final summary_view_content_bar_value_text_balance_color:I = 0x7f070058

.field public static final summary_view_content_bar_value_text_no_data_color:I = 0x7f07005a

.field public static final summary_view_content_bar_value_text_unbalance_color:I = 0x7f070059

.field public static final summary_view_content_divider_color:I = 0x7f07005d

.field public static final summary_view_header_balance_state_text_color:I = 0x7f070050

.field public static final summary_view_health_care_background_color:I = 0x7f070057

.field public static final summary_view_normal_color:I = 0x7f07006a

.field public static final summary_view_progress_bar_aftermeal_column_unbalance_color:I = 0x7f070061

.field public static final summary_view_progress_bar_column_balance_color:I = 0x7f07005e

.field public static final summary_view_progress_bar_fasting_column_unbalance_color:I = 0x7f070060

.field public static final summary_view_progress_bar_label_color:I = 0x7f070063

.field public static final summary_view_progress_bar_slider_balance_color:I = 0x7f07005f

.field public static final summary_view_progress_bar_title_color:I = 0x7f070062

.field public static final sunday_color:I = 0x7f0700b1

.field public static final sunday_color_again:I = 0x7f0700b5

.field public static final text_care:I = 0x7f070094

.field public static final text_date_bar:I = 0x7f070084

.field public static final text_regular:I = 0x7f07007b

.field public static final text_regular_50:I = 0x7f07009d

.field public static final text_view_item_color:I = 0x7f070066

.field public static final text_white:I = 0x7f070085

.field public static final txt_contents_0_light:I = 0x7f0700c3

.field public static final txt_contents_1_light:I = 0x7f0700c1

.field public static final txt_contents_6_light:I = 0x7f0700c2

.field public static final txt_contents_disabled:I = 0x7f0700c6

.field public static final vertical_progress_mark_color:I = 0x7f070076

.field public static final white:I = 0x7f070044


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
