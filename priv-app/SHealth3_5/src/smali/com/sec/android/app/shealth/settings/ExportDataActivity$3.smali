.class Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;
.super Ljava/lang/Object;
.source "ExportDataActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/ExportDataActivity;->completeExportPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 353
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    const v0, 0x7f080375

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportPath:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$1002(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportPath:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$1000(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0907d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090880

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090881

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > S Health > Export"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    return-void
.end method
