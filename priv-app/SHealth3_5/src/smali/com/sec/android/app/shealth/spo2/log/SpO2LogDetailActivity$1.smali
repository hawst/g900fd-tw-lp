.class Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;
.super Ljava/lang/Object;
.source "SpO2LogDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 165
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 166
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 167
    .local v0, "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 181
    .end local v0    # "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 169
    .restart local v0    # "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    const-class v3, Lcom/sec/android/app/shealth/spo2/log/SpO2InputActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 171
    const-string v2, "MODE_KEY"

    const-string v3, "Edit"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v2, "SPO2_ID_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$000(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getId()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 173
    const-string v2, "TIME_DATE_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mTimeDate:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$100(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    const-string v2, "HEART_RATE_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mPulse:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$200(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    const-string v2, "SPO2_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$300(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    const-string v3, "COMMENT_KEY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    invoke-static {v2}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$000(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getComment()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, ""

    :goto_1
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 176
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    invoke-static {v2}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$000(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getComment()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 167
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
