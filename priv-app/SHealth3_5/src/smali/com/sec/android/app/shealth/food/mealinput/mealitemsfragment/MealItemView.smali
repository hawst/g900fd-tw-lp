.class public Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
.super Landroid/widget/RelativeLayout;
.source "MealItemView.java"


# instance fields
.field private context:Landroid/content/Context;

.field protected dividerView:Landroid/view/View;

.field protected mDishMinusButton:Landroid/view/View;

.field protected mMealItemKcal:Landroid/widget/TextView;

.field protected mMealItemName:Landroid/widget/TextView;

.field private mPortionSizeClickArea:Landroid/view/View;

.field protected mealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->context:Landroid/content/Context;

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300ea

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    return-void
.end method


# virtual methods
.method protected getFoodInfoDataByMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 1
    .param p1, "mealItemData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->context:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFoodInfoItemFromMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    return-object v0
.end method

.method public getMealItemName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemName:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 2
    .param p1, "mealItemData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 55
    const v1, 0x7f0803ec

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemName:Landroid/widget/TextView;

    .line 56
    const v1, 0x7f0803ed

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemKcal:Landroid/widget/TextView;

    .line 57
    const v1, 0x7f0803ee

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mDishMinusButton:Landroid/view/View;

    .line 58
    const v1, 0x7f0803eb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mPortionSizeClickArea:Landroid/view/View;

    .line 60
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getFoodInfoDataByMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    .line 61
    .local v0, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->updateMealItemData(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 62
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setClickable(Z)V

    .line 68
    return-void
.end method

.method public init(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "mealItemData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p2, "dishMinusButtonClickListener"    # Landroid/view/View$OnClickListener;
    .param p3, "portionSizeClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->init(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mDishMinusButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mDishMinusButton:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mPortionSizeClickArea:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020222

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mPortionSizeClickArea:Landroid/view/View;

    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    return-void
.end method

.method public setEditable(ZZ)V
    .locals 4
    .param p1, "editable"    # Z
    .param p2, "isVisiblePortionButton"    # Z

    .prologue
    .line 137
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 138
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mDishMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 140
    if-nez p2, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mPortionSizeClickArea:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mPortionSizeClickArea:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :cond_0
    return-void

    .line 137
    .end local v0    # "visibility":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setMealKcal(Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;)V
    .locals 7
    .param p1, "foodItemCaloriesAndNutrients"    # Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    .prologue
    const v6, 0x7f0900bf

    const v5, 0x7f0900b9

    const/4 v4, 0x0

    .line 162
    const-string v2, " "

    .line 163
    .local v2, "space":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCarbos()F

    move-result v3

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getProteins()F

    move-result v3

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getFat()F

    move-result v3

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    const/4 v0, 0x1

    .line 164
    .local v0, "isAllNutrientsZero":Z
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportsBoohee()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCarbos()F

    move-result v3

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    .line 166
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCalories()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "kCalDesc":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemKcal:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemKcal:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 181
    return-void

    .line 163
    .end local v0    # "isAllNutrientsZero":Z
    .end local v1    # "kCalDesc":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 168
    .restart local v0    # "isAllNutrientsZero":Z
    :cond_1
    if-eqz v0, :cond_2

    .line 170
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCalories()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "kCalDesc":Ljava/lang/String;
    goto :goto_1

    .line 174
    .end local v1    # "kCalDesc":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCalories()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0909cb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getFat()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0909ca

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCarbos()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090966

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getProteins()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "kCalDesc":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method protected setMealKcal(Ljava/lang/String;)V
    .locals 1
    .param p1, "mealKcal"    # Ljava/lang/String;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemKcal:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemKcal:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 192
    return-void
.end method

.method protected setMealName(Ljava/lang/String;)V
    .locals 1
    .param p1, "mealName"    # Ljava/lang/String;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mMealItemName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method public updateMealItemData(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 2
    .param p1, "foodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v0

    const v1, 0x46cd7

    if-ne v0, v1, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealKcal(Ljava/lang/String;)V

    .line 102
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealName(Ljava/lang/String;)V

    .line 105
    :cond_0
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->context:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorieAndNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Landroid/content/Context;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealKcal(Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;)V

    goto :goto_0
.end method
