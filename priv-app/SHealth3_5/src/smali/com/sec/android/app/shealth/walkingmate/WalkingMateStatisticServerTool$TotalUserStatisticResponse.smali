.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;
.super Ljava/lang/Object;
.source "WalkingMateStatisticServerTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TotalUserStatisticResponse"
.end annotation


# instance fields
.field public final totalDistance:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "JSONString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 509
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 510
    .local v0, "object":Lorg/json/JSONObject;
    const-string/jumbo v1, "totalDistance"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;->totalDistance:Ljava/lang/String;

    .line 511
    return-void
.end method


# virtual methods
.method public getTotalDistance()J
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;->totalDistance:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalDistanceStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$TotalUserStatisticResponse;->totalDistance:Ljava/lang/String;

    return-object v0
.end method
