.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$7;
.super Ljava/lang/Object;
.source "AMapExerciseProStatusFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateAudioGuideButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0

    .prologue
    .line 981
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 985
    if-eqz p2, :cond_0

    .line 986
    invoke-static {v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeAudioGuideIndex(I)V

    .line 988
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 989
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090a77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 990
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v2, "P037"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->keepMusicText:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2002(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;Z)Z

    .line 999
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateAudioGuideButton()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V

    .line 1000
    return-void

    .line 993
    :cond_0
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeAudioGuideIndex(I)V

    .line 995
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090a78

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 996
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v2, "P038"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
