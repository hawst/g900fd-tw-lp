.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;
.super Landroid/os/AsyncTask;
.source "BarcodeSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchFoodInfoFromBarCodeAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final BARCODE_NOT_FOUND_IN_FATSECRET_DB:I = 0x2

.field private static final BARCODE_NOT_FOUND_IN_SAMSUNG_OSP:I = 0x3

.field private static final NO_BARCODE_IN_QR:I = 0x4

.field public static final NO_INTERNET_CONNECTION_CODE:I = -0x1


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;

    .prologue
    .line 271
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 4
    .param p1, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 287
    aget-object v0, p1, v3

    const/16 v1, 0x101

    invoke-static {v1}, Lcom/sec/android/app/shealth/core/barcode/BarcodeUtil;->getBarcodeType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    aget-object v0, p1, v2

    invoke-static {v0}, Lcom/sec/android/app/shealth/core/barcode/BarcodeUtil;->onlyDigits(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    new-array v0, v3, [Ljava/lang/Integer;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 290
    const/4 v0, 0x0

    .line 293
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchFactory;->getSearchApi(Landroid/content/Context;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;

    aget-object v2, p1, v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performBarcodeRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 271
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->doInBackground([Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "extraFoodInfoHolder":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;>;"
    const/4 v4, 0x1

    .line 315
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 316
    if-eqz p1, :cond_1

    .line 317
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->isSuccessful()Z

    move-result v1

    if-nez v1, :cond_0

    .line 318
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$8;->$SwitchMap$com$sec$android$app$shealth$food$foodpick$search$api$base$TaskResult:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getTaskResult()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/TaskResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 327
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 328
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->startCameraPreview(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1400(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Z)V

    .line 342
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 343
    return-void

    .line 321
    :pswitch_0
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "No food item found in FatSecret"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 324
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showErrorDialogAboutFailSearchByBarcodeFromDb()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    goto :goto_0

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mExtraFoodInfoHolder:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1502(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    .line 333
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getResultContents()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getRequest()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;->getFoodInfoData()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v0

    .line 335
    .local v0, "foodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1200()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Food item was found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getResultContents()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;->getRequest()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;->getFoodInfoData()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showSuccessDialog(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1000(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 340
    .end local v0    # "foodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->showErrorDialogAboutFailSearchByBarcodeFromDb()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    goto :goto_0

    .line 318
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 271
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->onPostExecute(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 280
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 281
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 299
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 300
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 310
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 311
    return-void

    .line 300
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 271
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
