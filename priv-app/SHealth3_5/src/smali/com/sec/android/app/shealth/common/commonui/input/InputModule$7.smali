.class Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7$TimerTaskOne;
    }
.end annotation


# static fields
.field public static final INCREASE_DELAY:I = 0x32


# instance fields
.field public final LONGPRESS_TIMEOUT:I

.field private mCurrentlyPressedView:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private volatile mIsQuickIncDecBegins:Z

.field private mTimer:Ljava/util/Timer;

.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V
    .locals 1

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->LONGPRESS_TIMEOUT:I

    .line 413
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mHandler:Landroid/os/Handler;

    .line 494
    return-void
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;
    .param p1, "x1"    # Z

    .prologue
    .line 406
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mIsQuickIncDecBegins:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 419
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 490
    :cond_0
    :goto_0
    return v0

    .line 421
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->isInputWithEmptyListener()Z

    move-result v1

    if-nez v1, :cond_1

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->hideKeyboard()V

    .line 425
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 427
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 431
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mCurrentlyPressedView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mCurrentlyPressedView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v7

    .line 433
    goto :goto_0

    .line 436
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 437
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 438
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mIsQuickIncDecBegins:Z

    .line 439
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7$TimerTaskOne;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7$TimerTaskOne;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;Landroid/view/View;)V

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->LONGPRESS_TIMEOUT:I

    int-to-long v2, v2

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 440
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mCurrentlyPressedView:Landroid/view/View;

    move v0, v7

    .line 441
    goto :goto_0

    .line 443
    :cond_3
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mCurrentlyPressedView:Landroid/view/View;

    goto :goto_0

    .line 447
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 448
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 449
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {p1, v6}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 450
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v6, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_4

    .line 457
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 459
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 460
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mIsQuickIncDecBegins:Z

    if-nez v1, :cond_5

    .line 461
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->incrementDecrementValue(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;Landroid/view/View;)V

    .line 464
    :cond_5
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 465
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mIsQuickIncDecBegins:Z

    goto/16 :goto_0

    .line 472
    .end local v6    # "rect":Landroid/graphics/Rect;
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 474
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mCurrentlyPressedView:Landroid/view/View;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mCurrentlyPressedView:Landroid/view/View;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_7

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 480
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mIsQuickIncDecBegins:Z

    if-nez v1, :cond_8

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->incrementDecrementValue(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;Landroid/view/View;)V

    .line 485
    :cond_8
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 486
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$7;->mIsQuickIncDecBegins:Z

    goto/16 :goto_0

    .line 419
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
