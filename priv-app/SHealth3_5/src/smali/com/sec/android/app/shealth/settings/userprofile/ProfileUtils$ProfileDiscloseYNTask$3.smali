.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;
.super Ljava/lang/Object;
.source "ProfileUtils.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->getUserToken()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;)V
    .locals 0

    .prologue
    .line 632
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->initConnectionManager()V

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mUserToken:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->access$102(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;Ljava/lang/String;)Ljava/lang/String;

    .line 639
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "requestAccessTokenForOperation() mUserToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mUserToken:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mUserToken:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mUserToken:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1y90e30264"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->setProfileDiscloseYN(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :cond_0
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 646
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SamsungAccountUserTokenManager(): onFailed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    return-void
.end method

.method public setNetworkFailure()V
    .locals 2

    .prologue
    .line 651
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SamsungAccountUserTokenManager(): onNetworkFailure"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    return-void
.end method
