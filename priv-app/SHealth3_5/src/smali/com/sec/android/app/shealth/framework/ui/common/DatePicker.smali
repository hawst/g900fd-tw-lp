.class public Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;
.super Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;


# static fields
.field public static final DATE_PICKER_DIALOG:Ljava/lang/String; = "date_picker_dialog"


# instance fields
.field dateFormat:Ljava/text/DateFormat;

.field private mMinDate:J

.field numberPicker1:Landroid/view/ViewGroup;

.field numberPicker2:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/widget/Button;)V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;-><init>(Landroid/widget/Button;)V

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->mMinDate:J

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->mMinDate:J

    return-void
.end method

.method public constructor <init>(Landroid/widget/Button;J)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;-><init>(Landroid/widget/Button;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->mMinDate:J

    iput-wide p2, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->mMinDate:J

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;)J
    .locals 2

    iget-wide v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->mMinDate:J

    return-wide v0
.end method


# virtual methods
.method protected getDialogTag()Ljava/lang/String;
    .locals 1

    const-string v0, "date_picker_dialog"

    return-object v0
.end method

.method protected getSimpleDateFormatter()Ljava/text/DateFormat;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->dateFormat:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->dateFormat:Ljava/text/DateFormat;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->dateFormat:Ljava/text/DateFormat;

    return-object v0
.end method

.method protected initDialog()V
    .locals 4

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->context:Landroid/content/Context;

    const/4 v2, 0x2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->set_date:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->set:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->datepicker_shealth:I

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRefreshFocusables(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-void
.end method
