.class public Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ReminderNotificationReceiver.java"


# static fields
.field public static final ACTION_REMINDER_AUTOBACKUP_NOTIFICATION:Ljava/lang/String; = "com.sec.shealth.intent.action.AUTOBACKUP_OFF"

.field public static final ACTION_REMINDER_SAMSUNGACCOUNT_NOTIFICATION:Ljava/lang/String; = "com.sec.shealth.intent.action.SAMSUNG_ACCOUNT_OFF"

.field public static final REQUEST_AUTOBACKUP_NOTIFICATION_ID:I = 0x65

.field public static final REQUEST_SAMSUNGACCOUNT_NOTIFICATION_ID:I = 0x66

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final SAM_ACC_SIGNIN_ACTION:Ljava/lang/String;

.field private final SAM_ACC_SIGNOUT_ACTION:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 23
    const-string v0, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->SAM_ACC_SIGNIN_ACTION:Ljava/lang/String;

    .line 24
    const-string v0, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->SAM_ACC_SIGNOUT_ACTION:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    if-nez p2, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.shealth.intent.action.AUTOBACKUP_OFF"

    if-ne v7, v8, :cond_3

    .line 36
    sget-object v7, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->TAG:Ljava/lang/String;

    const-string v8, "ACTION_REMINDER_AUTOBACKUP_NOTIFICATION"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getReminderNotificationCount(Landroid/content/Context;)I

    move-result v3

    .line 38
    .local v3, "reminderCount":I
    add-int/lit8 v3, v3, 0x1

    .line 39
    sget-object v7, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Reminder Count "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    new-instance v4, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v4, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    .local v4, "resultIntent":Landroid/content/Intent;
    const-string v7, "launched_from_settings"

    const/4 v8, 0x1

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 43
    invoke-static {p1}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v6

    .line 45
    .local v6, "stackBuilder":Landroid/app/TaskStackBuilder;
    const-class v7, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v6, v7}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    .line 47
    invoke-virtual {v6, v4}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    .line 49
    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-virtual {v6, v7, v8}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v5

    .line 52
    .local v5, "resultPendingIntent":Landroid/app/PendingIntent;
    new-instance v7, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v7, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x7f02020b

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const v8, 0x7f09001d

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const v8, 0x7f090808

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 60
    .local v1, "mBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 61
    .local v0, "bigTextStyle":Landroid/support/v4/app/NotificationCompat$BigTextStyle;
    const v7, 0x7f090808

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    .line 62
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 63
    const-string/jumbo v7, "notification"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 64
    .local v2, "notifyMgr":Landroid/app/NotificationManager;
    const/16 v7, 0x65

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 65
    invoke-static {p1, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setReminderNotificationCount(Landroid/content/Context;I)V

    .line 67
    const/4 v7, 0x4

    if-ne v3, v7, :cond_2

    .line 70
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->reScheduleReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    .line 73
    :cond_2
    const/16 v7, 0x8

    if-ne v3, v7, :cond_0

    .line 76
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto/16 :goto_0

    .line 81
    .end local v0    # "bigTextStyle":Landroid/support/v4/app/NotificationCompat$BigTextStyle;
    .end local v1    # "mBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v2    # "notifyMgr":Landroid/app/NotificationManager;
    .end local v3    # "reminderCount":I
    .end local v4    # "resultIntent":Landroid/content/Intent;
    .end local v5    # "resultPendingIntent":Landroid/app/PendingIntent;
    .end local v6    # "stackBuilder":Landroid/app/TaskStackBuilder;
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.shealth.intent.action.SAMSUNG_ACCOUNT_OFF"

    if-ne v7, v8, :cond_5

    .line 83
    sget-object v7, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->TAG:Ljava/lang/String;

    const-string v8, "ACTION_REMINDER_SAMSUNGACCOUNT_NOTIFICATION"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-static {p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getReminderNotificationCount(Landroid/content/Context;)I

    move-result v3

    .line 85
    .restart local v3    # "reminderCount":I
    add-int/lit8 v3, v3, 0x1

    .line 86
    sget-object v7, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Reminder Count "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    new-instance v4, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-direct {v4, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    .restart local v4    # "resultIntent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v6

    .line 91
    .restart local v6    # "stackBuilder":Landroid/app/TaskStackBuilder;
    const-class v7, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v6, v7}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    .line 93
    invoke-virtual {v6, v4}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    .line 95
    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-virtual {v6, v7, v8}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v5

    .line 98
    .restart local v5    # "resultPendingIntent":Landroid/app/PendingIntent;
    new-instance v7, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v7, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x7f02020b

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const v8, 0x7f09001d

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const v8, 0x7f090809

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 106
    .restart local v1    # "mBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 107
    .restart local v0    # "bigTextStyle":Landroid/support/v4/app/NotificationCompat$BigTextStyle;
    const v7, 0x7f090809

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    .line 108
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 109
    const-string/jumbo v7, "notification"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 110
    .restart local v2    # "notifyMgr":Landroid/app/NotificationManager;
    const/16 v7, 0x66

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 111
    invoke-static {p1, v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setReminderNotificationCount(Landroid/content/Context;I)V

    .line 113
    const/4 v7, 0x4

    if-ne v3, v7, :cond_4

    .line 115
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->reScheduleReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    .line 118
    :cond_4
    const/16 v7, 0x8

    if-ne v3, v7, :cond_0

    .line 120
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto/16 :goto_0

    .line 124
    .end local v0    # "bigTextStyle":Landroid/support/v4/app/NotificationCompat$BigTextStyle;
    .end local v1    # "mBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v2    # "notifyMgr":Landroid/app/NotificationManager;
    .end local v3    # "reminderCount":I
    .end local v4    # "resultIntent":Landroid/content/Intent;
    .end local v5    # "resultPendingIntent":Landroid/app/PendingIntent;
    .end local v6    # "stackBuilder":Landroid/app/TaskStackBuilder;
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    if-ne v7, v8, :cond_6

    .line 125
    sget-object v7, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->TAG:Ljava/lang/String;

    const-string v8, "Logout from Samsung account"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationScheduled(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 128
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    const/16 v8, 0x65

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->dismissNotification(Landroid/content/Context;I)V

    .line 129
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto/16 :goto_0

    .line 133
    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    if-ne v7, v8, :cond_0

    .line 134
    sget-object v7, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationReceiver;->TAG:Ljava/lang/String;

    const-string v8, "Login into Samsung account"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationScheduled(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 137
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    const/16 v8, 0x66

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->dismissNotification(Landroid/content/Context;I)V

    .line 138
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto/16 :goto_0
.end method
