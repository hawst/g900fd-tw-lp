.class public Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;
.source "MealItemDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
        ">;",
        "Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "create_time"

    const-string v4, "_id"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 43
    if-nez p1, :cond_0

    .line 44
    const/4 v1, 0x0

    .line 52
    :goto_0
    return-object v1

    .line 46
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 47
    .local v0, "mealItem":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string v1, "amount"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 48
    const-string v1, "food_info__id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 49
    const-string/jumbo v1, "meal__id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getMealId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 50
    const-string/jumbo v1, "unit"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 52
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .locals 16
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 57
    new-instance v15, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 58
    .local v15, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    const-string v2, "_id"

    invoke-virtual {v15, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string/jumbo v4, "meal__id"

    invoke-virtual {v15, v4}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v6, "food_info__id"

    invoke-virtual {v15, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string v8, "amount"

    invoke-virtual {v15, v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v8

    const-string/jumbo v9, "unit"

    invoke-virtual {v15, v9}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v9

    const-string v10, "create_time"

    invoke-virtual {v15, v10}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    const-string/jumbo v12, "update_time"

    invoke-virtual {v15, v12}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    const-string/jumbo v14, "time_zone"

    invoke-virtual {v15, v14}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-direct/range {v1 .. v14}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;-><init>(JJJFIJJI)V

    .line 68
    .local v1, "mealItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    return-object v1
.end method

.method public getMealItemsListByMealId(J)Ljava/util/List;
    .locals 4
    .param p1, "mealId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v1, "meal__id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "="

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
