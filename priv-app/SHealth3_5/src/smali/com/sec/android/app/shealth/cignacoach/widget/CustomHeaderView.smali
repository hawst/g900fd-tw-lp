.class public Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;
.super Landroid/widget/RelativeLayout;
.source "CustomHeaderView.java"


# instance fields
.field private mBackgroundImage:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->init()V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->init()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->init()V

    .line 32
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030041

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    const v0, 0x7f080159

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->mBackgroundImage:Landroid/widget/ImageView;

    .line 38
    return-void
.end method


# virtual methods
.method public setBackgoundImage(I)V
    .locals 1
    .param p1, "backgroundResID"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->mBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 42
    return-void
.end method

.method public setBackgoundImage(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "backgroundDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomHeaderView;->mBackgroundImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 46
    return-void
.end method
