.class public interface abstract Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$SharedPreferencesKeys;
.super Ljava/lang/Object;
.source "SharedPreferencesHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SharedPreferencesKeys"
.end annotation


# static fields
.field public static final ACTIVITY_TYPE:Ljava/lang/String; = "activity_type"

.field public static final ANT_DEVICE_TYPE_AVAILABLE:Ljava/lang/String; = "ant_device_type_available"

.field public static final AUTO_BACKUP_INTERVAL:Ljava/lang/String; = "auto_backup_interval"

.field public static final AUTO_BACKUP_SWITCH_STATUS:Ljava/lang/String; = "auto_backup_switch_status"

.field public static final AUTO_BACKUP_WIFI_ENABLE:Ljava/lang/String; = "auto_backup_wifi_enable"

.field public static final BACKUP_BEFORE_RESTORING:Ljava/lang/String; = "backup_before_restoring"

.field public static final BP_DEVICE_TYPE_AVAILABLE:Ljava/lang/String; = " bp_device_type_available"

.field public static final COACH_RESTORE_TRIGGER:Ljava/lang/String; = "coach_restore_trigger"

.field public static final DAY_OF_WEEK_MASK:Ljava/lang/String; = "day_of_week_mask"

.field public static final DEVICE_TYPE_AVAILABLE_CHECKED:Ljava/lang/String; = "device_type_available_checked"

.field public static final DO_NOT_SHOW_HOME_POPUP:Ljava/lang/String; = "do_not_show_home_popup"

.field public static final END_TIME_OF_PERIOD:Ljava/lang/String; = "end_time_of_period"

.field public static final EXPORT_DIALOG:Ljava/lang/String; = "EXPORT_DIALOG"

.field public static final FIFTH_AGREEMENT_ACCEPTED:Ljava/lang/String; = "fifth_agreement_accepted"

.field public static final FIRSTBEAT_TRAINING_GOAL:Ljava/lang/String; = "firstbeat_training_goal"

.field public static final FIRST_ACTIVITY:Ljava/lang/String; = "first_activity"

.field public static final FIRST_AGREEMENT_ACCEPTED:Ljava/lang/String; = "first_agreement_accepted"

.field public static final FIXED_TIME_ZONE_ID:Ljava/lang/String; = "fixed_time_zone_id"

.field public static final FOURTH_AGREEMENT_ACCEPTED:Ljava/lang/String; = "fourth_agreement_accepted"

.field public static final HOME_PAGE_MARK:Ljava/lang/String; = "home_page_mark"

.field public static final HOME_WALLPAPER_ID:Ljava/lang/String; = "home_wallpaper_id"

.field public static final HR_DEVICE_TYPE_AVAILABLE:Ljava/lang/String; = " hr_device_type_available"

.field public static final HUMIDITY_MAXIMAL:Ljava/lang/String; = "humidity_maximal"

.field public static final HUMIDITY_MINIMAL:Ljava/lang/String; = "humidity_minimal"

.field public static final INITIALIZATION_COMFORT_ZONE:Ljava/lang/String; = "initialization_comfort_zone"

.field public static final INITIALIZATION_HOME_NEEDED:Ljava/lang/String; = "initialization_home_needed"

.field public static final INITIALIZATION_NEEDED:Ljava/lang/String; = "initialization_needed"

.field public static final IS_UPGRADE_FROM_35X:Ljava/lang/String; = "is_upgrade_from_35x"

.field public static final LAST_HUMIDITY:Ljava/lang/String; = "last_humidity"

.field public static final LAST_TEMPERATURE:Ljava/lang/String; = "last_temperature"

.field public static final LAST_UPDATED:Ljava/lang/String; = "last_updated"

.field public static final LAUNCH_MOBILE_POPUP_WAS_SHOWN:Ljava/lang/String; = "launch_mobile_popup_was_shown"

.field public static final LAUNCH_WIFI_POPUP_WAS_SHOWN:Ljava/lang/String; = "launch_wifi_popup_was_shown"

.field public static final LICENSE_ACCEPTED:Ljava/lang/String; = "license_accepted"

.field public static final MANUALY_MAX_HR_MODE:Ljava/lang/String; = "manualy_max_hr"

.field public static final MANUAL_MOBILE_POPUP_WAS_SHOWN:Ljava/lang/String; = "manual_mobile_popup_was_shown"

.field public static final MANUAL_ROAMING_POPUP_WAS_SHOWN:Ljava/lang/String; = "manual_roaming_popup_was_shown"

.field public static final MAX_HR_MODE:Ljava/lang/String; = "max_hr_mode"

.field public static final NOTI_ACHIEVEMENT_ENABLED:Ljava/lang/String; = "noti_achievement_enabled"

.field public static final NOTI_CHALLENGE_ENABLED:Ljava/lang/String; = "noti_challenge_enabled"

.field public static final NOTI_CIGNA_ENABLED:Ljava/lang/String; = "noti_cigna_enabled"

.field public static final NO_DATA_1:Ljava/lang/String; = "no_data_1"

.field public static final NO_DATA_2:Ljava/lang/String; = "no_data_2"

.field public static final PEDO_DEVICE_TYPE_AVAILABLE:Ljava/lang/String; = "pedo_device_type_available"

.field public static final PIN:Ljava/lang/String; = "pin"

.field public static final PREF_VERSION:Ljava/lang/String; = "PREF_VERSION"

.field public static final PRIVACY_POLICY:Ljava/lang/String; = "privacy_policy"

.field public static final PRIVACY_SET_VALUE:Ljava/lang/String; = "PRIVACY_SET_VALUE"

.field public static final PRIVACY_VERSION_CODE:Ljava/lang/String; = "privacy_version_code"

.field public static final REMINDER_NOTIFICATION_COUNT:Ljava/lang/String; = "reminder_notification_count"

.field public static final RESTORE_AGAIN_AT_HOME:Ljava/lang/String; = "restore_again_at_home"

.field public static final RESTORE_ONGOING:Ljava/lang/String; = "restore_ongoing"

.field public static final SAMSUNGACCOUNT_LAST_SYNCED_TIME:Ljava/lang/String; = "samsungaccount_last_synced_time"

.field public static final SECOND_AGREEMENT_ACCEPTED:Ljava/lang/String; = "second_agreement_accepted"

.field public static final SEND_PROFILE_DISCLOSE:Ljava/lang/String; = "send_profile_disclose"

.field public static final SPO2_DEVICE_TYPE_AVAILABLE:Ljava/lang/String; = " spo2_device_type_available"

.field public static final START_TIME_OF_PERIOD:Ljava/lang/String; = "start_time_of_period"

.field public static final TEMPERATURE_MAXIMAL:Ljava/lang/String; = "temperature_maximal"

.field public static final TEMPERATURE_MINIMAL:Ljava/lang/String; = "temperature_minimal"

.field public static final TERMS_OF_USE_VERSION_CODE:Ljava/lang/String; = "term_of_use_version_code"

.field public static final TGH_DEVICE_TYPE_AVAILABLE:Ljava/lang/String; = " tgh_device_type_available"

.field public static final THIRD_AGREEMENT_ACCEPTED:Ljava/lang/String; = "third_agreement_accepted"

.field public static final TIME_ZONE_FIXED:Ljava/lang/String; = "time_zone_fixed"

.field public static final UNSUPPORTED_FEATURE_LIST:Ljava/lang/String; = "unsupported_feature_list"

.field public static final UPDATE_LATER_BUTTON_CLICKED:Ljava/lang/String; = "update_later_button_clicked"

.field public static final UPGRADE_STATUS:Ljava/lang/String; = "upgrade_status"

.field public static final USE_PEDOMETER_RANKING:Ljava/lang/String; = "use_pedometer_ranking"

.field public static final UV_DEVICE_TYPE_AVAILABLE:Ljava/lang/String; = " uv_device_type_available"
