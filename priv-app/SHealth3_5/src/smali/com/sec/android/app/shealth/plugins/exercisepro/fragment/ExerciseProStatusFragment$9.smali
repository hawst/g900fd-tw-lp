.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;
.super Ljava/lang/Object;
.source "ExerciseProStatusFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V
    .locals 0

    .prologue
    .line 1354
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityTypeChanged(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1443
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1453
    :goto_0
    return-void

    .line 1446
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onChangedWorkoutStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 1488
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1498
    :goto_0
    return-void

    .line 1491
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$7;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onChannelStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "uid"    # Ljava/lang/String;

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1424
    :goto_0
    return-void

    .line 1374
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1375
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1397
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onMaxDurationReached(J)V
    .locals 2
    .param p1, "second"    # J

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1484
    :goto_0
    return-void

    .line 1482
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1483
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    const/16 v1, 0x7d0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setWorkoutModeState(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;I)V

    goto :goto_0
.end method

.method public onNewLocation(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1475
    :goto_0
    return-void

    .line 1460
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onScreenLockMode(Z)V
    .locals 2
    .param p1, "locked"    # Z

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1528
    :goto_0
    return-void

    .line 1521
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$9;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;Z)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onShownUnlockProgress(Z)V
    .locals 1
    .param p1, "isShown"    # Z

    .prologue
    .line 1551
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1554
    :cond_0
    return-void
.end method

.method public onStartWorkOut()V
    .locals 2

    .prologue
    .line 1532
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1547
    :goto_0
    return-void

    .line 1536
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mCanHideHRMData:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;Z)Z

    .line 1537
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mMapView:Lcom/google/android/gms/maps/MapView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/google/android/gms/maps/MapView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setUpMapIfNeeded(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$4000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;Landroid/view/View;)V

    .line 1538
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1539
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getHRMStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1540
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispHRMData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    .line 1546
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->FroceRecreateActionbar()V

    goto :goto_0

    .line 1542
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mCanHideHRMData:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1543
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->hideHRMData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    goto :goto_1
.end method

.method public onUpdateGoalFromWorkoutController(II)V
    .locals 2
    .param p1, "goalType"    # I
    .param p2, "value"    # I

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1567
    :goto_0
    return-void

    .line 1561
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$10;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onUpdateLapClock(J)V
    .locals 2
    .param p1, "second"    # J

    .prologue
    .line 1428
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1439
    :goto_0
    return-void

    .line 1431
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;J)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onUpdateVaule(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1367
    :goto_0
    return-void

    .line 1361
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onVisualGuideShow(Z)V
    .locals 2
    .param p1, "isShow"    # Z

    .prologue
    .line 1501
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1514
    :cond_0
    :goto_0
    return-void

    .line 1504
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->isVisualGuideLayoutVisible:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1505
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->isVisualGuideLayoutVisible:Z
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3602(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;Z)Z

    .line 1507
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$9;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
