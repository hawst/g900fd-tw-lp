.class Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;
.super Landroid/os/AsyncTask;
.source "SHealthApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/SHealthApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateUserProfileInCoachTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 525
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/SHealthApplication$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/SHealthApplication$1;

    .prologue
    .line 525
    invoke-direct {p0}, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 525
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/SHealthApplication$UpdateUserProfileInCoachTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 529
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 530
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->updateUserProfile(Landroid/content/Context;)V

    .line 532
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
