.class Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

.field final synthetic val$datePicker:Landroid/widget/DatePicker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;Landroid/widget/DatePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->val$datePicker:Landroid/widget/DatePicker;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->val$datePicker:Landroid/widget/DatePicker;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->calendar:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->calendar:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->calendar:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/DatePicker;->updateDate(III)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->mMinDate:J
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->access$000(Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->val$datePicker:Landroid/widget/DatePicker;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->mMinDate:J
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;->access$000(Lcom/sec/android/app/shealth/framework/ui/common/DatePicker;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/DatePicker;->setMinDate(J)V

    :cond_0
    return-void
.end method
