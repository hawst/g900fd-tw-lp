.class Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;
.super Ljava/lang/Object;
.source "StressCircularProgressBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getProgress()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    # getter for: Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mEndRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->access$000(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getProgress()F

    move-result v1

    const v2, 0x3ba3d70a    # 0.005f

    add-float/2addr v1, v2

    # setter for: Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->access$102(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;F)F

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    # getter for: Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->access$100(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    # getter for: Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mEndRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->access$000(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    # getter for: Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mEndRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->access$000(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->access$102(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;F)F

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    # getter for: Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->access$100(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgress(F)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->invalidate()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;->this$0:Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 123
    :cond_1
    return-void
.end method
