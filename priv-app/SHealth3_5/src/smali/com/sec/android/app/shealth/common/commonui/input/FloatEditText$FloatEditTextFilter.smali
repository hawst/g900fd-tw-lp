.class Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;
.super Ljava/lang/Object;
.source "FloatEditText.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FloatEditTextFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$1;

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;-><init>(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "destination"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 276
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4, v6, p5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    invoke-interface {p4, p6, v3}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$200(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "newString":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mFilterPattern:Ljava/util/regex/Pattern;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$300(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 284
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mSystemNumberSeparator:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$200(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/lang/String;

    move-result-object v2

    .line 305
    :goto_0
    return-object v2

    .line 287
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 289
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mOnSpecialInputListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$400(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mFilterPattern:Ljava/util/regex/Pattern;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$300(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_2

    .line 291
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$FloatEditTextFilter;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->mOnSpecialInputListener:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->access$400(Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnSpecialInputListener;->onSpecialInput()V

    .line 295
    :cond_2
    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "array":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 298
    aget-object v2, v0, v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_3

    .line 300
    new-instance p4, Landroid/text/SpannableString;

    .end local p4    # "destination":Landroid/text/Spanned;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v5

    invoke-virtual {v3, v6, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p4, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 303
    .restart local p4    # "destination":Landroid/text/Spanned;
    :cond_3
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    if-gt p5, v2, :cond_4

    .line 304
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    invoke-interface {p4, p5, v2}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    .line 305
    :cond_4
    const-string v2, ""

    goto :goto_0
.end method
