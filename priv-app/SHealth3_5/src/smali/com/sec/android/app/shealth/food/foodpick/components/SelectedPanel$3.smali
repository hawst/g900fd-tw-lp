.class Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;
.super Ljava/lang/Object;
.source "SelectedPanel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->createAndInitTextView(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 205
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$200(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->DELETE_SELECTED_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 206
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 207
    .local v1, "uniqueId":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mMealItemDataHolder:Ljava/util/Map;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$000(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .line 208
    .local v0, "selectedPanelHolder":Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->removeItem(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$300(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;Ljava/lang/String;)V

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemRemovedListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$400(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->mOnSelectedItemRemovedListener:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->access$400(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getId()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getCategory()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel$OnSelectedItemRemovedListener;->onSelectedItemRemoved(JLcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;)V

    .line 213
    :cond_0
    return-void
.end method
