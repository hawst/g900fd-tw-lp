.class public Lcom/sec/android/app/shealth/widget/DropDownListButton;
.super Landroid/widget/FrameLayout;
.source "DropDownListButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;,
        Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;
    }
.end annotation


# static fields
.field private static final SHOW_DROWDOWN_SIZE:I = 0x5


# instance fields
.field private final GAP:I

.field private mContext:Landroid/content/Context;

.field private mDropDownButtonBottomRightImage:Landroid/widget/ImageView;

.field private mDropDownButtonText:Ljava/lang/CharSequence;

.field private mDropDownButtonTextColor:I

.field private mDropDownButtonTextFont:Ljava/lang/CharSequence;

.field private mDropDownButtonTextSize:F

.field private mDropDownButtonTextView:Landroid/widget/TextView;

.field private mDropDownList:Landroid/widget/ListView;

.field private mDropDownListHeight:I

.field private mDropDownListItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDropDownListWidth:I

.field private mDropDownPopup:Landroid/widget/PopupWindow;

.field private mOnDropDownItemClickListener:Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0523

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->GAP:I

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0524

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListWidth:I

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0525

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListHeight:I

    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0523

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->GAP:I

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0524

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListWidth:I

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0525

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListHeight:I

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0523

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->GAP:I

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0524

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListWidth:I

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0525

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListHeight:I

    .line 82
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mOnDropDownItemClickListener:Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/widget/DropDownListButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/widget/DropDownListButton;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextSize:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/widget/DropDownListButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextColor:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextFont:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/widget/DropDownListButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/widget/DropDownListButton;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->showDropDownList()V

    return-void
.end method

.method private getViewHeight()I
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getHeight()I

    move-result v0

    return v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 86
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;

    .line 88
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    .line 89
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 90
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 96
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonBottomRightImage:Landroid/widget/ImageView;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonBottomRightImage:Landroid/widget/ImageView;

    const v2, 0x7f02042f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 98
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .end local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v1, 0x55

    invoke-direct {v0, v4, v4, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 99
    .restart local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonBottomRightImage:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonBottomRightImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonBottomRightImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 103
    if-eqz p2, :cond_0

    .line 104
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->setAttr(Landroid/util/AttributeSet;)V

    .line 107
    :cond_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->setClickable(Z)V

    .line 108
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->setFocusable(Z)V

    .line 109
    return-void
.end method

.method private setAttr(Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/android/app/shealth/R$styleable;->DropDownListButton:[I

    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 114
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonText:Ljava/lang/CharSequence;

    .line 115
    const/4 v2, 0x1

    const/high16 v3, 0x41d80000    # 27.0f

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextSize:F

    .line 116
    const/4 v2, 0x2

    const v3, 0x106000b

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextColor:I

    .line 117
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextFont:Ljava/lang/CharSequence;

    .line 119
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 121
    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonText:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextSize:F

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextFont:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FontUtil;->createFont(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 125
    .local v1, "typeface":Landroid/graphics/Typeface;
    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 126
    return-void
.end method

.method private showDropDownList()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    if-nez v3, :cond_0

    .line 146
    new-instance v3, Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    .line 147
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 148
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    new-instance v4, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;-><init>(Lcom/sec/android/app/shealth/widget/DropDownListButton;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    const v4, 0x7f02042a

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    new-instance v4, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton$1;-><init>(Lcom/sec/android/app/shealth/widget/DropDownListButton;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 172
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;

    if-nez v3, :cond_1

    .line 173
    new-instance v3, Landroid/widget/PopupWindow;

    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    iget v5, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListWidth:I

    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getViewHeight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownList:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v7

    add-int/2addr v6, v7

    mul-int/lit8 v6, v6, 0x5

    iget v7, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->GAP:I

    sub-int/2addr v6, v7

    invoke-direct {v3, v4, v5, v6, v9}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v8}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 179
    new-array v0, v10, [I

    .line 180
    .local v0, "location":[I
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getLocationOnScreen([I)V

    .line 181
    new-array v2, v10, [I

    .line 182
    .local v2, "parentlocation":[I
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getLocationOnScreen([I)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownPopup:Landroid/widget/PopupWindow;

    const/16 v4, 0x33

    aget v5, v0, v8

    aget v6, v0, v9

    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getViewHeight()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->GAP:I

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    invoke-virtual {v3, p0, v4, v5, v6}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 185
    .end local v0    # "location":[I
    .end local v2    # "parentlocation":[I
    :cond_2
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 237
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 238
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 239
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 240
    .local v1, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 241
    cmpl-float v2, v0, v3

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    cmpl-float v2, v1, v3

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 242
    new-instance v2, Lcom/sec/android/app/shealth/widget/DropDownListButton$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton$2;-><init>(Lcom/sec/android/app/shealth/widget/DropDownListButton;)V

    const-wide/16 v3, 0x64

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 252
    :cond_0
    return v5
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 260
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_1

    .line 262
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/widget/DropDownListButton$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/widget/DropDownListButton$3;-><init>(Lcom/sec/android/app/shealth/widget/DropDownListButton;)V

    const-wide/16 v1, 0x64

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 271
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public setDropDownItems(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "dropDownListItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;

    .line 142
    return-void
.end method

.method public setOnDropDownItemClickListener(Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mOnDropDownItemClickListener:Lcom/sec/android/app/shealth/widget/DropDownListButton$OnDropDownItemClickListener;

    .line 278
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    return-void
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 138
    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1, "size"    # F

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 134
    return-void
.end method

.method public setTypeface(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "fontPkg"    # Ljava/lang/String;
    .param p3, "fontPath"    # Ljava/lang/String;

    .prologue
    .line 286
    iget-object v5, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 289
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual {v2, p2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    .line 290
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 291
    .local v0, "assets":Landroid/content/res/AssetManager;
    invoke-static {v0, p3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v4

    .line 292
    .local v4, "typeface":Landroid/graphics/Typeface;
    if-eqz p1, :cond_0

    .line 293
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 300
    .end local v0    # "assets":Landroid/content/res/AssetManager;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "typeface":Landroid/graphics/Typeface;
    :goto_0
    return-void

    .line 295
    .restart local v0    # "assets":Landroid/content/res/AssetManager;
    .restart local v3    # "res":Landroid/content/res/Resources;
    .restart local v4    # "typeface":Landroid/graphics/Typeface;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 297
    .end local v0    # "assets":Landroid/content/res/AssetManager;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "typeface":Landroid/graphics/Typeface;
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
