.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;
.super Ljava/lang/Object;
.source "ImageGridView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->initView(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

.field final synthetic val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImageDeletedListener:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$300(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;->imageWasDeleted(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->mImagePathCollection:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$400(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$3;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->init()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$500(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)V

    .line 303
    return-void
.end method
