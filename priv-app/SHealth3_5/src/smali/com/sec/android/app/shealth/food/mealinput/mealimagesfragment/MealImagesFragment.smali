.class public Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;
.super Landroid/support/v4/app/Fragment;
.source "MealImagesFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;
    }
.end annotation


# static fields
.field private static final DEFAULT_FRAGMENT_MODE:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

.field private static final DEFAULT_TIME_ZONE:I = -0x1

.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final FROM_GALLERY:I = 0x1

.field private static final GALLERY_NOT_ENABLE_POPUP:Ljava/lang/String; = "gallery_not_enabled"

.field private static final IMAGE_CAPTURE_PATH_KEY:Ljava/lang/String; = "IMAGE_CAPTURE_PATH_KEY"

.field private static final IMAGE_CAPTURE_URI_KEY:Ljava/lang/String; = "IMAGE_CAPTURE_URI_KEY"

.field private static final IMAGE_MINE_TYPE:Ljava/lang/String; = "image/*"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MEAL_DELETED_IMAGES_KEY:Ljava/lang/String; = "deleted_meal_images"

.field private static final MEAL_IMAGES_KEY:Ljava/lang/String; = "meal_images"

.field private static final MULTI_PICK_EXTRA_TAG:Ljava/lang/String; = "multi-pick"

.field private static final PICK_MAX_ITEM_EXTRA_TAG:Ljava/lang/String; = "pick-max-item"

.field private static final SELECTED_ITEMS_EXTRA_DATAS:Ljava/lang/String; = "selectedItems"

.field private static final SUPPORTED_FILE_PREFIXES:[Ljava/lang/String;

.field private static final TAKE_PHOTO:I


# instance fields
.field private mAddPhotoLayout:Landroid/view/View;

.field private mDeletedImagesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation
.end field

.field private mGalleryButton:Landroid/view/View;

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

.field private mLoadingImagesCount:I

.field private mMealImagesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation
.end field

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field private newFilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;

    .line 74
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "file://"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->SUPPORTED_FILE_PREFIXES:[Ljava/lang/String;

    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->VIEW_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->DEFAULT_FRAGMENT_MODE:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 92
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    .line 499
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->canAddMoreImages()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutChanged()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutLoading()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I

    return v0
.end method

.method static synthetic access$1208(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I

    return v0
.end method

.method static synthetic access$1210(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->setMealImage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mGalleryButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->showMaxImagesToast()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->showGalleryDisabledPopup()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    return-object v0
.end method

.method private canAddMoreImages()Z
    .locals 2

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getImagesCount()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFileSupported(Ljava/lang/String;)Z
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 428
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->SUPPORTED_FILE_PREFIXES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v6, v0, v1

    .line 430
    .local v6, "prefix":Ljava/lang/String;
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 433
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 434
    .local v5, "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 435
    invoke-static {p1, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 437
    iget v2, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 438
    .local v2, "imageHeight":I
    iget v3, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 439
    .local v3, "imageWidth":I
    if-lez v2, :cond_0

    if-lez v3, :cond_0

    .line 441
    iput-boolean v9, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 442
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v10

    iput v10, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 443
    invoke-static {p1, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 444
    .local v7, "resultantBitmap":Landroid/graphics/Bitmap;
    if-eqz v7, :cond_0

    .line 446
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 452
    .end local v2    # "imageHeight":I
    .end local v3    # "imageWidth":I
    .end local v5    # "mBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v6    # "prefix":Ljava/lang/String;
    .end local v7    # "resultantBitmap":Landroid/graphics/Bitmap;
    :goto_1
    return v8

    .line 428
    .restart local v6    # "prefix":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v6    # "prefix":Ljava/lang/String;
    :cond_1
    move v8, v9

    .line 452
    goto :goto_1
.end method

.method private notifyActivityAboutChanged()V
    .locals 2

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;->onMealImagesQuantityChanged(I)V

    .line 267
    :cond_0
    return-void
.end method

.method private notifyActivityAboutLoading()V
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction$IMealImagesQuantityWatcher;->onMealImageLoading()V

    .line 273
    :cond_0
    return-void
.end method

.method private onGalleryActivityOkResult(Landroid/content/Intent;)V
    .locals 11
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x0

    .line 368
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 369
    .local v3, "inputImageCountByPath":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v0, "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 371
    const-string/jumbo v7, "selectedItems"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 372
    if-nez v0, :cond_0

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 374
    .restart local v0    # "arrayList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 378
    .local v6, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 379
    .local v5, "path":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 380
    sget-object v7, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;

    const-string v8, "File path is empty"

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 381
    :cond_1
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->isFileSupported(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 382
    sget-object v7, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->GALLERY_PATH:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 383
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->setMealImage(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->refreshLastTakenPhoto(Landroid/content/Context;)V

    goto :goto_0

    .line 386
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getPathToImage()Ljava/lang/String;

    move-result-object v4

    .line 387
    .local v4, "newFilePath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 388
    .local v2, "imageCount":I
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_3

    .line 389
    add-int/lit8 v2, v2, 0x1

    .line 390
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    :goto_1
    new-instance v7, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;

    invoke-direct {v7, p0, v5, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Ljava/lang/String;Ljava/lang/String;)V

    new-array v8, v10, [Ljava/lang/Void;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 392
    :cond_3
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 393
    add-int/lit8 v2, v2, 0x1

    .line 394
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    const-string v7, ".jpg"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 401
    .end local v2    # "imageCount":I
    .end local v4    # "newFilePath":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const v8, 0x7f090990

    invoke-static {v7, v8, v10}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 404
    .end local v5    # "path":Ljava/lang/String;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_5
    return-void
.end method

.method private onTakePhotoActivityOkResult(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 407
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 409
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 410
    .local v1, "photoFilePath":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 414
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 415
    .local v0, "mediaScanIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 419
    .end local v0    # "mediaScanIntent":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getPathToImage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    .line 420
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    invoke-direct {v2, p0, v1, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 424
    .end local v1    # "photoFilePath":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private setMealImage(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;)V
    .locals 5
    .param p1, "mealImageData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .prologue
    const/4 v4, 0x0

    .line 475
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;-><init>(Ljava/lang/String;)V

    .line 477
    .local v0, "imagePagerHolder":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setMealImage with path "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->addItem(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V

    .line 482
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutChanged()V

    .line 483
    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 484
    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    .line 486
    return-void
.end method

.method private setMealImage(Ljava/lang/String;)V
    .locals 11
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    const-wide/16 v1, -0x1

    .line 495
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const/4 v10, -0x1

    move-object v3, p1

    move-wide v4, v1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;-><init>(JLjava/lang/String;JJJI)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->setMealImage(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;)V

    .line 497
    return-void
.end method

.method private showGalleryDisabledPopup()V
    .locals 3

    .prologue
    .line 464
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0900d9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090936

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "gallery_not_enabled"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 468
    return-void
.end method

.method private showMaxImagesToast()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f090929

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/16 v3, 0x9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 461
    return-void
.end method


# virtual methods
.method public addAndResizeMealImage(Ljava/lang/String;)V
    .locals 3
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getPathToImage()Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "newFilePath":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 325
    return-void
.end method

.method public addListMealImages(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 299
    .local p1, "mealImageDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    if-nez p1, :cond_0

    .line 300
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "argument\'s mealImageDataList  is null, you must set not null argument"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 302
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 303
    .local v3, "imagePagerHolders":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 304
    .local v4, "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    new-instance v1, Ljava/io/File;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 305
    .local v1, "image":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 306
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;-><init>(Ljava/lang/String;)V

    .line 308
    .local v2, "imagePagerHolder":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    sget-object v5, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "setMealImage with path "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 314
    .end local v2    # "imagePagerHolder":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 317
    .end local v1    # "image":Ljava/io/File;
    .end local v4    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->addItems(Ljava/util/List;)V

    .line 318
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutChanged()V

    .line 319
    return-void
.end method

.method public deleteAllImages()V
    .locals 3

    .prologue
    .line 521
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    .line 522
    .local v0, "size":I
    if-lez v0, :cond_0

    .line 523
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->removeAllViews()V

    .line 524
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 525
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 527
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutChanged()V

    .line 528
    return-void
.end method

.method public getAllMealImages()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 330
    .local v2, "mealImageDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 331
    .local v1, "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 333
    .end local v1    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_0
    return-object v2
.end method

.method public getDeletedImagesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 354
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 355
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 356
    packed-switch p1, :pswitch_data_0

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 358
    :pswitch_0
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->onGalleryActivityOkResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 361
    :pswitch_1
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->onTakePhotoActivityOkResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 356
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 102
    const v4, 0x7f0300e4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 103
    .local v1, "fragmentView":Landroid/view/View;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    .line 104
    const v4, 0x7f0803df

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 105
    .local v0, "cameraButton":Landroid/view/View;
    const v4, 0x7f0803e1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mGalleryButton:Landroid/view/View;

    .line 106
    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mGalleryButton:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    const v4, 0x7f0803da

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    .line 155
    const v4, 0x7f0803dd

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mAddPhotoLayout:Landroid/view/View;

    .line 156
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    new-instance v5, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->setImageDeletedListener(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageDeletedListener;)V

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    new-instance v5, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$4;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$4;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->setOnGridItemListener(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;)V

    .line 187
    sget-object v4, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->DEFAULT_FRAGMENT_MODE:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 190
    if-eqz p3, :cond_3

    .line 191
    const-string v4, "deleted_meal_images"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 192
    const-string v4, "deleted_meal_images"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    .line 194
    :cond_0
    const-string/jumbo v4, "meal_images"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 195
    const-string/jumbo v4, "meal_images"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 196
    .local v2, "mealImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->addListMealImages(Ljava/util/List;)V

    .line 199
    .end local v2    # "mealImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    :cond_1
    const-string v4, "IMAGE_CAPTURE_URI_KEY"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "IMAGE_CAPTURE_URI_KEY"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 200
    const-string v4, "IMAGE_CAPTURE_URI_KEY"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 201
    .local v3, "uriString":Ljava/lang/String;
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    .line 204
    .end local v3    # "uriString":Ljava/lang/String;
    :cond_2
    const-string v4, "IMAGE_CAPTURE_PATH_KEY"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "IMAGE_CAPTURE_PATH_KEY"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 205
    const-string v4, "IMAGE_CAPTURE_PATH_KEY"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    .line 209
    :cond_3
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->destroyGridViewItems()V

    .line 347
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 348
    return-void
.end method

.method public onResume()V
    .locals 11

    .prologue
    .line 214
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v1, "deletedImagePagers":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;>;"
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    .line 217
    .local v5, "imagePagerHolder":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    iget-object v8, v5, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/app/shealth/common/utils/Utils;->isFileExist(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 218
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 222
    .end local v5    # "imagePagerHolder":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 224
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_2

    .line 226
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 230
    .end local v2    # "i":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    if-eqz v8, :cond_5

    .line 231
    new-instance v7, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 233
    .local v7, "mealImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    const/4 v4, 0x0

    .line 234
    .local v4, "imageAdded":Z
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    .line 235
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;->getFilePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 236
    const/4 v4, 0x1

    .line 240
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_4
    if-nez v4, :cond_5

    .line 241
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->onTakePhotoActivityOkResult(Landroid/content/Intent;)V

    .line 245
    .end local v4    # "imageAdded":Z
    .end local v7    # "mealImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->getImagesCount()I

    move-result v9

    if-eq v8, v9, :cond_6

    .line 246
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    new-array v6, v8, [Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    .line 247
    .local v6, "imagePagerHolders":[Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 248
    iget-object v8, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    new-instance v9, Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->setItems(Ljava/util/List;)V

    .line 249
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutChanged()V

    .line 251
    .end local v6    # "imagePagerHolders":[Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    :cond_6
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 278
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 279
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 280
    .local v1, "mealImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 281
    .local v0, "deletedImagesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;>;"
    const-string/jumbo v2, "meal_images"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 282
    const-string v2, "deleted_meal_images"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 284
    const-string v2, "IMAGE_CAPTURE_URI_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 287
    const-string v2, "IMAGE_CAPTURE_PATH_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->newFilePath:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_1
    return-void
.end method

.method public resetAllData()V
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mDeletedImagesList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->clear()V

    .line 341
    return-void
.end method

.method public setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V
    .locals 6
    .param p1, "fragmentMode"    # Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .prologue
    const/4 v0, 0x0

    .line 255
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    if-ne p1, v2, :cond_0

    const/4 v1, 0x1

    .line 256
    .local v1, "isEditElementsEnabled":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 257
    .local v0, "editImagesBlockVisibility":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mAddPhotoLayout:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mAddPhotoLayout:Landroid/view/View;

    const v3, 0x7f0803de

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901fd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mImageGridView:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->setEditableMode(Z)V

    .line 260
    return-void

    .end local v0    # "editImagesBlockVisibility":I
    .end local v1    # "isEditElementsEnabled":Z
    :cond_0
    move v1, v0

    .line 255
    goto :goto_0

    .line 256
    .restart local v1    # "isEditElementsEnabled":Z
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method
