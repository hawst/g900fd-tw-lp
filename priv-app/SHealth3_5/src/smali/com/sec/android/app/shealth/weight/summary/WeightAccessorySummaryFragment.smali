.class public Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;
.super Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;
.source "WeightAccessorySummaryFragment.java"


# instance fields
.field private mBMRTextView:Landroid/widget/TextView;

.field private mBMRUnitTextView:Landroid/widget/TextView;

.field private mBodyFatTextView:Landroid/widget/TextView;

.field private mBodyFatUnitTextView:Landroid/widget/TextView;

.field private mBodyYearTextView:Landroid/widget/TextView;

.field private mSkeletalMuscleTextView:Landroid/widget/TextView;

.field private mSkeletalMuscleUnitTextView:Landroid/widget/TextView;

.field private mVisceralFatTextView:Landroid/widget/TextView;

.field private mVisceralFatUnitTextView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;-><init>()V

    return-void
.end method

.method private getAgeValue()I
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 121
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 122
    .local v2, "rightNow":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 123
    .local v1, "birthDay":Ljava/util/Calendar;
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 124
    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    sub-int v0, v3, v4

    .line 125
    .local v0, "age":I
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 126
    add-int/lit8 v0, v0, -0x1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 127
    :cond_1
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 129
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private isDataValueDefault(F)Z
    .locals 1
    .param p1, "dataValue"    # F

    .prologue
    .line 117
    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateAccessoryValues()V
    .locals 6

    .prologue
    .line 81
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->getWeightData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    .line 82
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v0, :cond_4

    .line 83
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v1

    .line 84
    .local v1, "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "weightUnitName":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSkeletalMuscle()F

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->isDataValueDefault(F)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleUnitTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    .line 91
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyFat()F

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->isDataValueDefault(F)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatUnitTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    .line 97
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBasalMetabolicRate()F

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->isDataValueDefault(F)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRUnitTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    .line 102
    :goto_2
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getViscFat()F

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->isDataValueDefault(F)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 103
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatUnitTextView:Landroid/widget/ImageView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    .line 113
    .end local v1    # "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .end local v2    # "weightUnitName":Ljava/lang/String;
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyYearTextView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->getAgeValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void

    .line 88
    .restart local v1    # "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .restart local v2    # "weightUnitName":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSkeletalMuscle()F

    move-result v5

    invoke-static {v5, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->setViewInfoValue(Landroid/widget/TextView;Landroid/view/View;Ljava/lang/String;)V

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 94
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyFat()F

    move-result v5

    invoke-static {v5, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->setViewInfoValue(Landroid/widget/TextView;Landroid/view/View;Ljava/lang/String;)V

    .line 95
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 100
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBasalMetabolicRate()F

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->setViewInfoValue(Landroid/widget/TextView;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_2

    .line 105
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatUnitTextView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getViscFat()F

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->setViewInfoValue(Landroid/widget/TextView;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_3

    .line 108
    .end local v1    # "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .end local v2    # "weightUnitName":Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleUnitTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatUnitTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRUnitTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatUnitTextView:Landroid/widget/ImageView;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V

    goto :goto_3
.end method


# virtual methods
.method protected getContentViewId()I
    .locals 1

    .prologue
    .line 55
    const v0, 0x7f0302b8

    return v0
.end method

.method protected initializeValueTextViews(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->initializeValueTextViews(Landroid/view/View;)V

    .line 61
    const v0, 0x7f080c3e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleTextView:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f080c3f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mSkeletalMuscleUnitTextView:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f080c41

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatTextView:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f080c42

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyFatUnitTextView:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f080c50

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBodyYearTextView:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f080c53

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRTextView:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f080c54

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mBMRUnitTextView:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f080c56

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatTextView:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f080c57

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->mVisceralFatUnitTextView:Landroid/widget/ImageView;

    .line 70
    return-void
.end method

.method public updateMainView(Z)V
    .locals 0
    .param p1, "needAnimation"    # Z

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightSummaryFragment;->updateMainView(Z)V

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightAccessorySummaryFragment;->updateAccessoryValues()V

    .line 78
    return-void
.end method
