.class Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;
.super Ljava/lang/Object;
.source "SelectFavoriteActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->addSelectSpinner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "selectedItemView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-lez p3, :cond_0

    .line 144
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090071

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->setSelectAll()V

    .line 154
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    .line 156
    return-void

    .line 148
    :cond_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090073

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->setUnSelectAll()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
