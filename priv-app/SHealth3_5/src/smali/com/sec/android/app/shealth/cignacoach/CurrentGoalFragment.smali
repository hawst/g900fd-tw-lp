.class public Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
.super Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;
.source "CurrentGoalFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupCollapseListener;
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

.field private mCoachMessages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentExpandedGroupPosition:I

.field private mCurrentGoalCoachMessage:Landroid/widget/TextView;

.field private mCurrentGoalProgressMsg:Ljava/lang/String;

.field private mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

.field private mExpandableListView:Landroid/widget/ExpandableListView;

.field private mGoalDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mMissionDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOverallScore:I

.field private mToBeExpandGoalId:I

.field private mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;-><init>()V

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;

    .line 66
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    .line 68
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mToBeExpandGoalId:I

    .line 71
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    .line 227
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->startSuggestedGoalActivity()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->setAdapter()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(IZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->moreButtonAction(Landroid/view/View;I)V

    return-void
.end method

.method private collapseGroup(I)V
    .locals 5
    .param p1, "groupPosition"    # I

    .prologue
    const/4 v4, -0x1

    .line 638
    if-ne p1, v4, :cond_1

    .line 652
    :cond_0
    return-void

    .line 641
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v0

    .line 642
    .local v0, "adapterGroupCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 644
    if-ne p1, v1, :cond_2

    .line 645
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 647
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getCurrentGoalProgressMsg(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    .line 648
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setScoreCoachMessage(Ljava/lang/String;)V

    .line 649
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->updateNoOverallScoreHeader()V

    .line 642
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private expandGroup(I)V
    .locals 5
    .param p1, "groupPosition"    # I

    .prologue
    .line 656
    const/4 v3, -0x1

    if-ne p1, v3, :cond_1

    .line 669
    :cond_0
    return-void

    .line 659
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v0

    .line 660
    .local v0, "adapterGroupCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 661
    if-ne p1, v2, :cond_2

    .line 662
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, p1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 663
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    .line 664
    .local v1, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getId()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getCurrentGoalProgressMsg(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    .line 665
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setScoreCoachMessage(Ljava/lang/String;)V

    .line 666
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->updateNoOverallScoreHeader()V

    .line 660
    .end local v1    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private expandGroup(IZ)V
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "isRetain"    # Z

    .prologue
    .line 617
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 634
    :goto_0
    return-void

    .line 621
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;ZI)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private getGroupPosition(I)I
    .locals 3
    .param p1, "goalId"    # I

    .prologue
    const/4 v1, -0x1

    .line 601
    if-ne p1, v1, :cond_1

    .line 612
    :cond_0
    :goto_0
    return v1

    .line 604
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 605
    .local v0, "goalDataListSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 607
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getId()I

    move-result v2

    if-eq p1, v2, :cond_0

    .line 605
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 612
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getHeaderView()Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f030037

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 158
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f080132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    .line 159
    const v1, 0x7f080133

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalCoachMessage:Landroid/widget/TextView;

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->selectScoreViewMessageMode(Z)V

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v4, v4, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setSummaryHeaderMode(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;ZZZ)V

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 175
    return-object v0
.end method

.method private declared-synchronized getListData()V
    .locals 3

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 190
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    monitor-exit p0

    return-void

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private initHeader()V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method private moreButtonAction(Landroid/view/View;I)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "overallScore"    # I

    .prologue
    const v9, 0x7f090c6d

    const v8, 0x7f090c6c

    const v7, 0x7f090c69

    const v6, 0x7f090039

    const v5, 0x7f090037

    .line 364
    const/4 v2, 0x0

    .line 366
    .local v2, "items":[Ljava/lang/String;
    if-ltz p2, :cond_1

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 369
    const/4 v0, 0x0

    .local v0, "i":I
    const/16 v1, 0xa

    .line 370
    .local v1, "item_count":I
    new-array v2, v1, [Ljava/lang/String;

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 372
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090035

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 373
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090031

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 374
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 376
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090c66

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 377
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090033

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 378
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 379
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 380
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 381
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 421
    :goto_0
    return-void

    .line 385
    .end local v0    # "i":I
    .end local v1    # "item_count":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    const/4 v1, 0x6

    .line 386
    .restart local v1    # "item_count":I
    new-array v2, v1, [Ljava/lang/String;

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 389
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090c66

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 390
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 391
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 392
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 393
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 397
    .end local v0    # "i":I
    .end local v1    # "item_count":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 398
    const/4 v0, 0x0

    .restart local v0    # "i":I
    const/16 v1, 0x9

    .line 399
    .restart local v1    # "item_count":I
    new-array v2, v1, [Ljava/lang/String;

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 401
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090035

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 402
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090031

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 403
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 405
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090033

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 406
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 407
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 408
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 409
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 411
    .end local v0    # "i":I
    .end local v1    # "item_count":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    const/4 v1, 0x5

    .line 412
    .restart local v1    # "item_count":I
    new-array v2, v1, [Ljava/lang/String;

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 415
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 416
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 417
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 418
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0
.end method

.method private setAdapter()V
    .locals 5

    .prologue
    .line 199
    monitor-enter p0

    .line 200
    :try_start_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mMissionDataList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    .line 201
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->GOAL:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setMainType(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 204
    return-void

    .line 201
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private startCurrentMissionDetailActivity(Lcom/sec/android/app/shealth/cignacoach/data/MissionData;)V
    .locals 3
    .param p1, "missionData"    # Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .prologue
    .line 538
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 539
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 540
    const-string v1, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 541
    const-string v1, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 542
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 543
    return-void
.end method

.method private startSuggestedGoalActivity()V
    .locals 3

    .prologue
    .line 686
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 687
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 689
    return-void
.end method


# virtual methods
.method public getCurrentGoalProgressMsg(I)Ljava/lang/String;
    .locals 6
    .param p1, "expandGoalId"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 303
    if-ne p1, v4, :cond_0

    .line 304
    iput v4, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    .line 307
    :cond_0
    const/4 v1, 0x0

    .line 308
    .local v1, "coachMsg":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 310
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 311
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    if-ne v3, v4, :cond_2

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :cond_1
    :goto_0
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentGoalProgressMsg mCurrentExpandedGroupPosition: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    return-object v1

    .line 314
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 317
    :catch_0
    move-exception v2

    .line 318
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v2}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 320
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCoachMessages:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "coachMsg":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "coachMsg":Ljava/lang/String;
    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 547
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 549
    if-ne p2, v1, :cond_2

    .line 551
    const/16 v0, 0x258

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2bc

    if-ne p1, v0, :cond_1

    .line 553
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->updateList(I)V

    .line 556
    :cond_1
    if-eqz p3, :cond_2

    .line 557
    const-string v0, "intent_delete_all"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 558
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getCignaSummaryFragment()Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 562
    :cond_2
    return-void
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 5
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 517
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3, p3, p4}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 519
    .local v2, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    if-eqz v2, :cond_0

    .line 521
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->startCurrentMissionDetailActivity(Lcom/sec/android/app/shealth/cignacoach/data/MissionData;)V

    .line 534
    :goto_0
    const/4 v3, 0x0

    return v3

    .line 525
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    .line 527
    .local v0, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 528
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x24000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 529
    const-string v3, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getId()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 530
    const-string v3, "EXTRA_NAME_MISSION_FROM_WHERE"

    const/16 v4, 0x16

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 456
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 465
    :goto_0
    return-void

    .line 459
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->onChangeFragmentBtnClick()V

    goto :goto_0

    .line 456
    :pswitch_data_0
    .packed-switch 0x7f080124
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->setHasOptionsMenu(Z)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getListData()V

    .line 80
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 425
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 428
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 429
    instance-of v1, v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    if-eqz v1, :cond_0

    .line 430
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaBaseActivity;->isMenuShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 431
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mOverallScore:I

    if-ltz v1, :cond_2

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 433
    const v1, 0x7f100002

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    const v1, 0x7f100003

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 438
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 439
    const v1, 0x7f100004

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 441
    :cond_3
    const v1, 0x7f100005

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 86
    const v6, 0x7f030035

    invoke-virtual {p1, v6, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 88
    .local v3, "rootView":Landroid/view/View;
    const v6, 0x7f080124

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 91
    .local v0, "cignaMainChangeFragmentBtn":Landroid/widget/ImageButton;
    const v6, 0x7f080133

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalCoachMessage:Landroid/widget/TextView;

    .line 92
    const v6, 0x7f08012a

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ExpandableListView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    .line 93
    const v6, 0x7f08012b

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 94
    .local v1, "emptyView":Landroid/view/View;
    const v6, 0x7f080218

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 96
    .local v2, "noDataText":Landroid/widget/TextView;
    const v6, 0x7f080122

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 97
    .local v5, "setGoalBtn":Landroid/widget/LinearLayout;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090c8a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090334

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09020a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 98
    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    const v6, 0x7f09032e

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v6, v1}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 107
    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getHeaderView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ExpandableListView;->addHeaderView(Landroid/view/View;)V

    .line 112
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v4

    .line 116
    .local v4, "scores":Lcom/cigna/coach/apiobjects/Scores;
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getCurrentGoalProgressMsg(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    .line 117
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v8

    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Scores;->getOutOfMessage()Ljava/lang/String;

    move-result-object v9

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    if-nez v6, :cond_0

    const-string v6, ""

    :goto_0
    invoke-virtual {v7, v8, v10, v9, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setCurrentScoreInfo(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v6, p0}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 122
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v6, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 123
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v6, p0}, Landroid/widget/ExpandableListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 124
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v6, p0}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 125
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v6, p0}, Landroid/widget/ExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->initHeader()V

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->setAdapter()V

    .line 130
    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mOverallScore:I

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->invalidateOptionsMenu()V

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->updateNoOverallScoreHeader()V

    .line 136
    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mToBeExpandGoalId:I

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getGroupPosition(I)I

    move-result v6

    const/4 v7, 0x1

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(IZ)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->invalidateOptionsMenu()V

    .line 140
    return-object v3

    .line 117
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 146
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onDestroy()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->cancel(Z)Z

    .line 152
    :cond_0
    return-void
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 2
    .param p1, "elv"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v1, 0x0

    .line 470
    invoke-virtual {p1, v1}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    .line 471
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    if-ne v0, p3, :cond_1

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p3}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->collapseGroup(I)V

    .line 488
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 479
    :cond_0
    invoke-direct {p0, p3, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(IZ)V

    goto :goto_0

    .line 484
    :cond_1
    invoke-direct {p0, p3, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(IZ)V

    goto :goto_0
.end method

.method public onGroupCollapse(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 509
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    if-ne v0, p1, :cond_0

    .line 510
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    .line 512
    :cond_0
    return-void
.end method

.method public onGroupExpand(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 494
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    if-eq v0, p1, :cond_1

    .line 496
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 498
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->collapseGroup(I)V

    .line 501
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentExpandedGroupPosition:I

    .line 504
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setSelection(I)V

    .line 505
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "av":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/16 v7, 0x2bc

    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v3, 0x1

    .line 567
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    move-result v5

    if-nez v5, :cond_0

    .line 569
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 570
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "EXTRA_NAME_EXPAND_GROUP_POSITION"

    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 571
    const-string v4, "EXTRA_NAME_DELETE_TYPE"

    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 572
    invoke-virtual {p0, v2, v7}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 596
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return v3

    .line 576
    :cond_0
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    move-result v5

    if-ne v5, v3, :cond_3

    .line 578
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    move-result v1

    .line 579
    .local v1, "groupPosition":I
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionChild(J)I

    move-result v0

    .line 581
    .local v0, "childPosition":I
    if-eq v1, v6, :cond_1

    if-eq v0, v6, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v5, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->isEndPositionOfGourp(II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 583
    :cond_1
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->TAG:Ljava/lang/String;

    const-string v5, "New Mission Long Click~"

    invoke-static {v3, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 584
    goto :goto_0

    .line 587
    :cond_2
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 588
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "EXTRA_NAME_EXPAND_GROUP_POSITION"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 589
    const-string v4, "EXTRA_NAME_EXPAND_CHILD_POSITION"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 590
    const-string v4, "EXTRA_NAME_DELETE_TYPE"

    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 591
    invoke-virtual {p0, v2, v7}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .end local v0    # "childPosition":I
    .end local v1    # "groupPosition":I
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    move v3, v4

    .line 596
    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 180
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainBaseFragment;->onResume()V

    .line 182
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mToBeExpandGoalId:I

    .line 183
    return-void
.end method

.method public setActionBarNormalMode(I)V
    .locals 6
    .param p1, "overallScore"    # I

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    .line 334
    .local v2, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v2, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 338
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 339
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$3;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;I)V

    .line 352
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f02020d

    invoke-direct {v1, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 353
    .local v1, "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f090030

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 354
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method

.method public setToBeExpandGoalId(I)V
    .locals 0
    .param p1, "expandGoalId"    # I

    .prologue
    .line 672
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mToBeExpandGoalId:I

    .line 673
    return-void
.end method

.method public updateList()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 300
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->cancel(Z)Z

    .line 298
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mUpdateListTask:Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment$UpdateListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public updateList(I)V
    .locals 6
    .param p1, "expandGoalId"    # I

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 225
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getListData()V

    .line 217
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v0

    .line 219
    .local v0, "scores":Lcom/cigna/coach/apiobjects/Scores;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getCurrentGoalProgressMsg(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    .line 220
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Scores;->getOutOfMessage()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, ""

    :goto_1
    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setCurrentScoreInfo(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->setAdapter()V

    .line 224
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->getGroupPosition(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->expandGroup(IZ)V

    goto :goto_0

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    goto :goto_1
.end method

.method public updateNoOverallScoreHeader()V
    .locals 2

    .prologue
    .line 676
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mOverallScore:I

    if-ltz v0, :cond_0

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalCoachMessage:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 683
    :goto_0
    return-void

    .line 680
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalCoachMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 681
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalCoachMessage:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalFragment;->mCurrentGoalProgressMsg:Ljava/lang/String;

    goto :goto_1
.end method
