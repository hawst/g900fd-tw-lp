.class public Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "ExerciseMateInformationArea.java"


# static fields
.field public static mDayInMiliSecond:D


# instance fields
.field public final MONTHS:[Ljava/lang/String;

.field private date:Landroid/widget/TextView;

.field private exerciseUnit:Ljava/lang/String;

.field private firstInforvalue:Ljava/lang/String;

.field private firstTitle:Ljava/lang/String;

.field private firstUIValue:Ljava/lang/String;

.field private first_info:Landroid/widget/RelativeLayout;

.field private first_info_data:Landroid/widget/TextView;

.field private first_info_title:Landroid/widget/TextView;

.field private first_info_unit:Landroid/widget/TextView;

.field private informationAreaparent:Landroid/widget/LinearLayout;

.field private isFirstInfoValueSet:Z

.field private isSecondInfoValueSet:Z

.field private isThirdInfoValueSet:Z

.field private mLayoutBottomMargin:I

.field private mLayoutLeftMargin:I

.field private mLayoutRightMargin:I

.field private mLayoutTopMargin:I

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private oneDForm:Ljava/text/DecimalFormat;

.field params:Landroid/widget/LinearLayout$LayoutParams;

.field private secondInforAvgUint:Ljava/lang/String;

.field private secondInforvalue:Ljava/lang/String;

.field private second_info:Landroid/widget/RelativeLayout;

.field private second_info_avg:Landroid/widget/TextView;

.field private second_info_data:Landroid/widget/TextView;

.field private second_info_title:Landroid/widget/TextView;

.field private second_info_unit:Landroid/widget/TextView;

.field private secongTitle:Ljava/lang/String;

.field private secongUIValue:Ljava/lang/String;

.field private thirdInforvalue:Ljava/lang/String;

.field private thirdTitle:Ljava/lang/String;

.field private thirdUIValue:Ljava/lang/String;

.field private third_info:Landroid/widget/LinearLayout;

.field private third_info_data:Landroid/widget/TextView;

.field private third_info_title:Landroid/widget/TextView;

.field private third_info_unit:Landroid/widget/TextView;

.field private time_str:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 80
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->MONTHS:[Ljava/lang/String;

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isFirstInfoValueSet:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isThirdInfoValueSet:Z

    .line 69
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->oneDForm:Ljava/text/DecimalFormat;

    .line 70
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mLayoutLeftMargin:I

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mLayoutTopMargin:I

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mLayoutRightMargin:I

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mLayoutBottomMargin:I

    .line 76
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->params:Landroid/widget/LinearLayout$LayoutParams;

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->exerciseUnit:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->initLayout()V

    .line 84
    return-void
.end method

.method private getBubbleAreaUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->exerciseUnit:Ljava/lang/String;

    .line 452
    .local v0, "exerciseUnit":Ljava/lang/String;
    return-object v0
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 88
    const v0, 0x7f080357

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    .line 89
    const v0, 0x7f080358

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->date:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f08035a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info:Landroid/widget/RelativeLayout;

    .line 91
    const v0, 0x7f08035c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_data:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f08035b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_title:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f08035d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f08035e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info:Landroid/widget/RelativeLayout;

    .line 96
    const v0, 0x7f080363

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_avg:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f080360

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_data:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f08035f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_title:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f080362

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f080364

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info:Landroid/widget/LinearLayout;

    .line 102
    const v0, 0x7f080366

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_data:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f080365

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_title:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f080367

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_unit:Landroid/widget/TextView;

    .line 106
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 300
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300b7

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 8

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/high16 v5, 0x42280000    # 42.0f

    const/4 v4, 0x0

    .line 117
    const-string v0, ""

    .line 119
    .local v0, "tempContentDescription":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->date:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->date:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isFirstInfoValueSet:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isThirdInfoValueSet:Z

    if-eqz v1, :cond_4

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 149
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isFirstInfoValueSet:Z

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_data:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstTitle:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_title:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_title:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstUIValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 162
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    if-eqz v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforAvgUint:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_avg:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforAvgUint:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_avg:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforAvgUint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongUIValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_data:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongTitle:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_title:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isThirdInfoValueSet:Z

    if-eqz v1, :cond_2

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_data:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->thirdInforvalue:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->thirdTitle:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_title:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->thirdTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_title:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_unit:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->thirdUIValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 205
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstTitle:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongTitle:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->thirdTitle:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_data:Landroid/widget/TextView;

    invoke-virtual {v1, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_data:Landroid/widget/TextView;

    invoke-virtual {v1, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_data:Landroid/widget/TextView;

    invoke-virtual {v1, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 211
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 212
    return-void

    .line 127
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isFirstInfoValueSet:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    if-nez v1, :cond_7

    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isFirstInfoValueSet:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isThirdInfoValueSet:Z

    if-nez v1, :cond_7

    :cond_6
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    if-eqz v1, :cond_8

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isThirdInfoValueSet:Z

    if-eqz v1, :cond_8

    .line 128
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    goto/16 :goto_0

    .line 130
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    goto/16 :goto_0

    .line 156
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->first_info_title:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 176
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_avg:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongUIValue:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_avg:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_avg:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->bringToFront()V

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 186
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 199
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->third_info_title:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "realX"    # D
    .param p3, "periodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 216
    new-instance v0, Ljava/util/Date;

    double-to-long v1, p1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 217
    .local v0, "date":Ljava/util/Date;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v1, :cond_0

    .line 218
    sput-wide p1, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mDayInMiliSecond:D

    .line 223
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->time_str:Ljava/lang/String;

    .line 224
    return-void

    .line 220
    :cond_0
    const-wide/16 v1, 0x0

    sput-wide v1, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mDayInMiliSecond:D

    goto :goto_0
.end method

.method public setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 228
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isFirstInfoValueSet:Z

    .line 231
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstInforvalue:Ljava/lang/String;

    .line 232
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstUIValue:Ljava/lang/String;

    .line 233
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->firstTitle:Ljava/lang/String;

    .line 235
    :cond_0
    return-void
.end method

.method public setSecondHrInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 261
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 263
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforAvgUint:Ljava/lang/String;

    .line 265
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforvalue:Ljava/lang/String;

    .line 266
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongUIValue:Ljava/lang/String;

    .line 267
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongTitle:Ljava/lang/String;

    .line 269
    :cond_0
    return-void
.end method

.method public setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;

    .prologue
    .line 239
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    .line 242
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforAvgUint:Ljava/lang/String;

    .line 243
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforvalue:Ljava/lang/String;

    .line 244
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongUIValue:Ljava/lang/String;

    .line 246
    :cond_0
    return-void
.end method

.method public setSecondInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "avgunit"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "unit"    # Ljava/lang/String;

    .prologue
    .line 250
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->isSecondInfoValueSet:Z

    .line 253
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforAvgUint:Ljava/lang/String;

    .line 254
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secondInforvalue:Ljava/lang/String;

    .line 255
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->secongUIValue:Ljava/lang/String;

    .line 257
    :cond_0
    return-void
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 24
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 306
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    if-eqz p1, :cond_1e

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1e

    .line 308
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v14

    .line 309
    .local v14, "dateValue":Ljava/lang/String;
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setSelectedDateInChart(J)V

    .line 311
    const-string v2, "Handler Data"

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 314
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmpl-double v2, v2, v6

    if-ltz v2, :cond_1d

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_2

    .line 317
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f09006e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->oneDForm:Ljava/text/DecimalFormat;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getBubbleAreaUnit()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setSecondInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v2, v3, :cond_0

    .line 436
    const-wide/16 v2, 0x0

    sput-wide v2, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mDayInMiliSecond:D

    .line 441
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->refreshInformationAreaView()V

    .line 447
    .end local v14    # "dateValue":Ljava/lang/String;
    :cond_1
    :goto_2
    return-void

    .line 318
    .restart local v14    # "dateValue":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_3

    .line 319
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->oneDForm:Ljava/text/DecimalFormat;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getBubbleAreaUnit()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 323
    :cond_3
    const/4 v12, 0x0

    .line 324
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 325
    .local v4, "projection":[Ljava/lang/String;
    new-instance v20, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 326
    .local v20, "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    const-wide/16 v18, 0x0

    .line 327
    .local v18, "kcal":D
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start_time >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "start_time"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfHour(J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "exercise_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x4e23

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") GROUP BY ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "strftime(\"%d-%m-%Y\",([Exercise].[start_time]/1000),\'unixepoch\',\'localtime\')"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 332
    .local v5, "selectionClause":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    .end local v4    # "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, " SUM(ROUND(total_calorie)) AS Sum_Kcal"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "start_time"

    aput-object v3, v4, v2

    .line 339
    .restart local v4    # "projection":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const-string/jumbo v7, "start_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 341
    if-eqz v12, :cond_4

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_4

    .line 342
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 343
    const-wide/16 v2, 0x0

    const-string v6, "Sum_Kcal"

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    cmpg-double v2, v2, v6

    if-gez v2, :cond_4

    .line 344
    const-string/jumbo v2, "start_time"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 345
    .local v22, "time":J
    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfHour(J)J

    move-result-wide v6

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3, v6, v7}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v18, v0

    .line 349
    .end local v22    # "time":J
    :cond_4
    if-eqz v12, :cond_5

    .line 350
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 355
    :cond_5
    const/4 v13, 0x0

    .line 356
    .local v13, "cursor1":Landroid/database/Cursor;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " select exercise.exercise_info__id, exercise._id, exercise_activity.exercise__id from exercise_activity   LEFT JOIN exercise ON exercise._id=exercise_activity.exercise__id  where exercise.start_time >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfHour(J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and exercise.start_time <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfHour(J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 362
    .local v9, "selectionClause1":Ljava/lang/String;
    const/16 v17, 0x0

    .line 363
    .local v17, "isDifferent":Z
    const-wide/16 v15, 0x0

    .line 366
    .local v15, "exerciseType":J
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 367
    if-eqz v13, :cond_6

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_6

    .line 368
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 369
    const-string v2, "exercise_info__id"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 371
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    .line 373
    :goto_3
    invoke-interface {v13}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_6

    .line 374
    const-string v2, "exercise_info__id"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    cmp-long v2, v2, v15

    if-eqz v2, :cond_b

    .line 375
    const/16 v17, 0x1

    .line 386
    :cond_6
    if-eqz v13, :cond_7

    .line 387
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 391
    :cond_7
    const/16 v21, -0x1

    .line 392
    .local v21, "resId":I
    if-eqz v17, :cond_8

    const-wide/16 v15, 0x0

    .line 394
    :cond_8
    const-wide/16 v2, 0x4652

    cmp-long v2, v15, v2

    if-nez v2, :cond_d

    .line 395
    const v21, 0x7f0909d2

    .line 428
    :cond_9
    :goto_4
    const/4 v2, -0x1

    move/from16 v0, v21

    if-ne v0, v2, :cond_1c

    .line 429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->oneDForm:Ljava/text/DecimalFormat;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getBubbleAreaUnit()Ljava/lang/String;

    move-result-object v3

    const-string v6, " "

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->oneDForm:Ljava/text/DecimalFormat;

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getBubbleAreaUnit()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090939

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setSecondHrInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 349
    .end local v9    # "selectionClause1":Ljava/lang/String;
    .end local v13    # "cursor1":Landroid/database/Cursor;
    .end local v15    # "exerciseType":J
    .end local v17    # "isDifferent":Z
    .end local v21    # "resId":I
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_a

    .line 350
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v2

    .line 378
    .restart local v9    # "selectionClause1":Ljava/lang/String;
    .restart local v13    # "cursor1":Landroid/database/Cursor;
    .restart local v15    # "exerciseType":J
    .restart local v17    # "isDifferent":Z
    :cond_b
    :try_start_2
    const-string v2, "exercise_info__id"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 380
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_3

    .line 386
    :catchall_1
    move-exception v2

    if-eqz v13, :cond_c

    .line 387
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 396
    .restart local v21    # "resId":I
    :cond_d
    const-wide/16 v2, 0x4653

    cmp-long v2, v15, v2

    if-nez v2, :cond_e

    .line 397
    const v21, 0x7f0909d3

    goto :goto_4

    .line 398
    :cond_e
    const-wide/16 v2, 0x4654

    cmp-long v2, v15, v2

    if-nez v2, :cond_f

    .line 399
    const v21, 0x7f0909d0

    goto/16 :goto_4

    .line 400
    :cond_f
    const-wide/16 v2, 0x4655

    cmp-long v2, v15, v2

    if-nez v2, :cond_10

    .line 401
    const v21, 0x7f0909d1

    goto/16 :goto_4

    .line 402
    :cond_10
    const-wide/16 v2, 0x2eea

    cmp-long v2, v15, v2

    if-nez v2, :cond_11

    .line 403
    const v21, 0x7f090b0f

    goto/16 :goto_4

    .line 404
    :cond_11
    const-wide/16 v2, 0x2f08

    cmp-long v2, v15, v2

    if-nez v2, :cond_12

    .line 405
    const v21, 0x7f090b1e

    goto/16 :goto_4

    .line 406
    :cond_12
    const-wide/16 v2, 0x2efd

    cmp-long v2, v15, v2

    if-nez v2, :cond_13

    .line 407
    const v21, 0x7f090b18

    goto/16 :goto_4

    .line 408
    :cond_13
    const-wide/16 v2, 0x3a9e

    cmp-long v2, v15, v2

    if-nez v2, :cond_14

    .line 409
    const v21, 0x7f090b27

    goto/16 :goto_4

    .line 410
    :cond_14
    const-wide/16 v2, 0x2ee4

    cmp-long v2, v15, v2

    if-nez v2, :cond_15

    .line 411
    const v21, 0x7f090b0e

    goto/16 :goto_4

    .line 412
    :cond_15
    const-wide/16 v2, 0x3a9a

    cmp-long v2, v15, v2

    if-nez v2, :cond_16

    .line 413
    const v21, 0x7f090b1b

    goto/16 :goto_4

    .line 414
    :cond_16
    const-wide/16 v2, 0x3aa3

    cmp-long v2, v15, v2

    if-nez v2, :cond_17

    .line 415
    const v21, 0x7f090b30

    goto/16 :goto_4

    .line 416
    :cond_17
    const-wide/16 v2, 0x2f03

    cmp-long v2, v15, v2

    if-nez v2, :cond_18

    .line 417
    const v21, 0x7f090b1c

    goto/16 :goto_4

    .line 418
    :cond_18
    const-wide/16 v2, 0x3e9f

    cmp-long v2, v15, v2

    if-nez v2, :cond_19

    .line 419
    const v21, 0x7f090b31

    goto/16 :goto_4

    .line 420
    :cond_19
    const-wide/16 v2, 0x2f16

    cmp-long v2, v15, v2

    if-nez v2, :cond_1a

    .line 421
    const v21, 0x7f090b09

    goto/16 :goto_4

    .line 422
    :cond_1a
    const-wide/16 v2, 0x2f05

    cmp-long v2, v15, v2

    if-nez v2, :cond_1b

    .line 423
    const v21, 0x7f090b1d

    goto/16 :goto_4

    .line 424
    :cond_1b
    const-wide/16 v2, 0x2ee9

    cmp-long v2, v15, v2

    if-nez v2, :cond_9

    .line 425
    const v21, 0x7f090b01

    goto/16 :goto_4

    .line 431
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->oneDForm:Ljava/text/DecimalFormat;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getBubbleAreaUnit()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 439
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v9    # "selectionClause1":Ljava/lang/String;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v13    # "cursor1":Landroid/database/Cursor;
    .end local v15    # "exerciseType":J
    .end local v17    # "isDifferent":Z
    .end local v18    # "kcal":D
    .end local v20    # "mergeCal":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    .end local v21    # "resId":I
    :cond_1d
    const-string v2, "0"

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->getBubbleAreaUnit()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateInformationArea;->setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 443
    .end local v14    # "dateValue":Ljava/lang/String;
    :cond_1e
    if-nez p1, :cond_1

    .line 445
    const-string v2, "Handler Data"

    const-string v3, "NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
