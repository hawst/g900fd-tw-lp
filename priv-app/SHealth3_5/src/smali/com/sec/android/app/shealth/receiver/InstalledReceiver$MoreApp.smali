.class public Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;
.super Ljava/lang/Object;
.source "InstalledReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/receiver/InstalledReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoreApp"
.end annotation


# instance fields
.field categoryName:Ljava/lang/String;

.field categoryOrder:Ljava/lang/String;

.field iconURL:Ljava/lang/String;

.field packageName:Ljava/lang/String;

.field productID:Ljava/lang/String;

.field productName:Ljava/lang/String;

.field sellerName:Ljava/lang/String;

.field type:Ljava/lang/String;

.field versionCode:Ljava/lang/String;

.field versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryOrder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->categoryOrder:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->versionCode:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->versionName:Ljava/lang/String;

    return-object v0
.end method

.method public geticonURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->iconURL:Ljava/lang/String;

    return-object v0
.end method

.method public getpackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getproductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->productID:Ljava/lang/String;

    return-object v0
.end method

.method public getproductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->productName:Ljava/lang/String;

    return-object v0
.end method

.method public getsellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/receiver/InstalledReceiver$MoreApp;->sellerName:Ljava/lang/String;

    return-object v0
.end method
