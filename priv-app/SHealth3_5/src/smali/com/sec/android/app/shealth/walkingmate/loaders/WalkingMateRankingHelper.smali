.class public Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;
.super Ljava/lang/Object;
.source "WalkingMateRankingHelper.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final CLIENT_ID:Ljava/lang/String; = "client_id"

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final EMAIL_ID:Ljava/lang/String; = "email_id"

.field public static final PERIOD_TYPE:Ljava/lang/String; = "type"

.field private static samsungAccountInfoIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->samsungAccountInfoIntent:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clear()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->samsungAccountInfoIntent:Landroid/content/Intent;

    .line 48
    return-void
.end method

.method public static getSamsungAccountInfoIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->samsungAccountInfoIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public static isAvailable()Z
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->samsungAccountInfoIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setSamsungAccountInfoIntent(Landroid/content/Intent;)V
    .locals 0
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    sput-object p0, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->samsungAccountInfoIntent:Landroid/content/Intent;

    .line 40
    return-void
.end method
