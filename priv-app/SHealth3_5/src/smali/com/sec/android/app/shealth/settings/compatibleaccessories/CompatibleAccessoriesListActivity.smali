.class public Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "CompatibleAccessoriesListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;
    }
.end annotation


# static fields
.field public static UPDATE_ACCESSORY_FILE:Ljava/lang/String;


# instance fields
.field public final SEND_THREAD_INFOMATION:I

.field public TAG:Ljava/lang/String;

.field protected accessoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;",
            ">;"
        }
    .end annotation
.end field

.field private adapter:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;

.field private deviceListView:Landroid/widget/ListView;

.field deviceType:[I

.field public isANTavailable:Z

.field onDeviceListItemClickListener:Landroid/view/View$OnClickListener;

.field serviceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-string v0, "2000"

    sput-object v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->UPDATE_ACCESSORY_FILE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 55
    const-string v0, "CompatibleAccessoriesListActivity"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->TAG:Ljava/lang/String;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->isANTavailable:Z

    .line 59
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->SEND_THREAD_INFOMATION:I

    .line 305
    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->onDeviceListItemClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private executeQuery()V
    .locals 15

    .prologue
    const v14, 0x7f090e05

    const v13, 0x7f090dfd

    const v9, 0x7f02022c

    const/4 v4, 0x0

    const/4 v12, 0x1

    .line 108
    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->serviceType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->serviceType:I

    if-ne v0, v12, :cond_2

    .line 109
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x271d

    const-string v2, ""

    const-string v3, ""

    const-string v6, ""

    const/4 v7, -0x1

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x271d

    const v1, 0x7f090e06

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020014

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x271d

    const v1, 0x7f090e07

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020012

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x271d

    const v1, 0x7f090e09

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020016

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x271d

    const v1, 0x7f090e08

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020017

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x271d

    const v1, 0x7f090bbb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020015

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x271d

    const v1, 0x7f090e0a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020011

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2718

    const-string v2, ""

    const-string v3, ""

    const-string v6, ""

    const/4 v7, -0x1

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x2718

    const v1, 0x7f090e0b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020013

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->isANTavailable:Z

    if-eqz v0, :cond_1

    .line 121
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2718

    const v2, 0x7f090e18

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090e17

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020229

    const v5, 0x7f02000b

    const v6, 0x7f090e01

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2718

    const v2, 0x7f090e11

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090e10

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02022a

    const v5, 0x7f02000e

    const v6, 0x7f090dfe

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2718

    const v2, 0x7f090e12

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090e10

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02022a

    const v5, 0x7f02000f

    const v6, 0x7f090dfe

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2718

    const v2, 0x7f090e14

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090e13

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02022d

    const v5, 0x7f020010

    const v6, 0x7f090dff

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2718

    const v2, 0x7f090e16

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090e15

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f02022e

    const v5, 0x7f020019

    const v6, 0x7f090e00

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_1
    :goto_0
    return-void

    .line 127
    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->serviceType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 128
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2712

    const-string v2, ""

    const-string v3, ""

    const-string v6, ""

    const/4 v7, -0x1

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x2712

    const v1, 0x7f090e0c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v14}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f020018

    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isEURModel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x2712

    const v1, 0x7f090e1a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v1, 0x7f090e19

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f02022b

    const v10, 0x7f02001a

    const v1, 0x7f090e02

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v6, 0x2712

    const v1, 0x7f090e1b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v1, 0x7f090e19

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f02022b

    const v10, 0x7f02001a

    const v1, 0x7f090e02

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v5 .. v12}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 138
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->serviceType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 139
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2714

    const-string v2, ""

    const-string v3, ""

    const-string v6, ""

    const/4 v7, -0x1

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    const/16 v1, 0x2714

    const v2, 0x7f090e0f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090e0e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020292

    const v5, 0x7f02000d

    const v6, 0x7f090e03

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, -0x1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>(ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private getTitleId()I
    .locals 2

    .prologue
    .line 179
    const/4 v0, -0x1

    .line 180
    .local v0, "titleResourceId":I
    iget v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->serviceType:I

    packed-switch v1, :pswitch_data_0

    .line 197
    const v0, 0x7f090021

    .line 200
    :goto_0
    return v0

    .line 182
    :pswitch_0
    const v0, 0x7f090cfb

    .line 183
    goto :goto_0

    .line 185
    :pswitch_1
    const v0, 0x7f090021

    .line 186
    goto :goto_0

    .line 188
    :pswitch_2
    const v0, 0x7f09002c

    .line 189
    goto :goto_0

    .line 191
    :pswitch_3
    const v0, 0x7f0901ae

    .line 192
    goto :goto_0

    .line 194
    :pswitch_4
    const v0, 0x7f0901b4

    .line 195
    goto :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 171
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getTitleId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 175
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 74
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "compatible_list_app_name"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->serviceType:I

    .line 75
    const-string v1, "compatible_list_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->deviceType:[I

    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    const v1, 0x7f0300a9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->setContentView(I)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->customizeActionBar()V

    .line 83
    const v1, 0x7f08032b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->deviceListView:Landroid/widget/ListView;

    .line 85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 87
    new-instance v1, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->accessoryList:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;-><init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->adapter:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->deviceListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->adapter:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity$DevListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAntAvailablilty(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->isANTavailable:Z

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->executeQuery()V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->refreshFocusables()V

    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getTitleId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0907e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoriesListActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 99
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 104
    return-void
.end method
