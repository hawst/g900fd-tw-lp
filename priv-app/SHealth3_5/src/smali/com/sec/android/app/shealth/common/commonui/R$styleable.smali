.class public final Lcom/sec/android/app/shealth/common/commonui/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CircleProgressView:[I

.field public static final CircleProgressView_progressImage:I = 0x0

.field public static final DragSortListView:[I

.field public static final DragSortListView_click_remove_id:I = 0x10

.field public static final DragSortListView_collapsed_height:I = 0x0

.field public static final DragSortListView_drag_enabled:I = 0xa

.field public static final DragSortListView_drag_handle_id:I = 0xe

.field public static final DragSortListView_drag_scroll_start:I = 0x1

.field public static final DragSortListView_drag_start_mode:I = 0xd

.field public static final DragSortListView_drop_animation_duration:I = 0x9

.field public static final DragSortListView_fling_handle_id:I = 0xf

.field public static final DragSortListView_float_alpha:I = 0x6

.field public static final DragSortListView_float_background_color:I = 0x3

.field public static final DragSortListView_max_drag_scroll_speed:I = 0x2

.field public static final DragSortListView_remove_animation_duration:I = 0x8

.field public static final DragSortListView_remove_enabled:I = 0xc

.field public static final DragSortListView_remove_mode:I = 0x4

.field public static final DragSortListView_slide_shuffle_speed:I = 0x7

.field public static final DragSortListView_sort_enabled:I = 0xb

.field public static final DragSortListView_track_drag_sort:I = 0x5

.field public static final DragSortListView_use_default_controller:I = 0x11

.field public static final EditTextWithLengthLimit:[I

.field public static final EditTextWithLengthLimit_idForToastText:I = 0x0

.field public static final HealthCareProgressBar:[I

.field public static final HealthCareProgressBar_isLeftAligned:I = 0x0

.field public static final InputModule:[I

.field public static final InputModule_drawing_strategy:I = 0x0

.field public static final SummaryButton:[I

.field public static final SummaryButton_btnIcon:I = 0x1

.field public static final SummaryButton_btnIconVisibility:I = 0x3

.field public static final SummaryButton_btnRightMargin:I = 0x2

.field public static final SummaryButton_btnTitleText:I = 0x0

.field public static final VerticalProgressbar:[I

.field public static final VerticalProgressbar_balance_background:I = 0x6

.field public static final VerticalProgressbar_layoutType:I = 0x7

.field public static final VerticalProgressbar_maxBalance:I = 0x3

.field public static final VerticalProgressbar_maxValue:I = 0x1

.field public static final VerticalProgressbar_minBalance:I = 0x2

.field public static final VerticalProgressbar_minValue:I = 0x0

.field public static final VerticalProgressbar_progressbar_background:I = 0x5

.field public static final VerticalProgressbar_value_labels_array:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1171
    new-array v0, v3, [I

    const v1, 0x7f01001c

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->CircleProgressView:[I

    .line 1173
    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->DragSortListView:[I

    .line 1192
    new-array v0, v3, [I

    const v1, 0x7f01001b

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->EditTextWithLengthLimit:[I

    .line 1194
    new-array v0, v3, [I

    const v1, 0x7f01000e

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->HealthCareProgressBar:[I

    .line 1196
    new-array v0, v3, [I

    const v1, 0x7f01001d

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->InputModule:[I

    .line 1198
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->SummaryButton:[I

    .line 1203
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->VerticalProgressbar:[I

    return-void

    .line 1173
    nop

    :array_0
    .array-data 4
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
    .end array-data

    .line 1198
    :array_1
    .array-data 4
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
    .end array-data

    .line 1203
    :array_2
    .array-data 4
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
