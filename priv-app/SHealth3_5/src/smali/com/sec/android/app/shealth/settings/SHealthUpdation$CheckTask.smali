.class Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;
.super Landroid/os/AsyncTask;
.source "SHealthUpdation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/SHealthUpdation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Lcom/sec/android/app/shealth/settings/SHealthUpdation$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/SHealthUpdation$1;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;-><init>(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v11, 0x0

    .line 96
    :try_start_0
    const-string v6, "SHealthUpdation"

    const-string/jumbo v7, "start update check"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 98
    .local v2, "model":Ljava/lang/String;
    const-string v6, "OMAP_SS"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 99
    # invokes: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->readModelCMCC()Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$100()Ljava/lang/String;

    move-result-object v2

    .line 102
    :cond_0
    const-string v6, "SAMSUNG-"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 103
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 107
    :cond_1
    const/4 v1, 0x0

    .line 110
    .local v1, "i":Landroid/content/pm/PackageInfo;
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$200(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$200(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 112
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    const-string v7, "%010d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCurVersionCode:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$302(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 121
    :try_start_2
    const-string v4, "http://hub.samsungapps.com/product/appCheck.as?"

    .line 122
    .local v4, "server_url":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "appInfo="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mAppContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$200(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "@"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 123
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&deviceId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 124
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # invokes: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->isPD()Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$400(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 125
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&mcc=000"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 129
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&mnc="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # invokes: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->getMNC()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$600(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 130
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&csc="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->getCSC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 131
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&openApi="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 133
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # invokes: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->isPD()Z
    invoke-static {v6}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$400(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 134
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&pd=1"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 139
    :goto_1
    const/4 v3, 0x0

    .line 141
    .local v3, "result":Z
    const-string v6, "SHealthUpdation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Update request] "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 144
    .local v5, "url":Ljava/net/URL;
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # invokes: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->checkUpdate(Ljava/net/URL;)Z
    invoke-static {v6, v5}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$700(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Ljava/net/URL;)Z

    move-result v3

    .line 146
    if-nez v3, :cond_4

    .line 147
    const-string v6, "SHealthUpdation"

    const-string v7, "No update~~~~~"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 166
    .end local v1    # "i":Landroid/content/pm/PackageInfo;
    .end local v2    # "model":Ljava/lang/String;
    .end local v3    # "result":Z
    .end local v4    # "server_url":Ljava/lang/String;
    .end local v5    # "url":Ljava/net/URL;
    :goto_2
    return-object v6

    .line 115
    .restart local v1    # "i":Landroid/content/pm/PackageInfo;
    .restart local v2    # "model":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "SHealthUpdation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_2

    .line 127
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "server_url":Ljava/lang/String;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&mcc="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # invokes: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->getMCC()Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$500(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 136
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&pd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 151
    .restart local v3    # "result":Z
    .restart local v5    # "url":Ljava/net/URL;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    const/4 v7, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUpdateNeeded:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$802(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Z)Z

    .line 153
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$900(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCurVersionCode:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$300(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x2

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    if-le v6, v7, :cond_5

    .line 155
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    const/4 v7, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mIsForceupdate:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1002(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Z)Z

    .line 157
    :cond_5
    const-string v6, "SHealthUpdation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Update needed from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCurVersionCode:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$300(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->versionCode:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$900(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is force updated : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mIsForceupdate:Z
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1000(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v6

    goto/16 :goto_2

    .line 162
    .end local v1    # "i":Landroid/content/pm/PackageInfo;
    .end local v2    # "model":Ljava/lang/String;
    .end local v3    # "result":Z
    .end local v4    # "server_url":Ljava/lang/String;
    .end local v5    # "url":Ljava/net/URL;
    :catch_1
    move-exception v0

    .line 163
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "SHealthUpdation"

    const-string v7, "Fail Upload"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 166
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/16 :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1100(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1100(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUiListener:Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1200(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUiListener:Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1200(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mUpdateNeeded:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$800(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mIsForceupdate:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1000(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;->onResult(ZZ)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    # getter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1100(Lcom/sec/android/app/shealth/settings/SHealthUpdation;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->this$0:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/SHealthUpdation;->mCheckTask:Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->access$1102(Lcom/sec/android/app/shealth/settings/SHealthUpdation;Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;)Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;

    .line 180
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 89
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/SHealthUpdation$CheckTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
