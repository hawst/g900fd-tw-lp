.class final Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$gridHoverImages:Landroid/widget/GridView;

.field final synthetic val$iconHoverImage:Landroid/widget/ImageView;

.field final synthetic val$layout:Landroid/view/View;

.field final synthetic val$path:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/GridView;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$layout:Landroid/view/View;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$iconHoverImage:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$gridHoverImages:Landroid/widget/GridView;

    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$path:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 6

    const/16 v3, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$layout:Landroid/view/View;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    move-object v0, p1

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$iconHoverImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$iconHoverImage:Landroid/widget/ImageView;

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->convertDrawableIntoBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$gridHoverImages:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$layout:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    return v5

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$path:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$path:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$iconHoverImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$iconHoverImage:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByPath(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapWithCorrectingOrientationToNormal(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$gridHoverImages:Landroid/widget/GridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$path:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$iconHoverImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$gridHoverImages:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x3c

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v0

    float-to-int v0, v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$path:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverGridAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$path:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/2addr v4, v0

    invoke-direct {v3, v4, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$gridHoverImages:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$gridHoverImages:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$path:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$1;->val$gridHoverImages:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0
.end method
