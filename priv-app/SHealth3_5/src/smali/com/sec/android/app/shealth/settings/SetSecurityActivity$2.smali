.class Lcom/sec/android/app/shealth/settings/SetSecurityActivity$2;
.super Ljava/lang/Object;
.source "SetSecurityActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/SetSecurityActivity;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 303
    sget-object v1, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$4;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 319
    :goto_0
    return-void

    .line 307
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.service.health.cp.serversync.syncadapter.authenticator.SHEALTH_ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 308
    .local v0, "accountIntent":Landroid/content/Intent;
    const-string/jumbo v1, "packageName"

    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/SetSecurityActivity;

    const/16 v2, 0x3e6

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 303
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
