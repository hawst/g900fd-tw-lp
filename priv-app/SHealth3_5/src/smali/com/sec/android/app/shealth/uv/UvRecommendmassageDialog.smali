.class public Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;
.super Landroid/app/Dialog;
.source "UvRecommendmassageDialog.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInfoView:Landroid/view/View;

.field mUvExtreme:Landroid/widget/ScrollView;

.field mUvHigh:Landroid/widget/ScrollView;

.field mUvLow:Landroid/widget/ScrollView;

.field mUvModerate:Landroid/widget/ScrollView;

.field mUvVeryHigh:Landroid/widget/ScrollView;

.field mUvrecommendHigh1:Landroid/widget/TextView;

.field mUvrecommendHigh2:Landroid/widget/TextView;

.field mUvrecommendHigh3:Landroid/widget/TextView;

.field mUvrecommendHigh4:Landroid/widget/TextView;

.field mUvrecommendLow1:Landroid/widget/TextView;

.field mUvrecommendLow2:Landroid/widget/TextView;

.field mUvrecommendLow3:Landroid/widget/TextView;

.field mUvrecommendModerate1:Landroid/widget/TextView;

.field mUvrecommendModerate2:Landroid/widget/TextView;

.field mUvrecommendModerate3:Landroid/widget/TextView;

.field mUvrecommendModerate4:Landroid/widget/TextView;

.field mUvrecommendVeryHigh1:Landroid/widget/TextView;

.field mUvrecommendVeryHigh2:Landroid/widget/TextView;

.field mUvrecommendVeryHigh3:Landroid/widget/TextView;

.field mUvrecommendVeryHigh4:Landroid/widget/TextView;

.field mUvrecommnedExtreme1:Landroid/widget/TextView;

.field mUvrecommnedExtreme2:Landroid/widget/TextView;

.field mUvrecommnedExtreme3:Landroid/widget/TextView;

.field mUvrecommnedExtreme4:Landroid/widget/TextView;

.field mUvtitle:Landroid/widget/TextView;

.field mUvtitleContent:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const v0, 0x7f0c0081

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->initLayout()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->setContentView(Landroid/view/View;)V

    .line 55
    return-void
.end method

.method private initLayout()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 65
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030264

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f08056a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 68
    .local v0, "btOk":Landroid/widget/Button;
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080568

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitle:Landroid/widget/TextView;

    .line 69
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080abc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitleContent:Landroid/widget/TextView;

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080abd

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvLow:Landroid/widget/ScrollView;

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvLow:Landroid/widget/ScrollView;

    const v3, 0x7f080ab5

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendLow1:Landroid/widget/TextView;

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvLow:Landroid/widget/ScrollView;

    const v3, 0x7f080ab6

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendLow2:Landroid/widget/TextView;

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvLow:Landroid/widget/ScrollView;

    const v3, 0x7f080ab7

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendLow3:Landroid/widget/TextView;

    .line 80
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080abe

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvModerate:Landroid/widget/ScrollView;

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvModerate:Landroid/widget/ScrollView;

    const v3, 0x7f080ab0

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate1:Landroid/widget/TextView;

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvModerate:Landroid/widget/ScrollView;

    const v3, 0x7f080ab1

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate2:Landroid/widget/TextView;

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvModerate:Landroid/widget/ScrollView;

    const v3, 0x7f080ab2

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate3:Landroid/widget/TextView;

    .line 84
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvModerate:Landroid/widget/ScrollView;

    const v3, 0x7f080ab3

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate4:Landroid/widget/TextView;

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080abf

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvHigh:Landroid/widget/ScrollView;

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa1

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh1:Landroid/widget/TextView;

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa2

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh2:Landroid/widget/TextView;

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa3

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh3:Landroid/widget/TextView;

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa4

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh4:Landroid/widget/TextView;

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ac0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvVeryHigh:Landroid/widget/ScrollView;

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvVeryHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa6

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh1:Landroid/widget/TextView;

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvVeryHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa7

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh2:Landroid/widget/TextView;

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvVeryHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa8

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh3:Landroid/widget/TextView;

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvVeryHigh:Landroid/widget/ScrollView;

    const v3, 0x7f080aa9

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh4:Landroid/widget/TextView;

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mInfoView:Landroid/view/View;

    const v3, 0x7f080ac1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvExtreme:Landroid/widget/ScrollView;

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvExtreme:Landroid/widget/ScrollView;

    const v3, 0x7f080aab

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme1:Landroid/widget/TextView;

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvExtreme:Landroid/widget/ScrollView;

    const v3, 0x7f080aac

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme2:Landroid/widget/TextView;

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvExtreme:Landroid/widget/ScrollView;

    const v3, 0x7f080aad

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme3:Landroid/widget/TextView;

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvExtreme:Landroid/widget/ScrollView;

    const v3, 0x7f080aae

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme4:Landroid/widget/TextView;

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvLow:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvModerate:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvHigh:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvVeryHigh:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvExtreme:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 116
    new-instance v2, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog$1;-><init>(Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    return-void
.end method

.method private setMessage(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const v7, 0x7f090dba

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 134
    packed-switch p1, :pswitch_data_0

    .line 193
    :goto_0
    return-void

    .line 138
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f090d94

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitleContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dca

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendLow1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dd2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendLow2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090dd3

    new-array v3, v6, [Ljava/lang/Object;

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendLow3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dd5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvLow:Landroid/widget/ScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto :goto_0

    .line 148
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f090d95

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitleContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dcb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dd6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dd7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dd8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendModerate4:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dd9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvModerate:Landroid/widget/ScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_0

    .line 159
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f090d96

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitleContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dcc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dda

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090ddc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090ddd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendHigh4:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dde

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvHigh:Landroid/widget/ScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_0

    .line 170
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f090d97

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitleContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dcd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090ddf

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090de1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090de2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommendVeryHigh4:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090de3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvVeryHigh:Landroid/widget/ScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_0

    .line 181
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f090d98

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvtitleContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090dce

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090de4

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090de6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090de7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvrecommnedExtreme4:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f090de8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->mUvExtreme:Landroid/widget/ScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected onStart()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 61
    return-void
.end method

.method public updateContent(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/UvRecommendmassageDialog;->setMessage(I)V

    .line 131
    return-void
.end method
