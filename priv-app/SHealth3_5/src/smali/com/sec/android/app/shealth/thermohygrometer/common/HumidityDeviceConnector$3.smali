.class Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$3;
.super Ljava/lang/Object;
.source "HumidityDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)V
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 402
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener onReceived1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener extra: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    const/16 v1, 0x12

    if-ne p1, v1, :cond_0

    move-object v0, p2

    .line 406
    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;

    .line 407
    .local v0, "humidityData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;
    if-eqz v0, :cond_0

    .line 408
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener humidityData.humidity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->humidity:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener humidityData.accuracy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->accuracy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->humidity:F

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->accuracy:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mHumiditySensorListener:Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;)V

    .line 423
    .end local v0    # "humidityData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;
    :cond_0
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 427
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener onReceived2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener extra: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    const/16 v1, 0x12

    if-ne p1, v1, :cond_1

    .line 431
    const/4 v1, 0x0

    aget-object v0, p2, v1

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;

    .line 432
    .local v0, "humidityData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;
    if-eqz v0, :cond_0

    .line 433
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener humidityData.humidity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->humidity:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener humidityData.accuracy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->accuracy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    :cond_0
    if-eqz v0, :cond_1

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->humidity:F

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->accuracy:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_1

    .line 443
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->mHumiditySensorListener:Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;)Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/thermohygrometer/HumiditySensorListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;)V

    .line 449
    .end local v0    # "humidityData":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;
    :cond_1
    return-void
.end method

.method public onStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 392
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDevice.DataListener onStarted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    return-void
.end method

.method public onStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 397
    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/HumidityDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDevice.DataListener onStopped : dataType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    return-void
.end method
