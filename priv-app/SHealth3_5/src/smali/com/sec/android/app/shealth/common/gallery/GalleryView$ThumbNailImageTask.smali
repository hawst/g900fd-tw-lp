.class Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;
.super Landroid/os/AsyncTask;
.source "GalleryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/gallery/GalleryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ThumbNailImageTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field mImageButton:Landroid/widget/ImageButton;

.field mImagePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/common/gallery/GalleryView;Landroid/widget/ImageButton;Ljava/lang/String;)V
    .locals 0
    .param p2, "thumbnailImageButton"    # Landroid/widget/ImageButton;
    .param p3, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 121
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImageButton:Landroid/widget/ImageButton;

    .line 122
    iput-object p3, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImagePath:Ljava/lang/String;

    .line 123
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 128
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImagePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->access$200(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->access$200(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedAndScaledByCenterBitmap(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 131
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 117
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 138
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImageButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImageButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImagePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImageButton:Landroid/widget/ImageButton;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImagePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImageButton:Landroid/widget/ImageButton;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-static {v0, v1, v4, v4}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImageButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask$1;-><init>(Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->access$500(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->mImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->access$500(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mImagePathList:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->access$600(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mCompleteItemAddNotifier:Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->access$700(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mCompleteItemAddNotifier:Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->access$700(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;->executePostItemAddition()V

    .line 172
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 117
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
