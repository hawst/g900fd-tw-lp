.class public Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
.super Landroid/widget/FrameLayout;
.source "RankingsItem.java"


# static fields
.field public static final DEFAULT_ICON_SRC:I = 0x7f0205c1

.field private static final ICON_APPEAR_ANIMATION_DURATION:I = 0x177

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private acolyteHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

.field private additionalView:Landroid/view/View;

.field private award:Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;

.field private icon:Landroid/widget/ViewSwitcher;

.field private isMyRankingZero:Z

.field public leader_list_item_layout:Landroid/widget/RelativeLayout;

.field private mHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

.field private textBot:Landroid/widget/TextView;

.field private textMyRanking:Landroid/widget/TextView;

.field private textTop:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->init()V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->init()V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->init()V

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->mHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    return-object v0
.end method

.method public static getDefaultBitmaps(Landroid/content/Context;)[Landroid/graphics/drawable/BitmapDrawable;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 277
    const/4 v4, 0x2

    new-array v3, v4, [Landroid/graphics/drawable/BitmapDrawable;

    .line 279
    .local v3, "result":[Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205c1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 280
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 281
    .local v1, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020616

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 282
    .local v2, "mask":Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->invert(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 283
    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->maskingProfileImage(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v5, v3, v4

    .line 284
    const/4 v4, 0x1

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->maskingProfileImage(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v5, v3, v4

    .line 287
    return-object v3
.end method

.method private getURLImages(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    .locals 5
    .param p1, "holder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .prologue
    .line 320
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->mHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 321
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;)V

    .line 346
    .local v0, "imageLoadingTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/String;Ljava/lang/Void;Landroid/graphics/drawable/Drawable;>;"
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getIconUrl()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 347
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030289

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->initViews()V

    .line 84
    return-void
.end method

.method private initIconSwitchAnimation()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x177

    const/high16 v3, 0x3f800000    # 1.0f

    .line 100
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v3, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 101
    .local v1, "outAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v1, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 102
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 103
    .local v0, "inAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->icon:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2, v0}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->icon:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 106
    return-void
.end method

.method private initViews()V
    .locals 2

    .prologue
    .line 87
    const v0, 0x7f080b7d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textMyRanking:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textMyRanking:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    const v0, 0x7f08080a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f080b80

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textBot:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f080b7c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->award:Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;

    .line 92
    const v0, 0x7f080b81

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->additionalView:Landroid/view/View;

    .line 93
    const v0, 0x7f080525

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->icon:Landroid/widget/ViewSwitcher;

    .line 94
    const v0, 0x7f080b7a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->leader_list_item_layout:Landroid/widget/RelativeLayout;

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->initIconSwitchAnimation()V

    .line 97
    return-void
.end method

.method public static maskingProfileImage(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const v10, 0x7f0a0a99

    const/4 v8, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 293
    if-nez p0, :cond_0

    move-object v6, v8

    .line 315
    :goto_0
    return-object v6

    .line 295
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 297
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 298
    .local v7, "width":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 300
    .local v1, "height":I
    invoke-static {p1, v7, v1, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 301
    .local v2, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020616

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 302
    .local v3, "mask":Landroid/graphics/Bitmap;
    invoke-static {v3, v7, v1, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 303
    .local v5, "resizedBitmap":Landroid/graphics/Bitmap;
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v1, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 305
    .local v6, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 306
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 307
    .local v4, "paint":Landroid/graphics/Paint;
    invoke-virtual {v4, v12}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 308
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 310
    invoke-virtual {v0, v2, v11, v11, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 311
    new-instance v9, Landroid/graphics/PorterDuffXfermode;

    sget-object v10, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v9, v10}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 312
    invoke-virtual {v0, v5, v11, v11, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 313
    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0
.end method


# virtual methods
.method public getValuesHolder()Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->acolyteHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    return-object v0
.end method

.method public setAdditionalState(Z)V
    .locals 2
    .param p1, "isAdditional"    # Z

    .prologue
    .line 109
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->additionalView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 110
    return-void

    .line 109
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setImageMasking(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;ZLandroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 10
    .param p1, "holder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p2, "isMy"    # Z
    .param p3, "src"    # Landroid/graphics/drawable/BitmapDrawable;
    .param p4, "background"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 226
    const/4 v2, 0x0

    .line 227
    .local v2, "iconSrc":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 229
    .local v1, "iconBack":Landroid/graphics/drawable/Drawable;
    const v7, 0x7f080b7f

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 231
    .local v3, "img":Landroid/widget/ImageView;
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getIconUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-nez v7, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020616

    invoke-static {v7, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 234
    .local v6, "mask":Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->invert(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 235
    .local v4, "inverted_mask":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 238
    .local v5, "mainImage":Landroid/graphics/Bitmap;
    if-nez v5, :cond_1

    .line 239
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getURLImages(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 250
    .end local v4    # "inverted_mask":Landroid/graphics/Bitmap;
    .end local v5    # "mainImage":Landroid/graphics/Bitmap;
    .end local v6    # "mask":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-nez v7, :cond_2

    .line 252
    move-object v2, p3

    .line 253
    move-object v1, p4

    .line 255
    invoke-virtual {p1, v2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconSrc(Landroid/graphics/drawable/Drawable;)V

    .line 256
    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconback(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    move-object v0, v2

    .line 264
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 265
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 266
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 267
    invoke-virtual {v3}, Landroid/widget/ImageView;->invalidate()V

    .line 268
    return-void

    .line 241
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v4    # "inverted_mask":Landroid/graphics/Bitmap;
    .restart local v5    # "mainImage":Landroid/graphics/Bitmap;
    .restart local v6    # "mask":Landroid/graphics/Bitmap;
    :cond_1
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->maskingProfileImage(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v7}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconSrc(Landroid/graphics/drawable/Drawable;)V

    .line 242
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->maskingProfileImage(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v7}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconback(Landroid/graphics/drawable/Drawable;)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->updateIconDrawable()V

    goto :goto_0

    .line 259
    .end local v4    # "inverted_mask":Landroid/graphics/Bitmap;
    .end local v5    # "mainImage":Landroid/graphics/Bitmap;
    .end local v6    # "mask":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 260
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getIconback()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1
.end method

.method public setRanking(IZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "isMyRanking"    # Z

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->award:Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->setRanking(IZ)V

    .line 140
    return-void
.end method

.method public setTextBot(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textBot:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    return-void
.end method

.method public setTextMyRanking(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textMyRanking:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    return-void
.end method

.method public setTextTop(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 113
    const-string/jumbo v0, "unknown"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0907de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTrophy(I)V
    .locals 1
    .param p1, "src"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->award:Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->setTrophySrc(I)V

    .line 136
    return-void
.end method

.method public setValuesHolder(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Landroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 6
    .param p1, "holder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p2, "myHolder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p3, "source"    # Landroid/graphics/drawable/BitmapDrawable;
    .param p4, "background"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 143
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setValuesHolder(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;ZLandroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V

    .line 144
    return-void
.end method

.method public setValuesHolder(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;ZLandroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 9
    .param p1, "holder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p2, "myHolder"    # Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    .param p3, "isMyRanking"    # Z
    .param p4, "source"    # Landroid/graphics/drawable/BitmapDrawable;
    .param p5, "background"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->acolyteHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    .line 149
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->isMyRankingZero:Z

    .line 150
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v5

    if-ne v4, v5, :cond_3

    .line 151
    const/4 p3, 0x1

    .line 152
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 153
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->isMyRankingZero:Z

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090bde

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "noRankingStr":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTextTop(Ljava/lang/String;)V

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    const/4 v5, 0x1

    const/high16 v6, 0x41880000    # 17.0f

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/16 v7, 0x17

    const/16 v8, 0xe

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textTop:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 165
    .end local v2    # "noRankingStr":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->leader_list_item_layout:Landroid/widget/RelativeLayout;

    const v5, 0x7f0208db

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 171
    :goto_1
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->isMyRankingZero:Z

    if-nez v4, :cond_0

    .line 174
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 175
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f090b8d

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getStep()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "stepTxt":Ljava/lang/String;
    :goto_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTextBot(Ljava/lang/String;)V

    .line 181
    .end local v3    # "stepTxt":Ljava/lang/String;
    :cond_0
    if-eqz p3, :cond_5

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 184
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f020616

    invoke-static {v0, v4, v5}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 186
    .local v1, "cropped_bmp":Landroid/graphics/Bitmap;
    new-instance p4, Landroid/graphics/drawable/BitmapDrawable;

    .end local p4    # "source":Landroid/graphics/drawable/BitmapDrawable;
    invoke-direct {p4, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 187
    .restart local p4    # "source":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v0, 0x0

    .line 189
    .end local v1    # "cropped_bmp":Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setImageMasking(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;ZLandroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V

    .line 193
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :goto_3
    if-eqz p3, :cond_8

    .line 194
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->award:Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->setVisibility(I)V

    .line 195
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->isMyRankingZero:Z

    if-eqz v4, :cond_6

    .line 196
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textBot:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 197
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textMyRanking:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    :goto_4
    return-void

    .line 161
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->isMyRankingZero:Z

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090b7f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTextTop(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTextTop(Ljava/lang/String;)V

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->leader_list_item_layout:Landroid/widget/RelativeLayout;

    const v5, 0x7f0208db

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 177
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090b8d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getStep()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "stepTxt":Ljava/lang/String;
    goto :goto_2

    .line 191
    .end local v3    # "stepTxt":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0, p1, p3, p4, p5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setImageMasking(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;ZLandroid/graphics/drawable/BitmapDrawable;Landroid/graphics/drawable/BitmapDrawable;)V

    goto :goto_3

    .line 198
    :cond_6
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    const/4 v5, 0x1

    if-lt v4, v5, :cond_7

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    const/16 v5, 0xa

    if-gt v4, v5, :cond_7

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textMyRanking:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 200
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTextMyRanking(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setRanking(IZ)V

    goto :goto_4

    .line 203
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textMyRanking:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTextMyRanking(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 208
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->textMyRanking:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->award:Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingTrophyView;->setVisibility(I)V

    .line 210
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v5

    if-ne v4, v5, :cond_9

    .line 211
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getTrophy()Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->getItemIcon(Z)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTrophy(I)V

    .line 212
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setRanking(IZ)V

    goto/16 :goto_4

    .line 214
    :cond_9
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getTrophy()Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->getItemIcon(Z)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setTrophy(I)V

    .line 215
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->setRanking(IZ)V

    goto/16 :goto_4
.end method

.method public updateIconDrawable()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->icon:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->acolyteHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getImageBitmap()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->icon:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 132
    return-void
.end method
