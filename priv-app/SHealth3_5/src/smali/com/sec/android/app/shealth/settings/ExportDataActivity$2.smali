.class Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;
.super Ljava/lang/Object;
.source "ExportDataActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/ExportDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 327
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 328
    .local v0, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/sec/android/app/shealth/settings/ExportDataActivity;>;"
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->data:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$300(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_1

    .line 329
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$800(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$800(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->mExportProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$800(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$700(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->exportDataRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$600(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 334
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->completeProgress:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$000(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 335
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->completeExportPopup()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$900(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)V

    .line 337
    :cond_1
    return-void
.end method
