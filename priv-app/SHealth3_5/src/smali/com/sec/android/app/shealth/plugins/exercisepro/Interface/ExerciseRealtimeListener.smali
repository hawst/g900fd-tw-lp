.class public interface abstract Lcom/sec/android/app/shealth/plugins/exercisepro/Interface/ExerciseRealtimeListener;
.super Ljava/lang/Object;
.source "ExerciseRealtimeListener.java"


# virtual methods
.method public abstract onChannelStateChanged(Ljava/lang/String;II)V
.end method

.method public abstract onHealthServiceError(Ljava/lang/String;II)V
.end method

.method public abstract onMaxDurationReached(J)V
.end method

.method public abstract onNewLocation(Landroid/location/Location;)V
.end method

.method public abstract onNotiGpsState(Z)V
.end method

.method public abstract onSensorInfoReceived(I)V
.end method

.method public abstract onUpdateLapClock(J)V
.end method

.method public abstract onUpdateTEPaceGuide(I)V
.end method

.method public abstract onUpdateVaule(IFI)V
.end method

.method public abstract onWorkoutSaved(J)V
.end method
