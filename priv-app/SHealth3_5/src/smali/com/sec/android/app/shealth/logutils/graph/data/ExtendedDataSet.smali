.class public Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
.super Lcom/samsung/android/sdk/chart/series/SchartDataSet;
.source "ExtendedDataSet.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/android/sdk/chart/series/SchartDataSet;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    return-void
.end method

.method private assertChartSeriesOfProperType(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V
    .locals 3
    .param p1, "series"    # Lcom/samsung/android/sdk/chart/series/SchartSeries;

    .prologue
    .line 182
    instance-of v0, p1, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;

    if-nez v0, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "You are allowed to give only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " instance in method."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_0
    return-void
.end method

.method private assertDataSetNotEmpty()V
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getSeriesCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot operate with data while there is no data added to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    return-void
.end method


# virtual methods
.method public addSeries(ILcom/samsung/android/sdk/chart/series/SchartSeries;)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "series"    # Lcom/samsung/android/sdk/chart/series/SchartSeries;

    .prologue
    .line 156
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->assertChartSeriesOfProperType(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 157
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(ILcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 158
    return-void
.end method

.method public addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V
    .locals 0
    .param p1, "series"    # Lcom/samsung/android/sdk/chart/series/SchartSeries;

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->assertChartSeriesOfProperType(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 151
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 152
    return-void
.end method

.method public addXYTimeSeriesList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "seriesDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 144
    .local v1, "timeSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    goto :goto_0

    .line 146
    .end local v1    # "timeSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    :cond_0
    return-void
.end method

.method public getMaxDataValue()D
    .locals 6

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->assertDataSetNotEmpty()V

    .line 50
    const-wide/16 v1, 0x1

    .line 51
    .local v1, "max":D
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;

    .line 52
    .local v3, "series":Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;
    invoke-interface {v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;->getMaxDataValue()D

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    .line 53
    goto :goto_0

    .line 54
    .end local v3    # "series":Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;
    :cond_0
    return-wide v1
.end method

.method public getMinDataValue()D
    .locals 6

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->assertDataSetNotEmpty()V

    .line 72
    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 74
    .local v1, "min":D
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;

    .line 76
    .local v3, "series":Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;
    invoke-interface {v3}, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;->getMinDataValue()D

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    .line 78
    goto :goto_0

    .line 80
    .end local v3    # "series":Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;
    :cond_0
    return-wide v1
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 35
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getSeriesCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 36
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSize()I

    move-result v1

    if-lez v1, :cond_0

    .line 37
    const/4 v1, 0x0

    .line 40
    :goto_1
    return v1

    .line 35
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;-><init>(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V

    return-object v0
.end method
