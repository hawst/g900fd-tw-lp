.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$3;
.super Ljava/lang/Object;
.source "AMapExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->setUpMap()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraChange(Lcom/amap/api/maps2d/model/CameraPosition;)V
    .locals 3
    .param p1, "pos"    # Lcom/amap/api/maps2d/model/CameraPosition;

    .prologue
    .line 393
    iget v0, p1, Lcom/amap/api/maps2d/model/CameraPosition;->zoom:F

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    iget v1, p1, Lcom/amap/api/maps2d/model/CameraPosition;->zoom:F

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;F)F

    .line 395
    const-string v0, "ExerciseProDetailsMapFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "currentZoomLevel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->currentZoomLevel:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProDetailsMapFragment;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_0
    return-void
.end method

.method public onCameraChangeFinish(Lcom/amap/api/maps2d/model/CameraPosition;)V
    .locals 0
    .param p1, "arg0"    # Lcom/amap/api/maps2d/model/CameraPosition;

    .prologue
    .line 402
    return-void
.end method
