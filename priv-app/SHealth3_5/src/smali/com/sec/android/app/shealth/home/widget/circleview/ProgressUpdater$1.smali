.class Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;
.super Ljava/lang/Object;
.source "ProgressUpdater.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    iput-object p2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 39
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->access$100(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->messagesCount:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->access$200(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->delay:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->access$300(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 42
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;->val$handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    # operator++ for: Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->counter:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->access$108(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 48
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    return-void
.end method
