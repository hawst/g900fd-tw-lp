.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateRankingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$7;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$keyblockhandler;,
        Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;
    }
.end annotation


# static fields
.field public static final CURRENT_FRAGMENT:Ljava/lang/String; = "exercise_pick_activity_current_fragment"

.field public static final SELECTED_TAB:Ljava/lang/String; = "selected_tab"

.field private static final TAG:Ljava/lang/String;

.field private static currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;


# instance fields
.field private actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private isOnSaveCalled:Z

.field private saveTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

.field private scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

.field private shareViaButtonIndex:I

.field private tab1:Landroid/view/View;

.field private tab2:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->TAG:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->saveTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->shareViaButtonIndex:I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->isOnSaveCalled:Z

    .line 253
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->isNetworkConnted()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->disableTabClick(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200()Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    return-object p1
.end method

.method private disableTabClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 250
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$keyblockhandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$keyblockhandler;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 251
    return-void
.end method

.method public static getCurrentFragment(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 387
    const-string v0, "exercise_pick_activity_current_fragment"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private isNetworkConnted()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 264
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 266
    .local v0, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 267
    .local v1, "mobile":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 269
    .local v2, "wifi":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 272
    :cond_1
    :goto_0
    return v3

    :cond_2
    move v3, v4

    goto :goto_0
.end method

.method private setCurrentTabContentDescription(Landroid/view/View;Z)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "value"    # Z

    .prologue
    const v4, 0x7f090203

    .line 158
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    .line 159
    .local v0, "tvView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 161
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 162
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setUserRanking()V
    .locals 2

    .prologue
    .line 412
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$6;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 452
    return-void
.end method

.method private showShareViaDialog()V
    .locals 0

    .prologue
    .line 391
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaDialog(Landroid/content/Context;)V

    .line 392
    return-void
.end method

.method private switchToTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;I)V
    .locals 4
    .param p1, "tab"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;
    .param p2, "exerciseRowId"    # I

    .prologue
    const/16 v3, 0x400

    .line 117
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    if-eqz v1, :cond_0

    .line 118
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->findViewForTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 120
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->findViewForTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 123
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->MY:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    if-ne p1, v1, :cond_2

    .line 124
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    .line 125
    invoke-static {p2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->newInstance(I)Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    move-result-object v0

    .line 136
    .local v0, "fragmentToSwitch":Landroid/support/v4/app/Fragment;
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->clear()V

    .line 138
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->switchToFragment(Landroid/support/v4/app/Fragment;)V

    .line 140
    sput-object p1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->saveTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    .line 142
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$7;->$SwitchMap$com$sec$android$app$shealth$walkingmate$WalkingMateRankingActivity$Tab:[I

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/Window;->addFlags(I)V

    .line 150
    :goto_1
    return-void

    .line 127
    .end local v0    # "fragmentToSwitch":Landroid/support/v4/app/Fragment;
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;-><init>()V

    .restart local v0    # "fragmentToSwitch":Landroid/support/v4/app/Fragment;
    goto :goto_0

    .line 130
    .end local v0    # "fragmentToSwitch":Landroid/support/v4/app/Fragment;
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->EVERYONE:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    if-ne p1, v1, :cond_3

    .line 131
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;-><init>()V

    .restart local v0    # "fragmentToSwitch":Landroid/support/v4/app/Fragment;
    goto :goto_0

    .line 134
    .end local v0    # "fragmentToSwitch":Landroid/support/v4/app/Fragment;
    :cond_3
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "This tab should not exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 145
    .restart local v0    # "fragmentToSwitch":Landroid/support/v4/app/Fragment;
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x500

    invoke-virtual {v1, v3, v2}, Landroid/view/Window;->setFlags(II)V

    goto :goto_1

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addShareViaButton()V
    .locals 4

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->shareViaButtonIndex:I

    .line 106
    :cond_0
    return-void
.end method

.method protected customizeActionBar()V
    .locals 4

    .prologue
    .line 363
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 365
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-eqz v0, :cond_0

    .line 366
    const v2, 0x7f090bd5

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 367
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v2, 0x7f0208e3

    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$5;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 375
    .local v1, "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v2, 0x7f090033

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 376
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 377
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 380
    .end local v1    # "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_0
    return-void
.end method

.method protected customizeActionBarWithTabs()V
    .locals 2

    .prologue
    .line 169
    const v0, 0x7f080bf8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    .line 170
    const v0, 0x7f080bf7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setCurrentTabContentDescription(Landroid/view/View;Z)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->MY:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->EVERYONE:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setCurrentTabSelected()V

    .line 244
    return-void
.end method

.method protected findViewForTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;)Landroid/view/View;
    .locals 2
    .param p1, "tab"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    .prologue
    .line 299
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$7;->$SwitchMap$com$sec$android$app$shealth$walkingmate$WalkingMateRankingActivity$Tab:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 305
    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "this tab does not exist"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 301
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab1:Landroid/view/View;

    .line 303
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->tab2:Landroid/view/View;

    goto :goto_0

    .line 299
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getCurrentFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getCurrentFragment(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 313
    .local v0, "currentFragment":Landroid/support/v4/app/Fragment;
    if-nez v0, :cond_0

    .line 346
    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    return-void

    .line 316
    .restart local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 319
    :pswitch_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_3

    .line 320
    instance-of v2, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    if-eqz v2, :cond_2

    .line 321
    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 325
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->finish()V

    goto :goto_0

    .line 322
    .restart local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :cond_2
    instance-of v2, v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    if-eqz v2, :cond_1

    .line 323
    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->dismissLoadingDialog()V

    goto :goto_1

    .line 327
    .restart local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :cond_3
    const-string v2, "error_code"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "errorCode":Ljava/lang/String;
    const-string v2, "SAC_0204"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 330
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestAccessToken(Landroid/content/Context;)V

    .line 332
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->finish()V

    goto :goto_0

    .line 336
    .end local v1    # "errorCode":Ljava/lang/String;
    :pswitch_1
    instance-of v2, v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    if-eqz v2, :cond_6

    .line 337
    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 341
    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->finish()V

    goto :goto_0

    .line 338
    .restart local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    :cond_6
    instance-of v2, v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    if-eqz v2, :cond_5

    .line 339
    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    .end local v0    # "currentFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->dismissLoadingDialog()V

    goto :goto_2

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0xa25
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 350
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    .line 351
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->isOnSaveCalled:Z

    .line 352
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 99
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->values()[Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getWalkScoreBoardSelectedTab()I

    move-result v2

    aget-object v1, v1, v2

    sput-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->saveTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    .line 61
    const v1, 0x7f0302ac

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setContentView(I)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    const/16 v3, 0x500

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 63
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 64
    .local v0, "context":Landroid/content/Context;
    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W005"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    if-nez v1, :cond_0

    .line 66
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->scm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 68
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->setUserRanking()V

    .line 69
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 397
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080c98

    if-ne v1, v2, :cond_0

    .line 398
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->showShareViaDialog()V

    .line 401
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f080ca7

    if-ne v1, v2, :cond_1

    .line 403
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/sec/android/app/shealth/common/utils/PrintUtils;->printSnapShot(Ljava/lang/String;Landroid/app/Activity;)V
    :try_end_0
    .catch Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :cond_1
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 404
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Lcom/sec/android/app/shealth/common/utils/PrintUtils$UnableToPrintException;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0900e5

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 356
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 357
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    if-eqz v0, :cond_0

    .line 358
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->ordinal()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setWalkScoreBoardSelectedTab(I)V

    .line 359
    :cond_0
    return-void
.end method

.method protected onPostCreate()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    if-nez v0, :cond_0

    .line 87
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;->MY:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->saveTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    .line 90
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->customizeActionBarWithTabs()V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->switchToTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;)V

    .line 94
    :cond_1
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->onPostCreate()V

    .line 83
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->saveTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    .line 74
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 76
    return-void
.end method

.method public removeShareViaButton()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 110
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->shareViaButtonIndex:I

    if-eq v0, v2, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->shareViaButtonIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 112
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->shareViaButtonIndex:I

    .line 114
    :cond_0
    return-void
.end method

.method protected setCurrentTabSelected()V
    .locals 2

    .prologue
    .line 280
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->currentTab:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->findViewForTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 281
    return-void
.end method

.method public switchToFragment(Landroid/support/v4/app/Fragment;)V
    .locals 4
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 285
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->isOnSaveCalled:Z

    if-nez v2, :cond_0

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 288
    .local v1, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const v2, 0x7f080bf9

    const-string v3, "exercise_pick_activity_current_fragment"

    invoke-virtual {v1, v2, p1, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 290
    :try_start_0
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    .end local v1    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 291
    .restart local v1    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->TAG:Ljava/lang/String;

    const-string v3, "could not save the instance state"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected switchToTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;)V
    .locals 1
    .param p1, "tab"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;

    .prologue
    .line 153
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->switchToTab(Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity$Tab;I)V

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;->refreshFocusables()V

    .line 155
    return-void
.end method
