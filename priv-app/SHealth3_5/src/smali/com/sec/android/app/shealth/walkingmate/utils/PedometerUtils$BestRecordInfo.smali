.class public Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;
.super Ljava/lang/Object;
.source "PedometerUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BestRecordInfo"
.end annotation


# instance fields
.field private mSteps:I

.field private mTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;
    .param p1, "x1"    # J

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mSteps:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;
    .param p1, "x1"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mSteps:I

    return p1
.end method


# virtual methods
.method public getSteps()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mSteps:I

    return v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils$BestRecordInfo;->mTime:J

    return-wide v0
.end method
