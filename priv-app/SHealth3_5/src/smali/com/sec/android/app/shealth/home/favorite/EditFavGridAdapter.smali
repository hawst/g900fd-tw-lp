.class public Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;
.super Lcom/sec/android/app/shealth/home/widget/dynamicgrid/BaseDynamicGridAdapter;
.source "EditFavGridAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$1;,
        Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;
    }
.end annotation


# static fields
.field private static mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "columnCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/home/data/IconData;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/BaseDynamicGridAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    .line 39
    sput-object p1, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 48
    if-nez p2, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03012d

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;

    invoke-direct {v0, p2, v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;-><init>(Landroid/view/View;Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$1;)V

    .line 52
    .local v0, "holder":Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 58
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->build(Ljava/lang/Object;)V

    .line 59
    return-object p2

    .line 56
    .end local v0    # "holder":Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;

    .restart local v0    # "holder":Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;
    goto :goto_0
.end method
