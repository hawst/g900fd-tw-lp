.class public Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;
.super Ljava/lang/Object;
.source "ExerciseData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private appName:Ljava/lang/String;

.field private cadence:I

.field private calories:F

.field private comment:Ljava/lang/String;

.field private distance:F

.field private endTime:J

.field private exerciseId:J

.field private id:J

.field private millisecond:J

.field private pulse:I

.field private sensorId:I

.field private speed:F

.field private step_counts:I

.field private time:J

.field private time_zone:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 352
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJFFIJIJJLjava/lang/String;)V
    .locals 21
    .param p1, "id"    # J
    .param p3, "millisecond"    # J
    .param p5, "calories"    # F
    .param p6, "distance"    # F
    .param p7, "cadence"    # I
    .param p8, "exerciseId"    # J
    .param p10, "inputType"    # I
    .param p11, "time"    # J
    .param p13, "endTime"    # J
    .param p15, "comment"    # Ljava/lang/String;

    .prologue
    .line 122
    const/4 v12, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const-string v18, "SHealth2"

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move/from16 v5, p5

    move-wide/from16 v6, p8

    move-wide/from16 v8, p11

    move-wide/from16 v10, p13

    move/from16 v13, p6

    move/from16 v15, p10

    move-object/from16 v17, p15

    move/from16 v20, p7

    invoke-direct/range {v0 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;-><init>(JJFJJJFFIIILjava/lang/String;Ljava/lang/String;II)V

    .line 124
    return-void
.end method

.method public constructor <init>(JJFJJJFFIIILjava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "millisecond"    # J
    .param p5, "calories"    # F
    .param p6, "exerciseId"    # J
    .param p8, "time"    # J
    .param p10, "endTime"    # J
    .param p12, "speed"    # F
    .param p13, "distance"    # F
    .param p14, "pulse"    # I
    .param p15, "inputType"    # I
    .param p16, "step_counts"    # I
    .param p17, "comment"    # Ljava/lang/String;
    .param p18, "appName"    # Ljava/lang/String;
    .param p19, "time_zone"    # I
    .param p20, "cadence"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->id:J

    .line 17
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->exerciseId:J

    .line 18
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->cadence:I

    .line 22
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->speed:F

    .line 23
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->distance:F

    .line 25
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->sensorId:I

    .line 26
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->step_counts:I

    .line 27
    const/16 v1, 0x37

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->time_zone:I

    .line 28
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->comment:Ljava/lang/String;

    .line 29
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->appName:Ljava/lang/String;

    .line 45
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    .line 46
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Wrong id"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->id:J

    .line 48
    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-gez v1, :cond_1

    .line 49
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Wrong amounts of minutes"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_1
    iput-wide p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->millisecond:J

    .line 51
    const/4 v1, 0x0

    cmpg-float v1, p5, v1

    if-gez v1, :cond_2

    .line 53
    const/4 p5, 0x0

    .line 56
    :cond_2
    float-to-int v1, p5

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->calories:F

    .line 57
    iput-wide p6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->exerciseId:J

    .line 58
    iput-wide p8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->time:J

    .line 59
    iput-wide p10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->endTime:J

    .line 60
    iput p12, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->speed:F

    .line 61
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->distance:F

    .line 62
    if-gez p14, :cond_3

    .line 63
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Wrong pulse value"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 64
    :cond_3
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->pulse:I

    .line 65
    const/4 v1, -0x2

    move/from16 v0, p15

    if-ge v0, v1, :cond_4

    .line 66
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Wrong input type value"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :cond_4
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->sensorId:I

    .line 68
    invoke-static/range {p17 .. p17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 69
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->comment:Ljava/lang/String;

    .line 71
    :cond_5
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->step_counts:I

    .line 72
    if-eqz p18, :cond_6

    invoke-virtual/range {p18 .. p18}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 73
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Wrong application name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 74
    :cond_7
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->appName:Ljava/lang/String;

    .line 75
    move/from16 v0, p19

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->time_zone:I

    .line 76
    move/from16 v0, p20

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->cadence:I

    .line 77
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x0

    return v0
.end method

.method public getCalories()F
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 139
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->calories:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 140
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->calories:F

    .line 143
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->calories:F

    return v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->distance:F

    return v0
.end method

.method public getExerciseId()J
    .locals 2

    .prologue
    .line 207
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->exerciseId:J

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 225
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->time:J

    return-wide v0
.end method

.method public getmillisecond()J
    .locals 2

    .prologue
    .line 216
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->millisecond:J

    return-wide v0
.end method

.method public setDistance(F)V
    .locals 0
    .param p1, "distance"    # F

    .prologue
    .line 265
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->distance:F

    .line 266
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 335
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 336
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->millisecond:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 337
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->calories:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 338
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->exerciseId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 339
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 340
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->endTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 341
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->speed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 342
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->distance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 343
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->pulse:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 344
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->sensorId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 345
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->step_counts:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->comment:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->appName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 348
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->time_zone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 349
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->cadence:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 350
    return-void
.end method
