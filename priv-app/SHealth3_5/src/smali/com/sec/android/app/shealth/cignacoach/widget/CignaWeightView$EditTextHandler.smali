.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;
.super Ljava/lang/Object;
.source "CignaWeightView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextHandler"
.end annotation


# instance fields
.field pos:I

.field preData:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;

    .prologue
    .line 326
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 375
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->pos:I

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->preData:Ljava/lang/String;

    .line 381
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v6, 0x1

    .line 332
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 333
    .local v2, "str":Ljava/lang/String;
    const-string v4, "\\."

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 334
    .local v0, "array":[Ljava/lang/String;
    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 335
    aget-object v4, v0, v6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_0

    .line 336
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->preData:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->pos:I

    if-lt v4, v6, :cond_0

    .line 341
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->pos:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v4

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    .line 352
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 353
    .local v3, "value":Ljava/lang/Float;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)I

    move-result v4

    const v5, 0x1fbd1

    if-ne v4, v5, :cond_2

    .line 355
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x43fa0000    # 500.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 356
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setMinValueText()V

    .line 357
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 370
    .end local v3    # "value":Ljava/lang/Float;
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->preData:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->pos:I

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setDatalimit(Ljava/lang/String;I)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$900(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Ljava/lang/String;I)V

    .line 371
    return-void

    .line 342
    :catch_0
    move-exception v1

    .line 343
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    .line 344
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 362
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v3    # "value":Ljava/lang/Float;
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const v5, 0x4489c99a    # 1102.3f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mListener:Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaFocusChangeInterface;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 363
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setMinValueText()V

    .line 364
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$EditTextHandler;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->setSelection(I)V

    goto :goto_1
.end method
