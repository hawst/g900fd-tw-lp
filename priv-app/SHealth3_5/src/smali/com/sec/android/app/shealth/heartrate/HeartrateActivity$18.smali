.class Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;
.super Ljava/lang/Object;
.source "HeartrateActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0

    .prologue
    .line 956
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 959
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    .line 960
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mInformationDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1402(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 961
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showSummarayFragment()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1600(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V

    .line 962
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1700(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1700(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 964
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo0:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1702(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 966
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1800(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 967
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1800(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 968
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$18;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->mAnimationViewInfo1:Landroid/widget/ImageView;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1802(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 970
    :cond_1
    return-void
.end method
