.class final Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;
.super Ljava/lang/Object;
.source "BooheeSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Base32Util"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 449
    return-void
.end method

.method static synthetic access$400(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 447
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 447
    invoke-static {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->str2Md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static characterIsDigit(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 484
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static characterIsLetter(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 488
    const/16 v0, 0x61

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7a

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static convert(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 5
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "buf"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 492
    const-string v1, "0123456789ABCDEF"

    .line 493
    .local v1, "digits":Ljava/lang/String;
    const-string v3, "UTF-8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 494
    .local v0, "bytes":[B
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 495
    const/16 v3, 0x25

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 496
    const-string v3, "0123456789ABCDEF"

    aget-byte v4, v0, v2

    and-int/lit16 v4, v4, 0xf0

    shr-int/lit8 v4, v4, 0x4

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 497
    const-string v3, "0123456789ABCDEF"

    aget-byte v4, v0, v2

    and-int/lit8 v4, v4, 0xf

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 494
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 499
    :cond_0
    return-void
.end method

.method private static encodeUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 452
    const/4 v0, 0x0

    .line 455
    .local v0, "buf":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x10

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 456
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    .local v1, "buf":Ljava/lang/StringBuilder;
    const/4 v5, -0x1

    .line 457
    .local v5, "start":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v4, v6, :cond_5

    .line 458
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 460
    .local v2, "ch":C
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->characterIsLetter(C)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->characterIsDigit(C)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "-._~"

    invoke-virtual {v6, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v7, -0x1

    if-le v6, v7, :cond_4

    .line 461
    :cond_0
    if-ltz v5, :cond_1

    .line 462
    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->convert(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 463
    const/4 v5, -0x1

    .line 465
    :cond_1
    const/16 v6, 0x20

    if-eq v2, v6, :cond_3

    .line 466
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 457
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 468
    :cond_3
    const-string v6, "%20"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 477
    .end local v2    # "ch":C
    :catch_0
    move-exception v3

    move-object v0, v1

    .line 478
    .end local v1    # "buf":Ljava/lang/StringBuilder;
    .end local v4    # "i":I
    .end local v5    # "start":I
    .restart local v0    # "buf":Ljava/lang/StringBuilder;
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    :goto_2
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->access$300()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 470
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    .restart local v1    # "buf":Ljava/lang/StringBuilder;
    .restart local v2    # "ch":C
    .restart local v4    # "i":I
    .restart local v5    # "start":I
    :cond_4
    if-gez v5, :cond_2

    .line 471
    move v5, v4

    goto :goto_1

    .line 474
    .end local v2    # "ch":C
    :cond_5
    if-ltz v5, :cond_6

    .line 475
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch$Base32Util;->convert(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_6
    move-object v0, v1

    .line 479
    .end local v1    # "buf":Ljava/lang/StringBuilder;
    .restart local v0    # "buf":Ljava/lang/StringBuilder;
    goto :goto_3

    .line 477
    .end local v4    # "i":I
    .end local v5    # "start":I
    :catch_1
    move-exception v3

    goto :goto_2
.end method

.method private static str2Md5(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "plainText"    # Ljava/lang/String;

    .prologue
    .line 503
    :try_start_0
    const-string v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 504
    .local v4, "md":Ljava/security/MessageDigest;
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/security/MessageDigest;->update([B)V

    .line 505
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 507
    .local v0, "b":[B
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ""

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 508
    .local v1, "buf":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "offset":I
    :goto_0
    array-length v6, v0

    if-ge v5, v6, :cond_2

    .line 509
    aget-byte v3, v0, v5

    .line 510
    .local v3, "i":I
    if-gez v3, :cond_0

    .line 511
    add-int/lit16 v3, v3, 0x100

    .line 513
    :cond_0
    const/16 v6, 0x10

    if-ge v3, v6, :cond_1

    .line 514
    const-string v6, "0"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 519
    .end local v3    # "i":I
    :cond_2
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->access$300()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "result: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->access$300()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "result: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x8

    const/16 v10, 0x18

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 525
    .end local v0    # "b":[B
    .end local v1    # "buf":Ljava/lang/StringBuilder;
    .end local v4    # "md":Ljava/security/MessageDigest;
    .end local v5    # "offset":I
    :goto_1
    return-object v6

    .line 523
    :catch_0
    move-exception v2

    .line 524
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;->access$300()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 525
    const/4 v6, 0x0

    goto :goto_1
.end method
