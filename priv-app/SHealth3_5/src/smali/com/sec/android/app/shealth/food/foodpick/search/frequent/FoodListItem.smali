.class public Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;
.super Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;
.source "FoodListItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field protected foodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

.field private mExtendedFoodInfo:Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

.field private mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V
    .locals 1
    .param p1, "foodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "foodInfoDao"    # Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V
    .locals 4
    .param p1, "foodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "foodInfoDao"    # Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    .param p3, "extraFoodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;-><init>()V

    .line 49
    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v0

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFoodInfoId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FoodInfoData (id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") not related for ExtraFoodInfoData (id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; foodInfoId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFoodInfoId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 55
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->foodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 56
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mExtendedFoodInfo:Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 57
    return-void
.end method


# virtual methods
.method public addToFavorites()V
    .locals 3

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "added to favorite: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->saveToDatabase()V

    .line 77
    return-void
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "another"    # Ljava/lang/Object;

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 122
    instance-of v2, p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    if-nez v2, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 125
    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .line 126
    .local v0, "that":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    iget-object v3, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method protected getDescription(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getDescription(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExtendedFoodInfo()Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mExtendedFoodInfo:Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    return-object v0
.end method

.method public getFoodInfoItem()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v0, ""

    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getFavorite()Z

    move-result v0

    return v0
.end method

.method public removeFromFavorites()V
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removed from favorite:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->setFavorite(Z)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->saveToDatabase()V

    .line 84
    return-void
.end method

.method public saveToDatabase()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->foodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 99
    return-void
.end method

.method public setExtendedFoodInfo(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V
    .locals 0
    .param p1, "mExtendedFoodInfo"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mExtendedFoodInfo:Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 103
    return-void
.end method

.method public setFoodInfoItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 0
    .param p1, "mFoodInfoItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->mFoodInfoItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 70
    return-void
.end method
