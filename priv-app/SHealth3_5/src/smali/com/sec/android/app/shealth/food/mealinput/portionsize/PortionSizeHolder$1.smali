.class final Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder$1;
.super Ljava/lang/Object;
.source "PortionSizeHolder.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
    .locals 12
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x1

    .line 297
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-ne v5, v4, :cond_0

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v7

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v9

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v10

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v11

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;-><init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 293
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 304
    new-array v0, p1, [Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 293
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder$1;->newArray(I)[Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    move-result-object v0

    return-object v0
.end method
