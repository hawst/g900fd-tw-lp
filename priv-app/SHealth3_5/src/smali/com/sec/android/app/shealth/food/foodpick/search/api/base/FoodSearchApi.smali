.class public interface abstract Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
.super Ljava/lang/Object;
.source "FoodSearchApi.java"


# virtual methods
.method public abstract editSearchPhrase(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getAutoCompleteMinLength()I
.end method

.method public abstract getIndexOfFirstPage()I
.end method

.method public abstract getLogoResourceId()I
.end method

.method public abstract getPerPageCount()I
.end method

.method public abstract getServerSourceType()I
.end method

.method public abstract hasLogo()Z
.end method

.method public abstract isAutoCompleteSupported()Z
.end method

.method public abstract performAutoCompleteSearch(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract performBarcodeRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract performCategorySearch()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract performExtraFoodInfoRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ExtraFoodInfoResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract performFoodSearch(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract performFoodSearchByCategory(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract performSubCategorySearch(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;",
            ")",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;",
            ">;"
        }
    .end annotation
.end method
