.class Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;
.super Ljava/lang/Object;
.source "SHealthAlertDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getGeneralParentContainer(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field final synthetic val$parentContent:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->val$parentContent:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x5

    const/4 v9, -0x2

    const/4 v12, 0x1

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 542
    const/4 v6, 0x2

    new-array v7, v6, [I

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->val$parentContent:Landroid/view/View;

    sget v8, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLineCount()I

    move-result v6

    aput v6, v7, v10

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->val$parentContent:Landroid/view/View;

    sget v8, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLineCount()I

    move-result v6

    aput v6, v7, v12

    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->getMax([I)I

    move-result v2

    .line 548
    .local v2, "maxLinesAmount":I
    if-le v2, v12, :cond_0

    .line 551
    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->val$parentContent:Landroid/view/View;

    sget v7, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_ok_buttons_layout:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 553
    .local v0, "buttonLayout":Landroid/widget/LinearLayout;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const/16 v7, 0x24

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-direct {v4, v9, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 554
    .local v4, "params":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const/16 v7, 0x24

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-direct {v5, v9, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 555
    .local v5, "paramsOkButton":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v11}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v11}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v7

    float-to-int v7, v7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v5, v6, v10, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 557
    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->val$parentContent:Landroid/view/View;

    sget v7, Lcom/sec/android/app/shealth/common/commonui/R$id;->cancel_button:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 558
    .local v1, "cancelButton":Landroid/widget/Button;
    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->val$parentContent:Landroid/view/View;

    sget v7, Lcom/sec/android/app/shealth/common/commonui/R$id;->ok_button:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 559
    .local v3, "okButton":Landroid/widget/Button;
    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 561
    iput v13, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 562
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v11}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v11}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v4, v6, v10, v7, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 563
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 564
    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v6, v6, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->addButton:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 566
    iput v13, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 567
    invoke-virtual {v3, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 570
    .end local v0    # "buttonLayout":Landroid/widget/LinearLayout;
    .end local v1    # "cancelButton":Landroid/widget/Button;
    .end local v3    # "okButton":Landroid/widget/Button;
    .end local v4    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v5    # "paramsOkButton":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void
.end method
