.class Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AwardSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/award/AwardSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceCheckedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/award/AwardSummaryFragment;)V
    .locals 0

    .prologue
    .line 753
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;->this$0:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 757
    if-nez p2, :cond_1

    .line 769
    :cond_0
    :goto_0
    return-void

    .line 761
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 763
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "com.sec.android.app.shealth.pedometer.viewstepcount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 764
    const-string v1, "DeviceCheckedReceiver"

    const-string v2, "Changed device"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;->this$0:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->access$000(Lcom/sec/android/app/shealth/award/AwardSummaryFragment;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;->this$0:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardSummaryFragment$DeviceCheckedReceiver;->this$0:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->awardSummaryFragmentView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->access$000(Lcom/sec/android/app/shealth/award/AwardSummaryFragment;)Landroid/view/View;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->initializeTodayActivityArea(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->access$100(Lcom/sec/android/app/shealth/award/AwardSummaryFragment;Landroid/view/View;)V

    goto :goto_0
.end method
