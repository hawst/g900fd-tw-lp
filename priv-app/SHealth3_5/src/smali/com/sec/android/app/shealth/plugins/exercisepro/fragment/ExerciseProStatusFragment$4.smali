.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;
.super Ljava/lang/Object;
.source "ExerciseProStatusFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V
    .locals 0

    .prologue
    .line 586
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 592
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 594
    .local v0, "type":I
    invoke-virtual {p1, v6}, Landroid/view/View;->setSelected(Z)V

    .line 595
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 614
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setActivityType(I)V

    .line 615
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->updateModeText()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    .line 616
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->updateActivityType()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    .line 617
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->updateCardView()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    .line 618
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mGoalTypeValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 619
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mGoalTypeValue:I
    invoke-static {v1, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;I)I

    .line 621
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mGoalTypeValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mGoalValue:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mTEenginlevel:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)I

    move-result v4

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setGoalValue(III)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;III)V

    .line 622
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateGoal(Z)V

    .line 623
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mGoalTypeValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)I

    move-result v1

    if-ne v1, v6, :cond_1

    .line 624
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispActivityTypeData(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;I)V

    .line 628
    :goto_1
    return-void

    .line 597
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->runningView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->firstbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 601
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->walkingView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->secondbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 605
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->cyclingView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 606
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->thirdbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 609
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->hikingView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 610
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->fourthbubbleView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 626
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mGoalTypeValue:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispGoalTypeData(IZ)V
    invoke-static {v1, v2, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;IZ)V

    goto :goto_1

    .line 595
    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
