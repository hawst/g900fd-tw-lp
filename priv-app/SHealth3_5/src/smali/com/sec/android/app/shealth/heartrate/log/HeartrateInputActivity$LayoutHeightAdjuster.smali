.class public Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;
.super Ljava/lang/Object;
.source "HeartrateInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutHeightAdjuster"
.end annotation


# instance fields
.field private mChildOfContent:Landroid/view/View;

.field private usableHeightPrevious:I

.field private vgParams:Landroid/view/ViewGroup$LayoutParams;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 504
    const v3, 0x1020002

    invoke-virtual {p1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 506
    .local v2, "contentChild":Landroid/widget/FrameLayout;
    if-nez v2, :cond_1

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 509
    .local v0, "c2":Landroid/view/ViewParent;
    if-eqz v0, :cond_0

    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 511
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 512
    .local v1, "content":Landroid/view/ViewGroup;
    if-eqz v1, :cond_0

    .line 514
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->mChildOfContent:Landroid/view/View;

    .line 515
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster$1;-><init>(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 521
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->vgParams:Landroid/view/ViewGroup$LayoutParams;

    goto :goto_0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;

    .prologue
    .line 490
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->possiblyResizeChildOfContent()V

    return-void
.end method

.method public static assistActivity(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 497
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;-><init>(Landroid/app/Activity;)V

    .line 498
    return-void
.end method

.method private computeUsableHeight()I
    .locals 3

    .prologue
    .line 545
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 546
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 547
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    return v1
.end method

.method private possiblyResizeChildOfContent()V
    .locals 5

    .prologue
    .line 525
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->computeUsableHeight()I

    move-result v1

    .line 526
    .local v1, "usableHeightNow":I
    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->usableHeightPrevious:I

    if-eq v1, v3, :cond_0

    .line 527
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 529
    .local v2, "usableHeightSansKeyboard":I
    sub-int v0, v2, v1

    .line 531
    .local v0, "heightDifference":I
    div-int/lit8 v3, v2, 0x4

    if-le v0, v3, :cond_1

    .line 533
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->vgParams:Landroid/view/ViewGroup$LayoutParams;

    sub-int v4, v2, v0

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 539
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->mChildOfContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestLayout()V

    .line 540
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->usableHeightPrevious:I

    .line 542
    .end local v0    # "heightDifference":I
    .end local v2    # "usableHeightSansKeyboard":I
    :cond_0
    return-void

    .line 537
    .restart local v0    # "heightDifference":I
    .restart local v2    # "usableHeightSansKeyboard":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/log/HeartrateInputActivity$LayoutHeightAdjuster;->vgParams:Landroid/view/ViewGroup$LayoutParams;

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method
