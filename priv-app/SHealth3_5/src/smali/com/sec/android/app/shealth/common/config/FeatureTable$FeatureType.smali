.class public final enum Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;
.super Ljava/lang/Enum;
.source "FeatureTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/config/FeatureTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FeatureType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum BloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum BloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum Sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum Stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum UV:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

.field public static final enum Weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "Stress"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 18
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "Sleep"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 19
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "SPO2"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 20
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "HeartRate"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 21
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "BloodPressure"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->BloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "BloodGlucose"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->BloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 23
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "Weight"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    const-string v1, "UV"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->UV:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    .line 15
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Stress:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Sleep:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->BloodPressure:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->BloodGlucose:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->UV:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    return-object v0
.end method
