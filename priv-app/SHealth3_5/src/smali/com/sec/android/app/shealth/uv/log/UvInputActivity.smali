.class public Lcom/sec/android/app/shealth/uv/log/UvInputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "UvInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/uv/log/UvInputActivity$LayoutHeightAdjuster;
    }
.end annotation


# instance fields
.field private mComment:Ljava/lang/String;

.field private mId:J

.field private mSpfLayout:Landroid/widget/LinearLayout;

.field private mSunProtectionLayout:Landroid/widget/LinearLayout;

.field private mUvDate:Landroid/widget/TextView;

.field private mUvIcon:Landroid/widget/ImageView;

.field private mUvIndex:Landroid/widget/TextView;

.field private mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

.field private mUvRecomendedTime:J

.field private mUvSPF:I

.field private mUvSPFValue:Landroid/widget/TextView;

.field private mUvScore:D

.field private mUvState:Landroid/widget/TextView;

.field private mUvSunProtectionTimeValue:Landroid/widget/TextView;

.field private mUvTime:Landroid/widget/TextView;

.field private spfDivier:Landroid/view/View;

.field private sunProtectionDivider:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    .line 184
    return-void
.end method


# virtual methods
.method protected getContentView()Landroid/view/View;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 92
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 93
    .local v0, "model":Ljava/lang/String;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-gt v2, v3, :cond_0

    const-string v2, "SM-G9098"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-G9092"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    const v2, 0x7f0a06a8

    invoke-static {p0, v2}, Lcom/sec/android/app/shealth/heartrate/utils/SupportUtils;->setScrollContainerHeight(Landroid/app/Activity;I)V

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "HEART_RATE_ID_KEY"

    invoke-virtual {v2, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mId:J

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "MODE_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mMode:Ljava/lang/String;

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "COMMENT_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mComment:Ljava/lang/String;

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SCORE_KEY"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvScore:D

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "RECOMENDED_TIME_KEY"

    invoke-virtual {v2, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvRecomendedTime:J

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SUN_PROTECTION_KEY"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvSPF:I

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mMemoHeader:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f09007c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0901fd

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 107
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03025d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 108
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0809cf

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvDate:Landroid/widget/TextView;

    .line 109
    const v2, 0x7f080a9e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvTime:Landroid/widget/TextView;

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvDate:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "Date"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "TIME_DATE_KEY"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvDate:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "DATE_TALK_KEY"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    const v2, 0x7f080a94

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvState:Landroid/widget/TextView;

    .line 119
    const v2, 0x7f080a9f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvIcon:Landroid/widget/ImageView;

    .line 122
    const v2, 0x7f080a99

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvSPFValue:Landroid/widget/TextView;

    .line 123
    const v2, 0x7f080a9d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvSunProtectionTimeValue:Landroid/widget/TextView;

    .line 125
    const v2, 0x7f080a95

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->spfDivier:Landroid/view/View;

    .line 126
    const v2, 0x7f080a9a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->sunProtectionDivider:Landroid/view/View;

    .line 128
    const v2, 0x7f080a9b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mSunProtectionLayout:Landroid/widget/LinearLayout;

    .line 129
    const v2, 0x7f080a96

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mSpfLayout:Landroid/widget/LinearLayout;

    .line 131
    iget-wide v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvRecomendedTime:J

    sget-object v4, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mSunProtectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->sunProtectionDivider:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 140
    :goto_0
    iget v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvSPF:I

    int-to-long v2, v2

    sget-object v4, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->spfDivier:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mSpfLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 148
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvState:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v4

    iget-wide v5, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvScore:D

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getUvIndexString(ZD)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 149
    return-object v1

    .line 135
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mSunProtectionLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->sunProtectionDivider:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvSunProtectionTimeValue:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvRecomendedTime:J

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->calculateSafetyTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 144
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->spfDivier:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mSpfLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvSPFValue:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvSPF:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected isInputChanged()Z
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mComment:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v1, 0x7f0803e8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v2, Lcom/sec/android/app/shealth/uv/log/UvInputActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity$1;-><init>(Lcom/sec/android/app/shealth/uv/log/UvInputActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 74
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 75
    .local v0, "model":Ljava/lang/String;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-gt v1, v2, :cond_1

    const-string v1, "SM-G9098"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G9092"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity$LayoutHeightAdjuster;->assistActivity(Landroid/app/Activity;)V

    .line 88
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onDestroy()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    if-eqz v0, :cond_0

    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mUvPluginUtils:Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    .line 175
    :cond_0
    return-void
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 4
    .param p1, "isAdd"    # Z

    .prologue
    .line 154
    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->isInputChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090835

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 158
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 160
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/log/UvInputActivity;->startActivity(Landroid/content/Intent;)V

    .line 161
    return-void
.end method
