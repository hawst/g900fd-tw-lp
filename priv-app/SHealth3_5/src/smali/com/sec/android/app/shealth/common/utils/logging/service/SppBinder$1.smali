.class Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;
.super Ljava/lang/Object;
.source "SppBinder.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 82
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "IDlc service is connected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    invoke-static {p2}, Lcom/sec/spp/push/dlc/api/IDlcService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/dlc/api/IDlcService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$102(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$200(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    .line 85
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ConMsgHandler is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$200(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 93
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "IDlc service is disconnected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mIDlcService:Lcom/sec/spp/push/dlc/api/IDlcService;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$102(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$200(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    .line 96
    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ConMsgHandler is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder$1;->this$0:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    # getter for: Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->access$200(Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
