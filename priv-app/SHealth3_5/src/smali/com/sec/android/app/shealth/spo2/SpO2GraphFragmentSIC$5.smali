.class Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;
.super Ljava/lang/Object;
.source "SpO2GraphFragmentSIC.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)V
    .locals 0

    .prologue
    .line 685
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/4 v6, 0x1

    .line 726
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v3

    if-nez v3, :cond_0

    .line 727
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v2

    .line 729
    .local v2, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 731
    const/high16 v3, 0x427c0000    # 63.0f

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 732
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0057

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0058

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 733
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 734
    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 735
    const/4 v1, 0x0

    .line 736
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$400(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v3, v4, :cond_2

    .line 737
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 738
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const/high16 v4, 0x426c0000    # 59.0f

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 739
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const-string v4, "HH:mm"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 740
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const-string v4, "HH"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 745
    :goto_0
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 746
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 748
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 751
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    if-eqz p1, :cond_1

    .line 752
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$700(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->update(Ljava/util/ArrayList;)V

    .line 754
    :cond_1
    return-void

    .line 742
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 743
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const/high16 v4, 0x41f80000    # 31.0f

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0
.end method

.method public OnReleaseTimeOut()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 714
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 715
    .local v0, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const-string v1, "Handler info"

    const-string v2, "Release Timeout"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 717
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 718
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 719
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 721
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 722
    return-void
.end method

.method public OnVisible()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 689
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v2

    .line 691
    .local v2, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemVisible(Z)V

    .line 692
    const/4 v1, 0x0

    .line 693
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$400(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v3, v4, :cond_0

    .line 694
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 695
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const/high16 v4, 0x426c0000    # 59.0f

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 696
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const-string v4, "HH:mm"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 697
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const-string v4, "HH"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 702
    :goto_0
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 703
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 704
    const/high16 v3, 0x427c0000    # 63.0f

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 705
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0057

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0058

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 706
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 707
    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextVisible(Z)V

    .line 709
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 710
    return-void

    .line 699
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 700
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v3

    const/high16 v4, 0x41f80000    # 31.0f

    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0
.end method
