.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V
    .locals 0

    .prologue
    .line 916
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 920
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 921
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_2

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mAppID:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MI02"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$2100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 925
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->startLocalMigration()V

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 927
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->isConfirmed:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Z)Z

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->startLocalMigration()V

    goto :goto_0

    .line 933
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 935
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->exitApplication()V

    goto :goto_0
.end method
