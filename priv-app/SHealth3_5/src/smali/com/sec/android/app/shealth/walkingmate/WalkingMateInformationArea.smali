.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "WalkingMateInformationArea.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final MONTHS:[Ljava/lang/String;

.field cal:Ljava/util/Calendar;

.field dateFormatter:Ljava/text/DateFormat;

.field private date_time:Landroid/widget/TextView;

.field private firstInforvalue:Ljava/lang/String;

.field private first_info_data:Landroid/widget/TextView;

.field private first_info_title:Landroid/widget/TextView;

.field private first_info_unit:Landroid/widget/TextView;

.field protected hourFormat:Ljava/text/DateFormat;

.field private hour_month_day_view:Landroid/widget/RelativeLayout;

.field private informationAreaparent:Landroid/widget/LinearLayout;

.field private isFirstInfoValueSet:Z

.field private isSecondInfoValueSet:Z

.field private mAvgTotalStepHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private mHealthyStepHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field protected mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private month_time:J

.field private oneDForm:Ljava/text/DecimalFormat;

.field private pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

.field private secondInforvalue:Ljava/lang/String;

.field private second_info_area:Landroid/widget/LinearLayout;

.field private second_info_data:Landroid/widget/TextView;

.field private second_info_title:Landroid/widget/TextView;

.field private second_info_unit:Landroid/widget/TextView;

.field private time_str:Ljava/lang/String;

.field private time_str_description:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p3, "isSupportHealhtyPace"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->MONTHS:[Ljava/lang/String;

    .line 44
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->cal:Ljava/util/Calendar;

    .line 45
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->dateFormatter:Ljava/text/DateFormat;

    .line 65
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isFirstInfoValueSet:Z

    .line 66
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    .line 70
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->oneDForm:Ljava/text/DecimalFormat;

    .line 71
    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mHealthyStepHashMap:Ljava/util/HashMap;

    .line 72
    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mAvgTotalStepHashMap:Ljava/util/HashMap;

    .line 77
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 78
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->initLayout()V

    .line 80
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method private initLayout()V
    .locals 4

    .prologue
    .line 93
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v3, " "

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const v2, 0x7f080357

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    .line 96
    const v2, 0x7f080b9e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->hour_month_day_view:Landroid/widget/RelativeLayout;

    .line 98
    const v2, 0x7f080358

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->date_time:Landroid/widget/TextView;

    .line 100
    const v2, 0x7f0803b7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 101
    const v2, 0x7f080ba6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    .line 103
    const v2, 0x7f08035b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_title:Landroid/widget/TextView;

    .line 104
    const v2, 0x7f08035c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_data:Landroid/widget/TextView;

    .line 106
    const v2, 0x7f080ba1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "firstUnit":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 112
    const v2, 0x7f080ba2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_area:Landroid/widget/LinearLayout;

    .line 113
    const v2, 0x7f08035f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_title:Landroid/widget/TextView;

    .line 114
    const v2, 0x7f080360

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_data:Landroid/widget/TextView;

    .line 115
    const v2, 0x7f080ba4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "secondUnit":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_area:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 120
    .end local v1    # "secondUnit":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 2

    .prologue
    .line 233
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 236
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030291

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 6

    .prologue
    const v5, 0x7f09006d

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 132
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v2, " "

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->date_time:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_title:Landroid/widget/TextView;

    const v2, 0x7f090b66

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 138
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isFirstInfoValueSet:Z

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_data:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    if-eqz v1, :cond_1

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_title:Landroid/widget/TextView;

    const v2, 0x7f0907e4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_data:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_4

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_title:Landroid/widget/TextView;

    const v2, 0x7f0907e6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str_description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_title:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    if-eqz v1, :cond_2

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->hour_month_day_view:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->hour_month_day_view:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->I(Ljava/lang/String;)V

    .line 174
    .end local v0    # "description":Ljava/lang/String;
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 176
    return-void

    .line 157
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_6

    .line 158
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str_description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_title:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 159
    .restart local v0    # "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    if-eqz v1, :cond_5

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->hour_month_day_view:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 165
    .end local v0    # "description":Ljava/lang/String;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_3

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_unit:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str_description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->first_info_title:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168
    .restart local v0    # "description":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    if-eqz v1, :cond_7

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_unit:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->hour_month_day_view:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public setAvgTotalStepData(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Double;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mAvgTotalStepHashMap:Ljava/util/HashMap;

    .line 89
    return-void
.end method

.method public setDate(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 12
    .param p1, "realX"    # J
    .param p3, "periodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v10, 0x2

    const/4 v11, 0x0

    .line 180
    sget-object v7, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v8, " "

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 182
    .local v0, "date":Ljava/util/Date;
    iput-object p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 186
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v7, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str:Ljava/lang/String;

    .line 187
    sget-object v7, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Selected timestamp2 : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v7, :cond_1

    .line 191
    const-wide/32 v7, 0x124f80

    add-long v3, p1, v7

    .line 192
    .local v3, "range":J
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 193
    .local v1, "daterange":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->hourFormat:Ljava/text/DateFormat;

    .line 194
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->hourFormat:Ljava/text/DateFormat;

    invoke-virtual {v7, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 195
    .local v5, "toHourString":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f090bdc

    new-array v9, v10, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str:Ljava/lang/String;

    aput-object v10, v9, v11

    const/4 v10, 0x1

    aput-object v5, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str_description:Ljava/lang/String;

    .line 196
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ~ "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str:Ljava/lang/String;

    .line 208
    .end local v1    # "daterange":Ljava/util/Date;
    .end local v3    # "range":J
    .end local v5    # "toHourString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v7, :cond_2

    .line 200
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str_description:Ljava/lang/String;

    goto :goto_0

    .line 201
    :cond_2
    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v7, :cond_0

    .line 203
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->cal:Ljava/util/Calendar;

    iget-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->month_time:J

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 204
    new-instance v7, Ljava/text/DateFormatSymbols;

    invoke-direct {v7}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v7}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->cal:Ljava/util/Calendar;

    invoke-virtual {v8, v10}, Ljava/util/Calendar;->get(I)I

    move-result v8

    aget-object v2, v7, v8

    .line 205
    .local v2, "monthFullName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->dateFormatter:Ljava/text/DateFormat;

    iget-wide v8, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->month_time:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 206
    .local v6, "year":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->time_str_description:Ljava/lang/String;

    goto :goto_0
.end method

.method public setFirstInformation(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 212
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    if-eqz p1, :cond_0

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isFirstInfoValueSet:Z

    .line 216
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->firstInforvalue:Ljava/lang/String;

    .line 218
    :cond_0
    return-void
.end method

.method public setHealthyStepData(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Double;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mHealthyStepHashMap:Ljava/util/HashMap;

    .line 85
    return-void
.end method

.method public setPieChart(DD)V
    .locals 8
    .param p1, "v1"    # D
    .param p3, "v2"    # D

    .prologue
    const/4 v7, 0x0

    const-wide/16 v5, 0x0

    .line 239
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "v1 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "v2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    cmpg-double v2, p1, v5

    if-gtz v2, :cond_0

    cmpg-double v2, p3, v5

    if-gtz v2, :cond_0

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setVisibility(I)V

    .line 257
    :goto_0
    return-void

    .line 246
    :cond_0
    const/4 v2, 0x2

    new-array v0, v2, [I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701a7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v7

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0701a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 249
    .local v0, "colors":[I
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 250
    .local v1, "values":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 251
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setColors([I)V

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setValues(Ljava/util/Vector;)V

    .line 254
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->refreshValuesToColorPairs()V

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSecondInformation(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 222
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    if-eqz p1, :cond_0

    .line 225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    .line 226
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->secondInforvalue:Ljava/lang/String;

    .line 228
    :cond_0
    return-void
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 14
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 261
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v10, " "

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_7

    .line 266
    const-string v10, "grapth selectedTime : "

    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->printDate(Ljava/lang/String;J)V

    .line 268
    const-wide/16 v6, 0x0

    .line 269
    .local v6, "totalStep":D
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_3

    .line 270
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v9

    sput-wide v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    iput-wide v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->month_time:J

    .line 271
    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mAvgTotalStepHashMap:Ljava/util/HashMap;

    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    .line 272
    .local v8, "totalStepData":Ljava/lang/Double;
    if-eqz v8, :cond_0

    .line 273
    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 282
    .end local v8    # "totalStepData":Ljava/lang/Double;
    :cond_0
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    iget-object v11, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mPeriodH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v9, v10, v11}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setDate(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 284
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 285
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    double-to-int v13, v6

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setFirstInformation(Ljava/lang/String;)V

    .line 290
    :goto_0
    iget-boolean v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->isSecondInfoValueSet:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mHealthyStepHashMap:Ljava/util/HashMap;

    if-eqz v9, :cond_2

    .line 292
    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->mHealthyStepHashMap:Ljava/util/HashMap;

    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    .line 293
    .local v2, "healthyStepData":Ljava/lang/Double;
    const-wide/16 v0, 0x0

    .line 295
    .local v0, "healthyStep":D
    if-eqz v2, :cond_1

    .line 296
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 299
    :cond_1
    sub-double v4, v6, v0

    .line 302
    .local v4, "normalStep":D
    invoke-virtual {p0, v4, v5, v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setPieChart(DD)V

    .line 304
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 305
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    double-to-int v13, v0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setSecondInformation(Ljava/lang/String;)V

    .line 309
    :goto_1
    const-wide/16 v9, 0x0

    cmpl-double v9, v0, v9

    if-nez v9, :cond_6

    .line 310
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setVisibility(I)V

    .line 316
    .end local v0    # "healthyStep":D
    .end local v2    # "healthyStepData":Ljava/lang/Double;
    .end local v4    # "normalStep":D
    :cond_2
    :goto_2
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Handler Data : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->refreshInformationAreaView()V

    .line 322
    .end local v6    # "totalStep":D
    :goto_3
    return-void

    .line 276
    .restart local v6    # "totalStep":D
    :cond_3
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sput-wide v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->selectedTime:J

    iput-wide v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->month_time:J

    .line 277
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_4
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v9

    if-ge v3, v9, :cond_0

    .line 278
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Double;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    add-double/2addr v6, v9

    .line 277
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 287
    .end local v3    # "i":I
    :cond_4
    const-string v9, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    double-to-int v12, v6

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setFirstInformation(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 307
    .restart local v0    # "healthyStep":D
    .restart local v2    # "healthyStepData":Ljava/lang/Double;
    .restart local v4    # "normalStep":D
    :cond_5
    const-string v9, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    double-to-int v12, v0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->setSecondInformation(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 312
    :cond_6
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->pieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setVisibility(I)V

    goto/16 :goto_2

    .line 320
    .end local v0    # "healthyStep":D
    .end local v2    # "healthyStepData":Ljava/lang/Double;
    .end local v4    # "normalStep":D
    .end local v6    # "totalStep":D
    :cond_7
    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/WalkingMateInformationArea;->TAG:Ljava/lang/String;

    const-string v10, "NULL"

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
