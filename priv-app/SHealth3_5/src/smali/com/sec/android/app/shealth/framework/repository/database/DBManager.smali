.class public Lcom/sec/android/app/shealth/framework/repository/database/DBManager;
.super Ljava/lang/Object;
.source "DBManager.java"


# static fields
.field public static HPASSWD:[B

.field private static final TAG:Ljava/lang/String;

.field private static _context:Landroid/content/Context;

.field private static instance:Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

.field private static mDatabaseHelper:Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;

.field private static mIsInitialized:Z


# instance fields
.field private mIsSecure:Z

.field private securepasswd:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "App_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->TAG:Ljava/lang/String;

    .line 23
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->instance:Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    .line 26
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mIsInitialized:Z

    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->HPASSWD:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mIsSecure:Z

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->securepasswd:[B

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->TAG:Ljava/lang/String;

    const-string v1, "created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->_context:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/framework/repository/database/DBManager;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->instance:Lcom/sec/android/app/shealth/framework/repository/database/DBManager;

    return-object v0
.end method


# virtual methods
.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 7

    .prologue
    .line 97
    sget-boolean v2, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mIsInitialized:Z

    if-nez v2, :cond_0

    .line 99
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Invalid State. Check CP initialization"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 102
    :cond_0
    const/4 v1, 0x0

    .line 104
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mIsSecure:Z

    if-eqz v2, :cond_3

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->securepasswd:[B

    if-nez v2, :cond_2

    .line 108
    monitor-enter p0

    .line 110
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/service/health/cp/common/ContentConstants$TrustzoneConstants;->TRUSTZONE_AUTHORITY_URI:Landroid/net/Uri;

    const-string v4, ""

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 111
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 113
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Exception occured while getting secure passwd from TrustZone"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 116
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 115
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    :try_start_1
    const-string/jumbo v2, "securepasswd"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->securepasswd:[B

    .line 116
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->TAG:Ljava/lang/String;

    const-string v3, "getWritableDatabase trustzone with securepasswdd "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mDatabaseHelper:Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->securepasswd:[B

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 126
    :goto_0
    const-string v2, "PRAGMA foreign_keys=ON;"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 127
    return-object v1

    .line 123
    :cond_3
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mDatabaseHelper:Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    sget-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 38
    sput-object p1, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->_context:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mDatabaseHelper:Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;

    .line 40
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mIsInitialized:Z

    .line 42
    :cond_0
    return-void
.end method

.method public initialize(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "databaseName"    # Ljava/lang/String;

    .prologue
    .line 46
    sput-object p1, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->_context:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mDatabaseHelper:Lcom/sec/android/app/shealth/framework/repository/database/DatabaseHelper;

    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->mIsInitialized:Z

    .line 49
    return-void
.end method
