.class Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$4;
.super Ljava/lang/Object;
.source "DeleteUserDataActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 233
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "S011"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$4;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getAccountPasswordVerifyIntent()Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x1c6d

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 238
    :cond_0
    return-void
.end method
