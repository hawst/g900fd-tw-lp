.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;
.super Ljava/lang/Object;
.source "HeartRateScoverActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->showInfomationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 5
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 367
    const v3, 0x7f0805b0

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 368
    .local v0, "mCancelErrorDialog":Landroid/widget/Button;
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    const v3, 0x7f0805b1

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v4, v3}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$402(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 369
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    const v3, 0x7f0804ee

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4, v3}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$502(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 370
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-object v4, v4, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 371
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mShowAgainCheckLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;)Landroid/widget/RelativeLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;

    iget-object v4, v4, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 373
    const v3, 0x7f0805af

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 375
    .local v1, "mTitle":Landroid/widget/TextView;
    const v3, 0x7f0900e3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 377
    const v3, 0x7f0805b2

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 378
    .local v2, "okButoonLayout":Landroid/widget/TextView;
    new-instance v3, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5$1;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    new-instance v3, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5$2;-><init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverActivity$5;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 404
    return-void
.end method
