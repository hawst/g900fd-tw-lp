.class Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ScalableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/gallery/ScalableView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DoubleTapListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;-><init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    .line 225
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 226
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 227
    .local v2, "y":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v3

    cmpg-float v3, v3, v6

    if-gtz v3, :cond_0

    .line 228
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    const/high16 v4, 0x40400000    # 3.0f

    # setter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$302(Lcom/sec/android/app/shealth/common/gallery/ScalableView;F)F

    .line 229
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    invoke-virtual {v3, v4, v5, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 239
    :goto_0
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v3

    return v3

    .line 231
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v3

    div-float v0, v6, v3

    .line 232
    .local v0, "restoreScale":F
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1100(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v4

    div-float/2addr v4, v7

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F

    move-result v5

    div-float/2addr v5, v7

    invoke-virtual {v3, v0, v0, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # setter for: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F
    invoke-static {v3, v6}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$302(Lcom/sec/android/app/shealth/common/gallery/ScalableView;F)F

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;->this$0:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    # invokes: Lcom/sec/android/app/shealth/common/gallery/ScalableView;->centeredBitmapResource()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->access$1500(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V

    goto :goto_0
.end method
