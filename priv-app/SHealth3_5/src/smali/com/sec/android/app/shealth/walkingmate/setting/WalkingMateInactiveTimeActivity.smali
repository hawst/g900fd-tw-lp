.class public Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateInactiveTimeActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;
    }
.end annotation


# static fields
.field private static final INACTIVE_TIME_FRAGMENT:Ljava/lang/String; = "inactive_time_fragment"

.field private static final TAG:Ljava/lang/String; = "WalkingMateInactiveTimeActivity"


# instance fields
.field private mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;)Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    return-object v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 39
    return-void
.end method

.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mProcessClick:Z

    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 64
    const-string v0, "INACTIVE_TIME_SET_DIALOG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity$PeriodChosenItemListener;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;)V

    .line 68
    :goto_0
    return-object v0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->mProcessClick:Z

    .line 68
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v1, 0x7f030294

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->setContentView(I)V

    .line 29
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 31
    .local v0, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f080ba7

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    const-string v3, "inactive_time_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 32
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 34
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;->mWalkingMateInactiveTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeFragment;->setCurrentInactiveConfig()V

    .line 76
    return-void
.end method
