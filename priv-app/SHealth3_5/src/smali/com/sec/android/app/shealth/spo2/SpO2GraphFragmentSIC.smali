.class public Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
.super Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
.source "SpO2GraphFragmentSIC.java"


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field private static final FRAGMENT_SWITCHING_DELAY:I = 0xc8

.field public static final HOURS_IN_DAY:B = 0x18t

.field private static MARKING_COUNT:I = 0x0

.field private static MAX_Y:F = 0.0f

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field private static final MIN_Y:F = 0.0f

.field private static final PATTERN_24_HOURS:Ljava/lang/String; = "HH"

.field private static final PATTERN_DAY_FORMAT:Ljava/lang/String; = "dd"

.field private static final PATTERN_MONTH_FORMAT:Ljava/lang/String; = "MM"

.field private static final PATTERN_TIME:Ljava/lang/String; = "HH:mm"

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static volatile mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# instance fields
.field private chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

.field private density:F

.field private isConfigurationChanged:Z

.field private mContext:Landroid/content/Context;

.field private mDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field

.field private mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field private mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

.field private mLegend:Landroid/widget/LinearLayout;

.field private mLegendPulseLabel:Landroid/widget/TextView;

.field private mLegendSpo2Label:Landroid/widget/TextView;

.field private mLegendView:Landroid/view/View;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mPoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field

.field private mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mSeries1:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mSpO2DataCount:I

.field private mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

.field private switchToSpO2Summary:Landroid/widget/ImageButton;

.field private timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/high16 v0, 0x42f00000    # 120.0f

    sput v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->MAX_Y:F

    .line 71
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->MARKING_COUNT:I

    .line 81
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;-><init>()V

    .line 72
    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 73
    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries1:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 82
    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegend:Landroid/widget/LinearLayout;

    .line 83
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mContext:Landroid/content/Context;

    .line 685
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$5;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->switchToSpO2Summary:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->showSummarayFragment()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mReadyToShown:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    return-object v0
.end method

.method private initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 17
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 563
    const-string v12, "SpO2GraphFragmentSIC"

    const-string v13, "initChartStyle"

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    new-instance v9, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 565
    .local v9, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a004e

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v9, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 566
    const/4 v12, 0x1

    invoke-virtual {v9, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 567
    const/16 v12, 0xff

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v9, v12, v13, v14, v15}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 568
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 569
    const-string v12, "font/Roboto-Light.ttf"

    invoke-virtual {v9, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 571
    new-instance v11, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v11}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 572
    .local v11, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0050

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 573
    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 574
    const/16 v12, 0xff

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v11, v12, v13, v14, v15}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 575
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 576
    const-string v12, "font/Roboto-Light.ttf"

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 579
    new-instance v10, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 580
    .local v10, "yLabelTitleStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0052

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v10, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 581
    const/4 v12, 0x1

    invoke-virtual {v10, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 582
    const/16 v12, 0xff

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v10, v12, v13, v14, v15}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 583
    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 584
    const-string v12, "font/Roboto-Light.ttf"

    invoke-virtual {v10, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 587
    new-instance v5, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 588
    .local v5, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0053

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v5, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 589
    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 590
    const/16 v12, 0xff

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v5, v12, v13, v14, v15}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 591
    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 593
    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/high16 v15, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13, v14, v15}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 596
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0203b2

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 597
    .local v2, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 598
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 600
    const v12, -0xa0a0b

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 603
    const/high16 v12, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    mul-float/2addr v12, v13

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 604
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 605
    const v12, -0xcbb1ec

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 606
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 607
    const/high16 v12, 0x41f00000    # 30.0f

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 613
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 614
    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 615
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 616
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    sget-object v13, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 617
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090fa1

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 619
    :cond_0
    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 620
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 621
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    const/high16 v13, 0x41000000    # 8.0f

    mul-float/2addr v12, v13

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 622
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 624
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    sget-object v13, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 625
    const/4 v12, 0x2

    new-array v6, v12, [Ljava/lang/String;

    .line 626
    .local v6, "labels":[Ljava/lang/String;
    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090a69

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v12

    .line 627
    const/4 v12, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090a6b

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v12

    .line 628
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisCustomLabel([Ljava/lang/String;I)V

    .line 630
    .end local v6    # "labels":[Ljava/lang/String;
    :cond_1
    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 631
    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 632
    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 633
    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 634
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 635
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const v13, 0x7f0900d2

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 636
    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 637
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 638
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    const/high16 v13, 0x41000000    # 8.0f

    mul-float/2addr v12, v13

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 642
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v13, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v12, v13, :cond_2

    .line 643
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0203c3

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 644
    .restart local v2    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    const/high16 v12, 0x426c0000    # 59.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    mul-float/2addr v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 645
    const-string v12, "HH:mm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 646
    const-string v12, "HH"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 652
    :goto_0
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 653
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 655
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 656
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 657
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 658
    const/high16 v12, 0x427c0000    # 63.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    mul-float/2addr v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 659
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0057

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    int-to-float v12, v12

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v12, v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0a0058

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    int-to-float v13, v13

    add-float/2addr v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 660
    const/high16 v12, 0x41a00000    # 20.0f

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 661
    const-wide/16 v12, 0x1388

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 662
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerAnimation(Z)V

    .line 663
    const-wide/16 v12, 0x3e8

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerAnimationTime(J)V

    .line 664
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 667
    const/16 v12, 0x12c

    const/16 v13, 0x12c

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 668
    .local v7, "lineBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0203c4

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 669
    .local v4, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 670
    .local v3, "canvas":Landroid/graphics/Canvas;
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v14

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v15

    invoke-virtual {v4, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 671
    invoke-virtual {v4, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 672
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 674
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 675
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 676
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 677
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    const/high16 v13, 0x42080000    # 34.0f

    mul-float/2addr v12, v13

    float-to-int v12, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 678
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 680
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v8

    .line 681
    .local v8, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v12, 0x50

    invoke-virtual {v8, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 682
    const/high16 v12, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    mul-float/2addr v12, v13

    invoke-virtual {v8, v12}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 683
    return-void

    .line 648
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v7    # "lineBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0203c2

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 649
    .restart local v2    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    const/high16 v12, 0x41f80000    # 31.0f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    mul-float/2addr v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto/16 :goto_0
.end method

.method private initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 17
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 486
    const-string v13, "SpO2GraphFragmentSIC"

    const-string v14, "initSeriesStyle"

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    new-instance v9, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 488
    .local v9, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    new-instance v10, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v10}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 489
    .local v10, "seriesStyle1":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const-string/jumbo v13, "spo2"

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 490
    const-string v13, "bpm"

    invoke-virtual {v10, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 492
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0203c0

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 493
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 495
    .local v6, "normalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0203c1

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 496
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 498
    .local v2, "handlerOverBitmap":Landroid/graphics/Bitmap;
    new-instance v5, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 499
    .local v5, "lineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0a0053

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 500
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 501
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x106000c

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 502
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 503
    const-string v13, "font/Roboto-Light.ttf"

    invoke-virtual {v5, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 505
    new-instance v4, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 506
    .local v4, "lineLabelStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0a0052

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 507
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 508
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x106000c

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v4, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 509
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 510
    const-string v13, "font/Roboto-Light.ttf"

    invoke-virtual {v4, v13}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 512
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 513
    const/4 v13, 0x1

    const/4 v14, 0x1

    const/high16 v15, 0x428c0000    # 70.0f

    const/high16 v16, 0x42ca0000    # 101.0f

    move/from16 v0, v16

    invoke-virtual {v9, v13, v14, v15, v0}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 514
    :cond_0
    invoke-virtual {v9, v5}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 515
    invoke-virtual {v9, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 516
    const/4 v13, 0x1

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 517
    invoke-virtual {v9, v6}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 518
    invoke-virtual {v9, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 519
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v13

    iget v8, v13, Landroid/util/DisplayMetrics;->density:F

    .line 520
    .local v8, "scale":F
    const/high16 v13, 0x40a00000    # 5.0f

    mul-float/2addr v13, v8

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 521
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v14, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v13, v14, :cond_2

    .line 522
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f07008a

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 527
    :goto_0
    const/high16 v13, 0x40a00000    # 5.0f

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 528
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f07008b

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalOverColor(I)V

    .line 529
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setEnableDisconnectByTime(Z)V

    .line 531
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0203cb

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 532
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 534
    .local v7, "normalBitmap2":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0203cc

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 535
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 537
    .local v3, "handlerOverBitmap2":Landroid/graphics/Bitmap;
    const/4 v13, 0x1

    invoke-virtual {v10, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 538
    invoke-virtual {v10, v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 539
    invoke-virtual {v10, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 540
    const/high16 v13, 0x40a00000    # 5.0f

    mul-float/2addr v13, v8

    invoke-virtual {v10, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 541
    const/4 v13, 0x1

    const/4 v14, 0x1

    const/4 v15, 0x0

    sget v16, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->MAX_Y:F

    move/from16 v0, v16

    invoke-virtual {v10, v13, v14, v15, v0}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 542
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v14, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v13, v14, :cond_3

    .line 543
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f07008a

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v10, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 548
    :goto_1
    const/high16 v13, 0x40a00000    # 5.0f

    invoke-virtual {v10, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 550
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 551
    const/4 v13, 0x4

    new-array v11, v13, [I

    fill-array-data v11, :array_0

    .line 552
    .local v11, "visibleIndices":[I
    const/4 v13, 0x1

    const/16 v14, 0x20

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v11, v15}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisUseManualMarking(ZI[II)V

    .line 555
    .end local v11    # "visibleIndices":[I
    :cond_1
    const/4 v13, 0x5

    new-array v12, v13, [I

    fill-array-data v12, :array_1

    .line 556
    .local v12, "visibleIndices2":[I
    const/4 v13, 0x1

    const/4 v14, 0x7

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v12, v15}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisUseManualMarking(ZI[II)V

    .line 558
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 559
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 560
    return-void

    .line 524
    .end local v3    # "handlerOverBitmap2":Landroid/graphics/Bitmap;
    .end local v7    # "normalBitmap2":Landroid/graphics/Bitmap;
    .end local v12    # "visibleIndices2":[I
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f07023a

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v9, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    goto/16 :goto_0

    .line 545
    .restart local v3    # "handlerOverBitmap2":Landroid/graphics/Bitmap;
    .restart local v7    # "normalBitmap2":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f07023b

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v10, v13}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    goto :goto_1

    .line 551
    :array_0
    .array-data 4
        0x0
        0xa
        0x14
        0x1e
    .end array-data

    .line 555
    :array_1
    .array-data 4
        0x0
        0x3
        0x4
        0x5
        0x6
    .end array-data
.end method

.method private initTimeLineChartView(Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 8
    .param p1, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    const/4 v4, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 337
    const-string v2, "SpO2GraphFragmentSIC"

    const-string v3, "initTimeLineChartView"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    if-eqz v2, :cond_0

    .line 339
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v1

    .line 340
    .local v1, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 341
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 342
    iput-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .line 345
    .end local v1    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {v2, v3, v4, p1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .line 346
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    new-instance v3, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$4;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setReadyToShowListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;)V

    .line 356
    new-instance v0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 357
    .local v0, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 358
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 359
    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 361
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_1

    .line 362
    const/high16 v2, 0x40e00000    # 7.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 363
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 364
    invoke-virtual {v0, v5, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalLimit(II)V

    .line 365
    invoke-virtual {v0, v5, v5}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 371
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v2, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 374
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v2, v6, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 376
    return-void

    .line 367
    :cond_1
    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 368
    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_0
.end method

.method private isDataCountChanged()Z
    .locals 2

    .prologue
    .line 171
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getDataCount()I

    move-result v0

    .line 172
    .local v0, "spO2DataCount":I
    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSpO2DataCount:I

    if-ne v0, v1, :cond_0

    .line 173
    const/4 v1, 0x0

    .line 176
    :goto_0
    return v1

    .line 175
    :cond_0
    iput v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSpO2DataCount:I

    .line 176
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private setChartData(Lcom/samsung/android/sdk/chart/series/SchartDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 7
    .param p1, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 444
    const-string v5, "SpO2GraphFragmentSIC"

    const-string v6, "addData"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 446
    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries1:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 447
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-virtual {v5, p2}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getSpO2GraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;

    move-result-object v4

    .line 449
    .local v4, "spO2Datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/spo2/data/SpO2Data;>;"
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    .line 450
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    .line 454
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPoDatas:Ljava/util/ArrayList;

    if-nez v5, :cond_1

    .line 455
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPoDatas:Ljava/util/ArrayList;

    .line 460
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 461
    .local v1, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v1, :cond_5

    .line 462
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 463
    .local v0, "bpmdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    new-instance v3, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 465
    .local v3, "podata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setTime(J)V

    .line 466
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v5, :cond_2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getSpo2()F

    move-result v5

    float-to-double v5, v5

    :goto_3
    invoke-virtual {v3, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setValue(D)V

    .line 467
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setTime(J)V

    .line 468
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v5, :cond_3

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getHeartrate()F

    move-result v5

    float-to-double v5, v5

    :goto_4
    invoke-virtual {v0, v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setValue(D)V

    .line 469
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v5, :cond_4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getHeartrate()F

    move-result v5

    float-to-int v5, v5

    :goto_5
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setMaxY(I)V

    .line 472
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPoDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 452
    .end local v0    # "bpmdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v1    # "dataSize":I
    .end local v2    # "i":I
    .end local v3    # "podata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 457
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPoDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 466
    .restart local v0    # "bpmdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .restart local v1    # "dataSize":I
    .restart local v2    # "i":I
    .restart local v3    # "podata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_2
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getAvgSpO2()D

    move-result-wide v5

    goto :goto_3

    .line 468
    :cond_3
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getAvgHeartrate()D

    move-result-wide v5

    goto :goto_4

    .line 469
    :cond_4
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getAvgHeartrate()D

    move-result-wide v5

    double-to-int v5, v5

    goto :goto_5

    .line 476
    .end local v0    # "bpmdata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v3    # "podata":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPoDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->setAllData(Ljava/util/List;)V

    .line 477
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries1:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->setAllData(Ljava/util/List;)V

    .line 478
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 479
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries1:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 480
    return-void
.end method

.method private setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 2
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 379
    const-string v0, "SpO2GraphFragmentSIC"

    const-string/jumbo v1, "setDateFormat"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 381
    return-void
.end method

.method private setMaxY(I)V
    .locals 2
    .param p1, "compareY"    # I

    .prologue
    .line 782
    int-to-float v0, p1

    sget v1, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->MAX_Y:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 783
    div-int/lit8 p1, p1, 0xa

    .line 784
    add-int/lit8 p1, p1, 0x2

    .line 785
    mul-int/lit8 p1, p1, 0xa

    .line 786
    int-to-float v0, p1

    sput v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->MAX_Y:F

    .line 788
    :cond_0
    return-void
.end method

.method private setStartVisual(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 14
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const-wide/16 v12, 0x0

    .line 384
    const/4 v1, 0x0

    .line 385
    .local v1, "level":I
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_3

    .line 386
    const/4 v1, 0x0

    .line 393
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v0

    if-lez v0, :cond_d

    .line 394
    sget v5, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->MARKING_COUNT:I

    .line 395
    .local v5, "markingCount":I
    const/4 v4, 0x1

    .line 397
    .local v4, "intervel":I
    const-wide/16 v6, 0x0

    .line 398
    .local v6, "selectedTime":J
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_5

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v6

    .line 404
    :goto_1
    const-wide/16 v10, 0x0

    .line 405
    .local v10, "startTimes":J
    const-wide/16 v8, 0x0

    .line 407
    .local v8, "startTime":J
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_1

    .line 408
    sput-object p1, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 412
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_8

    .line 413
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;->getInstance()Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;

    move-result-object v2

    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v3, :cond_7

    const-string v0, "%Y-%m-%d"

    :goto_2
    invoke-virtual {v2, v0, v6, v7}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v10

    .line 414
    cmp-long v0, v10, v12

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v10

    .line 415
    :cond_2
    const-wide/32 v2, 0x2160ec0

    sub-long v2, v10, v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartHourToMillis(J)J

    move-result-wide v8

    .line 417
    const/16 v5, 0xd

    .line 418
    const/16 v4, 0x3c

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setScrollRangeDepthLevel(I)V

    .line 434
    :goto_3
    sput-object p1, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    long-to-double v2, v8

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    long-to-double v2, v10

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 441
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "selectedTime":J
    .end local v8    # "startTime":J
    .end local v10    # "startTimes":J
    :goto_4
    return-void

    .line 387
    :cond_3
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_4

    .line 388
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 389
    :cond_4
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 390
    const/4 v1, 0x5

    goto/16 :goto_0

    .line 401
    .restart local v4    # "intervel":I
    .restart local v5    # "markingCount":I
    .restart local v6    # "selectedTime":J
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getSelectedDateInChart()J

    move-result-wide v6

    :goto_5
    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v6

    goto :goto_5

    .line 413
    .restart local v8    # "startTime":J
    .restart local v10    # "startTimes":J
    :cond_7
    const-string v0, "%Y-%m"

    goto :goto_2

    .line 421
    :cond_8
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_b

    .line 422
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;->getInstance()Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;

    move-result-object v2

    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v3, :cond_a

    const-string v0, "%Y-%m-%d"

    :goto_6
    invoke-virtual {v2, v0, v6, v7}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v10

    .line 423
    cmp-long v0, v10, v12

    if-gtz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v10

    .line 424
    :cond_9
    const-wide/32 v2, 0x19bfcc00

    sub-long v8, v10, v2

    .line 425
    const/4 v5, 0x7

    goto/16 :goto_3

    .line 422
    :cond_a
    const-string v0, "%Y-%m"

    goto :goto_6

    .line 428
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;->getInstance()Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;

    move-result-object v0

    const-string v2, "%Y-%m"

    invoke-virtual {v0, v2, v6, v7}, Lcom/sec/android/app/shealth/spo2/utils/PulseOximeterPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v10

    .line 429
    cmp-long v0, v10, v12

    if-gtz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v10

    .line 430
    :cond_c
    const-wide v2, 0x59cce4400L

    sub-long v8, v10, v2

    .line 431
    const/16 v5, 0xc

    goto/16 :goto_3

    .line 439
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "selectedTime":J
    .end local v8    # "startTime":J
    .end local v10    # "startTimes":J
    :cond_d
    const v0, 0x7f0205b7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->showNoData(I)V

    goto/16 :goto_4
.end method

.method private showSummarayFragment()V
    .locals 5

    .prologue
    .line 237
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .line 238
    .local v1, "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->switchFragmentToSummary()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    return-void

    .line 239
    .end local v1    # "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of HeartrateActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method protected customizeActionBarButtons(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;Landroid/view/View$OnClickListener;)V
    .locals 6
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 211
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 212
    new-array v0, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207c0

    const v3, 0x7f09005a

    invoke-direct {v1, v2, v4, v3, p2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v1, v0, v4

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v0

    if-lez v0, :cond_0

    .line 214
    new-array v0, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207cf

    const v3, 0x7f090033

    invoke-direct {v1, v2, v4, v3, p2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v1, v0, v4

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 216
    :cond_0
    return-void
.end method

.method protected customizeActionBarDisable(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 203
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 204
    return-void
.end method

.method protected customizeActionBarShow(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 207
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 208
    return-void
.end method

.method protected getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 8
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 758
    const-string v2, "-"

    .line 759
    .local v2, "REGEX_ONE_OR_MORE":Ljava/lang/String;
    const-string v3, "/"

    .line 760
    .local v3, "SLASH":Ljava/lang/String;
    const-string v0, "d"

    .line 761
    .local v0, "DAY_CHAR":Ljava/lang/String;
    const-string v1, "M"

    .line 762
    .local v1, "MONTH_CHAR":Ljava/lang/String;
    const-string/jumbo v4, "yyyy"

    .line 764
    .local v4, "YEAR_STRING":Ljava/lang/String;
    const-string v5, ""

    .line 765
    .local v5, "dateFormat":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "date_format"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 766
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v6, :cond_0

    .line 767
    const-string v6, "d"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 768
    const-string v6, "M"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 769
    const-string v6, "--"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 779
    :goto_0
    return-object v5

    .line 770
    :cond_0
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v6, :cond_1

    .line 771
    const-string v6, "dd-"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 772
    const-string v6, "-dd"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 773
    const-string v6, "-"

    const-string v7, "/"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 777
    :cond_1
    const-string v6, "-"

    const-string v7, "/"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method protected getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 3
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 319
    const-string v0, "SpO2GraphFragmentSIC"

    const-string v1, "getTimeDateFormat"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 321
    const-string v0, "dd"

    .line 325
    :goto_0
    return-object v0

    .line 322
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    .line 323
    const-string v0, "MM"

    goto :goto_0

    .line 324
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    .line 325
    const-string v0, "HH"

    goto :goto_0

    .line 327
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal periodType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 110
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->initDateBar()V

    .line 111
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    .line 272
    const-string v2, "SpO2GraphFragmentSIC"

    const-string v3, "initGraphLegendArea"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 274
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030223

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f0809a1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegend:Landroid/widget/LinearLayout;

    .line 276
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f0809a4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->switchToSpO2Summary:Landroid/widget/ImageButton;

    .line 277
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f0809a2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendSpo2Label:Landroid/widget/TextView;

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f0809a3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendPulseLabel:Landroid/widget/TextView;

    .line 280
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->switchToSpO2Summary:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$2;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->switchToSpO2Summary:Landroid/widget/ImageButton;

    invoke-static {v4, v5, v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->disableClickFor(JLandroid/view/View;)V

    .line 293
    :try_start_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$3;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v2

    if-lez v2, :cond_0

    .line 305
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegend:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 309
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v2

    .line 300
    :catch_0
    move-exception v0

    .line 301
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 307
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegend:Landroid/widget/LinearLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 4
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 246
    const-string v1, "SpO2GraphFragmentSIC"

    const-string v2, "initGraphView"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    new-instance v1, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .line 248
    iput-object p3, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 249
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 250
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    .line 251
    .local v0, "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 252
    invoke-direct {p0, v0, p3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setChartData(Lcom/samsung/android/sdk/chart/series/SchartDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 256
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->initTimeLineChartView(Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    .line 257
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setStartVisual(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 258
    const/high16 v1, 0x42f00000    # 120.0f

    sput v1, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->MAX_Y:F

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v1
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 264
    const-string v0, "SpO2GraphFragmentSIC"

    const-string v1, "initInformationArea"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 220
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->onConfiguarationLanguageChanged()V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendPulseLabel:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendSpo2Label:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendPulseLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f98

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mLegendSpo2Label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090240

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->isConfigurationChanged:Z

    .line 227
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setPeriodType()V

    .line 101
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setHasOptionsMenu(Z)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getDataCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mSpO2DataCount:I

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->density:F

    .line 105
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 232
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onDestroyView()V

    .line 233
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 118
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onResume()V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 121
    .local v2, "mPref":Landroid/content/SharedPreferences;
    const-string v4, "FromSpO2LogScreen"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 122
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 123
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "FromSpO2LogScreen"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 124
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 128
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .line 129
    .local v3, "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isDrawerVisible()Z

    move-result v4

    if-nez v4, :cond_1

    .line 131
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->customizeActionBarDisable(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    .line 132
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarListener()Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->customizeActionBarButtons(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->isConfigurationChanged:Z

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->isDataCountChanged()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 139
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->initGeneralView()V

    .line 140
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->isConfigurationChanged:Z

    .line 143
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->switchToSpO2Summary:Landroid/widget/ImageButton;

    if-eqz v4, :cond_4

    .line 145
    :try_start_1
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;)V

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 156
    :cond_4
    :goto_0
    return-void

    .line 134
    .end local v3    # "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v4, Ljava/lang/ClassCastException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-class v6, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " must be instance of SpO2Activity"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 152
    .end local v0    # "e":Ljava/lang/ClassCastException;
    .restart local v3    # "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    :catch_1
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 5

    .prologue
    .line 161
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->onStop()V

    .line 163
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .line 164
    .local v1, "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->customizeActionBarShow(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    return-void

    .line 165
    .end local v1    # "spO2Activity":Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of HeartrateActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 182
    .local v1, "mPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 183
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "period_type_spo2"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 184
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 185
    return-void
.end method

.method protected setPeriodType()V
    .locals 4

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 189
    .local v0, "mPref":Landroid/content/SharedPreferences;
    const-string/jumbo v2, "period_type_spo2"

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 190
    .local v1, "period":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 191
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 193
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method protected setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 315
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 316
    return-void
.end method
