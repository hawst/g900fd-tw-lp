.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;
.super Ljava/lang/Object;
.source "CignaLifestyleScoreView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setSummaryHeaderMode(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->access$000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->clearScoreMessageAnimation()V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCignaScoreLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->access$100(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mScoreMessageLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->access$000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->mCircleLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    :cond_0
    return-void
.end method
