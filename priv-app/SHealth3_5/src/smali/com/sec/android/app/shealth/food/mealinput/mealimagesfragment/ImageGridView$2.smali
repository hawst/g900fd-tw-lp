.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;
.super Ljava/lang/Object;
.source "ImageGridView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->initView(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

.field final synthetic val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

.field final synthetic val$imageResource:Landroid/widget/ImageButton;

.field final synthetic val$parentView:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;Landroid/view/ViewGroup;Landroid/widget/ImageButton;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    iput-object p3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$parentView:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$imageResource:Landroid/widget/ImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 281
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->isHandlerAllowed:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 283
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 284
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$parentView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$parentView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedAndScaledByCenterBitmap(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$imageResource:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->handler:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$100(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;)Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$ImageSetHandler;->sendMessage(Landroid/os/Message;)Z

    .line 293
    .end local v0    # "message":Landroid/os/Message;
    :goto_0
    return-void

    .line 291
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView;->access$200()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File by path "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$2;->val$imagePagerHolder:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    iget-object v3, v3, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t exist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
