.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;
.super Ljava/lang/Thread;
.source "WalkingMateDayStepService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 327
    const-string v1, "WalkingMateDayStepService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KeyManager WalkingMateDayStepService ======= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_3

    .line 330
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "CP is not accessible on onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->prepareWidgetInCaseOfPasswordLocked()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$500(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    .line 337
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->notifiedContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInitializationNeeded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 339
    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotifiedContentProviderAccessible(Z)V

    .line 340
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    const-string/jumbo v3, "security_pin_enabled"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sendNotification()V

    .line 344
    :cond_0
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "Stop service on onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    .line 365
    :cond_1
    :goto_1
    return-void

    .line 334
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->prepareWidgetInCaseOfNoProfile(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$600(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V

    goto :goto_0

    .line 351
    :cond_3
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "CP is accessible on onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->initializeService()I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$700(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)I

    move-result v0

    .line 354
    .local v0, "initializeService":I
    if-eqz v0, :cond_4

    .line 355
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 356
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    goto :goto_1

    .line 360
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;->startAlarm(Landroid/content/Context;)V

    .line 361
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "initializeService is completed on onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotifiedContentProviderAccessible(Z)V

    .line 364
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->cancelNotification()V

    goto :goto_1
.end method
