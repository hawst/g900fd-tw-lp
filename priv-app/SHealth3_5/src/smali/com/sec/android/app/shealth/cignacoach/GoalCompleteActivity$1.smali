.class Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;
.super Ljava/lang/Object;
.source "GoalCompleteActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->setCustomActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 120
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 121
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 123
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->prepareShareView()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.cignacoach"

    const-string v4, "0032"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mFinishDelayHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :cond_0
    :goto_0
    return-void

    .line 129
    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
