.class Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;
.super Ljava/lang/Object;
.source "CignaTermsOfUseActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isCheck"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mAgreeCheckRight:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->mAgreeCheckRight:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->isAgreeCheck:Z
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$202(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;Z)Z

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->isAgreeCheck:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-static {p1, v3, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->enableNextBtn(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;Z)V

    .line 128
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->setNextBtnDescription(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;Z)V

    .line 129
    return-void

    .line 125
    :cond_0
    invoke-static {p1, v3, v1, v1}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->enableNextBtn(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/CignaTermsOfUseActivity;Z)V

    goto :goto_0
.end method
