.class public Lcom/sec/android/app/shealth/uv/UvInformationAreaView;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "UvInformationAreaView.java"


# static fields
.field private static final DATE_SEPARATOR:Ljava/lang/String; = "/"

.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field private static final DAY_FORMAT_PATTERN:Ljava/lang/String; = "dd"

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MM"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field private static final TIME_FORMAT_12HOUR:Ljava/lang/String; = "h:mm a"

.field private static final TIME_FORMAT_24HOUR:Ljava/lang/String; = "HH:mm"

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"

.field private static final YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"


# instance fields
.field private dateFormat:Ljava/text/DateFormat;

.field private mContext:Landroid/content/Context;

.field private mDateFormatOrder:[C

.field private mTimeFormatPattern:Ljava/lang/String;

.field private mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mUvMaxText:Landroid/widget/TextView;

.field private mUvState:Landroid/widget/TextView;

.field private mUvStateImage:Landroid/widget/ImageView;

.field private tvDate:Landroid/widget/TextView;

.field private tvTime:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mContext:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 59
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 60
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mDateFormatOrder:[C

    .line 61
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string v0, "HH:mm"

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    .line 67
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->initLayout()V

    .line 68
    return-void

    .line 64
    :cond_0
    const-string v0, "h:mm a"

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    goto :goto_0
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 110
    const v0, 0x7f0809e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->tvDate:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f080b06

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->tvTime:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f080b08

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    .line 114
    const v0, 0x7f080b09

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f080b07

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvMaxText:Landroid/widget/TextView;

    .line 118
    return-void
.end method

.method private setUvData(I)V
    .locals 8
    .param p1, "value"    # I

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvMaxText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 157
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->isChinaModel()Z

    move-result v0

    if-nez v0, :cond_5

    .line 159
    if-ltz p1, :cond_1

    if-gt p1, v3, :cond_1

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d94

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f02074f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 194
    :goto_1
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvMaxText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090d9c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 162
    :cond_1
    if-lt p1, v4, :cond_2

    if-gt p1, v5, :cond_2

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d95

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f020750

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 165
    :cond_2
    if-lt p1, v6, :cond_3

    if-gt p1, v7, :cond_3

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d96

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f02074e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 168
    :cond_3
    const/16 v0, 0x8

    if-lt p1, v0, :cond_4

    const/16 v0, 0xa

    if-gt p1, v0, :cond_4

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d97

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f020751

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 172
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f090d98

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f02074d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 176
    :cond_5
    if-ltz p1, :cond_6

    if-gt p1, v3, :cond_6

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f091158

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f02074f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 179
    :cond_6
    if-lt p1, v4, :cond_7

    const/4 v0, 0x4

    if-gt p1, v0, :cond_7

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f091159

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f020750

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 182
    :cond_7
    if-lt p1, v5, :cond_8

    if-gt p1, v6, :cond_8

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f09115a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f02074e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 185
    :cond_8
    if-lt p1, v7, :cond_9

    const/16 v0, 0x9

    if-gt p1, v0, :cond_9

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f09115b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f020751

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 189
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvState:Landroid/widget/TextView;

    const v1, 0x7f09115c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mUvStateImage:Landroid/widget/ImageView;

    const v1, 0x7f02074d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03026d

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public setDateFormat()V
    .locals 4

    .prologue
    .line 121
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mDateFormatOrder:[C

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    .line 124
    .local v0, "dateFormatPattern":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_1

    .line 125
    const-string v1, ""

    .line 130
    .local v1, "dayFormatPattern":Ljava/lang/String;
    :goto_0
    const-string v2, "d+"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    const-string v2, "M+"

    const-string v3, "MM/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string/jumbo v2, "y+"

    const-string/jumbo v3, "yyyy/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    const-string v2, "/$"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_0

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 139
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 140
    return-void

    .line 127
    .end local v1    # "dayFormatPattern":Ljava/lang/String;
    :cond_1
    const-string v1, "dd/"

    .restart local v1    # "dayFormatPattern":Ljava/lang/String;
    goto :goto_0
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 11
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/4 v10, 0x0

    .line 86
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "dateValue":Ljava/lang/String;
    new-instance v0, Ljava/util/Date;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 89
    .local v0, "date":Ljava/util/Date;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v7, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v6, v7, :cond_0

    .line 90
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->tvDate:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->tvTime:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyy/MM/dd"

    invoke-direct {v3, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 93
    .local v3, "dformat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "ddate":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->tvDate:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 98
    .end local v2    # "ddate":Ljava/lang/String;
    .end local v3    # "dformat":Ljava/text/SimpleDateFormat;
    :goto_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 99
    .local v4, "timeValue":J
    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->setSelectedDateInChart(J)V

    .line 100
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    double-to-int v6, v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->setUvData(I)V

    .line 107
    return-void

    .line 96
    .end local v4    # "timeValue":J
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->tvDate:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/shealth/uv/UvInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v7, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
