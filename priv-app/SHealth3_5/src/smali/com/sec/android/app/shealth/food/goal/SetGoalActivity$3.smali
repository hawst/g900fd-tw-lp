.class Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;
.super Ljava/lang/Object;
.source "SetGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->initViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

.field final synthetic val$calUnitBurn:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;->val$calUnitBurn:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChanged(Ljava/lang/Float;)V
    .locals 4
    .param p1, "newValue"    # Ljava/lang/Float;

    .prologue
    .line 154
    if-eqz p1, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->mParentLayout2:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->access$200(Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;->this$0:Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;

    const v3, 0x7f090954

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Float;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/goal/SetGoalActivity$3;->val$calUnitBurn:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 158
    :cond_0
    return-void
.end method
