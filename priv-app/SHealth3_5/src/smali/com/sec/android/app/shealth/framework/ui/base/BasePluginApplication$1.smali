.class Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 31

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->access$000()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, -0x28c6

    aput v23, v2, v22

    const/16 v22, -0x13

    aput v22, v2, v21

    const/16 v21, -0x8

    aput v21, v2, v20

    const/16 v20, -0x37

    aput v20, v2, v19

    const/16 v19, -0x40

    aput v19, v2, v18

    const/16 v18, 0x2458

    aput v18, v2, v17

    const/16 v17, 0x2754

    aput v17, v2, v16

    const/16 v16, -0xbe

    aput v16, v2, v15

    const/16 v15, -0x64

    aput v15, v2, v14

    const/16 v14, -0x45ec

    aput v14, v2, v13

    const/16 v13, -0x21

    aput v13, v2, v12

    const/16 v12, -0x53ae

    aput v12, v2, v11

    const/16 v11, -0x28

    aput v11, v2, v10

    const/16 v10, -0x6ee7

    aput v10, v2, v9

    const/16 v9, -0xa

    aput v9, v2, v8

    const/16 v8, -0x21

    aput v8, v2, v7

    const/16 v7, -0x4ab5

    aput v7, v2, v6

    const/16 v6, -0x2a

    aput v6, v2, v5

    const/16 v5, -0x45

    aput v5, v2, v3

    const/16 v3, 0x2f56

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x28e6

    aput v24, v1, v23

    const/16 v23, -0x29

    aput v23, v1, v22

    const/16 v22, -0x6a

    aput v22, v1, v21

    const/16 v21, -0x5a

    aput v21, v1, v20

    const/16 v20, -0x57

    aput v20, v1, v19

    const/16 v19, 0x242c

    aput v19, v1, v18

    const/16 v18, 0x2724

    aput v18, v1, v17

    const/16 v17, -0xd9

    aput v17, v1, v16

    const/16 v16, -0x1

    aput v16, v1, v15

    const/16 v15, -0x4594

    aput v15, v1, v14

    const/16 v14, -0x46

    aput v14, v1, v13

    const/16 v13, -0x538e

    aput v13, v1, v12

    const/16 v12, -0x54

    aput v12, v1, v11

    const/16 v11, -0x6e8f

    aput v11, v1, v10

    const/16 v10, -0x6f

    aput v10, v1, v9

    const/16 v9, -0x56

    aput v9, v1, v8

    const/16 v8, -0x4ad6

    aput v8, v1, v7

    const/16 v7, -0x4b

    aput v7, v1, v6

    const/16 v6, -0x2b

    aput v6, v1, v5

    const/16 v5, 0x2f03

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-static {v4, v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1f1e

    aput v29, v2, v28

    const/16 v28, 0x497a

    aput v28, v2, v27

    const/16 v27, 0x63b

    aput v27, v2, v26

    const/16 v26, -0x128d

    aput v26, v2, v25

    const/16 v25, -0x72

    aput v25, v2, v24

    const/16 v24, -0xf8a

    aput v24, v2, v23

    const/16 v23, -0x61

    aput v23, v2, v22

    const/16 v22, 0x7017

    aput v22, v2, v21

    const/16 v21, -0x63e2

    aput v21, v2, v20

    const/16 v20, -0xd

    aput v20, v2, v19

    const/16 v19, -0x2ed

    aput v19, v2, v18

    const/16 v18, -0x77

    aput v18, v2, v17

    const/16 v17, -0x3cf6

    aput v17, v2, v16

    const/16 v16, -0x5a

    aput v16, v2, v15

    const/16 v15, -0x32

    aput v15, v2, v14

    const/4 v14, -0x3

    aput v14, v2, v13

    const/16 v13, 0x401c

    aput v13, v2, v12

    const/16 v12, -0x3a0

    aput v12, v2, v11

    const/16 v11, -0x78

    aput v11, v2, v10

    const/16 v10, -0x80

    aput v10, v2, v9

    const/16 v9, 0x341d

    aput v9, v2, v8

    const/16 v8, -0x39bf

    aput v8, v2, v7

    const/16 v7, -0x59

    aput v7, v2, v6

    const/16 v6, 0x351d

    aput v6, v2, v5

    const/16 v5, 0x225b

    aput v5, v2, v3

    const/16 v3, 0x7977

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1f7a

    aput v30, v1, v29

    const/16 v29, 0x491f

    aput v29, v1, v28

    const/16 v28, 0x649

    aput v28, v1, v27

    const/16 v27, -0x12fa

    aput v27, v1, v26

    const/16 v26, -0x13

    aput v26, v1, v25

    const/16 v25, -0xfeb

    aput v25, v1, v24

    const/16 v24, -0x10

    aput v24, v1, v23

    const/16 v23, 0x7037

    aput v23, v1, v22

    const/16 v22, -0x6390

    aput v22, v1, v21

    const/16 v21, -0x64

    aput v21, v1, v20

    const/16 v20, -0x286

    aput v20, v1, v19

    const/16 v19, -0x3

    aput v19, v1, v18

    const/16 v18, -0x3c86

    aput v18, v1, v17

    const/16 v17, -0x3d

    aput v17, v1, v16

    const/16 v16, -0x53

    aput v16, v1, v15

    const/16 v15, -0x7b

    aput v15, v1, v14

    const/16 v14, 0x4079

    aput v14, v1, v13

    const/16 v13, -0x3c0

    aput v13, v1, v12

    const/4 v12, -0x4

    aput v12, v1, v11

    const/16 v11, -0x18

    aput v11, v1, v10

    const/16 v10, 0x347a

    aput v10, v1, v9

    const/16 v9, -0x39cc

    aput v9, v1, v8

    const/16 v8, -0x3a

    aput v8, v1, v7

    const/16 v7, 0x357e

    aput v7, v1, v6

    const/16 v6, 0x2235

    aput v6, v1, v5

    const/16 v5, 0x7922

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method
