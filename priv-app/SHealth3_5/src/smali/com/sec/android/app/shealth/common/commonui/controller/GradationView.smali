.class public abstract Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.super Landroid/view/View;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NoAdditionalDrawingStrategy;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;,
        Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;
    }
.end annotation


# static fields
.field private static final INERTIA_LIST_SIZE:I = 0x3

.field private static final NUMBER_FORMAT_STRING:Ljava/lang/String; = "###"

.field private static final NUMBER_OF_STREAMS:I = 0x2


# instance fields
.field private mAbnormalPaint:Landroid/graphics/Paint;

.field private mAdditionalDrawingStrategy:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;

.field private mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDistanceBetweenGradations:I

.field private mDistanceToMaxInputValue:I

.field private mDistanceToMinInputValue:I

.field private mGradationPaint:Landroid/graphics/Paint;

.field private mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

.field private mInterval:F

.field private mNormalPaint:Landroid/graphics/Paint;

.field private mNormalRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

.field private mNumberDecimalFormat:Ljava/text/DecimalFormat;

.field private mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

.field private mOnValueChangedListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTickSoundId:I

.field private mTicker:Landroid/media/SoundPool;

.field private mValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x461c3c00    # 9999.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 211
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 58
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    .line 59
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 60
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->initView()V

    .line 213
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const v2, 0x461c3c00    # 9999.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 221
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    .line 59
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 60
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 222
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->initView()V

    .line 223
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const v2, 0x461c3c00    # 9999.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 232
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    .line 59
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 60
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->initView()V

    .line 234
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Landroid/graphics/Canvas;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p1, "x1"    # Landroid/graphics/Canvas;
    .param p2, "x2"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawGradationLines(Landroid/graphics/Canvas;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Landroid/graphics/Canvas;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p1, "x1"    # Landroid/graphics/Canvas;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawBottomLine(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->shiftGradationView(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->moveToClosestGradation()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p1, "x1"    # F

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->pixelToValue(F)F

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p1, "x1"    # F

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getClosestGradationValue(F)F

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceToMaxInputValue:I

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceToMinInputValue:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    .param p1, "x1"    # F

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValueWithSound(F)V

    return-void
.end method

.method private createFormatter()V
    .locals 2

    .prologue
    .line 331
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    const-string v1, "###"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 333
    return-void
.end method

.method private drawBottomLine(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    .line 171
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    iget v7, v7, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getIndexOfGradationBelow(F)I

    move-result v4

    .line 172
    .local v4, "startNormalRangeIndex":I
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    iget v7, v7, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getIndexOfGradationAbove(F)I

    move-result v1

    .line 173
    .local v1, "endNormalRangeIndex":I
    const/4 v3, 0x0

    .line 174
    .local v3, "startNormalRange":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getLength()I

    move-result v0

    .line 175
    .local v0, "endNormalRange":I
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getVisibleRange()Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    move-result-object v6

    .line 176
    .local v6, "visibleRange":Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    int-to-float v7, v4

    iget v8, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float/2addr v7, v8

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->getValueLocation(F)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->access$300(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    move-result-object v5

    .line 177
    .local v5, "startRangeLocation":Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    sget-object v7, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->AFTER:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    if-ne v5, v7, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getLength()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v9, v7, v8}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V

    .line 193
    :goto_0
    return-void

    .line 180
    :cond_0
    sget-object v7, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->IN:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    if-ne v5, v7, :cond_1

    .line 181
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getGradationPosition(I)I

    move-result v3

    .line 182
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v9, v3, v7}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V

    .line 184
    :cond_1
    int-to-float v7, v1

    iget v8, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float/2addr v7, v8

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->getValueLocation(F)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->access$300(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    move-result-object v2

    .line 185
    .local v2, "endRangeLocation":Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    sget-object v7, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->BEFORE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    if-ne v2, v7, :cond_2

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getLength()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v9, v7, v8}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V

    goto :goto_0

    .line 188
    :cond_2
    sget-object v7, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->IN:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    if-ne v2, v7, :cond_3

    .line 189
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getGradationPosition(I)I

    move-result v0

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getLength()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v0, v7, v8}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V

    .line 192
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v3, v0, v7}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private drawGradationLines(Landroid/graphics/Canvas;I)V
    .locals 20
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bottomOffset"    # I

    .prologue
    .line 150
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getVisibleRange()Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    move-result-object v19

    .line 151
    .local v19, "visibleRange":Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    move-object/from16 v0, v19

    iget v1, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    add-int/lit8 v16, v1, -0x1

    .line 152
    .local v16, "startGradationIndex":I
    move-object/from16 v0, v19

    iget v1, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    div-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v13, v1, 0x1

    .line 153
    .local v13, "endGradationIndex":I
    move/from16 v0, v16

    int-to-float v1, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float/2addr v1, v2

    move-object/from16 v0, v19

    iget v2, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->valueToPixels(F)I

    move-result v14

    .line 154
    .local v14, "firstGradationPosition":I
    move/from16 v15, v16

    .local v15, "i":I
    move v4, v14

    .local v4, "gradationPosition":I
    :goto_0
    if-gt v15, v13, :cond_2

    .line 155
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getGradationType(I)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    move-result-object v17

    .line 156
    .local v17, "type":Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object/from16 v0, v17

    iget v2, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->lengthResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 157
    .local v6, "gradationLength":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mGradationPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawGradationLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;III)V

    .line 158
    int-to-float v1, v15

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float v18, v1, v2

    .line 160
    .local v18, "value":F
    move-object/from16 v0, v17

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->drawText:Z

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    move/from16 v0, v18

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->isValueInRange(F)Z
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->access$200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)Z

    move-result v1

    if-nez v1, :cond_0

    move/from16 v0, v18

    float-to-double v1, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    iget v3, v3, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    float-to-double v7, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->floor(D)D

    move-result-wide v7

    cmpl-double v1, v1, v7

    if-nez v1, :cond_1

    .line 161
    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    move/from16 v0, v18

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    add-int v12, v6, p2

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move v11, v4

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->drawGradationText(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/lang/String;II)V

    .line 154
    :cond_1
    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceBetweenGradations:I

    add-int/2addr v4, v1

    goto :goto_0

    .line 164
    .end local v6    # "gradationLength":I
    .end local v17    # "type":Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;
    .end local v18    # "value":F
    :cond_2
    return-void
.end method

.method private getClosestGradationValue(F)F
    .locals 6
    .param p1, "value"    # F

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getIndexOfGradationBelow(F)I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float v3, v4, v5

    .line 365
    .local v3, "gradationBelow":F
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getIndexOfGradationAbove(F)I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float v2, v4, v5

    .line 366
    .local v2, "gradationAbove":F
    sub-float v1, p1, v3

    .line 367
    .local v1, "distanceToGradationBelow":F
    sub-float v0, v2, p1

    .line 368
    .local v0, "distanceToGradationAbove":F
    cmpl-float v4, v0, v1

    if-lez v4, :cond_0

    .end local v3    # "gradationBelow":F
    :goto_0
    return v3

    .restart local v3    # "gradationBelow":F
    :cond_0
    move v3, v2

    goto :goto_0
.end method

.method private getGradationPosition(I)I
    .locals 4
    .param p1, "gradationIndex"    # I

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getLength()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceBetweenGradations:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float v0, v2, v3

    .line 435
    .local v0, "length":F
    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v0, v3

    sub-float v1, v2, v3

    .line 436
    .local v1, "startValue":F
    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float/2addr v2, v3

    sub-float/2addr v2, v1

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->valueToPixels(F)I

    move-result v2

    return v2
.end method

.method private getGradationType(I)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 196
    const/16 v0, 0xa

    .line 197
    .local v0, "gradationsInDecadeQuantity":I
    rem-int/lit8 v1, p1, 0xa

    if-nez v1, :cond_0

    .line 198
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_BIG:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    .line 203
    :goto_0
    return-object v1

    .line 200
    :cond_0
    rem-int/lit8 v1, p1, 0x5

    if-nez v1, :cond_1

    .line 201
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_MIDDLE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    goto :goto_0

    .line 203
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;->TYPE_SMALL:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationType;

    goto :goto_0
.end method

.method private getIndexOfGradationAbove(F)I
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 351
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    div-float v0, p1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private getIndexOfGradationBelow(F)I
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 355
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    div-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private getVisibleRange()Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getLength()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->pixelToValue(F)F

    move-result v0

    .line 360
    .local v0, "length":F
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    iget v2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    div-float v3, v0, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    div-float v4, v0, v4

    add-float/2addr v3, v4

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    return-object v1
.end method

.method private initView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, -0x1000000

    const/4 v3, 0x1

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getDistanceBetweenGradations()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceBetweenGradations:I

    .line 290
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->createFormatter()V

    .line 291
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$GradationViewOnTouchListener;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 292
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTextPaint:Landroid/graphics/Paint;

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_gradationbar_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTextPaint:Landroid/graphics/Paint;

    const-string/jumbo v1, "sec-roboto-light"

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 298
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mGradationPaint:Landroid/graphics/Paint;

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mGradationPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mGradationPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_gradationbar_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 302
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$color;->input_field_ubnormal_bottom_line:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 305
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAbnormalPaint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalPaint:Landroid/graphics/Paint;

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$color;->input_field_normal_bottom_line:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 307
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    .line 309
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v3, v5}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTicker:Landroid/media/SoundPool;

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTicker:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$raw;->s_health_graduation_control:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTickSoundId:I

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAudioManager:Landroid/media/AudioManager;

    .line 313
    return-void
.end method

.method private moveToClosestGradation()V
    .locals 1

    .prologue
    .line 372
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getClosestGradationValue(F)F

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValueWithSound(F)V

    .line 373
    return-void
.end method

.method private pixelToValue(F)F
    .locals 2
    .param p1, "distanceInPixels"    # F

    .prologue
    .line 426
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceBetweenGradations:I

    int-to-float v0, v0

    div-float v0, p1, v0

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    mul-float/2addr v0, v1

    return v0
.end method

.method private setValueWithSound(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->ensureValueInRange(F)F
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->access$700(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)F

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getIndexOfGradationBelow(F)I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getIndexOfGradationBelow(F)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 377
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTickSoundId:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->playSoundIndex(I)V

    .line 380
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValue(F)V

    .line 381
    return-void
.end method

.method private shiftGradationView(I)V
    .locals 2
    .param p1, "pixelOffset"    # I

    .prologue
    .line 490
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    int-to-float v1, p1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->pixelToValue(F)F

    move-result v1

    add-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValueWithSound(F)V

    .line 491
    return-void
.end method

.method private valueToPixels(F)I
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 430
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    div-float v0, p1, v0

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceBetweenGradations:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method


# virtual methods
.method public animateButtonLongpress(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V
    .locals 1
    .param p1, "direction"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    .prologue
    .line 509
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->animateButtonLongPress(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->access$1000(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;)V

    .line 510
    return-void
.end method

.method public decrementValue()V
    .locals 2

    .prologue
    .line 422
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    sub-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValueWithSound(F)V

    .line 423
    return-void
.end method

.method protected abstract drawGradationLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;III)V
.end method

.method protected abstract drawGradationText(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/lang/String;II)V
.end method

.method protected abstract drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V
.end method

.method protected abstract getBottomLineHeight()I
.end method

.method protected abstract getDistanceBetweenGradations()I
.end method

.method public getInterval()F
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    return v0
.end method

.method protected abstract getLength()I
.end method

.method protected abstract getOrientationTouchCoord(Landroid/view/MotionEvent;)F
.end method

.method protected getTextHalfHeight(Landroid/graphics/Paint;)F
    .locals 2
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 322
    invoke-virtual {p1}, Landroid/graphics/Paint;->descent()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public incrementValue()V
    .locals 2

    .prologue
    .line 415
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    add-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->setValueWithSound(F)V

    .line 416
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAdditionalDrawingStrategy:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;->onDraw(Landroid/graphics/Canvas;)V

    .line 328
    return-void
.end method

.method public playSoundIndex(I)V
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-nez v0, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 388
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTicker:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mTicker:Landroid/media/SoundPool;

    move v1, p1

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_0
.end method

.method public setDrawingStrategy(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;)V
    .locals 3
    .param p1, "strategy"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;

    .prologue
    const/4 v2, 0x0

    .line 340
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;->$SwitchMap$com$sec$android$app$shealth$common$commonui$controller$GradationView$AdditionalDrawingStrategy$AdditionalDrawingType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy$AdditionalDrawingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 345
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NoAdditionalDrawingStrategy;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NoAdditionalDrawingStrategy;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAdditionalDrawingStrategy:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;

    .line 348
    :goto_0
    return-void

    .line 342
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$NormalRangeDrawingStrategy;-><init>(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAdditionalDrawingStrategy:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AdditionalDrawingStrategy;

    goto :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setInputRange(FF)V
    .locals 1
    .param p1, "minInputRange"    # F
    .param p2, "maxInputRange"    # F

    .prologue
    .line 461
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 462
    return-void
.end method

.method public setInterval(F)V
    .locals 0
    .param p1, "interval"    # F

    .prologue
    .line 444
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInterval:F

    .line 445
    return-void
.end method

.method public setNormalRange(FF)V
    .locals 1
    .param p1, "minNormalRange"    # F
    .param p2, "maxNormalRange"    # F

    .prologue
    .line 470
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mNormalRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    .line 471
    return-void
.end method

.method public setOnControllerTapListener(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    .prologue
    .line 486
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnControllerTapListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnControllerTapListener;

    .line 487
    return-void
.end method

.method public setOnValueChangedListener(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;

    .line 479
    return-void
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 400
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->ensureValueInRange(F)F
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->access$700(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)F

    move-result p1

    .line 401
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    iget v1, v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    sub-float v1, p1, v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->valueToPixels(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceToMinInputValue:I

    .line 402
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mInputRange:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;

    iget v1, v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    sub-float v1, p1, v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->valueToPixels(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mDistanceToMaxInputValue:I

    .line 403
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getClosestGradationValue(F)F

    move-result v0

    .line 404
    .local v0, "valueToUpdate":F
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->getClosestGradationValue(F)F

    move-result v1

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v1

    if-nez v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$OnValueChangedListener;->onValueChanged(F)V

    .line 407
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mValue:F

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->invalidate()V

    .line 409
    return-void
.end method

.method public stopInertia()V
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->isRunning()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->access$800(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->mAnimationOperator:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->cancelAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;->access$900(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$AnimationOperator;)V

    .line 500
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->moveToClosestGradation()V

    .line 502
    :cond_0
    return-void
.end method
