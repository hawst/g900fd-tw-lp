.class public Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "AssessmentPageAdapter.java"


# instance fields
.field private mAssessmentDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;",
            ">;"
        }
    .end annotation
.end field

.field private mAssessmentViewWeight:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

.field private mContext:Landroid/content/Context;

.field private mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

.field private mCurrentAssessmentDataIndex:I

.field private mListener:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

.field private mWeightCategoryIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "weightCategoryIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p2, "assessmentDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;>;"
    const/high16 v0, -0x80000000

    .line 37
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 22
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentDataIndex:I

    .line 23
    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mWeightCategoryIndex:I

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mContext:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    .line 40
    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mWeightCategoryIndex:I

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->init()V

    .line 43
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentDataIndex:I

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentDataIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    .line 50
    :cond_0
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 124
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 125
    instance-of v0, p3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    if-eqz v0, :cond_0

    .line 126
    check-cast p3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .end local p3    # "view":Ljava/lang/Object;
    invoke-virtual {p3}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->onDestroyView()V

    .line 128
    :cond_0
    return-void
.end method

.method public getAssessmentAndQAIndex(I)[I
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 99
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 101
    .local v0, "assessmentAndQAIndex":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 103
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    .line 104
    .local v1, "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQADataCount()I

    move-result v3

    .line 106
    .local v3, "qaCount":I
    sub-int v4, p1, v3

    if-ltz v4, :cond_0

    .line 108
    sub-int/2addr p1, v3

    .line 116
    const/4 v4, 0x0

    aget v5, v0, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v0, v4

    .line 101
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 112
    :cond_0
    const/4 v4, 0x1

    aput p1, v0, v4

    .line 119
    .end local v1    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v3    # "qaCount":I
    :cond_1
    return-object v0
.end method

.method public getAssessmentData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    return-object v0
.end method

.method public getCount()I
    .locals 5

    .prologue
    .line 55
    const/4 v2, 0x0

    .line 56
    .local v2, "pageCount":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 57
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    .line 58
    .local v0, "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQADataCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 59
    goto :goto_0

    .line 62
    .end local v0    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mWeightCategoryIndex:I

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_1

    .line 63
    add-int/lit8 v2, v2, -0x1

    .line 66
    :cond_1
    return v2
.end method

.method public getQACountOfAssessmentData(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 141
    :cond_0
    const/4 v0, 0x0

    .line 144
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQADataCount()I

    move-result v0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x1

    .line 71
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;-><init>(Landroid/content/Context;)V

    .line 73
    .local v1, "assessmentView":Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getAssessmentAndQAIndex(I)[I

    move-result-object v0

    .line 75
    .local v0, "assessmentAndQAIndex":[I
    const/4 v2, 0x0

    aget v2, v0, v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentDataIndex:I

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentDatas:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentDataIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    .line 79
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    aget v3, v0, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQAData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    move-result-object v2

    aget v3, v0, v5

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    if-ne v2, v3, :cond_0

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mCurrentAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    aget v3, v0, v5

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQAData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setWeightAssessmentQAData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;)V

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentViewWeight:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .line 86
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mListener:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->setOnItemClickListener(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;)V

    .line 88
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v1}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 90
    return-object v1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 132
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnItemClickListener(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mListener:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;

    .line 137
    return-void
.end method

.method public updateTrackingData()V
    .locals 6

    .prologue
    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentViewWeight:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    if-eqz v1, :cond_0

    .line 154
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 155
    .local v0, "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->mAssessmentViewWeight:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;->updateTrackingData(IIFF)V

    .line 157
    .end local v0    # "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_0
    return-void
.end method
