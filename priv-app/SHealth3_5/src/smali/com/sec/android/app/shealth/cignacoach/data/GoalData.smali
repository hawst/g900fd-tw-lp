.class public Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
.super Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
.source "GoalData.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/GoalData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mGoalStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field private mMinFrequency:I

.field private mMissionCompleteCount:I

.field private mMissionToComplete:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "iconDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "titleString"    # Ljava/lang/String;
    .param p4, "extraString"    # Ljava/lang/String;
    .param p5, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;-><init>(Landroid/os/Parcel;)V

    .line 40
    return-void
.end method

.method public static valueOf(Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;)Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    .locals 3
    .param p0, "gimi"    # Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .prologue
    .line 132
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;-><init>()V

    .line 133
    .local v0, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setId(I)V

    .line 134
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaGoalIconIdByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setTitleString(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalFrequency()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setExtraString(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCompletedDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setCompleteDate(Ljava/util/Calendar;)V

    .line 139
    instance-of v1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;

    if-eqz v1, :cond_0

    .line 140
    check-cast p0, Lcom/cigna/coach/dataobjects/UserGoalData;

    .end local p0    # "gimi":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setGoalStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 143
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getGoalStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mGoalStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    return-object v0
.end method

.method public getMinFrequency()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mMinFrequency:I

    return v0
.end method

.method public getMissionCompleteCount()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mMissionCompleteCount:I

    return v0
.end method

.method public getMissionToComplete()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mMissionToComplete:I

    return v0
.end method

.method public setGoalStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V
    .locals 0
    .param p1, "goalStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mGoalStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 80
    return-void
.end method

.method public setMinFrequency(I)V
    .locals 0
    .param p1, "minFrequency"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mMinFrequency:I

    .line 96
    return-void
.end method

.method public setMissionCompleteCount(I)V
    .locals 0
    .param p1, "missionCompleteCount"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mMissionCompleteCount:I

    .line 88
    return-void
.end method

.method public setMissionToComplete(I)V
    .locals 0
    .param p1, "missionToComplete"    # I

    .prologue
    .line 103
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->mMissionToComplete:I

    .line 104
    return-void
.end method
