.class public Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;
.super Landroid/widget/LinearLayout;
.source "CompleteHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$1;,
        Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;
    }
.end annotation


# instance fields
.field private mAdditionalInfo1:Landroid/widget/TextView;

.field private mAdditionalInfo2:Landroid/widget/TextView;

.field private mExclamations:Landroid/widget/TextView;

.field private mSubTitle:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mType:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->initialize()V

    .line 35
    return-void
.end method

.method private initialize()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030032

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 39
    const v0, 0x7f08011e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mTitle:Landroid/widget/TextView;

    .line 40
    const v0, 0x7f08011f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mSubTitle:Landroid/widget/TextView;

    .line 42
    const v0, 0x7f080120

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo1:Landroid/widget/TextView;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 45
    const v0, 0x7f080121

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    const v0, 0x7f08011d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    .line 49
    return-void
.end method


# virtual methods
.method public setCompleteType(Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;)V
    .locals 4
    .param p1, "type"    # Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    .prologue
    const v3, 0x7f070144

    const v2, 0x7f0700e3

    .line 94
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mType:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    .line 96
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$widget$CompleteHeaderView$TYPE:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mType:Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView$TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 126
    :goto_0
    return-void

    .line 98
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mSubTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 106
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mSubTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 116
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mSubTitle:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mSubTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setExclamations(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "exclamations"    # Ljava/lang/CharSequence;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method

.method public setProgressMessage(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "progressMsg"    # Ljava/lang/CharSequence;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->setProgressMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 66
    return-void
.end method

.method public setProgressMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "progressMsg"    # Ljava/lang/CharSequence;
    .param p2, "progressMsg2"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    .line 69
    if-eqz p1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo1:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo1:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 73
    if-eqz p2, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :goto_0
    const/4 v0, 0x0

    .line 82
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 83
    .restart local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a027c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mExclamations:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void

    .line 78
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mAdditionalInfo2:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSubTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "subTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mSubTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CompleteHeaderView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    return-void
.end method
