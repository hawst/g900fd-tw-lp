.class public Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "InitSetTermsOfUse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;
    }
.end annotation


# static fields
.field protected static final ADD_SHEALTH_ACCOUNT:Ljava/lang/String; = "com.sec.android.service.health.cp.serversync.syncadapter.authenticator.SHEALTH_ACCOUNT"

.field public static final LAUNCHED_FROM_SETTINGS:Ljava/lang/String; = "launched_from_settings"

.field protected static final REQUEST_CODE:I = 0x3e7

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private TC_STEP:I

.field private disabledDescription:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private isAgreeCheck:Z

.field private isAgreeCheck_PP:Z

.field private isAustralia:Z

.field private isInInitialSettings:Z

.field private isLaunchedFromSetting:Z

.field private isUpgradeFrom35x:Z

.field private mAgreeCheck_PP:Landroid/widget/CheckBox;

.field private mAgreeCheck_Terms:Landroid/widget/CheckBox;

.field mAgreeCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field mAgreeLayoutClickListener:Landroid/view/View$OnClickListener;

.field private mCheckLayout_PP:Landroid/widget/RelativeLayout;

.field private mCheckLayout_Terms:Landroid/widget/RelativeLayout;

.field private mNextImage:Landroid/widget/ImageView;

.field private mNextText:Landroid/widget/TextView;

.field private mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

.field private mSHealthUpdation:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mToast1:Landroid/widget/Toast;

.field private nextBtnDescription:Ljava/lang/String;

.field private nextButtonLayout:Landroid/widget/RelativeLayout;

.field private ppKorSub1Text2:Landroid/widget/TextView;

.field private ppKorSub2Text3:Landroid/widget/TextView;

.field private runnable:Ljava/lang/Runnable;

.field private termsInInitialWithPP:Landroid/view/View;

.field private termsInSettings:Landroid/view/View;

.field private titleEraseData:Landroid/widget/TextView;

.field private titlePrivacyPolicy:Landroid/widget/TextView;

.field private titleTermsOfUse:Landroid/widget/TextView;

.field private w:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mCheckLayout_PP:Landroid/widget/RelativeLayout;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_PP:Landroid/widget/CheckBox;

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextText:Landroid/widget/TextView;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextImage:Landroid/widget/ImageView;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titleTermsOfUse:Landroid/widget/TextView;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titlePrivacyPolicy:Landroid/widget/TextView;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titleEraseData:Landroid/widget/TextView;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->ppKorSub2Text3:Landroid/widget/TextView;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    .line 66
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->handler:Landroid/os/Handler;

    .line 70
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isLaunchedFromSetting:Z

    .line 82
    iput v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I

    .line 85
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAustralia:Z

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    .line 89
    new-instance v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;-><init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->runnable:Ljava/lang/Runnable;

    .line 109
    new-instance v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$2;-><init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 127
    new-instance v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;-><init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 1251
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I

    return v0
.end method

.method static synthetic access$008(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TC_STEP:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAustralia:Z

    return v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 51
    sput-object p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->sBirthday:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->sBirthday:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startUpdateActivity(Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Lcom/sec/android/app/shealth/settings/SHealthUpdation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSHealthUpdation:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->w:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->enableNextBtn(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->setNextBtnDescription(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_Terms:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_PP:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private applyReviewedText()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const v10, 0x7f080a26

    const v9, 0x7f080a18

    const v8, 0x7f080a17

    const/4 v7, 0x0

    .line 642
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v3, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 643
    .local v3, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 644
    .local v2, "strLanguage":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 646
    .local v1, "countryName":Ljava/lang/String;
    const v4, 0x7f080690

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->makeParagraph2Text()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 649
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 651
    .local v0, "builder":Ljava/lang/StringBuilder;
    if-eqz v2, :cond_0

    const-string v4, "kk"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 652
    const v4, 0x7f090e6e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    const-string v4, "\n\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    :cond_0
    const v4, 0x7f090e6f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 657
    const v4, 0x7f090e70

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    const-string v4, "\n\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 660
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ZA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    if-eqz v2, :cond_1b

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 662
    const v4, 0x7f090ee2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    :goto_0
    const v4, 0x7f080694

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 691
    const v4, 0x7f080696

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090e74

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e75

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e76

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e77

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e78

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e79

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e7a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e7b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 705
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ES"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 707
    if-eqz v2, :cond_1

    const-string v4, "gl"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "ca"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "eu"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    const-string v4, "es"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "ES"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 710
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 711
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 712
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 713
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 714
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 715
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 720
    :cond_3
    if-eqz v2, :cond_4

    const-string v4, "cs"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 721
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 722
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 726
    :cond_4
    if-eqz v2, :cond_5

    const-string/jumbo v4, "pt"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "BR"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 727
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 728
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 729
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 730
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 734
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MX"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 736
    if-eqz v2, :cond_6

    const-string v4, "es"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "US"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 739
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 740
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 741
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eab

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 743
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 744
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 745
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eab

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 747
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eaa

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 750
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 751
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 752
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 753
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 754
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 755
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 760
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GB"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 762
    if-eqz v2, :cond_7

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "GB"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 764
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 765
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 766
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 767
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 768
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 769
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 774
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 776
    if-eqz v2, :cond_8

    const-string v4, "kk"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 779
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eaf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 780
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 781
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 782
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 783
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a20

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 784
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a21

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 785
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 786
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a24

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 789
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eaf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 790
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 791
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 792
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 793
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a20

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 794
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a21

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 795
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 796
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a24

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 799
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 800
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 801
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 806
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "KZ"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 808
    if-eqz v2, :cond_9

    const-string v4, "kk"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 811
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 812
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 817
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "FR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 819
    if-eqz v2, :cond_a

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 821
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eda

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 822
    const v4, 0x7f080691

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090edb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 824
    const-string v4, "CA"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 826
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 827
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 828
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090edd

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 834
    :cond_a
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 836
    if-eqz v2, :cond_21

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    const-string v4, "CA"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 839
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 840
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 841
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 842
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 844
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 845
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 913
    :cond_b
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 915
    if-eqz v2, :cond_c

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 917
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 918
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 919
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eba

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 920
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 921
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eb9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 922
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eba

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 923
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ebb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 924
    const v4, 0x7f080691

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ebc

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 929
    :cond_c
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 931
    if-eqz v2, :cond_d

    const-string v4, "de"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 935
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ebf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 936
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 937
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 938
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 939
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 940
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 941
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a20

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 942
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a21

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 943
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a22

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 944
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 945
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a24

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 946
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a27

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 947
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a28

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 950
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ebf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 951
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 952
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 953
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 954
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 955
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 956
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a20

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 957
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a21

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 958
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a22

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 959
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 960
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a24

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ec9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 961
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a27

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 962
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a28

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 965
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090e4e

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 966
    const v4, 0x7f080691

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090e50

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 967
    const v4, 0x7f080692

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090e51

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 968
    const v4, 0x7f080698

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090e52

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 969
    const v4, 0x7f08069b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090e53

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 970
    const v4, 0x7f080697

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 975
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CH"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 977
    if-eqz v2, :cond_e

    const-string v4, "de"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 979
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecc

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 980
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecc

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 981
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a24

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 982
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a24

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 983
    const v4, 0x7f080698

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090e52

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 984
    const v4, 0x7f080697

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 989
    :cond_e
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "NZ"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 991
    if-eqz v2, :cond_f

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 994
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 995
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 996
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 997
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 998
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 999
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ea7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1004
    :cond_f
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1006
    if-eqz v2, :cond_10

    const-string v4, "de"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1008
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecd

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1009
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecd

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1010
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ece

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1015
    :cond_10
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1017
    if-eqz v2, :cond_11

    const-string v4, "de"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1019
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecc

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1020
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecc

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1025
    :cond_11
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1027
    if-eqz v2, :cond_12

    const-string/jumbo v4, "sq"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1029
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1030
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ecf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1031
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1036
    :cond_12
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1038
    if-eqz v2, :cond_13

    const-string/jumbo v4, "sr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1040
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1041
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1042
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1043
    const v4, 0x7f080691

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1048
    :cond_13
    if-eqz v2, :cond_14

    const-string/jumbo v4, "ur"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1050
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1051
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1055
    :cond_14
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "KE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1057
    if-eqz v2, :cond_15

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1059
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ede

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1060
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ede

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1065
    :cond_15
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 1067
    if-eqz v2, :cond_16

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 1069
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1070
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1071
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1076
    :cond_16
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1078
    if-eqz v2, :cond_17

    const-string v4, "hr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1080
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1081
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1082
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1083
    const v4, 0x7f080691

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ed4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1088
    :cond_17
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AO"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 1090
    if-eqz v2, :cond_18

    const-string/jumbo v4, "pt"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 1092
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1093
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1094
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1095
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee9

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1100
    :cond_18
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 1102
    if-eqz v2, :cond_19

    const-string/jumbo v4, "pt"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 1104
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1105
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1106
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1107
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1108
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1113
    :cond_19
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ZA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1115
    if-eqz v2, :cond_1a

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1117
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1118
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1119
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a25

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1120
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1121
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1122
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1124
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090edf

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1125
    const v4, 0x7f0806b3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1126
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1127
    const v4, 0x7f0806b4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ee0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1131
    :cond_1a
    return-void

    .line 664
    :cond_1b
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    if-eqz v2, :cond_1c

    const-string/jumbo v4, "sq"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 666
    const v4, 0x7f090ed1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 668
    :cond_1c
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    if-eqz v2, :cond_1d

    const-string v4, "hr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 670
    const v4, 0x7f090ed5

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 672
    :cond_1d
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "FR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    if-eqz v2, :cond_1e

    const-string v4, "fr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 674
    const v4, 0x7f090edc

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 676
    :cond_1e
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    if-eqz v2, :cond_1f

    const-string/jumbo v4, "sr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 678
    const v4, 0x7f090ed9

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 680
    :cond_1f
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ES"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    if-eqz v2, :cond_20

    const-string v4, "es"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 682
    const v4, 0x7f090eac

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 685
    :cond_20
    const v4, 0x7f090e72

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 847
    :cond_21
    if-eqz v2, :cond_b

    const-string v4, "en"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 850
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eea

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 851
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eeb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 852
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eec

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 853
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090eed

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090eee

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 857
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eef

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 858
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 859
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a20

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 860
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a21

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 861
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a22

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 862
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090ef3

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090ef4

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 868
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eea

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 869
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eeb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 870
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eec

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 871
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090eed

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090eee

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 875
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090eef

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 876
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 877
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a20

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 878
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a21

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 879
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a22

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 880
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090ef3

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090ef4

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 886
    const v4, 0x7f08068f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 887
    const v4, 0x7f080691

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef7

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 888
    const v4, 0x7f080692

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090ef8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 889
    const v4, 0x7f080694

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090ef9

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efa

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efb

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efc

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 895
    const v4, 0x7f080696

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090efd

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090efe

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090eff

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f00

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f01

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f02

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f03

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090f04

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 906
    const v4, 0x7f080697

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090f05

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 907
    const v4, 0x7f080698

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090f06

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 908
    const v4, 0x7f08069b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090f07

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1
.end method

.method private checkApplicationUpgrade()V
    .locals 3

    .prologue
    .line 303
    sget-object v1, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;

    const-string v2, "checkApplicationUpgrade()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaterButtonClickedOnAppUpdatePopup(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 306
    sget-object v1, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;

    const-string v2, "Check AppStore version"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    new-instance v1, Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSHealthUpdation:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    .line 310
    new-instance v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$5;-><init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V

    .line 322
    .local v0, "onAppUpdateListener":Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSHealthUpdation:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->registerAppUpdateListener(Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;)V

    .line 324
    .end local v0    # "onAppUpdateListener":Lcom/sec/android/app/shealth/settings/SHealthUpdation$OnAppUpdateListener;
    :cond_0
    return-void
.end method

.method private enableNextBtn(Z)V
    .locals 4
    .param p1, "isEnable"    # Z

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3ecccccd    # 0.4f

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 227
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextText:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextImage:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 230
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 227
    goto :goto_0

    :cond_2
    move v1, v2

    .line 228
    goto :goto_1
.end method

.method private initLayout()V
    .locals 10

    .prologue
    const v9, 0x7f080a0f

    const v8, 0x7f080a0b

    const v7, 0x7f08069c

    const/4 v3, 0x0

    const/16 v4, 0x8

    .line 337
    const v2, 0x7f080a0c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    .line 338
    const v2, 0x7f080a10

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    .line 340
    const v2, 0x7f08039d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextText:Landroid/widget/TextView;

    .line 341
    const v2, 0x7f080614

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextImage:Landroid/widget/ImageView;

    .line 343
    const v2, 0x7f080a11

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mCheckLayout_Terms:Landroid/widget/RelativeLayout;

    .line 344
    const v2, 0x7f080a12

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_Terms:Landroid/widget/CheckBox;

    .line 346
    const v2, 0x7f080a14

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mCheckLayout_PP:Landroid/widget/RelativeLayout;

    .line 347
    const v2, 0x7f080a15

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_PP:Landroid/widget/CheckBox;

    .line 349
    const v2, 0x7f080613

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextBtnDescription:Ljava/lang/String;

    .line 351
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09022d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->disabledDescription:Ljava/lang/String;

    .line 353
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->setNextBtnDescription(Z)V

    .line 356
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 360
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->enableNextBtn(Z)V

    .line 375
    :goto_0
    const v2, 0x7f080a16

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titleTermsOfUse:Landroid/widget/TextView;

    .line 376
    const v2, 0x7f08068e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titlePrivacyPolicy:Landroid/widget/TextView;

    .line 377
    const v2, 0x7f080693

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titleEraseData:Landroid/widget/TextView;

    .line 379
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0806d6

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->ppKorSub1Text2:Landroid/widget/TextView;

    .line 380
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0806da

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->ppKorSub2Text3:Landroid/widget/TextView;

    .line 383
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 384
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titleTermsOfUse:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e85

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titlePrivacyPolicy:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e44

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 386
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->titleEraseData:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e6d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a17

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e94

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a18

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e95

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 390
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e96

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e97

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090ea0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 394
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a17

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e94

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a18

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e95

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a19

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e96

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a1a

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e97

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    const v5, 0x7f080a23

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\u200f"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090ea0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v1, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 403
    .local v1, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 405
    .local v0, "strLanguage":Ljava/lang/String;
    if-eqz v0, :cond_6

    const-string v2, "ko"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 409
    const v2, 0x7f080a0e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 410
    const v2, 0x7f080a0a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 411
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 412
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 413
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 414
    const v2, 0x7f08068d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 415
    const v2, 0x7f080a13

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 417
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 419
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->ppKorSub1Text2:Landroid/widget/TextView;

    const v5, 0x7f0905cf

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 420
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0806d9

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 421
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->ppKorSub2Text3:Landroid/widget/TextView;

    const v5, 0x7f0905d0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 422
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0806db

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 423
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0806dc

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 424
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f0806dd

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 538
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    if-eqz v2, :cond_b

    move v2, v3

    :goto_2
    invoke-virtual {v5, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 539
    const v2, 0x7f08028a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    if-eqz v2, :cond_c

    move v2, v3

    :goto_3
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 541
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    if-eqz v2, :cond_2

    .line 542
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 543
    const v2, 0x7f08028a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 546
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    if-eqz v2, :cond_d

    .line 548
    :cond_3
    const v2, 0x7f080a0d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 549
    const v2, 0x7f080a09

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 551
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mCheckLayout_Terms:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 552
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mCheckLayout_PP:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 554
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_Terms:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 555
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_PP:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 557
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$6;-><init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 638
    :goto_4
    return-void

    .line 362
    .end local v0    # "strLanguage":Ljava/lang/String;
    .end local v1    # "systemLocale":Ljava/util/Locale;
    :cond_4
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z

    if-eqz v2, :cond_5

    .line 364
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->enableNextBtn(Z)V

    goto/16 :goto_0

    .line 370
    :cond_5
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->enableNextBtn(Z)V

    goto/16 :goto_0

    .line 427
    .restart local v0    # "strLanguage":Ljava/lang/String;
    .restart local v1    # "systemLocale":Ljava/util/Locale;
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "AU"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v0, :cond_9

    const-string v2, "en"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 430
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 431
    const v2, 0x7f080a0e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 432
    const v2, 0x7f080a0a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 433
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 434
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 435
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 436
    const v2, 0x7f080a13

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 437
    const v2, 0x7f08068d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 440
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "AU"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    if-eqz v0, :cond_7

    const-string v2, "en"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 442
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f080a62

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f09075a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09075b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09075c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f080a62

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f09075a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09075b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09075c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    :cond_7
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_8

    .line 456
    const v2, 0x7f0806ba

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v5, 0x7f090e44

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 457
    const v2, 0x7f0806bb

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v5, 0x7f090e55

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 458
    const v2, 0x7f0806bc

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v5, 0x7f090e80

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 459
    const v2, 0x7f0806bd

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v5, 0x7f090e81

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 460
    const v2, 0x7f0806be

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v5, 0x7f090e7f

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 461
    const v2, 0x7f0806bf

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 462
    const v2, 0x7f0806c0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 463
    const v2, 0x7f0806c1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 464
    const v2, 0x7f0806c2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 465
    const v2, 0x7f0806c3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 466
    const v2, 0x7f0806c4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 467
    const v2, 0x7f0806c5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 468
    const v2, 0x7f0806c6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 469
    const v2, 0x7f0806c7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 470
    const v2, 0x7f0806c8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 471
    const v2, 0x7f0806c9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 472
    const v2, 0x7f0806ca

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 473
    const v2, 0x7f0806cb

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 474
    const v2, 0x7f0806cc

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 475
    const v2, 0x7f0806cd

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 476
    const v2, 0x7f0806ce

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 477
    const v2, 0x7f0806cf

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 478
    const v2, 0x7f0806d0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 479
    const v2, 0x7f0806d1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 480
    const v2, 0x7f0806d2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 485
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "AU"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    const-string v2, "en"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 487
    const v2, 0x7f0806c0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->makeParagraph2Text()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    const v2, 0x7f0806c9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090772

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090773

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090774

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 493
    const v2, 0x7f0806cc

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090777

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090778

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090779

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 499
    const v2, 0x7f0806cd

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f09077b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f09077e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 510
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 511
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 512
    const v2, 0x7f080a0e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 513
    const v2, 0x7f080a0a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 514
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 515
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 516
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 517
    const v2, 0x7f080a13

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 518
    const v2, 0x7f08068d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 520
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a

    .line 522
    const v2, 0x7f080690

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v5, 0x7f090e80

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 523
    const v2, 0x7f080691

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v5, 0x7f090e81

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 524
    const v2, 0x7f080692

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 525
    const v2, 0x7f080693

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 526
    const v2, 0x7f080694

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 527
    const v2, 0x7f080695

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 528
    const v2, 0x7f080696

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 529
    const v2, 0x7f080697

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 530
    const v2, 0x7f080698

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 534
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->applyReviewedText()V

    goto/16 :goto_1

    :cond_b
    move v2, v4

    .line 538
    goto/16 :goto_2

    :cond_c
    move v2, v4

    .line 539
    goto/16 :goto_3

    .line 633
    :cond_d
    const v2, 0x7f080a0d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 634
    const v2, 0x7f080a09

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method private makeParagraph2Text()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1134
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 1136
    .local v0, "enabledPlugIns":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v4, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1137
    .local v4, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 1139
    .local v2, "strLanguage":Ljava/lang/String;
    const v5, 0x7f090e59

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1140
    .local v3, "strParagraph2":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1141
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1143
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_c

    .line 1145
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090020

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_1

    .line 1146
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1143
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1148
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090028

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_2

    .line 1149
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 1151
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090021

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_3

    .line 1152
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1153
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e5a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1155
    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090022

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_4

    .line 1157
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e60

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1159
    :cond_4
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09017e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_5

    .line 1161
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e61

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1163
    :cond_5
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090026

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_6

    .line 1165
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e62

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1167
    :cond_6
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09002c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_7

    .line 1169
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e63

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1171
    :cond_7
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090240

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_8

    .line 1173
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e64

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1175
    :cond_8
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09002f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_9

    .line 1177
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e65

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1179
    :cond_9
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ae

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_a

    .line 1181
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e66

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1183
    :cond_a
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901b4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_b

    .line 1185
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e67

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1187
    :cond_b
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090cf5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-boolean v5, v5, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->enabled:Z

    if-eqz v5, :cond_0

    .line 1189
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e68

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1190
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e69

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1193
    :cond_c
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f090e6a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1194
    return-object v3
.end method

.method private registerProfileFinishedReceiver()V
    .locals 2

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

    if-nez v0, :cond_0

    .line 1270
    new-instance v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;-><init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

    .line 1271
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 1272
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1276
    :cond_0
    return-void
.end method

.method private setNextBtnDescription(Z)V
    .locals 3
    .param p1, "isDisabled"    # Z

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextBtnDescription:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1217
    :cond_0
    :goto_0
    return-void

    .line 1209
    :cond_1
    if-eqz p1, :cond_2

    .line 1211
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextBtnDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1213
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->disabledDescription:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1215
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->nextBtnDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->disabledDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private startUpdateActivity(Z)V
    .locals 3
    .param p1, "isForced"    # Z

    .prologue
    .line 327
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/UpdateCheckActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    .local v0, "appUpdate":Landroid/content/Intent;
    const-string v1, "FORCE_UPDATE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 329
    const-string v1, "FROM_INITIAL_SETTINGS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 330
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 331
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    .line 332
    return-void
.end method

.method private unregisterProfileFinishedReceiver()V
    .locals 1

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

    if-eqz v0, :cond_0

    .line 1285
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$ProfileFinishedReceiver;

    .line 1288
    :cond_0
    return-void
.end method


# virtual methods
.method public closeScreen()V
    .locals 1

    .prologue
    .line 1222
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    if-eqz v0, :cond_1

    .line 1223
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->onBackPressed()V

    .line 1227
    :goto_0
    return-void

    .line 1225
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->closeScreen()V

    goto :goto_0
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 1241
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 1242
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 1243
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1244
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907ec

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 1248
    :goto_0
    return-void

    .line 1246
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907eb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 235
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    if-eqz v2, :cond_0

    .line 236
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 237
    .local v0, "splashIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    .line 238
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->moveTaskToBack(Z)Z

    .line 246
    .end local v0    # "splashIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    if-eqz v2, :cond_1

    .line 240
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 241
    .local v1, "startMain":Landroid/content/Intent;
    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 244
    .end local v1    # "startMain":Landroid/content/Intent;
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 141
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->checkApplicationUpgrade()V

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 147
    .local v2, "termsOfUseIntent":Landroid/content/Intent;
    const-string v3, "isUpgradeFrom35x"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    .line 148
    const-string v3, "isUpgradeFrom35x"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    const-string v3, "isUpgradeFrom35x"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 153
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isUpgradeFrom35x:Z

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v3

    const-string/jumbo v6, "security_pin_enabled"

    invoke-virtual {v3, p0, v6}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v3

    const-string/jumbo v6, "request_password"

    invoke-virtual {v3, p0, v6, v5}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 159
    :cond_1
    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 160
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    .line 163
    const-string v3, "launched_from_settings"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isLaunchedFromSetting:Z

    .line 165
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isLaunchedFromSetting:Z

    if-nez v3, :cond_5

    .line 167
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->isInitialSettingOngoing()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_2
    move v3, v5

    :goto_0
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    .line 168
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v3

    sget-object v6, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v3, p0, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationEnable(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 170
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "1y90e30264"

    const-string v8, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v9, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$4;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$4;-><init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V

    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 197
    :cond_3
    :goto_1
    const-string v3, "Restart_after_update"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->finish()V

    .line 199
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 218
    :goto_2
    return-void

    :cond_4
    move v3, v4

    .line 167
    goto :goto_0

    .line 194
    :cond_5
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isInInitialSettings:Z

    goto :goto_1

    .line 203
    :cond_6
    const v3, 0x7f030244

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->setContentView(I)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 206
    .local v1, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "strLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AU"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    const-string v3, "en"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 209
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAustralia:Z

    .line 212
    :cond_7
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAustralia:Z

    if-eqz v3, :cond_8

    .line 214
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->w:Ljava/lang/ref/WeakReference;

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->runnable:Ljava/lang/Runnable;

    const-wide/16 v5, 0x64

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 217
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->registerProfileFinishedReceiver()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1231
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->unregisterProfileFinishedReceiver()V

    .line 1232
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 1233
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSHealthUpdation:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    if-eqz v0, :cond_0

    .line 1234
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mSHealthUpdation:Lcom/sec/android/app/shealth/settings/SHealthUpdation;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/SHealthUpdation;->cancelCheckTask()V

    .line 1236
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1237
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 293
    const-string v0, "Restart_after_update"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->finish()V

    .line 295
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 299
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 1199
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 1200
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 278
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 279
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 251
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onStart()V

    .line 253
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->initLayout()V

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRestoreOngoing(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isLaunchedFromSetting:Z

    if-nez v1, :cond_1

    .line 256
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_PP:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 257
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck_PP:Z

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_Terms:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 260
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->isAgreeCheck:Z

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mNextImage:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 263
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->enableNextBtn(Z)V

    .line 266
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->setNextBtnDescription(Z)V

    .line 269
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 270
    .local v0, "intent":Landroid/content/Intent;
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 271
    const-string/jumbo v1, "profileRestored"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 272
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->startActivity(Landroid/content/Intent;)V

    .line 274
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 287
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mToast1:Landroid/widget/Toast;

    .line 289
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onStop()V

    .line 290
    return-void
.end method
