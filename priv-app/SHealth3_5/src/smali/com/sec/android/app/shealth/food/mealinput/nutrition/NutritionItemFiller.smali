.class public Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;
.super Ljava/lang/Object;
.source "NutritionItemFiller.java"


# static fields
.field private static final CHOLESTEROL:F = 3.0f

.field private static final DEFAULT_SERVING_SIZE:I = 0x1

.field private static final DIETARY_FIBER:F = 0.25f

.field private static final SANS_SERIF:Ljava/lang/String; = "sans-serif"

.field private static final SANS_SERIF_LIGHT:Ljava/lang/String; = "sans-serif-light"

.field private static final SATURATED_FAT:F = 0.2f

.field private static final SODIUM:F = 24.0f

.field private static final TOTAL_CARBOHYDRATE:F = 3.0f

.field private static final TOTAL_FAT:F = 0.65f


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mDelimiter:Ljava/lang/String;

.field private final mGramUnit:Ljava/lang/String;

.field private final mMiliGramUnit:Ljava/lang/String;

.field private final mPercentFormat:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mContext:Landroid/content/Context;

    .line 62
    const v0, 0x7f0900bf

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mGramUnit:Ljava/lang/String;

    .line 63
    const v0, 0x7f0900be

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mMiliGramUnit:Ljava/lang/String;

    .line 64
    const v0, 0x7f090976

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mDelimiter:Ljava/lang/String;

    .line 65
    const v0, 0x7f090d1a

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mPercentFormat:Ljava/lang/String;

    .line 66
    return-void
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static hideView(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 209
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 210
    return-void
.end method

.method private static varargs hideViews([Landroid/view/View;)V
    .locals 4
    .param p0, "views"    # [Landroid/view/View;

    .prologue
    .line 218
    move-object v0, p0

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 219
    .local v3, "view":Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->hideView(Landroid/view/View;)V

    .line 218
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 221
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private setTextOrHideAssociatedGroupViews(Ljava/lang/String;Landroid/widget/TextView;FLandroid/widget/TextView;FLandroid/view/View;Ljava/lang/String;Landroid/widget/TextView;F)V
    .locals 5
    .param p1, "labelString"    # Ljava/lang/String;
    .param p2, "valueTextView"    # Landroid/widget/TextView;
    .param p3, "value"    # F
    .param p4, "percentTextView"    # Landroid/widget/TextView;
    .param p5, "perPercentValue"    # F
    .param p6, "splitLineView"    # Landroid/view/View;
    .param p7, "secondLabelString"    # Ljava/lang/String;
    .param p8, "secondValueTextView"    # Landroid/widget/TextView;
    .param p9, "secondValue"    # F

    .prologue
    .line 170
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p9, v0

    if-ltz v0, :cond_3

    .line 171
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 172
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_1

    .line 173
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 174
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mDelimiter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mGramUnit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mPercentFormat:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    div-float v4, p3, p5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    :goto_0
    const/4 v0, 0x0

    cmpl-float v0, p9, v0

    if-ltz v0, :cond_2

    .line 181
    const/4 v0, 0x0

    invoke-virtual {p8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mDelimiter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mGramUnit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    :goto_1
    return-void

    .line 178
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->hideViews([Landroid/view/View;)V

    goto :goto_0

    .line 184
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p8, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->hideViews([Landroid/view/View;)V

    goto :goto_1

    .line 187
    :cond_3
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    aput-object p8, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->hideViews([Landroid/view/View;)V

    goto :goto_1
.end method

.method private setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .param p1, "labelAndValueTextView"    # Landroid/widget/TextView;
    .param p2, "value"    # F
    .param p3, "unit"    # Ljava/lang/String;
    .param p4, "percentTextView"    # Landroid/widget/TextView;
    .param p5, "perPercentValue"    # F
    .param p6, "splitLineView"    # Landroid/view/View;
    .param p7, "labelTextResId"    # I
    .param p8, "labelFontFamily"    # Ljava/lang/String;
    .param p9, "valueFontFamily"    # Ljava/lang/String;
    .param p10, "isAddedByUser"    # Z

    .prologue
    .line 135
    const/4 v4, 0x0

    cmpl-float v4, p2, v4

    if-ltz v4, :cond_2

    .line 136
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mContext:Landroid/content/Context;

    move/from16 v0, p7

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "label":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139
    .local v3, "valueAndUnit":Ljava/lang/String;
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 140
    .local v1, "builder":Landroid/text/SpannableStringBuilder;
    new-instance v4, Landroid/text/style/TypefaceSpan;

    move-object/from16 v0, p8

    invoke-direct {v4, v0}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 141
    const-string v4, " "

    invoke-virtual {v1, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 142
    new-instance v4, Landroid/text/style/TypefaceSpan;

    move-object/from16 v0, p9

    invoke-direct {v4, v0}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 143
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    if-eqz p4, :cond_0

    if-nez p10, :cond_0

    .line 145
    const/4 v4, 0x0

    invoke-virtual {p4, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mPercentFormat:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    div-float v8, p2, p5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p4, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p6, v4}, Landroid/view/View;->setVisibility(I)V

    .line 156
    .end local v1    # "builder":Landroid/text/SpannableStringBuilder;
    .end local v2    # "label":Ljava/lang/String;
    .end local v3    # "valueAndUnit":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 151
    :cond_2
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/view/View;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p6, v4, v5

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->hideViews([Landroid/view/View;)V

    .line 152
    if-eqz p4, :cond_1

    .line 153
    invoke-static {p4}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->hideView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;IZ)V
    .locals 11
    .param p1, "labelAndValueTextView"    # Landroid/widget/TextView;
    .param p2, "value"    # F
    .param p3, "unit"    # Ljava/lang/String;
    .param p4, "percentTextView"    # Landroid/widget/TextView;
    .param p5, "perPercentValue"    # F
    .param p6, "splitLineView"    # Landroid/view/View;
    .param p7, "labelTextResId"    # I
    .param p8, "isAddedByUser"    # Z

    .prologue
    .line 162
    const-string/jumbo v8, "sans-serif"

    const-string/jumbo v9, "sans-serif-light"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 164
    return-void
.end method


# virtual methods
.method public fillView(Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)V
    .locals 10
    .param p1, "viewHolder"    # Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;
    .param p2, "mealItemData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .param p3, "foodInfo"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p4, "extraFoodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 73
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->titleName:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    if-eqz p4, :cond_1

    .line 76
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->servingSize:Landroid/widget/TextView;

    const v1, 0x7f090972

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getUnit()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextToTextViewWithDelimiter(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->calories:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dropDownButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 80
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dropDownClickableArea:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 82
    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dailyValues:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->servingSize:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "1 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09017b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dailyValuesInfo:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    :cond_0
    iget-object v1, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalFat:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mGramUnit:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalFatPercent:Landroid/widget/TextView;

    const v5, 0x3f266666    # 0.65f

    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v6, 0x0

    aget-object v6, v0, v6

    const v7, 0x7f09096f

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v8

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;IZ)V

    .line 93
    iget-object v1, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalCarbohydrate:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mGramUnit:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->totalCarbohydratePercent:Landroid/widget/TextView;

    const/high16 v5, 0x40400000    # 3.0f

    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v6, 0x4

    aget-object v6, v0, v6

    const v7, 0x7f090961

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v8

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;IZ)V

    .line 96
    iget-object v1, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->protein:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mGramUnit:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v6, 0x6

    aget-object v6, v0, v6

    const v7, 0x7f090966

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v8

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;IZ)V

    .line 98
    iget-object v1, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->cholesterol:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCholesterol()I

    move-result v0

    int-to-float v2, v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mMiliGramUnit:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->cholesterolPercent:Landroid/widget/TextView;

    const/high16 v5, 0x40400000    # 3.0f

    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v6, 0x2

    aget-object v6, v0, v6

    const v7, 0x7f090964

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v8

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;IZ)V

    .line 101
    iget-object v1, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->sodium:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getSodium()I

    move-result v0

    int-to-float v2, v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mMiliGramUnit:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->sodiumPercent:Landroid/widget/TextView;

    const/high16 v5, 0x41c00000    # 24.0f

    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v6, 0x3

    aget-object v6, v0, v6

    const v7, 0x7f090965

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v8

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedViews(Landroid/widget/TextView;FLjava/lang/String;Landroid/widget/TextView;FLandroid/view/View;IZ)V

    .line 104
    const v0, 0x7f09096d

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->saturatedFat:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getSaturated()F

    move-result v3

    iget-object v4, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->saturatedFatPercent:Landroid/widget/TextView;

    const v5, 0x3e4ccccd    # 0.2f

    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v6, 0x1

    aget-object v6, v0, v6

    const v0, 0x7f09096e

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->transFat:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getTransFat()F

    move-result v9

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedGroupViews(Ljava/lang/String;Landroid/widget/TextView;FLandroid/widget/TextView;FLandroid/view/View;Ljava/lang/String;Landroid/widget/TextView;F)V

    .line 108
    const v0, 0x7f09096b

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dietaryFiber:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDietary()F

    move-result v3

    iget-object v4, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dietaryFiberPercent:Landroid/widget/TextView;

    const/high16 v5, 0x3e800000    # 0.25f

    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->splitLines:[Landroid/view/View;

    const/4 v6, 0x4

    aget-object v6, v0, v6

    const v0, 0x7f09096c

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->sugars:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getSugar()F

    move-result v9

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->setTextOrHideAssociatedGroupViews(Ljava/lang/String;Landroid/widget/TextView;FLandroid/widget/TextView;FLandroid/view/View;Ljava/lang/String;Landroid/widget/TextView;F)V

    .line 112
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->nutritionBlocksView:Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionBlockView;

    const/4 v1, 0x4

    new-array v1, v1, [Landroid/util/Pair;

    const/4 v2, 0x0

    new-instance v3, Landroid/util/Pair;

    const v4, 0x7f090967

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getVitaminA()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Landroid/util/Pair;

    const v4, 0x7f090968

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getVitaminC()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Landroid/util/Pair;

    const v4, 0x7f090969

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCalcium()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Landroid/util/Pair;

    const v4, 0x7f09096a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getIron()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionBlockView;->setBlocks([Landroid/util/Pair;)V

    .line 130
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->servingSize:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "1 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09017b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->calories:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertFloatKcalValueToInt(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dropDownButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    iget-object v0, p1, Lcom/sec/android/app/shealth/food/mealinput/nutrition/ViewHolder;->dropDownClickableArea:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected setTextToTextViewWithDelimiter(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "textStart"    # Ljava/lang/String;
    .param p3, "textEnd"    # Ljava/lang/String;

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionItemFiller;->mDelimiter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    return-void
.end method
