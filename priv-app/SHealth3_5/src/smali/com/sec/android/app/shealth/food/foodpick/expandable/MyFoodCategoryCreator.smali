.class public Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;
.super Ljava/lang/Object;
.source "MyFoodCategoryCreator.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/MyFoodCategoryCreator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public getFoodListItems(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v4, "myFoodsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-static {p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMyFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 34
    .local v2, "foodInfoDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 35
    .local v0, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 36
    .local v1, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;

    invoke-direct {v5, v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    .end local v1    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_0
    return-object v4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 48
    return-void
.end method
