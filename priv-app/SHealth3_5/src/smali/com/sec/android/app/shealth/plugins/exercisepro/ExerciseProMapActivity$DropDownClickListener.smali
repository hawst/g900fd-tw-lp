.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DropDownClickListener"
.end annotation


# instance fields
.field private curDataType:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

.field private viewIdx:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Landroid/view/View;II)V
    .locals 0
    .param p2, "v"    # Landroid/view/View;
    .param p3, "viewIdx"    # I
    .param p4, "dataType"    # I

    .prologue
    .line 1201
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202
    iput p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->viewIdx:I

    .line 1203
    iput p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->curDataType:I

    .line 1204
    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;

    .prologue
    .line 1197
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->curDataType:I

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;

    .prologue
    .line 1197
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->viewIdx:I

    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 21
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1208
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->temporarilyDisableClick([Landroid/view/View;)V

    .line 1209
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1210
    .local v9, "data_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->DISP_DATA_LIST:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Integer;

    .line 1211
    .local v16, "type":Ljava/lang/Integer;
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1213
    .end local v16    # "type":Ljava/lang/Integer;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->isAvailableBarometer(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1215
    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1216
    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1221
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 1222
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1223
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1224
    :cond_2
    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1225
    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1226
    :cond_3
    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1227
    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1248
    :cond_4
    :goto_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 1249
    .local v13, "len":I
    new-array v4, v13, [Ljava/lang/String;

    .line 1250
    .local v4, "items":[Ljava/lang/String;
    new-instance v17, Ljava/util/HashMap;

    move-object/from16 v0, v17

    invoke-direct {v0, v13}, Ljava/util/HashMap;-><init>(I)V

    .line 1251
    .local v17, "typesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v8, 0x0

    .line 1252
    .local v8, "curIdx":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    if-ge v10, v13, :cond_c

    .line 1253
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 1254
    .local v16, "type":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;

    move-result-object v2

    move/from16 v0, v16

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getListDataStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v14

    .line 1255
    .local v14, "name":Ljava/lang/String;
    aput-object v14, v4, v10

    .line 1256
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1257
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->curDataType:I

    move/from16 v0, v16

    if-ne v0, v2, :cond_5

    .line 1258
    move v8, v10

    .line 1252
    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 1230
    .end local v4    # "items":[Ljava/lang/String;
    .end local v8    # "curIdx":I
    .end local v10    # "i":I
    .end local v13    # "len":I
    .end local v14    # "name":Ljava/lang/String;
    .end local v16    # "type":I
    .end local v17    # "typesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_9

    .line 1231
    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1232
    const/4 v2, 0x5

    const/16 v3, 0x17

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1233
    :cond_7
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1234
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1235
    :cond_8
    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1236
    const/4 v2, 0x6

    const/16 v3, 0xe

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 1239
    :cond_9
    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1240
    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1241
    :cond_a
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1242
    const/4 v2, 0x5

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1243
    :cond_b
    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1244
    const/4 v2, 0x6

    const/16 v3, 0xe

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 1261
    .restart local v4    # "items":[Ljava/lang/String;
    .restart local v8    # "curIdx":I
    .restart local v10    # "i":I
    .restart local v13    # "len":I
    .restart local v17    # "typesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_c
    const v2, 0x7f0806de

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1263
    .local v5, "dropdownboxView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    move-object/from16 v20, v0

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopupImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->internalViewIdUniquizeHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopupImpl;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V

    move-object/from16 v0, v20

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .line 1265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x41400000    # 12.0f

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v12, v2

    .line 1266
    .local v12, "leftPadding":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x41900000    # 18.0f

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v15, v2

    .line 1268
    .local v15, "rightPadding":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    invoke-direct {v3, v12, v15}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setTextPaddings(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;)V

    .line 1269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setCurrentItem(I)V

    .line 1270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v2

    const v3, 0x20002

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setAlignment(I)V

    .line 1271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v3, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;Ljava/util/Map;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setOnItemClickedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;)V

    .line 1298
    if-eqz v5, :cond_d

    .line 1300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v2

    add-int v3, v12, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v2, v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getXOffset(Landroid/view/View;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;ILandroid/content/Context;)I

    move-result v18

    .line 1303
    .local v18, "xoffset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x42680000    # 58.0f

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    neg-int v0, v2

    move/from16 v19, v0

    .line 1304
    .local v19, "yoffset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    move-result-object v2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->show(II)V

    .line 1306
    .end local v18    # "xoffset":I
    .end local v19    # "yoffset":I
    :cond_d
    return-void
.end method
