.class public Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;
.super Ljava/lang/Object;
.source "TextDrawer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;
    }
.end annotation


# instance fields
.field private fontName:Ljava/lang/String;

.field private outlineAlpha:F

.field private outlineColor:I

.field private outlineEnabled:Z

.field private outlineRadius:I

.field private shadowAlpha:F

.field private shadowColor:I

.field private shadowDx:I

.field private shadowDy:I

.field private shadowEnabled:Z

.field private shadowRadius:I

.field private textAlpha:F

.field private textColor:I

.field private textSize:F


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->textSize:F
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$000(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->textSize:F

    .line 176
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->fontName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$100(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->fontName:Ljava/lang/String;

    .line 177
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->textColor:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$200(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->textColor:I

    .line 178
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->textAlpha:F
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$300(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->textAlpha:F

    .line 179
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowEnabled:Z
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$400(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->shadowEnabled:Z

    .line 180
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowColor:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$500(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->shadowColor:I

    .line 181
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowAlpha:F
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$600(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->shadowAlpha:F

    .line 182
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowDx:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$700(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->shadowDx:I

    .line 183
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowDy:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$800(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->shadowDy:I

    .line 184
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->shadowRadius:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$900(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->shadowRadius:I

    .line 185
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineEnabled:Z
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$1000(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->outlineEnabled:Z

    .line 186
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineColor:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$1100(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->outlineColor:I

    .line 187
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineRadius:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$1200(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->outlineRadius:I

    .line 188
    # getter for: Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->outlineAlpha:F
    invoke-static {p1}, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;->access$1300(Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer$Builder;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/TextDrawer;->outlineAlpha:F

    .line 189
    return-void
.end method
