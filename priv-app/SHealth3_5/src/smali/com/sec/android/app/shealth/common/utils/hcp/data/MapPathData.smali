.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
.super Ljava/lang/Object;
.source "MapPathData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private accuracy:F

.field private altitude:F

.field private createTime:J

.field private exerciseId:J

.field private id:J

.field private latitude:D

.field private longitude:D

.field private timeZone:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->id:J

    .line 39
    return-void
.end method

.method public constructor <init>(JJDDFFJI)V
    .locals 4
    .param p1, "id"    # J
    .param p3, "exerciseId"    # J
    .param p5, "latitude"    # D
    .param p7, "longitude"    # D
    .param p9, "altitude"    # F
    .param p10, "accuracy"    # F
    .param p11, "createTime"    # J
    .param p13, "timeZone"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-wide/16 v1, -0x2

    iput-wide v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->id:J

    .line 23
    const-wide/16 v1, -0x2

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    .line 25
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id should not be "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 27
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->id:J

    .line 28
    iput-wide p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->exerciseId:J

    .line 29
    iput-wide p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->latitude:D

    .line 30
    iput-wide p7, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->longitude:D

    .line 31
    iput p9, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->altitude:F

    .line 32
    iput p10, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->accuracy:F

    .line 33
    iput-wide p11, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->createTime:J

    .line 34
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->timeZone:I

    .line 35
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public getAccuracy()F
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->accuracy:F

    return v0
.end method

.method public getAltitude()F
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->altitude:F

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->createTime:J

    return-wide v0
.end method

.method public getExerciseId()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->exerciseId:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->id:J

    return-wide v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->longitude:D

    return-wide v0
.end method

.method public getTimeZone()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->timeZone:I

    return v0
.end method

.method public setAccuracy(F)V
    .locals 0
    .param p1, "accuracy"    # F

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->accuracy:F

    .line 99
    return-void
.end method

.method public setAltitude(F)V
    .locals 0
    .param p1, "altitude"    # F

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->altitude:F

    .line 89
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .param p1, "createTime"    # J

    .prologue
    .line 108
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->createTime:J

    .line 109
    return-void
.end method

.method public setExerciseId(J)V
    .locals 0
    .param p1, "exerciseId"    # J

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->exerciseId:J

    .line 59
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 48
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->id:J

    .line 49
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "latitude"    # D

    .prologue
    .line 68
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->latitude:D

    .line 69
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "longitude"    # D

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->longitude:D

    .line 79
    return-void
.end method

.method public setTimeZone(I)V
    .locals 0
    .param p1, "timeZone"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->timeZone:I

    .line 119
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 124
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 125
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->exerciseId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 126
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->latitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 127
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->longitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 128
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->altitude:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 129
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->accuracy:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 130
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->createTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 131
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->timeZone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    return-void
.end method
