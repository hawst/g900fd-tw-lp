.class public Lcom/sec/android/app/shealth/common/gallery/ScalableView;
.super Landroid/widget/ImageView;
.source "ScalableView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;,
        Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;
    }
.end annotation


# static fields
.field private static final MAX_SCALE:F = 3.0f

.field private static final MIN_SCALE:F = 1.0f


# instance fields
.field private mBitmapHeight:F

.field private mBitmapWidth:F

.field private mDoubleTapGestureDetector:Landroid/view/GestureDetector;

.field private mLastPoint:Landroid/graphics/PointF;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMatrixValue:[F

.field private mRedundantMarginLeftAndRight:F

.field private mRedundantMarginTopAndBottom:F

.field private mSaveScale:F

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mStartHeightOfBitmapInImageView:F

.field private mStartPoint:Landroid/graphics/PointF;

.field private mStartWidthOfBitmapInImageView:F

.field private mViewHeight:F

.field private mViewPagerChangeStats:Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;

.field private mViewWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    .line 39
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;

    .line 40
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartPoint:Landroid/graphics/PointF;

    .line 41
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F

    .line 42
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->init()V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    .line 39
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;

    .line 40
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartPoint:Landroid/graphics/PointF;

    .line 41
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F

    .line 42
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->init()V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    .line 39
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;

    .line 40
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartPoint:Landroid/graphics/PointF;

    .line 41
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F

    .line 42
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->init()V

    .line 63
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartWidthOfBitmapInImageView:F

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartHeightOfBitmapInImageView:F

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->centeredBitmapResource()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewPagerChangeStats:Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/common/gallery/ScalableView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    return p1
.end method

.method static synthetic access$332(Lcom/sec/android/app/shealth/common/gallery/ScalableView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/view/ScaleGestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mDoubleTapGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mLastPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/gallery/ScalableView;FF)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;
    .param p1, "x1"    # F
    .param p2, "x2"    # F

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->correctMatrixMeasure(FF)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)Landroid/graphics/Matrix;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private centeredBitmapResource()V
    .locals 6

    .prologue
    .line 152
    iget v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F

    iget v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mBitmapWidth:F

    div-float v1, v3, v4

    .line 153
    .local v1, "scaleX":F
    iget v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F

    iget v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mBitmapHeight:F

    div-float v2, v3, v4

    .line 154
    .local v2, "scaleY":F
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 155
    .local v0, "scale":F
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 157
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    .line 159
    iget v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mBitmapWidth:F

    mul-float/2addr v3, v1

    iput v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartWidthOfBitmapInImageView:F

    .line 160
    iget v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mBitmapHeight:F

    mul-float/2addr v3, v0

    iput v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartHeightOfBitmapInImageView:F

    .line 161
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mRedundantMarginLeftAndRight:F

    .line 162
    iget v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F

    iget v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartHeightOfBitmapInImageView:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mRedundantMarginTopAndBottom:F

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    iget v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mRedundantMarginLeftAndRight:F

    iget v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mRedundantMarginTopAndBottom:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 165
    return-void
.end method

.method private correctMatrixMeasure(FF)V
    .locals 12
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F

    const/4 v1, 0x2

    aget v2, v0, v1

    .line 109
    .local v2, "x":F
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrixValue:[F

    const/4 v1, 0x5

    aget v6, v0, v1

    .line 110
    .local v6, "y":F
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartWidthOfBitmapInImageView:F

    iget v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v3, v0

    .line 111
    .local v3, "scaledWidth":F
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mStartHeightOfBitmapInImageView:F

    iget v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mSaveScale:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v7, v0

    .line 112
    .local v7, "scaledHeight":F
    iget v4, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F

    iget v5, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mRedundantMarginLeftAndRight:F

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->getAxisPostTranslate(FFFFF)F

    move-result v10

    .line 113
    .local v10, "deltaXPostTranslate":F
    iget v8, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F

    iget v9, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mRedundantMarginTopAndBottom:F

    move-object v4, p0

    move v5, p2

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->getAxisPostTranslate(FFFFF)F

    move-result v11

    .line 114
    .local v11, "deltaYPostTranslate":F
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v10, v11}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 115
    return-void
.end method

.method private getAxisPostTranslate(FFFFF)F
    .locals 3
    .param p1, "deltaOfMoving"    # F
    .param p2, "currentBorderPosition"    # F
    .param p3, "scaledBitmapSize"    # F
    .param p4, "viewSize"    # F
    .param p5, "borderSize"    # F

    .prologue
    .line 118
    move v0, p1

    .line 119
    .local v0, "resultDeltaOfMoving":F
    cmpl-float v1, p3, p4

    if-ltz v1, :cond_2

    .line 120
    add-float v1, p2, p1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 121
    neg-float v0, p2

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 122
    :cond_1
    add-float v1, p2, p1

    sub-float v2, p3, p4

    neg-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 123
    sub-float v1, p3, p4

    add-float/2addr v1, p2

    neg-float v0, v1

    goto :goto_0

    .line 126
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;

    if-eqz v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewPagerChangeStats:Lcom/sec/android/app/shealth/common/gallery/ViewPagerChangeStats;

    .line 69
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setClickable(Z)V

    .line 70
    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 71
    new-instance v1, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;

    invoke-direct {v3, p0, v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView$ScaleListener;-><init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;)V

    invoke-direct {v1, v2, v3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 72
    new-instance v0, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;

    invoke-direct {v0, p0, v5}, Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;-><init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;)V

    .line 73
    .local v0, "gestureListener":Lcom/sec/android/app/shealth/common/gallery/ScalableView$DoubleTapListener;
    new-instance v1, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mDoubleTapGestureDetector:Landroid/view/GestureDetector;

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mDoubleTapGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, v0}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v4, v4}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 77
    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView$1;-><init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 104
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 143
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 145
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewWidth:F

    .line 146
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mViewHeight:F

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->centeredBitmapResource()V

    .line 148
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 134
    if-eqz p1, :cond_0

    .line 135
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mBitmapWidth:F

    .line 136
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->mBitmapHeight:F

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->centeredBitmapResource()V

    .line 139
    :cond_0
    return-void
.end method
