.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeleteAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_2

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mIsItemsSelected:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getLogDataTypeURI()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_2
    return-object v5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 14

    const-wide/16 v12, 0x0

    const-wide v10, -0x245cafe690d24d62L    # -2.741670887990383E133

    const/4 v9, 0x1

    const/16 v8, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v4, v9

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1800(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)I

    move-result v1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v5, v4, v2

    long-to-int v2, v5

    if-gtz v2, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v1, v1

    shl-long/2addr v1, v8

    ushr-long v5, v1, v8

    aget-wide v1, v4, v3

    cmp-long v7, v1, v12

    if-eqz v7, :cond_1

    xor-long/2addr v1, v10

    :cond_1
    ushr-long/2addr v1, v8

    shl-long/2addr v1, v8

    xor-long/2addr v1, v5

    xor-long/2addr v1, v10

    aput-wide v1, v4, v3

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v4, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v9, [I

    const/16 v0, 0x6262

    aput v0, v1, v3

    new-array v0, v9, [I

    const/16 v2, 0x6252

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v1, v4, v3

    cmp-long v3, v1, v12

    if-eqz v3, :cond_5

    xor-long/2addr v1, v10

    :cond_5
    shl-long/2addr v1, v8

    shr-long/2addr v1, v8

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->hideLoadingPopup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1600(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->applyFilter(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshAdapter()V

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->removeDeleteMode()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1900(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->registerContentObserver()V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->unregisterContentObserver()V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->deleting:I

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showLoadingPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
