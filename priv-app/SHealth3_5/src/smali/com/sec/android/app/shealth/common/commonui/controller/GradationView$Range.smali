.class Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
.super Ljava/lang/Object;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Range"
.end annotation


# instance fields
.field public final end:F

.field public final start:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1, "start"    # F
    .param p2, "end"    # F

    .prologue
    .line 723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 724
    iput p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    .line 725
    iput p2, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    .line 726
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    .param p1, "x1"    # F

    .prologue
    .line 719
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->isValueInRange(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    .param p1, "x1"    # F

    .prologue
    .line 719
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->getValueLocation(F)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;
    .param p1, "x1"    # F

    .prologue
    .line 719
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->ensureValueInRange(F)F

    move-result v0

    return v0
.end method

.method private ensureValueInRange(F)F
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 750
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 751
    iget p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    .line 756
    .end local p1    # "value":F
    :cond_0
    :goto_0
    return p1

    .line 753
    .restart local p1    # "value":F
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 754
    iget p1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    goto :goto_0
.end method

.method private getValueLocation(F)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 734
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 735
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->BEFORE:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    .line 740
    :goto_0
    return-object v0

    .line 737
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 738
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->AFTER:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    goto :goto_0

    .line 740
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;->IN:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$LocationRelativeToRange;

    goto :goto_0
.end method

.method private isValueInRange(F)Z
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 764
    iget v0, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->start:F

    iget v1, p0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$Range;->end:F

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isInRange(FFF)Z

    move-result v0

    return v0
.end method
