.class Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;
.super Ljava/lang/Object;
.source "ComfortZoneLevelActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->initTextWachers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 250
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->removeSpace(Landroid/text/Editable;)V
    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$400(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;Landroid/text/Editable;)V

    .line 251
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 254
    .local v1, "currentstring":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_TEMPETATURE:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    .line 255
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 259
    .local v0, "currentTemperature":F
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->temperatureTo:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 267
    .local v2, "secondTemperature":F
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setButtonState()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$700(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)V

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_TEMPETATURE:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v3

    cmpg-float v3, v3, v0

    if-gtz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_TEMPETATURE:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$600(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v3

    cmpg-float v3, v0, v3

    if-gtz v3, :cond_0

    .line 270
    cmpg-float v3, v0, v2

    if-gez v3, :cond_3

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setTemperatureRangeMinimal(F)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$800(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    .line 272
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setTemperatureRangeMaximal(F)V
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$900(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    .line 278
    :cond_0
    :goto_2
    return-void

    .line 257
    .end local v0    # "currentTemperature":F
    .end local v2    # "secondTemperature":F
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MINIMAL_TEMPETATURE:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$500(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v0

    .restart local v0    # "currentTemperature":F
    goto :goto_0

    .line 265
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # getter for: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->MAXIMAL_TEMPETATURE:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$600(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;)F

    move-result v2

    .restart local v2    # "secondTemperature":F
    goto :goto_1

    .line 273
    :cond_3
    cmpl-float v3, v0, v2

    if-lez v3, :cond_0

    .line 274
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setTemperatureRangeMaximal(F)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$900(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity$3;->this$0:Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;

    # invokes: Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->setTemperatureRangeMinimal(F)V
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;->access$800(Lcom/sec/android/app/shealth/thermohygrometer/ComfortZoneLevelActivity;F)V

    goto :goto_2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 246
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 241
    return-void
.end method
