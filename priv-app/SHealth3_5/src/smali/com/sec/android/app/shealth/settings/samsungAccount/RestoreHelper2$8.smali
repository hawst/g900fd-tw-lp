.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showRestoreConfirmation(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0

    .prologue
    .line 957
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 10
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 962
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v7

    if-eqz v7, :cond_5

    .line 963
    const v7, 0x7f0804ef

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 964
    .local v2, "doNotShowAgainCehckBox":Landroid/widget/CheckBox;
    if-eqz v2, :cond_0

    .line 965
    const/16 v7, 0x8

    invoke-virtual {v2, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 967
    :cond_0
    const v7, 0x7f080681

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 968
    .local v3, "doNotShowAgainTextView":Landroid/widget/TextView;
    if-eqz v3, :cond_1

    .line 969
    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 971
    :cond_1
    const v7, 0x7f080683

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 972
    .local v0, "backupRestoreCheckBox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_2

    .line 973
    const/16 v7, 0x8

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 975
    :cond_2
    const v7, 0x7f080684

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 976
    .local v1, "backupRestoreTextView":Landroid/widget/TextView;
    if-eqz v1, :cond_3

    .line 977
    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 978
    :cond_3
    const v7, 0x7f0804ed

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f090d43

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 1033
    .end local v0    # "backupRestoreCheckBox":Landroid/widget/CheckBox;
    .end local v1    # "backupRestoreTextView":Landroid/widget/TextView;
    .end local v2    # "doNotShowAgainCehckBox":Landroid/widget/CheckBox;
    .end local v3    # "doNotShowAgainTextView":Landroid/widget/TextView;
    :cond_4
    :goto_0
    return-void

    .line 981
    :cond_5
    const/4 v4, 0x0

    .line 982
    .local v4, "mPopupDesc":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRestoreAgainHomePopup(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUpgradeStatus(Landroid/content/Context;)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_7

    .line 984
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const/4 v8, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->isFwGRestoreFromHome:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    .line 985
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f090d45

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f090d46

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 991
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 992
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "\u200f"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 995
    :cond_6
    const v7, 0x7f0804ed

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 997
    const v7, 0x7f080681

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f09078c

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 998
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v7, 0x7f0804ef

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v8, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 1000
    const v7, 0x7f0804ee

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    new-instance v8, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1009
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v7

    if-nez v7, :cond_4

    .line 1012
    new-instance v6, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1013
    .local v6, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_8

    const/4 v5, 0x1

    .line 1015
    .local v5, "needToBackup":Z
    :goto_2
    if-eqz v5, :cond_4

    .line 1017
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v7, 0x7f080683

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mBackupRestore:Landroid/widget/CheckBox;
    invoke-static {v8, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 1018
    const v7, 0x7f080684

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f090d44

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 1020
    const v7, 0x7f080682

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    new-instance v8, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 989
    .end local v5    # "needToBackup":Z
    .end local v6    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupDescRID:I
    invoke-static {v8}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$2400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1013
    .restart local v6    # "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_8
    const/4 v5, 0x0

    goto :goto_2
.end method
