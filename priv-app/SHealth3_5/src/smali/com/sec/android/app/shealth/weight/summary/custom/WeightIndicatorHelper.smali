.class final Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;
.super Ljava/lang/Object;
.source "WeightIndicatorHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method static calculateAlpha(I)I
    .locals 2
    .param p0, "alphaInPercent"    # I

    .prologue
    .line 126
    mul-int/lit16 v0, p0, 0xff

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method static getAngleForValue(FF)F
    .locals 2
    .param p0, "value"    # F
    .param p1, "weight"    # F

    .prologue
    .line 74
    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getMinWeightInPanel(F)F

    move-result v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-gez v0, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 79
    :goto_0
    return v0

    .line 76
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getMaxWeightInPanel(F)F

    move-result v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-lez v0, :cond_1

    .line 77
    const/high16 v0, -0x3c790000    # -270.0f

    goto :goto_0

    .line 79
    :cond_1
    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getMinWeightInPanel(F)F

    move-result v1

    sub-float v1, p0, v1

    mul-float/2addr v0, v1

    const v1, 0x3dcccccd    # 0.1f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method static getAngleForWeightValue(F)F
    .locals 1
    .param p0, "weight"    # F

    .prologue
    .line 115
    invoke-static {p0, p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getAngleForValue(FF)F

    move-result v0

    return v0
.end method

.method static getMaxWeightInPanel(F)F
    .locals 2
    .param p0, "weight"    # F

    .prologue
    .line 106
    invoke-static {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getMinWeightInPanel(F)F

    move-result v0

    const/high16 v1, 0x41d80000    # 27.0f

    add-float/2addr v0, v1

    return v0
.end method

.method static getMinWeightInPanel(F)F
    .locals 2
    .param p0, "weight"    # F

    .prologue
    .line 91
    const v0, 0x3dcccccd    # 0.1f

    div-float v0, p0, v0

    const/high16 v1, 0x41c00000    # 24.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 93
    const v0, 0x41b1999a    # 22.2f

    sub-float v0, p0, v0

    float-to-int v0, v0

    int-to-float v0, v0

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static getStartAngle(FF)F
    .locals 1
    .param p0, "goalValue"    # F
    .param p1, "weight"    # F

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->isGoalInDials(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getAngleForValue(FF)F

    move-result v0

    goto :goto_0
.end method

.method static getStartValue(FF)F
    .locals 1
    .param p0, "goalValue"    # F
    .param p1, "weight"    # F

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->isGoalInDials(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getMinWeightInPanel(F)F

    move-result p0

    .line 61
    .end local p0    # "goalValue":F
    :cond_0
    return p0
.end method

.method private static isGoalInDials(FF)Z
    .locals 1
    .param p0, "goalValue"    # F
    .param p1, "weight"    # F

    .prologue
    .line 130
    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getMinWeightInPanel(F)F

    move-result v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
