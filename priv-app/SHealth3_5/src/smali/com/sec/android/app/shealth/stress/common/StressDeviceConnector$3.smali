.class Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$3;
.super Ljava/lang/Object;
.source "StressDeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 358
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    move-object v0, p2

    .line 359
    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;

    .line 361
    .local v0, "hrm":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;
    iget-object v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 370
    .end local v0    # "hrm":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;
    :cond_0
    :goto_0
    return-void

    .line 363
    .restart local v0    # "hrm":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;
    :cond_1
    iget v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRate:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 364
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onReceived - valid data"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mStressSensorListener:Lcom/sec/android/app/shealth/stress/StressSensorListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$500(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/sec/android/app/shealth/stress/StressSensorListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/stress/StressSensorListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;)V

    goto :goto_0
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 374
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDevice.DataListener onReceived2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    return-void
.end method

.method public onStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 348
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    return-void
.end method

.method public onStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 353
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStopped : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    return-void
.end method
