.class public Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;
.super Ljava/lang/Object;
.source "LogGenerator.java"


# static fields
.field public static final ACTIVITY_DATA_SIZE:Ljava/lang/String; = "007"

.field public static final ACTIVITY_USE_APP_FEATURE:Ljava/lang/String; = "002"

.field private static final BODY_SIZE:I = 0x100

.field static final CONTEXT_FRAMEWORK_LOG_TYPE:Ljava/lang/String; = "BIZ"

.field private static final DEFAULT_COUNT:I = 0x1

.field public static final FIRMWARE_VERSION:Ljava/lang/String;

.field private static final MODEL_NAME:Ljava/lang/String;

.field static final SHEALTH_LOG_SERVICE_CODE:Ljava/lang/String; = "706"

.field static final SHEALTH_LOG_VERSION:Ljava/lang/String; = "99.01.00"

.field public static final SURVEY_BODY_TIME_FORMAT:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ssZ"

.field private static final TAG:Ljava/lang/String;

.field private static final UPLOAD_APP_ID_NAME:Ljava/lang/String; = "app_id"

.field private static final UPLOAD_APP_VERSION_NAME:Ljava/lang/String; = "app_ver"

.field private static final UPLOAD_COUNT_NAME:Ljava/lang/String; = "count"

.field private static final UPLOAD_DATE_NAME:Ljava/lang/String; = "date"

.field private static final UPLOAD_DELIMITER_NAME:Ljava/lang/String; = "\u00b6"

.field private static final UPLOAD_EXTRA_NAME:Ljava/lang/String; = "extra"

.field private static final UPLOAD_FEATURE_NAME:Ljava/lang/String; = "feature"

.field private static final UPLOAD_FIRMWARE_VERSION_NAME:Ljava/lang/String; = "fw_ver"

.field private static final UPLOAD_MCC_NAME:Ljava/lang/String; = "mcc"

.field private static final UPLOAD_MNC_NAME:Ljava/lang/String; = "mnc"

.field private static final UPLOAD_MODEL_NAME:Ljava/lang/String; = "model"

.field private static final UPLOAD_RAW_DATA:I = 0x0

.field private static final UPLOAD_SUMMARY_NAME:Ljava/lang/String; = "sum"

.field private static final UPLOAD_USE_APP_FEATURE_SIZE_NAME:Ljava/lang/String; = "size_002"

.field private static final UPLOAD_VALUE_NAME:Ljava/lang/String; = "value"

.field static instance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

.field private static sAppVersion:Ljava/lang/String;

.field private static sMcc:Ljava/lang/String;

.field private static sMnc:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    const-class v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->TAG:Ljava/lang/String;

    .line 56
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    .line 57
    const-string/jumbo v0, "ro.build.PDA"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->FIRMWARE_VERSION:Ljava/lang/String;

    .line 67
    sput-object v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMcc:Ljava/lang/String;

    .line 68
    sput-object v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMnc:Ljava/lang/String;

    .line 69
    sput-object v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sAppVersion:Ljava/lang/String;

    .line 71
    sput-object v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->instance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->TAG:Ljava/lang/String;

    const-string v1, "Construct LogGenerator"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->mContext:Landroid/content/Context;

    .line 76
    return-void
.end method

.method public static getDataSizeBody(II)Ljava/lang/String;
    .locals 8
    .param p0, "daysBefore"    # I
    .param p1, "logSize"    # I

    .prologue
    const/16 v7, 0x3d

    const/4 v6, 0x0

    .line 162
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 163
    .local v3, "surveyBodyTimeFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 164
    .local v1, "dataSizeBodyCal":Ljava/util/GregorianCalendar;
    const/4 v4, 0x5

    neg-int v5, p0

    invoke-virtual {v1, v4, v5}, Ljava/util/GregorianCalendar;->add(II)V

    .line 165
    const/16 v4, 0xb

    invoke-virtual {v1, v4, v6}, Ljava/util/GregorianCalendar;->set(II)V

    .line 166
    const/16 v4, 0xc

    invoke-virtual {v1, v4, v6}, Ljava/util/GregorianCalendar;->set(II)V

    .line 167
    const/16 v4, 0xd

    invoke-virtual {v1, v4, v6}, Ljava/util/GregorianCalendar;->set(II)V

    .line 168
    const/16 v4, 0xe

    invoke-virtual {v1, v4, v6}, Ljava/util/GregorianCalendar;->set(II)V

    .line 169
    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 170
    .local v2, "date":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 172
    .local v0, "body":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v0, v6, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 173
    sget-object v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMcc:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v4, "\u00b6"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMnc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    const-string/jumbo v4, "\u00b6"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "model"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string/jumbo v4, "\u00b6"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "fw_ver"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->FIRMWARE_VERSION:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    const-string/jumbo v4, "\u00b6"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "date"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    const-string/jumbo v4, "\u00b6"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "size_002"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 180
    const-string/jumbo v4, "\u00b6"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sAppVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static getUseAppFeatureBody(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 10
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x3d

    .line 186
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 187
    .local v5, "surveyBodyTimeFormat":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "date":Ljava/lang/String;
    const-string v6, "feature"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 190
    .local v3, "feature":Ljava/lang/String;
    const-string v6, "extra"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, "extra":Ljava/lang/String;
    const-string/jumbo v6, "value"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, "strValue":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v6, 0x100

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 194
    .local v0, "body":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {v0, v9, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 195
    sget-object v6, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMcc:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMnc:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "model"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->MODEL_NAME:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "fw_ver"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->FIRMWARE_VERSION:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "date"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "app_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "feature"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "sum"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 205
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "extra"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 207
    if-eqz v2, :cond_0

    const-string v6, ""

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 208
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :cond_0
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "value"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 212
    if-eqz v4, :cond_1

    const-string v6, ""

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 213
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_1
    const-string/jumbo v6, "\u00b6"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sAppVersion:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static init(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x3

    const/4 v10, 0x0

    .line 83
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->instance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    if-nez v8, :cond_0

    .line 84
    new-instance v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;-><init>(Landroid/content/Context;)V

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->instance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    .line 87
    :cond_0
    const/4 v7, 0x0

    .line 88
    .local v7, "telephonyMgr":Landroid/telephony/TelephonyManager;
    const/4 v5, 0x0

    .line 89
    .local v5, "simOperator":Ljava/lang/String;
    const/4 v6, -0x1

    .line 90
    .local v6, "simState":I
    const/4 v2, 0x0

    .line 91
    .local v2, "mcc":Ljava/lang/String;
    const/4 v3, 0x0

    .line 94
    .local v3, "mnc":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 95
    :try_start_0
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->instance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    iget-object v8, v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->mContext:Landroid/content/Context;

    const-string/jumbo v9, "phone"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/telephony/TelephonyManager;

    move-object v7, v0

    .line 98
    :cond_1
    if-eqz v7, :cond_2

    .line 99
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v5

    .line 100
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimState()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 107
    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v8, v12, :cond_3

    if-eq v6, v12, :cond_4

    .line 108
    :cond_3
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->TAG:Ljava/lang/String;

    const-string v9, "Sim operator is invalid"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const-string/jumbo v8, "mcc="

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMcc:Ljava/lang/String;

    .line 110
    const-string/jumbo v8, "mnc="

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMnc:Ljava/lang/String;

    .line 117
    :goto_0
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MCC: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", MNC: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v4, 0x0

    .line 122
    .local v4, "sHealthAppVersion":Ljava/lang/String;
    :try_start_1
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->instance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    iget-object v8, v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->instance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;

    iget-object v9, v9, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    iget-object v4, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 123
    sget-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SHealth application version: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "app_ver="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sAppVersion:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 130
    .end local v4    # "sHealthAppVersion":Ljava/lang/String;
    :goto_1
    return-void

    .line 102
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 112
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_4
    invoke-virtual {v5, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 114
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "mcc="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMcc:Ljava/lang/String;

    .line 115
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "mnc="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sMnc:Ljava/lang/String;

    goto/16 :goto_0

    .line 125
    .restart local v4    # "sHealthAppVersion":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 126
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "app_ver="

    sput-object v8, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->sAppVersion:Ljava/lang/String;

    .line 127
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method
