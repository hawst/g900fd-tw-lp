.class Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;
.super Ljava/lang/Object;
.source "BaseFoodSearchApi.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->performSubCategorySearch(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$ResultRunnable",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

.field final synthetic val$foodSubCategoryRequest:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;->val$foodSubCategoryRequest:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;->val$foodSubCategoryRequest:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi;->performSubCategorySearchRealization(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodSubCategoryRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic run()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/BaseFoodSearchApi$5;->run()Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategoryResult;

    move-result-object v0

    return-object v0
.end method
