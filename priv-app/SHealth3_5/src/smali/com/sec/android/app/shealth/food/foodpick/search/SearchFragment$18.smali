.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;
.super Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->performAutoCompleteSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

.field final synthetic val$searchString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 953
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;->val$searchString:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;-><init>()V

    return-void
.end method


# virtual methods
.method public doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 957
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2300(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;->val$searchString:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performAutoCompleteSearch(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    return-object v0
.end method

.method public onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;)V
    .locals 1
    .param p1, "result"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    .prologue
    .line 967
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 971
    :goto_0
    return-void

    .line 970
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->showAutoCompletePopup(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$2600(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V

    goto :goto_0
.end method

.method public bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 953
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$18;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/AutoCompleteSearchResult;)V

    return-void
.end method

.method protected withErrorToast()Z
    .locals 1

    .prologue
    .line 962
    const/4 v0, 0x0

    return v0
.end method
