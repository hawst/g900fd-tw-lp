.class public Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    }
.end annotation


# instance fields
.field mBtnBackground:I

.field mContentDesc:I

.field mHoverTextResId:I

.field mISOKCancelType:Z

.field mIconRes:Landroid/graphics/drawable/Drawable;

.field mIconResId:I

.field mTextResId:I

.field mlistener:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mHoverTextResId:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mlistener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(IIILandroid/view/View$OnClickListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconResId:I

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mTextResId:I

    iput p3, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mHoverTextResId:I

    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mlistener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(IIILandroid/view/View$OnClickListener;I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    iput p5, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mBtnBackground:I

    return-void
.end method

.method public constructor <init>(IILandroid/view/View$OnClickListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconResId:I

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mHoverTextResId:I

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mlistener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(IILandroid/view/View$OnClickListener;I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    iput p4, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mBtnBackground:I

    return-void
.end method

.method public constructor <init>(IILandroid/view/View$OnClickListener;Z)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconResId:I

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mHoverTextResId:I

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mlistener:Landroid/view/View$OnClickListener;

    iput-boolean p4, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    return-void
.end method

.method public constructor <init>(ILandroid/view/View$OnClickListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mHoverTextResId:I

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mlistener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public getBtnBackground()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mBtnBackground:I

    return v0
.end method

.method public getClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mlistener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getContentDescID()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mContentDesc:I

    return v0
.end method

.method public getHoverTextID()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mHoverTextResId:I

    return v0
.end method

.method public getIconRes()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconRes:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getIconResID()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mIconResId:I

    return v0
.end method

.method public getTextResID()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mTextResId:I

    return v0
.end method

.method public isOKCancelType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->mISOKCancelType:Z

    return v0
.end method
