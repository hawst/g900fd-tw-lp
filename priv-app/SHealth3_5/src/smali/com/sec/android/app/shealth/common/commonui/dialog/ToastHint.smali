.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;
.super Ljava/lang/Object;
.source "ToastHint.java"


# static fields
.field public static POPUP_SHADOW_SIZE:I

.field private static final TRANSPARENT_COLOR_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;


# instance fields
.field private callerView:Landroid/view/View;

.field private ctx:Landroid/support/v4/app/FragmentActivity;

.field private layout:Landroid/view/View;

.field private popupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

.field private popupWindow:Landroid/widget/PopupWindow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const-string v1, "#00000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->TRANSPARENT_COLOR_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callerView"    # Landroid/view/View;
    .param p3, "textBody"    # Ljava/lang/String;
    .param p4, "popupController"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    .prologue
    const/4 v4, -0x2

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->callerView:Landroid/view/View;

    move-object v2, p1

    .line 35
    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->ctx:Landroid/support/v4/app/FragmentActivity;

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->popup_shadow_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->POPUP_SHADOW_SIZE:I

    .line 40
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 41
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$layout;->popup_hint:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->layout:Landroid/view/View;

    .line 43
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->layout:Landroid/view/View;

    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$id;->text_popup_hint:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 44
    .local v1, "text":Landroid/widget/TextView;
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    new-instance v2, Landroid/widget/PopupWindow;

    invoke-direct {v2, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    .line 46
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    sget-object v3, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->TRANSPARENT_COLOR_DRAWABLE:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 47
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->layout:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 48
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    const v3, 0x1030002

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 49
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 53
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$1;

    invoke-direct {v2, p0, p4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 78
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;

    invoke-direct {v2, p0, p4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$2;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Landroid/support/v4/app/FragmentActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->ctx:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 134
    return-void
.end method

.method public show()V
    .locals 7

    .prologue
    .line 105
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->onShow()V

    .line 106
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 107
    .local v0, "callerViewRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->callerView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->callerView:Landroid/view/View;

    const/16 v4, 0x35

    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->callerView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    iget v6, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    sget v6, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->POPUP_SHADOW_SIZE:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->callerView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 111
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 112
    .local v1, "popUpDismissHandler":Landroid/os/Handler;
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint$3;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)V

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 126
    return-void
.end method
