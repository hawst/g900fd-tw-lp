.class Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;
.super Ljava/lang/Object;
.source "GeneralMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddToMyMealButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V
    .locals 0

    .prologue
    .line 612
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;

    .prologue
    .line 612
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 11
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 615
    sget-object v6, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v6, :cond_0

    .line 616
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>()V

    .line 617
    .local v5, "myMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 618
    .local v4, "mealName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-virtual {v6, v4}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->doesMyMealNameExistInDb(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 619
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    const v8, 0x7f090943

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    .line 632
    .end local v4    # "mealName":Ljava/lang/String;
    .end local v5    # "myMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_0
    :goto_0
    return-void

    .line 621
    .restart local v4    # "mealName":Ljava/lang/String;
    .restart local v5    # "myMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$600(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setName(Ljava/lang/String;)V

    .line 622
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getTotalKcal()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setKcal(F)V

    .line 623
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    invoke-interface {v6, v5}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v1

    .line 624
    .local v1, "mealId":J
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v6}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 625
    .local v3, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setMealId(J)V

    goto :goto_1

    .line 627
    .end local v3    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v6, v6, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v7, v7, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v7}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->insertOrUpdateData(Ljava/util/List;)Ljava/util/List;

    .line 628
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v7, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    const v8, 0x7f09093b

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v4, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    .line 629
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mAddToMyMealDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$800(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    goto :goto_0
.end method
