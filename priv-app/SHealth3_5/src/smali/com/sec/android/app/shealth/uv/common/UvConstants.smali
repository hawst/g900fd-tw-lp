.class public final Lcom/sec/android/app/shealth/uv/common/UvConstants;
.super Ljava/lang/Object;
.source "UvConstants.java"


# static fields
.field public static final ACCESSORY_NAME:Ljava/lang/String; = "custom_name"

.field public static final ACCESSORY_TYPE_SENSOR:Ljava/lang/String; = "10029"

.field public static final ALL:Ljava/lang/String; = "*"

.field public static final AND:Ljava/lang/String; = " AND "

.field public static final ASC:Ljava/lang/String; = " ASC"

.field public static final AVG_MONTH:Ljava/lang/String; = "AvgMonth"

.field public static final AVG_SCORE:Ljava/lang/String; = "AvgScore"

.field public static final AVG_UV:Ljava/lang/String; = "AvgUv"

.field public static final COMMENT_KEY:Ljava/lang/String; = "COMMENT_KEY"

.field public static final DATE_KEY:Ljava/lang/String; = "Date"

.field public static final DATE_TALK:Ljava/lang/String; = "DATE_TALK_KEY"

.field public static final DAYS_IN_MONTH_GRAPH:Ljava/lang/String; = "STRFTIME(\"%d-%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

.field public static final DEFAULT_MEASUREMENT_DURATION:I = 0x7530

.field public static final DEFAULT_OF_VALUE:I = 0x0

.field public static final DESC:Ljava/lang/String; = " DESC"

.field public static final DESC_LIMIT_2:Ljava/lang/String; = " DESC LIMIT 2"

.field public static final EQUAL:Ljava/lang/String; = "="

.field public static final EQUAL_THAN:Ljava/lang/String; = "=?"

.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field public static final FIRST_OF_MONTH:I = 0x1

.field public static final FRI:I = 0x6

.field public static final GRAPH_FONT:Ljava/lang/String; = "font/Roboto-Light.ttf"

.field public static final HANDLER_MESSAGE_MEASURE_ERROR:I = -0x1

.field public static final HANDLER_MESSAGE_MEASURING:I = 0x1

.field public static final HANDLER_MESSAGE_MEASURING_DATA_COLLECTING:I = 0x5

.field public static final HANDLER_MESSAGE_MEASURING_END:I = 0x4

.field public static final HANDLER_MESSAGE_MEASURING_FAIL:I = 0x3

.field public static final HANDLER_MESSAGE_MEASURING_WARNING:I = 0x2

.field public static final HANDLER_MESSAGE_READY:I = 0x0

.field public static final HANDLER_NOT_SHOW_ANIMATION:I = 0x1

.field public static final HANDLER_SHOW_ANIMATION:I = 0x0

.field public static final HR_DEFAULT_INTERVAL:I = -0x7d1

.field public static final HR_ERROR_TIMEOUT:I = -0x6

.field public static final HR_NEXT_STATE_FROM_FAILED:I = -0x3ea

.field public static final HR_NEXT_STATE_FROM_FINISH:I = -0x3e9

.field public static final ID_KEY:Ljava/lang/String; = "HEART_RATE_ID_KEY"

.field public static final INVALID:Ljava/lang/Long;

.field public static final LAST_HOUR_IN_DAY:I = 0x17

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:I = 0x3b

.field public static final LAST_SECOND_IN_MINUTE:I = 0x3b

.field public static final LESS_THAN:Ljava/lang/String; = "<?"

.field public static final MAX_SAMPLE_TIME:Ljava/lang/String; = "MaxSampleTime"

.field public static final MAX_SCORE:Ljava/lang/String; = "MaxScore"

.field public static final MINUTES_IN_DAY_GRAPH:Ljava/lang/String; = "(C.[sample_time]/(1000 * 60 *5))"

.field public static final MIN_SCORE:Ljava/lang/String; = "MinScore"

.field public static final MODE_KEY:Ljava/lang/String; = "MODE_KEY"

.field public static final MON:I = 0x2

.field public static final MONTHS_IN_YEAR_GRAPH:Ljava/lang/String; = "STRFTIME(\"%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

.field public static final MORE_THAN:Ljava/lang/String; = ">?"

.field public static final NO_SENSOR_INDEX:I = -0x2

.field public static final RECOMENDED_TIME_KEY:Ljava/lang/String; = "RECOMENDED_TIME_KEY"

.field public static final RECOMMENDED_DURATION:Ljava/lang/String; = "RECOMMENDEDDURATION"

.field public static final REQUEST_FOR_LOG:I = 0x45b

.field public static final SCORE_KEY:Ljava/lang/String; = "SCORE_KEY"

.field public static final SHOW_INFO_DIALOG:I = 0x1

.field public static final SHOW_SKIN_DIALOG:I = 0x2

.field public static final SKIN_TYPE_CHECKED:Ljava/lang/String; = "skin_type_checked"

.field public static final SPF_CHECKED:Ljava/lang/String; = "spf_checked"

.field public static final SPF_MAX:I = 0x64

.field public static final SPF_MAX_ZEROS:Ljava/lang/String; = "000"

.field public static final SPF_MIN:I = 0x1

.field public static final SPF_VALUE:Ljava/lang/String; = "spf_value"

.field public static final STATE_EXTREME:I = 0x4

.field public static final STATE_HIGH:I = 0x2

.field public static final STATE_INVAILD:I = -0x1

.field public static final STATE_KEY:Ljava/lang/String; = "STATE_KEY"

.field public static final STATE_LOW:I = 0x0

.field public static final STATE_MODERATE:I = 0x1

.field public static final STATE_VERY_HIGH:I = 0x3

.field public static final SUN:I = 0x1

.field public static final SUN_PROTECTION:Ljava/lang/String; = "SUNPROTECTION"

.field public static final SUN_PROTECTION_KEY:Ljava/lang/String; = "SUN_PROTECTION_KEY"

.field public static final THU:I = 0x5

.field public static final TIME_DATE_KEY:Ljava/lang/String; = "TIME_DATE_KEY"

.field public static final TUE:I = 0x3

.field public static final UV_CHARTTAB:Ljava/lang/String; = "UV_CHARTTAB"

.field public static final UV_EFFEET_END:I = 0x4

.field public static final UV_EFFEET_ERROR:I = 0x3

.field public static final UV_EFFEET_MEASURE:I = 0x1

.field public static final UV_EFFEET_START:I = 0x0

.field public static final UV_EFFEET_STOP:I = 0x2

.field public static final UV_LOGGING_ACCESSORIES:Ljava/lang/String; = "UV11"

.field public static final UV_LOGGING_APP_ID:Ljava/lang/String; = "com.sec.android.app.shealth.uv"

.field public static final UV_LOGGING_CHART:Ljava/lang/String; = "UV04"

.field public static final UV_LOGGING_FAIL:Ljava/lang/String; = "UV05"

.field public static final UV_LOGGING_FINISHED:Ljava/lang/String; = "UV02"

.field public static final UV_LOGGING_HELP:Ljava/lang/String; = "UV06"

.field public static final UV_LOGGING_HISTORY:Ljava/lang/String; = "UV03"

.field public static final UV_LOGGING_SHARVIA_1:Ljava/lang/String; = "UV07"

.field public static final UV_LOGGING_SHARVIA_2:Ljava/lang/String; = "UV08"

.field public static final UV_LOGGING_SHARVIA_3:Ljava/lang/String; = "UV09"

.field public static final UV_LOGGING_SHARVIA_4:Ljava/lang/String; = "UV10"

.field public static final UV_LOGGING_START:Ljava/lang/String; = "UV01"

.field public static final WARNING_CHECKED:Ljava/lang/String; = "uv_warning_checked"

.field public static final WED:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 167
    new-instance v0, Ljava/lang/Long;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
