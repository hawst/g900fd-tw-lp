.class public final enum Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;
.super Ljava/lang/Enum;
.source "FavoritesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

.field public static final enum DELETE:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

.field public static final enum NORMAL:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->NORMAL:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->DELETE:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->NORMAL:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->DELETE:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    return-object v0
.end method
