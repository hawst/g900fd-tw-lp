.class public final Lcom/sec/android/app/shealth/framework/repository/common/ContinuaDefines;
.super Ljava/lang/Object;
.source "ContinuaDefines.java"


# static fields
.field public static final ACTIVITY_INTENSITY:I = 0x7f

.field public static final ACTIVITY_TIME:I = 0x7d

.field public static final ACT_AMB:I = 0x3e8

.field public static final ACT_BIKE:I = 0x3f4

.field public static final ACT_HOME:I = 0x3f7

.field public static final ACT_LYING:I = 0x3eb

.field public static final ACT_MONITOR:I = 0x3f1

.field public static final ACT_MOTOR:I = 0x3ea

.field public static final ACT_MULTIPLE:I = 0x3f0

.field public static final ACT_PHYS:I = 0x3ed

.field public static final ACT_REST:I = 0x3e9

.field public static final ACT_ROW:I = 0x3f6

.field public static final ACT_RUN:I = 0x3f3

.field public static final ACT_SKI:I = 0x3f2

.field public static final ACT_SLEEP:I = 0x3ec

.field public static final ACT_STAIR:I = 0x3f5

.field public static final ACT_SUS_PHYS:I = 0x3ee

.field public static final ACT_UNKNOWN:I = 0x3ef

.field public static final ACT_WALK:I = 0x3f9

.field public static final ACT_WORK:I = 0x3f8

.field public static final AGE:I = 0x7e

.field public static final ALT:I = 0x66

.field public static final ALT_GAIN:I = 0x64

.field public static final ALT_LOSS:I = 0x65

.field public static final ANT:I = 0x3

.field public static final ANT_ACTIVE_METABOLIC_RATE:I = 0xa801

.field public static final ANT_BASAL_METABOLIC_RATE:I = 0xa802

.field public static final ANT_BODYFAT_PERCENTAGE:I = 0xa7fe

.field public static final ANT_BODY_WEIGHT:I = 0xa7fc

.field public static final ANT_BODY_WEIGHT_ADV_STATE:I = 0xa7fa

.field public static final ANT_BODY_WEIGHT_FALSE:F = 0.0f

.field public static final ANT_BODY_WEIGHT_IN_PROCESSING_REQUEST:I = 0xa807

.field public static final ANT_BODY_WEIGHT_REQ_CAP_STATE:I = 0xa7fb

.field public static final ANT_BODY_WEIGHT_STATE:I = 0xa7f9

.field public static final ANT_BODY_WEIGHT_STATE_BUSY:F = 5.0f

.field public static final ANT_BODY_WEIGHT_STATE_COMM_ERR:F = 4.0f

.field public static final ANT_BODY_WEIGHT_STATE_COMPUTING:F = 2.0f

.field public static final ANT_BODY_WEIGHT_STATE_TRANS_LOST:F = 3.0f

.field public static final ANT_BODY_WEIGHT_STATE_VALID:F = 1.0f

.field public static final ANT_BODY_WEIGHT_TRUE:F = 1.0f

.field public static final ANT_BONE_MASS:I = 0xa800

.field public static final ANT_COMPUTATION_TIMESTAMP:I = 0xa02d

.field public static final ANT_COMPUTED_HEART_RATE:I = 0xa412

.field public static final ANT_CUMULATIVE_CALORIES:I = 0xa02f

.field public static final ANT_CUMULATIVE_DISTANCE:I = 0xa02b

.field public static final ANT_CUMULATIVE_OPERATING_TIME:I = 0xa416

.field public static final ANT_CUMULATIVE_STRIDES:I = 0xa02c

.field public static final ANT_CURRENT_MESSAGE_COUNT:I = 0xa411

.field public static final ANT_DATA_LATENCY:I = 0xa02e

.field public static final ANT_DEVICE_TYPE_BLOOD_PRESSURE:I = 0x12

.field public static final ANT_DEVICE_TYPE_FITNESS_EQUIPMENT:I = 0x11

.field public static final ANT_DEVICE_TYPE_HEART_RATE:I = 0x78

.field public static final ANT_DEVICE_TYPE_STRIDE_SDM:I = 0x7c

.field public static final ANT_DEVICE_TYPE_WATCH:I = -0x1

.field public static final ANT_DEVICE_TYPE_WEIGHT_SCALE:I = 0x77

.field public static final ANT_FIT_AGE:I = 0xb7a0

.field public static final ANT_FIT_BATTERY_STATUS:I = 0xb7a9

.field public static final ANT_FIT_BATTERY_VOLTAGE:I = 0xb7a8

.field public static final ANT_FIT_BP_DIATOLIC_PRESSURE:I = 0xafca

.field public static final ANT_FIT_BP_HEART_RATE:I = 0xafcf

.field public static final ANT_FIT_BP_HEART_RATE_TYPE:I = 0xafd0

.field public static final ANT_FIT_BP_MAP_3_SAMPLE_MEAN:I = 0xafcc

.field public static final ANT_FIT_BP_MAP_EVENING_VALUES:I = 0xafce

.field public static final ANT_FIT_BP_MAP_MORNING_VALUES:I = 0xafcd

.field public static final ANT_FIT_BP_MEAN_ARTERIAL_PRESSURE:I = 0xafcb

.field public static final ANT_FIT_BP_STATUS:I = 0xafd1

.field public static final ANT_FIT_BP_SYSTOLIC_PRESSURE:I = 0xafc9

.field public static final ANT_FIT_BP_USER_PROFILE_INDEX:I = 0xafd2

.field public static final ANT_FIT_CUM_OPERATING_TIME:I = 0xb7a7

.field public static final ANT_FIT_DEVICE_INDEX:I = 0xb7a3

.field public static final ANT_FIT_DEVICE_TYPE:I = 0xb7a4

.field public static final ANT_FIT_FE_AUTO_POWER_ZERO:I = 0xb3c8

.field public static final ANT_FIT_FE_AUTO_WHEELSIZE:I = 0xb3c4

.field public static final ANT_FIT_FE_AUTO_WHEEL_CAL:I = 0xb3c7

.field public static final ANT_FIT_FE_BIKE_CAD_ANT_ID:I = 0xb3c0

.field public static final ANT_FIT_FE_BIKE_POWER_ANT_ID:I = 0xb3c2

.field public static final ANT_FIT_FE_BIKE_SPDCAD_ANT_ID:I = 0xb3c1

.field public static final ANT_FIT_FE_BIKE_SPD_ANT_ID:I = 0xb3bf

.field public static final ANT_FIT_FE_BIKE_WEIGHT:I = 0xb3c5

.field public static final ANT_FIT_FE_CUSTOM_WHEELSIZE:I = 0xb3c3

.field public static final ANT_FIT_FE_ENABLED:I = 0xb3b2

.field public static final ANT_FIT_FE_HRM_ANT_ID:I = 0xb3b3

.field public static final ANT_FIT_FE_HRM_ANT_ID_TRANS_TYPE:I = 0xb3b5

.field public static final ANT_FIT_FE_ID:I = 0xb3c9

.field public static final ANT_FIT_FE_LOG_HRV:I = 0xb3b4

.field public static final ANT_FIT_FE_MESSAGE_INDEX:I = 0xb3b1

.field public static final ANT_FIT_FE_NAME:I = 0xb3bc

.field public static final ANT_FIT_FE_ODOMETER:I = 0xb3b8

.field public static final ANT_FIT_FE_ODOMETER_ROLLOVER:I = 0xb3bb

.field public static final ANT_FIT_FE_POWER_CAL_FACTOR:I = 0xb3c6

.field public static final ANT_FIT_FE_SDM_ANT_ID:I = 0xb3b6

.field public static final ANT_FIT_FE_SDM_ANT_ID_TRANS_TYPE:I = 0xb3ba

.field public static final ANT_FIT_FE_SDM_CAL_FACTOR:I = 0xb3b7

.field public static final ANT_FIT_FE_SPD_ENABLED:I = 0xb3ca

.field public static final ANT_FIT_FE_SPEED_SOURCE:I = 0xb3b9

.field public static final ANT_FIT_FE_SPORT:I = 0xb3bd

.field public static final ANT_FIT_FE_SUB_SPORT:I = 0xb3be

.field public static final ANT_FIT_FE_UTC_OFFSET:I = 0xb3cb

.field public static final ANT_FIT_FRIENDLY_NAME:I = 0xb79e

.field public static final ANT_FIT_GENDER:I = 0xb79f

.field public static final ANT_FIT_HARDWARE_VERSION:I = 0xb7a6

.field public static final ANT_FIT_HEIGHT:I = 0xb7a1

.field public static final ANT_FIT_MANUFACTURER:I = 0xb79a

.field public static final ANT_FIT_NUMBER:I = 0xb79d

.field public static final ANT_FIT_PRODUCT:I = 0xb79b

.field public static final ANT_FIT_SERIAL_NUMBER:I = 0xb79c

.field public static final ANT_FIT_SOFTWARE_VERSION:I = 0xb7a5

.field public static final ANT_FIT_TRANSFER_ENDED:I = 0xb7aa

.field public static final ANT_FIT_TYPE:I = 0xb799

.field public static final ANT_FIT_WATCH_CYCLES:I = 0xbf72

.field public static final ANT_FIT_WATCH_FAT_CALORIES:I = 0xbf6f

.field public static final ANT_FIT_WATCH_SESSION_AVG_CADENCE:I = 0xbf76

.field public static final ANT_FIT_WATCH_SESSION_AVG_HEART_RATE:I = 0xbf74

.field public static final ANT_FIT_WATCH_SESSION_AVG_SPEED:I = 0xbf70

.field public static final ANT_FIT_WATCH_SESSION_CALORIES:I = 0xbf6e

.field public static final ANT_FIT_WATCH_SESSION_DISTANCE:I = 0xbf6d

.field public static final ANT_FIT_WATCH_SESSION_DURATION:I = 0xbf6c

.field public static final ANT_FIT_WATCH_SESSION_MAX_CADENCE:I = 0xbf77

.field public static final ANT_FIT_WATCH_SESSION_MAX_HEART_RATE:I = 0xbf75

.field public static final ANT_FIT_WATCH_SESSION_MAX_SPEED:I = 0xbf71

.field public static final ANT_FIT_WATCH_SESSION_SPORT:I = 0xbf6a

.field public static final ANT_FIT_WATCH_SESSION_START_TIME:I = 0xbf6b

.field public static final ANT_FIT_WATCH_STRIDES:I = 0xbf73

.field public static final ANT_FIT_WATCH_TIMESTAMP:I = 0xbf69

.field public static final ANT_FIT_WEIGHT:I = 0xb7a2

.field public static final ANT_FIT_WS_ACTIVE_MET:I = 0xbb8a

.field public static final ANT_FIT_WS_BASAL_MET:I = 0xbb88

.field public static final ANT_FIT_WS_BONE_MASS:I = 0xbb86

.field public static final ANT_FIT_WS_METABOLIC_AGE:I = 0xbb8b

.field public static final ANT_FIT_WS_MUSCLE_MASS:I = 0xbb87

.field public static final ANT_FIT_WS_PERCENT_FAT:I = 0xbb83

.field public static final ANT_FIT_WS_PERCENT_HYDRATION:I = 0xbb84

.field public static final ANT_FIT_WS_PHYSIQUE_RATING:I = 0xbb89

.field public static final ANT_FIT_WS_TIMESTAMP:I = 0xbb81

.field public static final ANT_FIT_WS_USER_PROFILE_INDEX:I = 0xbb8d

.field public static final ANT_FIT_WS_VISCERAL_FAT_MASS:I = 0xbb85

.field public static final ANT_FIT_WS_VISCERAL_FAT_RATING:I = 0xbb8c

.field public static final ANT_FIT_WS_WEIGHT:I = 0xbb82

.field public static final ANT_FS_STATUS_FAIL_ALREADY_BUSY_EXTERNAL:F = -20.0f

.field public static final ANT_FS_STATUS_FAIL_AUTHENTICATION_REJECTED:F = -1040.0f

.field public static final ANT_FS_STATUS_FAIL_BAD_PARAMS:F = -50.0f

.field public static final ANT_FS_STATUS_FAIL_DEVICE_COMMUNICATION_FAILURE:F = -40.0f

.field public static final ANT_FS_STATUS_FAIL_DEVICE_TRANSMISSION_LOST:F = -41.0f

.field public static final ANT_FS_STATUS_FAIL_NOT_SUPPORTED:F = -61.0f

.field public static final ANT_FS_STATUS_FAIL_NO_PERMISSION:F = -60.0f

.field public static final ANT_FS_STATUS_FAIL_OTHER:F = -10.0f

.field public static final ANT_FS_STATUS_FAIL_PARTIAL_DOWNLOAD:F = -1030.0f

.field public static final ANT_FS_STATUS_SUCCESS:F = 0.0f

.field public static final ANT_HEART_BEAT_COUNTER:I = 0xa413

.field public static final ANT_HEART_BEAT_EVENT_TIME:I = 0xa414

.field public static final ANT_HISTORY_SUPPORT:I = 0xa805

.field public static final ANT_HYDRATION_PERCENTAGE:I = 0xa7fd

.field public static final ANT_INSTANTANEOUS_CADENCE:I = 0xa02a

.field public static final ANT_INSTANTANEOUS_SPEED:I = 0xa029

.field public static final ANT_MUSCLE_MASS:I = 0xa7ff

.field public static final ANT_PREVIOUS_HEART_BEAT_EVENT_TIME:I = 0xa415

.field public static final ANT_USERPROFILE_EXCHANGE:I = 0xa803

.field public static final ANT_USERPROFILE_ID:I = 0xa806

.field public static final ANT_USERPROFILE_SELECTED:I = 0xa804

.field public static final ASC_TME_DIST:I = 0x68

.field public static final ATTR_TIME_ABS:I = 0x987

.field public static final ATTR_TIME_BATT_REMAIN:I = 0x988

.field public static final ATTR_TIME_END_SEG:I = 0x98a

.field public static final ATTR_TIME_PD_SAMP:I = 0x98d

.field public static final ATTR_TIME_REL:I = 0x98f

.field public static final ATTR_TIME_STAMP_ABS:I = 0x990

.field public static final ATTR_TIME_STAMP_REL:I = 0x991

.field public static final ATTR_TIME_START_SEG:I = 0x992

.field public static final BLUETOOTH:I = 0x1

.field public static final BODY_FAT:I = 0xe14c

.field public static final CAD:I = 0x6f

.field public static final CAL_INGEST:I = 0x78

.field public static final CAL_INGEST_CARB:I = 0x79

.field public static final CONNECTED:I = 0x2

.field public static final CONNECTING:I = 0x1

.field public static final CONNECTION_ERROR:I = 0x66

.field public static final DESC_TIME_DIST:I = 0x69

.field public static final DIM_ANG_DEG:I = 0x2e0

.field public static final DIM_BEATS:I = 0x119a

.field public static final DIM_BEAT_PER_MIN:I = 0xaa0

.field public static final DIM_CENTI_M:I = 0x511

.field public static final DIM_CYCLES:I = 0x11a1

.field public static final DIM_DAY:I = 0x8e0

.field public static final DIM_DEGC:I = 0x17a0

.field public static final DIM_DIMLESS:I = 0x200

.field public static final DIM_FAHR:I = 0x1140

.field public static final DIM_HR:I = 0x8c0

.field public static final DIM_INCH:I = 0x560

.field public static final DIM_KCAL:I = 0x1197

.field public static final DIM_KCAL_PER_DAY:I = 0x119f

.field public static final DIM_KCAL_PER_H:I = 0x119c

.field public static final DIM_KG_PER_M_SQ:I = 0x7a0

.field public static final DIM_KILO_G:I = 0x6c3

.field public static final DIM_KILO_PASCAL:I = 0xf03

.field public static final DIM_LB:I = 0x6e0

.field public static final DIM_METS:I = 0x119b

.field public static final DIM_MILLI_G:I = 0x6d2

.field public static final DIM_MILLI_G_PER_DL:I = 0x852

.field public static final DIM_MILLI_L:I = 0x652

.field public static final DIM_MILLI_MOLE_PER_L:I = 0x1272

.field public static final DIM_MIN:I = 0x8a0

.field public static final DIM_MMHG:I = 0xf20

.field public static final DIM_M_PER_SEC:I = 0x11a2

.field public static final DIM_PERCENT:I = 0x220

.field public static final DIM_RESP_PER_MIN:I = 0xae0

.field public static final DIM_RPM:I = 0x1aa0

.field public static final DIM_SEC:I = 0x1196

.field public static final DIM_STRIDES:I = 0x1195

.field public static final DIM_STRIDES_PER_MIN:I = 0x119e

.field public static final DIM_STROKES_PER_MIN:I = 0x119d

.field public static final DIM_VOLTAGE:I = 0x11a0

.field public static final DIM_X_CAL:I = 0x1a80

.field public static final DIM_X_FOOT:I = 0x540

.field public static final DIM_X_FOOT_PER_MIN:I = 0x1a20

.field public static final DIM_X_G:I = 0x6c0

.field public static final DIM_X_INCH_PER_MIN:I = 0x1a40

.field public static final DIM_X_JOULES:I = 0xf80

.field public static final DIM_X_M:I = 0x500

.field public static final DIM_X_M_PER_MIN:I = 0x19a0

.field public static final DIM_X_STEP:I = 0x1a00

.field public static final DIM_X_STEP_PER_MIN:I = 0x1a60

.field public static final DIM_X_WATT:I = 0xfc0

.field public static final DIM_YR:I = 0x940

.field public static final DISCONNECTED:I = 0x0

.field public static final DISTANCE:I = 0x67

.field public static final ENERGY:I = 0x77

.field public static final ERROR_CONNECTION_ERROR:I = 0x66

.field public static final ERROR_DATA_NOT_FOUND:I = 0x6a

.field public static final ERROR_DEPENDANCY_APK_NOT_INSTALLED:I = 0x6b

.field public static final ERROR_LONT_DATA_FORMAT:I = 0x67

.field public static final ERROR_MESSAGE_FLAG:I = -0x1a

.field public static final ERROR_TIMEOUT:I = 0x65

.field public static final ERROR_UNKNOWN:I = 0x64

.field public static final FIELD_FE_BIKE_INSTANTANEOUS_CADENCE:I = 0xabf7

.field public static final FIELD_FE_BIKE_INSTANTANEOUS_POWER:I = 0xabf8

.field public static final FIELD_FE_CLIMBER_CUMULATIVE_STRIDES:I = 0xabfc

.field public static final FIELD_FE_CLIMBER_INSTANTANEOUS_CADENCE:I = 0xabfd

.field public static final FIELD_FE_CLIMBER_INSTANTANEOUS_POWER:I = 0xabfe

.field public static final FIELD_FE_EP_CUMULATIVE_POS_VERT_DISTANCE:I = 0xabf4

.field public static final FIELD_FE_EP_CUMULATIVE_STRIDES:I = 0xabf5

.field public static final FIELD_FE_EP_INSTANTANEOUS_CADENCE:I = 0xabf3

.field public static final FIELD_FE_EP_INSTANTANEOUS_POWER:I = 0xabf6

.field public static final FIELD_FE_EQUIPMENT_TYPE:I = 0xabee

.field public static final FIELD_FE_GEN_CUMULATIVE_CALORIES:I = 0xabed

.field public static final FIELD_FE_GEN_CUMULATIVE_DISTANCE:I = 0xabe3

.field public static final FIELD_FE_GEN_CYCLE_LENGTH:I = 0xabe8

.field public static final FIELD_FE_GEN_ELAPSED_TIME:I = 0xabe2

.field public static final FIELD_FE_GEN_HR_DATA_SOURCE:I = 0xabe6

.field public static final FIELD_FE_GEN_INCLINE_PERCENTAGE:I = 0xabe9

.field public static final FIELD_FE_GEN_INSTANTANEOUS_CALORICBURN:I = 0xabec

.field public static final FIELD_FE_GEN_INSTANTANEOUS_HEARTRATE:I = 0xabe5

.field public static final FIELD_FE_GEN_INSTANTANEOUS_METS:I = 0xabeb

.field public static final FIELD_FE_GEN_INSTANTANEOUS_SPEED:I = 0xabe4

.field public static final FIELD_FE_GEN_LAP_COUNT:I = 0xabe7

.field public static final FIELD_FE_GEN_RESISTANCE_LEVEL:I = 0xabea

.field public static final FIELD_FE_MSG_COUNT:I = 0xabe1

.field public static final FIELD_FE_ROWER_CUMULATIVE_STROKERS:I = 0xabf9

.field public static final FIELD_FE_ROWER_INSTANTANEOUS_CADENCE:I = 0xabfa

.field public static final FIELD_FE_ROWER_INSTANTANEOUS_POWER:I = 0xabfb

.field public static final FIELD_FE_SKIER_CUMULATIVE_STRIDES:I = 0xabff

.field public static final FIELD_FE_SKIER_INSTANTANEOUS_CADENCE:I = 0xac00

.field public static final FIELD_FE_SKIER_INSTANTANEOUS_POWER:I = 0xac01

.field public static final FIELD_FE_STATE:I = 0xabef

.field public static final FIELD_FE_TM_CUMULATIVE_NEG_VERT_DISTANCE:I = 0xabf1

.field public static final FIELD_FE_TM_CUMULATIVE_POS_VERT_DISTANCE:I = 0xabf2

.field public static final FIELD_FE_TM_INSTANTANEOUS_CADENCE:I = 0xabf0

.field public static final GLU_ARTERIAL_PLASMA:I = 0x71cc

.field public static final GLU_ARTERIAL_WHOLEBLOOD:I = 0x71c8

.field public static final GLU_CAPILLARY_PLASMA:I = 0x71bc

.field public static final GLU_CAPILLARY_WHOLEBLOOD:I = 0x71b8

.field public static final GLU_CONTROL:I = 0x71d0

.field public static final GLU_GEN:I = 0x7114

.field public static final GLU_ISF:I = 0x71d4

.field public static final GLU_VENOUS_PLASMA:I = 0x71c4

.field public static final GLU_VENOUS_WHOLEBLOOD:I = 0x71c0

.field public static final HBA1C:I = 0x71dc

.field public static final HR:I = 0x72

.field public static final HR_MAX_USER:I = 0x71

.field public static final INCLINE:I = 0x70

.field public static final LATITUDE:I = 0x6a

.field public static final LEN_BODY_ACTUAL:I = 0xe144

.field public static final LONGITUDE:I = 0x6b

.field public static final MASS_BODY_ACTUAL:I = 0xe140

.field public static final MAX:I = 0x7d2

.field public static final MEAN_NULL_EXLCUDE:I = 0x7d1

.field public static final MEAN_NULL_INCLUDE:I = 0x7d0

.field public static final MIN:I = 0x7d3

.field public static final OMRON_BMR:I = 0xf001

.field public static final OMRON_BODY_YEAR:I = 0xf003

.field public static final OMRON_SKELETAL_MUSCLE:I = 0xf00a

.field public static final OMRON_VISCERAL_FAT:I = 0xf002

.field public static final POWER:I = 0x73

.field public static final PRESS_BLD_NONINV:I = 0x4a04

.field public static final PRESS_BLD_NONINV_DIA:I = 0x4a06

.field public static final PRESS_BLD_NONINV_MEAN:I = 0x4a07

.field public static final PRESS_BLD_NONINV_SYS:I = 0x4a05

.field public static final PROCESS_REQUEST:I = 0x12c

.field public static final PROFILE_AI_ACTIVITY_HUB:I = 0x1047

.field public static final PROFILE_AI_MED_MINDER:I = 0x1048

.field public static final PROFILE_BP:I = 0x1007

.field public static final PROFILE_GLUCOSE:I = 0x1011

.field public static final PROFILE_HF_CARDIO:I = 0x1029

.field public static final PROFILE_HF_STRENGTH:I = 0x102a

.field public static final PROFILE_PULS_OXIM:I = 0x1004

.field public static final PROFILE_SCALE:I = 0x100f

.field public static final PROFILE_TEMP:I = 0x1008

.field public static final PROGRAM_ID:I = 0x6c

.field public static final PULS_OXIM_PULS_RATE:I = 0x481a

.field public static final PULS_OXIM_SAT_O2:I = 0x4bb8

.field public static final PULS_RATE_NON_INV:I = 0x482a

.field public static final RATIO_MASS_BODY_LEN_SQ:I = 0xe150

.field public static final RESIST:I = 0x74

.field public static final SENSOR_NOT_PAIRED_ERROR:I = 0x68

.field public static final SENSOR_SEARCH_FAILED_ERROR:I = 0x69

.field public static final SESSION:I = 0x7b

.field public static final SLOPES:I = 0x6d

.field public static final SPECIAL_VALUE_INFINITY:I = 0x7fe

.field public static final SPECIAL_VALUE_NAN:I = 0x7ff

.field public static final SPECIAL_VALUE_NEGATIVE_INFINITY:I = 0x802

.field public static final SPECIAL_VALUE_NRES:I = 0x800

.field public static final SPECIAL_VALUE_RESERVED:I = 0x801

.field public static final SPECIAL_VALUE_TYPE_INFINITY:I = 0x3

.field public static final SPECIAL_VALUE_TYPE_NAN:I = 0x1

.field public static final SPECIAL_VALUE_TYPE_NEGATIVE_INFINITY:I = 0x4

.field public static final SPECIAL_VALUE_TYPE_NONE:I = 0x0

.field public static final SPECIAL_VALUE_TYPE_NRES:I = 0x2

.field public static final SPECIAL_VALUE_TYPE_RESERVED:I = 0x5

.field public static final SPECIAL_VALUE_WEIGHT_INFINITY:I = 0x7ffffe

.field public static final SPECIAL_VALUE_WEIGHT_NAN:I = 0x7fffff

.field public static final SPECIAL_VALUE_WEIGHT_NEGATIVE_INFINITY:I = 0x800002

.field public static final SPECIAL_VALUE_WEIGHT_NRES:I = 0x800000

.field public static final SPECIAL_VALUE_WEIGHT_RESERVED:I = 0x800001

.field public static final SPEED:I = 0x6e

.field public static final STRIDE:I = 0x75

.field public static final SUBSESSION:I = 0x7c

.field public static final SUST_PA_THRESHOLD:I = 0x7a

.field public static final TIME_OUT_ERROR:I = 0x65

.field public static final UNKNOWN_ERROR:I = 0x64

.field public static final UNKNOWN_TYPE:I = 0x0

.field public static final UNKNOWN_UNIT:I = 0x0

.field public static final USB:I = 0x2

.field public static final WRONG_DATA_FORMAT:I = 0x67


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
