.class public Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;
.super Ljava/lang/Object;
.source "CignaLanguageUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;,
        Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;


# instance fields
.field private mChangeLanguageListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;",
            ">;"
        }
    .end annotation
.end field

.field private mFilter:Landroid/content/IntentFilter;

.field private mLocaleChangeReceiver:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;-><init>(Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mLocaleChangeReceiver:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;

    .line 33
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mFilter:Landroid/content/IntentFilter;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;

    .line 38
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mInstance:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mInstance:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    .line 28
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mInstance:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    return-object v0
.end method


# virtual methods
.method public registerReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "changeLanguageListener"    # Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->TAG:Ljava/lang/String;

    const-string v1, "CignaTranslateUtils registerReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mLocaleChangeReceiver:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method

.method public unRegisterReceiver(Landroid/content/Context;Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "changeLanguageListener"    # Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->TAG:Ljava/lang/String;

    const-string v1, "CignaTranslateUtils unRegisterReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mLocaleChangeReceiver:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method
