.class public Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;
.super Ljava/lang/Object;
.source "AwardTodayActivity.java"


# instance fields
.field intentAction:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDeviceItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;",
            ">;"
        }
    .end annotation
.end field

.field mPedoCalories:F

.field private mTodayContainerLayout:Landroid/widget/LinearLayout;

.field private mView:Landroid/view/View;

.field monthTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mView"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object v2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    .line 91
    iput-object v2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mView:Landroid/view/View;

    .line 92
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mPedoCalories:F

    .line 93
    const-wide v0, 0x9a7ec800L

    iput-wide v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->monthTime:J

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mTodayContainerLayout:Landroid/widget/LinearLayout;

    .line 135
    new-instance v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$1;-><init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->intentAction:Landroid/view/View$OnClickListener;

    .line 175
    iput-object v2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mDeviceItemList:Ljava/util/ArrayList;

    .line 98
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    .line 99
    iput-object p2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mView:Landroid/view/View;

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->setViewArea()V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->connectedWearableDevice()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private connectedWearableDevice()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 179
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getLoadUserDeviceName()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mDeviceItemList:Ljava/util/ArrayList;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mDeviceItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mDeviceItemList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/utils/Utils$DeviceItem;->deviceName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->showDevicePopup(Ljava/lang/String;)V

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mDeviceItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    const v1, 0x7f090bbf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->showDevicePopup(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAvgExerciseData(Landroid/widget/LinearLayout;I)V
    .locals 21
    .param p1, "item"    # Landroid/widget/LinearLayout;
    .param p2, "zeroResId"    # I

    .prologue
    .line 357
    const/16 v18, 0x0

    .line 358
    .local v18, "valueMeasured":F
    const-wide/16 v16, 0x0

    .line 359
    .local v16, "valueGoal":J
    const/4 v10, 0x0

    .line 360
    .local v10, "cursor":Landroid/database/Cursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 361
    .local v15, "nowTime":Ljava/lang/Long;
    const/4 v1, 0x2

    new-array v13, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v13, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v13, v1

    .line 362
    .local v13, "mSelectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v1

    .line 363
    const/4 v1, 0x1

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v1

    .line 365
    new-instance v14, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-direct {v14, v1}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;-><init>(Landroid/content/Context;)V

    .line 366
    .local v14, "mergeCalrorie":Lcom/sec/android/app/shealth/common/utils/MergeCalorie;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    const-wide/16 v19, 0x1

    add-long v5, v5, v19

    invoke-virtual {v14, v1, v2, v5, v6}, Lcom/sec/android/app/shealth/common/utils/MergeCalorie;->getMergedCalorieGetEarlier(JJ)F

    move-result v18

    .line 368
    invoke-static {}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getInstance()Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    move-result-object v1

    const v2, 0x9c4b

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getProgressGoalValue(I)Lcom/sec/android/app/shealth/data/ProgressGoalData;

    move-result-object v12

    .line 369
    .local v12, "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v16

    .line 370
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mPedoCalories:F

    add-float v18, v18, v1

    .line 373
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 374
    .local v8, "currentTime":J
    const/4 v7, 0x0

    .line 376
    .local v7, "avgBurntCalorie":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT AVG(total_kcal) AS avg_total_kcal FROM (SELECT strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'), SUM(total_calorie) AS total_kcal FROM exercise WHERE start_time >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->monthTime:J

    sub-long v2, v8, v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "start_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " GROUP BY strftime(\"%d-%m-%Y\",(["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "start_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]/1000),\'unixepoch\',\'localtime\'))"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 383
    .local v4, "selectionClause":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 384
    if-eqz v10, :cond_0

    .line 385
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 386
    const-string v1, "avg_total_kcal"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 395
    :cond_0
    :goto_0
    if-eqz v10, :cond_1

    .line 396
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 401
    :cond_1
    :goto_1
    const v1, 0x7f080059

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move/from16 v0, p2

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 404
    const v1, 0x7f080084

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f090924

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v5, v6

    invoke-static {v2, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    const v1, 0x7f08005a

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090906

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0908f9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v5, v6

    invoke-static {v2, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    return-void

    .line 388
    :cond_2
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 392
    :catch_0
    move-exception v11

    .line 393
    .local v11, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 395
    if-eqz v10, :cond_1

    .line 396
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 395
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_3

    .line 396
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private getAvgFoodData(Landroid/widget/LinearLayout;I)V
    .locals 16
    .param p1, "item"    # Landroid/widget/LinearLayout;
    .param p2, "zeroResId"    # I

    .prologue
    .line 314
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 315
    .local v8, "currentTime":J
    const/4 v7, 0x0

    .line 316
    .local v7, "avgIntakeCalorie":I
    const/4 v10, 0x0

    .line 318
    .local v10, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT AVG(total_kcal) AS avg_total_kcal FROM (SELECT strftime(\"%d-%m-%Y\",([sample_time]/1000),\'unixepoch\',\'localtime\'), SUM(total_kilo_calorie) AS total_kcal FROM meal WHERE sample_time BETWEEN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->monthTime:J

    sub-long v2, v8, v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " GROUP BY strftime(\"%d-%m-%Y\",(["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "sample_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]/1000),\'unixepoch\',\'localtime\'))"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 325
    .local v4, "selectionClause":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 326
    if-eqz v10, :cond_0

    .line 327
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 328
    const-string v1, "avg_total_kcal"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 337
    :cond_0
    :goto_0
    if-eqz v10, :cond_1

    .line 338
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 343
    :cond_1
    :goto_1
    invoke-static {}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getInstance()Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    move-result-object v1

    const v2, 0x9c43

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getProgressGoalValue(I)Lcom/sec/android/app/shealth/data/ProgressGoalData;

    move-result-object v12

    .line 344
    .local v12, "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v13

    .line 348
    .local v13, "valueGoal":J
    const v1, 0x7f080059

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move/from16 v0, p2

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 350
    const v1, 0x7f080084

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f090924

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v5, v6

    invoke-static {v2, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    const v1, 0x7f08005a

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090906

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0908f9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v5, v6

    invoke-static {v2, v3, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    const v1, 0x7f080085

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 354
    return-void

    .line 330
    .end local v12    # "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    .end local v13    # "valueGoal":J
    :cond_2
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 334
    :catch_0
    move-exception v11

    .line 335
    .local v11, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 337
    if-eqz v10, :cond_1

    .line 338
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 337
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_3

    .line 338
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private getAvgPedometerData(Landroid/widget/LinearLayout;I)V
    .locals 13
    .param p1, "item"    # Landroid/widget/LinearLayout;
    .param p2, "zeroResId"    # I

    .prologue
    .line 411
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 412
    .local v7, "currentTime":J
    const/4 v6, 0x0

    .line 413
    .local v6, "avgSteps":I
    const/4 v9, 0x0

    .line 415
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT AVG(total_step) AS avg_total_step FROM (SELECT strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'), SUM(total_step) AS total_step FROM walk_info WHERE START_TIME >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->monthTime:J

    sub-long v1, v7, v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND START_TIME < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY strftime(\"%d-%m-%Y\",([start_time]/1000),\'unixepoch\',\'localtime\'))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 422
    .local v3, "selectionClause":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 423
    if-eqz v9, :cond_0

    .line 424
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 425
    const-string v0, "avg_total_step"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 434
    :cond_0
    :goto_0
    if-eqz v9, :cond_1

    .line 435
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 439
    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mPedoCalories:F

    .line 441
    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v11

    .line 443
    .local v11, "pedoGoalValue":I
    const v0, 0x7f080059

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 444
    const v0, 0x7f080084

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f090b68

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v4, v5

    invoke-static {v1, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    const v0, 0x7f08005a

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090906

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090b8d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v4, v5

    invoke-static {v1, v2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 450
    return-void

    .line 427
    .end local v11    # "pedoGoalValue":I
    :cond_2
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 431
    :catch_0
    move-exception v10

    .line 432
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 434
    if-eqz v9, :cond_1

    .line 435
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 434
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_3

    .line 435
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private getAvgWeightData(Landroid/widget/LinearLayout;I)V
    .locals 22
    .param p1, "item"    # Landroid/widget/LinearLayout;
    .param p2, "zeroResId"    # I

    .prologue
    .line 241
    const/16 v18, 0x0

    .line 242
    .local v18, "valueMeasured":Ljava/lang/String;
    const-wide/16 v16, 0x0

    .line 244
    .local v16, "valueGoal":J
    const/4 v4, 0x0

    .line 245
    .local v4, "projection":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 247
    .local v8, "cursor":Landroid/database/Cursor;
    const-string v10, "%.1f"

    .line 249
    .local v10, "floatFormat":Ljava/lang/String;
    new-instance v14, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-direct {v14, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 250
    .local v14, "mWeightDao":Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    new-instance v13, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-direct {v13, v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    .line 253
    .local v13, "mUnitHelper":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v14, v2, v3}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 254
    const v2, 0x7f080084

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-interface {v14, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v7

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v7, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v10, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "sample_time DESC LIMIT 1"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 277
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 278
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 280
    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 281
    .local v15, "unitStr":Ljava/lang/String;
    const-string/jumbo v2, "weight"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v19

    .line 284
    .local v19, "weightValue":F
    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v15}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->getWeightUnitValut(FLjava/lang/String;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v18

    .line 294
    .end local v15    # "unitStr":Ljava/lang/String;
    .end local v19    # "weightValue":F
    :cond_0
    if-eqz v8, :cond_1

    .line 295
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 300
    :cond_1
    :goto_1
    const v2, 0x7f080059

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 304
    const v2, 0x7f08005a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0908ff

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v20, " "

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    const v2, 0x7f080085

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 308
    return-void

    .line 258
    :cond_2
    new-instance v12, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-direct {v12, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 259
    .local v12, "healthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v11

    .line 260
    .local v11, "goalweight":F
    const v2, 0x7f080084

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 291
    .end local v11    # "goalweight":F
    .end local v12    # "healthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :catch_0
    move-exception v9

    .line 292
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294
    if-eqz v8, :cond_1

    .line 295
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 294
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_3

    .line 295
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method private getWeightUnitValut(FLjava/lang/String;)F
    .locals 1
    .param p1, "value"    # F
    .param p2, "unit"    # Ljava/lang/String;

    .prologue
    .line 454
    if-eqz p2, :cond_0

    const-string v0, "lb"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToLb(F)F

    move-result p1

    .line 457
    .end local p1    # "value":F
    :cond_0
    return p1
.end method

.method private setGoalValue(IILandroid/widget/LinearLayout;)V
    .locals 0
    .param p1, "zeroResId"    # I
    .param p2, "pluginSeparator"    # I
    .param p3, "item"    # Landroid/widget/LinearLayout;

    .prologue
    .line 216
    packed-switch p2, :pswitch_data_0

    .line 238
    :goto_0
    return-void

    .line 218
    :pswitch_0
    invoke-direct {p0, p3, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->getAvgPedometerData(Landroid/widget/LinearLayout;I)V

    goto :goto_0

    .line 223
    :pswitch_1
    invoke-direct {p0, p3, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->getAvgExerciseData(Landroid/widget/LinearLayout;I)V

    goto :goto_0

    .line 227
    :pswitch_2
    invoke-direct {p0, p3, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->getAvgFoodData(Landroid/widget/LinearLayout;I)V

    goto :goto_0

    .line 231
    :pswitch_3
    invoke-direct {p0, p3, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->getAvgWeightData(Landroid/widget/LinearLayout;I)V

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setTodayActivity(II)V
    .locals 5
    .param p1, "zeroResId"    # I
    .param p2, "pluginSeparator"    # I

    .prologue
    .line 116
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 117
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 118
    .local v1, "item":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    .line 120
    .local v2, "setGoalLayout":Landroid/widget/LinearLayout;
    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .end local v1    # "item":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 121
    .restart local v1    # "item":Landroid/widget/LinearLayout;
    invoke-direct {p0, p1, p2, v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->setGoalValue(IILandroid/widget/LinearLayout;)V

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mTodayContainerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 127
    if-eqz v1, :cond_0

    .line 128
    const v3, 0x7f080082

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "setGoalLayout":Landroid/widget/LinearLayout;
    check-cast v2, Landroid/widget/LinearLayout;

    .line 129
    .restart local v2    # "setGoalLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v2, p2}, Landroid/widget/LinearLayout;->setId(I)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->intentAction:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    :cond_0
    return-void
.end method

.method private setViewArea()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mView:Landroid/view/View;

    const v1, 0x7f080079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mTodayContainerLayout:Landroid/widget/LinearLayout;

    .line 107
    const v0, 0x7f0205b4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->setTodayActivity(II)V

    .line 108
    const v0, 0x7f0205b2

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->setTodayActivity(II)V

    .line 109
    const v0, 0x7f0205b3

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->setTodayActivity(II)V

    .line 111
    return-void
.end method

.method private showDevicePopup(Ljava/lang/String;)V
    .locals 4
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f030182

    .line 191
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 192
    .local v0, "b":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090901

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 193
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 194
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$2;-><init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 201
    new-instance v1, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity$3;-><init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 212
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardTodayActivity;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, ""

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 213
    return-void
.end method
