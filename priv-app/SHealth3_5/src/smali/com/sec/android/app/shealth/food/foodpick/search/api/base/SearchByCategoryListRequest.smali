.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
.source "SearchByCategoryListRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mSubCategory:Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Landroid/os/Parcel;)V

    .line 50
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->mSubCategory:Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;II)V
    .locals 1
    .param p1, "requestSubCategory"    # Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;
    .param p2, "pageNumber"    # I
    .param p3, "perPageNumber"    # I

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->getSubCategoryName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Ljava/lang/String;II)V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->mSubCategory:Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V
    .locals 1
    .param p1, "requestSubCategory"    # Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;
    .param p2, "foodSearchApi"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->getSubCategoryName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->mSubCategory:Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    .line 46
    return-void
.end method


# virtual methods
.method public getSubCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->mSubCategory:Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 55
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->writeToParcel(Landroid/os/Parcel;I)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->mSubCategory:Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 57
    return-void
.end method
