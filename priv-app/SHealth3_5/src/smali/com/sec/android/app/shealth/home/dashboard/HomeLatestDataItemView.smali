.class public Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
.super Landroid/widget/LinearLayout;
.source "HomeLatestDataItemView.java"


# instance fields
.field private animation:Z

.field private mCircleIcon:Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

.field private mContext:Landroid/content/Context;

.field private mDivider:Landroid/view/View;

.field private mExerciseItemValueLayout:Landroid/widget/LinearLayout;

.field private mHeartRateStateBar:Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

.field private mHomeLatestDataConstants:I

.field private mItemDate:Landroid/widget/TextView;

.field private mItemIcon:Landroid/widget/ImageView;

.field private mItemSecondValue:Landroid/widget/TextView;

.field private mItemTitle:Landroid/widget/TextView;

.field private mItemTitleSecond:Landroid/widget/TextView;

.field private mItemValue:Landroid/widget/TextView;

.field private mItemWearableIcon:Landroid/widget/ImageView;

.field private mSpo2StateBar:Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;

.field private mStressStateBar:Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mContext:Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->init()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mHomeLatestDataConstants"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mContext:Landroid/content/Context;

    .line 44
    iput p2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mHomeLatestDataConstants:I

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->init()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mContext:Landroid/content/Context;

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->init()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mContext:Landroid/content/Context;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->init()V

    .line 58
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030131

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 62
    const v0, 0x7f08007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mDivider:Landroid/view/View;

    .line 63
    const v0, 0x7f080502

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mCircleIcon:Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    .line 65
    const v0, 0x7f080507

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mHeartRateStateBar:Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mHeartRateStateBar:Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 68
    const v0, 0x7f080511

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemWearableIcon:Landroid/widget/ImageView;

    .line 69
    const v0, 0x7f080503

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemIcon:Landroid/widget/ImageView;

    .line 70
    const v0, 0x7f080509

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemValue:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f08050f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemSecondValue:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f080505

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemTitle:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f080506

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemTitleSecond:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f080510

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemDate:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f08050a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mExerciseItemValueLayout:Landroid/widget/LinearLayout;

    .line 78
    const v0, 0x7f080504

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mStressStateBar:Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mStressStateBar:Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 80
    const v0, 0x7f080508

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mSpo2StateBar:Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mSpo2StateBar:Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    return-void
.end method


# virtual methods
.method public convertDptoPx(F)F
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 268
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 269
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 270
    .local v1, "px":F
    return v1
.end method

.method public getCircleIcon()Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mCircleIcon:Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    return-object v0
.end method

.method public getExerciseItemLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mExerciseItemValueLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getHeartRateStateBar()Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mHeartRateStateBar:Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

    return-object v0
.end method

.method public getHomeLatestDataConstants()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mHomeLatestDataConstants:I

    return v0
.end method

.method public getItemDate()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemDate:Landroid/widget/TextView;

    return-object v0
.end method

.method public getItemIcon()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getItemSecondValue()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemSecondValue:Landroid/widget/TextView;

    return-object v0
.end method

.method public getItemTitle()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method public getItemTitleSecond()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemTitleSecond:Landroid/widget/TextView;

    return-object v0
.end method

.method public getItemValue()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemValue:Landroid/widget/TextView;

    return-object v0
.end method

.method public getItemWearableIcon()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemWearableIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getSpo2StateBar()Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mSpo2StateBar:Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;

    return-object v0
.end method

.method public getStressStateBar()Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mStressStateBar:Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;

    return-object v0
.end method

.method public isAnimation()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->animation:Z

    return v0
.end method

.method public setAnmation(Z)V
    .locals 0
    .param p1, "anmation"    # Z

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->animation:Z

    .line 264
    return-void
.end method

.method public setCircleIcon(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 96
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 99
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mCircleIcon:Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setCircleIconCurrentValue(F)V
    .locals 1
    .param p1, "currentValue"    # F

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mCircleIcon:Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setCurrentValue(F)V

    .line 103
    return-void
.end method

.method public setCircleIconGoalValue(F)V
    .locals 1
    .param p1, "goalValue"    # F

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mCircleIcon:Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleProgressView;->setGoalValue(F)V

    .line 107
    return-void
.end method

.method public setDividerVisible(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 86
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mDivider:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setExerciseItemLayout(ILjava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p1, "firstImage"    # I
    .param p2, "firstText"    # Ljava/lang/String;
    .param p3, "secondImage"    # I
    .param p4, "secondText"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mExerciseItemValueLayout:Landroid/widget/LinearLayout;

    const v5, 0x7f08050b

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 169
    .local v0, "im1":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mExerciseItemValueLayout:Landroid/widget/LinearLayout;

    const v5, 0x7f08050d

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 170
    .local v1, "im2":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mExerciseItemValueLayout:Landroid/widget/LinearLayout;

    const v5, 0x7f08050c

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 171
    .local v2, "text1":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mExerciseItemValueLayout:Landroid/widget/LinearLayout;

    const v5, 0x7f08050e

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 172
    .local v3, "text2":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 174
    :cond_0
    if-eqz v1, :cond_1

    .line 175
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 176
    :cond_1
    if-eqz v2, :cond_2

    .line 177
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    :cond_2
    if-eqz v3, :cond_3

    .line 179
    invoke-virtual {v3, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    :cond_3
    return-void
.end method

.method public setHeartRateStateBarVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 235
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mHeartRateStateBar:Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/HomeHeartRateStateBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setHomeLatestDataConstants(I)V
    .locals 0
    .param p1, "homeLatestDataConstants"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mHomeLatestDataConstants:I

    .line 256
    return-void
.end method

.method public setItemDate(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 202
    if-eqz p1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    :cond_0
    return-void
.end method

.method public setItemIcon(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 114
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setItemIconVisible(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 120
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setItemOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 207
    if-nez p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setItemSecondValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 193
    if-eqz p1, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemSecondValue:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    :cond_0
    return-void
.end method

.method public setItemTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 146
    if-eqz p1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    :cond_0
    return-void
.end method

.method public setItemTitleSecond(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 155
    if-eqz p1, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemTitleSecond:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    :cond_0
    return-void
.end method

.method public setItemValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 183
    if-eqz p1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    :cond_0
    return-void
.end method

.method public setItemWearableIcon(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 130
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemWearableIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setItemWearableIconVisible(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 136
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mItemWearableIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSpo2StateBarVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 245
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 248
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mSpo2StateBar:Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/HomeSpO2StateBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setStressChangePolygonImage(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mStressStateBar:Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->changePolygonImage(I)V

    .line 228
    return-void
.end method

.method public setStressStateBarValue(Ljava/lang/Double;)V
    .locals 3
    .param p1, "stressScore"    # Ljava/lang/Double;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mStressStateBar:Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->moveToPolygon(D)V

    .line 218
    return-void
.end method

.method public setStressStateBarVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 221
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;->mStressStateBar:Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/home/widget/HomeStressStateBar;->setVisibility(I)V

    goto :goto_0
.end method
