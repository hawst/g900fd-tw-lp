.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$NEWS_TABLE;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NEWS_TABLE"
.end annotation


# static fields
.field public static final KEY_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final KEY_IMAGE:Ljava/lang/String; = "image"

.field public static final KEY_LINK:Ljava/lang/String; = "link"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_SOURCE:Ljava/lang/String; = "publish_by"

.field public static final KEY_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_TITLE:Ljava/lang/String; = "title"

.field public static final TABLE_NAME:Ljava/lang/String; = "news"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
