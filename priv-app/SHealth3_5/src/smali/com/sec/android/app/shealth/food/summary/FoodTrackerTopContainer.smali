.class public Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;
.super Landroid/widget/LinearLayout;
.source "FoodTrackerTopContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$3;,
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;
    }
.end annotation


# static fields
.field static final SPACE:Ljava/lang/String; = " "


# instance fields
.field private goalTextView:Landroid/widget/TextView;

.field private intakeTextView:Landroid/widget/TextView;

.field private isResumed:Z

.field private mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

.field private mSummaryContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->init()V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->init()V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;)Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->isResumed:Z

    return p1
.end method

.method private animateAppearingView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 257
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 259
    return-void
.end method

.method private animateDisappearingView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 253
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 254
    return-void
.end method

.method private getGoalText(I)Landroid/text/SpannableStringBuilder;
    .locals 10
    .param p1, "goalCalories"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 228
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090924

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "goalCaloriesString":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 232
    .local v3, "goalValue":Ljava/lang/String;
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 234
    .local v1, "goalSpannable":Landroid/text/SpannableString;
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v4, v8, v5, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 235
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v1, v4, v5, v6, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 236
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v4, v5, v6, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 237
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 238
    .local v2, "goalText":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 250
    return-object v2
.end method

.method private init()V
    .locals 3

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 70
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03011e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 71
    const v1, 0x7f0804ba

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    const/high16 v2, -0x3d4c0000    # -90.0f

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setRotation(F)V

    .line 73
    const v1, 0x7f0803f0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->mSummaryContainer:Landroid/widget/LinearLayout;

    .line 74
    return-void
.end method

.method private initGoalCaloriesView(F)V
    .locals 2
    .param p1, "goalCalories"    # F

    .prologue
    .line 184
    const v0, 0x7f0804c0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->goalTextView:Landroid/widget/TextView;

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->goalTextView:Landroid/widget/TextView;

    float-to-int v1, p1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getGoalText(I)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    return-void
.end method

.method private initIntakeCaloriesView(IZ)V
    .locals 3
    .param p1, "intakeCalories"    # I
    .param p2, "isGoalExceeded"    # Z

    .prologue
    .line 151
    const v0, 0x7f0804bf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->intakeTextView:Landroid/widget/TextView;

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->intakeTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    if-eqz p2, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->intakeTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701f5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 179
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->intakeTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private initnutrientsView(FFF)V
    .locals 10
    .param p1, "intakeProtein"    # F
    .param p2, "intakeCarbo"    # F
    .param p3, "intakeFat"    # F

    .prologue
    const v8, 0x7f0900bf

    const/4 v7, 0x0

    .line 191
    const v6, 0x7f0804c1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 193
    .local v2, "mNutrientsContainer":Landroid/widget/LinearLayout;
    const v6, 0x7f0804c5

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 195
    .local v0, "carboValue":Landroid/widget/TextView;
    const v6, 0x7f0804c7

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 197
    .local v4, "proteinValue":Landroid/widget/TextView;
    const v6, 0x7f0804c3

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 199
    .local v1, "fatValue":Landroid/widget/TextView;
    const-string v5, " "

    .line 201
    .local v5, "space":Ljava/lang/String;
    const-string v3, " "

    .line 204
    .local v3, "nutrientsText":Ljava/lang/String;
    cmpl-float v6, p2, v7

    if-gtz v6, :cond_0

    cmpl-float v6, p3, v7

    if-gtz v6, :cond_0

    cmpl-float v6, p1, v7

    if-lez v6, :cond_1

    .line 206
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 208
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0909cb

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090962

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090966

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 223
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->mSummaryContainer:Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->mContext:Landroid/content/Context;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->intakeTextView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->goalTextView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 224
    return-void

    .line 219
    :cond_1
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setPercentValue(FF)V
    .locals 8
    .param p1, "achievedValue"    # F
    .param p2, "goalValue"    # F

    .prologue
    const-wide/16 v6, 0x0

    const/high16 v5, 0x41200000    # 10.0f

    .line 291
    div-float v2, p1, p2

    .line 292
    .local v2, "progress":F
    const/high16 v3, 0x42c80000    # 100.0f

    mul-float v1, v2, v3

    .line 293
    .local v1, "per":F
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    cmpg-float v3, v1, v5

    if-gez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->changeCircleWidth(Z)V

    .line 295
    cmpl-float v3, v1, v5

    if-ltz v3, :cond_1

    .line 298
    move v0, v1

    .line 301
    .local v0, "aniPer":F
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$1;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$1;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;F)V

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 339
    .end local v0    # "aniPer":F
    :goto_1
    return-void

    .line 293
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 322
    :cond_1
    const/4 v1, 0x0

    .line 325
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$2;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;)V

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method


# virtual methods
.method public setCaloriesToDisplay(IFFFF)V
    .locals 8
    .param p1, "intakeCalories"    # I
    .param p2, "goalCalories"    # F
    .param p3, "intakeProtein"    # F
    .param p4, "intakeCarbo"    # F
    .param p5, "intakeFat"    # F

    .prologue
    .line 84
    int-to-float v5, p1

    div-float/2addr v5, p2

    invoke-static {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isCalorieIntakeGoalExceeded(F)Z

    move-result v2

    .line 86
    .local v2, "isGoalExceeded":Z
    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->initIntakeCaloriesView(IZ)V

    .line 88
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->initGoalCaloriesView(F)V

    .line 90
    invoke-direct {p0, p3, p4, p5}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->initnutrientsView(FFF)V

    .line 91
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->isResumed:Z

    .line 93
    if-gez p1, :cond_0

    .line 95
    const/4 p1, 0x0

    .line 97
    const-string v5, "FoodTrackerTopContainer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Negative intake calorie observed : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    const/4 v5, 0x0

    cmpg-float v5, p2, v5

    if-gez v5, :cond_1

    .line 104
    const/high16 p2, 0x3f800000    # 1.0f

    .line 106
    const-string v5, "FoodTrackerTopContainer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Negative goal calorie observed : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_1
    int-to-float v5, p1

    div-float v1, v5, p2

    .line 112
    .local v1, "intakeToGoalCalorieRatio":F
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->getIntakeCaloriesLevel(F)Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    move-result-object v0

    .line 113
    .local v0, "intakeCaloriesLevel":Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;
    const v5, 0x7f0804bc

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 114
    .local v3, "lowCaloriesView":Landroid/view/View;
    const v5, 0x7f0804bd

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 116
    .local v4, "medalView":Landroid/widget/ImageView;
    sget-object v5, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$3;->$SwitchMap$com$sec$android$app$shealth$food$summary$FoodTrackerTopContainer$IntakeCaloriesLevel:[I

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 143
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Wrong intake calories level"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 119
    :pswitch_0
    int-to-float v5, p1

    invoke-direct {p0, v5, p2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->setPercentValue(FF)V

    .line 120
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->animateDisappearingView(Landroid/view/View;)V

    .line 121
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->animateAppearingView(Landroid/view/View;)V

    .line 146
    :goto_0
    return-void

    .line 125
    :pswitch_1
    int-to-float v5, p1

    invoke-direct {p0, v5, p2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->setPercentValue(FF)V

    .line 126
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->animateDisappearingView(Landroid/view/View;)V

    .line 127
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->animateAppearingView(Landroid/view/View;)V

    goto :goto_0

    .line 131
    :pswitch_2
    sget-object v5, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;->GOAL_ACHIEVED:Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer$IntakeCaloriesLevel;

    if-ne v0, v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02068b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    :goto_1
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->animateDisappearingView(Landroid/view/View;)V

    .line 136
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->animateAppearingView(Landroid/view/View;)V

    .line 138
    int-to-float v5, p1

    invoke-direct {p0, v5, p2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->setPercentValue(FF)V

    goto :goto_0

    .line 131
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02068c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto :goto_1

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public setResumed(Z)V
    .locals 0
    .param p1, "isResumed"    # Z

    .prologue
    .line 284
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerTopContainer;->isResumed:Z

    .line 285
    return-void
.end method
