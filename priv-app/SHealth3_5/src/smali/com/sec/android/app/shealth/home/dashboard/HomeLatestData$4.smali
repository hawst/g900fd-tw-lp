.class Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;
.super Ljava/lang/Object;
.source "HomeLatestData.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 386
    const-string v1, "HomeLatestData"

    const-string v2, "########## [StatusActivity] onServiceConnected ##########"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    check-cast p2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;->getService()Landroid/app/Service;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1502(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    new-instance v2, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;-><init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->wwflListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1602(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    .line 405
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->wwflListener:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1600(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$WFLDataReceivedListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->registerListener(Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;)V

    .line 406
    return-void

    .line 389
    :catch_0
    move-exception v0

    .line 399
    .local v0, "e":Ljava/lang/ClassCastException;
    const-string v1, "HomeLatestData"

    const-string v2, "BinderProxy was returned instead of Binder as it tried to bind to a service running in different/old process"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$4;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1502(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 410
    return-void
.end method
