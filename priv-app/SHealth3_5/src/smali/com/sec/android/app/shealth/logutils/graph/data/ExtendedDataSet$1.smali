.class Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;
.super Ljava/lang/Object;
.source "ExtendedDataSet.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;",
        ">;"
    }
.end annotation


# instance fields
.field private index:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 1

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->this$0:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->index:I

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->this$0:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getSeriesCount()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->index:I

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->this$0:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getSeriesCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;
    .locals 2

    .prologue
    .line 125
    iget v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->index:I

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->this$0:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    iget v1, p0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->index:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet$1;->next()Lcom/sec/android/app/shealth/logutils/graph/data/ICommonChartSeries;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 131
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Remove operation is not supported by this iterator"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
