.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$5;
.super Ljava/lang/Object;
.source "WalkingMateActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V
    .locals 0

    .prologue
    .line 721
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 724
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.walkingmate"

    const-string v2, "W020"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mGraphFrag:Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGraphFragmentSIC;->getChartReadyToShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    const-string v0, "WalkingMateActivity"

    const-string v1, "actionBarShareviaButtonBuilder::onClick - getChartReadyToShown is true"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->prepareShareView()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)V

    .line 729
    :cond_0
    return-void
.end method
