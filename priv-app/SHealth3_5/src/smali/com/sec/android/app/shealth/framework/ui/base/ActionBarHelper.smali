.class public Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;
    }
.end annotation


# instance fields
.field private final ACTIONBAR_BUTTON_HEIGHT:I

.field private final ACTIONBAR_BUTTON_PADDING:I

.field private final ACTIONBAR_BUTTON_WIDTH:I

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarAnimator:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;

.field private mActionBarBtnParentLayout:Landroid/widget/FrameLayout;

.field private mActionBarBtnParentLayout2:Landroid/widget/FrameLayout;

.field private mActionBarButtonContainer:Landroid/widget/LinearLayout;

.field private mActionBarButtonContainer2:Landroid/widget/LinearLayout;

.field private mActionBarLayout:Landroid/view/View;

.field private mActionBarListener:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;

.field private mActionBarTitleHolder:Landroid/widget/LinearLayout;

.field private mBackButtonLayout:Landroid/widget/LinearLayout;

.field private mButtonLayout:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCustomView:Landroid/widget/LinearLayout;

.field private mEmptyLayout:Landroid/widget/LinearLayout;

.field mIsDrawMenuOpen:Z

.field private mIsDrwaMenuEnabled:Z

.field private mMainActionBar:Landroid/widget/FrameLayout;

.field private mSpinnerViewHolder:Landroid/widget/LinearLayout;

.field private mTitleID:I

.field private mTitleID2:I

.field private mTitleIcon:Landroid/widget/ImageView;

.field private mTitleSubActionBar:Landroid/widget/LinearLayout;

.field private mTitleText:Landroid/widget/TextView;

.field private mTitleText2:Landroid/widget/TextView;

.field private mTitleTextStr:Ljava/lang/String;

.field private mUpButton:Landroid/widget/ImageButton;

.field private simplePopupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

.field private toast:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID:I

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID2:I

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrawMenuOpen:Z

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrwaMenuEnabled:Z

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->simplePopupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->actionbar_button_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_PADDING:I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->actionbar_button_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_WIDTH:I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->action_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_HEIGHT:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrwaMenuEnabled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarListener:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->toast:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->toast:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->simplePopupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    return-object v0
.end method

.method private getActionBarButton(I)Landroid/widget/Button;
    .locals 10

    const/4 v1, 0x2

    new-array v5, v1, [J

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    aput-wide v2, v5, v1

    const/4 v1, 0x0

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v3, 0x0

    int-to-long v1, p1

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    ushr-long v6, v1, v4

    aget-wide v1, v5, v3

    const-wide/16 v8, 0x0

    cmp-long v4, v1, v8

    if-eqz v4, :cond_1

    const-wide v8, -0x48b968135828abfdL    # -2.0262511075027293E-42

    xor-long/2addr v1, v8

    :cond_1
    const/16 v4, 0x20

    ushr-long/2addr v1, v4

    const/16 v4, 0x20

    shl-long/2addr v1, v4

    xor-long/2addr v1, v6

    const-wide v6, -0x48b968135828abfdL    # -2.0262511075027293E-42

    xor-long/2addr v1, v6

    aput-wide v1, v5, v3

    const/4 v2, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    if-gtz v3, :cond_4

    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v3, v1, [I

    const/4 v1, 0x0

    const/16 v4, -0x6b

    aput v4, v3, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v4, 0x0

    const/16 v7, -0x5b

    aput v7, v1, v4

    const/4 v4, 0x0

    :goto_0
    array-length v7, v1

    if-lt v4, v7, :cond_2

    array-length v1, v3

    new-array v1, v1, [C

    const/4 v4, 0x0

    :goto_1
    array-length v7, v1

    if-lt v4, v7, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    move-object v1, v2

    :goto_2
    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_d

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x26

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x16

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    :try_start_1
    aget v7, v1, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    aget-wide v3, v5, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-eqz v6, :cond_5

    const-wide v6, -0x48b968135828abfdL    # -2.0262511075027293E-42

    xor-long/2addr v3, v6

    :cond_5
    const/16 v6, 0x20

    shl-long/2addr v3, v6

    const/16 v6, 0x20

    shr-long/2addr v3, v6

    long-to-int v3, v3

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_6
    :goto_5
    return-object v1

    :catch_1
    move-exception v2

    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer2:Landroid/widget/LinearLayout;

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_9

    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, 0x6363

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v7, 0x6353

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_6
    array-length v7, v2

    if-lt v4, v7, :cond_7

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_7
    array-length v7, v2

    if-lt v4, v7, :cond_8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6

    :catch_2
    move-exception v2

    goto/16 :goto_2

    :cond_7
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_8
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_9
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-eqz v6, :cond_a

    const-wide v6, -0x48b968135828abfdL    # -2.0262511075027293E-42

    xor-long/2addr v2, v6

    :cond_a
    const/16 v6, 0x20

    shl-long/2addr v2, v6

    const/16 v6, 0x20

    shr-long/2addr v2, v6

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/Button;

    move-object v1, v0
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_5

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_d
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-eqz v4, :cond_e

    const-wide v6, -0x48b968135828abfdL    # -2.0262511075027293E-42

    xor-long/2addr v2, v6

    :cond_e
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_11

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, 0x215b

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, 0x216b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_10
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_11
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v6, 0x0

    cmp-long v4, v1, v6

    if-eqz v4, :cond_12

    const-wide v6, -0x48b968135828abfdL    # -2.0262511075027293E-42

    xor-long/2addr v1, v6

    :cond_12
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_5

    :catch_3
    move-exception v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer2:Landroid/widget/LinearLayout;

    array-length v1, v5

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v5, v1

    long-to-int v1, v1

    if-gtz v1, :cond_15

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x25c8

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x25f8

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_13

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_13
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_14
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_15
    const/4 v1, 0x0

    aget-wide v1, v5, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_16

    const-wide v4, -0x48b968135828abfdL    # -2.0262511075027293E-42

    xor-long/2addr v1, v4

    :cond_16
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    goto/16 :goto_5
.end method

.method private setOnSingleClickListener(Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;Landroid/view/View$OnClickListener;)V
    .locals 1

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$4;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$4;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public varargs addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V
    .locals 8

    const/4 v3, -0x1

    const/4 v7, -0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarTitleHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v0, v1

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setFocusable(Z)V

    aget-object v5, p1, v0

    if-nez v5, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getBtnBackground()I

    move-result v5

    if-lez v5, :cond_a

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getBtnBackground()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setBackgroundResource(I)V

    :goto_2
    add-int/2addr v3, v0

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionButtonIndex(I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconResID()I

    move-result v3

    if-gtz v3, :cond_4

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconRes()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_5

    :cond_4
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_WIDTH:I

    iget v6, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_HEIGHT:I

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconResID()I

    move-result v3

    if-lez v3, :cond_b

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconResID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionIcon(I)V

    :cond_5
    :goto_3
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setOnSingleClickListener(Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;Landroid/view/View$OnClickListener;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    if-lez v3, :cond_6

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->isOKCancelType()Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_HEIGHT:I

    invoke-direct {v3, v7, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setText(I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$style;->actionbar_text_btn:I

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setTextAppearance(Landroid/content/Context;I)V

    iget v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_PADDING:I

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_PADDING:I

    invoke-virtual {v4, v3, v1, v5, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setPadding(IIII)V

    :cond_6
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    if-lez v3, :cond_7

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->isOKCancelType()Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_HEIGHT:I

    invoke-direct {v3, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setText(I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$style;->actionbar_ok_cancel_btn:I

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_7
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v3

    if-lez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_8
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getContentDescID()I

    move-result v3

    if-lez v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getContentDescID()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_9
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v3

    if-lez v3, :cond_2

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    if-gtz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setTag(Ljava/lang/Object;)V

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_1

    :cond_a
    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->action_item_background_selector:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setBackgroundResource(I)V

    goto/16 :goto_2

    :cond_b
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconRes()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionIcon(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3
.end method

.method public varargs addActionButtonToSupportLayout([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, -0x2

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {v2, v0, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout2:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v0, v1

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer2:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setFocusable(Z)V

    aget-object v5, p1, v0

    if-nez v5, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getBtnBackground()I

    move-result v5

    if-lez v5, :cond_9

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getBtnBackground()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setBackgroundResource(I)V

    :goto_2
    add-int/2addr v3, v0

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionButtonIndex(I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer2:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconResID()I

    move-result v3

    if-gtz v3, :cond_4

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconRes()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_5

    :cond_4
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_WIDTH:I

    iget v6, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_HEIGHT:I

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconResID()I

    move-result v3

    if-lez v3, :cond_a

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconResID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionIcon(I)V

    :cond_5
    :goto_3
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setOnSingleClickListener(Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;Landroid/view/View$OnClickListener;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    if-lez v3, :cond_6

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->isOKCancelType()Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_HEIGHT:I

    invoke-direct {v3, v7, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setText(I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$style;->actionbar_text_btn:I

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_6
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    if-lez v3, :cond_7

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->isOKCancelType()Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->ACTIONBAR_BUTTON_HEIGHT:I

    invoke-direct {v3, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setText(I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$style;->actionbar_ok_cancel_btn:I

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout2:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_7
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v3

    if-lez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_8
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v3

    if-lez v3, :cond_2

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getTextResID()I

    move-result v3

    if-gtz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getHoverTextID()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setTag(Ljava/lang/Object;)V

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_1

    :cond_9
    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->action_item_background_selector:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setBackgroundResource(I)V

    goto/16 :goto_2

    :cond_a
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;->getIconRes()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionIcon(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3
.end method

.method public addCustomView(Landroid/view/View;)V
    .locals 41

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v1, 0x1e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, -0xcd

    aput v32, v2, v31

    const/16 v31, -0x66

    aput v31, v2, v30

    const/16 v30, -0x28ce

    aput v30, v2, v29

    const/16 v29, -0x7f

    aput v29, v2, v28

    const/16 v28, -0x4af4

    aput v28, v2, v27

    const/16 v27, -0x26

    aput v27, v2, v26

    const/16 v26, 0x5c4b

    aput v26, v2, v25

    const/16 v25, 0x542f

    aput v25, v2, v24

    const/16 v24, -0x17df

    aput v24, v2, v23

    const/16 v23, -0x55

    aput v23, v2, v22

    const/16 v22, -0x3482

    aput v22, v2, v21

    const/16 v21, -0x51

    aput v21, v2, v20

    const/16 v20, -0x15

    aput v20, v2, v19

    const/16 v19, -0x7fd7

    aput v19, v2, v18

    const/16 v18, -0x46

    aput v18, v2, v17

    const/16 v17, -0x7f

    aput v17, v2, v16

    const/16 v16, -0x60bb

    aput v16, v2, v15

    const/16 v15, -0x11

    aput v15, v2, v14

    const/16 v14, -0x17

    aput v14, v2, v13

    const/16 v13, -0x22

    aput v13, v2, v12

    const/16 v12, 0x2637

    aput v12, v2, v11

    const/16 v11, 0x954

    aput v11, v2, v10

    const/16 v10, -0x5098

    aput v10, v2, v9

    const/16 v9, -0x13

    aput v9, v2, v8

    const/16 v8, -0x73

    aput v8, v2, v7

    const/16 v7, -0x11

    aput v7, v2, v6

    const/16 v6, -0x6af7

    aput v6, v2, v5

    const/16 v5, -0x1f

    aput v5, v2, v4

    const/16 v4, -0x47

    aput v4, v2, v3

    const/16 v3, -0x2087

    aput v3, v2, v1

    const/16 v1, 0x1e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, -0xbc

    aput v33, v1, v32

    const/16 v32, -0x1

    aput v32, v1, v31

    const/16 v31, -0x28a5

    aput v31, v1, v30

    const/16 v30, -0x29

    aput v30, v1, v29

    const/16 v29, -0x4a9f

    aput v29, v1, v28

    const/16 v28, -0x4b

    aput v28, v1, v27

    const/16 v27, 0x5c3f

    aput v27, v1, v26

    const/16 v26, 0x545c

    aput v26, v1, v25

    const/16 v25, -0x17ac

    aput v25, v1, v24

    const/16 v24, -0x18

    aput v24, v1, v23

    const/16 v23, -0x34e6

    aput v23, v1, v22

    const/16 v22, -0x35

    aput v22, v1, v21

    const/16 v21, -0x71

    aput v21, v1, v20

    const/16 v20, -0x7fb8

    aput v20, v1, v19

    const/16 v19, -0x80

    aput v19, v1, v18

    const/16 v18, -0xd

    aput v18, v1, v17

    const/16 v17, -0x60e0

    aput v17, v1, v16

    const/16 v16, -0x61

    aput v16, v1, v15

    const/16 v15, -0x7b

    aput v15, v1, v14

    const/16 v14, -0x45

    aput v14, v1, v13

    const/16 v13, 0x267f

    aput v13, v1, v12

    const/16 v12, 0x926

    aput v12, v1, v11

    const/16 v11, -0x50f7

    aput v11, v1, v10

    const/16 v10, -0x51

    aput v10, v1, v9

    const/16 v9, -0x1d

    aput v9, v1, v8

    const/16 v8, -0x80

    aput v8, v1, v7

    const/16 v7, -0x6aa0

    aput v7, v1, v6

    const/16 v6, -0x6b

    aput v6, v1, v5

    const/16 v5, -0x26

    aput v5, v1, v4

    const/16 v4, -0x20c8

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x24

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, -0x10

    aput v39, v2, v38

    const/16 v38, -0x3e

    aput v38, v2, v37

    const/16 v37, -0x48

    aput v37, v2, v36

    const/16 v36, -0x70

    aput v36, v2, v35

    const/16 v35, 0x5a76

    aput v35, v2, v34

    const/16 v34, 0x752a

    aput v34, v2, v33

    const/16 v33, 0x1b55

    aput v33, v2, v32

    const/16 v32, 0x7e7a

    aput v32, v2, v31

    const/16 v31, 0x725e

    aput v31, v2, v30

    const/16 v30, 0x6d17

    aput v30, v2, v29

    const/16 v29, 0x211b

    aput v29, v2, v28

    const/16 v28, -0x43c0

    aput v28, v2, v27

    const/16 v27, -0x2c

    aput v27, v2, v26

    const/16 v26, -0x7

    aput v26, v2, v25

    const/16 v25, -0x25c7

    aput v25, v2, v24

    const/16 v24, -0x42

    aput v24, v2, v23

    const/16 v23, 0x1739

    aput v23, v2, v22

    const/16 v22, -0x1b8e

    aput v22, v2, v21

    const/16 v21, -0x6a

    aput v21, v2, v20

    const/16 v20, -0x35

    aput v20, v2, v19

    const/16 v19, 0x3701

    aput v19, v2, v18

    const/16 v18, 0xa17

    aput v18, v2, v17

    const/16 v17, -0x4883

    aput v17, v2, v16

    const/16 v16, -0x2e

    aput v16, v2, v15

    const/16 v15, -0x51

    aput v15, v2, v14

    const/16 v14, -0x5e

    aput v14, v2, v13

    const/16 v13, -0x7dd7

    aput v13, v2, v12

    const/16 v12, -0x1a

    aput v12, v2, v11

    const/16 v11, -0x495

    aput v11, v2, v10

    const/16 v10, -0x6e

    aput v10, v2, v9

    const/16 v9, 0x154e

    aput v9, v2, v8

    const/16 v8, -0x6684

    aput v8, v2, v7

    const/4 v7, -0x6

    aput v7, v2, v6

    const/16 v6, -0xc

    aput v6, v2, v5

    const/16 v5, 0x7e3c

    aput v5, v2, v3

    const/16 v3, -0x6bd3

    aput v3, v2, v1

    const/16 v1, 0x24

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, -0x7c

    aput v40, v1, v39

    const/16 v39, -0x54

    aput v39, v1, v38

    const/16 v38, -0x23

    aput v38, v1, v37

    const/16 v37, -0x1e

    aput v37, v1, v36

    const/16 v36, 0x5a17

    aput v36, v1, v35

    const/16 v35, 0x755a

    aput v35, v1, v34

    const/16 v34, 0x1b75

    aput v34, v1, v33

    const/16 v33, 0x7e1b

    aput v33, v1, v32

    const/16 v32, 0x727e

    aput v32, v1, v31

    const/16 v31, 0x6d72

    aput v31, v1, v30

    const/16 v30, 0x216d

    aput v30, v1, v29

    const/16 v29, -0x43df

    aput v29, v1, v28

    const/16 v28, -0x44

    aput v28, v1, v27

    const/16 v27, -0x27

    aput v27, v1, v26

    const/16 v26, -0x25c0

    aput v26, v1, v25

    const/16 v25, -0x26

    aput v25, v1, v24

    const/16 v24, 0x1758

    aput v24, v1, v23

    const/16 v23, -0x1be9

    aput v23, v1, v22

    const/16 v22, -0x1c

    aput v22, v1, v21

    const/16 v21, -0x59

    aput v21, v1, v20

    const/16 v20, 0x3760

    aput v20, v1, v19

    const/16 v19, 0xa37

    aput v19, v1, v18

    const/16 v18, -0x48f6

    aput v18, v1, v17

    const/16 v17, -0x49

    aput v17, v1, v16

    const/16 v16, -0x3a

    aput v16, v1, v15

    const/16 v15, -0x2c

    aput v15, v1, v14

    const/16 v14, -0x7df7

    aput v14, v1, v13

    const/16 v13, -0x7e

    aput v13, v1, v12

    const/16 v12, -0x4f2

    aput v12, v1, v11

    const/4 v11, -0x5

    aput v11, v1, v10

    const/16 v10, 0x1528

    aput v10, v1, v9

    const/16 v9, -0x66eb

    aput v9, v1, v8

    const/16 v8, -0x67

    aput v8, v1, v7

    const/16 v7, -0x6f

    aput v7, v1, v6

    const/16 v6, 0x7e4c

    aput v6, v1, v5

    const/16 v5, -0x6b82

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    return-void

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->custom_view_holder:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_4

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_4
.end method

.method public addSpinnerView(Landroid/widget/Spinner;)V
    .locals 41

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual/range {p1 .. p1}, Landroid/widget/Spinner;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v1, 0x1e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, -0x75

    aput v32, v2, v31

    const/16 v31, -0x57b3

    aput v31, v2, v30

    const/16 v30, -0x3f

    aput v30, v2, v29

    const/16 v29, -0x76b4

    aput v29, v2, v28

    const/16 v28, -0x5

    aput v28, v2, v27

    const/16 v27, -0x3f

    aput v27, v2, v26

    const/16 v26, -0x6ab9

    aput v26, v2, v25

    const/16 v25, -0x5

    aput v25, v2, v24

    const/16 v24, 0x3c6f

    aput v24, v2, v23

    const/16 v23, 0x704c

    aput v23, v2, v22

    const/16 v22, -0x19dd

    aput v22, v2, v21

    const/16 v21, -0x7e

    aput v21, v2, v20

    const/16 v20, 0x4859

    aput v20, v2, v19

    const/16 v19, 0x3629

    aput v19, v2, v18

    const/16 v18, -0x13f4

    aput v18, v2, v17

    const/16 v17, -0x62

    aput v17, v2, v16

    const/16 v16, -0x3b

    aput v16, v2, v15

    const/16 v15, -0x69

    aput v15, v2, v14

    const/16 v14, -0x11

    aput v14, v2, v13

    const/16 v13, -0x4d

    aput v13, v2, v12

    const/16 v12, -0x589e

    aput v12, v2, v11

    const/16 v11, -0x2b

    aput v11, v2, v10

    const/16 v10, -0x64

    aput v10, v2, v9

    const/16 v9, -0x42

    aput v9, v2, v8

    const/16 v8, -0x7eb5

    aput v8, v2, v7

    const/16 v7, -0x12

    aput v7, v2, v6

    const/16 v6, 0x1d6e

    aput v6, v2, v5

    const/16 v5, 0x7469

    aput v5, v2, v4

    const/16 v4, 0x7b17

    aput v4, v2, v3

    const/16 v3, 0x663a

    aput v3, v2, v1

    const/16 v1, 0x1e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, -0x4

    aput v33, v1, v32

    const/16 v32, -0x57d8

    aput v32, v1, v31

    const/16 v31, -0x58

    aput v31, v1, v30

    const/16 v30, -0x76e6

    aput v30, v1, v29

    const/16 v29, -0x77

    aput v29, v1, v28

    const/16 v28, -0x5c

    aput v28, v1, v27

    const/16 v27, -0x6ad7

    aput v27, v1, v26

    const/16 v26, -0x6b

    aput v26, v1, v25

    const/16 v25, 0x3c06

    aput v25, v1, v24

    const/16 v24, 0x703c

    aput v24, v1, v23

    const/16 v23, -0x1990

    aput v23, v1, v22

    const/16 v22, -0x1a

    aput v22, v1, v21

    const/16 v21, 0x483d

    aput v21, v1, v20

    const/16 v20, 0x3648

    aput v20, v1, v19

    const/16 v19, -0x13ca

    aput v19, v1, v18

    const/16 v18, -0x14

    aput v18, v1, v17

    const/16 v17, -0x60

    aput v17, v1, v16

    const/16 v16, -0x19

    aput v16, v1, v15

    const/16 v15, -0x7d

    aput v15, v1, v14

    const/16 v14, -0x2a

    aput v14, v1, v13

    const/16 v13, -0x58d6

    aput v13, v1, v12

    const/16 v12, -0x59

    aput v12, v1, v11

    const/4 v11, -0x3

    aput v11, v1, v10

    const/4 v10, -0x4

    aput v10, v1, v9

    const/16 v9, -0x7edb

    aput v9, v1, v8

    const/16 v8, -0x7f

    aput v8, v1, v7

    const/16 v7, 0x1d07

    aput v7, v1, v6

    const/16 v6, 0x741d

    aput v6, v1, v5

    const/16 v5, 0x7b74

    aput v5, v1, v4

    const/16 v4, 0x667b

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x24

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, -0x5bbb

    aput v39, v2, v38

    const/16 v38, -0x36

    aput v38, v2, v37

    const/16 v37, -0x34

    aput v37, v2, v36

    const/16 v36, -0x3b

    aput v36, v2, v35

    const/16 v35, -0x31e4

    aput v35, v2, v34

    const/16 v34, -0x42

    aput v34, v2, v33

    const/16 v33, -0x16

    aput v33, v2, v32

    const/16 v32, -0x65

    aput v32, v2, v31

    const/16 v31, -0x1c

    aput v31, v2, v30

    const/16 v30, -0x3e

    aput v30, v2, v29

    const/16 v29, -0x59f4

    aput v29, v2, v28

    const/16 v28, -0x39

    aput v28, v2, v27

    const/16 v27, -0x2e9b

    aput v27, v2, v26

    const/16 v26, -0xf

    aput v26, v2, v25

    const/16 v25, 0x5f34

    aput v25, v2, v24

    const/16 v24, 0x593b

    aput v24, v2, v23

    const/16 v23, 0x4838

    aput v23, v2, v22

    const/16 v22, -0x70d3

    aput v22, v2, v21

    const/16 v21, -0x3

    aput v21, v2, v20

    const/16 v20, -0x149e

    aput v20, v2, v19

    const/16 v19, -0x76

    aput v19, v2, v18

    const/16 v18, -0x1785

    aput v18, v2, v17

    const/16 v17, -0x61

    aput v17, v2, v16

    const/16 v16, -0x12db

    aput v16, v2, v15

    const/16 v15, -0x7c

    aput v15, v2, v14

    const/16 v14, -0x72

    aput v14, v2, v13

    const/16 v13, -0x21

    aput v13, v2, v12

    const/16 v12, -0x13

    aput v12, v2, v11

    const/16 v11, -0x27

    aput v11, v2, v10

    const/16 v10, -0x1c

    aput v10, v2, v9

    const/16 v9, -0x22

    aput v9, v2, v8

    const/16 v8, -0x23

    aput v8, v2, v7

    const/16 v7, -0x63

    aput v7, v2, v6

    const/16 v6, -0x10f3

    aput v6, v2, v5

    const/16 v5, -0x61

    aput v5, v2, v3

    const/16 v3, -0x46

    aput v3, v2, v1

    const/16 v1, 0x24

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, -0x5bcf

    aput v40, v1, v39

    const/16 v39, -0x5c

    aput v39, v1, v38

    const/16 v38, -0x57

    aput v38, v1, v37

    const/16 v37, -0x49

    aput v37, v1, v36

    const/16 v36, -0x3183

    aput v36, v1, v35

    const/16 v35, -0x32

    aput v35, v1, v34

    const/16 v34, -0x36

    aput v34, v1, v33

    const/16 v33, -0x6

    aput v33, v1, v32

    const/16 v32, -0x3c

    aput v32, v1, v31

    const/16 v31, -0x59

    aput v31, v1, v30

    const/16 v30, -0x5986

    aput v30, v1, v29

    const/16 v29, -0x5a

    aput v29, v1, v28

    const/16 v28, -0x2ef3

    aput v28, v1, v27

    const/16 v27, -0x2f

    aput v27, v1, v26

    const/16 v26, 0x5f4d

    aput v26, v1, v25

    const/16 v25, 0x595f

    aput v25, v1, v24

    const/16 v24, 0x4859

    aput v24, v1, v23

    const/16 v23, -0x70b8

    aput v23, v1, v22

    const/16 v22, -0x71

    aput v22, v1, v21

    const/16 v21, -0x14f2

    aput v21, v1, v20

    const/16 v20, -0x15

    aput v20, v1, v19

    const/16 v19, -0x17a5

    aput v19, v1, v18

    const/16 v18, -0x18

    aput v18, v1, v17

    const/16 v17, -0x12c0

    aput v17, v1, v16

    const/16 v16, -0x13

    aput v16, v1, v15

    const/4 v15, -0x8

    aput v15, v1, v14

    const/4 v14, -0x1

    aput v14, v1, v13

    const/16 v13, -0x77

    aput v13, v1, v12

    const/16 v12, -0x44

    aput v12, v1, v11

    const/16 v11, -0x73

    aput v11, v1, v10

    const/16 v10, -0x48

    aput v10, v1, v9

    const/16 v9, -0x4c

    aput v9, v1, v8

    const/4 v8, -0x2

    aput v8, v1, v7

    const/16 v7, -0x1098

    aput v7, v1, v6

    const/16 v6, -0x11

    aput v6, v1, v5

    const/16 v5, -0x17

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    return-void

    :catch_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->spinner_view_holder:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mEmptyLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_4

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_4
.end method

.method clearActionBarButton()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public createActionBar()V
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->custom_actionbar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->ly_sub_action_title_holder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->ly_action_bar_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mButtonLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->action_item_background_selector:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->ly_action_btn_holder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->ly_action_btn_holder_canceldone:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer2:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->action_bar_title_icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->actionBarMain:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mMainActionBar:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->action_up_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->action_up_button_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mBackButtonLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->tvActionTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->tvActionTitle2:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->ab_btn_parent:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->actionBarCancelDone:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout2:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->ab_action_title_holder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarTitleHolder:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->empty_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mEmptyLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarAnimator:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public disableActionBarButton(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x4eaad86ba35095b0L    # 9.264004454783655E70

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x21

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x11

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarButton(I)Landroid/widget/Button;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public enableActionBarButton(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x4ba20d3c2b31a152L    # 2.2131319009469372E56

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x7db9

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x7d89

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarButton(I)Landroid/widget/Button;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public getActionBarAnimator()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarAnimator:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarAnimator;

    return-object v0
.end method

.method public getActionBarDrawable()Landroid/graphics/drawable/Drawable;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-array v0, v5, [I

    const v1, 0x10100d4

    aput v1, v0, v4

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x10102ce

    invoke-virtual {v2, v3, v1, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v1, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-object v1
.end method

.method public getActionBarLayout()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    return-object v0
.end method

.method getActionBarText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getActionBarUpButton()Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getBackButtonLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mBackButtonLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getButtonContainer()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getTotalActionBarButtonCount()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    return v0
.end method

.method public hideActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    return-void
.end method

.method public makeAllActionBarButtonsInvisible()V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v10, 0x1

    const-wide v8, 0x1ec9830a79b8b919L    # 2.268270054341169E-160

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v10

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, v3

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v8

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v8

    aput-wide v0, v2, v3

    :goto_0
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v10, [I

    const/16 v0, -0x1d

    aput v0, v1, v3

    new-array v0, v10, [I

    const/16 v2, -0x2d

    aput v2, v0, v3

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_2
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_5

    xor-long/2addr v0, v8

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_e

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v10, [I

    const/16 v0, -0x4d

    aput v0, v1, v3

    new-array v0, v10, [I

    const/16 v2, -0x7d

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    aget-wide v0, v2, v3

    cmp-long v5, v0, v11

    if-eqz v5, :cond_9

    xor-long/2addr v0, v8

    :cond_9
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_a

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_b

    xor-long/2addr v0, v8

    :cond_b
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v4, v2, v1

    long-to-int v1, v4

    if-gtz v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    int-to-long v0, v0

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_d

    xor-long/2addr v0, v8

    :cond_d
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v8

    aput-wide v0, v2, v3

    goto/16 :goto_0

    :cond_e
    return-void
.end method

.method public makeAllActionBarButtonsVisible()V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v10, 0x1

    const-wide v8, 0x1e1a1b8398ef6e66L

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v10

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, v3

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v8

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v8

    aput-wide v0, v2, v3

    :goto_0
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v10, [I

    const/16 v0, 0x709

    aput v0, v1, v3

    new-array v0, v10, [I

    const/16 v2, 0x739

    aput v2, v0, v3

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_2
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_5

    xor-long/2addr v0, v8

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_e

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v10, [I

    const/16 v0, -0x4bb6

    aput v0, v1, v3

    new-array v0, v10, [I

    const/16 v2, -0x4b86

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    aget-wide v0, v2, v3

    cmp-long v5, v0, v11

    if-eqz v5, :cond_9

    xor-long/2addr v0, v8

    :cond_9
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v10}, Landroid/view/View;->setFocusable(Z)V

    :goto_5
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_a

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_b

    xor-long/2addr v0, v8

    :cond_b
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v4, v2, v1

    long-to-int v1, v4

    if-gtz v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    int-to-long v0, v0

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_d

    xor-long/2addr v0, v8

    :cond_d
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v8

    aput-wide v0, v2, v3

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto :goto_5

    :cond_e
    return-void
.end method

.method public onActivityPause()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->toast:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->toast:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->dismiss()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public removeActionButton(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x41e6be32fd396d58L    # -1.470152897946592E-9

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x169e

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x16ae

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    if-ltz v0, :cond_a

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, 0x791c

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, 0x792c

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_9

    xor-long/2addr v0, v9

    :cond_9
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_b

    :cond_a
    :goto_4
    return-void

    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x3f

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0xf

    aput v2, v0, v3

    move v2, v3

    :goto_5
    array-length v5, v0

    if-lt v2, v5, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    :goto_6
    array-length v2, v0

    if-lt v3, v2, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_c
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_d
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_e
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_f

    xor-long/2addr v0, v9

    :cond_f
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_4
.end method

.method public removeAllActionBarButtons()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public removeCustomView()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->custom_view_holder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public removeSpinnerView()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->spinner_view_holder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mEmptyLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setActionBarAppTitle()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :try_start_1
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID2:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID2:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleTextStr:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleTextStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0

    :cond_1
    :try_start_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setActionBarBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setActionBarButtonVisible(ZI)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x6d66c50cb32d11c4L    # 1.004719083364609E219

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p2

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    if-ne p1, v8, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x55

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x65

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusable(Z)V

    :goto_2
    return-void

    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_9

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x5f

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x6f

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_7

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_7
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_a

    xor-long/2addr v0, v9

    :cond_a
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public setActionBarButtonVisiblity(ZI)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x1c064e74550a1169L    # -3.971745577067102E173

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p2

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    if-ne p1, v8, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x5a

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x6a

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusable(Z)V

    :goto_2
    return-void

    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_9

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/4 v0, -0x7

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x37

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_7

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_7
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_a

    xor-long/2addr v0, v9

    :cond_a
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public setActionBarDefaultTitle()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->s_health:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->tvActionTitle2:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->custom_actionbar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public setActionBarListener(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;Z)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarListener:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrwaMenuEnabled:Z

    return-void
.end method

.method public setActionBarTitle(I)V
    .locals 19

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x2310b21927bc5240L    # 8.762470275725362E-140

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x2310b21927bc5240L    # 8.762470275725362E-140

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x29

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x19

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-eqz v4, :cond_5

    const-wide v4, 0x2310b21927bc5240L    # 8.762470275725362E-140

    xor-long/2addr v1, v4

    :cond_5
    const/16 v4, 0x20

    shl-long/2addr v1, v4

    const/16 v4, 0x20

    shr-long/2addr v1, v4

    long-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID:I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x2e

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x1e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0x335f

    aput v17, v2, v16

    const/16 v16, -0x35aa

    aput v16, v2, v15

    const/16 v15, -0x46

    aput v15, v2, v14

    const/16 v14, -0x9

    aput v14, v2, v13

    const/16 v13, -0x66

    aput v13, v2, v12

    const/16 v12, 0x406e

    aput v12, v2, v11

    const/16 v11, -0x74ce

    aput v11, v2, v10

    const/16 v10, -0x16

    aput v10, v2, v9

    const/16 v9, -0x19

    aput v9, v2, v8

    const/16 v8, 0x135

    aput v8, v2, v7

    const/16 v7, -0xa92

    aput v7, v2, v6

    const/16 v6, -0x64

    aput v6, v2, v5

    const/16 v5, -0x47

    aput v5, v2, v4

    const/16 v4, -0x30

    aput v4, v2, v3

    const/16 v3, -0x4fd1

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0x332d

    aput v18, v1, v17

    const/16 v17, -0x35cd

    aput v17, v1, v16

    const/16 v16, -0x36

    aput v16, v1, v15

    const/16 v15, -0x65

    aput v15, v1, v14

    const/4 v14, -0x1

    aput v14, v1, v13

    const/16 v13, 0x4026

    aput v13, v1, v12

    const/16 v12, -0x74c0

    aput v12, v1, v11

    const/16 v11, -0x75

    aput v11, v1, v10

    const/16 v10, -0x5b

    aput v10, v1, v9

    const/16 v9, 0x15b

    aput v9, v1, v8

    const/16 v8, -0xaff

    aput v8, v1, v7

    const/16 v7, -0xb

    aput v7, v1, v6

    const/16 v6, -0x33

    aput v6, v1, v5

    const/16 v5, -0x4d

    aput v5, v1, v4

    const/16 v4, -0x4f92

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x136e

    aput v13, v2, v12

    const/16 v12, -0x3d86

    aput v12, v2, v11

    const/16 v11, -0x1e

    aput v11, v2, v10

    const/16 v10, -0x36

    aput v10, v2, v9

    const/16 v9, 0x13a

    aput v9, v2, v8

    const/16 v8, 0xc6d

    aput v8, v2, v7

    const/16 v7, -0x7a86

    aput v7, v2, v6

    const/16 v6, -0x1c

    aput v6, v2, v5

    const/16 v5, 0xd79

    aput v5, v2, v3

    const/16 v3, -0x2a9c

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x130a

    aput v14, v1, v13

    const/16 v13, -0x3ded

    aput v13, v1, v12

    const/16 v12, -0x3e

    aput v12, v1, v11

    const/16 v11, -0x52

    aput v11, v1, v10

    const/16 v10, 0x153

    aput v10, v1, v9

    const/16 v9, 0xc01

    aput v9, v1, v8

    const/16 v8, -0x7af4

    aput v8, v1, v7

    const/16 v7, -0x7b

    aput v7, v1, v6

    const/16 v6, 0xd17

    aput v6, v1, v5

    const/16 v5, -0x2af3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_6
    :try_start_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_8
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_9

    const-wide v5, 0x2310b21927bc5240L    # 8.762470275725362E-140

    xor-long/2addr v1, v5

    :cond_9
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_8

    :cond_a
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_b
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_c
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_d
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7
.end method

.method public setActionBarTitle(II)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x5ad62b8475c2227L    # -1.689446490583559E281

    const/16 v8, 0x20

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x2

    aput-wide v0, v2, v7

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v8

    ushr-long v4, v0, v8

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-lt v7, v0, :cond_2

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    int-to-long v0, p2

    shl-long v4, v0, v8

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_3

    xor-long/2addr v0, v9

    :cond_3
    shl-long/2addr v0, v8

    ushr-long/2addr v0, v8

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_6

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v7, [I

    const/16 v0, -0x39

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, -0x9

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_7

    xor-long/2addr v0, v9

    :cond_7
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID:I

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v7, [I

    const/16 v0, -0x43

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, -0x73

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_8

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_a
    aget-wide v0, v2, v3

    cmp-long v5, v0, v11

    if-eqz v5, :cond_b

    xor-long/2addr v0, v9

    :cond_b
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gt v0, v7, :cond_16

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v7, [I

    const/16 v0, -0xc

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, -0x3b

    aput v2, v0, v3

    move v2, v3

    :goto_5
    array-length v5, v0

    if-lt v2, v5, :cond_14

    array-length v0, v1

    new-array v0, v0, [C

    :goto_6
    array-length v2, v0

    if-lt v3, v2, :cond_15

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->tvActionTitle2:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gt v0, v7, :cond_e

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v7, [I

    const/16 v0, -0x48

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, -0x77

    aput v2, v0, v3

    move v2, v3

    :goto_8
    array-length v5, v0

    if-lt v2, v5, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    :goto_9
    array-length v2, v0

    if-lt v3, v2, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->custom_actionbar:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarLayout:Landroid/view/View;

    goto :goto_7

    :cond_c
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_d
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_e
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_f

    xor-long/2addr v0, v9

    :cond_f
    shr-long/2addr v0, v8

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID2:I

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gt v0, v7, :cond_12

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v7, [I

    const/16 v0, -0x18da

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, -0x18e9

    aput v2, v0, v3

    move v2, v3

    :goto_a
    array-length v5, v0

    if-lt v2, v5, :cond_10

    array-length v0, v1

    new-array v0, v0, [C

    :goto_b
    array-length v2, v0

    if-lt v3, v2, :cond_11

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_10
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_11
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_12
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_13

    xor-long/2addr v0, v9

    :cond_13
    shr-long/2addr v0, v8

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_c
    return-void

    :cond_14
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_15
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_16
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_17

    xor-long/2addr v0, v9

    :cond_17
    shr-long/2addr v0, v8

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID2:I

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gt v0, v7, :cond_1a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v7, [I

    const/16 v0, -0x3b

    aput v0, v1, v3

    new-array v0, v7, [I

    const/16 v2, -0xc

    aput v2, v0, v3

    move v2, v3

    :goto_d
    array-length v5, v0

    if-lt v2, v5, :cond_18

    array-length v0, v1

    new-array v0, v0, [C

    :goto_e
    array-length v2, v0

    if-lt v3, v2, :cond_19

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_18
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    :cond_19
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    :cond_1a
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_1b

    xor-long/2addr v0, v9

    :cond_1b
    shr-long/2addr v0, v8

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_c

    :catch_2
    move-exception v0

    goto/16 :goto_4

    :catch_3
    move-exception v0

    goto/16 :goto_c
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleTextStr:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->action_bar_text_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleID:I

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setActionBarTitleIconVisibility(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setActionBarTitleTextVisibility(Z)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setActionBarUpButton(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x19b4bab7527c3e96L    # 7.622695299135374E-185

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x658b

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x65bb

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    return-void
.end method

.method public setActionBarUpButtonDescription(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mUpButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setCancelDoneVisibility(Z)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mMainActionBar:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout2:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mMainActionBar:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarBtnParentLayout2:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setContentDescription(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleText:Landroid/widget/TextView;

    invoke-static {v0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCustomViewVisibility(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setDrawMenuStateOpen(Z)V
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarAppTitle()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->showActionBarButton()V

    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrawMenuOpen:Z

    return-void
.end method

.method public setFocusOntheRootView()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setHomeButtonVisible(Z)V
    .locals 2

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mButtonLayout:Landroid/widget/LinearLayout;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMenuDividervisibility(Z)V
    .locals 0

    return-void
.end method

.method public setSpinnerViewEnabled(Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    :goto_1
    return-void

    :cond_0
    const v0, 0x3ecccccd    # 0.4f

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public setSpinnerViewVisibility(Z)V
    .locals 14

    const-wide/16 v12, 0x0

    const-wide v10, 0x3c5fdbaef185b191L    # 6.908132524166953E-18

    const/4 v9, 0x1

    const/16 v8, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [J

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v9

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mSpinnerViewHolder:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_1

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    int-to-long v2, v0

    shl-long/2addr v2, v8

    ushr-long v6, v2, v8

    aget-wide v2, v4, v1

    cmp-long v0, v2, v12

    if-eqz v0, :cond_2

    xor-long/2addr v2, v10

    :cond_2
    ushr-long/2addr v2, v8

    shl-long/2addr v2, v8

    xor-long/2addr v2, v6

    xor-long/2addr v2, v10

    aput-wide v2, v4, v1

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v2, v4, v0

    long-to-int v0, v2

    if-gtz v0, :cond_5

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v2, v9, [I

    const/16 v0, -0x618f

    aput v0, v2, v1

    new-array v0, v9, [I

    const/16 v3, -0x61bf

    aput v3, v0, v1

    move v3, v1

    :goto_1
    array-length v5, v0

    if-lt v3, v5, :cond_3

    array-length v0, v2

    new-array v0, v0, [C

    :goto_2
    array-length v3, v0

    if-lt v1, v3, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3
    aget v5, v0, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget v3, v2, v1

    int-to-char v3, v3

    aput-char v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    aget-wide v0, v4, v1

    cmp-long v2, v0, v12

    if-eqz v2, :cond_6

    xor-long/2addr v0, v10

    :cond_6
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method public setTitleActionBarBackground(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x706307509ef3501dL

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x14cd

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x14fd

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public setTitleActionBarVisibility(Z)V
    .locals 14

    const-wide/16 v12, 0x0

    const-wide v10, -0x5118448a3f09461aL    # -9.773056283479086E-83

    const/4 v9, 0x1

    const/16 v8, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [J

    const-wide/16 v2, 0x1

    aput-wide v2, v4, v9

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v4, v2

    long-to-int v2, v2

    if-gtz v2, :cond_1

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    int-to-long v2, v0

    shl-long/2addr v2, v8

    ushr-long v6, v2, v8

    aget-wide v2, v4, v1

    cmp-long v0, v2, v12

    if-eqz v0, :cond_2

    xor-long/2addr v2, v10

    :cond_2
    ushr-long/2addr v2, v8

    shl-long/2addr v2, v8

    xor-long/2addr v2, v6

    xor-long/2addr v2, v10

    aput-wide v2, v4, v1

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v2, v4, v0

    long-to-int v0, v2

    if-gtz v0, :cond_5

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v2, v9, [I

    const/16 v0, -0x4a

    aput v0, v2, v1

    new-array v0, v9, [I

    const/16 v3, -0x7a

    aput v3, v0, v1

    move v3, v1

    :goto_1
    array-length v5, v0

    if-lt v3, v5, :cond_3

    array-length v0, v2

    new-array v0, v0, [C

    :goto_2
    array-length v3, v0

    if-lt v1, v3, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3
    aget v5, v0, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget v3, v2, v1

    int-to-char v3, v3

    aput-char v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    aget-wide v0, v4, v1

    cmp-long v2, v0, v12

    if-eqz v2, :cond_6

    xor-long/2addr v0, v10

    :cond_6
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mTitleSubActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method public showActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    return-void
.end method

.method showActionBarButton()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mActionBarButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mCustomView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method
