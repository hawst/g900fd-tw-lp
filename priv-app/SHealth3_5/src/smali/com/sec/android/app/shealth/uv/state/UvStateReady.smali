.class public Lcom/sec/android/app/shealth/uv/state/UvStateReady;
.super Lcom/sec/android/app/shealth/uv/state/UvState;
.source "UvStateReady.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;
    }
.end annotation


# static fields
.field private static final CURRENT_STATE_TIMEOUT:J = 0x2710L

.field private static final DELAYTIME_CHECK_SENSOR_DETECT:J = 0x96L

.field private static final DUMMY_READY_TIMEOUT:J = 0x7d0L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isInCurrentState:Z

.field private mCurrentStateTimer:Landroid/os/CountDownTimer;

.field private mDetectDelayTimer:Ljava/util/Timer;

.field private mDummyReadyTimer:Landroid/os/CountDownTimer;

.field private mUpdateViewTask:Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 7
    .param p1, "uvSummaryFragment"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    const-wide/16 v4, 0x3e8

    const/4 v6, 0x0

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/state/UvState;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    .line 48
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;

    const-wide/16 v2, 0x7d0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/state/UvStateReady$1;-><init>(Lcom/sec/android/app/shealth/uv/state/UvStateReady;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mDummyReadyTimer:Landroid/os/CountDownTimer;

    .line 111
    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;

    .line 112
    iput-object v6, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    .line 114
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady$2;

    const-wide/16 v2, 0x2710

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/state/UvStateReady$2;-><init>(Lcom/sec/android/app/shealth/uv/state/UvStateReady;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    .line 23
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/uv/state/UvStateReady;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/state/UvStateReady;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z

    return v0
.end method


# virtual methods
.method public clearForNextState()V
    .locals 2

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;

    const-string v1, "clearForNextState()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mDummyReadyTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mDummyReadyTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mUpdateViewTask:Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady$HRUpdateViewTask;->cancel()Z

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mDetectDelayTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 77
    :cond_3
    return-void
.end method

.method public reStartStates()V
    .locals 2

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "reStartStates()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 29
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->updateViewWithAnimation(Z)V

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsDummyStart:Z

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mIsDummyStart:Z

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mDummyReadyTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 34
    :cond_0
    return-void
.end method

.method public terminateState()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->clearForNextState()V

    .line 140
    return-void
.end method

.method public updateNextState(II)V
    .locals 3
    .param p1, "rate"    # I
    .param p2, "interval"    # I

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UvStateMeasuring  updateNextState rate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->clearForNextState()V

    .line 46
    return-void
.end method

.method public updateViewWithAnimation(Z)V
    .locals 5
    .param p1, "hasAnimation"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 81
    sget-object v3, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "updateView()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->isInCurrentState:Z

    .line 84
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 85
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 86
    if-eqz p1, :cond_0

    :goto_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateReady;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 88
    return-void

    :cond_0
    move v1, v2

    .line 86
    goto :goto_0
.end method
