.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "ViewPager.java"


# instance fields
.field private downTime:J

.field private enabled:Z

.field private eventTime:J

.field private isReady:Z

.field private isSelfTouch:Z

.field private mx:F

.field private my:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->enabled:Z

    .line 16
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 70
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->enabled:Z

    if-eqz v1, :cond_0

    .line 72
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 77
    :goto_0
    return v1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 77
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 20
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->enabled:Z

    if-eqz v1, :cond_0

    .line 21
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 63
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    .line 65
    :cond_0
    :goto_1
    return v4

    .line 23
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->mx:F

    .line 24
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->my:F

    .line 25
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->downTime:J

    .line 26
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->eventTime:J

    .line 27
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isReady:Z

    .line 28
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isSelfTouch:Z

    .line 29
    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 32
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isSelfTouch:Z

    if-eqz v1, :cond_1

    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_1

    .line 35
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isReady:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->mx:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v5

    if-lez v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->mx:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->my:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 36
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isSelfTouch:Z

    .line 37
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isReady:Z

    .line 38
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->downTime:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->eventTime:J

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->mx:F

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->my:F

    move v7, v4

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 39
    .local v8, "down":Landroid/view/MotionEvent;
    invoke-super {p0, v8}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 40
    if-eqz v8, :cond_2

    .line 41
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 43
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_1

    .line 44
    .end local v8    # "down":Landroid/view/MotionEvent;
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isReady:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->my:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v5

    if-lez v1, :cond_4

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->mx:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->my:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 45
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isSelfTouch:Z

    .line 46
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isReady:Z

    .line 47
    invoke-super {p0, v4}, Landroid/support/v4/view/ViewPager;->requestDisallowInterceptTouchEvent(Z)V

    .line 48
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->downTime:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->eventTime:J

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->mx:F

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->my:F

    move v7, v4

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 49
    .restart local v8    # "down":Landroid/view/MotionEvent;
    invoke-super {p0, v8}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 50
    if-eqz v8, :cond_0

    .line 51
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_1

    .line 54
    .end local v8    # "down":Landroid/view/MotionEvent;
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->isReady:Z

    if-eqz v1, :cond_0

    move v4, v0

    .line 55
    goto/16 :goto_1

    .line 60
    :pswitch_2
    invoke-super {p0, v4}, Landroid/support/v4/view/ViewPager;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_0

    .line 21
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setSwipingEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->enabled:Z

    .line 82
    return-void
.end method
