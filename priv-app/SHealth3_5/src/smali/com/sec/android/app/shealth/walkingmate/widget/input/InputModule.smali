.class public abstract Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
.super Landroid/widget/FrameLayout;
.source "InputModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;,
        Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;,
        Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$TYPE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WalkingMateInputModule"


# instance fields
.field LastVaule:I

.field private mBackupString:Ljava/lang/String;

.field private mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

.field private mCurrentScaleValue:I

.field private mCurrentValue:I

.field private mDecButton:Landroid/widget/ImageButton;

.field protected mDummyEdit:Landroid/widget/EditText;

.field private mFilterText:Z

.field private mIncButton:Landroid/widget/ImageButton;

.field private mIncDecTouchListener:Landroid/view/View$OnTouchListener;

.field private mInputFilters:[Landroid/text/InputFilter;

.field private mInputModuleTextWatcher:Landroid/text/TextWatcher;

.field private mIsOccupiedControllerTab:Z

.field private mKeyListener:Landroid/view/View$OnKeyListener;

.field private mMaxInputRange:I

.field private mMaxNormalRange:I

.field private mMaxValueLength:I

.field private mMinInputRange:I

.field private mMinNormalRange:I

.field private mMinValueCallback:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;

.field private mMoveDistance:I

.field private mNeedCursorLastPosition:Z

.field private final mRegularExpression:Landroid/text/InputFilter;

.field private mRootFocusLayout:Landroid/view/ViewGroup;

.field private mSystemNumberSeparator:Ljava/lang/String;

.field private mTextChangedListener:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;

.field private mUnitTextView:Landroid/widget/TextView;

.field protected mValueEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 122
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 61
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIsOccupiedControllerTab:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mFilterText:Z

    .line 369
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->LastVaule:I

    .line 449
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    .line 526
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    .line 662
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 690
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 123
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIsOccupiedControllerTab:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mFilterText:Z

    .line 369
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->LastVaule:I

    .line 449
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    .line 526
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    .line 662
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 690
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIsOccupiedControllerTab:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mFilterText:Z

    .line 369
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->LastVaule:I

    .line 449
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$7;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    .line 526
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$8;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    .line 662
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$9;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    .line 690
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$10;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->initialize(Landroid/content/Context;)V

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIsOccupiedControllerTab:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mBackupString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mBackupString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isNumeric(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mFilterText:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableIncBtn(Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableDecBtn(Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMoveDistance:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxValueLength:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZZ)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mTextChangedListener:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$OnTextChangedListener;

    return-object v0
.end method

.method private initialize(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getRootLayoutId()I

    move-result v1

    invoke-static {p1, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 139
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f080bb3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    .line 140
    const v1, 0x7f080bbe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    .line 141
    const v1, 0x7f080bbc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    .line 142
    const v1, 0x7f080bb2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0702b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 144
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getControllerView(Landroid/view/View;)Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 161
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->initAdditionalViews(Landroid/view/View;)V

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->registerListener()V

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->moveFocusFromValueEditText()V

    .line 164
    return-void
.end method

.method private isDecimalNumber()Z
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxNormalRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinNormalRange:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMoveDistance:I

    rem-int/lit8 v0, v0, 0xa

    if-eqz v0, :cond_1

    .line 410
    :cond_0
    const/4 v0, 0x1

    .line 412
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNumeric(Ljava/lang/String;)Z
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 801
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 805
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 802
    :catch_0
    move-exception v0

    .line 803
    .local v0, "nfe":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private registerListener()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputModuleTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setOnValueChangedListener(Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationView$OnValueChangedListener;)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setOnControllerTapListener(Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationView$OnControllerTapListener;)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncDecTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 354
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setInputFilter()V

    .line 355
    return-void
.end method

.method private setEnableDecBtn(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 271
    if-eqz p1, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 275
    :cond_0
    return-void
.end method

.method private setEnableIncBtn(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 254
    if-eqz p1, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusRightId(I)V

    .line 257
    :cond_0
    return-void
.end method

.method private setInputFilter()V
    .locals 5

    .prologue
    .line 416
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    if-nez v1, :cond_0

    .line 417
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/InputFilter;

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    .line 419
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 420
    .local v0, "maxRangeString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxValueLength:I

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxValueLength:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRegularExpression:Landroid/text/InputFilter;

    aput-object v3, v1, v2

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mInputFilters:[Landroid/text/InputFilter;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 425
    return-void
.end method

.method private setInputType()V
    .locals 2

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$6;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 447
    :goto_0
    return-void

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0
.end method

.method private setNewValue(IZ)V
    .locals 1
    .param p1, "newValue"    # I
    .param p2, "mustShowSparator"    # Z

    .prologue
    .line 372
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZZ)V

    .line 373
    return-void
.end method

.method private setNewValue(IZZ)V
    .locals 7
    .param p1, "newValue"    # I
    .param p2, "mustShowSparator"    # Z
    .param p3, "withSound"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 378
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mNeedCursorLastPosition:Z

    .line 379
    int-to-float v1, p1

    const/high16 v2, 0x41200000    # 10.0f

    div-float v0, v1, v2

    .line 381
    .local v0, "floatNewValue":F
    rem-int/lit8 v1, p1, 0xa

    if-nez v1, :cond_2

    .line 382
    if-eqz p2, :cond_1

    .line 383
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.1f"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 391
    :goto_0
    if-eqz p3, :cond_3

    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    if-nez v1, :cond_3

    .line 392
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->LastVaule:I

    if-eq v1, p1, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->LastVaule:I

    if-eqz v1, :cond_0

    .line 393
    sget v1, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_GRADUATION_CONTROL:I

    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->playSound(I)V

    .line 394
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->LastVaule:I

    .line 399
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090243

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090244

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 402
    return-void

    .line 385
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    float-to-int v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 388
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.1f"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 397
    :cond_3
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->LastVaule:I

    goto :goto_1
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 866
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 867
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 868
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    .line 869
    .local v1, "isActive":Z
    if-eqz v1, :cond_0

    .line 870
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->hideEdittextBackground()V

    .line 871
    const/4 v2, 0x1

    .line 874
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .end local v1    # "isActive":Z
    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method protected abstract getControllerView(Landroid/view/View;)Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;
.end method

.method protected abstract getRootLayoutId()I
.end method

.method public getValue()F
    .locals 2

    .prologue
    .line 223
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public hideDecBtn(Z)V
    .locals 2
    .param p1, "isHide"    # Z

    .prologue
    .line 278
    if-eqz p1, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 283
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mDecButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public hideEdittextBackground()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 824
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->hasFocus()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 825
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 827
    .local v2, "mTempString":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 828
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_0
    invoke-direct {p0, v5, v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V

    .line 862
    .end local v2    # "mTempString":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .restart local v2    # "mTempString":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 828
    goto :goto_0

    .line 831
    :cond_2
    const-string v5, ","

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 833
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v6, "."

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 836
    :cond_3
    const/4 v1, 0x0

    .line 839
    .local v1, "mTempFloat":F
    :try_start_0
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, 0x41200000    # 10.0f

    mul-float v1, v5, v6

    .line 841
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I

    int-to-float v5, v5

    cmpg-float v5, v1, v5

    if-gez v5, :cond_4

    .line 843
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I

    int-to-float v1, v5

    .line 845
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinValueCallback:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;

    if-nez v5, :cond_6

    .line 846
    float-to-int v5, v1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z

    move-result v6

    if-eqz v6, :cond_5

    :goto_2
    invoke-direct {p0, v5, v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 859
    :cond_4
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->moveFocusFromValueEditText()V

    goto :goto_1

    :cond_5
    move v3, v4

    .line 846
    goto :goto_2

    .line 848
    :cond_6
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinValueCallback:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;->onMinValueReceived()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 854
    :catch_0
    move-exception v0

    .line 856
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-class v3, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Input format is incorrect! Not a double"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public hideIncBtn(Z)V
    .locals 2
    .param p1, "isHide"    # Z

    .prologue
    .line 260
    if-eqz p1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 265
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIncButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public hideKeyboard()V
    .locals 3

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 656
    return-void
.end method

.method public hideUnit(Z)V
    .locals 2
    .param p1, "isHide"    # Z

    .prologue
    .line 181
    if-eqz p1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected abstract initAdditionalViews(Landroid/view/View;)V
.end method

.method public isValueEmpty()Z
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected moveFocusFromValueEditText()V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->hideKeyboard()V

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 367
    :cond_0
    return-void
.end method

.method protected onTouchModeEditTextColor(Z)V
    .locals 4
    .param p1, "isTaped"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 286
    if-eqz p1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 288
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIsOccupiedControllerTab:Z

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const v1, -0xc25400

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 298
    :goto_0
    return-void

    .line 293
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mIsOccupiedControllerTab:Z

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    goto :goto_0
.end method

.method public setEditTextSize(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 169
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/16 v2, 0x10b

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setMaxWidth(I)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 171
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    .line 172
    .local v0, "filterArray":[Landroid/text/InputFilter;
    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v2, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 174
    return-void
.end method

.method public setEnabled(Z)V
    .locals 4
    .param p1, "isEnable"    # Z

    .prologue
    .line 231
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 232
    move v1, p1

    .local v1, "enableIncBtn":Z
    move v0, p1

    .line 234
    .local v0, "enableDecBtn":Z
    if-eqz p1, :cond_0

    .line 235
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I

    if-lt v2, v3, :cond_1

    .line 236
    const/4 v1, 0x0

    .line 242
    :cond_0
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableIncBtn(Z)V

    .line 243
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setEnableDecBtn(Z)V

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 247
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setEnabled(Z)V

    .line 249
    return-void

    .line 237
    :cond_1
    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I

    if-gt v2, v3, :cond_0

    .line 238
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFilterText(Z)V
    .locals 0
    .param p1, "filterText"    # Z

    .prologue
    .line 523
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mFilterText:Z

    .line 524
    return-void
.end method

.method public setInputRange(FF)V
    .locals 2
    .param p1, "minRange"    # F
    .param p2, "maxRange"    # F

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    .line 189
    mul-float v0, p1, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinInputRange:I

    .line 190
    mul-float v0, p2, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxInputRange:I

    .line 192
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setInputFilter()V

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setInputType()V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setInputRange(FF)V

    .line 195
    return-void
.end method

.method public setMoveDistance(F)V
    .locals 1
    .param p1, "distance"    # F

    .prologue
    .line 206
    const/high16 v0, 0x41200000    # 10.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMoveDistance:I

    .line 208
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setInputType()V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setInterval(F)V

    .line 210
    return-void
.end method

.method public setNormalRange(FF)V
    .locals 2
    .param p1, "minNormalRange"    # F
    .param p2, "maxNormalRange"    # F

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    .line 198
    mul-float v0, p1, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinNormalRange:I

    .line 199
    mul-float v0, p2, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMaxNormalRange:I

    .line 201
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setInputType()V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mController:Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ControllerView;->setNormalRange(FF)V

    .line 203
    return-void
.end method

.method public setUnit(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "unit"    # Ljava/lang/CharSequence;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mUnitTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    return-void
.end method

.method public setValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 213
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setValue(FZ)V

    .line 214
    return-void
.end method

.method public setValue(FZ)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "withSound"    # Z

    .prologue
    .line 217
    const/high16 v0, 0x41200000    # 10.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    .line 218
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentScaleValue:I

    .line 219
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mCurrentValue:I

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->isDecimalNumber()Z

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->setNewValue(IZZ)V

    .line 220
    return-void
.end method

.method public setmMinValueCallback(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;

    .prologue
    .line 819
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mMinValueCallback:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;

    .line 820
    return-void
.end method

.method public showKeyboard()V
    .locals 3

    .prologue
    .line 659
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mValueEditText:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 660
    return-void
.end method
