.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;
.source "ExerciseProMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExerciseImageScaleCopyTask"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private exerciseRowId:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "inputImagePath"    # Ljava/lang/String;
    .param p4, "outputImagePath"    # Ljava/lang/String;
    .param p5, "exerciseRowId"    # J

    .prologue
    .line 1597
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .line 1598
    invoke-direct {p0, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->context:Landroid/content/Context;

    .line 1600
    iput-wide p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->exerciseRowId:J

    .line 1601
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "isSuccessful"    # Ljava/lang/Boolean;

    .prologue
    .line 1605
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ImageScaleCopyAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1606
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1608
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3700()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onPostExecute() outputImagePath="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", exerciseRowId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->exerciseRowId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1610
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->exerciseRowId:J

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Ljava/lang/String;J)V

    .line 1611
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->refreshLastTakenPhoto(Landroid/content/Context;)V

    .line 1613
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1593
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
