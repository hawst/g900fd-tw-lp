.class public Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "HeartrateAddTagActivity.java"

# interfaces
.implements Landroid/text/method/KeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mGridListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mIsEditMode:Z

.field private mLastTagIconId:I

.field private mLastTagName:Ljava/lang/String;

.field private mLayout:Landroid/widget/LinearLayout;

.field private mSelectedIconPos:I

.field private mSelectedTagIcon:Landroid/widget/ImageView;

.field private mTagGridView:Landroid/widget/GridView;

.field private mTagIconId:I

.field private max_character:I

.field private oldText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x55f0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    .line 69
    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    .line 70
    const/16 v0, 0x15

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->max_character:I

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mIsEditMode:Z

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    .line 75
    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagIconId:I

    .line 77
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedIconPos:I

    .line 302
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mGridListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 357
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;
    .param p1, "x1"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getmSelectedIconPos()I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;
    .param p1, "x1"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->setmSelectedIconPos(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$2000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->max_character:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->oldText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    return-object v0
.end method

.method private getmSelectedIconPos()I
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedIconPos:I

    return v0
.end method

.method private intitializeView()V
    .locals 8

    .prologue
    const v7, 0x7f0204cb

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 97
    const v2, 0x7f080634

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLayout:Landroid/widget/LinearLayout;

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    const v2, 0x7f080639

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagGridView:Landroid/widget/GridView;

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v5}, Landroid/widget/GridView;->setVisibility(I)V

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagGridView:Landroid/widget/GridView;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;

    invoke-direct {v3, p0, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$TagIconAdapter;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagGridView:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mGridListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 103
    const v2, 0x7f080636

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconResourceId(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 107
    const v2, 0x7f080637

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    .line 108
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->setmSelectedIconPos(I)V

    .line 109
    const v2, 0x7f080633

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    const/16 v3, 0x19

    const/16 v4, 0xa

    invoke-virtual {v2, v3, v5, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setPadding(IIII)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setGravity(I)V

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090f2a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setHint(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setImeOptions(I)V

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    const-string v3, "disableEmoticonInput=true"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setHorizontallyScrolling(Z)V

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$1;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$2;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$3;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSingleLine()V

    .line 181
    new-array v0, v6, [Landroid/text/InputFilter;

    .line 182
    .local v0, "fArray":[Landroid/text/InputFilter;
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v5

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setFilters([Landroid/text/InputFilter;)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 186
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    const-string v2, "TAG_TEXT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "TAG_INDEX"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188
    const-string v2, "TAG_TEXT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "TAG_TEXT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    .line 189
    const-string v2, "TAG_INDEX"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "TAG_INDEX"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagIconId:I

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagIconId:I

    if-eqz v2, :cond_0

    .line 191
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mIsEditMode:Z

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->requestFocus()Z

    .line 194
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagIconId:I

    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconResourceId(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 197
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagIconId:I

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    .line 200
    :cond_0
    return-void

    .line 188
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 189
    :cond_2
    const-string v2, "0"

    goto :goto_1
.end method

.method private setmSelectedIconPos(I)V
    .locals 0
    .param p1, "mSelectedIconPos"    # I

    .prologue
    .line 454
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedIconPos:I

    .line 455
    return-void
.end method


# virtual methods
.method public clearMetaKeyState(Landroid/view/View;Landroid/text/Editable;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/text/Editable;
    .param p3, "arg2"    # I

    .prologue
    .line 330
    return-void
.end method

.method protected getContentView()Landroid/view/View;
    .locals 4

    .prologue
    .line 220
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030140

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 221
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method

.method public getInputType()I
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    return v0
.end method

.method protected isInputChanged()Z
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagIconId:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeDiscardDialog(II)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 3
    .param p1, "title"    # I
    .param p2, "message"    # I

    .prologue
    .line 204
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$4;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 216
    .local v0, "dialog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    return-object v1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 83
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    const v0, 0x7f0803e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 85
    const v0, 0x7f080632

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoHeader:Landroid/widget/TextView;

    const v1, 0x7f090c0b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    const-string v0, "Add"

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMode:Ljava/lang/String;

    .line 89
    const v0, 0x7f080638

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 90
    const v0, 0x7f080546

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 91
    const v0, 0x7f0803e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->intitializeView()V

    .line 93
    return-void
.end method

.method public onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/text/Editable;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # Landroid/view/KeyEvent;

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyOther(Landroid/view/View;Landroid/text/Editable;Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/text/Editable;
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 348
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/text/Editable;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # Landroid/view/KeyEvent;

    .prologue
    .line 354
    const/4 v0, 0x0

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 442
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 443
    const-string v0, "TagIconID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    const-string v1, "TagText"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 445
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 226
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onResume()V

    .line 227
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    if-gtz v0, :cond_0

    .line 235
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    const v1, 0x7f0204cb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 230
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    const/16 v1, 0x57e7

    if-gt v0, v1, :cond_1

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconResourceId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 14
    .param p1, "isAdd"    # Z

    .prologue
    .line 239
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 240
    .local v8, "mTagText":Ljava/lang/String;
    if-eqz v8, :cond_9

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_9

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    const/16 v12, 0x14

    if-gt v11, v12, :cond_9

    .line 241
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 243
    const/4 v6, 0x0

    .line 244
    .local v6, "isNameDuplicated":Z
    const/16 v11, 0xd

    new-array v2, v11, [I

    fill-array-data v2, :array_0

    .line 247
    .local v2, "defaultTags":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v11, v2

    if-ge v3, v11, :cond_0

    .line 248
    aget v11, v2, v3

    invoke-static {v11}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v10

    .line 249
    .local v10, "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    iget v11, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 250
    .local v1, "defTagName":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 251
    const/4 v6, 0x1

    .line 255
    .end local v1    # "defTagName":Ljava/lang/String;
    .end local v10    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    :cond_0
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getCustomTags()Ljava/util/ArrayList;

    move-result-object v0

    .line 256
    .local v0, "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v3, v11, :cond_3

    .line 257
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    .line 258
    .restart local v10    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    iget-object v11, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    if-eqz v11, :cond_1

    iget-object v11, v10, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 259
    const/4 v6, 0x1

    .line 256
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 247
    .end local v0    # "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    .restart local v1    # "defTagName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 262
    .end local v1    # "defTagName":Ljava/lang/String;
    .end local v10    # "tag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .restart local v0    # "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    :cond_3
    if-eqz v6, :cond_5

    iget-boolean v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mIsEditMode:Z

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    invoke-virtual {v11, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 263
    :cond_4
    const v11, 0x7f090f2b

    const/4 v12, 0x0

    invoke-static {p0, v11, v12}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    .line 294
    .end local v0    # "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    .end local v2    # "defaultTags":[I
    .end local v3    # "i":I
    .end local v6    # "isNameDuplicated":Z
    :goto_2
    return-void

    .line 267
    .restart local v0    # "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    .restart local v2    # "defaultTags":[I
    .restart local v3    # "i":I
    .restart local v6    # "isNameDuplicated":Z
    :cond_5
    iget-boolean v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mIsEditMode:Z

    if-eqz v11, :cond_7

    .line 269
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget v12, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    iget-object v13, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mLastTagName:Ljava/lang/String;

    invoke-virtual {v11, v12, v8, v13}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->updateCustomTagByName(ILjava/lang/String;Ljava/lang/String;)Z

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "com.sec.android.app.shealth.heartrate"

    const-string v13, "HR13"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :goto_3
    const v11, 0x7f090835

    const/4 v12, 0x0

    invoke-static {p0, v11, v12}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    .line 277
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 278
    .local v7, "mIntent":Landroid/content/Intent;
    const/4 v5, 0x0

    .line 279
    .local v5, "isFromMoreTags":Z
    if-eqz v7, :cond_6

    .line 280
    const-string v11, "from_more_activity"

    const/4 v12, 0x0

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 282
    :cond_6
    if-eqz v5, :cond_8

    .line 283
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 284
    .local v9, "returnIntent":Landroid/content/Intent;
    const/4 v11, -0x1

    invoke-virtual {p0, v11, v9}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->setResult(ILandroid/content/Intent;)V

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->finish()V

    goto :goto_2

    .line 272
    .end local v5    # "isFromMoreTags":Z
    .end local v7    # "mIntent":Landroid/content/Intent;
    .end local v9    # "returnIntent":Landroid/content/Intent;
    :cond_7
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget v12, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    invoke-virtual {v11, v12, v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->createCustomTag(ILjava/lang/String;)Landroid/net/Uri;

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "com.sec.android.app.shealth.heartrate"

    const-string v13, "HR12"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 287
    .restart local v5    # "isFromMoreTags":Z
    .restart local v7    # "mIntent":Landroid/content/Intent;
    :cond_8
    new-instance v4, Landroid/content/Intent;

    const-class v11, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-direct {v4, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 288
    .local v4, "intent":Landroid/content/Intent;
    const/high16 v11, 0x4000000

    invoke-virtual {v4, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 289
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 292
    .end local v0    # "customTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;>;"
    .end local v2    # "defaultTags":[I
    .end local v3    # "i":I
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "isFromMoreTags":Z
    .end local v6    # "isNameDuplicated":Z
    .end local v7    # "mIntent":Landroid/content/Intent;
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090f2a

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {p0, v11, v12}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 244
    :array_0
    .array-data 4
        0x5208
        0x5335
        0x526d
        0x526f
        0x52d3
        0x52d6
        0x52d5
        0x52d2
        0x52d1
        0x52d4
        0x5336
        0x5337
        0x5338
    .end array-data
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 435
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 436
    const-string v0, "TagIconID"

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 437
    const-string v0, "TagText"

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    return-void
.end method
