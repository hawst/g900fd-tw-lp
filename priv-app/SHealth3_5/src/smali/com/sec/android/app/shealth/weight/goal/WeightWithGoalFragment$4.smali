.class Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$4;
.super Ljava/lang/Object;
.source "WeightWithGoalFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->showAlertDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 327
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->deleteDataById(J)Z

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment$4;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightWithGoalFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->finish()V

    .line 330
    return-void
.end method
