.class Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;
.super Ljava/lang/Object;
.source "UvSkinTypeDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 275
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clearSkinSelection()V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 277
    .local v0, "imageSelected":Landroid/widget/ImageView;
    const v1, 0x7f020703

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->setSelectedSkinType(Landroid/view/View;)V

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$4;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateButton()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$500(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    .line 280
    return-void
.end method
