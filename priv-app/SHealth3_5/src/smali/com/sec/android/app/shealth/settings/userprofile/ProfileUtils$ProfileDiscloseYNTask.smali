.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;
.super Ljava/lang/Object;
.source "ProfileUtils.java"

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProfileDiscloseYNTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;
    }
.end annotation


# static fields
.field private static final PROFILE_SETDISCLOSE_URL:Ljava/lang/String; = "platform/profile/setProfileDiscloseYN"

.field private static final REQUEST_PROFILE_SETDISCLOSE:I = 0x7d8

.field private static handler:Landroid/os/Handler;

.field private static sProfileDiscloseYN:Ljava/lang/String;

.field private static sResponse:Ljava/lang/String;


# instance fields
.field private deviceId:Ljava/lang/String;

.field private httpHeaders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppID:Ljava/lang/String;

.field private mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private mContext:Landroid/content/Context;

.field private mGson:Lcom/google/gson/Gson;

.field private mTryToRenewToken:Z

.field private mUserToken:Ljava/lang/String;

.field telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sResponse:Ljava/lang/String;

    .line 446
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileDiscloseYN"    # Ljava/lang/Boolean;

    .prologue
    .line 461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 427
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->httpHeaders:Ljava/util/HashMap;

    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mTryToRenewToken:Z

    .line 462
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mContext:Landroid/content/Context;

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 464
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->deviceId:Ljava/lang/String;

    .line 465
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mGson:Lcom/google/gson/Gson;

    .line 467
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    const-string v0, "Y"

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sProfileDiscloseYN:Ljava/lang/String;

    .line 471
    :goto_0
    return-void

    .line 470
    :cond_0
    const-string v0, "N"

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sProfileDiscloseYN:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mUserToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 418
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mUserToken:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getUserToken()V
    .locals 6

    .prologue
    .line 627
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mTryToRenewToken:Z

    .line 628
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "requestAccessTokenForOperation()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    :try_start_0
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mContext:Landroid/content/Context;

    const-string v3, "1y90e30264"

    const-string v4, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v5, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$3;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;)V

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 659
    :goto_0
    return-void

    .line 655
    :catch_0
    move-exception v0

    .line 657
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public initConnectionManager()V
    .locals 5

    .prologue
    .line 477
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-nez v1, :cond_0

    .line 480
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 481
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 485
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    sget v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddress(Ljava/lang/String;I)Z

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(ZLjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 488
    :catch_0
    move-exception v0

    .line 490
    .local v0, "e":Lcom/sec/android/service/health/connectionmanager2/NetException;
    invoke-virtual {v0}, Lcom/sec/android/service/health/connectionmanager2/NetException;->printStackTrace()V

    goto :goto_0
.end method

.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 544
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    const/4 v1, 0x0

    .line 547
    .local v1, "exceptionObject":Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;
    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 551
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mGson:Lcom/google/gson/Gson;

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "exceptionObject":Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;
    check-cast v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    .restart local v1    # "exceptionObject":Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;
    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mTryToRenewToken:Z

    if-eqz v4, :cond_2

    .line 607
    :cond_1
    :goto_1
    return-void

    .line 553
    .end local v1    # "exceptionObject":Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;
    :catch_0
    move-exception v0

    .line 555
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .restart local v1    # "exceptionObject":Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;
    goto :goto_0

    .line 564
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;-><init>()V

    .line 565
    .local v2, "mErrorCodeConstant":Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;
    if-eqz v2, :cond_3

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getHttpResCode()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->getUserTokenErrorCode(I)I

    move-result v4

    sget v5, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->ACCESS_TOKEN_ERROR_CODE:I

    if-ne v4, v5, :cond_3

    .line 567
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Samsung Account Token is not validate. request again"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mTryToRenewToken:Z

    .line 571
    :try_start_1
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 595
    :catch_1
    move-exception v0

    .line 597
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 602
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    if-eqz v1, :cond_1

    const-string v4, "PRO_5412"

    iget-object v5, v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask$ExceptionObject;->code:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 603
    sget-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 604
    .local v3, "msg":Landroid/os/Message;
    const/16 v4, 0x64

    iput v4, v3, Landroid/os/Message;->what:I

    .line 605
    sget-object v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "tag"    # Ljava/lang/Object;
    .param p5, "ReRequest"    # Ljava/lang/Object;

    .prologue
    .line 621
    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "response"    # Ljava/lang/Object;
    .param p5, "tag"    # Ljava/lang/Object;

    .prologue
    .line 525
    const/16 v1, 0x7d8

    if-ne p3, v1, :cond_0

    .line 527
    if-eqz p4, :cond_0

    .line 529
    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    check-cast p4, Ljava/lang/String;

    .end local p4    # "response":Ljava/lang/Object;
    sput-object p4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sResponse:Ljava/lang/String;

    .line 531
    sget-object v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sResponse:Ljava/lang/String;

    const-string v2, "\"success\""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 532
    sget-object v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 533
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->what:I

    .line 534
    sget-object v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 539
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public setProfileDiscloseYN(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "appID"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 505
    const/4 v6, 0x0

    .line 507
    .local v6, "bodyData":Ljava/lang/String;
    new-instance v10, Lcom/google/gson/JsonObject;

    invoke-direct {v10}, Lcom/google/gson/JsonObject;-><init>()V

    .line 509
    .local v10, "body":Lcom/google/gson/JsonObject;
    sget-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sProfileDiscloseYN:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 510
    const-string v0, "Y"

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sProfileDiscloseYN:Ljava/lang/String;

    .line 511
    :cond_0
    const-string/jumbo v0, "profileDiscloseYN"

    sget-object v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->sProfileDiscloseYN:Ljava/lang/String;

    invoke-virtual {v10, v0, v1}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->httpHeaders:Ljava/util/HashMap;

    const-string v1, "Accept"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "application/json"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->httpHeaders:Ljava/util/HashMap;

    const-string/jumbo v1, "reqAppId"

    new-array v2, v7, [Ljava/lang/String;

    aput-object p2, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->httpHeaders:Ljava/util/HashMap;

    const-string v1, "access_token"

    new-array v2, v7, [Ljava/lang/String;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->httpHeaders:Ljava/util/HashMap;

    const-string v1, "deviceId"

    new-array v2, v7, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->deviceId:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mGson:Lcom/google/gson/Gson;

    invoke-virtual {v0, v10}, Lcom/google/gson/Gson;->toJson(Lcom/google/gson/JsonElement;)Ljava/lang/String;

    move-result-object v6

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const/16 v2, 0x7d8

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    const-string/jumbo v4, "platform/profile/setProfileDiscloseYN"

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils$ProfileDiscloseYNTask;->httpHeaders:Ljava/util/HashMap;

    move-object v1, p0

    move-object v7, p0

    move-object v8, v5

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    .line 520
    return-void
.end method
