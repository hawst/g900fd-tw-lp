.class final Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustCheckerImpl;
.super Ljava/lang/Object;
.source "ConditionChecker.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustChecker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MustCheckerImpl"
.end annotation


# instance fields
.field private final mValidatingObject:[Ljava/lang/Object;


# direct methods
.method private varargs constructor <init>([Ljava/lang/Object;)V
    .locals 0
    .param p1, "validatingObject"    # [Ljava/lang/Object;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustCheckerImpl;->mValidatingObject:[Ljava/lang/Object;

    .line 37
    return-void
.end method

.method synthetic constructor <init>([Ljava/lang/Object;Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$1;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$1;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustCheckerImpl;-><init>([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public beGreaterThen(I)V
    .locals 8
    .param p1, "comparatorValue"    # I

    .prologue
    .line 46
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustCheckerImpl;->mValidatingObject:[Ljava/lang/Object;

    if-nez v5, :cond_0

    .line 47
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Validated object is Null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustCheckerImpl;->mValidatingObject:[Ljava/lang/Object;

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v4, v0, v1

    .line 51
    .local v4, "object":Ljava/lang/Object;
    instance-of v5, v4, Ljava/lang/Number;

    if-eqz v5, :cond_1

    move-object v3, v4

    .line 52
    check-cast v3, Ljava/lang/Number;

    .line 53
    .local v3, "number":Ljava/lang/Number;
    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v5

    if-ge v5, p1, :cond_2

    .line 54
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Validated object is less then "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 57
    .end local v3    # "number":Ljava/lang/Number;
    :cond_1
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Trying to use numeric comparison with not a number"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 50
    .restart local v3    # "number":Ljava/lang/Number;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    .end local v3    # "number":Ljava/lang/Number;
    .end local v4    # "object":Ljava/lang/Object;
    :cond_3
    return-void
.end method

.method public beNotNull(Ljava/lang/String;)V
    .locals 5
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustCheckerImpl;->mValidatingObject:[Ljava/lang/Object;

    if-nez v4, :cond_0

    .line 65
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/ConditionChecker$MustCheckerImpl;->mValidatingObject:[Ljava/lang/Object;

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 69
    .local v3, "object":Ljava/lang/Object;
    if-nez v3, :cond_1

    .line 70
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 68
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    .end local v3    # "object":Ljava/lang/Object;
    :cond_2
    return-void
.end method
