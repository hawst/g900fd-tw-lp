.class public Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->sSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static digestValue(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 11

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    new-array v1, v4, [B

    const/4 v2, 0x5

    :try_start_2
    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x63

    aput v9, v3, v8

    const/16 v8, -0x5686

    aput v8, v3, v7

    const/16 v7, -0x11

    aput v7, v3, v6

    const/16 v6, -0x27

    aput v6, v3, v5

    const/16 v5, 0x441a

    aput v5, v3, v2

    const/4 v2, 0x5

    new-array v2, v2, [I

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/16 v10, -0x5b

    aput v10, v2, v9

    const/16 v9, -0x56a9

    aput v9, v2, v8

    const/16 v8, -0x57

    aput v8, v2, v7

    const/16 v7, -0x73

    aput v7, v2, v6

    const/16 v6, 0x444f

    aput v6, v2, v5

    move v5, v4

    :goto_1
    array-length v6, v2

    if-lt v5, v6, :cond_0

    array-length v2, v3

    new-array v2, v2, [C

    :goto_2
    array-length v5, v2

    if-lt v4, v5, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    :goto_3
    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    :goto_4
    return-object v0

    :catch_0
    move-exception v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->TAG:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    :try_start_3
    aget v6, v2, v5

    aget v7, v3, v5

    xor-int/2addr v6, v7

    aput v6, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :catch_1
    move-exception v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->TAG:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_4
.end method

.method public static doesPinCodeMatch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 28

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/16 v9, 0x627a

    aput v9, v2, v8

    const/16 v8, -0x17a9

    aput v8, v2, v7

    const/16 v7, -0x26

    aput v7, v2, v6

    const/16 v6, -0x55

    aput v6, v2, v5

    const/16 v5, 0x7a0c

    aput v5, v2, v4

    const/16 v4, 0x2f32

    aput v4, v2, v3

    const/16 v3, -0x3984

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/16 v10, 0x624c

    aput v10, v1, v9

    const/16 v9, -0x179e

    aput v9, v1, v8

    const/16 v8, -0x18

    aput v8, v1, v7

    const/16 v7, -0x7a

    aput v7, v1, v6

    const/16 v6, 0x7a4d

    aput v6, v1, v5

    const/16 v5, 0x2f7a

    aput v5, v1, v4

    const/16 v4, -0x39d1

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getEncryptedPinCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/16 v5, -0x1c

    aput v5, v2, v4

    const/16 v4, 0x5c11

    aput v4, v2, v3

    const/16 v3, -0x5cef

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/16 v6, -0x2f

    aput v6, v1, v5

    const/16 v5, 0x5c55

    aput v5, v1, v4

    const/16 v4, -0x5ca4

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getEncryptedPinCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->sSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x1115

    aput v26, v2, v25

    const/16 v25, 0x7a63

    aput v25, v2, v24

    const/16 v24, 0x7915

    aput v24, v2, v23

    const/16 v23, 0x610e

    aput v23, v2, v22

    const/16 v22, 0x7912

    aput v22, v2, v21

    const/16 v21, 0x570a

    aput v21, v2, v20

    const/16 v20, 0x1d36

    aput v20, v2, v19

    const/16 v19, -0x5993

    aput v19, v2, v18

    const/16 v18, -0x7

    aput v18, v2, v17

    const/16 v17, -0x62

    aput v17, v2, v16

    const/16 v16, 0x1041

    aput v16, v2, v15

    const/16 v15, -0x6081

    aput v15, v2, v14

    const/16 v14, -0xd

    aput v14, v2, v13

    const/16 v13, -0x2ee0

    aput v13, v2, v12

    const/16 v12, -0x58

    aput v12, v2, v11

    const/16 v11, -0x3e

    aput v11, v2, v10

    const/16 v10, -0x34

    aput v10, v2, v9

    const/16 v9, -0x18

    aput v9, v2, v8

    const/16 v8, -0x2bb9

    aput v8, v2, v7

    const/16 v7, -0x49

    aput v7, v2, v6

    const/16 v6, -0x17a5

    aput v6, v2, v3

    const/16 v3, -0x65

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x1171

    aput v27, v1, v26

    const/16 v26, 0x7a11

    aput v26, v1, v25

    const/16 v25, 0x797a

    aput v25, v1, v24

    const/16 v24, 0x6179

    aput v24, v1, v23

    const/16 v23, 0x7961

    aput v23, v1, v22

    const/16 v22, 0x5779

    aput v22, v1, v21

    const/16 v21, 0x1d57

    aput v21, v1, v20

    const/16 v20, -0x59e3

    aput v20, v1, v19

    const/16 v19, -0x5a

    aput v19, v1, v18

    const/16 v18, -0xb

    aput v18, v1, v17

    const/16 v17, 0x1022

    aput v17, v1, v16

    const/16 v16, -0x60f0

    aput v16, v1, v15

    const/16 v15, -0x61

    aput v15, v1, v14

    const/16 v14, -0x2e81

    aput v14, v1, v13

    const/16 v13, -0x2f

    aput v13, v1, v12

    const/16 v12, -0x4a

    aput v12, v1, v11

    const/16 v11, -0x5b

    aput v11, v1, v10

    const/16 v10, -0x66

    aput v10, v1, v9

    const/16 v9, -0x2bce

    aput v9, v1, v8

    const/16 v8, -0x2c

    aput v8, v1, v7

    const/16 v7, -0x17c2

    aput v7, v1, v6

    const/16 v6, -0x18

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/16 v12, -0x79

    aput v12, v2, v11

    const/16 v11, -0x3d

    aput v11, v2, v10

    const/16 v10, -0x35

    aput v10, v2, v9

    const/16 v9, -0x34

    aput v9, v2, v8

    const/16 v8, -0x43b4

    aput v8, v2, v7

    const/16 v7, -0xc

    aput v7, v2, v3

    const/16 v3, -0x6d

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/16 v13, -0x4f

    aput v13, v1, v12

    const/16 v12, -0xa

    aput v12, v1, v11

    const/4 v11, -0x7

    aput v11, v1, v10

    const/16 v10, -0x1f

    aput v10, v1, v9

    const/16 v9, -0x43f3

    aput v9, v1, v8

    const/16 v8, -0x44

    aput v8, v1, v7

    const/16 v7, -0x40

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v7, v1

    if-lt v3, v7, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v7, v1

    if-lt v3, v7, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getEncryptedPinCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v6, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveStringValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v1, v4

    :goto_8
    return v1

    :cond_1
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_8
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_9
    const/4 v1, 0x1

    goto :goto_8
.end method

.method public static getAccountPasswordVerifyIntent()Landroid/content/Intent;
    .locals 37

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const/16 v0, 0x12

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, -0x42

    aput v20, v1, v19

    const/16 v19, -0x23f5

    aput v19, v1, v18

    const/16 v18, -0x4e

    aput v18, v1, v17

    const/16 v17, -0x708f

    aput v17, v1, v16

    const/16 v16, -0x1a

    aput v16, v1, v15

    const/4 v15, -0x8

    aput v15, v1, v14

    const/16 v14, 0x424a

    aput v14, v1, v13

    const/16 v13, 0x5a32

    aput v13, v1, v12

    const/16 v12, 0x4e2a

    aput v12, v1, v11

    const/16 v11, 0x5b2f

    aput v11, v1, v10

    const/16 v10, 0x5d75

    aput v10, v1, v9

    const/16 v9, -0x7cd3

    aput v9, v1, v8

    const/16 v8, -0x10

    aput v8, v1, v7

    const/16 v7, -0x508e

    aput v7, v1, v6

    const/16 v6, -0x7f

    aput v6, v1, v5

    const/16 v5, -0xfd1

    aput v5, v1, v4

    const/16 v4, -0x61

    aput v4, v1, v2

    const/16 v2, 0x7d76

    aput v2, v1, v0

    const/16 v0, 0x12

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, -0x30

    aput v21, v0, v20

    const/16 v20, -0x239e

    aput v20, v0, v19

    const/16 v19, -0x24

    aput v19, v0, v18

    const/16 v18, -0x70ea

    aput v18, v0, v17

    const/16 v17, -0x71

    aput v17, v0, v16

    const/16 v16, -0x75

    aput v16, v0, v15

    const/16 v15, 0x4264

    aput v15, v0, v14

    const/16 v14, 0x5a42

    aput v14, v0, v13

    const/16 v13, 0x4e5a

    aput v13, v0, v12

    const/16 v12, 0x5b4e

    aput v12, v0, v11

    const/16 v11, 0x5d5b

    aput v11, v0, v10

    const/16 v10, -0x7ca3

    aput v10, v0, v9

    const/16 v9, -0x7d

    aput v9, v0, v8

    const/16 v8, -0x50e3

    aput v8, v0, v7

    const/16 v7, -0x51

    aput v7, v0, v6

    const/16 v6, -0xfbe

    aput v6, v0, v5

    const/16 v5, -0x10

    aput v5, v0, v4

    const/16 v4, 0x7d15

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0x1e

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, -0x52

    aput v33, v1, v32

    const/16 v32, -0x60

    aput v32, v1, v31

    const/16 v31, -0x7c8e

    aput v31, v1, v30

    const/16 v30, -0x2b

    aput v30, v1, v29

    const/16 v29, -0x4d

    aput v29, v1, v28

    const/16 v28, -0x4d

    aput v28, v1, v27

    const/16 v27, -0x32

    aput v27, v1, v26

    const/16 v26, -0x79

    aput v26, v1, v25

    const/16 v25, 0x762e

    aput v25, v1, v24

    const/16 v24, 0x5c15

    aput v24, v1, v23

    const/16 v23, 0x131d

    aput v23, v1, v22

    const/16 v22, 0x763d

    aput v22, v1, v21

    const/16 v21, -0x77e8

    aput v21, v1, v20

    const/16 v20, -0x1f

    aput v20, v1, v19

    const/16 v19, 0x485d

    aput v19, v1, v18

    const/16 v18, -0x8d1

    aput v18, v1, v17

    const/16 v17, -0x62

    aput v17, v1, v16

    const/16 v16, -0x77ca

    aput v16, v1, v15

    const/16 v15, -0x5a

    aput v15, v1, v14

    const/16 v14, -0x44

    aput v14, v1, v13

    const/16 v13, -0x5e

    aput v13, v1, v12

    const/16 v12, -0x32

    aput v12, v1, v11

    const/16 v11, -0xdd0

    aput v11, v1, v10

    const/16 v10, -0x7e

    aput v10, v1, v9

    const/16 v9, -0x4f

    aput v9, v1, v8

    const/16 v8, -0x35

    aput v8, v1, v7

    const/16 v7, -0x71

    aput v7, v1, v6

    const/16 v6, 0x5d05

    aput v6, v1, v5

    const/16 v5, 0x7f32

    aput v5, v1, v2

    const/16 v2, -0x54e4

    aput v2, v1, v0

    const/16 v0, 0x1e

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, -0x27

    aput v34, v0, v33

    const/16 v33, -0x3b

    aput v33, v0, v32

    const/16 v32, -0x7ce5

    aput v32, v0, v31

    const/16 v31, -0x7d

    aput v31, v0, v30

    const/16 v30, -0x39

    aput v30, v0, v29

    const/16 v29, -0x23

    aput v29, v0, v28

    const/16 v28, -0x45

    aput v28, v0, v27

    const/16 v27, -0x18

    aput v27, v0, v26

    const/16 v26, 0x764d

    aput v26, v0, v25

    const/16 v25, 0x5c76

    aput v25, v0, v24

    const/16 v24, 0x135c

    aput v24, v0, v23

    const/16 v23, 0x7613

    aput v23, v0, v22

    const/16 v22, -0x778a

    aput v22, v0, v21

    const/16 v21, -0x78

    aput v21, v0, v20

    const/16 v20, 0x4833

    aput v20, v0, v19

    const/16 v19, -0x8b8

    aput v19, v0, v18

    const/16 v18, -0x9

    aput v18, v0, v17

    const/16 v17, -0x77bb

    aput v17, v0, v16

    const/16 v16, -0x78

    aput v16, v0, v15

    const/16 v15, -0x34

    aput v15, v0, v14

    const/16 v14, -0x2e

    aput v14, v0, v13

    const/16 v13, -0x51

    aput v13, v0, v12

    const/16 v12, -0xde2

    aput v12, v0, v11

    const/16 v11, -0xe

    aput v11, v0, v10

    const/16 v10, -0x3e

    aput v10, v0, v9

    const/16 v9, -0x5c

    aput v9, v0, v8

    const/16 v8, -0x5f

    aput v8, v0, v7

    const/16 v7, 0x5d68

    aput v7, v0, v6

    const/16 v6, 0x7f5d

    aput v6, v0, v5

    const/16 v5, -0x5481

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0x9

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, -0x5c

    aput v11, v1, v10

    const/16 v10, -0x45

    aput v10, v1, v9

    const/16 v9, -0x2d

    aput v9, v1, v8

    const/16 v8, -0x37be

    aput v8, v1, v7

    const/16 v7, -0x5a

    aput v7, v1, v6

    const/16 v6, -0x17

    aput v6, v1, v5

    const/16 v5, -0x19

    aput v5, v1, v4

    const/16 v4, -0x51d3

    aput v4, v1, v2

    const/16 v2, -0x33

    aput v2, v1, v0

    const/16 v0, 0x9

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, -0x40

    aput v12, v0, v11

    const/16 v11, -0x2e

    aput v11, v0, v10

    const/16 v10, -0x74

    aput v10, v0, v9

    const/16 v9, -0x37ca

    aput v9, v0, v8

    const/16 v8, -0x38

    aput v8, v0, v7

    const/16 v7, -0x74

    aput v7, v0, v6

    const/16 v6, -0x72

    aput v6, v0, v5

    const/16 v5, -0x51bf

    aput v5, v0, v4

    const/16 v4, -0x52

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v4, v0

    if-lt v2, v4, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v4, v0

    if-lt v2, v4, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x65bc

    aput v13, v1, v12

    const/16 v12, -0x54

    aput v12, v1, v11

    const/16 v11, -0x51c2

    aput v11, v1, v10

    const/16 v10, -0x62

    aput v10, v1, v9

    const/16 v9, -0x20c3

    aput v9, v1, v8

    const/16 v8, -0x46

    aput v8, v1, v7

    const/16 v7, -0x2a

    aput v7, v1, v6

    const/16 v6, -0x63

    aput v6, v1, v5

    const/16 v5, -0x43c0

    aput v5, v1, v2

    const/16 v2, -0x73

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x6590

    aput v14, v0, v13

    const/16 v13, -0x66

    aput v13, v0, v12

    const/16 v12, -0x51f4

    aput v12, v0, v11

    const/16 v11, -0x52

    aput v11, v0, v10

    const/16 v10, -0x20f2

    aput v10, v0, v9

    const/16 v9, -0x21

    aput v9, v0, v8

    const/16 v8, -0x1a

    aput v8, v0, v7

    const/16 v7, -0x5c

    aput v7, v0, v6

    const/16 v6, -0x43c7

    aput v6, v0, v5

    const/16 v5, -0x44

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_6
    array-length v5, v0

    if-lt v2, v5, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_7
    array-length v5, v0

    if-lt v2, v5, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xe

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, -0x69f1

    aput v16, v1, v15

    const/16 v15, -0xd

    aput v15, v1, v14

    const/16 v14, -0x52

    aput v14, v1, v13

    const/16 v13, -0x31db

    aput v13, v1, v12

    const/16 v12, -0x55

    aput v12, v1, v11

    const/16 v11, -0x2c

    aput v11, v1, v10

    const/16 v10, -0x75e8

    aput v10, v1, v9

    const/16 v9, -0x2b

    aput v9, v1, v8

    const/16 v8, -0x31

    aput v8, v1, v7

    const/16 v7, -0x4f

    aput v7, v1, v6

    const/16 v6, 0x2741

    aput v6, v1, v5

    const/16 v5, -0x79b2

    aput v5, v1, v4

    const/16 v4, -0x16

    aput v4, v1, v2

    const/16 v2, -0x12

    aput v2, v1, v0

    const/16 v0, 0xe

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x6985

    aput v17, v0, v16

    const/16 v16, -0x6a

    aput v16, v0, v15

    const/16 v15, -0x24

    aput v15, v0, v14

    const/16 v14, -0x31ba

    aput v14, v0, v13

    const/16 v13, -0x32

    aput v13, v0, v12

    const/16 v12, -0x59

    aput v12, v0, v11

    const/16 v11, -0x75c8

    aput v11, v0, v10

    const/16 v10, -0x76

    aput v10, v0, v9

    const/16 v9, -0x45

    aput v9, v0, v8

    const/16 v8, -0x21

    aput v8, v0, v7

    const/16 v7, 0x2724

    aput v7, v0, v6

    const/16 v6, -0x79d9

    aput v6, v0, v5

    const/16 v5, -0x7a

    aput v5, v0, v4

    const/16 v4, -0x73

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_8
    array-length v4, v0

    if-lt v2, v4, :cond_8

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_9
    array-length v4, v0

    if-lt v2, v4, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0x20

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, -0x269e

    aput v35, v1, v34

    const/16 v34, -0x64

    aput v34, v1, v33

    const/16 v33, 0x7370

    aput v33, v1, v32

    const/16 v32, 0x43

    aput v32, v1, v31

    const/16 v31, -0x53cd

    aput v31, v1, v30

    const/16 v30, -0x6b

    aput v30, v1, v29

    const/16 v29, 0x1e76

    aput v29, v1, v28

    const/16 v28, 0x928

    aput v28, v1, v27

    const/16 v27, 0x43a

    aput v27, v1, v26

    const/16 v26, 0x5c34

    aput v26, v1, v25

    const/16 v25, -0xf95

    aput v25, v1, v24

    const/16 v24, -0x3d

    aput v24, v1, v23

    const/16 v23, -0x1b1

    aput v23, v1, v22

    const/16 v22, -0x43

    aput v22, v1, v21

    const/16 v21, 0x183d

    aput v21, v1, v20

    const/16 v20, -0x72e0

    aput v20, v1, v19

    const/16 v19, -0x4b

    aput v19, v1, v18

    const/16 v18, -0x26ca

    aput v18, v1, v17

    const/16 v17, -0x65

    aput v17, v1, v16

    const/16 v16, -0x33

    aput v16, v1, v15

    const/16 v15, -0x7a

    aput v15, v1, v14

    const/16 v14, -0x4894

    aput v14, v1, v13

    const/16 v13, -0x7c

    aput v13, v1, v12

    const/16 v12, 0x1738

    aput v12, v1, v11

    const/16 v11, 0x5d2e

    aput v11, v1, v10

    const/16 v10, -0x67e7

    aput v10, v1, v9

    const/16 v9, -0x25

    aput v9, v1, v8

    const/16 v8, -0x2688

    aput v8, v1, v7

    const/16 v7, -0x12

    aput v7, v1, v6

    const/16 v6, 0x4b3d

    aput v6, v1, v5

    const/16 v5, -0x4285

    aput v5, v1, v2

    const/16 v2, -0x7b

    aput v2, v1, v0

    const/16 v0, 0x20

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, -0x26a9

    aput v36, v0, v35

    const/16 v35, -0x27

    aput v35, v0, v34

    const/16 v34, 0x7342

    aput v34, v0, v33

    const/16 v33, 0x73

    aput v33, v0, v32

    const/16 v32, -0x5400

    aput v32, v0, v31

    const/16 v31, -0x54

    aput v31, v0, v30

    const/16 v30, 0x1e45

    aput v30, v0, v29

    const/16 v29, 0x91e

    aput v29, v0, v28

    const/16 v28, 0x409

    aput v28, v0, v27

    const/16 v27, 0x5c04

    aput v27, v0, v26

    const/16 v26, -0xfa4

    aput v26, v0, v25

    const/16 v25, -0x10

    aput v25, v0, v24

    const/16 v24, -0x188

    aput v24, v0, v23

    const/16 v23, -0x2

    aput v23, v0, v22

    const/16 v22, 0x1805

    aput v22, v0, v21

    const/16 v21, -0x72e8

    aput v21, v0, v20

    const/16 v20, -0x73

    aput v20, v0, v19

    const/16 v19, -0x26ff

    aput v19, v0, v18

    const/16 v18, -0x27

    aput v18, v0, v17

    const/16 v17, -0x72

    aput v17, v0, v16

    const/16 v16, -0x49

    aput v16, v0, v15

    const/16 v15, -0x48a4

    aput v15, v0, v14

    const/16 v14, -0x49

    aput v14, v0, v13

    const/16 v13, 0x177c

    aput v13, v0, v12

    const/16 v12, 0x5d17

    aput v12, v0, v11

    const/16 v11, -0x67a3

    aput v11, v0, v10

    const/16 v10, -0x68

    aput v10, v0, v9

    const/16 v9, -0x26c3

    aput v9, v0, v8

    const/16 v8, -0x27

    aput v8, v0, v7

    const/16 v7, 0x4b78

    aput v7, v0, v6

    const/16 v6, -0x42b5

    aput v6, v0, v5

    const/16 v5, -0x43

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_a
    array-length v5, v0

    if-lt v2, v5, :cond_a

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_b
    array-length v5, v0

    if-lt v2, v5, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xc

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, -0x18

    aput v14, v1, v13

    const/16 v13, -0x15

    aput v13, v1, v12

    const/16 v12, -0x1e

    aput v12, v1, v11

    const/4 v11, -0x5

    aput v11, v1, v10

    const/16 v10, 0x381a

    aput v10, v1, v9

    const/16 v9, -0x52b4

    aput v9, v1, v8

    const/16 v8, -0x3d

    aput v8, v1, v7

    const/16 v7, -0x4f

    aput v7, v1, v6

    const/16 v6, 0x690a

    aput v6, v1, v5

    const/16 v5, -0x3ff6

    aput v5, v1, v4

    const/16 v4, -0x5d

    aput v4, v1, v2

    const/16 v2, 0x2e24

    aput v2, v1, v0

    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, -0x73

    aput v15, v0, v14

    const/16 v14, -0x71

    aput v14, v0, v13

    const/16 v13, -0x73

    aput v13, v0, v12

    const/16 v12, -0x6a

    aput v12, v0, v11

    const/16 v11, 0x3845

    aput v11, v0, v10

    const/16 v10, -0x52c8

    aput v10, v0, v9

    const/16 v9, -0x53

    aput v9, v0, v8

    const/16 v8, -0x3c

    aput v8, v0, v7

    const/16 v7, 0x6965

    aput v7, v0, v6

    const/16 v6, -0x3f97

    aput v6, v0, v5

    const/16 v5, -0x40

    aput v5, v0, v4

    const/16 v4, 0x2e45

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_c
    array-length v4, v0

    if-lt v2, v4, :cond_c

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_d
    array-length v4, v0

    if-lt v2, v4, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0xe

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, -0x1cc9

    aput v17, v1, v16

    const/16 v16, -0x5b

    aput v16, v1, v15

    const/4 v15, -0x6

    aput v15, v1, v14

    const/16 v14, 0x666f

    aput v14, v1, v13

    const/16 v13, -0x69dd

    aput v13, v1, v12

    const/16 v12, -0x40

    aput v12, v1, v11

    const/16 v11, 0x3f39

    aput v11, v1, v10

    const/16 v10, 0xf6b

    aput v10, v1, v9

    const/16 v9, -0x3abf

    aput v9, v1, v8

    const/16 v8, -0x70

    aput v8, v1, v7

    const/16 v7, -0x2b

    aput v7, v1, v6

    const/16 v6, -0x6b6

    aput v6, v1, v5

    const/16 v5, -0x46

    aput v5, v1, v2

    const/16 v2, -0x5383

    aput v2, v1, v0

    const/16 v0, 0xe

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, -0x1c92

    aput v18, v0, v17

    const/16 v17, -0x1d

    aput v17, v0, v16

    const/16 v16, -0x4d

    aput v16, v0, v15

    const/16 v15, 0x663d

    aput v15, v0, v14

    const/16 v14, -0x699a

    aput v14, v0, v13

    const/16 v13, -0x6a

    aput v13, v0, v12

    const/16 v12, 0x3f66

    aput v12, v0, v11

    const/16 v11, 0xf3f

    aput v11, v0, v10

    const/16 v10, -0x3af1

    aput v10, v0, v9

    const/16 v9, -0x3b

    aput v9, v0, v8

    const/16 v8, -0x66

    aput v8, v0, v7

    const/16 v7, -0x6f7

    aput v7, v0, v6

    const/4 v6, -0x7

    aput v6, v0, v5

    const/16 v5, -0x53c4

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_e
    array-length v5, v0

    if-lt v2, v5, :cond_e

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_f
    array-length v5, v0

    if-lt v2, v5, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v3

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_5
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_6
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    :cond_7
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7

    :cond_8
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_8

    :cond_9
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_9

    :cond_a
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_a

    :cond_b
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_b

    :cond_c
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_c

    :cond_d
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_d

    :cond_e
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_e

    :cond_f
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_f
.end method

.method public static getEncryptedPinCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/16 v4, -0x15

    aput v4, v1, v2

    const/16 v2, -0x29

    aput v2, v1, v0

    const/16 v0, -0x40

    aput v0, v1, v9

    const/16 v0, -0x26

    aput v0, v1, v8

    const/16 v0, -0x528b

    aput v0, v1, v7

    const/16 v0, -0x1b

    aput v0, v1, v6

    const/16 v0, -0x70d5

    aput v0, v1, v3

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/16 v5, -0x23

    aput v5, v0, v4

    const/16 v4, -0x1e

    aput v4, v0, v2

    const/16 v2, -0xe

    aput v2, v0, v9

    const/16 v2, -0x9

    aput v2, v0, v8

    const/16 v2, -0x52cc

    aput v2, v0, v7

    const/16 v2, -0x53

    aput v2, v0, v6

    const/16 v2, -0x7088

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getEncryptedPinCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static getEncryptedPinCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->digestValue(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->toHex([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->sSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    return-object v0
.end method

.method private static toHex([B)Ljava/lang/String;
    .locals 14

    const-wide/16 v12, 0x0

    const/4 v11, 0x1

    const-wide v9, -0x68fb7d263ef9db06L    # -8.57477146294053E-198

    const/16 v8, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v11

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, v3

    shl-long/2addr v0, v8

    ushr-long v5, v0, v8

    aget-wide v0, v2, v3

    cmp-long v7, v0, v12

    if-eqz v7, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v5

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    :goto_0
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v11, [I

    const/16 v0, -0x48fe

    aput v0, v1, v3

    new-array v0, v11, [I

    const/16 v2, -0x48ce

    aput v2, v0, v3

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_2
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v5, v0, v12

    if-eqz v5, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    array-length v1, p0

    if-ge v0, v1, :cond_f

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v11, [I

    const/16 v0, -0x58

    aput v0, v1, v3

    new-array v0, v11, [I

    const/16 v2, -0x68

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    aget-wide v0, v2, v3

    cmp-long v5, v0, v12

    if-eqz v5, :cond_9

    xor-long/2addr v0, v9

    :cond_9
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v11, :cond_a

    const/16 v1, 0x30

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_b

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    aget-wide v0, v2, v3

    cmp-long v5, v0, v12

    if-eqz v5, :cond_c

    xor-long/2addr v0, v9

    :cond_c
    shl-long/2addr v0, v8

    shr-long/2addr v0, v8

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v5, v2, v1

    long-to-int v1, v5

    if-gtz v1, :cond_d

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    int-to-long v0, v0

    shl-long/2addr v0, v8

    ushr-long v5, v0, v8

    aget-wide v0, v2, v3

    cmp-long v7, v0, v12

    if-eqz v7, :cond_e

    xor-long/2addr v0, v9

    :cond_e
    ushr-long/2addr v0, v8

    shl-long/2addr v0, v8

    xor-long/2addr v0, v5

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    goto/16 :goto_0

    :cond_f
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 24

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/16 v6, -0x74

    aput v6, v2, v5

    const/4 v5, -0x7

    aput v5, v2, v3

    const/16 v3, 0xd0e

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, -0xb

    aput v7, v1, v6

    const/16 v6, -0x64

    aput v6, v1, v5

    const/16 v5, 0xd65

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/16 v8, -0x4e81

    aput v8, v2, v7

    const/16 v7, -0x3c

    aput v7, v2, v6

    const/16 v6, -0x66

    aput v6, v2, v5

    const/16 v5, -0x38b2

    aput v5, v2, v3

    const/16 v3, -0x4f

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x4ee6

    aput v9, v1, v8

    const/16 v8, -0x4f

    aput v8, v1, v7

    const/16 v7, -0xa

    aput v7, v1, v6

    const/16 v6, -0x38d1

    aput v6, v1, v5

    const/16 v5, -0x39

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const/16 v1, 0x11

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, -0x1c

    aput v22, v2, v21

    const/16 v21, 0x205b

    aput v21, v2, v20

    const/16 v20, 0x5667

    aput v20, v2, v19

    const/16 v19, 0x4109

    aput v19, v2, v18

    const/16 v18, 0x620f

    aput v18, v2, v17

    const/16 v17, -0x6ad3

    aput v17, v2, v16

    const/16 v16, -0x24

    aput v16, v2, v15

    const/16 v15, -0x349d

    aput v15, v2, v14

    const/16 v14, -0x65

    aput v14, v2, v13

    const/16 v13, -0x3c

    aput v13, v2, v12

    const/16 v12, 0x7d3a

    aput v12, v2, v11

    const/16 v11, -0x2cc6

    aput v11, v2, v10

    const/16 v10, -0x66

    aput v10, v2, v9

    const/16 v9, -0x2f6

    aput v9, v2, v8

    const/16 v8, -0x4d

    aput v8, v2, v7

    const/16 v7, -0x74

    aput v7, v2, v3

    const/16 v3, -0x7a

    aput v3, v2, v1

    const/16 v1, 0x11

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, -0x50

    aput v23, v1, v22

    const/16 v22, 0x201e

    aput v22, v1, v21

    const/16 v21, 0x5620

    aput v21, v1, v20

    const/16 v20, 0x4156

    aput v20, v1, v19

    const/16 v19, 0x6241

    aput v19, v1, v18

    const/16 v18, -0x6a9e

    aput v18, v1, v17

    const/16 v17, -0x6b

    aput v17, v1, v16

    const/16 v16, -0x34c9

    aput v16, v1, v15

    const/16 v15, -0x35

    aput v15, v1, v14

    const/16 v14, -0x75

    aput v14, v1, v13

    const/16 v13, 0x7d65

    aput v13, v1, v12

    const/16 v12, -0x2c83

    aput v12, v1, v11

    const/16 v11, -0x2d

    aput v11, v1, v10

    const/16 v10, -0x2b4

    aput v10, v1, v9

    const/4 v9, -0x3

    aput v9, v1, v8

    const/16 v8, -0x3d

    aput v8, v1, v7

    const/16 v7, -0x3b

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v7, v1

    if-lt v3, v7, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v7, v1

    if-lt v3, v7, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v5, v6, v1, v2, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/16 v8, -0x29c1

    aput v8, v2, v7

    const/16 v7, -0x5d

    aput v7, v2, v6

    const/16 v6, -0x77f0

    aput v6, v2, v5

    const/16 v5, -0x17

    aput v5, v2, v3

    const/4 v3, -0x3

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x29a6

    aput v9, v1, v8

    const/16 v8, -0x2a

    aput v8, v1, v7

    const/16 v7, -0x7784

    aput v7, v1, v6

    const/16 v6, -0x78

    aput v6, v1, v5

    const/16 v5, -0x75

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_8
    return v1

    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_8

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7
.end method

.method public getStringValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 24

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/16 v6, -0x13

    aput v6, v2, v5

    const/16 v5, -0x74db

    aput v5, v2, v3

    const/16 v3, -0x20

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, -0x74c0

    aput v6, v1, v5

    const/16 v5, -0x75

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/16 v8, -0x11

    aput v8, v2, v7

    const/16 v7, -0x36

    aput v7, v2, v6

    const/16 v6, -0x3d

    aput v6, v2, v5

    const/16 v5, -0x4a

    aput v5, v2, v3

    const/16 v3, -0x178c

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x76

    aput v9, v1, v8

    const/16 v8, -0x41

    aput v8, v1, v7

    const/16 v7, -0x51

    aput v7, v1, v6

    const/16 v6, -0x29

    aput v6, v1, v5

    const/16 v5, -0x17fe

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/16 v8, -0x32fa

    aput v8, v2, v7

    const/16 v7, -0x42

    aput v7, v2, v6

    const/16 v6, -0x37

    aput v6, v2, v3

    const/16 v3, 0xa08

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/16 v9, -0x328e

    aput v9, v1, v8

    const/16 v8, -0x33

    aput v8, v1, v7

    const/16 v7, -0x54

    aput v7, v1, v6

    const/16 v6, 0xa7c

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const/16 v1, 0x11

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, -0x7bf2

    aput v22, v2, v21

    const/16 v21, -0x3f

    aput v21, v2, v20

    const/16 v20, -0xa

    aput v20, v2, v19

    const/16 v19, -0x2

    aput v19, v2, v18

    const/16 v18, -0x73

    aput v18, v2, v17

    const/16 v17, -0x7f

    aput v17, v2, v16

    const/16 v16, -0x6f

    aput v16, v2, v15

    const/16 v15, -0x5686

    aput v15, v2, v14

    const/4 v14, -0x7

    aput v14, v2, v13

    const/16 v13, -0x77

    aput v13, v2, v12

    const/16 v12, -0x7bf

    aput v12, v2, v11

    const/16 v11, -0x41

    aput v11, v2, v10

    const/16 v10, -0x2c

    aput v10, v2, v9

    const/4 v9, -0x7

    aput v9, v2, v8

    const/16 v8, -0x7f6

    aput v8, v2, v7

    const/16 v7, -0x49

    aput v7, v2, v3

    const/16 v3, -0x3f

    aput v3, v2, v1

    const/16 v1, 0x11

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, -0x7ba6

    aput v23, v1, v22

    const/16 v22, -0x7c

    aput v22, v1, v21

    const/16 v21, -0x4f

    aput v21, v1, v20

    const/16 v20, -0x5f

    aput v20, v1, v19

    const/16 v19, -0x3d

    aput v19, v1, v18

    const/16 v18, -0x32

    aput v18, v1, v17

    const/16 v17, -0x28

    aput v17, v1, v16

    const/16 v16, -0x56d2

    aput v16, v1, v15

    const/16 v15, -0x57

    aput v15, v1, v14

    const/16 v14, -0x3a

    aput v14, v1, v13

    const/16 v13, -0x7e2

    aput v13, v1, v12

    const/4 v12, -0x8

    aput v12, v1, v11

    const/16 v11, -0x63

    aput v11, v1, v10

    const/16 v10, -0x41

    aput v10, v1, v9

    const/16 v9, -0x7bc

    aput v9, v1, v8

    const/4 v8, -0x8

    aput v8, v1, v7

    const/16 v7, -0x7e

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v7, v1

    if-lt v3, v7, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v7, v1

    if-lt v3, v7, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v5, v6, v1, v2, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/16 v8, -0x4f

    aput v8, v2, v7

    const/16 v7, -0x4d

    aput v7, v2, v6

    const/4 v6, -0x6

    aput v6, v2, v5

    const/16 v5, -0x6f

    aput v5, v2, v3

    const/16 v3, -0x77

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x2c

    aput v9, v1, v8

    const/16 v8, -0x3a

    aput v8, v1, v7

    const/16 v7, -0x6a

    aput v7, v1, v6

    const/16 v6, -0x10

    aput v6, v1, v5

    const/4 v5, -0x1

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_a
    return-object v1

    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_a

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9
.end method

.method public saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 24

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/16 v6, -0x5d

    aput v6, v2, v5

    const/16 v5, -0x47

    aput v5, v2, v3

    const/16 v3, -0x60

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, -0x26

    aput v7, v1, v6

    const/16 v6, -0x24

    aput v6, v1, v5

    const/16 v5, -0x35

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/16 v8, -0x71ef

    aput v8, v2, v7

    const/4 v7, -0x5

    aput v7, v2, v6

    const/16 v6, -0x7cdd

    aput v6, v2, v5

    const/16 v5, -0x1e

    aput v5, v2, v3

    const/16 v3, -0x4a

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x718c

    aput v9, v1, v8

    const/16 v8, -0x72

    aput v8, v1, v7

    const/16 v7, -0x7cb1

    aput v7, v1, v6

    const/16 v6, -0x7d

    aput v6, v1, v5

    const/16 v5, -0x40

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move/from16 v0, p3

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const/16 v1, 0x11

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, -0x35

    aput v22, v2, v21

    const/16 v21, -0x22

    aput v21, v2, v20

    const/16 v20, -0xc

    aput v20, v2, v19

    const/16 v19, 0x5966

    aput v19, v2, v18

    const/16 v18, -0x45e9

    aput v18, v2, v17

    const/16 v17, -0xb

    aput v17, v2, v16

    const/16 v16, -0x3c4

    aput v16, v2, v15

    const/16 v15, -0x58

    aput v15, v2, v14

    const/16 v14, -0xf

    aput v14, v2, v13

    const/16 v13, -0x6a

    aput v13, v2, v12

    const/16 v12, 0x125a

    aput v12, v2, v11

    const/16 v11, 0x455

    aput v11, v2, v10

    const/16 v10, 0x174d

    aput v10, v2, v9

    const/16 v9, 0x1651

    aput v9, v2, v8

    const/16 v8, 0x4658

    aput v8, v2, v7

    const/16 v7, -0x3f7

    aput v7, v2, v3

    const/16 v3, -0x41

    aput v3, v2, v1

    const/16 v1, 0x11

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, -0x61

    aput v23, v1, v22

    const/16 v22, -0x75

    aput v22, v1, v21

    const/16 v21, -0x5c

    aput v21, v1, v20

    const/16 v20, 0x5939

    aput v20, v1, v19

    const/16 v19, -0x45a7

    aput v19, v1, v18

    const/16 v18, -0x46

    aput v18, v1, v17

    const/16 v17, -0x38b

    aput v17, v1, v16

    const/16 v16, -0x4

    aput v16, v1, v15

    const/16 v15, -0x5f

    aput v15, v1, v14

    const/16 v14, -0x27

    aput v14, v1, v13

    const/16 v13, 0x1205

    aput v13, v1, v12

    const/16 v12, 0x412

    aput v12, v1, v11

    const/16 v11, 0x1704

    aput v11, v1, v10

    const/16 v10, 0x1617

    aput v10, v1, v9

    const/16 v9, 0x4616

    aput v9, v1, v8

    const/16 v8, -0x3ba

    aput v8, v1, v7

    const/4 v7, -0x4

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v7, v1

    if-lt v3, v7, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v7, v1

    if-lt v3, v7, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v5, v6, v1, v2, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    :goto_6
    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :catch_0
    move-exception v1

    goto :goto_6
.end method

.method public saveStringValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 24

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x3

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/16 v6, 0x2409

    aput v6, v2, v5

    const/16 v5, 0x4641

    aput v5, v2, v3

    const/16 v3, -0x41d3

    aput v3, v2, v1

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, 0x2470

    aput v7, v1, v6

    const/16 v6, 0x4624

    aput v6, v1, v5

    const/16 v5, -0x41ba

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, -0x6

    aput v8, v2, v7

    const/16 v7, 0x64e

    aput v7, v2, v6

    const/16 v6, -0x6396

    aput v6, v2, v5

    const/4 v5, -0x3

    aput v5, v2, v3

    const/16 v3, 0x2c45

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x61

    aput v9, v1, v8

    const/16 v8, 0x63b

    aput v8, v1, v7

    const/16 v7, -0x63fa

    aput v7, v1, v6

    const/16 v6, -0x64

    aput v6, v1, v5

    const/16 v5, 0x2c33

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const/16 v1, 0x11

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, -0x1b

    aput v22, v2, v21

    const/16 v21, -0x7be

    aput v21, v2, v20

    const/16 v20, -0x58

    aput v20, v2, v19

    const/16 v19, 0x1a1f

    aput v19, v2, v18

    const/16 v18, -0x7dac

    aput v18, v2, v17

    const/16 v17, -0x33

    aput v17, v2, v16

    const/16 v16, -0x70

    aput v16, v2, v15

    const/16 v15, -0x19a5

    aput v15, v2, v14

    const/16 v14, -0x4a

    aput v14, v2, v13

    const/16 v13, -0x77

    aput v13, v2, v12

    const/16 v12, 0x6b02

    aput v12, v2, v11

    const/16 v11, 0x6f2c

    aput v11, v2, v10

    const/16 v10, -0x3fda

    aput v10, v2, v9

    const/16 v9, -0x7a

    aput v9, v2, v8

    const/16 v8, -0x58e

    aput v8, v2, v7

    const/16 v7, -0x4b

    aput v7, v2, v3

    const/4 v3, -0x2

    aput v3, v2, v1

    const/16 v1, 0x11

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, -0x4f

    aput v23, v1, v22

    const/16 v22, -0x7e9

    aput v22, v1, v21

    const/16 v21, -0x8

    aput v21, v1, v20

    const/16 v20, 0x1a40

    aput v20, v1, v19

    const/16 v19, -0x7de6

    aput v19, v1, v18

    const/16 v18, -0x7e

    aput v18, v1, v17

    const/16 v17, -0x27

    aput v17, v1, v16

    const/16 v16, -0x19f1

    aput v16, v1, v15

    const/16 v15, -0x1a

    aput v15, v1, v14

    const/16 v14, -0x3a

    aput v14, v1, v13

    const/16 v13, 0x6b5d

    aput v13, v1, v12

    const/16 v12, 0x6f6b

    aput v12, v1, v11

    const/16 v11, -0x3f91

    aput v11, v1, v10

    const/16 v10, -0x40

    aput v10, v1, v9

    const/16 v9, -0x5c4

    aput v9, v1, v8

    const/4 v8, -0x6

    aput v8, v1, v7

    const/16 v7, -0x43

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v7, v1

    if-lt v3, v7, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v7, v1

    if-lt v3, v7, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v5, v6, v1, v2, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    :goto_6
    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :catch_0
    move-exception v1

    goto :goto_6
.end method
