.class public Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "WeightGraphFragmentSic.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic$1;
    }
.end annotation


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final HOURS_IN_DAY:B = 0x18t

.field private static MARKING_COUNT:I = 0x0

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MINUTE:I = 0xea60

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field private static final PATTERN_24_HANDLER:Ljava/lang/String; = "HH:mm"

.field private static final PATTERN_24_HOURS:Ljava/lang/String; = "HH"

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static volatile mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# instance fields
.field private density:F

.field private mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field private mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 117
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->MARKING_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    return-void
.end method

.method public static getStartHourToMillis(J)J
    .locals 3
    .param p0, "time"    # J

    .prologue
    const/4 v2, 0x0

    .line 476
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 477
    .local v0, "tempCalendar":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 478
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 479
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 480
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 481
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method


# virtual methods
.method protected createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 13
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x0

    .line 429
    new-instance v12, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-direct {v12}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;-><init>()V

    .line 430
    .local v12, "weightDataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 431
    .local v1, "weightContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 432
    .local v2, "weightExtractContract":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;>;"
    const/4 v4, 0x0

    .line 433
    .local v4, "countWeight":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 434
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 441
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->WEIGHT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    sget-object v3, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->AVG_WEIGHT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    invoke-virtual {p0, v0, v3, p1}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getAvgDaoContract(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;

    move-result-object v11

    .line 443
    .local v11, "avgDaoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    sget-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 444
    invoke-interface {v1, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 445
    invoke-interface {v2, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 447
    sget-object v3, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    move-object v0, p0

    move-object v6, p1

    move-object v8, v7

    move-object v9, p0

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getDataSeriesList(Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;I)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 453
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_3

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v12, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 465
    :goto_1
    return-object v12

    .line 435
    .end local v11    # "avgDaoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    .line 436
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->COUNT_WEIGHT:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    .line 437
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 439
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Illegal period given: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    .restart local v11    # "avgDaoContract":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    :cond_3
    sget-object v3, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    move-object v0, p0

    move-object v6, p1

    move-object v8, v7

    move-object v9, p0

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getTimeSeriesList(Ljava/util/Set;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IValueConversionProvider;I)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v12, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addXYTimeSeriesList(Ljava/util/List;)V

    goto :goto_1
.end method

.method protected customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V
    .locals 4
    .param p1, "interaction"    # Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 339
    const/high16 v0, 0x40e00000    # 7.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 340
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 341
    invoke-virtual {p1, v2, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalLimit(II)V

    .line 342
    invoke-virtual {p1, v2, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 347
    :goto_0
    return-void

    .line 344
    :cond_0
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 345
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_0
.end method

.method protected customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 4
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/4 v3, 0x0

    .line 160
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->density:F

    .line 162
    const/high16 v1, 0x41800000    # 16.0f

    iget v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v3, v3, v3, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 163
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    .line 164
    .local v0, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 165
    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 166
    return-void
.end method

.method protected customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 4
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 403
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->density:F

    .line 406
    const/4 v1, 0x0

    .line 407
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_1

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 410
    const/high16 v2, 0x426c0000    # 59.0f

    iget v3, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 413
    const-string v2, "HH:mm"

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 414
    const-string v2, "HH"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 421
    :goto_0
    if-eqz v1, :cond_0

    .line 423
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 424
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 426
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-void

    .line 416
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 418
    const/high16 v2, 0x41f80000    # 31.0f

    iget v3, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0
.end method

.method protected getAvgDaoContract(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .locals 1
    .param p1, "baseContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p2, "avgContract"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;
    .param p3, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 199
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getAvgDaoContract(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;

    move-result-object v0

    return-object v0
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFloat10MaxValue(F)F
    .locals 3
    .param p1, "maxY"    # F

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 388
    div-float/2addr p1, v2

    .line 389
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float p1, v0

    .line 390
    mul-float/2addr p1, v2

    .line 392
    return p1
.end method

.method public getFloat10MinValue(F)F
    .locals 3
    .param p1, "minY"    # F

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 380
    div-float/2addr p1, v2

    .line 381
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float p1, v0

    .line 382
    mul-float/2addr p1, v2

    .line 384
    return p1
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 187
    const v0, 0x7f0208fb

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    const/4 v1, 0x0

    const v2, 0x7f0203bb

    const v3, 0x7f09002c

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->createDrawabledLegendMark(II)Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 204
    const v0, 0x7f020709

    return v0
.end method

.method public getValueInCurrentUnits(F)F
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v0

    return v0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 128
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 129
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 3

    .prologue
    .line 470
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphLegendArea()Landroid/view/View;

    move-result-object v0

    .line 471
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f080643

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 472
    return-object v0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 1
    .param p1, "periodStart"    # J
    .param p3, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 300
    iput-object p3, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 301
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 302
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 5
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v2, 0x0

    .line 170
    new-instance v3, Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq p3, v1, :cond_0

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-direct {v3, v4, v1, p3}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;-><init>(Landroid/content/Context;ZLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v2, p3}, Lcom/sec/android/app/shealth/logutils/graph/data/GraphInformationItemFactory;->getGraphInformationItem(Landroid/content/Context;ILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;

    move-result-object v0

    .line 174
    .local v0, "weightItem":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setUnit(Ljava/lang/String;)V

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setTitle(Ljava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setColor(I)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c95

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;->setValueFormat(Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;->addInformationItem(Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;->hideInformationItemsTitle()V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    invoke-virtual {v1, p3}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    return-object v1

    .end local v0    # "weightItem":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    :cond_1
    move v1, v2

    .line 170
    goto :goto_0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 12
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    const v9, 0x7f070030

    const/4 v11, 0x1

    .line 351
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 355
    .local v0, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->getLatestGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v6

    .line 356
    .local v6, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-eqz v6, :cond_0

    sget-object v2, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v4, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->PRELOAD:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 357
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v7

    .line 358
    .local v7, "goalValueInActualUnits":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v2, v0, v7, v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->setGoal(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;FI)V

    .line 360
    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineVisible(Z)V

    .line 366
    .end local v7    # "goalValueInActualUnits":F
    :goto_0
    const v2, 0x7f0203c0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 367
    .local v1, "normalBitmap":Landroid/graphics/Bitmap;
    const v2, 0x7f0203c1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v3

    .local v3, "handlerBitmap":Landroid/graphics/Bitmap;
    move-object v2, v1

    move-object v4, v3

    move-object v5, v3

    .line 369
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->setSeriesValueMarking(Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0003

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 374
    const-wide v4, 0x3feccccccccccccdL    # 0.9

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getMinDataValue()D

    move-result-wide v9

    double-to-float v2, v9

    float-to-double v9, v2

    mul-double/2addr v4, v9

    double-to-float v8, v4

    .line 375
    .local v8, "minY":F
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getFloat10MinValue(F)F

    move-result v2

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->getMaxDataValue()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getChartHeight(F)F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getFloat10MaxValue(F)F

    move-result v4

    invoke-virtual {v0, v11, v11, v2, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 376
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 377
    return-void

    .line 362
    .end local v1    # "normalBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "handlerBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "minY":F
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineVisible(Z)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onAttach(Landroid/app/Activity;)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.weight"

    const-string v2, "WT05"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    .line 123
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    .line 124
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "WEIGHT_CHARTTAB"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 140
    .local v0, "mPeriod":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 142
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 147
    :goto_0
    return-void

    .line 143
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 144
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0

    .line 146
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 295
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDestroyView()V

    .line 296
    return-void
.end method

.method public savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 307
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_1

    .line 308
    const-string v1, "WEIGHT_CHARTTAB"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 314
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 315
    return-void

    .line 309
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_2

    .line 310
    const-string v1, "WEIGHT_CHARTTAB"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 311
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    .line 312
    const-string v1, "WEIGHT_CHARTTAB"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 13
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    .line 214
    const/4 v1, 0x0

    .line 215
    .local v1, "level":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_3

    .line 216
    const/4 v1, 0x0

    .line 223
    :cond_0
    :goto_0
    const-wide/16 v9, 0x0

    .line 224
    .local v9, "selectedTime":J
    sget-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_5

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v9

    .line 229
    :goto_1
    const-wide/16 v7, 0x0

    .line 230
    .local v7, "dataToShow":J
    const-wide/16 v11, 0x0

    .line 231
    .local v11, "startTime":J
    sget v5, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->MARKING_COUNT:I

    .line 232
    .local v5, "markingCount":I
    const/4 v4, 0x1

    .line 233
    .local v4, "intervel":I
    sget-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic$1;->$SwitchMap$com$sec$android$app$shealth$framework$ui$graph$PeriodH:[I

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 265
    const-string v0, "%Y-%m"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 267
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 268
    :cond_1
    const-wide v2, 0x59cce4400L

    sub-long v11, v7, v2

    .line 269
    const/16 v5, 0xc

    .line 272
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 274
    .local v6, "calendar":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    cmp-long v0, v7, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v0, v2, :cond_2

    .line 276
    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 277
    const/16 v0, 0xb

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 278
    const/16 v0, 0xc

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 279
    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 280
    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 281
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 283
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sput-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 284
    long-to-double v2, v11

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 285
    long-to-double v2, v7

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 289
    return-void

    .line 217
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v7    # "dataToShow":J
    .end local v9    # "selectedTime":J
    .end local v11    # "startTime":J
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_4

    .line 218
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 219
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_0

    .line 220
    const/4 v1, 0x5

    goto/16 :goto_0

    .line 227
    .restart local v9    # "selectedTime":J
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphInformationAreaView;->getSelectedDateInChart()J

    move-result-wide v9

    :goto_3
    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v9

    goto :goto_3

    .line 237
    .restart local v4    # "intervel":I
    .restart local v5    # "markingCount":I
    .restart local v7    # "dataToShow":J
    .restart local v11    # "startTime":J
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_8

    .line 238
    const-string v0, "%Y-%m-%d"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 245
    :goto_4
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 246
    :cond_7
    const-wide/32 v2, 0x2160ec0

    sub-long v2, v7, v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->getStartHourToMillis(J)J

    move-result-wide v11

    .line 247
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setScrollRangeDepthLevel(I)V

    .line 248
    const/16 v4, 0x3c

    .line 249
    const/16 v5, 0xd

    .line 250
    goto/16 :goto_2

    .line 241
    :cond_8
    sget-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_9

    const-string v0, "%Y-%m-%d"

    :goto_5
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    goto :goto_4

    :cond_9
    const-string v0, "%Y-%m"

    goto :goto_5

    .line 252
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_b

    .line 253
    const-string v0, "%Y-%m-%d"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 259
    :goto_6
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 260
    :cond_a
    const-wide/32 v2, 0x19bfcc00

    sub-long v11, v7, v2

    .line 261
    const/4 v5, 0x7

    .line 262
    goto/16 :goto_2

    .line 257
    :cond_b
    sget-object v0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_c

    const-string v0, "%Y-%m-%d"

    :goto_7
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    goto :goto_6

    :cond_c
    const-string v0, "%Y-%m"

    goto :goto_7

    .line 233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 6
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v3, 0x0

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    sget-object v5, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;->MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/weight/graph/WeightGraphFragmentSic;->updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/util/List;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V

    .line 398
    return-void
.end method
