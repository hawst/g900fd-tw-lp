.class public Lcom/sec/android/app/shealth/uv/log/UvLogActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "UvLogActivity.java"


# static fields
.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field public static SELECTED_FILTER_INDEX:I


# instance fields
.field private mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

.field private mUvLogAdapter:Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

.field private mUvObserver:Landroid/database/ContentObserver;

.field private menu:Landroid/view/Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->SELECTED_FILTER_INDEX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 35
    new-instance v0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity$1;-><init>(Lcom/sec/android/app/shealth/uv/log/UvLogActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/uv/log/UvLogActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/uv/log/UvLogActivity;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/uv/log/UvLogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogActivity;

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->refreshAdapter()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/uv/log/UvLogActivity;)Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/log/UvLogActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvLogAdapter:Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

    return-object v0
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 129
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 2

    .prologue
    .line 86
    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    const-string v1, "0==0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getGroupCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mCursor:Landroid/database/Cursor;

    .line 88
    new-instance v0, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mCursor:Landroid/database/Cursor;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvLogAdapter:Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvLogAdapter:Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

    return-object v0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    const-string v0, "comment"

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x0

    .line 106
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 107
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    const-string/jumbo v7, "uv_index"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->generateState(I)I

    move-result v3

    .line 108
    .local v3, "state":I
    const-string/jumbo v6, "sample_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 109
    .local v4, "time":J
    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 110
    new-instance v6, Ljava/text/DateFormatSymbols;

    invoke-direct {v6}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v6}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x7

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    aget-object v2, v6, v7

    .line 111
    .local v2, "dname":Ljava/lang/String;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "dd/MM/yyyy hh:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "date":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x5

    invoke-virtual {v1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v2, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") | "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0900d2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v1, 0x7f090076

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 140
    sput p1, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->SELECTED_FILTER_INDEX:I

    .line 141
    const-string v0, "0==0"

    return-object v0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 3

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.uv"

    const-string v2, "UV08"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvLogAdapter:Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getShareDataByIds(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 118
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "comment":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 157
    packed-switch p1, :pswitch_data_0

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 159
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvLogAdapter:Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->notifyDataSetChanged()V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvLogAdapter:Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/log/UvLogAdapter;->getTotalChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->menu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->showNoDataView()V

    goto :goto_0

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x45b
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->registerObserver()V

    .line 58
    const v0, 0x7f0205b7

    const v1, 0x7f09006a

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->setNoLogImageAndText(II)V

    .line 59
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->unregisterObserver()V

    .line 64
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onDestroy()V

    .line 65
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f080c8b

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->menu:Landroid/view/Menu;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 72
    invoke-static {p0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDataCount()I

    move-result v2

    if-gtz v2, :cond_0

    .line 73
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    move v0, v1

    .line 81
    :goto_0
    return v0

    .line 76
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 78
    .local v0, "result":Z
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 79
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09110b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 80
    const v1, 0x7f080c8c

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public registerObserver()V
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 48
    return-void
.end method

.method protected setUpSelectMode()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setUpSelectMode()V

    .line 180
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 99
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/uv/log/UvLogDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "HEART_RATE_ID_KEY"

    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const/16 v1, 0x45b

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 102
    return-void
.end method

.method public unregisterObserver()V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 52
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "_id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->mUvDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z

    .line 125
    return-void
.end method
