.class Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$8;
.super Ljava/lang/Object;
.source "WalkingMateDayStepService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0

    .prologue
    .line 1362
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1365
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->lock:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$2000()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 1366
    :try_start_0
    const-string v4, "PEDOSTART"

    const-string/jumbo v6, "onStart()"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1369
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isSensorHubSupported()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1371
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getExerciseId()J

    move-result-wide v2

    .line 1372
    .local v2, "exerciseId":J
    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-gez v4, :cond_0

    .line 1373
    const-string v4, "WalkingMateDayStepService"

    const-string v6, "it\'s failed to get exercise id."

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    monitor-exit v5

    .line 1397
    .end local v2    # "exerciseId":J
    :goto_0
    return-void

    .line 1377
    .restart local v2    # "exerciseId":J
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMatePedometerManager;->registerPedometerListener(J)Z

    .line 1378
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 1380
    .local v0, "deviceType":I
    const/16 v4, 0x2719

    if-ne v0, v4, :cond_1

    .line 1381
    const-string v4, "PEDOSTART"

    const-string v6, "Healthy Step, Active time start"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setActiveTime()V

    .line 1383
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setHealthyStep()V

    .line 1384
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v4

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 1385
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sendCurrentStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1390
    .end local v0    # "deviceType":I
    .end local v2    # "exerciseId":J
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    # invokes: Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateLockScreenWidget(Landroid/content/Context;)V
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->access$1900(Landroid/content/Context;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1394
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    .line 1396
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 1391
    :catch_0
    move-exception v1

    .line 1392
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
