.class final Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;
.super Landroid/os/AsyncTask;
.source "WeightUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->runQuery(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$mSelectionArgs:[Ljava/lang/String;

.field final synthetic val$periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

.field final synthetic val$projection:[Ljava/lang/String;

.field final synthetic val$responseListener:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;

.field final synthetic val$selectionClause:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;)V
    .locals 0

    .prologue
    .line 489
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$projection:[Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$selectionClause:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$mSelectionArgs:[Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    iput-object p6, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$responseListener:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 489
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 493
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 494
    .local v11, "weightDataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v6, 0x0

    .line 497
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$projection:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$selectionClause:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$mSelectionArgs:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 498
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 500
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 502
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-double v9, v0

    .line 503
    .local v9, "value":D
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 504
    .local v7, "time":J
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne v0, v1, :cond_2

    .line 505
    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getStartOfMonth(J)J

    move-result-wide v7

    .line 508
    :cond_0
    :goto_1
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$ChartResponseData;

    invoke-direct {v0, v9, v10, v7, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$ChartResponseData;-><init>(DJ)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 516
    .end local v7    # "time":J
    .end local v9    # "value":D
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 518
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 519
    const/4 v6, 0x0

    :cond_1
    throw v0

    .line 506
    .restart local v7    # "time":J
    .restart local v9    # "value":D
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$periodType:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    if-ne v0, v1, :cond_0

    .line 507
    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils;->getStartOfDay(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v7

    goto :goto_1

    .line 516
    .end local v7    # "time":J
    .end local v9    # "value":D
    :cond_3
    if-eqz v6, :cond_4

    .line 518
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 519
    const/4 v6, 0x0

    .line 523
    :cond_4
    return-object v11
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 489
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 530
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/WeightUtils$1;->val$responseListener:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$IOnDBDataFetchListener;->onDBdataReceived(Ljava/util/List;)V

    .line 531
    return-void
.end method
