.class Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$3;
.super Ljava/lang/Object;
.source "SpO2DeviceConnector.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getDataListener()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 412
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDevice.DataListener onReceived 1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    const/4 v1, 0x1

    new-array v0, v1, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    .line 415
    .local v0, "h":[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 416
    const/16 v1, 0x11

    if-ne p1, v1, :cond_0

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$500(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;->onDataReceivedArray([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;)V

    .line 419
    :cond_0
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 423
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDevice.DataListener onReceived 2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    const/16 v0, 0x11

    if-ne p1, v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$3;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$500(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;->onDataReceivedArray([Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;)V

    .line 427
    :cond_0
    return-void
.end method

.method public onStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 402
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    return-void
.end method

.method public onStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 407
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStopped : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    return-void
.end method
