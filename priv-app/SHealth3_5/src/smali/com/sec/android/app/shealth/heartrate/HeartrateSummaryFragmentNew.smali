.class public Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$28;,
        Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;,
        Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceConnChkListner;,
        Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;
    }
.end annotation


# static fields
.field private static final DELAYTIME_CHECK_SENSOR_DETECT:I = 0x96

.field private static HRM_FINISH_ANIMATION_Y_AXIS:F

.field private static final MEASURE_FAIL_MAX_COUNT:I

.field public static final TAG:Ljava/lang/String;

.field private static effectAudio:[I

.field private static mHeartrateState:I


# instance fields
.field animationFadeIn:Landroid/view/animation/AnimationSet;

.field animationFadeOut:Landroid/view/animation/AnimationSet;

.field private errorImages:[I

.field private errorImagesArray:[Ljava/lang/String;

.field fadeIn:Landroid/view/animation/Animation;

.field fadeOut:Landroid/view/animation/Animation;

.field private heartRateId:J

.field iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private isErrorByTimeout:Z

.field private isFingerDetected:Z

.field isImage1Found:Z

.field isImage2Found:Z

.field isReady:Z

.field private isRetryMeasuring:Z

.field private mAccessaryConnected:Landroid/widget/TextView;

.field private mAnimationViewErr0:Landroid/widget/ImageView;

.field private mAnimationViewErr1:Landroid/widget/ImageView;

.field private mBarInidcator:Landroid/widget/FrameLayout;

.field private mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

.field private mContext:Landroid/content/Context;

.field public mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCurrDateTxt:Landroid/widget/TextView;

.field private mDeviceConnectedStatusView:Landroid/view/View;

.field private mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

.field private mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

.field private mDeviceSupportCoverSDK:Z

.field private mDiscardButton:Landroid/widget/LinearLayout;

.field private mDiscardText:Landroid/widget/TextView;

.field private mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mErrorDialogMsg1:Landroid/widget/TextView;

.field private mErrorDialogMsg2:Landroid/widget/TextView;

.field private mErrorDialogMsg3:Landroid/widget/TextView;

.field private mErrorDialogMsg4:Landroid/widget/TextView;

.field private mErrorType:I

.field private mErrorView:Landroid/view/View;

.field private mFirstLayout:Landroid/widget/LinearLayout;

.field private mFirstMessage:Landroid/widget/TextView;

.field public mFirstStatus:Landroid/widget/TextView;

.field private final mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

.field protected mHeartRate:I

.field private mHearterateObserverForWithoutSensor:Landroid/database/ContentObserver;

.field private mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

.field private mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

.field private mHighPercentile:Landroid/widget/TextView;

.field private mIsGreen:Z

.field private mIsOnConfigChanged:Z

.field private mIsRunPulseView:Z

.field private mIsWinAttached:Z

.field mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

.field private mLowPercentile:Landroid/widget/TextView;

.field private mMeasureFailCount:I

.field private mNoSensorData:Landroid/widget/TextView;

.field private mNoSensorGuide:Landroid/widget/TextView;

.field private mNonSensorDateLayout:Landroid/widget/LinearLayout;

.field private mNonSensorSecondLayout:Landroid/widget/LinearLayout;

.field private mNonSensorThirdLayout:Landroid/widget/LinearLayout;

.field private mPaused:Z

.field private mPrevError:I

.field private mPrevTagIcon:Landroid/widget/ImageButton;

.field private mPreviousHRMLayout:Landroid/widget/LinearLayout;

.field private mScaleAnimation:Landroid/view/animation/Animation;

.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field private mSecondBpm:Landroid/widget/TextView;

.field private mSecondBpmLayout:Landroid/widget/LinearLayout;

.field private mSecondBpmUnit:Landroid/widget/TextView;

.field private mSecondCenterIcon:Landroid/widget/ImageView;

.field private mSecondCenterIconBig:Landroid/widget/ImageView;

.field private mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

.field private mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

.field private mSecondIcon:Landroid/widget/RelativeLayout;

.field private mSecondLayout:Landroid/widget/FrameLayout;

.field private mSecondRetry:Landroid/widget/LinearLayout;

.field private mSelectedTag:Landroid/widget/TextView;

.field private mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;

.field private mServiceConnected:Z

.field private mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private mStartButton:Landroid/widget/TextView;

.field private mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

.field private mStateBarType2:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;

.field public mStateMeasuringEndUI:Z

.field private mTagAssociated:Z

.field private mTagIcon:Landroid/widget/ImageButton;

.field private mTagIconListener:Landroid/view/View$OnClickListener;

.field private mTagLayout:Landroid/widget/RelativeLayout;

.field private mTagText:Ljava/lang/String;

.field private mThirdBpm:Landroid/widget/TextView;

.field private mThirdBpmUnit:Landroid/widget/TextView;

.field private mThirdDate:Landroid/widget/TextView;

.field private mThirdGraph:Landroid/widget/ImageButton;

.field private mThirdPreviousLayout:Landroid/widget/LinearLayout;

.field private needsReadyUI:Z

.field private pool:Landroid/media/SoundPool;

.field private pulseAnimationID:[I

.field private readyTimer:Landroid/os/CountDownTimer;

.field private sendedStartLogging:Z

.field private sensorHandler:Landroid/os/Handler;

.field private sensorRunnable:Ljava/lang/Runnable;

.field private wasShowInformationDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    const-class v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    .line 124
    const/high16 v0, -0x40c00000    # -0.75f

    sput v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->HRM_FINISH_ANIMATION_Y_AXIS:F

    .line 138
    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    .line 222
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 128
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    .line 129
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isRetryMeasuring:Z

    .line 130
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsRunPulseView:Z

    .line 131
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    .line 132
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsOnConfigChanged:Z

    .line 133
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsWinAttached:Z

    .line 135
    iput v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mMeasureFailCount:I

    .line 146
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    .line 149
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    .line 183
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;

    .line 185
    new-array v0, v9, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pulseAnimationID:[I

    .line 198
    new-array v0, v10, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    .line 199
    new-array v0, v10, [Ljava/lang/String;

    const-string v1, "hr_fail_t_1"

    aput-object v1, v0, v6

    const-string v1, "hr_fail_t_2"

    aput-object v1, v0, v8

    const-string v1, "hr_fail_t_3"

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImagesArray:[Ljava/lang/String;

    .line 200
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage1Found:Z

    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage2Found:Z

    .line 206
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    .line 208
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isReady:Z

    .line 210
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mServiceConnected:Z

    .line 216
    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    .line 217
    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    .line 218
    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPreviousHRMLayout:Landroid/widget/LinearLayout;

    .line 220
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagAssociated:Z

    .line 221
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->wasShowInformationDialog:Z

    .line 223
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sendedStartLogging:Z

    .line 224
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isErrorByTimeout:Z

    .line 225
    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .line 226
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPaused:Z

    .line 271
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    invoke-direct {v0, p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    .line 1102
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$9;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    .line 1688
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$15;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$15;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHearterateObserverForWithoutSensor:Landroid/database/ContentObserver;

    .line 1805
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$17;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;

    .line 2208
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$25;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIconListener:Landroid/view/View$OnClickListener;

    .line 2216
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->needsReadyUI:Z

    return-void

    .line 185
    :array_0
    .array-data 4
        0x7f0201f5
        0x7f020202
    .end array-data

    .line 198
    :array_1
    .array-data 4
        0x7f0201f2
        0x7f0201f3
        0x7f0201f4
    .end array-data
.end method

.method private HRMmeasuringFinishScaleAnimation()V
    .locals 2

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v0, :cond_0

    .line 1323
    :goto_0
    return-void

    .line 1276
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$13;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 121
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevError:I

    return p1
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    return-object v0
.end method

.method static synthetic access$102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 121
    sput p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I

    return p0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mServiceConnected:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mServiceConnected:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setReadyUI(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showGraphFragment()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2800()[I
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->playSound(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringUI(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3700()F
    .locals 1

    .prologue
    .line 121
    sget v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->HRM_FINISH_ANIMATION_Y_AXIS:F

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Z

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pulseAnimationShow(Z)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/view/animation/Animation;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringFailUI(I)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isRetryMeasuring:Z

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Z

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->changeUIReadyToMeasuring(Z)V

    return-void
.end method

.method static synthetic access$4302(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isErrorByTimeout:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showErrorDialog(I)V

    return-void
.end method

.method static synthetic access$4600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4802(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg1:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$5002(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg2:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg3:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5202(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg4:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initDialogAnimation()V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPaused:Z

    return v0
.end method

.method static synthetic access$5500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # [I
    .param p4, "x4"    # [Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # Z

    .prologue
    .line 121
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$5600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setHRMStateBar(I)V

    return-void
.end method

.method static synthetic access$5800(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorType:I

    return v0
.end method

.method static synthetic access$5900(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startTagChooser()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V

    return-void
.end method

.method static synthetic access$6000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->launchMoreTagActivity()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sensorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sensorHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "imageView1"    # Landroid/widget/ImageView;
    .param p2, "imageView2"    # Landroid/widget/ImageView;
    .param p3, "images"    # [I
    .param p4, "ImageArr"    # [Ljava/lang/String;
    .param p5, "imageIndex"    # I
    .param p6, "forever"    # Z

    .prologue
    .line 2010
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initFadingAnimationEffect(I)V

    .line 2011
    invoke-virtual {p1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 2012
    .local v9, "ImgTag1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 2013
    .local v10, "ImgTag2":Ljava/lang/String;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage1Found:Z

    .line 2014
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage2Found:Z

    .line 2015
    aget-object v1, p4, p5

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2016
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage1Found:Z

    .line 2017
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2018
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2019
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v1, v0, :cond_2

    .line 2020
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2021
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 2026
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2027
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2029
    :cond_0
    aget-object v1, p4, p5

    invoke-virtual {v10, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2030
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isImage2Found:Z

    .line 2031
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2032
    invoke-virtual {p2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2033
    array-length v1, p3

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p5

    if-ne v0, v1, :cond_3

    .line 2035
    const/4 v1, 0x0

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2036
    const/4 v1, 0x0

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 2041
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeOut:Landroid/view/animation/AnimationSet;

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2042
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2045
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeIn:Landroid/view/animation/AnimationSet;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;

    invoke-direct {v2, p0, p2, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$21;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2065
    iget-object v11, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeOut:Landroid/view/animation/AnimationSet;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$22;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move/from16 v6, p5

    move-object/from16 v7, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$22;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/widget/ImageView;Landroid/widget/ImageView;[II[Ljava/lang/String;Z)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2092
    return-void

    .line 2023
    :cond_2
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2024
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2038
    :cond_3
    add-int/lit8 v1, p5, 0x1

    aget v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2039
    add-int/lit8 v1, p5, 0x1

    aget-object v1, p4, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private changeUIReadyToMeasuring(Z)V
    .locals 5
    .param p1, "isFail"    # Z

    .prologue
    .line 2143
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeReadyToMeasuringUI - isFail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2146
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v2}, Landroid/os/CountDownTimer;->cancel()V

    .line 2148
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;

    if-eqz v2, :cond_0

    .line 2149
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$SensorErrorTimer;->cancel()V

    .line 2153
    :cond_0
    if-eqz p1, :cond_1

    .line 2154
    const/4 v0, 0x0

    .line 2158
    .local v0, "failMsg":I
    :goto_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 2159
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 2160
    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 2161
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->sendMessage(Landroid/os/Message;)Z

    .line 2162
    return-void

    .line 2156
    .end local v0    # "failMsg":I
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "failMsg":I
    goto :goto_0
.end method

.method private checkCircleViewStatus()V
    .locals 2

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_1

    .line 1551
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->getIsRun()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1552
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->startAnimation(Z)V

    .line 1554
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->changeColor(Z)V

    .line 1556
    :cond_1
    return-void
.end method

.method private checkRunPulseView(Z)V
    .locals 2
    .param p1, "isShowScaleAnimator"    # Z

    .prologue
    .line 1576
    if-eqz p1, :cond_0

    .line 1577
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->scaleAnimationStarter(Z)V

    .line 1582
    :goto_0
    return-void

    .line 1579
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1580
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showHRMPulseAnimation(Z)V

    goto :goto_0
.end method

.method private convertDptoPx(F)I
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 2316
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2317
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2318
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 2319
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method private firstAnimation(Landroid/view/View;I)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "stringID"    # I

    .prologue
    .line 1186
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v0, :cond_0

    .line 1215
    :goto_0
    return-void

    .line 1189
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$11;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$11;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private initAoudioFile()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1710
    new-instance v0, Landroid/media/SoundPool;

    invoke-direct {v0, v5, v6, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    .line 1711
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const v3, 0x7f06002c

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v4

    .line 1712
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const v3, 0x7f060027

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v5

    .line 1713
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const v4, 0x7f06002d

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 1714
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const v3, 0x7f060028

    invoke-virtual {v1, v2, v3, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    aput v1, v0, v6

    .line 1715
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const v4, 0x7f06002a

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v0, v1

    .line 1716
    return-void
.end method

.method private initConnectionListener()V
    .locals 1

    .prologue
    .line 487
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$3;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 498
    return-void
.end method

.method private initDeviceFinder()V
    .locals 3

    .prologue
    .line 437
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnectedStatusView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDeviceConnectionCheckListener(Landroid/view/View;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    move-result-object v0

    .line 438
    .local v0, "listener":Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$2;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$2;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    .line 484
    return-void
.end method

.method private initDialogAnimation()V
    .locals 3

    .prologue
    .line 1949
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1950
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1951
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;

    const v1, 0x7f080555

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    .line 1952
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorView:Landroid/view/View;

    const v1, 0x7f080556

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    .line 1953
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1954
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1957
    :cond_0
    return-void
.end method

.method private initFadingAnimationEffect(I)V
    .locals 7
    .param p1, "imageIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1985
    const/16 v0, 0x1f4

    .line 1986
    .local v0, "fadeInDuration":I
    const/16 v2, 0x5dc

    .line 1987
    .local v2, "timeBetween":I
    const/16 v1, 0x1f4

    .line 1989
    .local v1, "fadeOutDuration":I
    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    const/4 v3, 0x5

    if-eq p1, v3, :cond_0

    const/4 v3, 0x6

    if-ne p1, v3, :cond_1

    .line 1990
    :cond_0
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeIn:Landroid/view/animation/Animation;

    .line 1991
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeOut:Landroid/view/animation/Animation;

    .line 1997
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeIn:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1998
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeIn:Landroid/view/animation/Animation;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1999
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeIn:Landroid/view/animation/Animation;

    const/16 v4, 0x514

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 2001
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeOut:Landroid/view/animation/Animation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2002
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 2003
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeOut:Landroid/view/animation/Animation;

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2004
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeIn:Landroid/view/animation/AnimationSet;

    .line 2005
    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeOut:Landroid/view/animation/AnimationSet;

    .line 2006
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeIn:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2007
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animationFadeOut:Landroid/view/animation/AnimationSet;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2008
    return-void

    .line 1993
    :cond_1
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeIn:Landroid/view/animation/Animation;

    .line 1994
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->fadeOut:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method private initMeasureStatus()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2165
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    .line 2166
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    .line 2167
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mMeasureFailCount:I

    .line 2168
    return-void
.end method

.method private initSCover()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 309
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 311
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 318
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initSCover - mDeviceSupportCoverSDK : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    if-eqz v1, :cond_0

    .line 321
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 322
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$1;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 352
    :cond_0
    return-void

    .line 312
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    goto :goto_0

    .line 314
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 315
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    goto :goto_0
.end method

.method private initView(Landroid/view/View;)Landroid/view/View;
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v7, 0x7f09020b

    const v6, 0x7f09020a

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 365
    const v0, 0x7f0805ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    .line 366
    const v0, 0x7f0805cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    .line 367
    const v0, 0x7f0805da

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    .line 368
    const v0, 0x7f0805eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBarType2:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;

    .line 369
    const v0, 0x7f0804fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    .line 370
    const v0, 0x7f0804ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mLowPercentile:Landroid/widget/TextView;

    .line 371
    const v0, 0x7f080500

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHighPercentile:Landroid/widget/TextView;

    .line 372
    const v0, 0x7f0805e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    .line 373
    const v0, 0x7f0805d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    .line 375
    const v0, 0x7f0805d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorSecondLayout:Landroid/widget/LinearLayout;

    .line 376
    const v0, 0x7f0805d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNoSensorData:Landroid/widget/TextView;

    .line 377
    const v0, 0x7f0805bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    .line 378
    const v0, 0x7f080595

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    .line 379
    const v0, 0x7f0805d8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    .line 380
    const v0, 0x7f0805c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    .line 381
    const v0, 0x7f0805c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    .line 382
    const v0, 0x7f0805c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pulseAnimationID:[I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setMultiMovieResource([IZ)V

    .line 384
    const v0, 0x7f0805db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagLayout:Landroid/widget/RelativeLayout;

    .line 386
    const v0, 0x7f0805c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    .line 387
    const v0, 0x7f0805be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    .line 388
    const v0, 0x7f0805bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    .line 389
    const v0, 0x7f0805ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAccessaryConnected:Landroid/widget/TextView;

    .line 390
    const v0, 0x7f0805c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    .line 391
    const v0, 0x7f0805e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090f4d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 395
    const v0, 0x7f0805dd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    .line 396
    const v0, 0x7f0805de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    .line 397
    const v0, 0x7f0805f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStartButton:Landroid/widget/TextView;

    .line 398
    const v0, 0x7f0805d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardText:Landroid/widget/TextView;

    .line 400
    const v0, 0x7f0805e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    .line 401
    const v0, 0x7f0805e0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNoSensorGuide:Landroid/widget/TextView;

    .line 402
    const v0, 0x7f0805e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCurrDateTxt:Landroid/widget/TextView;

    .line 403
    const v0, 0x7f0805df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorThirdLayout:Landroid/widget/LinearLayout;

    .line 404
    const v0, 0x7f0805e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorDateLayout:Landroid/widget/LinearLayout;

    .line 405
    const v0, 0x7f0805e7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPreviousHRMLayout:Landroid/widget/LinearLayout;

    .line 406
    const v0, 0x7f0805ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdBpmUnit:Landroid/widget/TextView;

    .line 407
    const v0, 0x7f0805e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdBpm:Landroid/widget/TextView;

    .line 408
    const v0, 0x7f0805ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    .line 409
    const v0, 0x7f0805ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    .line 410
    const v0, 0x7f0805ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnectedStatusView:Landroid/view/View;

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPreviousHRMLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateOnClcickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 422
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setGesturesEnabled(Z)V

    .line 425
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 426
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initConnectionListener()V

    .line 427
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 428
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initDeviceFinder()V

    .line 429
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    .line 431
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v0, v1, :cond_0

    .line 432
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initViewForNonMedicalDA(Landroid/view/View;)V

    .line 433
    :cond_0
    return-object p1
.end method

.method private initViewForNonMedicalDA(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 2296
    const v3, -0x40d9999a    # -0.65f

    sput v3, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->HRM_FINISH_ANIMATION_Y_AXIS:F

    .line 2297
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2298
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPreviousHRMLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2299
    .local v1, "prevLayout":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2301
    .local v2, "seondIconLayout":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v3, 0x41d00000    # 26.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->convertDptoPx(F)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2302
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2304
    const/high16 v3, 0x42860000    # 67.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->convertDptoPx(F)I

    move-result v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2305
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->convertDptoPx(F)I

    move-result v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2306
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2307
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagLayout:Landroid/widget/RelativeLayout;

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->convertDptoPx(F)I

    move-result v4

    invoke-virtual {v3, v5, v4, v5, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 2309
    const/high16 v3, 0x41400000    # 12.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->convertDptoPx(F)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2310
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPreviousHRMLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2311
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    const/high16 v4, 0x41100000    # 9.0f

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->convertDptoPx(F)I

    move-result v4

    invoke-virtual {v3, v5, v4, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2312
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBarType2:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->setVisibility(I)V

    .line 2313
    return-void
.end method

.method private isToday(J)Z
    .locals 4
    .param p1, "timeMills"    # J

    .prologue
    .line 741
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchMoreTagActivity()V
    .locals 3

    .prologue
    .line 2420
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v1, :cond_0

    .line 2425
    :goto_0
    return-void

    .line 2423
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const-class v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateMoreTagActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2424
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const/16 v2, 0x94

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private measureFinishAnimation()V
    .locals 2

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v0, :cond_0

    .line 1270
    :goto_0
    return-void

    .line 1221
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$12;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private playSound(I)V
    .locals 3
    .param p1, "effectAudio"    # I

    .prologue
    .line 1719
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v1, :cond_1

    .line 1739
    :cond_0
    :goto_0
    return-void

    .line 1722
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1724
    .local v0, "aManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1727
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$16;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$16;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private pulseAnimationShow(Z)V
    .locals 2
    .param p1, "isGreen"    # Z

    .prologue
    .line 2202
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2203
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2204
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2205
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showHRMPulseAnimation(Z)V

    .line 2206
    return-void
.end method

.method private scaleAnimationStarter(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    .line 1444
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "scaleAnimationStarter - isGreen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1445
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 1447
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1448
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 1449
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$14;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$14;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1478
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1482
    :goto_0
    return-void

    .line 1480
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020526

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f020529

    goto :goto_1
.end method

.method private setFinishedSpannableText()V
    .locals 13

    .prologue
    .line 2323
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    iget-object v9, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMaleOrFemale()I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v4

    .line 2325
    .local v4, "percentile":[I
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v9, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090f7e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    aget v12, v4, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const/4 v12, 0x1

    aget v12, v4, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2326
    .local v0, "avgRestingRate1":Ljava/lang/String;
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v9, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090f92

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    aget v12, v4, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const/4 v12, 0x1

    aget v12, v4, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2328
    .local v1, "avgRestingRate2":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09090b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2329
    .local v3, "mMoreString":Ljava/lang/String;
    new-instance v6, Landroid/text/SpannableString;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2330
    .local v6, "ss1":Landroid/text/SpannableString;
    new-instance v7, Landroid/text/SpannableString;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2332
    .local v7, "ss2":Landroid/text/SpannableString;
    new-instance v5, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$26;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$26;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    .line 2347
    .local v5, "span2":Landroid/text/style/ClickableSpan;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x1

    const/16 v10, 0x21

    invoke-virtual {v6, v5, v8, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2348
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x1

    const/16 v10, 0x21

    invoke-virtual {v7, v5, v8, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2350
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2351
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v2

    .line 2352
    .local v2, "heartrateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 2353
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    .line 2356
    .end local v2    # "heartrateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    :cond_0
    iget v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    const/4 v9, 0x0

    aget v9, v4, v9

    if-lt v8, v9, :cond_3

    iget v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    const/4 v9, 0x1

    aget v9, v4, v9

    if-gt v8, v9, :cond_3

    .line 2357
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTextForFirstMessageBySpannableString(Landroid/text/SpannableString;)V

    .line 2362
    :cond_1
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2363
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    .line 2365
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090043

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2367
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2368
    return-void

    .line 2358
    :cond_3
    iget v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    const/4 v9, 0x0

    aget v9, v4, v9

    if-lt v8, v9, :cond_4

    iget v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    const/4 v9, 0x1

    aget v9, v4, v9

    if-le v8, v9, :cond_1

    .line 2359
    :cond_4
    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTextForFirstMessageBySpannableString(Landroid/text/SpannableString;)V

    goto :goto_0
.end method

.method private setHRMStateBar(I)V
    .locals 6
    .param p1, "bpm"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2261
    new-array v0, v5, [I

    aput v4, v0, v4

    .line 2262
    .local v0, "avgRange":[I
    new-array v2, v5, [I

    aput v4, v2, v4

    .line 2263
    .local v2, "percentile":[I
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserGender()I

    move-result v1

    .line 2265
    .local v1, "gender":I
    const v3, 0x2e636

    if-ne v1, v3, :cond_0

    .line 2266
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v3, v5, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile_5thAnd95th(II)[I

    move-result-object v0

    .line 2267
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v3, v5, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v2

    .line 2273
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    invoke-virtual {v3, p1, v0, v2}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->moveToPolygon(I[I[I)V

    .line 2275
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mLowPercentile:Landroid/widget/TextView;

    aget v4, v2, v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2276
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHighPercentile:Landroid/widget/TextView;

    aget v4, v2, v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2277
    return-void

    .line 2269
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v3, v4, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile_5thAnd95th(II)[I

    move-result-object v0

    .line 2270
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v3, v4, v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v2

    goto :goto_0
.end method

.method private setHRMStateBarType2(IJ)V
    .locals 7
    .param p1, "bpm"    # I
    .param p2, "sampletime"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2280
    new-array v0, v6, [I

    aput v5, v0, v5

    .line 2281
    .local v0, "avgRange":[I
    new-array v2, v6, [I

    aput v5, v2, v5

    .line 2282
    .local v2, "percentile":[I
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserGender()I

    move-result v1

    .line 2283
    .local v1, "gender":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4, p2, p3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserAge(J)I

    move-result v3

    .line 2285
    .local v3, "userage":I
    const v4, 0x2e636

    if-ne v1, v4, :cond_0

    .line 2286
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4, v6, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile_5thAnd95th(II)[I

    move-result-object v0

    .line 2287
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4, v6, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v2

    .line 2292
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBarType2:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;

    invoke-virtual {v4, p1, v0, v2}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBarType2;->moveToPolygon(I[I[I)V

    .line 2293
    return-void

    .line 2289
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile_5thAnd95th(II)[I

    move-result-object v0

    .line 2290
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getRestingPulsePercentile(II)[I

    move-result-object v2

    goto :goto_0
.end method

.method private setMeasuringFailUI(I)V
    .locals 6
    .param p1, "errorType"    # I

    .prologue
    const v5, 0x7f090c24

    const v4, 0x7f090c04

    const/4 v3, 0x0

    .line 1485
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isReady:Z

    .line 1486
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringFailUI - errorType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1488
    const/4 v0, -0x6

    if-ne p1, v0, :cond_2

    .line 1489
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v0, :cond_0

    .line 1490
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1493
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v0, :cond_1

    .line 1494
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->playSound(I)V

    .line 1495
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->showErrorDialog(I)V

    .line 1529
    :cond_1
    :goto_0
    return-void

    .line 1501
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    .line 1502
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsRunPulseView:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->checkRunPulseView(Z)V

    .line 1503
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsRunPulseView:Z

    .line 1505
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->checkCircleViewStatus()V

    .line 1508
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 1509
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 1510
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1511
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTextForFirstMessageById(I)V

    .line 1512
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1516
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1517
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020524

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1518
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v1, 0x7f020529

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1519
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1524
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1526
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorType:I

    .line 1527
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING_ERROR:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    goto/16 :goto_0
.end method

.method private setMeasuringUI(I)V
    .locals 8
    .param p1, "isShowAnimation"    # I

    .prologue
    const/4 v7, 0x1

    const v6, 0x7f090c23

    const v5, 0x7f090c04

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1392
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMeasuringUI - isShowAnimation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sendedStartLogging:Z

    if-nez v0, :cond_0

    .line 1394
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.shealth.heartrate"

    const-string v2, "HR01"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sendedStartLogging:Z

    .line 1399
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isReady:Z

    .line 1400
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1401
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1402
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v0, :cond_1

    .line 1403
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1405
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1406
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1408
    iput-boolean v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    .line 1409
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsRunPulseView:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->checkRunPulseView(Z)V

    .line 1410
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsRunPulseView:Z

    .line 1412
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 1414
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->checkCircleViewStatus()V

    .line 1416
    if-nez p1, :cond_2

    .line 1417
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v0, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1418
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v0, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1431
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1432
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1433
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1434
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1435
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1436
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1437
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 1438
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1439
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1440
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1441
    return-void

    .line 1420
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1421
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1422
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 1423
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 1424
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1426
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTextForFirstMessageById(I)V

    .line 1427
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1428
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method private setReadyUI(I)V
    .locals 9
    .param p1, "animation"    # I

    .prologue
    const v8, 0x7f090c22

    const v7, 0x7f090c05

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 1349
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setReadyUI - animation : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1350
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isReady:Z

    .line 1351
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsRunPulseView:Z

    .line 1352
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1353
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1354
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1355
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1356
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    const v2, 0x7f020527

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1357
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1358
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1359
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1361
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v2, 0x41b80000    # 23.0f

    invoke-virtual {v1, v6, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1363
    if-nez p1, :cond_0

    .line 1364
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-direct {p0, v1, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1365
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->firstAnimation(Landroid/view/View;I)V

    .line 1377
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f020522

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1378
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1379
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1380
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1381
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1382
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1383
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 1384
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1386
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1387
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1388
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->READY:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 1389
    return-void

    .line 1367
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1368
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1370
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1371
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1372
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setTextForFirstMessageById(I)V

    .line 1373
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1374
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method private setTextForFirstMessageById(I)V
    .locals 4
    .param p1, "resource"    # I

    .prologue
    const/4 v3, 0x1

    .line 1326
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1327
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_0

    .line 1328
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1334
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1335
    return-void

    .line 1329
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x23

    if-le v1, v2, :cond_1

    .line 1330
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v2, 0x41980000    # 19.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1332
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v2, 0x41b80000    # 23.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method private setTextForFirstMessageBySpannableString(Landroid/text/SpannableString;)V
    .locals 3
    .param p1, "text"    # Landroid/text/SpannableString;

    .prologue
    const/4 v2, 0x1

    .line 1338
    invoke-virtual {p1}, Landroid/text/SpannableString;->length()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 1339
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1345
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346
    return-void

    .line 1340
    :cond_0
    invoke-virtual {p1}, Landroid/text/SpannableString;->length()I

    move-result v0

    const/16 v1, 0x23

    if-le v0, v1, :cond_1

    .line 1341
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v1, 0x41980000    # 19.0f

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 1343
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/high16 v1, 0x41b80000    # 23.0f

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method private showErrorDialog(I)V
    .locals 7
    .param p1, "errorType"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 1862
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v1, :cond_1

    .line 1946
    :cond_0
    :goto_0
    return-void

    .line 1865
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "showErrorDialog error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1866
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isErrorByTimeout:Z

    if-nez v1, :cond_4

    .line 1867
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.app.shealth.heartrate"

    const-string v3, "HR05"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1871
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1872
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1873
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1874
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f020522

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1876
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopHRMPulseAnimation()V

    .line 1878
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v1, :cond_2

    .line 1879
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 1882
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1899
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1900
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1901
    const v1, 0x7f0907ae

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1902
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1903
    const v1, 0x7f030143

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$18;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1920
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$19;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1930
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$20;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1942
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1943
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startDialogAnimation()V

    .line 1944
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsWinAttached:Z

    if-eqz v1, :cond_0

    .line 1945
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hrm_error_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1869
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_4
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isErrorByTimeout:Z

    goto/16 :goto_1
.end method

.method private showGraphFragment()V
    .locals 4

    .prologue
    .line 1178
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v1, :cond_0

    .line 1179
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->switchFragmentToGraph()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1183
    :cond_0
    return-void

    .line 1180
    :catch_0
    move-exception v0

    .line 1181
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be instance of HeartrateActivity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private showHRMPulseAnimation(Z)V
    .locals 3
    .param p1, "isGreen"    # Z

    .prologue
    const/16 v2, 0x8

    .line 1532
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    if-eqz v0, :cond_0

    .line 1533
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1534
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->clearAnimation()V

    .line 1535
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsGreen:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setPulseAnimationColor(Z)V

    .line 1536
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 1537
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setVisibility(I)V

    .line 1539
    :cond_0
    return-void
.end method

.method private startTagChooser()V
    .locals 7

    .prologue
    const v5, 0x7f090c0d

    .line 2383
    const/4 v2, 0x0

    .line 2384
    .local v2, "tagText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    .line 2385
    .local v1, "isAddTag":Z
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2386
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    .line 2390
    :goto_1
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$27;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    .line 2413
    .local v0, "dialogItemClick":Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    if-nez v3, :cond_0

    .line 2414
    new-instance v3, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const v6, 0x7f030145

    invoke-direct {v3, v4, v5, v6, v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentActivity;ILcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    .line 2416
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->showChooserDialog(ZLjava/lang/String;)V

    .line 2417
    return-void

    .line 2384
    .end local v0    # "dialogItemClick":Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;
    .end local v1    # "isAddTag":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2388
    .restart local v1    # "isAddTag":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V
    .locals 2
    .param p1, "timerType"    # Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    .prologue
    .line 2171
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v0, :cond_0

    .line 2199
    :goto_0
    return-void

    .line 2174
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$24;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$24;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private stopHRMPulseAnimation()V
    .locals 2

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    if-eqz v0, :cond_0

    .line 1543
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setVisibility(I)V

    .line 1544
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->clearAnimation()V

    .line 1545
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->stopAnimation()V

    .line 1547
    :cond_0
    return-void
.end method

.method private stopSensorAndAnimationInitialize()V
    .locals 2

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    if-eqz v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->stopMeasuring()V

    .line 1159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    if-eqz v0, :cond_1

    .line 1161
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->stopCountDownTimer()V

    .line 1163
    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$10;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1174
    return-void
.end method

.method private updateSummaryStringOnLocaleChange()V
    .locals 2

    .prologue
    const v1, 0x7f0900d2

    .line 2372
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2373
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2374
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAccessaryConnected:Landroid/widget/TextView;

    const v1, 0x7f090060

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2375
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    if-eqz v0, :cond_0

    .line 2376
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mListDialog:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->updateStringOnLocaleChange()V

    .line 2377
    :cond_0
    return-void
.end method


# virtual methods
.method public activateListners()V
    .locals 2

    .prologue
    .line 2256
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIconListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2257
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIconListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2258
    return-void
.end method

.method public deleteDailyData()V
    .locals 3

    .prologue
    .line 1706
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->deleteDailyDataByTime(J)Z

    .line 1707
    return-void
.end method

.method public displayChooserDialog()V
    .locals 0

    .prologue
    .line 2428
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startTagChooser()V

    .line 2429
    return-void
.end method

.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 761
    const-class v0, Lcom/sec/android/app/shealth/heartrate/calendar/HeartrateCalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 751
    const-string/jumbo v0, "start_time"

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 746
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 356
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v2, "getContentView"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 358
    new-instance v1, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateData:Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    .line 359
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 360
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03014a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1

    .prologue
    .line 766
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method protected getDeviceConnectionCheckListener(Landroid/view/View;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    .locals 1
    .param p1, "deviceConnectedStatusView"    # Landroid/view/View;

    .prologue
    .line 504
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceConnChkListner;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceConnChkListner;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public getmHeartrateState()I
    .locals 1

    .prologue
    .line 2379
    sget v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 275
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onAttach(Landroid/app/Activity;)V

    .line 276
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onAttach"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    check-cast p1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .line 278
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsWinAttached:Z

    .line 279
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mHeartrateActivity = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 282
    .local v0, "model":Ljava/lang/String;
    const-string v1, "SM-G850F"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850S"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850A"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850L"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850W"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G850FQ"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImagesArray:[Ljava/lang/String;

    const-string v2, "hr_fail_alpha_1"

    aput-object v2, v1, v5

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImagesArray:[Ljava/lang/String;

    const-string v2, "hr_fail_alpha_2"

    aput-object v2, v1, v4

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImagesArray:[Ljava/lang/String;

    const-string v2, "hr_fail_alpha_3"

    aput-object v2, v1, v6

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    const v2, 0x7f0201ef

    aput v2, v1, v5

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    const v2, 0x7f0201f0

    aput v2, v1, v4

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    const v2, 0x7f0201f1

    aput v2, v1, v6

    .line 294
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 533
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 534
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeartrateSummaryFragmentNew onConfigurationChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Landroid/content/res/Configuration;->userSetLocale:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->summaryFragContainer:Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/gesture/GestureDetectorLinearLayout;->setGesturesEnabled(Z)V

    .line 538
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 539
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setVisibility(I)V

    .line 542
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsOnConfigChanged:Z

    .line 543
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_2

    .line 544
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->updateSummaryStringOnLocaleChange()V

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->updateErrorDialogStringOnLocaleChange()V

    .line 547
    :cond_2
    return-void
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;)V
    .locals 8
    .param p1, "hrm"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1743
    iget v1, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRate:I

    .line 1745
    .local v1, "rate":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-nez v2, :cond_1

    .line 1746
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndAnimationInitialize()V

    .line 1788
    :cond_0
    :goto_0
    return-void

    .line 1750
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ShealthSensorDevice.DataListener onDataReceived - isDetected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1751
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    .line 1752
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    if-eqz v2, :cond_4

    .line 1753
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isRetryMeasuring:Z

    .line 1754
    if-eq v1, v5, :cond_0

    .line 1757
    if-gez v1, :cond_2

    const/16 v2, -0x9

    if-lt v1, v2, :cond_2

    .line 1758
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    .line 1760
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1761
    .local v0, "msg":Landroid/os/Message;
    iput v7, v0, Landroid/os/Message;->what:I

    .line 1762
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 1763
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1765
    .end local v0    # "msg":Landroid/os/Message;
    :cond_2
    if-lez v1, :cond_0

    .line 1766
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.shealth.heartrate"

    const-string v4, "HR02"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1767
    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    .line 1768
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v2, v3, :cond_3

    .line 1769
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndUpdateUI(I)V

    .line 1770
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->measureFinishAnimation()V

    .line 1774
    :goto_1
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->effectAudio:[I

    const/4 v3, 0x4

    aget v2, v2, v3

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->playSound(I)V

    goto :goto_0

    .line 1772
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->HRMmeasuringFinishScaleAnimation()V

    goto :goto_1

    .line 1777
    :cond_4
    if-ne v1, v5, :cond_0

    .line 1778
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    .line 1779
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isRetryMeasuring:Z

    if-eqz v2, :cond_5

    .line 1780
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;->MEASURING:Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/heartrate/data/HeartrateTimerType;)V

    .line 1783
    :cond_5
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevError:I

    if-ne v2, v7, :cond_0

    .line 1784
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isRetryMeasuring:Z

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->changeUIReadyToMeasuring(Z)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 692
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 693
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->clearListener()V

    .line 696
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 699
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mServiceConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->releaseAnimation()V

    .line 703
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    .line 705
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 706
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->clearListener()V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->release()V

    .line 665
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->clear()V

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPreviousHRMLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 680
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->iDeviceFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_1

    .line 683
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 684
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->releaseAnimation()V

    .line 685
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    .line 687
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroyView()V

    .line 688
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 299
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDetach()V

    .line 300
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDetach"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsWinAttached:Z

    .line 302
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 621
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPaused:Z

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->unregisterObserverForWithoutSensor()V

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initDialogAnimation()V

    .line 627
    :cond_1
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndUpdateUI(I)V

    .line 628
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceSupportCoverSDK:Z

    if-eqz v0, :cond_2

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 632
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopHRMPulseAnimation()V

    .line 634
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 635
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 637
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_3

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 642
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    if-eqz v0, :cond_4

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 644
    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->pool:Landroid/media/SoundPool;

    .line 646
    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onPause()V

    .line 647
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 551
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 553
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPaused:Z

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->registerObserverForWithoutSensor()V

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->updateDeviceConnectionStatus()V

    .line 558
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initAoudioFile()V

    .line 559
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initSCover()V

    .line 560
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 561
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsRunPulseView:Z

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-eqz v0, :cond_1

    .line 575
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAutoStartUp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dataBaseCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isMeasureCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsOnConfigChanged:Z

    if-eqz v0, :cond_2

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c0a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090f19

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdGraph:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 587
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->sendEmptyMessage(I)Z

    .line 617
    :cond_3
    :goto_0
    return-void

    .line 591
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 593
    :cond_5
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->wasShowInformationDialog:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getCallFromHome()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "heartrate_warning_checked"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_6

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->callShowInformationDialog()V

    .line 595
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->wasShowInformationDialog:Z

    goto :goto_0

    .line 596
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->needsReadyUI:Z

    if-eqz v0, :cond_3

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getShowDeleteDialog()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isAutoStartup:Z

    if-eqz v0, :cond_3

    .line 598
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsOnConfigChanged:Z

    if-eqz v0, :cond_7

    .line 599
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mIsOnConfigChanged:Z

    .line 601
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 602
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startDialogAnimation()V

    goto :goto_0

    .line 604
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-nez v0, :cond_9

    .line 605
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_0

    .line 607
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_a

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 609
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_0

    .line 611
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 756
    const-string v0, "Date"

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 757
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 655
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 658
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onStop()V

    .line 659
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 6

    .prologue
    .line 711
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 712
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 713
    .local v0, "sytemChangedTime":J
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->selectedDate:Ljava/util/Date;

    .line 714
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 715
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->updateBpmDataView(Z)V

    .line 717
    .end local v0    # "sytemChangedTime":J
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1802
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CountDownTimer onTick: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1803
    return-void
.end method

.method public onTimeout()V
    .locals 2

    .prologue
    .line 1792
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    if-lez v0, :cond_0

    .line 1798
    :goto_0
    return-void

    .line 1795
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onTimeout"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1796
    const/4 v0, -0x6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndUpdateUI(I)V

    goto :goto_0
.end method

.method public registerObserverForWithoutSensor()V
    .locals 4

    .prologue
    .line 1698
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHearterateObserverForWithoutSensor:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1699
    return-void
.end method

.method public setMeasuringEndUI()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const v6, 0x7f090c06

    const/4 v5, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 1585
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setMeasuringEndUI"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1587
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v1, "View is not initialized!, return"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1668
    :goto_0
    return-void

    .line 1591
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_1

    .line 1592
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 1594
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isReady:Z

    .line 1595
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090f19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    .line 1597
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1598
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopHRMPulseAnimation()V

    .line 1599
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorSecondLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1600
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorThirdLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1601
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNoSensorData:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1602
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNoSensorGuide:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1603
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorDateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1604
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCurrDateTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1606
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v0, :cond_2

    .line 1607
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1608
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-eqz v0, :cond_2

    .line 1610
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1611
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1612
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIconBig:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1613
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1614
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1615
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1616
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIconListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1617
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIconListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1618
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    .line 1622
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f020522

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 1624
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 1625
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1626
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1627
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->activateListners()V

    .line 1635
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1636
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1637
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1638
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1639
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1640
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1641
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondRetry:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1642
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1643
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1645
    :cond_4
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v0, v1, :cond_9

    .line 1646
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 1647
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1648
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1649
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1659
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1660
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStartButton:Landroid/widget/TextView;

    const v1, 0x7f090050

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1661
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDiscardText:Landroid/widget/TextView;

    const v1, 0x7f090f4d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1662
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->updateBpmDataView(Z)V

    .line 1665
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    .line 1666
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sendedStartLogging:Z

    goto/16 :goto_0

    .line 1629
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v7, :cond_8

    .line 1630
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1632
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1633
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 1651
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setFinishedSpannableText()V

    .line 1652
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 1653
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1654
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1655
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1656
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "     "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public setNeedsReadyUI(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 2433
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->needsReadyUI:Z

    .line 2434
    return-void
.end method

.method protected setNextAndPrevDates()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 721
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getPreviousDateOfData(J)Ljava/util/Date;

    move-result-object v1

    .line 722
    .local v1, "prevdate":Ljava/util/Date;
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getNextDateOfData(J)Ljava/util/Date;

    move-result-object v0

    .line 724
    .local v0, "nextdate":Ljava/util/Date;
    if-eqz v1, :cond_0

    .line 725
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    .line 729
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevDate(Ljava/util/Date;)V

    .line 731
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 732
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 733
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 738
    .end local v0    # "nextdate":Ljava/util/Date;
    :goto_1
    return-void

    .line 727
    .restart local v0    # "nextdate":Ljava/util/Date;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    goto :goto_0

    .line 735
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 736
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v2

    if-eqz v0, :cond_2

    .end local v0    # "nextdate":Ljava/util/Date;
    :goto_2
    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    goto :goto_1

    .restart local v0    # "nextdate":Ljava/util/Date;
    :cond_2
    new-instance v0, Ljava/util/Date;

    .end local v0    # "nextdate":Ljava/util/Date;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    goto :goto_2
.end method

.method public setNonSensorUI()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 1671
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 1672
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1673
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1674
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1675
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1676
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1677
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1678
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorSecondLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1679
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNoSensorData:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1680
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorThirdLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1681
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNoSensorGuide:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1682
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNoSensorData:Landroid/widget/TextView;

    const v1, 0x7f090f17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1683
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1684
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorDateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1685
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCurrDateTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1686
    return-void
.end method

.method public setTag(I)V
    .locals 6
    .param p1, "tagId"    # I

    .prologue
    .line 2219
    const/4 v0, 0x0

    .line 2220
    .local v0, "tagItem":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-eqz p1, :cond_0

    .line 2221
    invoke-static {p1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v0

    .line 2223
    :cond_0
    if-eqz v0, :cond_1

    .line 2224
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    iget v2, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2225
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    iget v2, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    .line 2226
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const v2, 0x7f020209

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 2228
    iget v1, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    const v2, 0x7f02053d

    if-ne v1, v2, :cond_2

    .line 2229
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const v2, 0x7f02051d

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2234
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->updateRecentTagPreference(ILandroid/content/Context;)V

    .line 2235
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagAssociated:Z

    .line 2236
    const-string/jumbo v1, "updateTagFromHeartRateLogById"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-wide v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->heartRateId:J

    invoke-virtual {v3, v4, v5, p1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->updateTagFromHeartRateLogById(JI)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2237
    return-void

    .line 2232
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    iget v2, v0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method public setTag(Ljava/lang/String;II)V
    .locals 6
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "tagId"    # I
    .param p3, "tagIconId"    # I

    .prologue
    const v2, 0x7f090c0d

    .line 2240
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2241
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2243
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2244
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    .line 2245
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-static {p2, v0}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->updateRecentTagPreference(ILandroid/content/Context;)V

    .line 2246
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const v1, 0x7f020209

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 2248
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-static {p3}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconResourceId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2249
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2250
    const p3, 0x7f0204e6

    .line 2251
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagAssociated:Z

    .line 2252
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->heartRateId:J

    move v3, p2

    move-object v4, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->updateTagFromHeartRateLogById(JILjava/lang/String;I)Z

    .line 2253
    return-void
.end method

.method public startDialogAnimation()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1975
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1976
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    aget v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1977
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1978
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImagesArray:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1980
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr0:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mAnimationViewErr1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImages:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->errorImagesArray:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V

    .line 1982
    :cond_0
    return-void
.end method

.method protected startDrawerView()V
    .locals 2

    .prologue
    .line 1559
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getmHeartrateState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getmHeartrateState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1560
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_1

    .line 1561
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;->stopAnimation(Z)V

    .line 1564
    :cond_1
    return-void
.end method

.method public startSensorAndDelayUIForDetectedFinger(I)V
    .locals 6
    .param p1, "animation"    # I

    .prologue
    const/4 v5, 0x1

    .line 770
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v2, :cond_0

    .line 818
    :goto_0
    return-void

    .line 773
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startSensorAndDelayUIForDetectedFinger - animation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initMeasureStatus()V

    .line 776
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    if-nez v2, :cond_1

    .line 777
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 778
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, p0, v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    .line 784
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$4;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 799
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    const v3, 0x7f090f19

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 801
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    .line 802
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$5;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    .line 816
    .local v0, "task":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 817
    .local v1, "timer":Ljava/util/Timer;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 780
    .end local v0    # "task":Ljava/util/TimerTask;
    .end local v1    # "timer":Ljava/util/Timer;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, p0, v5}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    goto :goto_1
.end method

.method public startSensorforSCover(I)V
    .locals 6
    .param p1, "animation"    # I

    .prologue
    .line 822
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v2, :cond_0

    .line 877
    :goto_0
    return-void

    .line 825
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startSensorforSCover - animation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->initMeasureStatus()V

    .line 829
    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sensorHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 843
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$7;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 858
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    const v3, 0x7f090f19

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 860
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    .line 861
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$8;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$8;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    .line 875
    .local v0, "task":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 876
    .local v1, "timer":Ljava/util/Timer;
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method protected stopDrawerView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1567
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getmHeartrateState()I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getmHeartrateState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1568
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircle;

    if-eqz v0, :cond_1

    .line 1569
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->isFingerDetected:Z

    if-eqz v0, :cond_1

    .line 1570
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringUI(I)V

    .line 1573
    :cond_1
    return-void
.end method

.method public stopSensorAndUpdateUI(I)V
    .locals 5
    .param p1, "heartRate"    # I

    .prologue
    const/4 v4, 0x4

    .line 2096
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-nez v1, :cond_1

    .line 2140
    :cond_0
    :goto_0
    return-void

    .line 2099
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopSensorAndUpdateUI rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " RemainFailCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mMeasureFailCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2101
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2103
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mMeasureFailCount:I

    if-lez v1, :cond_2

    .line 2104
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mMeasureFailCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mMeasureFailCount:I

    goto :goto_0

    .line 2108
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndAnimationInitialize()V

    .line 2110
    if-nez p1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-nez v1, :cond_5

    .line 2111
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogshown:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateMeasuringEndUI:Z

    if-nez v1, :cond_4

    .line 2112
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2113
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2117
    :cond_5
    if-ltz p1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    if-nez v1, :cond_6

    .line 2118
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->sendEmptyMessage(I)Z

    .line 2121
    if-lez p1, :cond_0

    .line 2122
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isMeasureCompleted:Z

    .line 2123
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    new-instance v2, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2133
    :cond_6
    if-gez p1, :cond_0

    .line 2134
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 2135
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2136
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 2137
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHandler:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method

.method public unregisterObserverForWithoutSensor()V
    .locals 2

    .prologue
    .line 1702
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHearterateObserverForWithoutSensor:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1703
    return-void
.end method

.method public updateBpmDataView(Z)V
    .locals 18
    .param p1, "isMeasuring"    # Z

    .prologue
    .line 882
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    if-nez v12, :cond_0

    .line 883
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 886
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getLastTwoDataByTime(J)Ljava/util/ArrayList;

    move-result-object v5

    .line 888
    .local v5, "heartrateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    if-eqz v5, :cond_6

    if-eqz p1, :cond_6

    .line 889
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_2

    .line 890
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1098
    :cond_1
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setNextAndPrevDates()V

    .line 1100
    return-void

    .line 892
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 893
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdBpm:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 894
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v16

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v16

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 896
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v12, "yyyy/MM/dd"

    invoke-direct {v4, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 897
    .local v4, "format":Ljava/text/SimpleDateFormat;
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 898
    .local v2, "date":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 899
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v15

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 900
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0907af

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 901
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v13

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getSampleTime()J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14, v15}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setHRMStateBarType2(IJ)V

    .line 903
    const/4 v10, 0x0

    .line 904
    .local v10, "tagiconIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTag(J)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    .line 905
    .local v7, "prevtagIndexHrId":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-eqz v7, :cond_3

    .line 906
    iget v10, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 908
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f02051c

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 909
    if-eqz v10, :cond_4

    const v12, 0x7f02053d

    if-eq v10, v12, :cond_4

    .line 910
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v12, v10}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 915
    :goto_1
    if-eqz v7, :cond_1

    .line 916
    iget-object v12, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    if-eqz v12, :cond_5

    iget-object v12, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_5

    .line 917
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    iget-object v13, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 913
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f02051d

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    .line 920
    :cond_5
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    iget v14, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 922
    :catch_0
    move-exception v3

    .line 924
    .local v3, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v12, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v13, "invalid string Id recieved"

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 930
    .end local v2    # "date":Ljava/lang/String;
    .end local v3    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v4    # "format":Ljava/text/SimpleDateFormat;
    .end local v7    # "prevtagIndexHrId":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v10    # "tagiconIndex":I
    :cond_6
    if-eqz v5, :cond_9

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_9

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    if-nez v12, :cond_9

    .line 931
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 932
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 933
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 934
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 935
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 936
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 937
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1073
    :cond_7
    :goto_2
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_1

    .line 1074
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    move-result-object v6

    .line 1075
    .local v6, "mHeartrateDeviceData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    if-eqz v6, :cond_1

    .line 1076
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserDeviceId()Ljava/lang/String;

    move-result-object v11

    .line 1077
    .local v11, "userDeviceId":Ljava/lang/String;
    if-eqz v11, :cond_8

    .line 1078
    const-string v12, "10008"

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1e

    .line 1079
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1080
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1087
    :cond_8
    :goto_3
    sget-object v12, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v12}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v12

    sget-object v13, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v12, v13, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1088
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1089
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090a8c

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAccessoryName()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1091
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 938
    .end local v6    # "mHeartrateDeviceData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .end local v11    # "userDeviceId":Ljava/lang/String;
    :cond_9
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartRate:I

    if-lez v12, :cond_b

    .line 939
    sget-object v12, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v12}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v12

    sget-object v13, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v12, v13, :cond_a

    .line 940
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 941
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 946
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 947
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 948
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 949
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 950
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 943
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 944
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_4

    .line 952
    :cond_b
    sget-object v12, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v12}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v12

    sget-object v13, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v12, v13, :cond_14

    .line 953
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 954
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 959
    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondIcon:Landroid/widget/RelativeLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 960
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 961
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mNonSensorDateLayout:Landroid/widget/LinearLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 962
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCurrDateTxt:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 963
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCurrDateTxt:Landroid/widget/TextView;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v16

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v16

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 965
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v12, "yyyy/MM/dd"

    invoke-direct {v4, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 966
    .restart local v4    # "format":Ljava/text/SimpleDateFormat;
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 967
    .restart local v2    # "date":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 968
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v15

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 969
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mCurrDateTxt:Landroid/widget/TextView;

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 971
    .end local v2    # "date":Ljava/lang/String;
    .end local v4    # "format":Ljava/text/SimpleDateFormat;
    :cond_c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpmLayout:Landroid/widget/LinearLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 972
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 973
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 974
    if-eqz v5, :cond_12

    .line 975
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 976
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setHRMStateBar(I)V

    .line 977
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTag(J)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v1

    .line 979
    .local v1, "associatedTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    const/4 v9, -0x1

    .line 980
    .local v9, "tagIconId":I
    if-eqz v1, :cond_d

    .line 981
    iget-object v12, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    if-eqz v12, :cond_15

    iget-object v12, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_15

    .line 982
    iget-object v12, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    .line 993
    :goto_6
    iget v9, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 994
    iget v12, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    const/16 v13, 0x4e20

    if-eq v12, v13, :cond_16

    const/4 v12, 0x1

    :goto_7
    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagAssociated:Z

    .line 996
    :cond_d
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 998
    const/4 v12, -0x1

    if-eq v9, v12, :cond_e

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagAssociated:Z

    if-nez v12, :cond_18

    .line 999
    :cond_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f020209

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1000
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f02051d

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1001
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090f19

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1002
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090f19

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    .line 1003
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v12

    if-nez v12, :cond_17

    .line 1004
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0907b1

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1026
    :goto_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v12

    if-eqz v12, :cond_10

    .line 1027
    const/4 v12, -0x1

    if-eq v9, v12, :cond_f

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagAssociated:Z

    if-nez v12, :cond_10

    .line 1028
    :cond_f
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090c0d

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1029
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090c0d

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1032
    :cond_10
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->heartRateId:J

    .line 1033
    if-eqz v1, :cond_11

    iget v12, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mTagId:I

    if-nez v12, :cond_12

    .line 1034
    :cond_11
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->heartRateId:J

    const/16 v15, 0x4e20

    invoke-virtual {v12, v13, v14, v15}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->addTagToHeartRateLog(JI)V

    .line 1035
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagAssociated:Z

    .line 1040
    .end local v1    # "associatedTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v9    # "tagIconId":I
    :cond_12
    if-eqz v5, :cond_1d

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1d

    .line 1041
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1042
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdBpm:Landroid/widget/TextView;

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1043
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v16

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v16

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1045
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v12, "yyyy/MM/dd"

    invoke-direct {v4, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1046
    .restart local v4    # "format":Ljava/text/SimpleDateFormat;
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1047
    .restart local v2    # "date":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1048
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v15

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1049
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdDate:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0907af

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1050
    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v13

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getSampleTime()J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14, v15}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setHRMStateBarType2(IJ)V

    .line 1051
    const/4 v10, 0x0

    .line 1052
    .restart local v10    # "tagiconIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getId()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTag(J)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    move-result-object v7

    .line 1053
    .restart local v7    # "prevtagIndexHrId":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    if-eqz v7, :cond_13

    .line 1054
    iget v10, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mIconId:I

    .line 1055
    :cond_13
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f02051c

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1056
    if-eqz v10, :cond_1b

    const v12, 0x7f02053d

    if-eq v10, v12, :cond_1b

    .line 1057
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v12, v10}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1061
    :goto_9
    if-eqz v7, :cond_7

    .line 1062
    iget-object v12, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    if-eqz v12, :cond_1c

    iget-object v12, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_1c

    .line 1063
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    iget-object v13, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 956
    .end local v2    # "date":Ljava/lang/String;
    .end local v4    # "format":Ljava/text/SimpleDateFormat;
    .end local v7    # "prevtagIndexHrId":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v10    # "tagiconIndex":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mStateBar:Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/heartrate/barview/StateSummaryBar;->setVisibility(I)V

    .line 957
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mBarInidcator:Landroid/widget/FrameLayout;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_5

    .line 985
    .restart local v1    # "associatedTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .restart local v9    # "tagIconId":I
    :cond_15
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    iget v13, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_6

    .line 987
    :catch_1
    move-exception v8

    .line 988
    .local v8, "rnfe":Landroid/content/res/Resources$NotFoundException;
    sget-object v12, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v13, "error while loading tag name!"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    invoke-virtual {v8}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 990
    const-string v12, ""

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    goto/16 :goto_6

    .line 994
    .end local v8    # "rnfe":Landroid/content/res/Resources$NotFoundException;
    :cond_16
    const/4 v12, 0x0

    goto/16 :goto_7

    .line 1006
    :cond_17
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 1011
    :cond_18
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagText:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1012
    const v12, 0x7f02053d

    if-eq v9, v12, :cond_19

    .line 1013
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    invoke-virtual {v12, v9}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1016
    :goto_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isHrSensorNotAvailable()Z

    move-result v12

    if-nez v12, :cond_1a

    .line 1017
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0907b0

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1023
    :goto_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f020209

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_8

    .line 1015
    :cond_19
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f02051d

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_a

    .line 1019
    :cond_1a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_b

    .line 1059
    .end local v1    # "associatedTag":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v9    # "tagIconId":I
    .restart local v2    # "date":Ljava/lang/String;
    .restart local v4    # "format":Ljava/text/SimpleDateFormat;
    .restart local v7    # "prevtagIndexHrId":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .restart local v10    # "tagiconIndex":I
    :cond_1b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    const v13, 0x7f02051d

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_9

    .line 1065
    :cond_1c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevTagIcon:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    iget v14, v7, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;->mNameId:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1069
    .end local v2    # "date":Ljava/lang/String;
    .end local v4    # "format":Ljava/text/SimpleDateFormat;
    .end local v7    # "prevtagIndexHrId":Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
    .end local v10    # "tagiconIndex":I
    :cond_1d
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mThirdPreviousLayout:Landroid/widget/LinearLayout;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 1082
    .restart local v6    # "mHeartrateDeviceData":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .restart local v11    # "userDeviceId":Ljava/lang/String;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mTagIcon:Landroid/widget/ImageButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1083
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSelectedTag:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_3
.end method

.method public updateErrorDialogStringOnLocaleChange()V
    .locals 3

    .prologue
    .line 1960
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1961
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    .line 1962
    .local v0, "content":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1963
    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1964
    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0907ae

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1966
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg1:Landroid/widget/TextView;

    const v2, 0x7f090c28

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1967
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg2:Landroid/widget/TextView;

    const v2, 0x7f090c27

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1968
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg3:Landroid/widget/TextView;

    const v2, 0x7f090c25

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1969
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mErrorDialogMsg4:Landroid/widget/TextView;

    const v2, 0x7f090c2d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1972
    .end local v0    # "content":Landroid/view/View;
    :cond_0
    return-void
.end method
