.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BarcodePreviewCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 442
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    return-void
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 5
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 445
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusStateLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 446
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1200(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->FOCUSED:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    if-ne v0, v2, :cond_1

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->barcodeExecutor:Ljava/util/concurrent/ExecutorService;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    const/4 v4, 0x0

    invoke-direct {v2, v3, p1, v4}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodeTaker;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;[BLcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->READING_BARCODE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1202(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 453
    :cond_0
    :goto_0
    monitor-exit v1

    .line 454
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1200(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    if-ne v0, v2, :cond_0

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->FOCUSING:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->focusState:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1202(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$BarcodePreviewCallback;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->autoFocus()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->access$1900(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;)V

    goto :goto_0

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
