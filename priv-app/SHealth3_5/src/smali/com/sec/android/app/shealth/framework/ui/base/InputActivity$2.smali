.class Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    instance-of v0, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->isInputChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->showDiscardDialog()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->saveInputChanges()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V

    goto :goto_0
.end method
