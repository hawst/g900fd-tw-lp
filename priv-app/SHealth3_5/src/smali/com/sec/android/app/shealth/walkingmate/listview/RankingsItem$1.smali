.class Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;
.super Landroid/os/AsyncTask;
.source "RankingsItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getURLImages(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "strings"    # [Ljava/lang/String;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 325
    const/4 v0, 0x0

    .line 327
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->getDrawableFromUrl(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 321
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->doInBackground([Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/drawable/Drawable;)V
    .locals 7
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 334
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 335
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 336
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 337
    .local v1, "mainImage":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020616

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 338
    .local v2, "mask":Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->invert(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 340
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->mHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->access$000(Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v3

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->maskingProfileImage(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconSrc(Landroid/graphics/drawable/Drawable;)V

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->mHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->access$000(Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v3

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->maskingProfileImage(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconback(Landroid/graphics/drawable/Drawable;)V

    .line 342
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->updateIconDrawable()V

    .line 344
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v1    # "mainImage":Landroid/graphics/Bitmap;
    .end local v2    # "mask":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 321
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem$1;->onPostExecute(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
