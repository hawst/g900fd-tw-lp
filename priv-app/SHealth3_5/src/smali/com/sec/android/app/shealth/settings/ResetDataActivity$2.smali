.class Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;
.super Ljava/lang/Object;
.source "ResetDataActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/ResetDataActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 154
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 155
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v4

    if-nez v4, :cond_0

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 158
    const/4 v3, -0x1

    .line 159
    .local v3, "tempTextId":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;

    iget-object v4, v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;->mAppregistryData:Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v2, v4, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginSummary:Ljava/lang/String;

    .line 163
    .local v2, "tempText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->access$100(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 181
    const v3, 0x7f0908a0

    .line 187
    :goto_0
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    const/4 v5, 0x2

    invoke-direct {v1, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 189
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 190
    const v4, 0x7f0907e8

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 191
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 194
    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 201
    :goto_1
    const v4, 0x7f090032

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 202
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string/jumbo v6, "reset_data_popup"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 205
    .end local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .end local v2    # "tempText":Ljava/lang/String;
    .end local v3    # "tempTextId":I
    :cond_0
    return-void

    .line 184
    .restart local v2    # "tempText":Ljava/lang/String;
    .restart local v3    # "tempTextId":I
    :cond_1
    const v3, 0x7f09089d

    goto :goto_0

    .line 199
    .restart local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_2
    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_1
.end method
