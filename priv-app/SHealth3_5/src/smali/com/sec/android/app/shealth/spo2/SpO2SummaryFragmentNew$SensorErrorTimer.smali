.class public Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;
.super Landroid/os/CountDownTimer;
.source "SpO2SummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SensorErrorTimer"
.end annotation


# instance fields
.field errorType:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;JJ)V
    .locals 0
    .param p2, "millisInFuture"    # J
    .param p4, "countDownInterval"    # J

    .prologue
    .line 1551
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .line 1552
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1553
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1564
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->TAG:Ljava/lang/String;

    const-string v1, "SensorError Time out"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1566
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1568
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$902(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 1570
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopMeasuring()V

    .line 1572
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopCountDownTimer()V

    .line 1581
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1582
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1583
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1584
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    aget v1, v1, v2

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    .line 1585
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->errorType:I

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showErrorDialog(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    .line 1591
    :cond_0
    :goto_1
    return-void

    .line 1577
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopMeasuring()V

    .line 1579
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopCountDownTimer()V

    goto :goto_0

    .line 1588
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    sget-object v1, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->effectAudio:[I

    aget v1, v1, v2

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->playSound(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    .line 1589
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->errorType:I

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->showErrorDialog(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$4400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;I)V

    goto :goto_1
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1560
    return-void
.end method

.method public setErrorType(I)V
    .locals 0
    .param p1, "errorType"    # I

    .prologue
    .line 1556
    iput p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->errorType:I

    .line 1557
    return-void
.end method
