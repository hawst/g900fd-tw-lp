.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ProfileInformationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;
    }
.end annotation


# static fields
.field private static final GUIDE_CHECKED:Ljava/lang/String; = "guides_"

.field private static final GUIDE_COUNT:Ljava/lang/String; = "guides_count"


# instance fields
.field private final BMR:I

.field private final CALORIE:I

.field private linearLayoutContainer:Landroid/widget/LinearLayout;

.field private mCheckBoxList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mGuideList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollView:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->BMR:I

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->CALORIE:I

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mCheckBoxList:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->refreshFocusables()V

    return-void
.end method

.method private checkGuideWithoutAnimation(I)V
    .locals 3
    .param p1, "guideIndex"    # I

    .prologue
    const v2, 0x7f080632

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 94
    return-void
.end method

.method private initializeGuideList()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 98
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->linearLayoutContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 99
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 100
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    const/4 v4, 0x2

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    .line 103
    .local v2, "titleRes":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 104
    const/4 v3, 0x0

    .line 105
    .local v3, "view":Landroid/view/View;
    if-nez v0, :cond_1

    .line 106
    const v4, 0x7f0301eb

    invoke-virtual {v1, v4, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 111
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->linearLayoutContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 112
    aget v4, v2, v0

    invoke-direct {p0, v3, v4, v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->initializeGuidement(Landroid/view/View;II)V

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    .line 109
    const v4, 0x7f0301ec

    invoke-virtual {v1, v4, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 114
    .end local v3    # "view":Landroid/view/View;
    :cond_2
    return-void

    .line 100
    :array_0
    .array-data 4
        0x7f090855
        0x7f090856
    .end array-data
.end method

.method private initializeGuidement(Landroid/view/View;II)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "titleId"    # I
    .param p3, "index"    # I

    .prologue
    .line 118
    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    const v7, 0x7f08004d

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 121
    .local v1, "textView":Landroid/widget/TextView;
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 122
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09020b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090217

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 123
    const-string v0, ""

    .line 125
    .local v0, "str":Ljava/lang/String;
    const/4 v7, 0x1

    if-ne p3, v7, :cond_0

    .line 127
    const v7, 0x7f0807c5

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 128
    .local v3, "textView2":Landroid/widget/TextView;
    const v7, 0x7f080890

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 129
    .local v4, "textView4":Landroid/widget/TextView;
    const v7, 0x7f080892

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 130
    .local v5, "textView6":Landroid/widget/TextView;
    const v7, 0x7f080894

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 131
    .local v6, "textView8":Landroid/widget/TextView;
    const v7, 0x7f080896

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 133
    .local v2, "textView10":Landroid/widget/TextView;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f09082e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    const-string v7, "\n"

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 135
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 137
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f09082f

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    const-string v7, "\n"

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 139
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 141
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f090830

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    const-string v7, "\n"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 143
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 145
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f090864

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    const-string v7, "\n"

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 147
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 149
    .end local v2    # "textView10":Landroid/widget/TextView;
    .end local v3    # "textView2":Landroid/widget/TextView;
    .end local v4    # "textView4":Landroid/widget/TextView;
    .end local v5    # "textView6":Landroid/widget/TextView;
    .end local v6    # "textView8":Landroid/widget/TextView;
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0900e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 158
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v3, 0x7f0301ea

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->setContentView(I)V

    .line 60
    const v3, 0x7f0804e2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 61
    const v3, 0x7f0804e7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->linearLayoutContainer:Landroid/widget/LinearLayout;

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->initializeGuideList()V

    .line 63
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 64
    .local v0, "animatedViewContainer":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mCheckBoxList:Ljava/util/List;

    const v3, 0x7f08004d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    .end local v0    # "animatedViewContainer":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v5, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mGuideList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v4, v1, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity$CheckBoxCheckedListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;Landroid/view/View;IZ)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 67
    :cond_1
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 82
    const-string v2, "guides_count"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 83
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "guides_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->checkGuideWithoutAnimation(I)V

    .line 83
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 72
    const-string v1, "guides_count"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "guides_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;->mCheckBoxList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    return-void
.end method
