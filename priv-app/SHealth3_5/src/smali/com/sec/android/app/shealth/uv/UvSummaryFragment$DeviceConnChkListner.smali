.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeviceConnChkListner"
.end annotation


# instance fields
.field statusView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "connStatusView"    # Landroid/view/View;

    .prologue
    .line 700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 701
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;->statusView:Ljava/lang/ref/WeakReference;

    .line 702
    return-void
.end method


# virtual methods
.method public onDeviceConnectionChecked(Z)V
    .locals 2
    .param p1, "isConnected"    # Z

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;->statusView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;->statusView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner$1;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment$DeviceConnChkListner;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 721
    :cond_0
    return-void
.end method
