.class Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;
.super Ljava/lang/Object;
.source "GeneralMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteMealButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V
    .locals 0

    .prologue
    .line 635
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;

    .prologue
    .line 635
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 4
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 638
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v1, :cond_0

    .line 639
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    .line 640
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 641
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "MEAL_WAS_EDITED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 642
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->setResult(ILandroid/content/Intent;)V

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->finish()V

    .line 645
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
