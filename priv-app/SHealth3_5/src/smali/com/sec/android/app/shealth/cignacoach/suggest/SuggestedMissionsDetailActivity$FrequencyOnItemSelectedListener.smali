.class public Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$FrequencyOnItemSelectedListener;
.super Ljava/lang/Object;
.source "SuggestedMissionsDetailActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FrequencyOnItemSelectedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$FrequencyOnItemSelectedListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$FrequencyOnItemSelectedListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$FrequencyOnItemSelectedListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSpinnerAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity$SuggestedSpinnerAdapter;->getFrequencyId(I)I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->mSelectedFrequencyID:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;->access$002(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;I)I

    .line 263
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 267
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
