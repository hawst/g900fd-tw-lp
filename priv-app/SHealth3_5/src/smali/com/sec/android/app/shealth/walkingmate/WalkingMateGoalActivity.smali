.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateGoalActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;
.implements Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;


# static fields
.field public static DEFAULT_GOAL:I = 0x0

.field private static DLG_KEY_GOAL_OUT_RANGE:Ljava/lang/String; = null

.field private static DLG_RESET:Ljava/lang/String; = null

.field public static MAX_GOAL:I = 0x0

.field public static MIN_GOAL:I = 0x0

.field public static STEP_GOAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "WalkingMateGoalActivity"

.field private static mController:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected averageValue:I

.field private averageValueTextView:Landroid/widget/TextView;

.field protected bestRecordValue:I

.field private bestRecordValueTextView:Landroid/widget/TextView;

.field private goalValue:Ljava/lang/Long;

.field private mLoadedGoal:I

.field private mWalkingGoal:Ljava/lang/String;

.field private presentGoal:Ljava/lang/String;

.field private walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-string v0, "goal_out_of_range"

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DLG_KEY_GOAL_OUT_RANGE:Ljava/lang/String;

    .line 76
    const-string/jumbo v0, "reset"

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DLG_RESET:Ljava/lang/String;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mController:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkForValidGoal()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->setMinValue()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkChangesAndExit()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkAndSaveGoalToDB()V

    return-void
.end method

.method private checkAndSaveGoalToDB()V
    .locals 8

    .prologue
    const v5, 0x7f090b61

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 201
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkForValidGoal()Z

    move-result v2

    if-nez v2, :cond_1

    .line 204
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    sget v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    sget v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "acceptedSteps":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 210
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090b60

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090f70

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 216
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DLG_KEY_GOAL_OUT_RANGE:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 223
    .end local v0    # "acceptedSteps":Ljava/lang/String;
    .end local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :goto_1
    return-void

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "acceptedSteps":Ljava/lang/String;
    goto :goto_0

    .line 220
    .end local v0    # "acceptedSteps":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->saveGoalToDB()V

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->finish()V

    goto :goto_1
.end method

.method private checkChangesAndExit()V
    .locals 1

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->isDataChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->showResetPopup()V

    .line 304
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->finish()V

    goto :goto_0
.end method

.method private checkForValidGoal()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 162
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->isValueEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v1

    .line 166
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v2

    float-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->goalValue:Ljava/lang/Long;

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->goalValue:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->goalValue:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 171
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private isDataChanged()Z
    .locals 3

    .prologue
    .line 295
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->presentGoal:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 296
    .local v0, "result":Z
    :goto_0
    return v0

    .line 295
    .end local v0    # "result":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private saveGoalToDB()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->getValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mWalkingGoal:Ljava/lang/String;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mWalkingGoal:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    float-to-int v7, v0

    .line 183
    .local v7, "changedGoal":I
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mLoadedGoal:I

    div-int/lit8 v0, v0, 0x2

    if-gt v0, v7, :cond_0

    .line 184
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->clearGoalNotification()V

    .line 191
    :goto_0
    const-string v0, "WalkingMateGoalActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "saved goal = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mWalkingGoal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x9c41

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mWalkingGoal:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move v3, v1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->insertitem(Landroid/content/Context;IIIFJ)J

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090835

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 197
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->refreshTodayGoalFromDB()V

    .line 198
    return-void

    .line 186
    :cond_0
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setGoalNotification(Z)V

    goto :goto_0
.end method

.method private setAverageGoalValueTextView()V
    .locals 6

    .prologue
    const v4, 0x7f090b8d

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 263
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getAverageSteps(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->averageValue:I

    .line 265
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->averageValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "text":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->averageValueTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    return-void

    .line 268
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->averageValue:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method private setBestRecordGoalValueTextView()V
    .locals 6

    .prologue
    const v4, 0x7f090b8d

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 274
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getBestStepValue(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->bestRecordValue:I

    .line 276
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->bestRecordValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "text":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->bestRecordValueTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    return-void

    .line 279
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->bestRecordValue:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method private setMinValue()V
    .locals 4

    .prologue
    .line 150
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 151
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    sget v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    .line 156
    return-void
.end method

.method private showResetPopup()V
    .locals 4

    .prologue
    .line 285
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 287
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090083

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->addFlags(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 290
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DLG_RESET:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 291
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 229
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 233
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V

    .line 256
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f090048

    invoke-direct {v3, v5, v4, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f090044

    invoke-direct {v3, v5, v4, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 260
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 308
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mController:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 0
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 323
    return-object p0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 88
    const v0, 0x7f03028e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->setContentView(I)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DEFAULT_GOAL:I

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->STEP_GOAL:I

    .line 95
    const v0, 0x7f080b88

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->getValueEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0ae8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setMinWidth(I)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setEnabled(Z)V

    .line 98
    const v0, 0x7f080bb6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->averageValueTextView:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f080bba

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->bestRecordValueTextView:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0909d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setUnit(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    sget v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->STEP_GOAL:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setMoveDistance(F)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    sget v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    int-to-float v1, v1

    sget v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setInputRange(FF)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    sget v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    int-to-float v1, v1

    sget v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    sget v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->STEP_GOAL:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setNormalRange(FF)V

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtils;->loadGoalAndSearch(J)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mLoadedGoal:I

    .line 107
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mLoadedGoal:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->presentGoal:Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mLoadedGoal:I

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setValue(FZ)V

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->setAverageGoalValueTextView()V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->setBestRecordGoalValueTextView()V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->removeEditTextFocus()V

    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mController:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 115
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mController:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DLG_KEY_GOAL_OUT_RANGE:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->mController:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DLG_RESET:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->walkingInputModule:Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/ProHorizontalInputModule;->setmMinValueCallback(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$IMinValueNotifier;)V

    .line 145
    return-void
.end method

.method public onDismiss()V
    .locals 1

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->checkForValidGoal()Z

    move-result v0

    if-nez v0, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->setMinValue()V

    .line 319
    :cond_0
    return-void
.end method

.method public onMinValueReceived()V
    .locals 8

    .prologue
    const v5, 0x7f090b61

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 329
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    sget v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    sget v5, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 335
    .local v0, "acceptedSteps":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 336
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090b60

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 338
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090f70

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 342
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->DLG_KEY_GOAL_OUT_RANGE:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 343
    return-void

    .line 332
    .end local v0    # "acceptedSteps":Ljava/lang/String;
    .end local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MIN_GOAL:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget v4, Lcom/sec/android/app/shealth/walkingmate/WalkingMateGoalActivity;->MAX_GOAL:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "acceptedSteps":Ljava/lang/String;
    goto :goto_0
.end method
