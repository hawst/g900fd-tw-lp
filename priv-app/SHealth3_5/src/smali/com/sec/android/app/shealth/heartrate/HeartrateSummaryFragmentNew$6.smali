.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->startSensorforSCover(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 829
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 831
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    if-nez v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->getInstance()Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2502(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    .line 837
    :goto_0
    return-void

    .line 835
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$2500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateActivity:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1000(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$6;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDeviceConnector;->startMeasuring(Landroid/content/Context;Lcom/sec/android/app/shealth/heartrate/HeartrateSensorListener;Z)V

    goto :goto_0
.end method
