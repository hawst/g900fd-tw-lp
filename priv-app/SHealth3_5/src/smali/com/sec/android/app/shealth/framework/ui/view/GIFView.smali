.class public Lcom/sec/android/app/shealth/framework/ui/view/GIFView;
.super Landroid/view/View;


# static fields
.field private static final CACHE_SIZE:I = 0x100000

.field private static final DEFAULT_MOVIEW_DURATION:I = 0x3e8

.field private static sMovieCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Movie;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final MOVIE_TAG:Ljava/lang/String;

.field private mCurrentAnimationTime:I

.field private mLeft:F

.field private mMeasuredMovieHeight:I

.field private mMeasuredMovieWidth:I

.field private mMovie:Landroid/graphics/Movie;

.field private mMovieResourceId:I

.field private mMovieStart:J

.field private volatile mPaused:Z

.field private mScale:F

.field private mTop:F

.field private mVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/support/v4/util/LruCache;

    const/high16 v1, 0x100000

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->sMovieCache:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mCurrentAnimationTime:I

    const-string/jumbo v0, "movie"

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->MOVIE_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mPaused:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mVisible:Z

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private drawMovieFrame(Landroid/graphics/Canvas;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mCurrentAnimationTime:I

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mScale:F

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mScale:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mLeft:F

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mScale:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mTop:F

    iget v3, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mScale:F

    div-float/2addr v2, v3

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private invalidateView()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mVisible:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->postInvalidateOnAnimation()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->invalidate()V

    goto :goto_0
.end method

.method private setViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovieResourceId:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->sMovieCache:Landroid/support/v4/util/LruCache;

    const-string/jumbo v2, "movie"

    invoke-virtual {v0, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->frag_scanning:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->sMovieCache:Landroid/support/v4/util/LruCache;

    const-string/jumbo v2, "movie"

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->sMovieCache:Landroid/support/v4/util/LruCache;

    const-string/jumbo v1, "movie"

    invoke-virtual {v0, v1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Movie;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    goto :goto_0
.end method

.method private updateAnimationTime()V
    .locals 7

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovieStart:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-nez v0, :cond_0

    iput-wide v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovieStart:J

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->duration()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x3e8

    :cond_1
    iget-wide v3, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovieStart:J

    sub-long/2addr v1, v3

    int-to-long v3, v0

    rem-long v0, v1, v3

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mCurrentAnimationTime:I

    return-void
.end method


# virtual methods
.method public getMovie()Landroid/graphics/Movie;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mPaused:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->updateAnimationTime()V

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->drawMovieFrame(Landroid/graphics/Canvas;)V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->invalidateView()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->drawMovieFrame(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMeasuredMovieWidth:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mLeft:F

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMeasuredMovieHeight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mTop:F

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mVisible:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->width()I

    move-result v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->height()I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-le v3, v0, :cond_2

    int-to-float v2, v3

    int-to-float v0, v0

    div-float v0, v2, v0

    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    if-le v4, v2, :cond_1

    int-to-float v5, v4

    int-to-float v2, v2

    div-float v2, v5, v2

    :goto_1
    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mScale:F

    int-to-float v0, v3

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mScale:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMeasuredMovieWidth:I

    int-to-float v0, v4

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mScale:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMeasuredMovieHeight:I

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMeasuredMovieWidth:I

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMeasuredMovieHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->setMeasuredDimension(II)V

    :goto_2
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->setMeasuredDimension(II)V

    goto :goto_2

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onScreenStateChanged(I)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/view/View;->onScreenStateChanged(I)V

    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mVisible:Z

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->invalidateView()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mVisible:Z

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->invalidateView()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mVisible:Z

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->invalidateView()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMovie(Landroid/graphics/Movie;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->requestLayout()V

    return-void
.end method

.method public setMovieResource(I)V
    .locals 3

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovieResourceId:I

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovieResourceId:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mMovie:Landroid/graphics/Movie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->requestLayout()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public setMovieTime(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->mCurrentAnimationTime:I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/GIFView;->invalidate()V

    return-void
.end method
