.class Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;
.super Ljava/lang/Object;
.source "SpO2LogDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->showDeletePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$400(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->mSpO2Data:Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->access$000(Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->deleteRowById(Ljava/lang/String;)Z

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->setResult(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/log/SpO2LogDetailActivity;->finish()V

    .line 193
    return-void
.end method
