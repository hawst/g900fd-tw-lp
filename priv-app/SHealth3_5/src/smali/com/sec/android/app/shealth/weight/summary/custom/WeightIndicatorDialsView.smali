.class Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;
.super Landroid/view/View;
.source "WeightIndicatorDialsView.java"


# instance fields
.field private mBoldDialsPaint:Landroid/graphics/Paint;

.field private mDialsPaint:Landroid/graphics/Paint;

.field private mDialsTextPaint:Landroid/graphics/Paint;

.field private mGoalLinePaint:Landroid/graphics/Paint;

.field private mGoalValue:F

.field private mRadius:I

.field private mWeightValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initialize()V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initialize()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initialize()V

    .line 60
    return-void
.end method

.method private drawDials(Landroid/graphics/Canvas;FF)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "coordinateX"    # F
    .param p3, "position"    # F

    .prologue
    .line 127
    invoke-static {p3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isWholeNumber(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mBoldDialsPaint:Landroid/graphics/Paint;

    .line 128
    .local v5, "paint":Landroid/graphics/Paint;
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0b1a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 129
    .local v6, "dialsYPosition":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0b18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 130
    .local v7, "lineLength":I
    int-to-float v2, v6

    sub-int v0, v6, v7

    int-to-float v4, v0

    move-object v0, p1

    move v1, p2

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 131
    return-void

    .line 127
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "dialsYPosition":I
    .end local v7    # "lineLength":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsPaint:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method private drawDialsText(Landroid/graphics/Canvas;FF)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "coordinateX"    # F
    .param p3, "position"    # F

    .prologue
    .line 134
    invoke-static {p3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isWholeNumber(F)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0b1c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 136
    .local v0, "dialsTextYPosition":I
    float-to-int v1, p3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    int-to-float v2, v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, p2, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 138
    .end local v0    # "dialsTextYPosition":I
    :cond_0
    return-void
.end method

.method private drawGoalLine(Landroid/graphics/Canvas;FFF)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "pivotCoordinateX"    # F
    .param p3, "pivotCoordinateY"    # F
    .param p4, "position"    # F

    .prologue
    .line 141
    invoke-direct {p0, p4}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->isGoalPosition(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const/4 v2, 0x0

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mGoalLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 144
    :cond_0
    return-void
.end method

.method private initialize()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initializePaint()V

    .line 64
    return-void
.end method

.method private initializeBoldDialsPaint()V
    .locals 3

    .prologue
    .line 82
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsPaint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mBoldDialsPaint:Landroid/graphics/Paint;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mBoldDialsPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mBoldDialsPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->calculateAlpha(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 85
    return-void
.end method

.method private initializeDialsPaint()V
    .locals 3

    .prologue
    .line 75
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsPaint:Landroid/graphics/Paint;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0b15

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->calculateAlpha(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 79
    return-void
.end method

.method private initializeDialsTextPaint()V
    .locals 4

    .prologue
    .line 94
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsTextPaint:Landroid/graphics/Paint;

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0055

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->calculateAlpha(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 98
    const-string v1, "Roboto-Light"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 99
    .local v0, "typeface":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0b14

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 101
    return-void
.end method

.method private initializeGoalLinePaint()V
    .locals 3

    .prologue
    .line 88
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mDialsPaint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mGoalLinePaint:Landroid/graphics/Paint;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mGoalLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0b16

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mGoalLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    return-void
.end method

.method private initializePaint()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initializeDialsPaint()V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initializeBoldDialsPaint()V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initializeGoalLinePaint()V

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->initializeDialsTextPaint()V

    .line 71
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->setBackgroundColor(I)V

    .line 72
    return-void
.end method

.method private isGoalPosition(F)Z
    .locals 1
    .param p1, "position"    # F

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mGoalValue:F

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getPivotX()F

    move-result v2

    .line 113
    .local v2, "pivotX":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->getPivotY()F

    move-result v3

    .line 114
    .local v3, "pivotY":F
    iget v4, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mWeightValue:F

    invoke-static {v4}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getMinWeightInPanel(F)F

    move-result v1

    .line 115
    .local v1, "currentDialValue":F
    const/4 v0, 0x0

    .local v0, "angle":I
    :goto_0
    const/16 v4, 0x10e

    if-ge v0, v4, :cond_1

    .line 116
    invoke-direct {p0, p1, v2, v3, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->drawGoalLine(Landroid/graphics/Canvas;FFF)V

    .line 117
    rem-int/lit8 v4, v0, 0x2

    if-nez v4, :cond_0

    .line 118
    invoke-direct {p0, p1, v2, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->drawDials(Landroid/graphics/Canvas;FF)V

    .line 119
    invoke-direct {p0, p1, v2, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->drawDialsText(Landroid/graphics/Canvas;FF)V

    .line 121
    :cond_0
    invoke-virtual {p1, v6, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 122
    const v4, 0x3dcccccd    # 0.1f

    add-float/2addr v4, v1

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToDigit(FI)F

    move-result v1

    .line 115
    int-to-float v4, v0

    add-float/2addr v4, v6

    float-to-int v0, v4

    goto :goto_0

    .line 124
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 106
    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mRadius:I

    mul-int/lit8 v0, v1, 0x2

    .line 107
    .local v0, "size":I
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->setMeasuredDimension(II)V

    .line 108
    return-void
.end method

.method setGoalValue(F)V
    .locals 0
    .param p1, "goalValue"    # F

    .prologue
    .line 165
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mGoalValue:F

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->invalidate()V

    .line 167
    return-void
.end method

.method setRadius(I)V
    .locals 0
    .param p1, "radius"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mRadius:I

    .line 157
    return-void
.end method

.method setWeightValue(F)V
    .locals 0
    .param p1, "weightValue"    # F

    .prologue
    .line 175
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->mWeightValue:F

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->invalidate()V

    .line 177
    return-void
.end method
