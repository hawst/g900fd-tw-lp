.class public Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;
.super Ljava/lang/Object;
.source "EearnedBadgeCountUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static volatile myLock:Ljava/lang/Object;

.field private static sInstance:Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;


# instance fields
.field private mRemainCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->TAG:Ljava/lang/String;

    .line 10
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->myLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;
    .locals 2

    .prologue
    .line 16
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->myLock:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->sInstance:Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clearBadgeCount()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->mRemainCount:I

    .line 46
    return-void
.end method

.method public declared-synchronized getRemainBadgeCount()I
    .locals 1

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->mRemainCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reduceRemainBadgeCount()V
    .locals 1

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->mRemainCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->mRemainCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit p0

    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setRemainBadgeCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/util/EearnedBadgeCountUtils;->mRemainCount:I

    .line 42
    return-void
.end method
