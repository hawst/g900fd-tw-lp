.class public interface abstract Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$ExerciseStaticKeys;
.super Ljava/lang/Object;
.source "SharedPreferencesHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExerciseStaticKeys"
.end annotation


# static fields
.field public static final BACKUP_REALTIME_KEY_GOAL_TYPE:Ljava/lang/String; = "pro_backup_realtime_goal_type"

.field public static final BACKUP_REALTIME_KEY_GOAL_VALUE:Ljava/lang/String; = "pro_backup_realtime_goal_value"

.field public static final BACKUP_REALTIME_KEY_TRAINING_LEVEL:Ljava/lang/String; = "pro_backup_realtime_training_goal_value"

.field public static final CHART_FLAG_BUTTON:Ljava/lang/String; = "pro_chartFlagButton"

.field public static final COACHING_SETGOAL_SELECT:Ljava/lang/String; = "pro_coaching_setgoal_select"

.field public static final EX_CHART_FLAG_BUTTON:Ljava/lang/String; = "pro_chartFlagButton"

.field public static final EX_DUPLICATE_POPUP_FLAG:Ljava/lang/String; = "pro_duplicate_popup_flag"

.field public static final EX_LOCATION_POPUP_FLAG:Ljava/lang/String; = "pro_location_popup_flag"

.field public static final EX_TEMP_DEVICEID_INDEX:Ljava/lang/String; = "pro_TEMP_DEVICEID_INDEX"

.field public static final EX_TEMP_EXERCISE_ID:Ljava/lang/String; = "pro_temp_exercise_id"

.field public static final FIRSTBEAT_ASCR_ACTIVATED:Ljava/lang/String; = "pro_firstbeat_ascr_activated"

.field public static final NEED_MAX_HR_CONFIRM:Ljava/lang/String; = "pro_need_max_hr_confirm"

.field public static final REALITME_MAP_ZOOM_LEVEL:Ljava/lang/String; = "pro_realtime_map_zoom_level"

.field public static final REALTIME_EXERCISE_ID:Ljava/lang/String; = "pro_realtime_exercise_id"

.field public static final REALTIME_FREQUENCY_OF_AUDIO_GUIDE:Ljava/lang/String; = "pro_realtime_frequency_of_audio_guide"

.field public static final REALTIME_KEY_ACTIVITY_TYPE:Ljava/lang/String; = "pro_realtime_activity_type"

.field public static final REALTIME_KEY_AUDIO_GUIDE_ON:Ljava/lang/String; = "pro_realtime_audio_guide_on"

.field public static final REALTIME_KEY_BEAT_MY_PREVIOUS_RECORD_EXID:Ljava/lang/String; = "pro_realtime_key_beat_my_previous_record_exid"

.field public static final REALTIME_KEY_BEAT_MY_PREVIOUS_RECORD_VALUE:Ljava/lang/String; = "pro_realtime_beat_my_previous_record_value"

.field public static final REALTIME_KEY_CALORIES_VALUE:Ljava/lang/String; = "pro_realtime_calories_value"

.field public static final REALTIME_KEY_CYCLING_GOAL_TYPE:Ljava/lang/String; = "pro_realtime_cycling_goal_type"

.field public static final REALTIME_KEY_DISPLAY_DATA_BACKUP:Ljava/lang/String; = "pro_realtime_disp_data_backup"

.field public static final REALTIME_KEY_DISPLAY_DATA_MAP:Ljava/lang/String; = "pro_realtime_disp_data_map"

.field public static final REALTIME_KEY_DISPLAY_DATA_STATUS:Ljava/lang/String; = "pro_realtime_disp_data_status"

.field public static final REALTIME_KEY_DISTANCE_VALUE:Ljava/lang/String; = "pro_realtime_distance_value"

.field public static final REALTIME_KEY_EQUIPMENT_TYPE:Ljava/lang/String; = "pro_realtime_equipment_type"

.field public static final REALTIME_KEY_GOAL_TYPE:Ljava/lang/String; = "pro_realtime_goal_type"

.field public static final REALTIME_KEY_GYM_GUIDE_POPUP_WAS_SHOWN:Ljava/lang/String; = "pro_realtime_gym_guide_popup_was_shown"

.field public static final REALTIME_KEY_HIKING_GOAL_TYPE:Ljava/lang/String; = "pro_realtime_hiking_goal_type"

.field public static final REALTIME_KEY_LOCATION_POPUP_WAS_SHOWN:Ljava/lang/String; = "pro_realtime_location_popup_was_shown"

.field public static final REALTIME_KEY_MODE:Ljava/lang/String; = "pro_realtime_mode"

.field public static final REALTIME_KEY_RUNNING_GOAL_TYPE:Ljava/lang/String; = "pro_realtime_running_goal_type"

.field public static final REALTIME_KEY_TIME_VALUE:Ljava/lang/String; = "pro_realtime_time_value"

.field public static final REALTIME_KEY_TRAINING_LEVEL:Ljava/lang/String; = "pro_realtime_training_level"

.field public static final REALTIME_KEY_TRAINING_TIME_VALUE:Ljava/lang/String; = "pro_realtime_training_time_value"

.field public static final REALTIME_KEY_WALKING_GOAL_TYPE:Ljava/lang/String; = "pro_realtime_walking_goal_type"

.field public static final REALTIME_TYPE_OF_FEEDBACK:Ljava/lang/String; = "pro_realtime_type_of_feedback"

.field public static final TEMPERATURE_UNIT:Ljava/lang/String; = "pro_temperature_unit"

.field public static final TRAINING_PROGRAM_LIST:Ljava/lang/String; = "pro_training_program_list"

.field public static final UI_LOCK_STATE:Ljava/lang/String; = "pro_ui_lock_state"

.field public static final WALKMATE_SENSOR_LISTENER_REGISTERED:Ljava/lang/String; = "pro_walkmate_sensor_register_state"

.field public static final WORKOUT_SELECT_PAGE:Ljava/lang/String; = "pro_workout_select_page"
