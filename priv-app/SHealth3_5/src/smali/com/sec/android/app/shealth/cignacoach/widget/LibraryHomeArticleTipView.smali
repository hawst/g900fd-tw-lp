.class public Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;
.super Landroid/widget/RelativeLayout;
.source "LibraryHomeArticleTipView.java"


# instance fields
.field private mBackgroundLayout:Landroid/widget/RelativeLayout;

.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->initLayout()V

    .line 28
    return-void
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03005d

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    const v0, 0x7f0801cd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    .line 33
    const v0, 0x7f0801cf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->mTitleTextView:Landroid/widget/TextView;

    .line 34
    return-void
.end method


# virtual methods
.method public setBackground(I)V
    .locals 1
    .param p1, "backgroundResID"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 50
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 46
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "titleResID"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 42
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/LibraryHomeArticleTipView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    return-void
.end method
